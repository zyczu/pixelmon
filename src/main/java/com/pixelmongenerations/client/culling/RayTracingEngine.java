/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.culling;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.storage.ExtendedBlockStorage;

public final class RayTracingEngine {
    private static final BlockPos.MutableBlockPos MUTABLE_POS = new BlockPos.MutableBlockPos();
    private static int cachedChunkX = 0;
    private static int cachedChunkY = 0;
    private static int cachedChunkZ = 0;
    private static ExtendedBlockStorage cachedChunk = null;
    private static boolean isChunkCached = false;

    private RayTracingEngine() {
    }

    private static boolean isOpaqueBlock(World world, int x, int y, int z) {
        if (world.isOutsideBuildHeight(MUTABLE_POS.setPos(x, y, z))) {
            return false;
        }
        if (!isChunkCached || x >> 4 != cachedChunkX || y >> 4 != cachedChunkY || z >> 4 != cachedChunkZ) {
            cachedChunkX = x >> 4;
            cachedChunkY = y >> 4;
            cachedChunkZ = z >> 4;
            cachedChunk = world.getChunkProvider().provideChunk(cachedChunkX, cachedChunkZ).getBlockStorageArray()[cachedChunkY];
            isChunkCached = true;
        }
        if (cachedChunk == null) {
            return false;
        }
        IBlockState state = cachedChunk.get(x & 0xF, y & 0xF, z & 0xF);
        return state.isOpaqueCube();
    }

    public static void resetCache() {
        cachedChunkX = 0;
        cachedChunkY = 0;
        cachedChunkZ = 0;
        cachedChunk = null;
        isChunkCached = false;
    }

    public static MutableRayTraceResult rayTraceBlocks(World world, double startX, double startY, double startZ, double endX, double endY, double endZ, boolean ignoreStart, double maxIgnore, MutableRayTraceResult returnValue) {
        double dirX = endX - startX;
        double dirY = endY - startY;
        double dirZ = endZ - startZ;
        if (maxIgnore <= 0.0) {
            return returnValue.set(startX, startY, startZ, EnumFacing.getFacingFromVector((float)dirX, (float)dirY, (float)dirZ).getOpposite());
        }
        if (dirX * dirX + dirY * dirY + dirZ * dirZ < maxIgnore * maxIgnore) {
            return null;
        }
        int incX = RayTracingEngine.signum(dirX);
        int incY = RayTracingEngine.signum(dirY);
        int incZ = RayTracingEngine.signum(dirZ);
        double dx = incX == 0 ? Double.MAX_VALUE : (double)incX / dirX;
        double dy = incY == 0 ? Double.MAX_VALUE : (double)incY / dirY;
        double dz = incZ == 0 ? Double.MAX_VALUE : (double)incZ / dirZ;
        double percentX = dx * (incX > 0 ? 1.0 - RayTracingEngine.frac(startX) : RayTracingEngine.frac(startX));
        double percentY = dy * (incY > 0 ? 1.0 - RayTracingEngine.frac(startY) : RayTracingEngine.frac(startY));
        double percentZ = dz * (incZ > 0 ? 1.0 - RayTracingEngine.frac(startZ) : RayTracingEngine.frac(startZ));
        EnumFacing facingX = incX > 0 ? EnumFacing.WEST : EnumFacing.EAST;
        EnumFacing facingY = incY > 0 ? EnumFacing.DOWN : EnumFacing.UP;
        EnumFacing facingZ = incZ > 0 ? EnumFacing.NORTH : EnumFacing.SOUTH;
        int x = RayTracingEngine.floor(startX);
        int y = RayTracingEngine.floor(startY);
        int z = RayTracingEngine.floor(startZ);
        boolean hasHitBlock = false;
        boolean hasHitBlockPreviously = false;
        double firstHitX = startX;
        double firstHitY = startY;
        double firstHitZ = startZ;
        EnumFacing firstHitFacing = EnumFacing.NORTH;
        double lastHitX = startX;
        double lastHitY = startY;
        double lastHitZ = startZ;
        if (!ignoreStart && RayTracingEngine.isOpaqueBlock(world, x, y, z)) {
            hasHitBlock = true;
            hasHitBlockPreviously = true;
            firstHitFacing = EnumFacing.getFacingFromVector((float)dirX, (float)dirY, (float)dirZ).getOpposite();
        }
        while (percentX <= 1.0 || percentY <= 1.0 || percentZ <= 1.0) {
            EnumFacing facing;
            if (percentX < percentY) {
                if (percentX < percentZ) {
                    x += incX;
                    percentX += dx;
                    facing = facingX;
                } else {
                    z += incZ;
                    percentZ += dz;
                    facing = facingZ;
                }
            } else if (percentY < percentZ) {
                y += incY;
                percentY += dy;
                facing = facingY;
            } else {
                z += incZ;
                percentZ += dz;
                facing = facingZ;
            }
            boolean hitOpaqueBlock = RayTracingEngine.isOpaqueBlock(world, x, y, z);
            if (!hasHitBlockPreviously && !hitOpaqueBlock) continue;
            double d = facing.getAxis() == EnumFacing.Axis.X ? percentX - dx : (facing.getAxis() == EnumFacing.Axis.Y ? percentY - dy : percentZ - dz);
            double d1 = startX + dirX * d;
            double d2 = startY + dirY * d;
            double d3 = startZ + dirZ * d;
            if (!hasHitBlock) {
                if (!hitOpaqueBlock) continue;
                hasHitBlock = true;
                hasHitBlockPreviously = true;
                firstHitFacing = facing;
                firstHitX = d1;
                firstHitY = d2;
                firstHitZ = d3;
                lastHitX = d1;
                lastHitY = d2;
                lastHitZ = d3;
                continue;
            }
            if (hasHitBlockPreviously && (maxIgnore -= Math.sqrt(RayTracingEngine.squareDist(lastHitX, lastHitY, lastHitZ, d1, d2, d3))) <= 0.0) {
                return returnValue.set(firstHitX, firstHitY, firstHitZ, firstHitFacing);
            }
            if (hitOpaqueBlock) {
                lastHitX = startX + dirX * d;
                lastHitY = startY + dirY * d;
                lastHitZ = startZ + dirZ * d;
                hasHitBlockPreviously = true;
                continue;
            }
            hasHitBlockPreviously = false;
        }
        return null;
    }

    private static double lerp(double pct, double start, double end) {
        return start + pct * (end - start);
    }

    private static int floor(double value) {
        int i = (int)value;
        return value < (double)i ? i - 1 : i;
    }

    private static int signum(double x) {
        if (x == 0.0) {
            return 0;
        }
        return x > 0.0 ? 1 : -1;
    }

    private static double frac(double number) {
        return number - (double)RayTracingEngine.lfloor(number);
    }

    private static long lfloor(double value) {
        long i = (long)value;
        return value < (double)i ? i - 1L : i;
    }

    private static double squareDist(double x1, double y1, double z1, double x2, double y2, double z2) {
        return (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1) + (z2 - z1) * (z2 - z1);
    }

    public static class MutableRayTraceResult {
        public double x;
        public double y;
        public double z;
        public EnumFacing facing;

        public MutableRayTraceResult() {
            this.x = 0.0;
            this.y = 0.0;
            this.z = 0.0;
            this.facing = EnumFacing.NORTH;
        }

        public MutableRayTraceResult(double x, double y, double z, EnumFacing facing) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.facing = facing;
        }

        public MutableRayTraceResult set(double x, double y, double z, EnumFacing facing) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.facing = facing;
            return this;
        }

        public double squareDist(MutableRayTraceResult other) {
            return this.squareDist(other.x, other.y, other.z);
        }

        public double squareDist(double x, double y, double z) {
            double d1 = x - this.x;
            double d2 = y - this.y;
            double d3 = z - this.z;
            return d1 * d1 + d2 * d2 + d3 * d3;
        }
    }
}

