/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.client.culling;

import java.util.Arrays;
import javax.annotation.Nullable;

public class RayTracingCache {
    public final int radiusBlocks;
    public final int radiusChunks;
    public final int sizeChunks;
    private final RayTracingCacheChunk[] chunks;

    public RayTracingCache(int radiusChunks) {
        this.radiusChunks = radiusChunks;
        this.sizeChunks = this.radiusChunks * 2;
        this.radiusBlocks = this.radiusChunks << 4;
        this.chunks = new RayTracingCacheChunk[this.sizeChunks * this.sizeChunks * this.sizeChunks];
        for (int i = 0; i < this.chunks.length; ++i) {
            this.chunks[i] = new RayTracingCacheChunk();
        }
    }

    public int getCachedValue(int x, int y, int z) {
        RayTracingCacheChunk chunk = this.getChunk((x += this.radiusBlocks) >> 4, (y += this.radiusBlocks) >> 4, (z += this.radiusBlocks) >> 4);
        if (chunk == null) {
            return -1;
        }
        return chunk.getCachedValue(x & 0xF, y & 0xF, z & 0xF);
    }

    public void setCachedValue(int x, int y, int z, int value) {
        RayTracingCacheChunk chunk = this.getChunk((x += this.radiusBlocks) >> 4, (y += this.radiusBlocks) >> 4, (z += this.radiusBlocks) >> 4);
        if (chunk == null) {
            return;
        }
        chunk.setCachedValue(x & 0xF, y & 0xF, z & 0xF, value);
    }

    @Nullable
    public RayTracingCacheChunk getChunk(int chunkX, int chunkY, int chunkZ) {
        int index = chunkZ * this.sizeChunks * this.sizeChunks | chunkY * this.sizeChunks | chunkX;
        if (index < 0 || index >= this.chunks.length) {
            return null;
        }
        return this.chunks[index];
    }

    public void clearCache() {
        for (RayTracingCacheChunk chunk : this.chunks) {
            chunk.clearChunk();
        }
    }

    public static class RayTracingCacheChunk {
        private static final int CHUNK_SIZE = 16;
        private int[] cache = new int[256];
        private boolean dirty = false;

        public int getCachedValue(int x, int y, int z) {
            int index = z << 4 | y;
            int offset = (x & 0xF) << 1;
            return this.cache[index] >> offset & 3;
        }

        public void setCachedValue(int x, int y, int z, int value) {
            int index = z << 4 | y;
            int offset = (x & 0xF) << 1;
            this.cache[index] = this.cache[index] & ~(3 << offset) | (value & 3) << offset;
            this.dirty = true;
        }

        public void clearChunk() {
            if (this.dirty) {
                Arrays.fill(this.cache, 0);
                this.dirty = false;
            }
        }
    }
}

