/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.util;

import com.pixelmongenerations.core.config.PixelmonConfig;
import java.util.Timer;
import java.util.TimerTask;
import net.arikia.dev.drpc.DiscordEventHandlers;
import net.arikia.dev.drpc.DiscordRPC;
import net.arikia.dev.drpc.DiscordRichPresence;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.util.Util;

public class PixelmonDRPC {
    private static RPCServerInfo serverInfo;
    private static String playerName;
    private static Timer discordTaskTimer;
    private static boolean hasDiscord;

    public static void setup() {
        if (Util.getOSType() == Util.EnumOS.WINDOWS) {
            discordTaskTimer = new Timer();
            discordTaskTimer.schedule((TimerTask)new DiscordTask(), 0L, 3000L);
            playerName = Minecraft.getMinecraft().getSession().getUsername();
            DiscordEventHandlers handlers = new DiscordEventHandlers.Builder().setReadyEventHandler(user -> {
                hasDiscord = true;
                PixelmonDRPC.sendLoadingRPC();
            }).build();
            DiscordRPC.discordInitialize("808398272078675989", handlers, false);
            DiscordRPC.discordRegister("808398272078675989", "");
        }
    }

    public static void setServerInfo(String displayName, String iconName) {
        if (Util.getOSType() == Util.EnumOS.WINDOWS && PixelmonConfig.enableRichPresence) {
            serverInfo = new RPCServerInfo(displayName, iconName);
        }
    }

    public static void sendLoadingRPC() {
        if (Util.getOSType() == Util.EnumOS.WINDOWS && PixelmonConfig.enableRichPresence) {
            DiscordRichPresence rich = new DiscordRichPresence.Builder("").setDetails("Loading Game").setSmallImage("player", playerName).setBigImage("pglogo", "https://pixelmongenerations.com").build();
            DiscordRPC.discordUpdatePresence(rich);
        }
    }

    public static void sendMainMenuRPC() {
        if (Util.getOSType() == Util.EnumOS.WINDOWS && PixelmonConfig.enableRichPresence) {
            DiscordRichPresence rich = new DiscordRichPresence.Builder("").setDetails("At Main Menu").setSmallImage("player", playerName).setBigImage("pglogo", "https://pixelmongenerations.com").build();
            DiscordRPC.discordUpdatePresence(rich);
        }
    }

    public static void sendSingleplayerRPC() {
        if (Util.getOSType() == Util.EnumOS.WINDOWS && PixelmonConfig.enableRichPresence) {
            DiscordRichPresence rich = new DiscordRichPresence.Builder("").setDetails("Playing Singleplayer").setSmallImage("player", playerName).setBigImage("pglogo", "https://pixelmongenerations.com").build();
            DiscordRPC.discordUpdatePresence(rich);
        }
    }

    public static void sendConnectingRPC() {
        if (Util.getOSType() == Util.EnumOS.WINDOWS && PixelmonConfig.enableRichPresence) {
            DiscordRichPresence rich = new DiscordRichPresence.Builder("").setDetails("Connecting To Server").setSmallImage("player", playerName).setBigImage("pglogo", "https://pixelmongenerations.com").build();
            DiscordRPC.discordUpdatePresence(rich);
        }
    }

    public static void sendServerRPC(int current, int max) {
        if (Util.getOSType() == Util.EnumOS.WINDOWS && PixelmonConfig.enableRichPresence) {
            DiscordRichPresence rich = new DiscordRichPresence.Builder("Player Count").setParty("ae488379-351d-4a4f-ad32-2b9b01c91657", current, max).setDetails("Playing " + serverInfo.getServerName()).setSmallImage("player", playerName).setBigImage(serverInfo.getServerIcon(), "https://pixelmongenerations.com").build();
            DiscordRPC.discordUpdatePresence(rich);
        }
    }

    static {
        hasDiscord = false;
    }

    private static class RPCServerInfo {
        private String serverDisplayName;
        private String serverIconName;

        public RPCServerInfo(String serverDisplayName, String serverIconName) {
            this.serverDisplayName = serverDisplayName;
            this.serverIconName = serverIconName;
        }

        public String getServerName() {
            return this.serverDisplayName;
        }

        public String getServerIcon() {
            return this.serverIconName;
        }
    }

    private static class DiscordTask
    extends TimerTask {
        private DiscordTask() {
        }

        @Override
        public void run() {
            DiscordRPC.discordRunCallbacks();
            try {
                if (Minecraft.getMinecraft().player != null) {
                    if (Minecraft.getMinecraft().player.connection != null && serverInfo != null) {
                        NetHandlerPlayClient netHandler = Minecraft.getMinecraft().player.connection;
                        if (netHandler.getNetworkManager().getRemoteAddress().toString().contains("local")) {
                            PixelmonDRPC.sendSingleplayerRPC();
                        } else {
                            PixelmonDRPC.sendServerRPC(netHandler.getPlayerInfoMap().size(), netHandler.currentServerMaxPlayers);
                        }
                    }
                } else if (Minecraft.getMinecraft().currentScreen instanceof GuiMultiplayer) {
                    PixelmonDRPC.sendMainMenuRPC();
                    serverInfo = null;
                }
            }
            catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }
}

