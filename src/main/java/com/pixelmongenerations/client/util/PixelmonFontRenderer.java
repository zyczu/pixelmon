/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.util;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.settings.GameSettings;

public class PixelmonFontRenderer
extends FontRenderer {
    private FontRenderer fontRenderer;

    public PixelmonFontRenderer(GameSettings gameSettingsIn, FontRenderer fontRenderer, TextureManager textureManagerIn) {
        super(gameSettingsIn, fontRenderer.locationFontTexture, textureManagerIn, fontRenderer.getUnicodeFlag());
        this.fontRenderer = fontRenderer;
    }

    public void renderStringAtPos(String text, boolean shadow) {
        for (int i = 0; i < text.length(); ++i) {
            boolean flag;
            char c0 = text.charAt(i);
            if (c0 == '\u00a7' && i + 1 < text.length()) {
                if (i + 7 < text.length() && text.charAt(i + 1) == '#') {
                    String colorCode = text.substring(i + 1, i + 8);
                    boolean valid = PixelmonFontRenderer.isValid(colorCode);
                    if (valid) {
                        Color color = Color.decode(colorCode);
                        if (shadow) {
                            float shadowDivider = 4.0f;
                            float r = (float)color.getRed() / shadowDivider;
                            float g = (float)color.getGreen() / shadowDivider;
                            float b = (float)color.getBlue() / shadowDivider;
                            color = new Color((int)r, (int)g, (int)b);
                        }
                        this.setColor((float)color.getRed() / 255.0f, (float)color.getGreen() / 255.0f, (float)color.getBlue() / 255.0f, this.alpha);
                        this.textColor = color.getRGB();
                        i += 6;
                    }
                } else {
                    int i1 = "0123456789abcdefklmnor".indexOf(String.valueOf(text.charAt(i + 1)).toLowerCase(Locale.ROOT).charAt(0));
                    if (i1 < 16) {
                        int j1;
                        this.randomStyle = false;
                        this.boldStyle = false;
                        this.strikethroughStyle = false;
                        this.underlineStyle = false;
                        this.italicStyle = false;
                        if (i1 < 0) {
                            i1 = 15;
                        }
                        if (shadow) {
                            i1 += 16;
                        }
                        this.textColor = j1 = this.colorCode[i1];
                        this.setColor((float)(j1 >> 16) / 255.0f, (float)(j1 >> 8 & 0xFF) / 255.0f, (float)(j1 & 0xFF) / 255.0f, this.alpha);
                    } else if (i1 == 16) {
                        this.randomStyle = true;
                    } else if (i1 == 17) {
                        this.boldStyle = true;
                    } else if (i1 == 18) {
                        this.strikethroughStyle = true;
                    } else if (i1 == 19) {
                        this.underlineStyle = true;
                    } else if (i1 == 20) {
                        this.italicStyle = true;
                    } else {
                        this.randomStyle = false;
                        this.boldStyle = false;
                        this.strikethroughStyle = false;
                        this.underlineStyle = false;
                        this.italicStyle = false;
                        this.setColor(this.red, this.blue, this.green, this.alpha);
                    }
                }
                ++i;
                continue;
            }
            int j = "\u00c0\u00c1\u00c2\u00c8\u00ca\u00cb\u00cd\u00d3\u00d4\u00d5\u00da\u00df\u00e3\u00f5\u011f\u0130\u0131\u0152\u0153\u015e\u015f\u0174\u0175\u017e\u0207\u0000\u0000\u0000\u0000\u0000\u0000\u0000 !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\u0000\u00c7\u00fc\u00e9\u00e2\u00e4\u00e0\u00e5\u00e7\u00ea\u00eb\u00e8\u00ef\u00ee\u00ec\u00c4\u00c5\u00c9\u00e6\u00c6\u00f4\u00f6\u00f2\u00fb\u00f9\u00ff\u00d6\u00dc\u00f8\u00a3\u00d8\u00d7\u0192\u00e1\u00ed\u00f3\u00fa\u00f1\u00d1\u00aa\u00ba\u00bf\u00ae\u00ac\u00bd\u00bc\u00a1\u00ab\u00bb\u2591\u2592\u2593\u2502\u2524\u2561\u2562\u2556\u2555\u2563\u2551\u2557\u255d\u255c\u255b\u2510\u2514\u2534\u252c\u251c\u2500\u253c\u255e\u255f\u255a\u2554\u2569\u2566\u2560\u2550\u256c\u2567\u2568\u2564\u2565\u2559\u2558\u2552\u2553\u256b\u256a\u2518\u250c\u2588\u2584\u258c\u2590\u2580\u03b1\u03b2\u0393\u03c0\u03a3\u03c3\u03bc\u03c4\u03a6\u0398\u03a9\u03b4\u221e\u2205\u2208\u2229\u2261\u00b1\u2265\u2264\u2320\u2321\u00f7\u2248\u00b0\u2219\u00b7\u221a\u207f\u00b2\u25a0\u0000".indexOf(c0);
            if (this.randomStyle && j != -1) {
                char c1;
                int k = this.getCharWidth(c0);
                while (k != this.getCharWidth(c1 = "\u00c0\u00c1\u00c2\u00c8\u00ca\u00cb\u00cd\u00d3\u00d4\u00d5\u00da\u00df\u00e3\u00f5\u011f\u0130\u0131\u0152\u0153\u015e\u015f\u0174\u0175\u017e\u0207\u0000\u0000\u0000\u0000\u0000\u0000\u0000 !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\u0000\u00c7\u00fc\u00e9\u00e2\u00e4\u00e0\u00e5\u00e7\u00ea\u00eb\u00e8\u00ef\u00ee\u00ec\u00c4\u00c5\u00c9\u00e6\u00c6\u00f4\u00f6\u00f2\u00fb\u00f9\u00ff\u00d6\u00dc\u00f8\u00a3\u00d8\u00d7\u0192\u00e1\u00ed\u00f3\u00fa\u00f1\u00d1\u00aa\u00ba\u00bf\u00ae\u00ac\u00bd\u00bc\u00a1\u00ab\u00bb\u2591\u2592\u2593\u2502\u2524\u2561\u2562\u2556\u2555\u2563\u2551\u2557\u255d\u255c\u255b\u2510\u2514\u2534\u252c\u251c\u2500\u253c\u255e\u255f\u255a\u2554\u2569\u2566\u2560\u2550\u256c\u2567\u2568\u2564\u2565\u2559\u2558\u2552\u2553\u256b\u256a\u2518\u250c\u2588\u2584\u258c\u2590\u2580\u03b1\u03b2\u0393\u03c0\u03a3\u03c3\u03bc\u03c4\u03a6\u0398\u03a9\u03b4\u221e\u2205\u2208\u2229\u2261\u00b1\u2265\u2264\u2320\u2321\u00f7\u2248\u00b0\u2219\u00b7\u221a\u207f\u00b2\u25a0\u0000".charAt(j = this.fontRandom.nextInt("\u00c0\u00c1\u00c2\u00c8\u00ca\u00cb\u00cd\u00d3\u00d4\u00d5\u00da\u00df\u00e3\u00f5\u011f\u0130\u0131\u0152\u0153\u015e\u015f\u0174\u0175\u017e\u0207\u0000\u0000\u0000\u0000\u0000\u0000\u0000 !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\u0000\u00c7\u00fc\u00e9\u00e2\u00e4\u00e0\u00e5\u00e7\u00ea\u00eb\u00e8\u00ef\u00ee\u00ec\u00c4\u00c5\u00c9\u00e6\u00c6\u00f4\u00f6\u00f2\u00fb\u00f9\u00ff\u00d6\u00dc\u00f8\u00a3\u00d8\u00d7\u0192\u00e1\u00ed\u00f3\u00fa\u00f1\u00d1\u00aa\u00ba\u00bf\u00ae\u00ac\u00bd\u00bc\u00a1\u00ab\u00bb\u2591\u2592\u2593\u2502\u2524\u2561\u2562\u2556\u2555\u2563\u2551\u2557\u255d\u255c\u255b\u2510\u2514\u2534\u252c\u251c\u2500\u253c\u255e\u255f\u255a\u2554\u2569\u2566\u2560\u2550\u256c\u2567\u2568\u2564\u2565\u2559\u2558\u2552\u2553\u256b\u256a\u2518\u250c\u2588\u2584\u258c\u2590\u2580\u03b1\u03b2\u0393\u03c0\u03a3\u03c3\u03bc\u03c4\u03a6\u0398\u03a9\u03b4\u221e\u2205\u2208\u2229\u2261\u00b1\u2265\u2264\u2320\u2321\u00f7\u2248\u00b0\u2219\u00b7\u221a\u207f\u00b2\u25a0\u0000".length())))) {
                }
                c0 = c1;
            }
            float f1 = j == -1 || this.getUnicodeFlag() ? 0.5f : 1.0f;
            boolean bl = flag = (c0 == '\u0000' || j == -1 || this.getUnicodeFlag()) && shadow;
            if (flag) {
                this.posX -= f1;
                this.posY -= f1;
            }
            float f = this.renderChar(c0, this.italicStyle);
            if (flag) {
                this.posX += f1;
                this.posY += f1;
            }
            if (this.boldStyle) {
                this.posX += f1;
                if (flag) {
                    this.posX -= f1;
                    this.posY -= f1;
                }
                this.renderChar(c0, this.italicStyle);
                this.posX -= f1;
                if (flag) {
                    this.posX += f1;
                    this.posY += f1;
                }
                f += 1.0f;
            }
            this.doDraw(f);
        }
    }

    private static boolean isValid(String hex) {
        for (char colorCodeChar : hex.split("#")[1].toCharArray()) {
            if ("0123456789abcdefABCDEF".indexOf(colorCodeChar) != -1) continue;
            return false;
        }
        return true;
    }

    @Override
    public int getStringWidth(String text) {
        int i = 0;
        boolean flag = false;
        for (int j = 0; j < text.length(); ++j) {
            int k;
            char c0 = text.charAt(j);
            if (j + 1 != text.length()) {
                k = this.getCharWidth(c0, text.charAt(j + 1));
                if (k == -7) {
                    j += 7;
                    continue;
                }
            } else {
                k = this.getCharWidth(c0);
            }
            if (k < 0 && j < text.length() - 1) {
                if ((c0 = text.charAt(++j)) != 'l' && c0 != 'L') {
                    if (c0 == 'r' || c0 == 'R') {
                        flag = false;
                    }
                } else {
                    flag = true;
                }
                k = 0;
            }
            i += k;
            if (!flag || k <= 0) continue;
            ++i;
        }
        return i;
    }

    @Override
    public String trimStringToWidth(String text, int width, boolean reverse) {
        StringBuilder stringbuilder = new StringBuilder();
        int i = 0;
        int j = reverse ? text.length() - 1 : 0;
        int k = reverse ? -1 : 1;
        boolean flag = false;
        boolean flag1 = false;
        for (int l = j; l >= 0 && l < text.length() && i < width; l += k) {
            int i1;
            char c0 = text.charAt(l);
            int n = i1 = l + 1 < text.length() ? this.getCharWidth(c0, text.charAt(l + 1)) : this.getCharWidth(c0);
            if (i1 == -7) {
                i -= 35;
            }
            if (flag) {
                flag = false;
                if (c0 != 'l' && c0 != 'L') {
                    if (c0 == 'r' || c0 == 'R') {
                        flag1 = false;
                    }
                } else {
                    flag1 = true;
                }
            } else if (i1 < 0) {
                flag = true;
            } else {
                i += i1;
                if (flag1) {
                    ++i;
                }
            }
            if (i > width) break;
            if (reverse) {
                stringbuilder.insert(0, c0);
                continue;
            }
            stringbuilder.append(c0);
        }
        return stringbuilder.toString();
    }

    public int getCharWidth(char character, char nextCharacter) {
        if (character == '\u00a0') {
            return 4;
        }
        if (character == '\u00a7') {
            if (nextCharacter == '#') {
                return -7;
            }
            return -1;
        }
        if (character == ' ') {
            return 4;
        }
        int i = "\u00c0\u00c1\u00c2\u00c8\u00ca\u00cb\u00cd\u00d3\u00d4\u00d5\u00da\u00df\u00e3\u00f5\u011f\u0130\u0131\u0152\u0153\u015e\u015f\u0174\u0175\u017e\u0207\u0000\u0000\u0000\u0000\u0000\u0000\u0000 !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\u0000\u00c7\u00fc\u00e9\u00e2\u00e4\u00e0\u00e5\u00e7\u00ea\u00eb\u00e8\u00ef\u00ee\u00ec\u00c4\u00c5\u00c9\u00e6\u00c6\u00f4\u00f6\u00f2\u00fb\u00f9\u00ff\u00d6\u00dc\u00f8\u00a3\u00d8\u00d7\u0192\u00e1\u00ed\u00f3\u00fa\u00f1\u00d1\u00aa\u00ba\u00bf\u00ae\u00ac\u00bd\u00bc\u00a1\u00ab\u00bb\u2591\u2592\u2593\u2502\u2524\u2561\u2562\u2556\u2555\u2563\u2551\u2557\u255d\u255c\u255b\u2510\u2514\u2534\u252c\u251c\u2500\u253c\u255e\u255f\u255a\u2554\u2569\u2566\u2560\u2550\u256c\u2567\u2568\u2564\u2565\u2559\u2558\u2552\u2553\u256b\u256a\u2518\u250c\u2588\u2584\u258c\u2590\u2580\u03b1\u03b2\u0393\u03c0\u03a3\u03c3\u03bc\u03c4\u03a6\u0398\u03a9\u03b4\u221e\u2205\u2208\u2229\u2261\u00b1\u2265\u2264\u2320\u2321\u00f7\u2248\u00b0\u2219\u00b7\u221a\u207f\u00b2\u25a0\u0000".indexOf(character);
        if (character > '\u0000' && i != -1 && !this.getUnicodeFlag()) {
            return this.charWidth[i];
        }
        if (this.glyphWidth[character] != 0) {
            int j = this.glyphWidth[character] & 0xFF;
            int k = j >>> 4;
            int l = j & 0xF;
            return (++l - k) / 2 + 1;
        }
        return 0;
    }

    @Override
    public List<String> listFormattedStringToWidth(String str, int wrapWidth) {
        return Arrays.asList(this.wrapFormattedStringToWidthPlus(str, wrapWidth).split("\n"));
    }

    private String wrapFormattedStringToWidthPlus(String str, int wrapWidth) {
        int i = this.sizeStringToWidth(str, wrapWidth);
        if (str.length() <= i) {
            return str;
        }
        String s = str.substring(0, i);
        char c0 = str.charAt(i);
        boolean flag = c0 == ' ' || c0 == '\n';
        String s1 = PixelmonFontRenderer.getFormatsFromString(s) + str.substring(i + (flag ? 1 : 0));
        return s + "\n" + this.wrapFormattedStringToWidthPlus(s1, wrapWidth);
    }

    public static String getFormatsFromString(String text) {
        StringBuilder s = new StringBuilder();
        int i = -1;
        int j = text.length();
        while ((i = text.indexOf(167, i + 1)) != -1) {
            String colorCode;
            boolean valid;
            if (i >= j - 1) continue;
            char c0 = text.charAt(i + 1);
            if (PixelmonFontRenderer.isFormatColor(c0)) {
                s = new StringBuilder("\u00a7" + c0);
                continue;
            }
            if (PixelmonFontRenderer.isFormatSpecial(c0)) {
                s.append("\u00a7").append(c0);
                continue;
            }
            if (c0 != '#' || i + 7 >= text.length() || text.charAt(i + 1) != '#' || !(valid = PixelmonFontRenderer.isValid(colorCode = text.substring(i + 1, i + 8)))) continue;
            s = new StringBuilder('\u00a7' + colorCode);
            i += 7;
        }
        return s.toString();
    }

    public int sizeStringToWidth(String str, int wrapWidth) {
        int k;
        int i = str.length();
        int j = 0;
        int l = -1;
        boolean flag = false;
        for (k = 0; k < i; ++k) {
            char c0 = str.charAt(k);
            switch (c0) {
                case '\n': {
                    --k;
                    break;
                }
                case ' ': {
                    l = k;
                }
                default: {
                    j += this.getCharWidth(c0);
                    if (!flag) break;
                    ++j;
                    break;
                }
                case '\u00a7': {
                    char c1;
                    if (k >= i - 1) break;
                    if ((c1 = str.charAt(++k)) != 'l' && c1 != 'L') {
                        if (c1 != 'r' && c1 != 'R' && !PixelmonFontRenderer.isFormatColor(c1)) break;
                        flag = false;
                        break;
                    }
                    flag = true;
                }
            }
            if (c0 == '\n') {
                l = ++k;
                break;
            }
            if (j > wrapWidth) break;
        }
        return k != i && l != -1 && l < k ? l : k;
    }

    private static boolean isFormatColor(char colorChar) {
        return colorChar >= '0' && colorChar <= '9' || colorChar >= 'a' && colorChar <= 'f' || colorChar >= 'A' && colorChar <= 'F';
    }

    private static boolean isFormatSpecial(char formatChar) {
        return formatChar >= 'k' && formatChar <= 'o' || formatChar >= 'K' && formatChar <= 'O' || formatChar == 'r' || formatChar == 'R';
    }

    public static void setupFontRenderer() {
        Minecraft mc = Minecraft.getMinecraft();
        mc.fontRenderer = new PixelmonFontRenderer(mc.gameSettings, mc.fontRenderer, mc.getTextureManager());
        if (mc.gameSettings.language != null) {
            mc.fontRenderer.setUnicodeFlag(mc.isUnicode());
            mc.fontRenderer.setBidiFlag(mc.getLanguageManager().isCurrentLanguageBidirectional());
        }
        mc.fontRenderer = new PixelmonFontRenderer(mc.gameSettings, mc.fontRenderer, mc.getTextureManager());
        mc.resourceManager.registerReloadListener(mc.fontRenderer);
    }
}

