/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.util;

import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.awt.image.BufferedImage;
import java.util.function.Function;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;

public class TextureAtlasDynamic
extends TextureAtlasSprite {
    public TextureAtlasDynamic(ResourceLocation resource) {
        super(resource.toString());
        try {
            BufferedImage bufferedimage = TextureUtil.readBufferedImage(Pixelmon.PROXY.getStreamForResourceLocation(resource));
            int[][] aint = new int[][]{new int[bufferedimage.getWidth() * bufferedimage.getHeight()]};
            bufferedimage.getRGB(0, 0, bufferedimage.getWidth(), bufferedimage.getHeight(), aint[0], 0, bufferedimage.getWidth());
            this.framesTextureData.clear();
            this.frameCounter = 0;
            this.tickCounter = 0;
            this.framesTextureData.add(aint);
            this.width = bufferedimage.getWidth();
            this.height = bufferedimage.getHeight();
            this.initSprite(this.getIconWidth(), this.getIconHeight(), 0, 0, false);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAnimation() {
        TextureResource resource = ClientProxy.TEXTURE_STORE.getObject(this.getIconName());
        if (resource != null) {
            resource.bindTexture();
        }
    }

    @Override
    public boolean hasCustomLoader(IResourceManager manager, ResourceLocation location) {
        return true;
    }

    @Override
    public boolean load(IResourceManager manager, ResourceLocation location, Function<ResourceLocation, TextureAtlasSprite> textureGetter) {
        return false;
    }
}

