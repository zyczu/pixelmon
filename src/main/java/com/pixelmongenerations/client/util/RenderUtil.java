/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.util;

import com.pixelmongenerations.api.board.DisplayBoard;
import com.pixelmongenerations.api.world.BoxZone;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.awt.Color;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import org.lwjgl.opengl.GL11;

public class RenderUtil {
    public static void addQuad(List<BakedQuad> quadList, TextureAtlasSprite texture, float xEnd, float yEnd, float zEnd, float uStart, float vStart, float uEnd, float vEnd) {
        int x = Float.floatToRawIntBits(xEnd);
        int y = Float.floatToRawIntBits(yEnd);
        int z = Float.floatToRawIntBits(zEnd);
        int us = Float.floatToRawIntBits(texture.getInterpolatedU(uStart));
        int ue = Float.floatToRawIntBits(texture.getInterpolatedU(uEnd));
        int vs = Float.floatToIntBits(texture.getInterpolatedV(vStart));
        int ve = Float.floatToIntBits(texture.getInterpolatedV(vEnd));
        int white = Color.white.getRGB();
        BakedQuad orbQuad = new BakedQuad(new int[]{x, 0, z, white, ue, ve, 0, x, y, z, white, ue, vs, 0, 0, y, z, white, us, vs, 0, 0, 0, z, white, us, ve, 0}, 0, EnumFacing.SOUTH, texture, false, Tessellator.getInstance().getBuffer().getVertexFormat());
        quadList.add(orbQuad);
        int zn = Float.floatToRawIntBits(-zEnd);
        orbQuad = new BakedQuad(new int[]{0, 0, zn, white, ue, ve, 0, 0, y, zn, white, ue, vs, 0, x, y, zn, white, us, vs, 0, x, 0, zn, white, us, ve, 0}, 0, EnumFacing.NORTH, texture, false, Tessellator.getInstance().getBuffer().getVertexFormat());
        quadList.add(orbQuad);
    }

    public static void drawBoxZone(BoxZone zone) {
        Color c = zone.getColor();
        Vec3d pos1 = zone.getMin();
        Vec3d pos2 = zone.getMax();
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        GL11.glPushAttrib((int)8192);
        GL11.glDisable((int)2884);
        GL11.glDisable((int)2896);
        GL11.glDisable((int)3553);
        GlStateManager.glLineWidth(4.0f);
        GL11.glDepthMask((boolean)false);
        buffer.begin(3, DefaultVertexFormats.POSITION_COLOR);
        buffer.pos(pos1.x, pos1.y, pos1.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos1.x, pos1.y, pos1.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos2.x, pos1.y, pos1.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos2.x, pos1.y, pos2.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos1.x, pos1.y, pos2.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos1.x, pos1.y, pos1.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos1.x, pos2.y, pos1.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos2.x, pos2.y, pos1.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos2.x, pos2.y, pos2.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos1.x, pos2.y, pos2.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos1.x, pos2.y, pos1.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos1.x, pos2.y, pos2.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos1.x, pos1.y, pos2.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos2.x, pos1.y, pos2.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos2.x, pos2.y, pos2.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos2.x, pos2.y, pos1.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos2.x, pos1.y, pos1.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        buffer.pos(pos2.x, pos1.y, pos1.z).color(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha()).endVertex();
        tessellator.draw();
        GL11.glDepthMask((boolean)true);
        GL11.glPopAttrib();
        GlStateManager.glLineWidth(0.2f);
    }

    public static void drawPositionedTextInViewScaled(String par1, double par2, double par3, double par4, float par5, double scale, int givenColor) {
        Minecraft mc = Minecraft.getMinecraft();
        EntityPlayerSP clientPlayer = mc.player;
        GL11.glPushMatrix();
        double d0 = clientPlayer.lastTickPosX + (clientPlayer.posX - clientPlayer.lastTickPosX) * (double)par5;
        double d1 = clientPlayer.lastTickPosY + (clientPlayer.posY - clientPlayer.lastTickPosY) * (double)par5;
        double d2 = clientPlayer.lastTickPosZ + (clientPlayer.posZ - clientPlayer.lastTickPosZ) * (double)par5;
        GL11.glTranslated((double)par2, (double)par3, (double)par4);
        GL11.glTranslated((double)(-d0), (double)(-d1), (double)(-d2));
        GL11.glNormal3f((float)0.0f, (float)1.0f, (float)0.0f);
        GL11.glColor4f((float)1.0f, (float)1.0f, (float)1.0f, (float)1.0f);
        GL11.glRotatef((float)(-clientPlayer.rotationYaw), (float)0.0f, (float)1.0f, (float)0.0f);
        GL11.glRotatef((float)clientPlayer.rotationPitch, (float)1.0f, (float)0.0f, (float)0.0f);
        GL11.glScalef((float)-0.03f, (float)-0.03f, (float)0.03f);
        GL11.glDisable((int)2896);
        GL11.glDepthMask((boolean)false);
        GL11.glDisable((int)2929);
        GL11.glEnable((int)3042);
        GL11.glBlendFunc((int)770, (int)771);
        RenderUtil.renderColor(0xFFFFFF);
        RenderUtil.renderCenteredTextScaledWithOutline(par1, 0, 0, scale, givenColor);
        GL11.glDepthMask((boolean)true);
        GL11.glEnable((int)2929);
        GL11.glEnable((int)2896);
        GL11.glDisable((int)3042);
        GL11.glPopMatrix();
    }

    public static void renderColor(int par1) {
        Color color = Color.decode("" + par1);
        float red = (float)color.getRed() / 255.0f;
        float green = (float)color.getGreen() / 255.0f;
        float blue = (float)color.getBlue() / 255.0f;
        GL11.glColor3f((float)red, (float)green, (float)blue);
    }

    public static void renderCenteredTextScaledWithOutline(String text, int posX, int posY, double par4, int givenColor) {
        Minecraft mc = Minecraft.getMinecraft();
        double width = (double)(mc.fontRenderer.getStringWidth(text) / 2) * par4;
        GL11.glPushMatrix();
        GL11.glTranslated((double)((double)posX - width), (double)posY, (double)0.0);
        GL11.glScaled((double)par4, (double)par4, (double)par4);
        mc.fontRenderer.drawString((Object)((Object)TextFormatting.BLACK) + text, -1, -1, 0);
        mc.fontRenderer.drawString((Object)((Object)TextFormatting.BLACK) + text, 1, -1, 0);
        mc.fontRenderer.drawString((Object)((Object)TextFormatting.BLACK) + text, -1, 1, 0);
        mc.fontRenderer.drawString((Object)((Object)TextFormatting.BLACK) + text, 1, 1, 0);
        mc.fontRenderer.drawString((Object)((Object)TextFormatting.BLACK) + text, 0, -1, 0);
        mc.fontRenderer.drawString((Object)((Object)TextFormatting.BLACK) + text, -1, 0, 0);
        mc.fontRenderer.drawString((Object)((Object)TextFormatting.BLACK) + text, 1, 0, 0);
        mc.fontRenderer.drawString((Object)((Object)TextFormatting.BLACK) + text, 0, 1, 0);
        mc.fontRenderer.drawString(text, 0, 0, givenColor);
        GL11.glPopMatrix();
    }

    public static void drawPositionedImageInView(ResourceLocation par1, double x, double y, double z, float partialTicks, float width, float height, boolean rotateTowards) {
        Minecraft mc = Minecraft.getMinecraft();
        EntityPlayerSP clientPlayer = mc.player;
        GL11.glPushMatrix();
        double d0 = clientPlayer.lastTickPosX + (clientPlayer.posX - clientPlayer.lastTickPosX) * (double)partialTicks;
        double d1 = clientPlayer.lastTickPosY + (clientPlayer.posY - clientPlayer.lastTickPosY) * (double)partialTicks;
        double d2 = clientPlayer.lastTickPosZ + (clientPlayer.posZ - clientPlayer.lastTickPosZ) * (double)partialTicks;
        GL11.glTranslated((double)x, (double)y, (double)z);
        GL11.glTranslated((double)(-d0), (double)(-d1), (double)(-d2));
        GL11.glNormal3f((float)0.0f, (float)1.0f, (float)0.0f);
        GL11.glColor4f((float)1.0f, (float)1.0f, (float)1.0f, (float)1.0f);
        if (rotateTowards) {
            GL11.glRotatef((float)(-clientPlayer.rotationYaw), (float)0.0f, (float)1.0f, (float)0.0f);
            GL11.glRotatef((float)clientPlayer.rotationPitch, (float)1.0f, (float)0.0f, (float)0.0f);
        }
        GL11.glScalef((float)-0.03f, (float)-0.03f, (float)0.03f);
        GL11.glDisable((int)2896);
        GL11.glDepthMask((boolean)false);
        GL11.glDisable((int)2929);
        GL11.glEnable((int)3042);
        GL11.glBlendFunc((int)770, (int)771);
        RenderUtil.renderColor(0xFFFFFF);
        RenderUtil.drawImage((double)(-width / 2.0f), (double)(-height / 2.0f), par1, (double)width, (double)height);
        GL11.glDepthMask((boolean)true);
        GL11.glEnable((int)2929);
        GL11.glEnable((int)2896);
        GL11.glDisable((int)3042);
        GL11.glPopMatrix();
    }

    public static void drawBoard(DisplayBoard board, float partialTicks, Vec3d eyePos, Vec3d eyePosForward) {
        String textureId;
        if (board.hoverTextureId() != null) {
            Vec3d pos2;
            double widthHalf = board.width() / 2.0f;
            double heightHalf = board.height() / 2.0f;
            Vec3d pos1 = new Vec3d(board.position()[0] - widthHalf * (double)-0.03f, board.position()[1] - heightHalf * (double)-0.03f, board.position()[2]);
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(pos1, pos2 = new Vec3d(board.position()[0] + widthHalf * (double)-0.03f, board.position()[1] + heightHalf * (double)-0.03f, board.position()[2])).expand(0.02, 0.02, 0.02);
            textureId = axisalignedbb.calculateIntercept(eyePos, eyePosForward) != null ? board.hoverTextureId() : board.textureId();
        } else {
            textureId = board.textureId();
        }
        if (ClientProxy.TEXTURE_STORE.getObject(textureId) != null) {
            EntityPlayerSP clientPlayer = Minecraft.getMinecraft().player;
            GL11.glPushMatrix();
            double d0 = clientPlayer.lastTickPosX + (clientPlayer.posX - clientPlayer.lastTickPosX) * (double)partialTicks;
            double d1 = clientPlayer.lastTickPosY + (clientPlayer.posY - clientPlayer.lastTickPosY) * (double)partialTicks;
            double d2 = clientPlayer.lastTickPosZ + (clientPlayer.posZ - clientPlayer.lastTickPosZ) * (double)partialTicks;
            GL11.glTranslated((double)board.position()[0], (double)board.position()[1], (double)board.position()[2]);
            GL11.glTranslated((double)(-d0), (double)(-d1), (double)(-d2));
            GL11.glNormal3f((float)0.0f, (float)1.0f, (float)0.0f);
            GL11.glColor4f((float)1.0f, (float)1.0f, (float)1.0f, (float)1.0f);
            if (board.facePlayer()) {
                GL11.glRotatef((float)(-clientPlayer.rotationYaw), (float)0.0f, (float)1.0f, (float)0.0f);
                GL11.glRotatef((float)clientPlayer.rotationPitch, (float)1.0f, (float)0.0f, (float)0.0f);
            } else if (board.rotation() != null) {
                GL11.glRotatef((float)board.rotation()[0], (float)board.rotation()[1], (float)board.rotation()[2], (float)board.rotation()[3]);
            }
            GL11.glScalef((float)-0.03f, (float)-0.03f, (float)0.03f);
            RenderUtil.renderColor(0xFFFFFF);
            RenderUtil.drawImage((double)(-board.width() / 2.0f), (double)(-board.height() / 2.0f), textureId, (double)board.width(), (double)board.height());
            GL11.glPopMatrix();
        }
    }

    public static void drawImage(double x, double y, ResourceLocation image, double width, double height) {
        Minecraft.getMinecraft().renderEngine.bindTexture(image);
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
        bufferbuilder.pos(x, y + height, 0.0).tex(0.0, 1.0).endVertex();
        bufferbuilder.pos(x + width, y + height, 0.0).tex(1.0, 1.0).endVertex();
        bufferbuilder.pos(x + width, y + 0.0, 0.0).tex(1.0, 0.0).endVertex();
        bufferbuilder.pos(x, y, 0.0).tex(0.0, 0.0).endVertex();
        tessellator.draw();
    }

    public static void drawImage(double x, double y, String textureId, double width, double height) {
        ClientProxy.TEXTURE_STORE.getObject(textureId).bindTexture();
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
        bufferbuilder.pos(x, y + height, 0.0).tex(0.0, 1.0).endVertex();
        bufferbuilder.pos(x + width, y + height, 0.0).tex(1.0, 1.0).endVertex();
        bufferbuilder.pos(x + width, y + 0.0, 0.0).tex(1.0, 0.0).endVertex();
        bufferbuilder.pos(x, y, 0.0).tex(0.0, 0.0).endVertex();
        tessellator.draw();
    }
}

