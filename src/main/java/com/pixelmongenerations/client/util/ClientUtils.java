/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.util;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

public class ClientUtils {
    public static void glColorHex(int color) {
        float alpha = (float)(color >> 24 & 0xFF) / 255.0f;
        float red = (float)(color >> 16 & 0xFF) / 255.0f;
        float green = (float)(color >> 8 & 0xFF) / 255.0f;
        float blue = (float)(color & 0xFF) / 255.0f;
        GlStateManager.color(red, green, blue, alpha);
    }

    public static void glColorHex(int color, int alpha) {
        ClientUtils.glColorHex(color | alpha << 24);
    }

    public static void drawImageQuad(double x, double y, double w, float h, double us, double vs, double ue, double ve, float zLevel) {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        buffer.begin(7, DefaultVertexFormats.POSITION_TEX);
        buffer.pos(x, y + (double)h, zLevel).tex(us, ve).endVertex();
        buffer.pos(x + w, y + (double)h, zLevel).tex(ue, ve).endVertex();
        buffer.pos(x + w, y, zLevel).tex(ue, vs).endVertex();
        buffer.pos(x, y, zLevel).tex(us, vs).endVertex();
        tessellator.draw();
    }

    public static void drawTexturedModalRect(int x, int y, int textureX, int textureY, int width, int height, double zLevel) {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
        bufferbuilder.pos(x, y + height, zLevel).tex((float)textureX * 0.00390625f, (float)(textureY + height) * 0.00390625f).endVertex();
        bufferbuilder.pos(x + width, y + height, zLevel).tex((float)(textureX + width) * 0.00390625f, (float)(textureY + height) * 0.00390625f).endVertex();
        bufferbuilder.pos(x + width, y, zLevel).tex((float)(textureX + width) * 0.00390625f, (float)textureY * 0.00390625f).endVertex();
        bufferbuilder.pos(x, y, zLevel).tex((float)textureX * 0.00390625f, (float)textureY * 0.00390625f).endVertex();
        tessellator.draw();
    }
}

