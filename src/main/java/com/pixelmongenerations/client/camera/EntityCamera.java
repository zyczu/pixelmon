/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.camera;

import com.pixelmongenerations.client.camera.CameraMode;
import com.pixelmongenerations.client.camera.ICameraTarget;
import com.pixelmongenerations.client.camera.movement.CameraMovement;
import com.pixelmongenerations.client.camera.movement.PlayerControlledMovement;
import com.pixelmongenerations.client.camera.movement.PositionedMovement;
import com.pixelmongenerations.client.gui.GuiEvolve;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.proxy.ClientProxy;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import org.jetbrains.annotations.NotNull;

public class EntityCamera
extends EntityLiving {
    private ICameraTarget target;
    public CameraMode mode;
    private CameraMovement movement;

    public EntityCamera(World par1World) {
        this(par1World, CameraMode.Battle);
    }

    public EntityCamera(World par1World, CameraMode mode) {
        super(par1World);
        this.height = 0.0f;
        this.width = 0.0f;
        this.mode = mode;
        this.movement = PixelmonConfig.playerControlCamera ? new PlayerControlledMovement(par1World, this) : new PositionedMovement(par1World, this);
    }

    @Override
    protected void entityInit() {
        super.entityInit();
    }

    @Override
    public void onEntityUpdate() {
        this.prevMovedDistance = this.movedDistance;
        this.prevRenderYawOffset = this.renderYawOffset;
        this.prevRotationYawHead = this.rotationYawHead;
        this.prevRotationYaw = this.rotationYaw;
        this.prevRotationPitch = this.rotationPitch;
    }

    private float updateRotation(float par1, float par2, float par3) {
        float f3 = MathHelper.wrapDegrees(par2 - par1);
        if (f3 > par3) {
            f3 = par3;
        }
        if (f3 < -par3) {
            f3 = -par3;
        }
        return par1 + f3;
    }

    public void setTarget(ICameraTarget t) {
        this.target = t;
    }

    public void setTargetRandomPosition(ICameraTarget t) {
        this.setTarget(t);
        this.getMovement().setRandomPosition(t);
    }

    public ICameraTarget getTarget() {
        return this.target;
    }

    public void updatePositionAndRotation() {
        if (Minecraft.getMinecraft().getRenderViewEntity() == this && this.target != null && this.target.isValidTarget()) {
            double vecX = this.target.getX() - this.posX;
            double vecY = this.target.getY() + this.target.getYSeeOffset() - 0.5 - this.posY;
            double vecZ = this.target.getZ() - this.posZ;
            double length = MathHelper.sqrt(vecX * vecX + vecZ * vecZ);
            float f2 = (float)(Math.atan2(vecZ, vecX) * 180.0 / Math.PI) - 90.0f;
            float f3 = (float)(-Math.atan2(vecY, length) * 180.0 / Math.PI);
            this.rotationPitch = this.updateRotation(this.rotationPitch, f3, 50.0f);
            this.rotationYaw = this.updateRotation(this.rotationYaw, f2, 50.0f);
            this.movement.updatePositionAndRotation();
        }
    }

    @Override
    public void onLivingUpdate() {
        if (!this.isDead && (this.mode == CameraMode.Battle && ClientProxy.battleManager.battleEnded || this.mode == CameraMode.Evolution && !(Minecraft.getMinecraft().currentScreen instanceof GuiEvolve))) {
            this.setDead();
            return;
        }
        this.updatePositionAndRotation();
        if (!this.isDead) {
            this.getMovement().onLivingUpdate();
        }
    }

    @Override
    public boolean isInsideOfMaterial(@NotNull Material materialIn) {
        return false;
    }

    @Override
    public float getEyeHeight() {
        Pixelmon.PROXY.resetMouseOver();
        return super.getEyeHeight();
    }

    public CameraMovement getMovement() {
        return this.movement;
    }
}

