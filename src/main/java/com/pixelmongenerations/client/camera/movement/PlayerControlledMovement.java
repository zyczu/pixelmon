/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 *  org.lwjgl.util.vector.Matrix3f
 */
package com.pixelmongenerations.client.camera.movement;

import com.pixelmongenerations.client.camera.CameraMode;
import com.pixelmongenerations.client.camera.CameraTargetLocation;
import com.pixelmongenerations.client.camera.EntityCamera;
import com.pixelmongenerations.client.camera.ICameraTarget;
import com.pixelmongenerations.client.camera.movement.CameraMovement;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Matrix3f;

public class PlayerControlledMovement
extends CameraMovement {
    private boolean initSphere = false;
    private Vec3d centerPos;
    private Vec3d baseVec;
    private float radius = 0.0f;
    private float pitch = 0.0f;
    private float yaw = 0.0f;
    private boolean spherePosChanged = false;
    private ICameraTarget target;

    public PlayerControlledMovement(World world, EntityCamera entityCamera) {
        super(world, entityCamera);
    }

    @Override
    public void setRandomPosition(ICameraTarget t) {
        this.target = t;
    }

    @Override
    public void onLivingUpdate() {
        if (this.camera.mode == CameraMode.Evolution) {
            if (!this.initSphere && this.target != null) {
                this.generateSphere();
            }
            this.updatePosition();
        } else {
            EntityPixelmon pix = ClientProxy.battleManager.getUserPokemon(this.camera.mode);
            if (pix == null) {
                return;
            }
            EntityPlayer player = ClientProxy.battleManager.getViewPlayer();
            if (!this.initSphere && pix.posX != player.posX && pix.posZ != player.posZ) {
                this.generateSphere();
            }
            EntityPixelmon opp = this.getOpponentPokemon();
            this.centerPos = new Vec3d(pix.posX, pix.posY, pix.posZ);
            if (opp != null) {
                this.centerPos = new Vec3d((this.centerPos.x + opp.posX) / 2.0, (this.centerPos.y + opp.posY) / 2.0, (this.centerPos.z + opp.posZ) / 2.0);
            }
            Vec3d playerPos = new Vec3d(player.posX, pix.posY + 1.0, player.posZ);
            Vec3d vec = playerPos.subtract(this.centerPos);
            vec = new Vec3d(vec.x, 0.0, vec.z);
            if (vec.x == 0.0 && vec.z == 0.0) {
                vec.add(new Vec3d(1.0, 0.0, 0.0));
            }
            this.camera.setTarget(new CameraTargetLocation((int)this.centerPos.x, (int)this.centerPos.y, (int)this.centerPos.z));
            this.baseVec = vec.normalize();
            this.updatePosition();
        }
    }

    private void updatePosition() {
        if (this.spherePosChanged) {
            Matrix3f mat = this.constructYawMatrix();
            if (this.baseVec == null) {
                return;
            }
            Vec3d newPos = PlayerControlledMovement.multiply(mat, this.baseVec);
            mat = this.constructRotMatrix(newPos.crossProduct(new Vec3d(0.0, 1.0, 0.0)), this.pitch);
            Vec3d rotPos = PlayerControlledMovement.multiply(mat, newPos);
            Vec3d testPos = new Vec3d(this.centerPos.x + rotPos.x * (double)this.radius, this.centerPos.y + rotPos.y * (double)this.radius, this.centerPos.z + rotPos.z * (double)this.radius);
            this.camera.setPosition(testPos.x, testPos.y, testPos.z);
            this.spherePosChanged = false;
            this.camera.updatePositionAndRotation();
        }
    }

    private EntityPixelmon getOpponentPokemon() {
        if (ClientProxy.battleManager == null || ClientProxy.battleManager.displayedEnemyPokemon == null || ClientProxy.battleManager.displayedEnemyPokemon.length == 0) {
            return null;
        }
        int[] id = ClientProxy.battleManager.displayedEnemyPokemon[0].pokemonID;
        if (id != null) {
            Minecraft mc = Minecraft.getMinecraft();
            if (mc.world != null) {
                List<Entity> loadedEntityList = mc.world.loadedEntityList;
                for (Entity o : loadedEntityList) {
                    EntityPixelmon pokemon;
                    if (!(o instanceof EntityPixelmon) || !PixelmonMethods.isIDSame(pokemon = (EntityPixelmon)o, id)) continue;
                    return pokemon;
                }
            }
        }
        return null;
    }

    private Matrix3f constructYawMatrix() {
        return this.constructRotMatrix(new Vec3d(0.0, 1.0, 0.0), this.yaw);
    }

    private Matrix3f constructRotMatrix(Vec3d axis, float rot) {
        Matrix3f mat = new Matrix3f();
        mat.m00 = (float)(Math.cos(rot) + axis.x * axis.x * (1.0 - Math.cos(rot)));
        mat.m01 = (float)(axis.x * axis.y * (1.0 - Math.cos(rot)) - axis.z * Math.sin(rot));
        mat.m02 = (float)(axis.x * axis.z * (1.0 - Math.cos(rot)) + axis.y * Math.sin(rot));
        mat.m10 = (float)(axis.x * axis.y * (1.0 - Math.cos(rot)) + axis.z * Math.sin(rot));
        mat.m11 = (float)(Math.cos(rot) + axis.y * axis.y * (1.0 - Math.cos(rot)));
        mat.m12 = (float)(axis.y * axis.z * (1.0 - Math.cos(rot)) - axis.x * Math.sin(rot));
        mat.m20 = (float)(axis.x * axis.z * (1.0 - Math.cos(rot)) - axis.y * Math.sin(rot));
        mat.m21 = (float)(axis.y * axis.z * (1.0 - Math.cos(rot)) + axis.x * Math.sin(rot));
        mat.m22 = (float)(Math.cos(rot) + axis.z * axis.z * (1.0 - Math.cos(rot)));
        return mat;
    }

    public static Vec3d multiply(Matrix3f matrix, Vec3d vector) {
        double x = (double)matrix.m00 * vector.x + (double)matrix.m01 * vector.y + (double)matrix.m02 * vector.z;
        double y = (double)matrix.m10 * vector.x + (double)matrix.m11 * vector.y + (double)matrix.m12 * vector.z;
        double z = (double)matrix.m20 * vector.x + (double)matrix.m21 * vector.y + (double)matrix.m22 * vector.z;
        return new Vec3d(x, y, z);
    }

    public void generateSphere() {
        if (this.camera.mode == CameraMode.Evolution) {
            this.initSphere = true;
            if (this.target.getTargetData() instanceof EntityPixelmon) {
                EntityPixelmon poke = (EntityPixelmon)this.target.getTargetData();
                BaseStats stats = poke.initializeBaseStatsIfNull();
                float height = 0.0f;
                if (stats != null) {
                    height = stats.height;
                }
                this.centerPos = new Vec3d(this.target.getX(), this.target.getY() + (double)height, this.target.getZ());
                Vec3d lookVec = poke.getLookVec();
                lookVec = new Vec3d(lookVec.x, 0.0, lookVec.z);
                lookVec.normalize();
                this.baseVec = lookVec;
                this.pitch = 0.19634955f;
                this.yaw = 0.0f;
                this.radius = poke.baseStats.width * 4.0f;
                this.radius = Math.max(this.radius, 4.0f);
                this.spherePosChanged = true;
            }
        } else {
            this.initSphere = true;
            EntityPixelmon pix = ClientProxy.battleManager.getUserPokemon(this.camera.mode);
            EntityPlayer player = ClientProxy.battleManager.getViewPlayer();
            EntityPixelmon opp = this.getOpponentPokemon();
            this.centerPos = new Vec3d(pix.posX, pix.posY, pix.posZ);
            if (opp != null) {
                this.centerPos = new Vec3d((this.centerPos.x + opp.posX) / 2.0, (this.centerPos.y + opp.posY) / 2.0, (this.centerPos.z + opp.posZ) / 2.0);
            }
            Vec3d playerPos = new Vec3d(player.posX, pix.posY + 1.0, player.posZ);
            Vec3d vec = playerPos.subtract(this.centerPos);
            vec = new Vec3d(vec.x, 0.0, vec.z);
            if (vec.x == 0.0 && vec.z == 0.0) {
                vec.add(new Vec3d(1.0, 0.0, 0.0));
            }
            this.baseVec = vec.normalize();
            this.pitch = 0.7853982f;
            this.yaw = 0.0f;
            this.radius = (float)playerPos.distanceTo(this.centerPos) + 2.0f;
            this.radius = Math.max(this.radius, 10.0f);
            this.spherePosChanged = true;
        }
        ClientProxy.battleManager.setViewEntity(this.camera);
    }

    @Override
    public void generatePositions() {
    }

    @Override
    public void handleKeyboardInput() {
        if (Keyboard.isKeyDown((int)17)) {
            if ((double)this.pitch <= 1.5707963267948966) {
                this.pitch = (float)((double)this.pitch + 0.01);
            }
            this.spherePosChanged = true;
        }
        if (Keyboard.isKeyDown((int)31)) {
            if ((double)this.pitch >= -1.5707963267948966) {
                this.pitch = (float)((double)this.pitch - 0.01);
            }
            this.spherePosChanged = true;
        }
        if (Keyboard.isKeyDown((int)30)) {
            this.yaw = (float)((double)this.yaw - 0.01);
            this.spherePosChanged = true;
        }
        if (Keyboard.isKeyDown((int)32)) {
            this.yaw = (float)((double)this.yaw + 0.01);
            this.spherePosChanged = true;
        }
        if (Keyboard.isKeyDown((int)16)) {
            this.radius = (float)((double)this.radius - 0.1);
            this.spherePosChanged = true;
        }
        if (Keyboard.isKeyDown((int)18)) {
            this.radius = (float)((double)this.radius + 0.1);
            this.spherePosChanged = true;
        }
        this.updatePosition();
    }

    @Override
    public void handleMouseMovement(int dx, int dy, int dwheel) {
        if (dx != 0 || dy != 0 || dwheel != 0) {
            this.yaw -= (float)dx * 0.005f;
            this.pitch -= (float)dy * 0.008f;
            this.radius -= (float)dwheel * 0.01f;
            if (this.radius > 40.0f) {
                this.radius = 40.0f;
            } else if (this.camera.mode == CameraMode.Battle) {
                if (this.radius < 4.0f) {
                    this.radius = 4.0f;
                }
            } else if (this.radius < 2.0f) {
                this.radius = 2.0f;
            }
            this.spherePosChanged = true;
            this.updatePosition();
        }
    }
}

