/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.materials;

import com.pixelmongenerations.client.materials.EnumMaterialOption;
import com.pixelmongenerations.core.util.helper.ImageHelper;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.HashMap;
import javax.imageio.ImageIO;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.texture.SimpleTexture;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.resources.IResource;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;

@SideOnly(value=Side.CLIENT)
public class Cubemap
extends SimpleTexture {
    public int sqr;
    public int width;
    public int height;
    public final ByteBuffer[] buffers = new ByteBuffer[6];
    protected static HashMap<ResourceLocation, Cubemap> cubemaps = new HashMap();
    public final ResourceLocation resourceLoc;

    public Cubemap(ResourceLocation resLoc) throws IOException {
        super(resLoc);
        this.resourceLoc = resLoc;
        BufferedImage fullImg = ImageHelper.getImage(resLoc);
        this.initBuffers(fullImg, resLoc.toString());
        cubemaps.put(resLoc, this);
    }

    protected void initBuffers(BufferedImage fullImg, String fileLoc) throws IOException {
        this.width = fullImg.getWidth();
        this.height = fullImg.getHeight();
        if ((double)this.height % 4.0 != 0.0 || (double)this.width % 3.0 != 0.0 || this.height / 4 != this.width / 3) {
            String dimError = "The Dimensions of %s are invalid! A Cubemap image must have an exact width-to-height ratio of 3:4!";
            throw new IOException(String.format(dimError, fileLoc));
        }
        this.sqr = this.width / 3;
        BufferedImage subImg = null;
        for (int i = 0; i < 6; ++i) {
            ByteBuffer buffer;
            switch (i) {
                case 0: {
                    subImg = ImageHelper.rotateImg(fullImg.getSubimage(2 * this.sqr, this.sqr, this.sqr, this.sqr), 90.0);
                    break;
                }
                case 1: {
                    subImg = ImageHelper.rotateImg(fullImg.getSubimage(0, this.sqr, this.sqr, this.sqr), -90.0);
                    break;
                }
                case 2: {
                    subImg = fullImg.getSubimage(this.sqr, this.sqr, this.sqr, this.sqr);
                    break;
                }
                case 3: {
                    subImg = fullImg.getSubimage(this.sqr, 3 * this.sqr, this.sqr, this.sqr);
                    break;
                }
                case 4: {
                    subImg = fullImg.getSubimage(this.sqr, 2 * this.sqr, this.sqr, this.sqr);
                    break;
                }
                case 5: {
                    subImg = ImageHelper.rotateImg(fullImg.getSubimage(this.sqr, 0, this.sqr, this.sqr), 180.0);
                }
            }
            this.buffers[i] = buffer = ImageHelper.getBuffer(subImg);
            GL11.glTexImage2D((int)(34069 + i), (int)0, (int)32856, (int)this.sqr, (int)this.sqr, (int)0, (int)6408, (int)5121, (ByteBuffer)buffer);
        }
    }

    public static void begin(ResourceLocation resLoc) {
        if (!Minecraft.isFancyGraphicsEnabled()) {
            return;
        }
        Cubemap map = cubemaps.get(resLoc);
        if (map == null) {
            try {
                map = new Cubemap(resLoc);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        map.start();
    }

    protected void newBinding() {
        int bindID = this.getGlTextureId();
        GL11.glBindTexture((int)34067, (int)bindID);
        for (int i = 0; i < 6; ++i) {
            GL11.glTexImage2D((int)(34069 + i), (int)0, (int)32856, (int)this.sqr, (int)this.sqr, (int)0, (int)6408, (int)5121, (ByteBuffer)this.buffers[i]);
        }
    }

    public void start() {
        OpenGlHelper.setActiveTexture(EnumMaterialOption.cubemapID);
        GL11.glEnable((int)3168);
        GL11.glEnable((int)3169);
        GL11.glEnable((int)3170);
        GL11.glEnable((int)34067);
        GL11.glEnable((int)2977);
        for (int i = 0; i < 6; ++i) {
            GL11.glTexImage2D((int)(34069 + i), (int)0, (int)32856, (int)this.sqr, (int)this.sqr, (int)0, (int)6408, (int)5121, (ByteBuffer)this.buffers[i]);
        }
        GL11.glPixelStorei((int)3317, (int)1);
        GL11.glTexGeni((int)8192, (int)9472, (int)34066);
        GL11.glTexGeni((int)8193, (int)9472, (int)34066);
        GL11.glTexGeni((int)8194, (int)9472, (int)34066);
    }

    public void end() {
        GL11.glPixelStorei((int)3317, (int)1);
        GL11.glDisable((int)3168);
        GL11.glDisable((int)3169);
        GL11.glDisable((int)3170);
        GL11.glDisable((int)34067);
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
    }

    public static void debugResourceStuff(ResourceLocation resLoc) {
        TextureManager texManager = Minecraft.getMinecraft().renderEngine;
        BufferedImage img = null;
        try {
            Field texManagerResManager = TextureManager.class.getDeclaredField("theResourceManager");
            texManagerResManager.setAccessible(true);
            IResourceManager resManager = (IResourceManager)texManagerResManager.get(texManager);
            IResource resource = resManager.getResource(resLoc);
            InputStream stream = resource.getInputStream();
            img = ImageIO.read(stream);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        new SimpleTexture(resLoc);
        System.out.println(img);
    }

    public String toString() {
        return "Cubemap(" + this.sqr + "x" + this.sqr + ";" + this.resourceLoc + ")";
    }

    static {
        GL11.glPixelStorei((int)3317, (int)1);
        GL11.glTexParameteri((int)34067, (int)10242, (int)33071);
        GL11.glTexParameteri((int)34067, (int)10243, (int)33071);
        GL11.glTexParameteri((int)34067, (int)32882, (int)33071);
        GL11.glTexParameteri((int)34067, (int)10240, (int)9728);
        GL11.glTexParameteri((int)34067, (int)10241, (int)9729);
    }
}

