/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.BufferUtils
 *  org.lwjgl.opengl.ARBBufferObject
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.models.obj;

import com.pixelmongenerations.client.models.IPixelmonModel;
import com.pixelmongenerations.client.models.obj.Face;
import com.pixelmongenerations.client.models.obj.Group;
import com.pixelmongenerations.client.models.obj.Material;
import com.pixelmongenerations.client.models.obj.TextureCoordinate;
import com.pixelmongenerations.client.models.obj.Vertex;
import com.pixelmongenerations.client.models.obj.parser.LineParser;
import com.pixelmongenerations.client.models.obj.parser.obj.ObjLineParserFactory;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Objects;
import java.util.function.Function;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModel;
import net.minecraftforge.common.model.IModelState;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBBufferObject;
import org.lwjgl.opengl.GL11;

public class WavefrontObject
implements IPixelmonModel,
IModel {
    private ArrayList<Vertex> vertices = new ArrayList();
    private ArrayList<Vertex> normals = new ArrayList();
    private ArrayList<TextureCoordinate> textures = new ArrayList();
    private ArrayList<Group> groups = new ArrayList();
    private HashMap<String, Group> groupsDirectAccess = new HashMap();
    HashMap<String, Material> materials = new HashMap();
    private ObjLineParserFactory parserFactory;
    private Material currentMaterial;
    private Group currentGroup;
    public double radius = 0.0;
    public float xScale;
    public float yScale;
    public float zScale;
    public Vertex translate;
    public Vertex rotate;
    public int displayListId = 0;
    public int vertexVbo = -1;
    public int textureVbo = -1;
    public int normalsVbo = -1;
    public int vertexCount = 0;

    public WavefrontObject(InputStream stream) {
        this(stream, 1.0f, 1.0f, 1.0f, new Vertex(), new Vertex());
    }

    public WavefrontObject(InputStream stream, float xScale, float yScale, float zScale) {
        this(stream, xScale, yScale, zScale, new Vertex(), new Vertex());
    }

    public WavefrontObject(InputStream stream, float scale) {
        this(stream, scale, scale, scale, new Vertex(), new Vertex());
    }

    public WavefrontObject(InputStream stream, float scale, Vertex translation, Vertex rotation) {
        this(stream, scale, scale, scale, translation, rotation);
    }

    public WavefrontObject(InputStream stream, float xScale, float yScale, float zScale, Vertex translation, Vertex rotation) {
        try {
            this.translate = translation;
            this.rotate = rotation;
            this.xScale = xScale;
            this.yScale = yScale;
            this.zScale = zScale;
            this.parse(stream);
            this.calculateRadius();
        }
        catch (Exception e) {
            System.out.println("Error, could not load obj");
        }
    }

    private void calculateRadius() {
        for (Vertex vertex : this.vertices) {
            double currentNorm = vertex.norm();
            if (currentNorm <= this.radius) continue;
            this.radius = currentNorm;
        }
    }

    public void parse(InputStream fileInput) {
        this.parserFactory = new ObjLineParserFactory(this);
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(fileInput));
            String currentLine = null;
            while ((currentLine = in.readLine()) != null) {
                this.parseLine(currentLine);
            }
            in.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error reading file");
        }
    }

    private void parseLine(String currentLine) {
        if ("".equals(currentLine)) {
            return;
        }
        LineParser parser = this.parserFactory.getLineParser(currentLine);
        parser.parse();
        parser.incoporateResults(this);
    }

    public void render() {
        boolean useVBO = OpenGlHelper.useVbo();
        if (!useVBO) {
            if (this.displayListId != 0) {
                GL11.glCallList((int)this.displayListId);
                return;
            }
            this.displayListId = GL11.glGenLists((int)1);
            GL11.glNewList((int)this.displayListId, (int)4864);
            for (int i = 0; i < this.getGroups().size(); ++i) {
                this.renderGroup(this.getGroups().get(i));
            }
            GL11.glEndList();
            GL11.glCallList((int)this.displayListId);
        } else {
            if (this.vertexVbo == -1) {
                this.vertexVbo = ARBBufferObject.glGenBuffersARB();
                this.textureVbo = ARBBufferObject.glGenBuffersARB();
                this.normalsVbo = ARBBufferObject.glGenBuffersARB();
                this.buildVBO();
                this.vertexCount = 0;
                for (Group g : this.groups) {
                    this.vertexCount += g.vertices.size();
                }
            }
            OpenGlHelper.glBindBuffer(OpenGlHelper.GL_ARRAY_BUFFER, this.vertexVbo);
            GL11.glVertexPointer((int)3, (int)5126, (int)0, (long)0L);
            OpenGlHelper.glBindBuffer(OpenGlHelper.GL_ARRAY_BUFFER, this.textureVbo);
            GL11.glTexCoordPointer((int)2, (int)5126, (int)0, (long)0L);
            OpenGlHelper.glBindBuffer(OpenGlHelper.GL_ARRAY_BUFFER, this.normalsVbo);
            GL11.glNormalPointer((int)5126, (int)0, (long)0L);
            GL11.glEnableClientState((int)32884);
            GL11.glEnableClientState((int)32888);
            GL11.glEnableClientState((int)32885);
            GL11.glDrawArrays((int)4, (int)0, (int)this.vertexCount);
            GL11.glDisableClientState((int)32884);
            GL11.glDisableClientState((int)32888);
            GL11.glDisableClientState((int)32885);
            OpenGlHelper.glBindBuffer(OpenGlHelper.GL_ARRAY_BUFFER, 0);
        }
    }

    public void buildVBO() {
        int vertexCount = 0;
        int texCount = 0;
        int normalCount = 0;
        for (Group group : this.groups) {
            vertexCount += group.vertices.size();
            texCount += group.texcoords.size();
            normalCount += group.normals.size();
        }
        FloatBuffer vertexBuffer = BufferUtils.createFloatBuffer((int)(vertexCount * 3));
        for (Group group : this.groups) {
            for (Vertex vertex2 : group.vertices) {
                vertexBuffer.put(vertex2.getX());
                vertexBuffer.put(vertex2.getY());
                vertexBuffer.put(vertex2.getZ());
            }
        }
        vertexBuffer.flip();
        OpenGlHelper.glBindBuffer(OpenGlHelper.GL_ARRAY_BUFFER, this.vertexVbo);
        ARBBufferObject.glBufferDataARB((int)OpenGlHelper.GL_ARRAY_BUFFER, (FloatBuffer)vertexBuffer, (int)35044);
        FloatBuffer textureCoords = BufferUtils.createFloatBuffer((int)(texCount * 2));
        for (Group group : this.groups) {
            group.texcoords.stream().filter(Objects::nonNull).forEach(texCoord -> {
                textureCoords.put(texCoord.getU());
                textureCoords.put(texCoord.getV());
            });
        }
        textureCoords.flip();
        OpenGlHelper.glBindBuffer(OpenGlHelper.GL_ARRAY_BUFFER, this.textureVbo);
        ARBBufferObject.glBufferDataARB((int)OpenGlHelper.GL_ARRAY_BUFFER, (FloatBuffer)textureCoords, (int)35044);
        FloatBuffer normals = BufferUtils.createFloatBuffer((int)(normalCount * 3));
        for (Group group : this.groups) {
            group.normals.stream().filter(Objects::nonNull).forEach(vertex -> {
                normals.put(vertex.getX());
                normals.put(vertex.getY());
                normals.put(vertex.getZ());
            });
        }
        normals.flip();
        OpenGlHelper.glBindBuffer(OpenGlHelper.GL_ARRAY_BUFFER, this.normalsVbo);
        ARBBufferObject.glBufferDataARB((int)OpenGlHelper.GL_ARRAY_BUFFER, (FloatBuffer)normals, (int)35044);
    }

    protected void renderGroup(Group group) {
        this.currentGroup = group;
        for (Face face : this.currentGroup.getFaces()) {
            GL11.glBegin((int)4);
            for (int vertexID = 0; vertexID < face.getVertices().length; ++vertexID) {
                Vertex vertex = face.getVertices()[vertexID];
                if (vertexID < face.getNormals().length && face.getNormals()[vertexID] != null) {
                    Vertex normal = face.getNormals()[vertexID];
                    GL11.glNormal3f((float)normal.getX(), (float)normal.getY(), (float)normal.getZ());
                }
                if (vertexID < face.getTextures().length && face.getTextures()[vertexID] != null) {
                    TextureCoordinate textureCoord = face.getTextures()[vertexID];
                    GL11.glTexCoord2d((double)textureCoord.getU(), (double)textureCoord.getV());
                }
                GL11.glVertex3f((float)vertex.getX(), (float)vertex.getY(), (float)vertex.getZ());
            }
            GL11.glEnd();
        }
    }

    public void setMaterials(HashMap<String, Material> materials) {
        this.materials = materials;
    }

    public void setTextures(ArrayList<TextureCoordinate> textures) {
        this.textures = textures;
    }

    public ArrayList<TextureCoordinate> getTextureList() {
        return this.textures;
    }

    public void setVertices(ArrayList<Vertex> vertices) {
        this.vertices = vertices;
    }

    public ArrayList<Vertex> getVertices() {
        return this.vertices;
    }

    public void setNormals(ArrayList<Vertex> normals) {
        this.normals = normals;
    }

    public ArrayList<Vertex> getNormals() {
        return this.normals;
    }

    public HashMap<String, Material> getMaterials() {
        return this.materials;
    }

    public Material getCurrentMaterial() {
        return this.currentMaterial;
    }

    public void setCurrentMaterial(Material currentMaterial) {
        this.currentMaterial = currentMaterial;
    }

    public ArrayList<Group> getGroups() {
        return this.groups;
    }

    public HashMap<String, Group> getGroupsDirectAccess() {
        return this.groupsDirectAccess;
    }

    public Group getCurrentGroup() {
        return this.currentGroup;
    }

    public void setCurrentGroup(Group currentGroup) {
        this.currentGroup = currentGroup;
    }

    public String getBoudariesText() {
        float minX = 0.0f;
        float maxX = 0.0f;
        float minY = 0.0f;
        float maxY = 0.0f;
        float minZ = 0.0f;
        float maxZ = 0.0f;
        Vertex currentVertex = null;
        for (int i = 0; i < this.getVertices().size(); ++i) {
            currentVertex = this.getVertices().get(i);
            if (currentVertex.getX() > maxX) {
                maxX = currentVertex.getX();
            }
            if (currentVertex.getX() < minX) {
                minX = currentVertex.getX();
            }
            if (currentVertex.getY() > maxY) {
                maxY = currentVertex.getY();
            }
            if (currentVertex.getY() < minY) {
                minY = currentVertex.getY();
            }
            if (currentVertex.getZ() > maxZ) {
                maxZ = currentVertex.getZ();
            }
            if (currentVertex.getZ() >= minZ) continue;
            minZ = currentVertex.getZ();
        }
        return "maxX=" + maxX + " minX=" + minX + " maxY=" + maxY + " minY=" + minY + " maxZ=" + maxZ + " minZ=" + minZ;
    }

    public void printBoudariesText() {
        System.out.println(this.getBoudariesText());
    }

    @Override
    public void renderAll() {
        block2: {
            try {
                this.render();
            }
            catch (Exception e) {
                e.printStackTrace();
                if (!OpenGlHelper.useVbo()) break block2;
                OpenGlHelper.glBindBuffer(OpenGlHelper.GL_ARRAY_BUFFER, 0);
            }
        }
    }

    @Override
    public Collection<ResourceLocation> getDependencies() {
        return null;
    }

    @Override
    public IBakedModel bake(IModelState state, VertexFormat format, Function<ResourceLocation, TextureAtlasSprite> bakedTextureGetter) {
        return null;
    }

    @Override
    public IModelState getDefaultState() {
        return null;
    }

    @Override
    public Collection<ResourceLocation> getTextures() {
        return null;
    }
}

