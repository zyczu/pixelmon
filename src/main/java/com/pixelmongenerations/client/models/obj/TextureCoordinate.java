/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj;

public class TextureCoordinate {
    private float u;
    private float v;
    private float w;

    public float getU() {
        return this.u;
    }

    public void setU(float u) {
        this.u = u;
    }

    public float getV() {
        return this.v;
    }

    public void setV(float v) {
        this.v = v;
    }

    public float getW() {
        return this.w;
    }

    public void setW(float w) {
        this.w = w;
    }
}

