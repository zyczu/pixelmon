/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.obj.parser.obj;

import com.pixelmongenerations.client.models.obj.LineParserFactory;
import com.pixelmongenerations.client.models.obj.NormalParser;
import com.pixelmongenerations.client.models.obj.WavefrontObject;
import com.pixelmongenerations.client.models.obj.parser.CommentParser;
import com.pixelmongenerations.client.models.obj.parser.obj.FaceParser;
import com.pixelmongenerations.client.models.obj.parser.obj.FreeFormParser;
import com.pixelmongenerations.client.models.obj.parser.obj.GroupParser;
import com.pixelmongenerations.client.models.obj.parser.obj.MaterialParser;
import com.pixelmongenerations.client.models.obj.parser.obj.TextureCooParser;
import com.pixelmongenerations.client.models.obj.parser.obj.VertexParser;

public class ObjLineParserFactory
extends LineParserFactory {
    public ObjLineParserFactory(WavefrontObject object) {
        this.object = object;
        this.parsers.put("v", new VertexParser());
        this.parsers.put("vn", new NormalParser());
        this.parsers.put("vp", new FreeFormParser());
        this.parsers.put("vt", new TextureCooParser());
        this.parsers.put("f", new FaceParser(object));
        this.parsers.put("#", new CommentParser());
        this.parsers.put("usemtl", new MaterialParser());
        this.parsers.put("g", new GroupParser());
    }
}

