/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.util.vector.Matrix4f
 */
package com.pixelmongenerations.client.models.smd;

import com.pixelmongenerations.client.models.animations.EnumGeomData;
import com.pixelmongenerations.client.models.animations.IModulized;
import com.pixelmongenerations.client.models.smd.AnimFrame;
import com.pixelmongenerations.client.models.smd.DeformVertex;
import com.pixelmongenerations.client.models.smd.SmdModel;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.core.util.helper.VectorHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.lwjgl.util.vector.Matrix4f;

public class Bone
implements IModulized {
    public Bone copy = null;
    public String name;
    public int ID;
    public Bone parent;
    public SmdModel owner;
    private Boolean isDummy;
    public Matrix4f rest;
    public Matrix4f restInverted;
    public Matrix4f modified = new Matrix4f();
    public Matrix4f difference = new Matrix4f();
    public Matrix4f prevInverted = new Matrix4f();
    public ArrayList<Bone> children = new ArrayList();
    public HashMap<DeformVertex, Float> verts = new HashMap();
    public HashMap<String, HashMap<Integer, Matrix4f>> animatedTransforms = new HashMap();

    public Bone(String name, int ID, Bone parent, SmdModel owner) {
        this.name = name;
        this.ID = ID;
        this.parent = parent;
        this.owner = owner;
    }

    public Bone(Bone b, Bone parent, SmdModel owner) {
        this.name = b.name;
        this.ID = b.ID;
        this.owner = owner;
        this.parent = parent;
        for (Map.Entry<DeformVertex, Float> entry : b.verts.entrySet()) {
            this.verts.put(owner.verts.get(entry.getKey().ID), entry.getValue());
        }
        this.animatedTransforms = new HashMap<String, HashMap<Integer, Matrix4f>>(b.animatedTransforms);
        this.restInverted = b.restInverted;
        this.rest = b.rest;
        b.copy = this;
    }

    public void setChildren(Bone b, ArrayList<Bone> bones) {
        for (int i = 0; i < b.children.size(); ++i) {
            Bone child = b.children.get(i);
            this.children.add(bones.get(child.ID));
            bones.get((int)child.ID).parent = this;
        }
    }

    public boolean isDummy() {
        return this.isDummy == null ? (this.isDummy = Boolean.valueOf(this.parent == null && this.children.isEmpty())) : this.isDummy;
    }

    public void setRest(Matrix4f resting) {
        this.rest = resting;
    }

    public void addChild(Bone child) {
        this.children.add(child);
    }

    public void addVertex(DeformVertex v, float weight) {
        if (this.name.equals("blender_implicit")) {
            throw new UnsupportedOperationException("NO.");
        }
        this.verts.put(v, Float.valueOf(weight));
    }

    private void reform(Matrix4f parentMatrix) {
        this.rest = Matrix4f.mul((Matrix4f)parentMatrix, (Matrix4f)this.rest, null);
        if (ValveStudioModel.debugModel) {
            VectorHelper.print(this.name + ' ' + (Object)this.rest);
        }
        this.reformChildren();
    }

    public void reformChildren() {
        for (Bone child : this.children) {
            child.reform(this.rest);
        }
    }

    public void invertRestMatrix() {
        this.restInverted = Matrix4f.invert((Matrix4f)this.rest, null);
    }

    public void reset() {
        this.modified.setIdentity();
    }

    public void preloadAnimation(AnimFrame key, Matrix4f animated) {
        HashMap<Integer, Matrix4f> precalcArray = this.animatedTransforms.containsKey(key.owner.animationName) ? this.animatedTransforms.get(key.owner.animationName) : new HashMap<Integer, Matrix4f>();
        precalcArray.put(key.ID, animated);
        this.animatedTransforms.put(key.owner.animationName, precalcArray);
    }

    public void setModified() {
        Matrix4f real;
        Matrix4f realInverted;
        if (this.owner.owner.hasAnimations() && this.owner.currentAnim != null) {
            AnimFrame currentFrame = this.owner.currentAnim.frames.get(this.owner.currentAnim.currentFrameIndex);
            realInverted = currentFrame.transforms.get(this.ID);
            real = currentFrame.invertTransforms.get(this.ID);
        } else {
            realInverted = this.rest;
            real = this.restInverted;
        }
        Matrix4f delta = new Matrix4f();
        Matrix4f absolute = new Matrix4f();
        Matrix4f.mul((Matrix4f)realInverted, (Matrix4f)real, (Matrix4f)delta);
        this.modified = this.parent != null ? Matrix4f.mul((Matrix4f)this.parent.modified, (Matrix4f)delta, (Matrix4f)this.initModified()) : delta;
        Matrix4f.mul((Matrix4f)real, (Matrix4f)this.modified, (Matrix4f)absolute);
        Matrix4f.invert((Matrix4f)absolute, (Matrix4f)this.prevInverted);
        this.children.forEach(Bone::setModified);
    }

    protected Matrix4f initModified() {
        return this.modified == null ? (this.modified = new Matrix4f()) : this.modified;
    }

    public void applyModified() {
        AnimFrame currentFrame = this.owner.currentFrame();
        if (currentFrame != null) {
            HashMap<Integer, Matrix4f> precalcArray = this.animatedTransforms.get(currentFrame.owner.animationName);
            Matrix4f animated = precalcArray.get(currentFrame.ID);
            Matrix4f animatedChange = new Matrix4f();
            Matrix4f.mul((Matrix4f)animated, (Matrix4f)this.restInverted, (Matrix4f)animatedChange);
            this.modified = this.modified == null ? animatedChange : Matrix4f.mul((Matrix4f)this.modified, (Matrix4f)animatedChange, (Matrix4f)this.modified);
        }
        this.verts.forEach((key, value) -> key.applyModified(this, value.floatValue()));
        this.reset();
    }

    @Override
    public float setValue(float value, EnumGeomData d) {
        return value;
    }

    @Override
    public float getValue(EnumGeomData d) {
        return 0.0f;
    }
}

