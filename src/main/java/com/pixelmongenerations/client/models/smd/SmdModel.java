/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.smd;

import com.pixelmongenerations.client.models.smd.AnimFrame;
import com.pixelmongenerations.client.models.smd.Bone;
import com.pixelmongenerations.client.models.smd.DeformVertex;
import com.pixelmongenerations.client.models.smd.GabeNewellException;
import com.pixelmongenerations.client.models.smd.Material;
import com.pixelmongenerations.client.models.smd.NormalizedFace;
import com.pixelmongenerations.client.models.smd.SmdAnimation;
import com.pixelmongenerations.client.models.smd.TextureCoordinate;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.util.helper.CommonHelper;
import com.pixelmongenerations.core.util.helper.VectorHelper;
import dev.thecodewarrior.binarysmd.formats.SMDBinaryReader;
import dev.thecodewarrior.binarysmd.studiomdl.NodesBlock;
import dev.thecodewarrior.binarysmd.studiomdl.SMDFile;
import dev.thecodewarrior.binarysmd.studiomdl.SMDFileBlock;
import dev.thecodewarrior.binarysmd.studiomdl.SkeletonBlock;
import dev.thecodewarrior.binarysmd.studiomdl.TrianglesBlock;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import org.msgpack.core.MessagePack;
import org.msgpack.core.MessageUnpacker;

public class SmdModel {
    public final ValveStudioModel owner;
    public ArrayList<NormalizedFace> faces = new ArrayList(0);
    public ArrayList<DeformVertex> verts = new ArrayList(0);
    public ArrayList<Bone> bones = new ArrayList(0);
    public HashMap<String, Bone> nameToBoneMapping = new HashMap();
    public HashMap<String, Material> materialsByName;
    public HashMap<Material, ArrayList<NormalizedFace>> facesByMaterial;
    public SmdAnimation currentAnim;
    private int vertexIDBank = 0;
    protected boolean isBodyGroupPart;
    int lineCount = -1;
    public Bone root;

    public SmdModel(SmdModel model, ValveStudioModel owner) {
        Bone b;
        int i;
        this.owner = owner;
        this.isBodyGroupPart = model.isBodyGroupPart;
        for (NormalizedFace face2 : model.faces) {
            DeformVertex[] vertices = new DeformVertex[face2.vertices.length];
            for (int i2 = 0; i2 < vertices.length; ++i2) {
                DeformVertex d = new DeformVertex(face2.vertices[i2]);
                CommonHelper.ensureIndex(this.verts, d.ID);
                this.verts.set(d.ID, d);
            }
        }
        this.faces.addAll(model.faces.stream().map(face -> new NormalizedFace((NormalizedFace)face, this.verts)).collect(Collectors.toList()));
        for (i = 0; i < model.bones.size(); ++i) {
            b = model.bones.get(i);
            this.bones.add(new Bone(b, null, this));
        }
        for (i = 0; i < model.bones.size(); ++i) {
            b = model.bones.get(i);
            b.copy.setChildren(b, this.bones);
        }
        this.root = model.root.copy;
        owner.sendBoneData(this);
    }

    public SmdModel(ValveStudioModel owner, ResourceLocation resloc) throws GabeNewellException {
        this.owner = owner;
        this.isBodyGroupPart = false;
        this.loadSmdModel(resloc, null);
        this.setBoneChildren();
        this.determineRoot();
        owner.sendBoneData(this);
        ValveStudioModel.print("Number of vertices = " + this.verts.size());
    }

    public SmdModel(ValveStudioModel owner, ResourceLocation resloc, SmdModel body) throws GabeNewellException {
        this.owner = owner;
        this.isBodyGroupPart = true;
        this.loadSmdModel(resloc, body);
        this.setBoneChildren();
        this.determineRoot();
        owner.sendBoneData(this);
    }

    private void loadSmdModel(ResourceLocation resloc, SmdModel body) throws GabeNewellException {
        SMDFile file;
        BufferedInputStream inputStream = Pixelmon.PROXY.getStreamForResourceLocation(resloc);
        try (MessageUnpacker unpacker = MessagePack.newDefaultUnpacker(inputStream);){
            file = new SMDBinaryReader().read(unpacker);
        }
        catch (IOException e) {
            e.printStackTrace();
            return;
        }
        for (SMDFileBlock block : file.blocks) {
            if (block instanceof NodesBlock) {
                for (NodesBlock.Bone bone : ((NodesBlock)block).bones) {
                    this.parseBones(bone, body);
                }
                ValveStudioModel.print("Number of model bones = " + this.bones.size());
                continue;
            }
            if (block instanceof SkeletonBlock) {
                for (SkeletonBlock.Keyframe keyframe : ((SkeletonBlock)block).keyframes) {
                    if (this.isBodyGroupPart) continue;
                    this.parseKeyframe(keyframe);
                }
                continue;
            }
            if (!(block instanceof TrianglesBlock)) continue;
            for (TrianglesBlock.Triangle triangle : ((TrianglesBlock)block).triangles) {
                Material mat = this.owner.usesMaterials ? this.requestMaterial(triangle.material) : null;
                this.parseFace(triangle, mat);
            }
        }
        ValveStudioModel.print("Number of faces = " + this.faces.size());
    }

    private void parseFace(TrianglesBlock.Triangle triangle, Material mat) {
        DeformVertex[] faceVerts = new DeformVertex[3];
        TextureCoordinate[] uvs = new TextureCoordinate[3];
        for (int i = 0; i < triangle.vertices.size(); ++i) {
            TrianglesBlock.Vertex vertex = triangle.vertices.get(i);
            float x = vertex.posX;
            float y = -vertex.posY;
            float z = -vertex.posZ;
            float xn = vertex.normX;
            float yn = -vertex.normY;
            float zn = -vertex.normZ;
            DeformVertex v = this.getExisting(x, y, z);
            if (v == null) {
                faceVerts[i] = new DeformVertex(x, y, z, xn, yn, zn, this.vertexIDBank);
                CommonHelper.ensureIndex(this.verts, this.vertexIDBank);
                this.verts.set(this.vertexIDBank, faceVerts[i]);
                ++this.vertexIDBank;
            } else {
                faceVerts[i] = v;
            }
            uvs[i] = new TextureCoordinate(vertex.u, 1.0f - vertex.v);
            this.doBoneWeights(vertex.links, faceVerts[i]);
        }
        NormalizedFace face = new NormalizedFace(faceVerts, uvs);
        face.vertices = faceVerts;
        face.textureCoordinates = uvs;
        this.faces.add(face);
        if (mat != null) {
            ArrayList<NormalizedFace> list;
            if (this.facesByMaterial == null) {
                this.facesByMaterial = new HashMap();
            }
            if ((list = this.facesByMaterial.get(mat)) == null) {
                list = new ArrayList();
                this.facesByMaterial.put(mat, list);
            }
            list.add(face);
        }
    }

    private void parseKeyframe(SkeletonBlock.Keyframe keyframe) {
        for (SkeletonBlock.BoneState state : keyframe.states) {
            this.bones.get(state.bone).setRest(VectorHelper.matrix4FromLocRot(state.posX, -state.posY, -state.posZ, state.rotX, -state.rotY, -state.rotZ));
        }
    }

    private void parseBones(NodesBlock.Bone b, SmdModel body) {
        Bone theBone;
        int id = b.id;
        String boneName = b.name;
        Bone bone = theBone = body != null ? body.getBoneByName(boneName) : null;
        if (theBone == null) {
            int parentID = b.parent;
            Bone parent = parentID >= 0 ? this.bones.get(parentID) : null;
            theBone = new Bone(boneName, id, parent, this);
        }
        CommonHelper.ensureIndex(this.bones, id);
        this.bones.set(id, theBone);
        this.nameToBoneMapping.put(boneName, theBone);
        ValveStudioModel.print(boneName);
    }

    public Material requestMaterial(String materialName) throws GabeNewellException {
        Material result;
        if (!this.owner.usesMaterials) {
            return null;
        }
        if (this.materialsByName == null) {
            this.materialsByName = new HashMap();
        }
        if ((result = this.materialsByName.get(materialName)) != null) {
            return result;
        }
        String materialPath = this.owner.getMaterialPath(materialName);
        URL materialURL = SmdModel.class.getResource(materialPath);
        try {
            File materialFile = new File(materialURL.toURI());
            result = new Material(materialFile);
            this.materialsByName.put(materialName, result);
            return result;
        }
        catch (Exception e) {
            throw new GabeNewellException(e);
        }
    }

    private DeformVertex getExisting(float x, float y, float z) {
        for (DeformVertex v : this.verts) {
            if (!v.equals(x, y, z)) continue;
            return v;
        }
        return null;
    }

    private void doBoneWeights(List<TrianglesBlock.Link> values, DeformVertex vert) {
        int i;
        int links = values.size();
        float[] weights = new float[links];
        float sum = 0.0f;
        for (i = 0; i < links; ++i) {
            weights[i] = values.get((int)i).weight;
            sum += weights[i];
        }
        for (i = 0; i < links; ++i) {
            TrianglesBlock.Link link = values.get(i);
            float weight = weights[i] / sum;
            this.bones.get(link.bone).addVertex(vert, weight);
        }
    }

    private void setBoneChildren() {
        for (int i = 0; i < this.bones.size(); ++i) {
            Bone theBone = this.bones.get(i);
            this.bones.stream().filter(child -> child.parent == theBone).forEach(theBone::addChild);
        }
    }

    private void determineRoot() {
        for (Bone b : this.bones) {
            if (b.parent != null || b.children.isEmpty()) continue;
            this.root = b;
            break;
        }
        if (this.root == null) {
            for (Bone b : this.bones) {
                if (b.name.equals("blender_implicit")) continue;
                this.root = b;
                break;
            }
        }
    }

    public void setAnimation(SmdAnimation anim) {
        this.currentAnim = anim;
    }

    public Bone getBoneByID(int id) {
        try {
            return this.bones.get(id);
        }
        catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public Bone getBoneByName(String name) {
        for (Bone b : this.bones) {
            if (!b.name.equals(name)) continue;
            return b;
        }
        return null;
    }

    public AnimFrame currentFrame() {
        return this.currentAnim == null ? null : (this.currentAnim.frames == null ? null : (this.currentAnim.frames.isEmpty() ? null : this.currentAnim.frames.get(this.currentAnim.currentFrameIndex)));
    }

    public void resetVerts() {
        this.verts.forEach(DeformVertex::reset);
    }

    public void render(boolean hasChanged) {
        boolean isPokeball = this.owner.resource.getPath().contains("pokeballs");
        boolean smooth = isPokeball ? PixelmonConfig.enableSmoothPokeballShading : (this.owner.overrideSmoothShading ? false : PixelmonConfig.enableSmoothPokemonShading);
        Tessellator instance = Tessellator.getInstance();
        BufferBuilder buffer = instance.getBuffer();
        buffer.begin(4, DefaultVertexFormats.POSITION_TEX_NORMAL);
        Consumer<NormalizedFace> faceConsumer = face -> face.addFaceForRender(buffer, smooth);
        if (!this.owner.usesMaterials) {
            this.faces.forEach(faceConsumer);
        } else {
            this.facesByMaterial.forEach((material, faces) -> {
                material.pre();
                faces.forEach(faceConsumer);
                material.post();
            });
        }
        instance.draw();
    }
}

