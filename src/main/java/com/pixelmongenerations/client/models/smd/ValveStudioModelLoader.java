/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.smd;

import com.pixelmongenerations.client.models.smd.GabeNewellException;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ICustomModelLoader;
import net.minecraftforge.client.model.IModel;
import net.minecraftforge.client.model.ModelLoaderRegistry;

public class ValveStudioModelLoader
implements ICustomModelLoader {
    public static final ValveStudioModelLoader instance = new ValveStudioModelLoader();

    public ValveStudioModelLoader() {
        ModelLoaderRegistry.registerLoader(this);
    }

    @Override
    public void onResourceManagerReload(IResourceManager manager) {
    }

    @Override
    public boolean accepts(ResourceLocation modelLocation) {
        return modelLocation.getPath().endsWith(".pqc");
    }

    public IModel loadModel(ResourceLocation modelLocation, boolean overrideSmoothShading) {
        try {
            return new ValveStudioModel(modelLocation, overrideSmoothShading);
        }
        catch (GabeNewellException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public IModel loadModel(ResourceLocation modelLocation) {
        try {
            return new ValveStudioModel(modelLocation);
        }
        catch (GabeNewellException e) {
            e.printStackTrace();
            return null;
        }
    }
}

