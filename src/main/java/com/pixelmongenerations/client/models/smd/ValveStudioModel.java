/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.smd;

import com.pixelmongenerations.client.models.IPixelmonModel;
import com.pixelmongenerations.client.models.smd.Bone;
import com.pixelmongenerations.client.models.smd.DeformVertex;
import com.pixelmongenerations.client.models.smd.GabeNewellException;
import com.pixelmongenerations.client.models.smd.SmdAnimation;
import com.pixelmongenerations.client.models.smd.SmdModel;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.util.RegexPatterns;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModel;
import net.minecraftforge.common.model.IModelState;

public class ValveStudioModel
implements IModel,
IPixelmonModel {
    public static boolean debugModel = false;
    public SmdModel body;
    protected Bone root;
    ArrayList<Bone> allBones;
    public SmdAnimation currentAnimation;
    public ResourceLocation resource;
    public boolean overrideSmoothShading = false;
    public boolean hasChanged = true;
    private boolean hasAnimations = false;
    public HashMap<String, SmdAnimation> anims = new HashMap(4);
    protected String materialPath;
    protected boolean usesMaterials = false;
    private float scale = -1.0f;

    public ValveStudioModel(ValveStudioModel model) {
        this.body = new SmdModel(model.body, this);
        for (Map.Entry<String, SmdAnimation> entry : model.anims.entrySet()) {
            this.anims.put(entry.getKey(), new SmdAnimation(entry.getValue(), this));
        }
        this.hasAnimations = model.hasAnimations;
        this.usesMaterials = model.usesMaterials;
        this.scale = model.scale;
        this.resource = model.resource;
        this.currentAnimation = this.anims.get("idle");
        this.overrideSmoothShading = model.overrideSmoothShading;
    }

    public ValveStudioModel(ResourceLocation resource, boolean overrideSmoothShading) throws GabeNewellException {
        this.overrideSmoothShading = overrideSmoothShading;
        this.resource = resource;
        this.loadQC(resource);
        this.reformBones();
        this.precalculateAnimations();
    }

    public ValveStudioModel(ResourceLocation resource) throws GabeNewellException {
        this(resource, false);
    }

    public float getScale() {
        return this.scale;
    }

    public boolean hasAnimations() {
        return this.hasAnimations;
    }

    private void precalculateAnimations() {
        for (SmdAnimation anim : this.anims.values()) {
            anim.precalculateAnimation(this.body);
        }
    }

    @Override
    public void renderAll() {
        GlStateManager.shadeModel(7425);
        try {
            this.body.render(this.hasChanged);
        }
        catch (Exception e) {
            e.printStackTrace();
            if (!OpenGlHelper.useVbo()) {
                return;
            }
            OpenGlHelper.glBindBuffer(OpenGlHelper.GL_ARRAY_BUFFER, 0);
        }
        GlStateManager.shadeModel(7424);
    }

    void sendBoneData(SmdModel model) {
        this.allBones = model.bones;
        if (!model.isBodyGroupPart) {
            this.root = model.root;
        }
    }

    private void reformBones() {
        this.root.reformChildren();
        this.allBones.forEach(Bone::invertRestMatrix);
    }

    public void animate() {
        this.resetVerts(this.body);
        if (this.body.currentAnim == null) {
            this.setAnimation("idle");
        }
        this.root.setModified();
        this.allBones.forEach(Bone::applyModified);
        this.applyVertChange(this.body);
        this.hasChanged = true;
    }

    public void setAnimation(String animName) {
        this.currentAnimation = this.anims.containsKey(animName) ? this.anims.get(animName) : this.anims.get("idle");
        this.body.setAnimation(this.currentAnimation);
    }

    protected String getMaterialPath(String subFile) {
        int lastDot;
        String result = "/assets/pixelmon";
        if (!this.materialPath.startsWith("/")) {
            result = result + "/";
        }
        result = result + this.materialPath;
        if (!subFile.startsWith("/")) {
            result = result + "/";
        }
        result = (lastDot = (result = result + subFile).lastIndexOf(".")) == -1 ? result + ".mat" : result.substring(0, lastDot) + ".mat";
        return result;
    }

    private void resetVerts(SmdModel model) {
        if (model == null) {
            return;
        }
        model.verts.forEach(DeformVertex::reset);
    }

    private void applyVertChange(SmdModel model) {
        if (model == null) {
            return;
        }
        model.verts.forEach(DeformVertex::applyChange);
    }

    @Override
    public Collection<ResourceLocation> getDependencies() {
        return null;
    }

    @Override
    public Collection<ResourceLocation> getTextures() {
        return null;
    }

    @Override
    public IBakedModel bake(IModelState state, VertexFormat format, Function<ResourceLocation, TextureAtlasSprite> bakedTextureGetter) {
        return null;
    }

    @Override
    public IModelState getDefaultState() {
        return null;
    }

    public static void print(Object o) {
        if (debugModel) {
            System.out.println(o);
        }
    }

    private void loadQC(ResourceLocation resource) throws GabeNewellException {
        BufferedInputStream inputStream = Pixelmon.PROXY.getStreamForResourceLocation(resource);
        int lineCount = 0;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));){
            String currentLine;
            String[] bodyParams = null;
            ArrayList<String[]> animParams = new ArrayList<String[]>();
            while ((currentLine = reader.readLine()) != null) {
                ++lineCount;
                String[] params = RegexPatterns.MULTIPLE_WHITESPACE.split(currentLine);
                if (params[0].equalsIgnoreCase("$body")) {
                    bodyParams = params;
                    continue;
                }
                if (params[0].equalsIgnoreCase("$anim")) {
                    this.hasAnimations = true;
                    animParams.add(params);
                    continue;
                }
                if (params[0].equalsIgnoreCase("$cdmaterials")) {
                    this.usesMaterials = true;
                    this.materialPath = params[1];
                    continue;
                }
                if (!params[0].equalsIgnoreCase("$scale")) continue;
                this.scale = Float.parseFloat(params[1]);
            }
            if (this.scale == -1.0f) {
                this.scale = 1.0f;
                if (this.resource.getPath().startsWith("models/pokemon")) {
                    Pixelmon.LOGGER.error("Model " + resource.getPath() + " did not have a scale specified!");
                }
            }
            ResourceLocation modelPath = this.getResource(resource.getNamespace(), (String)bodyParams[1]);
            this.body = new SmdModel(this, modelPath);
            for (String[] animPars : animParams) {
                String animName = animPars[1];
                ResourceLocation animPath = this.getResource(resource.getNamespace(), animPars[2]);
                this.anims.put(animName, new SmdAnimation(this, animName, animPath));
                if (!animName.equalsIgnoreCase("idle")) continue;
                this.currentAnimation = this.anims.get(animName);
            }
        }
        catch (GabeNewellException e) {
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ResourceLocation getResource(String parent, String fileName) {
        String urlAsString = this.resource.getPath();
        int lastIndex = urlAsString.lastIndexOf(47);
        String startString = urlAsString.substring(0, lastIndex);
        return new ResourceLocation(parent, startString + "/" + fileName);
    }
}

