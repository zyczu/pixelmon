/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.smd;

public class TextureCoordinate {
    public float u;
    public float v;

    public TextureCoordinate(float u, float v) {
        this.u = u;
        this.v = v;
    }
}

