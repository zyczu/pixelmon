/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.util.vector.Matrix4f
 */
package com.pixelmongenerations.client.models.smd;

import com.pixelmongenerations.client.models.smd.Bone;
import com.pixelmongenerations.client.models.smd.SmdAnimation;
import com.pixelmongenerations.core.util.helper.VectorHelper;
import java.util.ArrayList;
import org.lwjgl.util.vector.Matrix4f;

public class AnimFrame {
    public ArrayList<Matrix4f> transforms = new ArrayList();
    public ArrayList<Matrix4f> invertTransforms = new ArrayList();
    public SmdAnimation owner;
    public final int ID;

    public AnimFrame(AnimFrame anim, SmdAnimation parent) {
        this.owner = parent;
        this.ID = anim.ID;
        this.transforms = anim.transforms;
        this.invertTransforms = anim.invertTransforms;
    }

    public AnimFrame(SmdAnimation parent) {
        this.owner = parent;
        this.ID = parent.requestFrameID();
    }

    public void addTransforms(int index, Matrix4f invertedData) {
        this.transforms.add(index, invertedData);
        this.invertTransforms.add(index, Matrix4f.invert((Matrix4f)invertedData, null));
    }

    public void fixUp(int id, float degrees) {
        float radians = (float)Math.toRadians(degrees);
        Matrix4f rotator = VectorHelper.matrix4FromLocRot(0.0f, 0.0f, 0.0f, radians, 0.0f, 0.0f);
        Matrix4f.mul((Matrix4f)rotator, (Matrix4f)this.transforms.get(id), (Matrix4f)this.transforms.get(id));
        Matrix4f.mul((Matrix4f)Matrix4f.invert((Matrix4f)rotator, null), (Matrix4f)this.invertTransforms.get(id), (Matrix4f)this.invertTransforms.get(id));
    }

    public void reform() {
        for (int i = 0; i < this.transforms.size(); ++i) {
            Bone bone = this.owner.bones.get(i);
            if (bone.parent == null) continue;
            Matrix4f temp = Matrix4f.mul((Matrix4f)this.transforms.get(bone.parent.ID), (Matrix4f)this.transforms.get(i), null);
            this.transforms.set(i, temp);
            this.invertTransforms.set(i, Matrix4f.invert((Matrix4f)temp, null));
        }
    }
}

