/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.util.vector.Matrix4f
 */
package com.pixelmongenerations.client.models.smd;

import com.pixelmongenerations.client.models.smd.AnimFrame;
import com.pixelmongenerations.client.models.smd.Bone;
import com.pixelmongenerations.client.models.smd.GabeNewellException;
import com.pixelmongenerations.client.models.smd.SmdModel;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.util.helper.VectorHelper;
import dev.thecodewarrior.binarysmd.formats.SMDBinaryReader;
import dev.thecodewarrior.binarysmd.studiomdl.NodesBlock;
import dev.thecodewarrior.binarysmd.studiomdl.SMDFile;
import dev.thecodewarrior.binarysmd.studiomdl.SMDFileBlock;
import dev.thecodewarrior.binarysmd.studiomdl.SkeletonBlock;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.util.vector.Matrix4f;
import org.msgpack.core.MessagePack;
import org.msgpack.core.MessageUnpacker;

public class SmdAnimation {
    public final ValveStudioModel owner;
    public ArrayList<AnimFrame> frames = new ArrayList();
    public ArrayList<Bone> bones = new ArrayList();
    public int currentFrameIndex = 0;
    public int lastFrameIndex;
    public int totalFrames;
    public String animationName;
    private int frameIDBank = 0;

    public SmdAnimation(ValveStudioModel owner, String animationName, ResourceLocation resloc) throws GabeNewellException {
        this.owner = owner;
        this.animationName = animationName;
        this.loadSmdAnim(resloc);
        this.setBoneChildren();
        this.reform();
    }

    public SmdAnimation(SmdAnimation anim, ValveStudioModel owner) {
        this.owner = owner;
        this.animationName = anim.animationName;
        for (Bone b : anim.bones) {
            this.bones.add(new Bone(b, b.parent != null ? this.bones.get(b.parent.ID) : null, null));
        }
        this.frames.addAll(anim.frames.stream().map(f -> new AnimFrame((AnimFrame)f, this)).collect(Collectors.toList()));
        this.totalFrames = anim.totalFrames;
    }

    private void loadSmdAnim(ResourceLocation resloc) throws GabeNewellException {
        SMDFile file;
        BufferedInputStream inputStream = Pixelmon.PROXY.getStreamForResourceLocation(resloc);
        try (MessageUnpacker unpacker = MessagePack.newDefaultUnpacker(inputStream);){
            file = new SMDBinaryReader().read(unpacker);
        }
        catch (IOException e) {
            e.printStackTrace();
            return;
        }
        for (SMDFileBlock block : file.blocks) {
            if (block instanceof NodesBlock) {
                for (NodesBlock.Bone bone : ((NodesBlock)block).bones) {
                    this.parseBone(bone);
                }
                continue;
            }
            if (!(block instanceof SkeletonBlock)) continue;
            this.startParsingAnimation(((SkeletonBlock)block).keyframes);
        }
    }

    private void parseBone(NodesBlock.Bone bone) {
        int id = bone.id;
        String boneName = bone.name;
        int parentID = bone.parent;
        Bone parent = parentID >= 0 ? this.bones.get(parentID) : null;
        this.bones.add(id, new Bone(boneName, id, parent, null));
        ValveStudioModel.print(boneName);
    }

    private void startParsingAnimation(List<SkeletonBlock.Keyframe> keyframes) {
        for (SkeletonBlock.Keyframe keyframe : keyframes) {
            int currentTime = keyframe.time;
            this.frames.add(currentTime, new AnimFrame(this));
            for (SkeletonBlock.BoneState state : keyframe.states) {
                Matrix4f animated = VectorHelper.matrix4FromLocRot(state.posX, -state.posY, -state.posZ, state.rotX, -state.rotY, -state.rotZ);
                this.frames.get(currentTime).addTransforms(state.bone, animated);
            }
        }
        this.totalFrames = this.frames.size();
        ValveStudioModel.print(String.format("Total number of frames = %s", this.totalFrames));
    }

    public int requestFrameID() {
        return this.frameIDBank++;
    }

    private void setBoneChildren() {
        for (Bone theBone : this.bones) {
            this.bones.stream().filter(child -> child.parent == theBone).forEach(theBone::addChild);
        }
    }

    public void reform() {
        int rootID = this.owner.body.root.ID;
        for (AnimFrame frame : this.frames) {
            frame.fixUp(rootID, 0.0f);
            frame.reform();
        }
    }

    public void precalculateAnimation(SmdModel model) {
        for (AnimFrame frame : this.frames) {
            model.resetVerts();
            for (int j = 0; j < model.bones.size(); ++j) {
                Bone bone = model.bones.get(j);
                Matrix4f animated = frame.transforms.get(j);
                bone.preloadAnimation(frame, animated);
            }
        }
    }

    public int getNumFrames() {
        return this.frames.size();
    }

    public void setCurrentFrame(int i) {
        if (this.lastFrameIndex != i) {
            this.currentFrameIndex = i;
            this.lastFrameIndex = i;
        }
    }
}

