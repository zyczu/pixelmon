/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableMap
 *  com.google.common.collect.ImmutableMap$Builder
 *  com.google.common.collect.Lists
 *  javax.vecmath.Matrix4f
 *  javax.vecmath.Vector3f
 *  org.apache.commons.lang3.tuple.Pair
 */
package com.pixelmongenerations.client.models.items;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.pixelmongenerations.common.capabilities.curry.CurryData;
import com.pixelmongenerations.common.item.ItemCurry;
import java.util.List;
import java.util.function.Function;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ItemOverrideList;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.IModel;
import net.minecraftforge.client.model.ItemLayerModel;
import net.minecraftforge.client.model.PerspectiveMapWrapper;
import net.minecraftforge.client.model.SimpleModelState;
import net.minecraftforge.common.model.IModelPart;
import net.minecraftforge.common.model.TRSRTransformation;
import org.apache.commons.lang3.tuple.Pair;

public class ItemCurryModel
implements IBakedModel {
    public static final ModelResourceLocation modelResourceLocation = new ModelResourceLocation("pixelmon:curry", "inventory");
    IBakedModel iBakedModel;
    private final ImmutableMap<ItemCameraTransforms.TransformType, TRSRTransformation> transforms = ItemCurryModel.itemTransforms();
    private static final TRSRTransformation flipX = new TRSRTransformation(null, null, new Vector3f(-1.0f, 1.0f, 1.0f), null);

    public ItemCurryModel(IBakedModel iBakedModel) {
        this.iBakedModel = iBakedModel;
    }

    @Override
    public List<BakedQuad> getQuads(IBlockState state, EnumFacing side, long rand) {
        return this.iBakedModel.getQuads(state, side, rand);
    }

    @Override
    public boolean isAmbientOcclusion() {
        return this.iBakedModel.isAmbientOcclusion();
    }

    @Override
    public boolean isGui3d() {
        return false;
    }

    @Override
    public boolean isBuiltInRenderer() {
        return false;
    }

    @Override
    public TextureAtlasSprite getParticleTexture() {
        return this.iBakedModel.getParticleTexture();
    }

    @Override
    public ItemCameraTransforms getItemCameraTransforms() {
        return ItemCameraTransforms.DEFAULT;
    }

    @Override
    public ItemOverrideList getOverrides() {
        return new OverrideList();
    }

    @Override
    public Pair<? extends IBakedModel, Matrix4f> handlePerspective(ItemCameraTransforms.TransformType cameraTransformType) {
        return PerspectiveMapWrapper.handlePerspective((IBakedModel)this, this.transforms, cameraTransformType);
    }

    private static ImmutableMap<ItemCameraTransforms.TransformType, TRSRTransformation> itemTransforms() {
        TRSRTransformation thirdperson = ItemCurryModel.get(0.0f, 3.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.55f);
        TRSRTransformation firstperson = ItemCurryModel.get(1.13f, 3.2f, 1.13f, 0.0f, -90.0f, 25.0f, 0.68f);
        ImmutableMap.Builder builder = ImmutableMap.builder();
        builder.put((Object)ItemCameraTransforms.TransformType.GROUND, (Object)ItemCurryModel.get(0.0f, 2.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f));
        builder.put((Object)ItemCameraTransforms.TransformType.HEAD, (Object)ItemCurryModel.get(0.0f, 13.0f, 7.0f, 0.0f, 180.0f, 0.0f, 1.0f));
        builder.put((Object)ItemCameraTransforms.TransformType.THIRD_PERSON_RIGHT_HAND, (Object)thirdperson);
        builder.put((Object)ItemCameraTransforms.TransformType.THIRD_PERSON_LEFT_HAND, (Object)ItemCurryModel.leftify(thirdperson));
        builder.put((Object)ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND, (Object)firstperson);
        builder.put((Object)ItemCameraTransforms.TransformType.FIRST_PERSON_LEFT_HAND, (Object)ItemCurryModel.leftify(firstperson));
        return builder.build();
    }

    private static TRSRTransformation get(float tx, float ty, float tz, float ax, float ay, float az, float s) {
        return TRSRTransformation.blockCenterToCorner(new TRSRTransformation(new Vector3f(tx / 16.0f, ty / 16.0f, tz / 16.0f), TRSRTransformation.quatFromXYZDegrees(new Vector3f(ax, ay, az)), new Vector3f(s, s, s), null));
    }

    private static TRSRTransformation leftify(TRSRTransformation transform) {
        return TRSRTransformation.blockCenterToCorner(flipX.compose(TRSRTransformation.blockCornerToCenter(transform)).compose(flipX));
    }

    class OverrideList
    extends ItemOverrideList {
        public OverrideList() {
            super(Lists.newArrayList());
        }

        @Override
        public IBakedModel handleItemState(IBakedModel originalModel, ItemStack stack, World world, EntityLivingBase entity) {
            CurryData data = ItemCurry.getData(stack);
            ItemCurryModel model = (ItemCurryModel)originalModel;
            Function<ResourceLocation, TextureAtlasSprite> textureGetter = l -> Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(l.toString());
            ImmutableMap texMap = ImmutableMap.of((Object)"layer0", (Object)data.getResourceLocation().toString());
            IModel iModel = ItemLayerModel.INSTANCE.retexture(texMap);
            ItemCurryModel.this.iBakedModel = iModel.bake(new SimpleModelState((ImmutableMap<? extends IModelPart, TRSRTransformation>)model.transforms), DefaultVertexFormats.ITEM, textureGetter);
            return ItemCurryModel.this.iBakedModel;
        }
    }
}

