/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models;

import com.pixelmongenerations.client.models.ModelCustomWrapper;
import com.pixelmongenerations.client.models.MysteriousRingModelSmd;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.animations.SkeletonBase;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.client.models.smd.ValveStudioModelLoader;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModel;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class MysteriousRingSmdFactory {
    protected float xRotation;
    protected float yRotation = 24.0f;
    protected float zRotation;
    protected float rotateAngleX = -1.5707964f;
    protected float rotateAngleY;
    protected float movementThreshold = 0.3f;
    protected float animationIncrement = 1.0f;
    private float scale = 1.0f;

    public MysteriousRingSmdFactory setYRotation(float yRotation) {
        this.yRotation = yRotation;
        return this;
    }

    public MysteriousRingSmdFactory setZRotation(float zRotation) {
        this.zRotation = zRotation;
        return this;
    }

    public MysteriousRingSmdFactory setMovementThreshold(float threshold) {
        this.movementThreshold = threshold;
        return this;
    }

    public MysteriousRingSmdFactory setRotateAngleX(float rotateAngleX) {
        this.rotateAngleX = rotateAngleX;
        return this;
    }

    public MysteriousRingSmdFactory setRotateAngleY(float rotateAngleY) {
        this.rotateAngleY = rotateAngleY;
        return this;
    }

    public MysteriousRingSmdFactory setAnimationIncrement(float animationIncrement) {
        this.animationIncrement = animationIncrement;
        return this;
    }

    public MysteriousRingSmdFactory setScale(float scale) {
        this.scale = scale;
        return this;
    }

    @SideOnly(value=Side.CLIENT)
    public MysteriousRingModelSmd createModel() {
        Impl impl = new Impl((ValveStudioModel)this.loadModel());
        impl.body.setRotationPoint(this.xRotation, this.yRotation, this.zRotation);
        impl.body.rotateAngleX = this.rotateAngleX;
        impl.body.rotateAngleY = this.rotateAngleY;
        impl.movementThreshold = this.movementThreshold;
        impl.animationIncrement = this.animationIncrement;
        impl.scale = this.scale;
        return impl;
    }

    public IModel loadModel() {
        try {
            ResourceLocation rl = new ResourceLocation("pixelmon:models/mysterious_ring/mysterious_ring.pqc");
            if (ValveStudioModelLoader.instance.accepts(rl)) {
                return ValveStudioModelLoader.instance.loadModel(rl);
            }
            System.out.println("Could not load the model: mysterious_ring.pqc");
        }
        catch (Exception var2) {
            System.out.println("Could not load the model: mysterious_ring.pqc");
            var2.printStackTrace();
        }
        return null;
    }

    public float getScale() {
        return this.scale;
    }

    @SideOnly(value=Side.CLIENT)
    public static class Impl
    extends MysteriousRingModelSmd {
        PixelmonModelRenderer body;

        Impl(ValveStudioModel valveStudioModel) {
            this.theModel = valveStudioModel;
            this.body = new PixelmonModelRenderer(this, "body");
            this.body.addCustomModel(new ModelCustomWrapper(valveStudioModel));
            this.skeleton = new SkeletonBase(this.body);
        }

        @Override
        @SideOnly(value=Side.CLIENT)
        public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
            super.render(entity, f, f1, f2, f3, f4, f5);
            this.body.render(f5);
        }
    }
}

