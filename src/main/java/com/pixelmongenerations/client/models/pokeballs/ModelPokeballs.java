/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokeballs;

import com.pixelmongenerations.client.models.ModelCustomWrapper;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.PixelmonModelSmd;
import com.pixelmongenerations.client.models.smd.SmdAnimation;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.common.entity.pixelmon.helpers.animation.IncrementingVariable;
import com.pixelmongenerations.common.entity.pokeballs.EntityPokeBall;
import com.pixelmongenerations.core.enums.EnumCustomModel;
import net.minecraft.entity.Entity;

public class ModelPokeballs
extends PixelmonModelSmd {
    PixelmonModelRenderer body = new PixelmonModelRenderer(this, "body");

    public ModelPokeballs(EnumCustomModel model) {
        ValveStudioModel smd = (ValveStudioModel)model.getModel();
        this.body.addCustomModel(new ModelCustomWrapper(smd));
        this.body.setRotationPoint(0.0f, 21.442f, 0.0f);
        this.theModel = smd;
    }

    public void doAnimation(Entity entity) {
        if (entity == null) {
            return;
        }
        EntityPokeBall pokeball = (EntityPokeBall)entity;
        if (!pokeball.getAnimation().equals(pokeball.lastAnim)) {
            if (pokeball.inc != null) {
                pokeball.inc.value = 0.0f;
            }
            pokeball.lastAnim = pokeball.getAnimation();
        }
        if (pokeball.inc == null) {
            pokeball.inc = new IncrementingVariable(this.animationIncrement, 2.14748365E9f);
        } else {
            pokeball.inc.increment = this.animationIncrement;
        }
        if (this.theModel.hasAnimations() && this.theModel.currentAnimation != null) {
            SmdAnimation theAnim = this.theModel.currentAnimation;
            int frame = pokeball.inc.value < (float)theAnim.totalFrames ? (int)(pokeball.inc.value % (float)theAnim.totalFrames) : theAnim.totalFrames - 1;
            theAnim.setCurrentFrame(frame);
            this.theModel.animate();
        }
    }

    public void render(Entity entity, float scale) {
        this.render(entity, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, scale);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        this.body.render(f5);
    }
}

