/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.plates;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelIngot
extends ModelBase {
    ModelRenderer Ingot1;
    ModelRenderer Ingot2;
    ModelRenderer Ingot3;
    ModelRenderer Ingot4;

    public ModelIngot() {
        this.textureWidth = 32;
        this.textureHeight = 32;
        this.Ingot1 = new ModelRenderer(this, 0, 0);
        this.Ingot1.addBox(-13.9f, 0.0f, -1.5f, 5, 1, 3);
        this.Ingot1.setRotationPoint(6.5f, 12.0f, 0.0f);
        this.Ingot1.setTextureSize(32, 32);
        this.Ingot1.mirror = true;
        this.setRotation(this.Ingot1, 0.0f, 0.0f, 0.0f);
        this.Ingot2 = new ModelRenderer(this, 0, 9);
        this.Ingot2.addBox(-9.4f, 0.0f, -2.0f, 1, 1, 4);
        this.Ingot2.setRotationPoint(6.5f, 13.0f, 0.0f);
        this.Ingot2.setTextureSize(32, 32);
        this.Ingot2.mirror = true;
        this.setRotation(this.Ingot2, 0.0f, 0.0f, 0.0f);
        this.Ingot3 = new ModelRenderer(this, 0, 4);
        this.Ingot3.addBox(-13.9f, 0.0f, -2.0f, 5, 1, 4);
        this.Ingot3.setRotationPoint(6.5f, 13.0f, 0.0f);
        this.Ingot3.setTextureSize(32, 32);
        this.Ingot3.mirror = true;
        this.setRotation(this.Ingot3, 0.0f, 0.0f, 0.0f);
        this.Ingot4 = new ModelRenderer(this, 0, 9);
        this.Ingot4.addBox(-14.4f, 0.0f, -2.0f, 1, 1, 4);
        this.Ingot4.setRotationPoint(6.5f, 13.0f, 0.0f);
        this.Ingot4.setTextureSize(32, 32);
        this.Ingot4.mirror = true;
        this.setRotation(this.Ingot4, 0.0f, 0.0f, 0.0f);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.Ingot1.render(f5);
        this.Ingot2.render(f5);
        this.Ingot3.render(f5);
        this.Ingot4.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    @Override
    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    }

    public void renderModel(float f5) {
        this.Ingot1.render(f5);
        this.Ingot2.render(f5);
        this.Ingot3.render(f5);
        this.Ingot4.render(f5);
    }
}

