/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.blocks;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelVendingMachine
extends ModelBase {
    ModelRenderer Shape1;
    ModelRenderer Shape2;
    ModelRenderer Shape3;
    ModelRenderer Shape4;
    ModelRenderer Shape5;

    public ModelVendingMachine() {
        this.textureWidth = 128;
        this.textureHeight = 256;
        this.Shape1 = new ModelRenderer(this, 0, 100);
        this.Shape1.addBox(0.0f, -16.0f, 0.0f, 14, 14, 16);
        this.Shape1.setRotationPoint(-7.0f, 9.0f, -6.0f);
        this.Shape1.setTextureSize(128, 256);
        this.Shape1.mirror = true;
        this.setRotation(this.Shape1, 0.0f, 0.0f, 0.0f);
        this.Shape2 = new ModelRenderer(this, 0, 32);
        this.Shape2.addBox(0.0f, 0.0f, 0.0f, 15, 1, 15);
        this.Shape2.setRotationPoint(-7.5f, 23.0f, -7.5f);
        this.Shape2.setTextureSize(128, 256);
        this.Shape2.mirror = true;
        this.setRotation(this.Shape2, 0.0f, 0.0f, 0.0f);
        this.Shape3 = new ModelRenderer(this, 0, 50);
        this.Shape3.addBox(0.0f, -32.0f, 0.0f, 16, 16, 16);
        this.Shape3.setRotationPoint(-8.0f, 23.0f, -8.0f);
        this.Shape3.setTextureSize(128, 256);
        this.Shape3.mirror = true;
        this.setRotation(this.Shape3, 0.0f, 0.0f, 0.0f);
        this.Shape4 = new ModelRenderer(this, 0, 0);
        this.Shape4.addBox(0.0f, -16.0f, 0.0f, 16, 16, 16);
        this.Shape4.setRotationPoint(-8.0f, 23.0f, -8.0f);
        this.Shape4.setTextureSize(128, 256);
        this.Shape4.mirror = true;
        this.setRotation(this.Shape4, 0.0f, 0.0f, 0.0f);
        this.Shape5 = new ModelRenderer(this, 0, 100);
        this.Shape5.addBox(0.0f, -16.0f, 0.0f, 14, 14, 16);
        this.Shape5.setRotationPoint(-7.0f, 23.0f, -6.0f);
        this.Shape5.setTextureSize(128, 256);
        this.Shape5.mirror = true;
        this.setRotation(this.Shape5, 0.0f, 0.0f, 0.0f);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.Shape1.render(f5);
        this.Shape2.render(f5);
        this.Shape3.render(f5);
        this.Shape4.render(f5);
        this.Shape5.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    @Override
    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    }
}

