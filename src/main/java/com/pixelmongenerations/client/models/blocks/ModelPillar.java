/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.blocks;

import com.pixelmongenerations.client.models.IPixelmonModel;
import com.pixelmongenerations.client.models.ModelCustomWrapper;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.blocks.ModelEntityBlock;
import com.pixelmongenerations.common.block.decorative.BlockContainerPlus;
import com.pixelmongenerations.common.block.decorative.BlockFancyPillar;
import com.pixelmongenerations.common.block.enums.EnumAxis;
import com.pixelmongenerations.common.block.tileEntities.TileEntityDecorativeBase;
import com.pixelmongenerations.core.enums.EnumCustomModel;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class ModelPillar
extends ModelEntityBlock {
    private final ResourceLocation pillar;
    private final ResourceLocation pillarDamaged;
    PixelmonModelRenderer platform = new PixelmonModelRenderer(this);
    PixelmonModelRenderer column = new PixelmonModelRenderer(this);
    PixelmonModelRenderer fracturedBottom = new PixelmonModelRenderer(this);
    PixelmonModelRenderer fracturedTop = new PixelmonModelRenderer(this);

    public ModelPillar() {
        this("");
    }

    public ModelPillar(String name) {
        name = name.isEmpty() ? name : name + "_";
        this.pillar = new ResourceLocation(String.format("pixelmon:textures/blocks/%spillar.png", name));
        this.pillarDamaged = new ResourceLocation(String.format("pixelmon:textures/blocks/%spillar_fractured.png", name));
        this.platform.addCustomModel(new ModelCustomWrapper((IPixelmonModel)((Object)EnumCustomModel.PillarPlatform.getModel())));
        this.column.addCustomModel(new ModelCustomWrapper((IPixelmonModel)((Object)EnumCustomModel.PillarColumn.getModel())));
        this.fracturedBottom.addCustomModel(new ModelCustomWrapper((IPixelmonModel)((Object)EnumCustomModel.PillarColumnFracturedBottom.getModel())));
        this.fracturedTop.addCustomModel(new ModelCustomWrapper((IPixelmonModel)((Object)EnumCustomModel.PillarColumnFracturedTop.getModel())));
    }

    @Override
    public void renderTileEntity(TileEntityDecorativeBase tileEnt, float scale) {
        BlockFancyPillar theBlock = (BlockFancyPillar)tileEnt.getBlockType();
        IBlockState state = tileEnt.getWorld().getBlockState(tileEnt.getPos());
        BlockFancyPillar.Connections connections = theBlock.getConnections(tileEnt.getWorld(), tileEnt.getPos(), state);
        GlStateManager.pushMatrix();
        GlStateManager.enableRescaleNormal();
        if (!theBlock.getIsDamaged(state)) {
            this.renderStandardPillar(connections, scale);
        } else {
            this.renderDamagedPillar(scale, tileEnt);
        }
        GlStateManager.popMatrix();
    }

    public void renderStandardPillar(BlockFancyPillar.Connections connections, float scale) {
        Minecraft.getMinecraft().renderEngine.bindTexture(this.pillar);
        this.column.render(scale);
        if (!connections.top) {
            this.platform.setRotationPoint(0.0f, 6.5f, 0.0f);
            this.platform.render(scale);
        }
        if (!connections.bottom) {
            this.platform.setRotationPoint(0.0f, -6.5f, 0.0f);
            this.platform.render(scale);
        }
    }

    public void renderDamagedPillarInv(float scale) {
        Minecraft.getMinecraft().renderEngine.bindTexture(this.pillarDamaged);
        this.platform.setRotationPoint(0.0f, 6.5f, 0.0f);
        this.platform.render(scale);
        this.platform.setRotationPoint(0.0f, -6.5f, 0.0f);
        this.platform.render(scale);
        this.fracturedBottom.render(scale);
        this.fracturedTop.render(scale);
    }

    public void renderDamagedPillar(float scale, TileEntityDecorativeBase tileEnt) {
        Boolean topState;
        Boolean bottomState;
        Minecraft.getMinecraft().renderEngine.bindTexture(this.pillarDamaged);
        BlockFancyPillar theBlock = (BlockFancyPillar)tileEnt.getBlockType();
        IBlockState state = tileEnt.getWorld().getBlockState(tileEnt.getPos());
        boolean renderTopPlatform = false;
        boolean renderBotPlatform = false;
        boolean renderTopDamaged = false;
        boolean renderBotDamaged = false;
        EnumAxis axis = state.getValue(BlockContainerPlus.AXIS);
        int index = axis.ordinal() - 1;
        if (index < 0) {
            index = 0;
        }
        if ((bottomState = theBlock.isEnd(tileEnt.getWorld(), tileEnt.getPos(), BlockFancyPillar.DOWN_VALS[index])) != null) {
            if (!bottomState.booleanValue()) {
                renderBotPlatform = true;
            } else {
                renderBotDamaged = true;
            }
        }
        if ((topState = theBlock.isEnd(tileEnt.getWorld(), tileEnt.getPos(), BlockFancyPillar.UP_VALS[index])) != null) {
            if (!topState.booleanValue()) {
                renderTopPlatform = true;
            } else {
                renderTopDamaged = true;
            }
        }
        if (renderBotDamaged) {
            this.fracturedTop.render(scale);
        }
        if (renderTopDamaged) {
            this.fracturedBottom.render(scale);
        }
        if (renderTopPlatform) {
            this.platform.setRotationPoint(0.0f, -6.5f, 0.0f);
            this.platform.render(scale);
            if (!renderBotDamaged) {
                this.column.render(scale);
            }
        }
        if (renderBotPlatform) {
            this.platform.setRotationPoint(0.0f, 6.5f, 0.0f);
            this.platform.render(scale);
            if (!renderTopDamaged) {
                this.column.render(scale);
            }
        }
        if (!(renderTopDamaged || renderBotDamaged || renderTopPlatform || renderBotPlatform)) {
            this.column.render(scale);
        }
    }
}

