/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.blocks;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelHealer
extends ModelBase {
    ModelRenderer HealerBase;
    ModelRenderer HealerLeg1;
    ModelRenderer HealerLeg2;
    ModelRenderer HealerLeg3;
    ModelRenderer HealerLeg4;
    ModelRenderer Mainplate;
    ModelRenderer HealerRightSide;
    ModelRenderer HealerLeftSide;
    ModelRenderer BackplateBase;
    ModelRenderer BackplateTop;
    ModelRenderer FrontCurveBase;
    ModelRenderer FrontCurveRight1;
    ModelRenderer FrontCurveRight2;
    ModelRenderer FrontCurveLeft1;
    ModelRenderer FrontCurveLeft2;
    ModelRenderer BackCurveBase;
    ModelRenderer BackCurveRight1;
    ModelRenderer BackCurveRight2;
    ModelRenderer BackCurveLeft1;
    ModelRenderer BackCurveLeft2;

    public ModelHealer() {
        this.textureWidth = 128;
        this.textureHeight = 128;
        this.HealerBase = new ModelRenderer(this, 0, 0);
        this.HealerBase.addBox(0.0f, 0.0f, 0.0f, 14, 10, 14);
        this.HealerBase.setRotationPoint(-7.0f, 14.0f, -7.0f);
        this.HealerBase.setTextureSize(128, 128);
        this.HealerBase.mirror = true;
        this.setRotation(this.HealerBase, 0.0f, 0.0f, 0.0f);
        this.HealerLeg1 = new ModelRenderer(this, 57, 0);
        this.HealerLeg1.addBox(0.0f, 0.0f, 0.0f, 2, 10, 1);
        this.HealerLeg1.setRotationPoint(-7.0f, 14.0f, -8.0f);
        this.HealerLeg1.setTextureSize(128, 128);
        this.HealerLeg1.mirror = true;
        this.setRotation(this.HealerLeg1, 0.0f, 0.0f, 0.0f);
        this.HealerLeg2 = new ModelRenderer(this, 64, 0);
        this.HealerLeg2.addBox(0.0f, 0.0f, 0.0f, 2, 10, 1);
        this.HealerLeg2.setRotationPoint(5.0f, 14.0f, -8.0f);
        this.HealerLeg2.setTextureSize(128, 128);
        this.HealerLeg2.mirror = true;
        this.setRotation(this.HealerLeg2, 0.0f, 0.0f, 0.0f);
        this.HealerLeg3 = new ModelRenderer(this, 57, 12);
        this.HealerLeg3.addBox(0.0f, 0.0f, 0.0f, 2, 10, 1);
        this.HealerLeg3.setRotationPoint(5.0f, 14.0f, 7.0f);
        this.HealerLeg3.setTextureSize(128, 128);
        this.HealerLeg3.mirror = true;
        this.setRotation(this.HealerLeg3, 0.0f, 0.0f, 0.0f);
        this.HealerLeg4 = new ModelRenderer(this, 64, 12);
        this.HealerLeg4.addBox(0.0f, 0.0f, 0.0f, 2, 10, 1);
        this.HealerLeg4.setRotationPoint(-7.0f, 14.0f, 7.0f);
        this.HealerLeg4.setTextureSize(128, 128);
        this.HealerLeg4.mirror = true;
        this.setRotation(this.HealerLeg4, 0.0f, 0.0f, 0.0f);
        this.Mainplate = new ModelRenderer(this, 72, 3);
        this.Mainplate.addBox(0.0f, 0.0f, 0.0f, 10, 2, 14);
        this.Mainplate.setRotationPoint(-5.0f, 12.5f, -7.0f);
        this.Mainplate.setTextureSize(128, 128);
        this.Mainplate.mirror = true;
        this.setRotation(this.Mainplate, 0.0f, 0.0f, 0.0f);
        this.HealerRightSide = new ModelRenderer(this, 0, 25);
        this.HealerRightSide.addBox(0.0f, 0.0f, 0.0f, 3, 3, 16);
        this.HealerRightSide.setRotationPoint(-7.0f, 14.0f, -8.0f);
        this.HealerRightSide.setTextureSize(128, 128);
        this.HealerRightSide.mirror = true;
        this.setRotation(this.HealerRightSide, 0.0f, 0.0f, -0.7853982f);
        this.HealerLeftSide = new ModelRenderer(this, 0, 45);
        this.HealerLeftSide.addBox(0.0f, 0.0f, 0.0f, 3, 3, 16);
        this.HealerLeftSide.setRotationPoint(7.0f, 14.0f, -8.0f);
        this.HealerLeftSide.setTextureSize(128, 128);
        this.HealerLeftSide.mirror = true;
        this.setRotation(this.HealerLeftSide, 0.0f, 0.0f, 2.356194f);
        this.BackplateBase = new ModelRenderer(this, 72, 21);
        this.BackplateBase.addBox(0.0f, 0.0f, 0.0f, 10, 3, 2);
        this.BackplateBase.setRotationPoint(-5.0f, 8.0f, 6.0f);
        this.BackplateBase.setTextureSize(128, 128);
        this.BackplateBase.mirror = true;
        this.setRotation(this.BackplateBase, 0.0f, 0.0f, 0.0f);
        this.BackplateTop = new ModelRenderer(this, 97, 21);
        this.BackplateTop.addBox(0.0f, 0.0f, 0.0f, 8, 1, 2);
        this.BackplateTop.setRotationPoint(-4.0f, 7.0f, 6.0f);
        this.BackplateTop.setTextureSize(128, 128);
        this.BackplateTop.mirror = true;
        this.setRotation(this.BackplateTop, 0.0f, 0.0f, 0.0f);
        this.FrontCurveBase = new ModelRenderer(this, 50, 32);
        this.FrontCurveBase.addBox(0.0f, 0.0f, 0.0f, 12, 3, 2);
        this.FrontCurveBase.setRotationPoint(-6.0f, 11.0f, -8.0f);
        this.FrontCurveBase.setTextureSize(128, 128);
        this.FrontCurveBase.mirror = true;
        this.setRotation(this.FrontCurveBase, 0.0f, 0.0f, 0.0f);
        this.FrontCurveRight1 = new ModelRenderer(this, 56, 38);
        this.FrontCurveRight1.addBox(0.0f, 0.0f, 0.0f, 2, 2, 2);
        this.FrontCurveRight1.setRotationPoint(-7.0f, 12.0f, -8.0f);
        this.FrontCurveRight1.setTextureSize(128, 128);
        this.FrontCurveRight1.mirror = true;
        this.setRotation(this.FrontCurveRight1, 0.0f, 0.0f, -0.4712389f);
        this.FrontCurveRight2 = new ModelRenderer(this, 46, 38);
        this.FrontCurveRight2.addBox(0.0f, 0.0f, 0.0f, 2, 2, 2);
        this.FrontCurveRight2.setRotationPoint(-7.0f, 14.0f, -8.0f);
        this.FrontCurveRight2.setTextureSize(128, 128);
        this.FrontCurveRight2.mirror = true;
        this.setRotation(this.FrontCurveRight2, 0.0f, 0.0f, -1.570796f);
        this.FrontCurveLeft1 = new ModelRenderer(this, 66, 38);
        this.FrontCurveLeft1.addBox(0.0f, -2.0f, 0.0f, 2, 2, 2);
        this.FrontCurveLeft1.setRotationPoint(7.0f, 12.0f, -8.0f);
        this.FrontCurveLeft1.setTextureSize(128, 128);
        this.FrontCurveLeft1.mirror = true;
        this.setRotation(this.FrontCurveLeft1, 0.0f, 0.0f, -2.6529f);
        this.FrontCurveLeft2 = new ModelRenderer(this, 76, 38);
        this.FrontCurveLeft2.addBox(0.0f, -2.0f, 0.0f, 2, 2, 2);
        this.FrontCurveLeft2.setRotationPoint(7.0f, 14.0f, -8.0f);
        this.FrontCurveLeft2.setTextureSize(128, 128);
        this.FrontCurveLeft2.mirror = true;
        this.setRotation(this.FrontCurveLeft2, 0.0f, 0.0f, -1.570796f);
        this.BackCurveBase = new ModelRenderer(this, 50, 44);
        this.BackCurveBase.addBox(0.0f, 0.0f, 0.0f, 12, 3, 2);
        this.BackCurveBase.setRotationPoint(-6.0f, 11.0f, 6.0f);
        this.BackCurveBase.setTextureSize(128, 128);
        this.BackCurveBase.mirror = true;
        this.setRotation(this.BackCurveBase, 0.0f, 0.0f, 0.0f);
        this.BackCurveRight1 = new ModelRenderer(this, 56, 50);
        this.BackCurveRight1.addBox(0.0f, 0.0f, 0.0f, 2, 2, 2);
        this.BackCurveRight1.setRotationPoint(-7.0f, 12.0f, 6.0f);
        this.BackCurveRight1.setTextureSize(128, 128);
        this.BackCurveRight1.mirror = true;
        this.setRotation(this.BackCurveRight1, 0.0f, 0.0f, -0.4712389f);
        this.BackCurveRight2 = new ModelRenderer(this, 46, 50);
        this.BackCurveRight2.addBox(0.0f, 0.0f, 0.0f, 2, 2, 2);
        this.BackCurveRight2.setRotationPoint(-7.0f, 14.0f, 6.0f);
        this.BackCurveRight2.setTextureSize(128, 128);
        this.BackCurveRight2.mirror = true;
        this.setRotation(this.BackCurveRight2, 0.0f, 0.0f, -1.570796f);
        this.BackCurveLeft1 = new ModelRenderer(this, 66, 50);
        this.BackCurveLeft1.addBox(0.0f, -2.0f, 0.0f, 2, 2, 2);
        this.BackCurveLeft1.setRotationPoint(7.0f, 12.0f, 6.0f);
        this.BackCurveLeft1.setTextureSize(128, 128);
        this.BackCurveLeft1.mirror = true;
        this.setRotation(this.BackCurveLeft1, 0.0f, 0.0f, -2.6529f);
        this.BackCurveLeft2 = new ModelRenderer(this, 76, 50);
        this.BackCurveLeft2.addBox(0.0f, -2.0f, 0.0f, 2, 2, 2);
        this.BackCurveLeft2.setRotationPoint(7.0f, 14.0f, 6.0f);
        this.BackCurveLeft2.setTextureSize(128, 128);
        this.BackCurveLeft2.mirror = true;
        this.setRotation(this.BackCurveLeft2, 0.0f, 0.0f, -1.570796f);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.HealerBase.render(f5);
        this.HealerLeg1.render(f5);
        this.HealerLeg2.render(f5);
        this.HealerLeg3.render(f5);
        this.HealerLeg4.render(f5);
        this.Mainplate.render(f5);
        this.HealerRightSide.render(f5);
        this.HealerLeftSide.render(f5);
        this.BackplateBase.render(f5);
        this.BackplateTop.render(f5);
        this.FrontCurveBase.render(f5);
        this.FrontCurveRight1.render(f5);
        this.FrontCurveRight2.render(f5);
        this.FrontCurveLeft1.render(f5);
        this.FrontCurveLeft2.render(f5);
        this.BackCurveBase.render(f5);
        this.BackCurveRight1.render(f5);
        this.BackCurveRight2.render(f5);
        this.BackCurveLeft1.render(f5);
        this.BackCurveLeft2.render(f5);
    }

    public void renderModel(float f5) {
        this.HealerBase.render(f5);
        this.HealerLeg1.render(f5);
        this.HealerLeg2.render(f5);
        this.HealerLeg3.render(f5);
        this.HealerLeg4.render(f5);
        this.Mainplate.render(f5);
        this.HealerRightSide.render(f5);
        this.HealerLeftSide.render(f5);
        this.BackplateBase.render(f5);
        this.BackplateTop.render(f5);
        this.FrontCurveBase.render(f5);
        this.FrontCurveRight1.render(f5);
        this.FrontCurveRight2.render(f5);
        this.FrontCurveLeft1.render(f5);
        this.FrontCurveLeft2.render(f5);
        this.BackCurveBase.render(f5);
        this.BackCurveRight1.render(f5);
        this.BackCurveRight2.render(f5);
        this.BackCurveLeft1.render(f5);
        this.BackCurveLeft2.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }
}

