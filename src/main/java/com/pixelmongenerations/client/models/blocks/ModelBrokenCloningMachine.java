/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.blocks;

import com.pixelmongenerations.client.models.ModelCustomWrapper;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.blocks.ModelEntityBlock;
import com.pixelmongenerations.client.models.obj.ObjLoader;
import com.pixelmongenerations.common.block.tileEntities.TileEntityDecorativeBase;
import net.minecraft.util.ResourceLocation;

public class ModelBrokenCloningMachine
extends ModelEntityBlock {
    ResourceLocation baseLoc = new ResourceLocation("pixelmon:models/blocks/cloning_machine/broken/brokenclonerbase.obj");
    ResourceLocation glassLoc = new ResourceLocation("pixelmon:models/blocks/cloning_machine/broken/brokenclonerglass.obj");
    PixelmonModelRenderer machine = new PixelmonModelRenderer(this);
    PixelmonModelRenderer glass;
    boolean travDown = true;

    public ModelBrokenCloningMachine() {
        this.machine.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(this.baseLoc)));
        this.glass = new PixelmonModelRenderer(this);
        this.glass.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(this.glassLoc)));
        this.glass.setTransparent(1.0f);
    }

    @Override
    public void renderTileEntity(TileEntityDecorativeBase tileEnt, float scale) {
        this.machine.render(0.0625f);
    }

    public void renderGlass() {
        this.glass.render(0.0625f);
    }
}

