/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.blocks;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelChair
extends ModelBase {
    ModelRenderer ChairBackTop;
    ModelRenderer LegFrontRight;
    ModelRenderer Cushion;
    ModelRenderer LegFrontLeft;
    ModelRenderer LegBackLeft;
    ModelRenderer LegBackRight;
    ModelRenderer ChairBackBottom;
    ModelRenderer BarRight;
    ModelRenderer BarLeft;
    ModelRenderer ChairBackSlantRight;
    ModelRenderer ChairBackSlantLeft;
    ModelRenderer ChairBackMid;

    public ModelChair() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.ChairBackTop = new ModelRenderer(this, 10, 10);
        this.ChairBackTop.addBox(-4.0f, -2.0f, 4.0f, 8, 4, 1);
        this.ChairBackTop.setRotationPoint(0.0f, 8.0f, 0.0f);
        this.ChairBackTop.setTextureSize(64, 32);
        this.ChairBackTop.mirror = true;
        this.setRotation(this.ChairBackTop, 0.0f, 0.0f, 0.0f);
        this.LegFrontRight = new ModelRenderer(this, 0, 0);
        this.LegFrontRight.addBox(-4.5f, 11.0f, -5.5f, 2, 5, 2);
        this.LegFrontRight.setRotationPoint(0.0f, 8.0f, 0.0f);
        this.LegFrontRight.setTextureSize(64, 32);
        this.LegFrontRight.mirror = true;
        this.setRotation(this.LegFrontRight, 0.0f, 0.0f, 0.0f);
        this.Cushion = new ModelRenderer(this, 0, 20);
        this.Cushion.addBox(-5.0f, 9.0f, -6.0f, 10, 2, 10);
        this.Cushion.setRotationPoint(0.0f, 8.0f, 0.0f);
        this.Cushion.setTextureSize(64, 32);
        this.Cushion.mirror = true;
        this.setRotation(this.Cushion, 0.0f, 0.0f, 0.0f);
        this.LegFrontLeft = new ModelRenderer(this, 0, 0);
        this.LegFrontLeft.addBox(2.5f, 11.0f, -5.5f, 2, 5, 2);
        this.LegFrontLeft.setRotationPoint(0.0f, 8.0f, 0.0f);
        this.LegFrontLeft.setTextureSize(64, 32);
        this.LegFrontLeft.mirror = true;
        this.setRotation(this.LegFrontLeft, 0.0f, 0.0f, 0.0f);
        this.LegBackLeft = new ModelRenderer(this, 0, 0);
        this.LegBackLeft.addBox(2.5f, 11.0f, 1.5f, 2, 5, 2);
        this.LegBackLeft.setRotationPoint(0.0f, 8.0f, 0.0f);
        this.LegBackLeft.setTextureSize(64, 32);
        this.LegBackLeft.mirror = true;
        this.setRotation(this.LegBackLeft, 0.0f, 0.0f, 0.0f);
        this.LegBackRight = new ModelRenderer(this, 0, 0);
        this.LegBackRight.addBox(-4.5f, 11.0f, 1.5f, 2, 5, 2);
        this.LegBackRight.setRotationPoint(0.0f, 8.0f, 0.0f);
        this.LegBackRight.setTextureSize(64, 32);
        this.LegBackRight.mirror = true;
        this.setRotation(this.LegBackRight, 0.0f, 0.0f, 0.0f);
        this.ChairBackBottom = new ModelRenderer(this, 9, 15);
        this.ChairBackBottom.addBox(-5.0f, 4.0f, 4.0f, 10, 4, 1);
        this.ChairBackBottom.setRotationPoint(0.0f, 8.0f, 0.0f);
        this.ChairBackBottom.setTextureSize(64, 32);
        this.ChairBackBottom.mirror = true;
        this.setRotation(this.ChairBackBottom, 0.0f, 0.0f, 0.0f);
        this.BarRight = new ModelRenderer(this, 0, 11);
        this.BarRight.addBox(-4.0f, 8.0f, -5.0f, 1, 1, 3);
        this.BarRight.setRotationPoint(0.0f, 8.0f, 0.0f);
        this.BarRight.setTextureSize(64, 32);
        this.BarRight.mirror = true;
        this.setRotation(this.BarRight, 0.7853982f, 0.0f, 0.0f);
        this.BarLeft = new ModelRenderer(this, 0, 11);
        this.BarLeft.addBox(3.0f, 8.0f, -5.0f, 1, 1, 3);
        this.BarLeft.setRotationPoint(0.0f, 8.0f, 0.0f);
        this.BarLeft.setTextureSize(64, 32);
        this.BarLeft.mirror = true;
        this.setRotation(this.BarLeft, 0.7853982f, 0.0f, 0.0f);
        this.ChairBackSlantRight = new ModelRenderer(this, 0, 15);
        this.ChairBackSlantRight.addBox(-4.2f, -1.2f, 4.0f, 1, 6, 1);
        this.ChairBackSlantRight.setRotationPoint(0.0f, 8.0f, 0.0f);
        this.ChairBackSlantRight.setTextureSize(64, 32);
        this.ChairBackSlantRight.mirror = true;
        this.setRotation(this.ChairBackSlantRight, 0.0f, 0.0f, 0.1745329f);
        this.ChairBackSlantLeft = new ModelRenderer(this, 0, 15);
        this.ChairBackSlantLeft.addBox(3.2f, -1.2f, 4.0f, 1, 6, 1);
        this.ChairBackSlantLeft.setRotationPoint(0.0f, 8.0f, 0.0f);
        this.ChairBackSlantLeft.setTextureSize(64, 32);
        this.ChairBackSlantLeft.mirror = true;
        this.setRotation(this.ChairBackSlantLeft, 0.0f, 0.0f, -0.1745329f);
        this.ChairBackMid = new ModelRenderer(this, 10, 6);
        this.ChairBackMid.addBox(-4.0f, 2.0f, 4.0f, 8, 2, 1);
        this.ChairBackMid.setRotationPoint(0.0f, 8.0f, 0.0f);
        this.ChairBackMid.setTextureSize(64, 32);
        this.ChairBackMid.mirror = true;
        this.setRotation(this.ChairBackMid, 0.0f, 0.0f, 0.0f);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.ChairBackTop.render(f5);
        this.LegFrontRight.render(f5);
        this.Cushion.render(f5);
        this.LegFrontLeft.render(f5);
        this.LegBackLeft.render(f5);
        this.LegBackRight.render(f5);
        this.ChairBackBottom.render(f5);
        this.BarRight.render(f5);
        this.BarLeft.render(f5);
        this.ChairBackSlantRight.render(f5);
        this.ChairBackSlantLeft.render(f5);
        this.ChairBackMid.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    @Override
    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    }
}

