/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.blocks;

import com.pixelmongenerations.client.models.ModelCustomWrapper;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.client.models.smd.ValveStudioModelLoader;
import java.util.ArrayList;
import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class GenericSmdModel
extends ModelBase {
    public static ArrayList<GenericSmdModel> loadedModels = new ArrayList();
    public PixelmonModelRenderer modelRenderer;
    private ValveStudioModel model;
    private ResourceLocation pqcResource;
    protected boolean overrideSmoothShading;

    public GenericSmdModel(ResourceLocation pqcResource, boolean overrideSmoothShading) {
        this.overrideSmoothShading = overrideSmoothShading;
        this.pqcResource = pqcResource;
        this.reloadModel();
    }

    public GenericSmdModel(String basePath, String pqcPath, boolean overrideSmoothShading) {
        this(new ResourceLocation("pixelmon:" + basePath + "/" + pqcPath), overrideSmoothShading);
    }

    public GenericSmdModel(String basePath, String pqcPath) {
        this(basePath, pqcPath, false);
    }

    public GenericSmdModel(String pqcPath) {
        this("models/pixelutilities", pqcPath);
    }

    public void reloadModel() {
        this.modelRenderer = new PixelmonModelRenderer(this, "body");
        this.model = (ValveStudioModel)ValveStudioModelLoader.instance.loadModel(this.pqcResource, this.overrideSmoothShading);
        this.modelRenderer.addCustomModel(new ModelCustomWrapper(this.model));
    }

    public ValveStudioModel getModel() {
        return this.model;
    }

    public void setFrame(int frame) {
        ValveStudioModel valveStudioModel = (ValveStudioModel)this.modelRenderer.objs.get((int)0).model;
        valveStudioModel.currentAnimation.setCurrentFrame((int)Math.floor(frame % valveStudioModel.currentAnimation.getNumFrames()));
        valveStudioModel.animate();
    }

    public void renderModel(float scale) {
        this.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, scale);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.modelRenderer.render(f5);
    }
}

