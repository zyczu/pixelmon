/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.blocks;

import com.pixelmongenerations.common.block.tileEntities.TileEntityDecorativeBase;
import net.minecraft.client.model.ModelBase;

public abstract class ModelEntityBlock
extends ModelBase {
    public abstract void renderTileEntity(TileEntityDecorativeBase var1, float var2);
}

