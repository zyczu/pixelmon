/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models;

import com.pixelmongenerations.client.models.ModelHolder;
import com.pixelmongenerations.client.models.MysteriousRingSmdFactory;
import com.pixelmongenerations.client.models.ResourceLoader;
import com.pixelmongenerations.core.Pixelmon;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.time.Instant;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import net.minecraft.client.model.ModelBase;

public class MysteriousRingModelHolder<M extends ModelBase>
extends ModelHolder<M> {
    private Class<M> clazz;
    private MysteriousRingSmdFactory factory;
    private Future<M> future;

    public MysteriousRingModelHolder(Class<M> clazz) {
        this.clazz = clazz;
    }

    public MysteriousRingModelHolder(MysteriousRingSmdFactory factory) {
        this.factory = factory;
        this.clazz = (Class<M>) MysteriousRingSmdFactory.Impl.class;
    }

    @Override
    public M getModel() {
        this.lastAccess = Instant.now().getEpochSecond();
        if (this.model != null) {
            return (M)this.model;
        }
        if (this.future == null) {
            this.future = ResourceLoader.addTask(this::loadModel);
        }
        if (this.future.isDone()) {
            try {
                this.model = (M) this.future.get();
                loadedHolders.add(this);
            }
            catch (ExecutionException executionException) {
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.future = null;
            return (M)this.model;
        }
        return (M)ResourceLoader.DUMMY;
    }

    @Override
    protected M loadModel() {
        if (this.factory != null) {
            return (M)this.factory.createModel();
        }
        if (this.clazz != null) {
            try {
                Constructor<?> constructor = this.clazz.getConstructors()[0];
                if (constructor.getParameterCount() == 0) {
                    return (M)((ModelBase)constructor.newInstance(new Object[0]));
                }
                Pixelmon.LOGGER.error("No valid constructor found in " + this.clazz.getSimpleName());
            }
            catch (IllegalAccessException | InstantiationException | InvocationTargetException var2) {
                var2.printStackTrace();
            }
            return (M)ResourceLoader.DUMMY;
        }
        return null;
    }

    @Override
    public void clear() {
        super.clear();
        this.future = null;
    }

    public Class<M> getModelClass() {
        return this.clazz;
    }
}

