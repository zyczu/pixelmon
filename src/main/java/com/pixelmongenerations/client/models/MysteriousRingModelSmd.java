/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models;

import com.pixelmongenerations.client.models.MysteriousRingModelBase;
import com.pixelmongenerations.client.models.smd.AnimationType;
import com.pixelmongenerations.client.models.smd.SmdAnimation;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.common.entity.EntityMysteriousRing;
import com.pixelmongenerations.common.entity.pixelmon.helpers.animation.IncrementingVariable;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;

public class MysteriousRingModelSmd
extends MysteriousRingModelBase {
    public float animationIncrement = 1.0f;
    public ValveStudioModel theModel;
    @Deprecated
    public float scale = 0.0f;

    public MysteriousRingModelSmd() {
        this.registerAnimationCounters();
    }

    @Override
    public void render(Entity var1, float f, float f1, float f2, float f3, float f4, float f5) {
    }

    @Override
    public float getScale() {
        return this.theModel.getScale();
    }

    public void setAnimationIncrement(float f) {
        this.animationIncrement = f;
    }

    public void setupForRender(EntityMysteriousRing ring) {
        this.setAnimation(AnimationType.IDLE.name());
        this.tickAnimation(ring);
    }

    protected void tickAnimation(EntityMysteriousRing ring) {
        SmdAnimation theAnim = this.theModel.currentAnimation;
        int frame = (int)Math.floor(this.getCounter((int)-1, (EntityMysteriousRing)ring).value % (float)theAnim.totalFrames);
        theAnim.setCurrentFrame(frame);
        ring.world.profiler.startSection("ring_animate");
        this.theModel.animate();
        ring.world.profiler.endSection();
    }

    protected void setAnimation(String string) {
        this.theModel.setAnimation(string);
    }

    public static boolean isMinecraftPaused() {
        Minecraft m = Minecraft.getMinecraft();
        return m.isSingleplayer() && m.currentScreen != null && m.currentScreen.doesGuiPauseGame() && !m.getIntegratedServer().getPublic();
    }

    @Override
    protected void setInt(int id, int value, EntityMysteriousRing ring) {
        ring.getAnimationVariables().setInt(id, value);
    }

    @Override
    protected int getInt(int id, EntityMysteriousRing ring) {
        return ring.getAnimationVariables().getInt(id);
    }

    protected IncrementingVariable setCounter(int id, float limit, float increment, EntityMysteriousRing ring) {
        ring.getAnimationVariables().setCounter(id, limit, increment);
        return this.getCounter(id, ring);
    }

    @Override
    protected IncrementingVariable getCounter(int id, EntityMysteriousRing ring) {
        return ring.getAnimationVariables().getCounter(id);
    }

    @Override
    protected void registerAnimationCounters() {
    }
}

