/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokemon.classic;

import com.pixelmongenerations.client.models.PixelmonModelBase;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.animations.EnumLeg;
import com.pixelmongenerations.client.models.animations.EnumPhase;
import com.pixelmongenerations.client.models.animations.EnumRotation;
import com.pixelmongenerations.client.models.animations.ModuleHead;
import com.pixelmongenerations.client.models.animations.ModuleLeg;
import com.pixelmongenerations.client.models.animations.ModuleTailBasic;
import com.pixelmongenerations.client.models.animations.quadruped.SkeletonQuadruped;
import net.minecraft.entity.Entity;

public class ModelGrowlithe
extends PixelmonModelBase {
    PixelmonModelRenderer Body;

    public ModelGrowlithe() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.Body = new PixelmonModelRenderer(this, "Body");
        this.Body.setRotationPoint(0.0f, 17.0f, 0.0f);
        PixelmonModelRenderer body_1 = new PixelmonModelRenderer(this, 0, 21);
        body_1.addBox(-3.0f, -2.266667f, -5.0f, 6, 6, 5);
        body_1.setTextureSize(64, 32);
        body_1.mirror = true;
        this.setRotation(body_1, -0.0174533f, 0.0f, 0.0f);
        PixelmonModelRenderer body_2 = new PixelmonModelRenderer(this, 22, 20);
        body_2.addBox(-2.5f, -2.0f, -3.0f, 5, 5, 7);
        body_2.setTextureSize(64, 32);
        body_2.mirror = true;
        this.setRotation(body_2, -0.0698132f, 0.0f, 0.0f);
        PixelmonModelRenderer belly = new PixelmonModelRenderer(this, 46, 2);
        belly.addBox(-2.0f, 2.8f, -0.4f, 4, 1, 3);
        belly.setTextureSize(64, 32);
        belly.mirror = true;
        this.setRotation(belly, 0.0698132f, 0.0f, 0.0f);
        PixelmonModelRenderer neck = new PixelmonModelRenderer(this, 24, 22);
        neck.addBox(-2.0f, 2.066667f, -5.0f, 4, 4, 6);
        neck.setTextureSize(64, 32);
        neck.mirror = true;
        this.setRotation(neck, -1.099557f, 0.0f, 0.0f);
        PixelmonModelRenderer mane_ = new PixelmonModelRenderer(this, 40, 0);
        mane_.addBox(-2.5f, 4.4f, -4.8f, 5, 2, 7);
        mane_.setTextureSize(64, 32);
        mane_.mirror = true;
        this.setRotation(mane_, -1.099557f, 0.0f, 0.0f);
        PixelmonModelRenderer mane__L = new PixelmonModelRenderer(this, 26, 10);
        mane__L.addBox(-2.5f, -6.1f, -5.0f, 5, 1, 6);
        mane__L.setTextureSize(64, 32);
        mane__L.mirror = true;
        this.setRotation(mane__L, -2.024582f, 2.495821f, 0.296706f);
        PixelmonModelRenderer mane__R = new PixelmonModelRenderer(this, 26, 10);
        mane__R.addBox(-2.5f, 6.133333f, -4.133333f, 5, 1, 6);
        mane__R.setTextureSize(64, 32);
        mane__R.mirror = true;
        this.setRotation(mane__R, -1.27409f, 0.715585f, 0.296706f);
        this.Body.addChild(body_1);
        this.Body.addChild(body_2);
        this.Body.addChild(belly);
        this.Body.addChild(neck);
        this.Body.addChild(mane_);
        this.Body.addChild(mane__L);
        this.Body.addChild(mane__R);
        PixelmonModelRenderer BLLeg = new PixelmonModelRenderer(this, "Back Left Leg");
        BLLeg.setRotationPoint(2.0f, 1.0f, 3.0f);
        PixelmonModelRenderer back_leg_1_L = new PixelmonModelRenderer(this, 0, 14);
        back_leg_1_L.addBox(-1.0f, -1.0f, -2.0f, 2, 4, 3);
        back_leg_1_L.setTextureSize(64, 32);
        back_leg_1_L.mirror = true;
        this.setRotation(back_leg_1_L, -0.0349066f, 0.0f, 0.0f);
        PixelmonModelRenderer back_leg_2_L = new PixelmonModelRenderer(this, 31, 26);
        back_leg_2_L.addBox(-1.2f, 2.0f, -1.533333f, 2, 4, 2);
        back_leg_2_L.setTextureSize(64, 32);
        back_leg_2_L.mirror = true;
        this.setRotation(back_leg_2_L, 0.0698132f, 0.0f, 0.0f);
        PixelmonModelRenderer paw_L = new PixelmonModelRenderer(this, 55, 11);
        paw_L.addBox(-1.2f, 5.0f, -1.6f, 2, 1, 2);
        paw_L.setTextureSize(64, 32);
        paw_L.mirror = true;
        this.setRotation(paw_L, 0.0f, 0.0f, 0.0f);
        BLLeg.addChild(back_leg_1_L);
        BLLeg.addChild(back_leg_2_L);
        BLLeg.addChild(paw_L);
        this.Body.addChild(BLLeg);
        PixelmonModelRenderer BRLeg = new PixelmonModelRenderer(this, "Back Right Leg");
        BRLeg.setRotationPoint(-2.0f, 1.0f, 3.0f);
        PixelmonModelRenderer back_leg_1_R = new PixelmonModelRenderer(this, 0, 14);
        back_leg_1_R.addBox(-1.0f, -1.0f, -2.0f, 2, 4, 3);
        back_leg_1_R.setTextureSize(64, 32);
        back_leg_1_R.mirror = true;
        this.setRotation(back_leg_1_R, -0.0349066f, 0.0f, 0.0f);
        PixelmonModelRenderer back_leg_2_R = new PixelmonModelRenderer(this, 32, 26);
        back_leg_2_R.addBox(-0.8f, 2.0f, -1.533333f, 2, 4, 2);
        back_leg_2_R.setTextureSize(64, 32);
        back_leg_2_R.mirror = true;
        this.setRotation(back_leg_2_R, 0.0698132f, 0.0f, 0.0f);
        PixelmonModelRenderer paw_R = new PixelmonModelRenderer(this, 55, 11);
        paw_R.addBox(-0.8f, 5.0f, -1.6f, 2, 1, 2);
        paw_R.setTextureSize(64, 32);
        paw_R.mirror = true;
        this.setRotation(paw_R, 0.0f, 0.0f, 0.0f);
        BRLeg.addChild(back_leg_1_R);
        BRLeg.addChild(back_leg_2_R);
        BRLeg.addChild(paw_R);
        this.Body.addChild(BRLeg);
        PixelmonModelRenderer FLLeg = new PixelmonModelRenderer(this, "Front Left Leg");
        FLLeg.setRotationPoint(3.0f, 1.0f, -4.0f);
        PixelmonModelRenderer front_leg_L = new PixelmonModelRenderer(this, 17, 18);
        front_leg_L.addBox(-1.5f, 0.0f, -1.0f, 2, 6, 2);
        front_leg_L.setTextureSize(64, 32);
        front_leg_L.mirror = true;
        this.setRotation(front_leg_L, 0.0f, 0.0f, 0.0f);
        PixelmonModelRenderer claw_1_L = new PixelmonModelRenderer(this, 15, 0);
        claw_1_L.addBox(-1.5f, 5.0f, -4.466667f, 1, 1, 2);
        claw_1_L.setTextureSize(64, 32);
        claw_1_L.mirror = true;
        this.setRotation(claw_1_L, 0.3490659f, 0.0f, 0.0f);
        PixelmonModelRenderer claw_2_L = new PixelmonModelRenderer(this, 15, 0);
        claw_2_L.addBox(-0.5f, 5.0f, -4.333333f, 1, 1, 2);
        claw_2_L.setTextureSize(64, 32);
        claw_2_L.mirror = true;
        this.setRotation(claw_2_L, 0.3490659f, 0.0f, 0.0f);
        FLLeg.addChild(front_leg_L);
        FLLeg.addChild(claw_1_L);
        FLLeg.addChild(claw_2_L);
        this.Body.addChild(FLLeg);
        PixelmonModelRenderer FRLeg = new PixelmonModelRenderer(this, "FRLeg");
        FRLeg.setRotationPoint(-3.0f, 1.0f, -4.0f);
        PixelmonModelRenderer front_leg_R = new PixelmonModelRenderer(this, 17, 18);
        front_leg_R.addBox(-0.5f, 0.0f, -1.0f, 2, 6, 2);
        front_leg_R.setTextureSize(64, 32);
        front_leg_R.mirror = true;
        this.setRotation(front_leg_R, 0.0f, 0.0f, 0.0f);
        PixelmonModelRenderer claw_1_R = new PixelmonModelRenderer(this, 15, 0);
        claw_1_R.addBox(-0.5f, 5.0f, -4.3f, 1, 1, 2);
        claw_1_R.setTextureSize(64, 32);
        claw_1_R.mirror = true;
        this.setRotation(claw_1_R, 0.3490659f, 0.0f, 0.0f);
        PixelmonModelRenderer claw_2_R = new PixelmonModelRenderer(this, 15, 0);
        claw_2_R.addBox(0.5f, 5.0f, -4.5f, 1, 1, 2);
        claw_2_R.setTextureSize(64, 32);
        claw_2_R.mirror = true;
        this.setRotation(claw_2_R, 0.3490659f, 0.0f, 0.0f);
        FRLeg.addChild(front_leg_R);
        FRLeg.addChild(claw_1_R);
        FRLeg.addChild(claw_2_R);
        this.Body.addChild(FRLeg);
        PixelmonModelRenderer Head = new PixelmonModelRenderer(this, "Head");
        Head.setRotationPoint(0.0f, -2.0f, -6.0f);
        PixelmonModelRenderer head_main = new PixelmonModelRenderer(this, 27, 23);
        head_main.addBox(-1.0f, -4.0f, -3.666667f, 2, 5, 4);
        head_main.setTextureSize(64, 32);
        head_main.mirror = true;
        this.setRotation(head_main, 0.0f, 0.0f, 0.0f);
        PixelmonModelRenderer head_L = new PixelmonModelRenderer(this, 10, 10);
        head_L.addBox(0.3333333f, -4.0f, -1.733333f, 3, 5, 4);
        head_L.setTextureSize(64, 32);
        head_L.mirror = true;
        this.setRotation(head_L, 0.0f, 0.8028515f, 0.0f);
        PixelmonModelRenderer head_R = new PixelmonModelRenderer(this, 10, 10);
        head_R.addBox(-3.3f, -4.0f, -1.733333f, 3, 5, 4);
        head_R.setTextureSize(64, 32);
        head_R.mirror = true;
        this.setRotation(head_R, 0.0f, -0.8028515f, 0.0f);
        PixelmonModelRenderer head_back = new PixelmonModelRenderer(this, 25, 23);
        head_back.addBox(-2.5f, -4.0f, -1.6f, 5, 5, 4);
        head_back.setTextureSize(64, 32);
        head_back.mirror = true;
        this.setRotation(head_back, 0.0f, 0.0f, 0.0f);
        PixelmonModelRenderer head_L_back = new PixelmonModelRenderer(this, 26, 24);
        head_L_back.addBox(0.3333333f, -4.0f, -2.2f, 3, 5, 3);
        head_L_back.setTextureSize(64, 32);
        head_L_back.mirror = true;
        this.setRotation(head_L_back, 0.0f, -0.4014257f, 0.0f);
        PixelmonModelRenderer head_R_back = new PixelmonModelRenderer(this, 28, 24);
        head_R_back.addBox(-3.3f, -4.0f, -2.2f, 3, 5, 3);
        head_R_back.setTextureSize(64, 32);
        head_R_back.mirror = true;
        this.setRotation(head_R_back, 0.0f, 0.4014257f, 0.0f);
        PixelmonModelRenderer snout_bottom = new PixelmonModelRenderer(this, 48, 0);
        snout_bottom.addBox(-1.5f, -0.06666667f, -3.9f, 3, 2, 4);
        snout_bottom.setTextureSize(64, 32);
        snout_bottom.mirror = true;
        this.setRotation(snout_bottom, -0.0872665f, 0.0f, 0.0f);
        PixelmonModelRenderer snout = new PixelmonModelRenderer(this, 46, 26);
        snout.addBox(-1.5f, -2.0f, -4.333333f, 3, 2, 3);
        snout.setTextureSize(64, 32);
        snout.mirror = true;
        this.setRotation(snout, 0.3665191f, 0.0f, 0.0f);
        PixelmonModelRenderer cheek_top_L = new PixelmonModelRenderer(this, 47, 0);
        cheek_top_L.addBox(1.766667f, -1.733333f, -2.666667f, 2, 2, 4);
        cheek_top_L.setTextureSize(64, 32);
        cheek_top_L.mirror = true;
        this.setRotation(cheek_top_L, 0.2443461f, 0.6108652f, 0.1745329f);
        PixelmonModelRenderer cheek_bottom_L = new PixelmonModelRenderer(this, 48, 0);
        cheek_bottom_L.addBox(1.766667f, -0.2666667f, -2.666667f, 2, 1, 4);
        cheek_bottom_L.setTextureSize(64, 32);
        cheek_bottom_L.mirror = true;
        this.setRotation(cheek_bottom_L, 0.0349066f, 0.6108652f, 0.1745329f);
        PixelmonModelRenderer snout_top_R = new PixelmonModelRenderer(this, 48, 0);
        snout_top_R.addBox(-3.8f, -1.733333f, -2.666667f, 2, 2, 4);
        snout_top_R.setTextureSize(64, 32);
        snout_top_R.mirror = true;
        this.setRotation(snout_top_R, 0.2443461f, -0.6108652f, -0.1570796f);
        PixelmonModelRenderer cheek_bottom_R = new PixelmonModelRenderer(this, 48, 0);
        cheek_bottom_R.addBox(-3.8f, -0.2666667f, -2.666667f, 2, 1, 4);
        cheek_bottom_R.setTextureSize(64, 32);
        cheek_bottom_R.mirror = true;
        this.setRotation(cheek_bottom_R, 0.0349066f, -0.6108652f, -0.1919862f);
        PixelmonModelRenderer head_top = new PixelmonModelRenderer(this, 23, 26);
        head_top.addBox(-2.0f, -4.8f, -2.333333f, 4, 2, 4);
        head_top.setTextureSize(64, 32);
        head_top.mirror = true;
        this.setRotation(head_top, 0.0698132f, 0.0f, 0.0f);
        PixelmonModelRenderer head_fluff_ = new PixelmonModelRenderer(this, 21, 0);
        head_fluff_.addBox(0.4666667f, -9.0f, -3.6f, 1, 6, 6);
        head_fluff_.setTextureSize(64, 32);
        head_fluff_.mirror = true;
        this.setRotation(head_fluff_, -0.1745329f, 0.715585f, -0.2094395f);
        PixelmonModelRenderer ear_L = new PixelmonModelRenderer(this, 0, 0);
        ear_L.addBox(2.666667f, -4.0f, -1.4f, 6, 6, 1);
        ear_L.setTextureSize(64, 32);
        ear_L.mirror = true;
        this.setRotation(ear_L, 0.0f, -0.3490659f, -0.3665191f);
        PixelmonModelRenderer head_fluff_2 = new PixelmonModelRenderer(this, 50, 0);
        head_fluff_2.addBox(-1.533333f, -6.8f, -2.0f, 3, 3, 4);
        head_fluff_2.setTextureSize(64, 32);
        head_fluff_2.mirror = true;
        this.setRotation(head_fluff_2, 0.0174533f, 0.715585f, -0.0349066f);
        PixelmonModelRenderer ear_R = new PixelmonModelRenderer(this, 0, 7);
        ear_R.addBox(-8.7f, -4.0f, -1.4f, 6, 6, 1);
        ear_R.setTextureSize(64, 32);
        ear_R.mirror = true;
        this.setRotation(ear_R, 0.0f, 0.3490659f, 0.3665191f);
        Head.addChild(head_main);
        Head.addChild(head_L);
        Head.addChild(head_R);
        Head.addChild(head_back);
        Head.addChild(head_L_back);
        Head.addChild(head_R_back);
        Head.addChild(snout_bottom);
        Head.addChild(snout);
        Head.addChild(cheek_top_L);
        Head.addChild(cheek_bottom_L);
        Head.addChild(snout_top_R);
        Head.addChild(cheek_bottom_R);
        Head.addChild(head_top);
        Head.addChild(head_fluff_);
        Head.addChild(ear_L);
        Head.addChild(head_fluff_2);
        Head.addChild(ear_R);
        this.Body.addChild(Head);
        PixelmonModelRenderer Tail = new PixelmonModelRenderer(this, "Tail");
        Tail.setRotationPoint(0.0f, -1.0f, 4.0f);
        PixelmonModelRenderer tail_main = new PixelmonModelRenderer(this, 46, 0);
        tail_main.addBox(-2.0f, -2.0f, -1.0f, 4, 4, 5);
        tail_main.setTextureSize(64, 32);
        tail_main.mirror = true;
        this.setRotation(tail_main, 0.5759587f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_bottom = new PixelmonModelRenderer(this, 50, 0);
        tail_bottom.addBox(-1.5f, -2.2f, -0.5333334f, 3, 5, 4);
        tail_bottom.setTextureSize(64, 32);
        tail_bottom.mirror = true;
        this.setRotation(tail_bottom, 0.5759587f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_top = new PixelmonModelRenderer(this, 54, 0);
        tail_top.addBox(-1.533333f, -1.466667f, 2.666667f, 3, 3, 2);
        tail_top.setTextureSize(64, 32);
        tail_top.mirror = true;
        this.setRotation(tail_top, 0.5759587f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_fluff = new PixelmonModelRenderer(this, 41, 10);
        tail_fluff.addBox(-1.733333f, -3.733333f, 2.0f, 1, 7, 7);
        tail_fluff.setTextureSize(64, 32);
        tail_fluff.mirror = true;
        this.setRotation(tail_fluff, 0.1396263f, 0.2792527f, 0.0f);
        Tail.addChild(tail_main);
        Tail.addChild(tail_bottom);
        Tail.addChild(tail_top);
        Tail.addChild(tail_fluff);
        this.Body.addChild(Tail);
        ModuleHead headModule = new ModuleHead(Head);
        float legspeed = 0.8f;
        float legRotationLimit = 1.1f;
        ModuleLeg frontlegLModule = new ModuleLeg(FLLeg, EnumLeg.FrontLeft, EnumPhase.OutPhase, EnumRotation.x, legRotationLimit, legspeed);
        ModuleLeg frontlegRModule = new ModuleLeg(FRLeg, EnumLeg.FrontRight, EnumPhase.OutPhase, EnumRotation.x, legRotationLimit, legspeed);
        ModuleLeg backlegLModule = new ModuleLeg(BLLeg, EnumLeg.BackLeft, EnumPhase.OutPhase, EnumRotation.x, legRotationLimit, legspeed);
        ModuleLeg backlegRModule = new ModuleLeg(BRLeg, EnumLeg.BackRight, EnumPhase.OutPhase, EnumRotation.x, legRotationLimit, legspeed);
        ModuleTailBasic tailModule = new ModuleTailBasic(Tail, 0.2f, 0.05f, legspeed);
        this.skeleton = new SkeletonQuadruped(this.Body, headModule, frontlegLModule, frontlegRModule, backlegLModule, backlegRModule, tailModule);
        this.scale = 0.65f;
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5);
        this.Body.render(f5);
    }

    private void setRotation(PixelmonModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5) {
    }
}

