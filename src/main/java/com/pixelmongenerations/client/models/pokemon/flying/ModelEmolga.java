/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokemon.flying;

import com.pixelmongenerations.client.models.ModelCustomWrapper;
import com.pixelmongenerations.client.models.PixelmonModelBase;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.animations.ModuleTailBasic;
import com.pixelmongenerations.client.models.animations.bird.SkeletonBird;
import com.pixelmongenerations.client.models.obj.ObjLoader;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

public class ModelEmolga
extends PixelmonModelBase {
    PixelmonModelRenderer Body;
    PixelmonModelRenderer Tail;

    public ModelEmolga() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.Body = new PixelmonModelRenderer(this, "Body");
        this.Body.setRotationPoint(0.0f, 24.1f, 0.0f);
        this.Body.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/emolga/flying/EmolgaBody.obj"))));
        this.Tail = new PixelmonModelRenderer(this, 0, 0);
        this.Tail.setRotationPoint(-0.004f, 2.81f, -1.01f);
        this.Tail.addCustomModel(new ModelCustomWrapper(ObjLoader.loadModel(new ResourceLocation("pixelmon:models/pokemon/emolga/flying/EmolgaTail.obj"))));
        this.Body.addChild(this.Tail);
        int degrees = 180;
        float radians = (float)Math.toRadians(degrees);
        this.setRotation(this.Body, radians, 0.0f, 0.0f);
        ModuleTailBasic tailModule = new ModuleTailBasic(this.Tail, 0.05f, 0.1f, 0.2f);
        this.skeleton = new SkeletonBird(this.Body, null, null, null, null, null, tailModule);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles();
        this.scale = 2.2f;
        this.Body.render(f5);
        this.Body.setRotationPoint(-2.0f * MathHelper.cos(f2 * 0.1f), -0.5f * MathHelper.cos(f2 * 0.075f) + 24.1f, 0.0f);
        this.Body.rotateAngleZ = MathHelper.cos((float)Math.toRadians(35.0)) * 0.2f * MathHelper.cos(f2 * 0.1f) * (float)Math.PI * 0.25f;
    }

    private void setRotation(PixelmonModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles() {
    }
}

