/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.pokemon.classic;

import com.pixelmongenerations.client.models.PixelmonModelBase;
import com.pixelmongenerations.client.models.PixelmonModelRenderer;
import com.pixelmongenerations.client.models.animations.EnumArm;
import com.pixelmongenerations.client.models.animations.EnumLeg;
import com.pixelmongenerations.client.models.animations.EnumPhase;
import com.pixelmongenerations.client.models.animations.EnumRotation;
import com.pixelmongenerations.client.models.animations.ModuleArm;
import com.pixelmongenerations.client.models.animations.ModuleHead;
import com.pixelmongenerations.client.models.animations.ModuleLeg;
import com.pixelmongenerations.client.models.animations.ModuleTailBasic;
import com.pixelmongenerations.client.models.animations.biped.SkeletonBiped;
import net.minecraft.entity.Entity;

public class ModelArmaldo
extends PixelmonModelBase {
    PixelmonModelRenderer Body;

    public ModelArmaldo() {
        this.textureWidth = 128;
        this.textureHeight = 64;
        this.Body = new PixelmonModelRenderer(this, "Body");
        this.Body.setRotationPoint(0.0f, 14.0f, 0.0f);
        PixelmonModelRenderer main_body = new PixelmonModelRenderer(this, 0, 50);
        main_body.addBox(-4.5f, -2.0f, -3.066667f, 9, 6, 8);
        main_body.setTextureSize(128, 64);
        main_body.mirror = true;
        this.setRotation(main_body, 0.122173f, 0.0f, 0.0f);
        PixelmonModelRenderer body_upper = new PixelmonModelRenderer(this, 0, 33);
        body_upper.addBox(-5.0f, -8.0f, -3.2f, 10, 8, 8);
        body_upper.setTextureSize(128, 64);
        body_upper.mirror = true;
        this.setRotation(body_upper, 0.0523599f, 0.0f, 0.0f);
        PixelmonModelRenderer neck_base = new PixelmonModelRenderer(this, 1, 22);
        neck_base.addBox(-4.0f, -9.4f, -3.066667f, 8, 3, 6);
        neck_base.setTextureSize(128, 64);
        neck_base.mirror = true;
        this.setRotation(neck_base, -0.0872665f, 0.0f, 0.0f);
        PixelmonModelRenderer neck_2 = new PixelmonModelRenderer(this, 32, 51);
        neck_2.addBox(-3.0f, -16.86667f, -2.033333f, 6, 8, 5);
        neck_2.setTextureSize(128, 64);
        neck_2.mirror = true;
        this.setRotation(neck_2, -0.0174533f, 0.0f, 0.0f);
        PixelmonModelRenderer neck_fin_1_L = new PixelmonModelRenderer(this, -7, 0);
        neck_fin_1_L.addBox(-0.5f, -15.266667f, 0.0f, 7, 3, 0);
        neck_fin_1_L.setRotationPoint(3.0f, 0.4666667f, 9.992007E-16f);
        neck_fin_1_L.setTextureSize(128, 64);
        neck_fin_1_L.mirror = true;
        this.setRotation(neck_fin_1_L, 0.0f, 0.0f, -0.1047198f);
        PixelmonModelRenderer neck_fin_2_L = new PixelmonModelRenderer(this, -7, 0);
        neck_fin_2_L.addBox(-0.5f, -15.466667f, 0.0f, 7, 3, 0);
        neck_fin_2_L.setRotationPoint(3.0f, 3.0f, 0.0f);
        neck_fin_2_L.setTextureSize(128, 64);
        neck_fin_2_L.mirror = true;
        this.setRotation(neck_fin_2_L, 0.0f, -0.0523599f, -0.0523599f);
        PixelmonModelRenderer neck_fin_3_L = new PixelmonModelRenderer(this, -7, 0);
        neck_fin_3_L.addBox(-0.5f, -15.333333f, 0.0f, 7, 3, 0);
        neck_fin_3_L.setRotationPoint(3.0f, 5.733333f, 0.0f);
        neck_fin_3_L.setTextureSize(128, 64);
        neck_fin_3_L.mirror = true;
        this.setRotation(neck_fin_3_L, 0.0f, -0.0872665f, -0.0349066f);
        PixelmonModelRenderer neck_fin_1_R = new PixelmonModelRenderer(this, 0, 0);
        neck_fin_1_R.addBox(-6.5f, -15.266667f, 0.0f, 7, 3, 0);
        neck_fin_1_R.setRotationPoint(-3.0f, 0.5f, 0.0f);
        neck_fin_1_R.setTextureSize(128, 64);
        neck_fin_1_R.mirror = true;
        this.setRotation(neck_fin_1_R, 0.0f, 0.0f, 0.1047198f);
        PixelmonModelRenderer neck_fin_2_R = new PixelmonModelRenderer(this, 0, 0);
        neck_fin_2_R.addBox(-6.5f, -15.466667f, 0.0f, 7, 3, 0);
        neck_fin_2_R.setRotationPoint(-3.0f, 3.0f, 0.0f);
        neck_fin_2_R.setTextureSize(128, 64);
        neck_fin_2_R.mirror = true;
        this.setRotation(neck_fin_2_R, 0.0f, 0.0523599f, 0.0523599f);
        PixelmonModelRenderer neck_fin_3_R = new PixelmonModelRenderer(this, 0, 0);
        neck_fin_3_R.addBox(-6.5f, -15.333333f, 0.0f, 7, 3, 0);
        neck_fin_3_R.setRotationPoint(-3.0f, 5.666667f, 0.0f);
        neck_fin_3_R.setTextureSize(128, 64);
        neck_fin_3_R.mirror = true;
        this.setRotation(neck_fin_3_R, 0.0f, 0.0872665f, 0.0349066f);
        PixelmonModelRenderer wing_base_top = new PixelmonModelRenderer(this, 37, 32);
        wing_base_top.addBox(-2.0f, -26.13333f, 3.466667f, 4, 5, 3);
        wing_base_top.setRotationPoint(0.0f, 14.0f, 0.0f);
        wing_base_top.setTextureSize(128, 64);
        wing_base_top.mirror = true;
        this.setRotation(wing_base_top, 0.2443461f, 0.0f, 0.0f);
        PixelmonModelRenderer wing_base_main = new PixelmonModelRenderer(this, 37, 41);
        wing_base_main.addBox(-3.0f, -22.133333f, 4.066667f, 6, 6, 3);
        wing_base_main.setRotationPoint(0.0f, 14.0f, 0.0f);
        wing_base_main.setTextureSize(128, 64);
        wing_base_main.mirror = true;
        this.setRotation(wing_base_main, 0.296706f, 0.0f, 0.0f);
        PixelmonModelRenderer wing_base_bottom = new PixelmonModelRenderer(this, 56, 45);
        wing_base_bottom.addBox(-1.5f, -19.0f, 1.666667f, 3, 3, 3);
        wing_base_bottom.setRotationPoint(0.0f, 14.0f, 0.0f);
        wing_base_bottom.setTextureSize(128, 64);
        wing_base_bottom.mirror = true;
        this.setRotation(wing_base_bottom, -0.1919862f, 0.0f, 0.0f);
        PixelmonModelRenderer wing_base_spike_L_1 = new PixelmonModelRenderer(this, 27, 5);
        wing_base_spike_L_1.addBox(1.0f, -21.133333f, 6.4f, 1, 1, 2);
        wing_base_spike_L_1.setRotationPoint(0.0f, 14.0f, 0.0f);
        wing_base_spike_L_1.setTextureSize(128, 64);
        wing_base_spike_L_1.mirror = true;
        this.setRotation(wing_base_spike_L_1, 0.3316126f, 0.1047198f, 0.0f);
        PixelmonModelRenderer wing_base_spike_L_2 = new PixelmonModelRenderer(this, 27, 5);
        wing_base_spike_L_2.addBox(0.6f, -19.4f, 5.866667f, 1, 1, 2);
        wing_base_spike_L_2.setRotationPoint(0.0f, 14.0f, 0.0f);
        wing_base_spike_L_2.setTextureSize(128, 64);
        wing_base_spike_L_2.mirror = true;
        this.setRotation(wing_base_spike_L_2, 0.1745329f, 0.1047198f, 0.0f);
        PixelmonModelRenderer wing_base_spike_R_2 = new PixelmonModelRenderer(this, 27, 5);
        wing_base_spike_R_2.addBox(-1.6f, -19.4f, 5.866667f, 1, 1, 2);
        wing_base_spike_R_2.setRotationPoint(0.0f, 14.0f, 0.0f);
        wing_base_spike_R_2.setTextureSize(128, 64);
        wing_base_spike_R_2.mirror = true;
        this.setRotation(wing_base_spike_R_2, 0.1745329f, -0.1047198f, 0.0f);
        PixelmonModelRenderer wing_base_spike_R_1 = new PixelmonModelRenderer(this, 27, 5);
        wing_base_spike_R_1.addBox(-2.0f, -21.133333f, 6.4f, 1, 1, 2);
        wing_base_spike_R_1.setRotationPoint(0.0f, 14.0f, 0.0f);
        wing_base_spike_R_1.setTextureSize(128, 64);
        wing_base_spike_R_1.mirror = true;
        this.setRotation(wing_base_spike_R_1, 0.3316126f, -0.1047198f, 0.0f);
        PixelmonModelRenderer wing_L = new PixelmonModelRenderer(this, 57, 25);
        wing_L.addBox(-3.0f, -2.133333f, 0.06666667f, 13, 18, 0);
        wing_L.setRotationPoint(3.0f, -8.0f, 4.0f);
        wing_L.setTextureSize(128, 64);
        wing_L.mirror = true;
        this.setRotation(wing_L, 0.296706f, -0.3141593f, -0.3141593f);
        PixelmonModelRenderer wing_R = new PixelmonModelRenderer(this, 71, 25);
        wing_R.addBox(-10.0f, -2.133333f, 0.06666667f, 13, 18, 0);
        wing_R.setRotationPoint(-3.0f, -8.0f, 4.0f);
        wing_R.setTextureSize(128, 64);
        wing_R.mirror = true;
        this.setRotation(wing_R, 0.296706f, 0.3141593f, 0.3141593f);
        this.Body.addChild(main_body);
        this.Body.addChild(body_upper);
        this.Body.addChild(neck_base);
        this.Body.addChild(neck_2);
        this.Body.addChild(neck_fin_1_L);
        this.Body.addChild(neck_fin_2_L);
        this.Body.addChild(neck_fin_3_L);
        this.Body.addChild(neck_fin_1_R);
        this.Body.addChild(neck_fin_2_R);
        this.Body.addChild(neck_fin_3_R);
        this.Body.addChild(wing_base_top);
        this.Body.addChild(wing_base_main);
        this.Body.addChild(wing_base_bottom);
        this.Body.addChild(wing_base_spike_L_1);
        this.Body.addChild(wing_base_spike_L_2);
        this.Body.addChild(wing_base_spike_R_2);
        this.Body.addChild(wing_base_spike_R_1);
        this.Body.addChild(wing_L);
        this.Body.addChild(wing_R);
        PixelmonModelRenderer Tail = new PixelmonModelRenderer(this, "Tail");
        Tail.setRotationPoint(0.0f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_base = new PixelmonModelRenderer(this, 102, 54);
        tail_base.addBox(-3.5f, 2.0f, -3.666667f, 7, 4, 6);
        tail_base.setTextureSize(128, 64);
        tail_base.mirror = true;
        this.setRotation(tail_base, 0.7330383f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_1 = new PixelmonModelRenderer(this, 106, 43);
        tail_1.addBox(-3.0f, -1.0f, -3.333333f, 6, 5, 5);
        tail_1.setRotationPoint(0.0f, 4.0f, 4.0f);
        tail_1.setTextureSize(128, 64);
        tail_1.mirror = true;
        this.setRotation(tail_1, 0.9948377f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_2 = new PixelmonModelRenderer(this, 110, 33);
        tail_2.addBox(-2.5f, -0.6f, -1.8f, 5, 5, 4);
        tail_2.setRotationPoint(0.0f, 7.0f, 6.6f);
        tail_2.setTextureSize(128, 64);
        tail_2.mirror = true;
        this.setRotation(tail_2, 1.413717f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_3 = new PixelmonModelRenderer(this, 114, 24);
        tail_3.addBox(-2.0f, -0.8666667f, -1.4f, 4, 5, 3);
        tail_3.setRotationPoint(0.0f, 7.33333f, 11.0f);
        tail_3.setTextureSize(128, 64);
        tail_3.mirror = true;
        this.setRotation(tail_3, 1.605703f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_4 = new PixelmonModelRenderer(this, 118, 3);
        tail_4.addBox(-1.5f, -0.8666667f, -0.6f, 3, 5, 2);
        tail_4.setRotationPoint(0.0f, 7.26667f, 15.0f);
        tail_4.setTextureSize(128, 64);
        tail_4.mirror = true;
        this.setRotation(tail_4, 1.710423f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_5 = new PixelmonModelRenderer(this, 122, 10);
        tail_5.addBox(-1.0f, -0.8666667f, -0.4f, 2, 4, 1);
        tail_5.setRotationPoint(0.0f, 6.26667f, 19.0f);
        tail_5.setTextureSize(128, 64);
        tail_5.mirror = true;
        this.setRotation(tail_5, 1.872157f, 0.0f, 0.0f);
        PixelmonModelRenderer tail_fin_L = new PixelmonModelRenderer(this, 107, 15);
        tail_fin_L.addBox(-1.0f, -0.8666667f, 0.06666667f, 9, 8, 0);
        tail_fin_L.setRotationPoint(0.0f, 6.26667f, 19.0f);
        tail_fin_L.setTextureSize(128, 64);
        tail_fin_L.mirror = true;
        this.setRotation(tail_fin_L, 1.867502f, 0.0f, -0.7330383f);
        PixelmonModelRenderer tail_fin_R = new PixelmonModelRenderer(this, 99, 15);
        tail_fin_R.addBox(-8.0f, -0.8666667f, 0.06666667f, 9, 8, 0);
        tail_fin_R.setRotationPoint(0.0f, 6.26667f, 19.0f);
        tail_fin_R.setTextureSize(128, 64);
        tail_fin_R.mirror = true;
        this.setRotation(tail_fin_R, 1.867502f, 0.0f, 0.7330383f);
        Tail.addChild(tail_base);
        Tail.addChild(tail_1);
        Tail.addChild(tail_2);
        Tail.addChild(tail_3);
        Tail.addChild(tail_4);
        Tail.addChild(tail_5);
        Tail.addChild(tail_fin_L);
        Tail.addChild(tail_fin_R);
        PixelmonModelRenderer Head = new PixelmonModelRenderer(this, "Head");
        Head.setRotationPoint(0.0f, -17.0f, 0.0f);
        PixelmonModelRenderer main_head = new PixelmonModelRenderer(this, 35, 0);
        main_head.addBox(-3.5f, -2.0f, -2.0f, 7, 4, 4);
        main_head.setTextureSize(128, 64);
        main_head.mirror = true;
        this.setRotation(main_head, 0.122173f, 0.0f, 0.0f);
        PixelmonModelRenderer head_front_1 = new PixelmonModelRenderer(this, 39, 9);
        head_front_1.addBox(-1.533333f, -2.3f, -3.066667f, 3, 3, 2);
        head_front_1.setTextureSize(128, 64);
        head_front_1.mirror = true;
        this.setRotation(head_front_1, 0.5585054f, 0.0f, 0.0f);
        PixelmonModelRenderer head_front_2 = new PixelmonModelRenderer(this, 79, 15);
        head_front_2.addBox(-1.0f, -2.933333f, -4.4f, 2, 2, 2);
        head_front_2.setTextureSize(128, 64);
        head_front_2.mirror = true;
        this.setRotation(head_front_2, 0.8726646f, 0.0f, 0.0f);
        PixelmonModelRenderer head_top = new PixelmonModelRenderer(this, 27, 20);
        head_top.addBox(-2.5f, -2.4f, -2.2f, 5, 2, 4);
        head_top.setTextureSize(128, 64);
        head_top.mirror = true;
        this.setRotation(head_top, 0.3141593f, 0.0f, 0.0f);
        PixelmonModelRenderer head_back = new PixelmonModelRenderer(this, 0, 11);
        head_back.addBox(-3.0f, -1.733333f, 1.8f, 6, 3, 1);
        head_back.setTextureSize(128, 64);
        head_back.mirror = true;
        this.setRotation(head_back, 0.122173f, 0.0f, 0.0f);
        PixelmonModelRenderer head_top_2 = new PixelmonModelRenderer(this, 30, 27);
        head_top_2.addBox(-1.5f, -3.066667f, -2.933333f, 3, 2, 2);
        head_top_2.setTextureSize(128, 64);
        head_top_2.mirror = true;
        this.setRotation(head_top_2, 0.8028515f, 0.0f, 0.0f);
        PixelmonModelRenderer eye_R = new PixelmonModelRenderer(this, 58, 0);
        eye_R.addBox(-9.9f, -2.333333f, 0.0f, 7, 6, 0);
        eye_R.setTextureSize(128, 64);
        eye_R.mirror = true;
        this.setRotation(eye_R, 0.122173f, 0.0f, 0.3839724f);
        PixelmonModelRenderer eye_L = new PixelmonModelRenderer(this, 65, 0);
        eye_L.addBox(2.9f, -2.333333f, 0.0f, 7, 6, 0);
        eye_L.setTextureSize(128, 64);
        eye_L.mirror = true;
        this.setRotation(eye_L, 0.122173f, 0.0f, -0.3839724f);
        PixelmonModelRenderer eye_R_2 = new PixelmonModelRenderer(this, 104, 61);
        eye_R_2.addBox(-7.9f, -0.6f, 0.1333333f, 5, 2, 1);
        eye_R_2.setTextureSize(128, 64);
        eye_R_2.mirror = true;
        this.setRotation(eye_R_2, 0.122173f, 0.0f, 0.3839724f);
        PixelmonModelRenderer eye_L_2 = new PixelmonModelRenderer(this, 104, 61);
        eye_L_2.addBox(2.9f, -0.6f, 0.1333333f, 5, 2, 1);
        eye_L_2.setTextureSize(128, 64);
        eye_L_2.mirror = true;
        this.setRotation(eye_L_2, 0.122173f, 0.0f, -0.3839724f);
        PixelmonModelRenderer head_front_1_R = new PixelmonModelRenderer(this, 58, 7);
        head_front_1_R.addBox(-3.333333f, -2.3f, -2.666667f, 2, 3, 3);
        head_front_1_R.setTextureSize(128, 64);
        head_front_1_R.mirror = true;
        this.setRotation(head_front_1_R, 0.6632251f, -0.4014257f, -0.1745329f);
        PixelmonModelRenderer head_front_1_L = new PixelmonModelRenderer(this, 16, 7);
        head_front_1_L.addBox(-3.3f, -2.3f, -0.6666667f, 2, 3, 3);
        head_front_1_L.setTextureSize(128, 64);
        head_front_1_L.mirror = true;
        this.setRotation(head_front_1_L, -0.6632251f, -2.740167f, -0.1745329f);
        PixelmonModelRenderer head_front_2_R = new PixelmonModelRenderer(this, 54, 15);
        head_front_2_R.addBox(-3.6f, -2.3f, -2.9f, 2, 2, 2);
        head_front_2_R.setTextureSize(128, 64);
        head_front_2_R.mirror = true;
        this.setRotation(head_front_2_R, 0.6981317f, -0.5934119f, -0.3490659f);
        PixelmonModelRenderer head_front_2_L = new PixelmonModelRenderer(this, 54, 15);
        head_front_2_L.addBox(1.6f, -2.3f, -2.933333f, 2, 2, 2);
        head_front_2_L.setTextureSize(128, 64);
        head_front_2_L.mirror = true;
        this.setRotation(head_front_2_L, 0.6981317f, 0.5934119f, 0.3490659f);
        PixelmonModelRenderer bottom_jaw_L = new PixelmonModelRenderer(this, 69, 9);
        bottom_jaw_L.addBox(2.333333f, -1.3f, -3.0f, 1, 1, 3);
        bottom_jaw_L.setTextureSize(128, 64);
        bottom_jaw_L.mirror = true;
        this.setRotation(bottom_jaw_L, 0.0f, 0.4886922f, 1.082104f);
        PixelmonModelRenderer bottom_jaw_R = new PixelmonModelRenderer(this, 69, 9);
        bottom_jaw_R.addBox(-3.3f, -1.3f, -3.0f, 1, 1, 3);
        bottom_jaw_R.setTextureSize(128, 64);
        bottom_jaw_R.mirror = true;
        this.setRotation(bottom_jaw_R, 0.0f, -0.4886922f, -1.082104f);
        PixelmonModelRenderer bottom_jaw_center = new PixelmonModelRenderer(this, 66, 13);
        bottom_jaw_center.addBox(-1.0f, 1.7f, -3.533333f, 2, 1, 4);
        bottom_jaw_center.setTextureSize(128, 64);
        bottom_jaw_center.mirror = true;
        this.setRotation(bottom_jaw_center, 0.0349066f, 0.0f, 0.0f);
        Head.addChild(main_head);
        Head.addChild(head_front_1);
        Head.addChild(head_front_2);
        Head.addChild(head_top);
        Head.addChild(head_back);
        Head.addChild(head_top_2);
        Head.addChild(eye_R);
        Head.addChild(eye_L);
        Head.addChild(eye_R_2);
        Head.addChild(eye_L_2);
        Head.addChild(head_front_1_R);
        Head.addChild(head_front_1_L);
        Head.addChild(head_front_2_R);
        Head.addChild(head_front_2_L);
        Head.addChild(bottom_jaw_L);
        Head.addChild(bottom_jaw_R);
        Head.addChild(bottom_jaw_center);
        PixelmonModelRenderer RightLeg = new PixelmonModelRenderer(this, "RightLeg");
        RightLeg.setRotationPoint(-4.0f, 4.0f, 0.0f);
        PixelmonModelRenderer leg_1_R = new PixelmonModelRenderer(this, 54, 52);
        leg_1_R.addBox(-2.0f, -2.0f, -3.133333f, 4, 7, 5);
        leg_1_R.setTextureSize(128, 64);
        leg_1_R.mirror = true;
        this.setRotation(leg_1_R, 0.8203047f, 0.5585054f, 0.2094395f);
        PixelmonModelRenderer leg_3_R = new PixelmonModelRenderer(this, 73, 49);
        leg_3_R.addBox(-2.466667f, 2.0f, -3.466667f, 4, 4, 4);
        leg_3_R.setTextureSize(128, 64);
        leg_3_R.mirror = true;
        this.setRotation(leg_3_R, 0.0f, 0.4014257f, 0.0f);
        PixelmonModelRenderer leg_2_R = new PixelmonModelRenderer(this, 73, 58);
        leg_2_R.addBox(-2.0f, -0.4f, -5.0f, 3, 3, 3);
        leg_2_R.setTextureSize(128, 64);
        leg_2_R.mirror = true;
        this.setRotation(leg_2_R, 0.5410521f, 0.4014257f, 0.0f);
        PixelmonModelRenderer toe_1_R = new PixelmonModelRenderer(this, 27, 0);
        toe_1_R.addBox(-2.0f, 5.0f, -4.466667f, 1, 1, 2);
        toe_1_R.setTextureSize(128, 64);
        toe_1_R.mirror = true;
        this.setRotation(toe_1_R, 0.0f, 0.4014257f, 0.0f);
        PixelmonModelRenderer toe_2_R = new PixelmonModelRenderer(this, 27, 0);
        toe_2_R.addBox(0.0f, 5.0f, -4.466667f, 1, 1, 2);
        toe_2_R.setTextureSize(128, 64);
        toe_2_R.mirror = true;
        this.setRotation(toe_2_R, 0.0f, 0.4014257f, 0.0f);
        RightLeg.addChild(leg_1_R);
        RightLeg.addChild(leg_3_R);
        RightLeg.addChild(leg_2_R);
        RightLeg.addChild(toe_1_R);
        RightLeg.addChild(toe_2_R);
        PixelmonModelRenderer LeftLeg = new PixelmonModelRenderer(this, "Left Leg");
        LeftLeg.setRotationPoint(4.0f, 4.0f, 0.0f);
        PixelmonModelRenderer leg_1_L = new PixelmonModelRenderer(this, 54, 52);
        leg_1_L.addBox(-2.0f, -2.0f, -3.133333f, 4, 7, 5);
        leg_1_L.setTextureSize(128, 64);
        leg_1_L.mirror = true;
        this.setRotation(leg_1_L, 0.8203047f, -0.5585054f, -0.2094395f);
        PixelmonModelRenderer leg_3_L = new PixelmonModelRenderer(this, 73, 49);
        leg_3_L.addBox(-1.466667f, 2.0f, -3.466667f, 4, 4, 4);
        leg_3_L.setTextureSize(128, 64);
        leg_3_L.mirror = true;
        this.setRotation(leg_3_L, 0.0f, -0.4014257f, 0.0f);
        PixelmonModelRenderer leg_2_L = new PixelmonModelRenderer(this, 73, 58);
        leg_2_L.addBox(-1.0f, -0.3666667f, -5.0f, 3, 3, 3);
        leg_2_L.setTextureSize(128, 64);
        leg_2_L.mirror = true;
        this.setRotation(leg_2_L, 0.5410521f, -0.4014257f, 0.0f);
        PixelmonModelRenderer toe_2_L = new PixelmonModelRenderer(this, 27, 0);
        toe_2_L.addBox(1.0f, 5.0f, -4.466667f, 1, 1, 2);
        toe_2_L.setTextureSize(128, 64);
        toe_2_L.mirror = true;
        this.setRotation(toe_2_L, 0.0f, -0.4014257f, 0.0f);
        PixelmonModelRenderer toe_1_L = new PixelmonModelRenderer(this, 27, 0);
        toe_1_L.addBox(-1.0f, 5.0f, -4.466667f, 1, 1, 2);
        toe_1_L.setTextureSize(128, 64);
        toe_1_L.mirror = true;
        this.setRotation(toe_1_L, 0.0f, -0.4014257f, 0.0f);
        LeftLeg.addChild(leg_1_L);
        LeftLeg.addChild(leg_3_L);
        LeftLeg.addChild(leg_2_L);
        LeftLeg.addChild(toe_2_L);
        LeftLeg.addChild(toe_1_L);
        PixelmonModelRenderer LeftArm = new PixelmonModelRenderer(this, "Left Arm");
        LeftArm.setRotationPoint(2.0f, -5.0f, -3.0f);
        PixelmonModelRenderer arm_1_L = new PixelmonModelRenderer(this, 81, 3);
        arm_1_L.addBox(0.0f, 0.0f, -0.6666667f, 2, 2, 2);
        arm_1_L.setTextureSize(128, 64);
        arm_1_L.mirror = true;
        this.setRotation(arm_1_L, 0.0f, -0.1919862f, 0.0f);
        PixelmonModelRenderer arm_2_L = new PixelmonModelRenderer(this, 82, 0);
        arm_2_L.addBox(-0.1f, 0.0f, -1.333333f, 1, 1, 2);
        arm_2_L.setRotationPoint(1.0f, 1.0f, -1.0f);
        arm_2_L.setTextureSize(128, 64);
        arm_2_L.mirror = true;
        this.setRotation(arm_2_L, 0.2617994f, -0.2792527f, 0.0f);
        PixelmonModelRenderer arm_3_L = new PixelmonModelRenderer(this, 89, 0);
        arm_3_L.addBox(-1.166667f, -0.6666667f, -0.6f, 3, 5, 2);
        arm_3_L.setRotationPoint(2.0f, 1.46667f, -2.266667f);
        arm_3_L.setTextureSize(128, 64);
        arm_3_L.mirror = true;
        this.setRotation(arm_3_L, -0.7330383f, -0.4712389f, 0.0f);
        PixelmonModelRenderer arm_4_L = new PixelmonModelRenderer(this, 100, 0);
        arm_4_L.addBox(-1.633333f, 2.333333f, -0.6f, 4, 5, 2);
        arm_4_L.setRotationPoint(2.0f, 1.46667f, -2.266667f);
        arm_4_L.setTextureSize(128, 64);
        arm_4_L.mirror = true;
        this.setRotation(arm_4_L, -0.5235988f, -0.4712389f, 0.0f);
        PixelmonModelRenderer claw_L = new PixelmonModelRenderer(this, 94, 8);
        claw_L.addBox(-0.3f, 6.333333f, -1.4f, 1, 3, 1);
        claw_L.setRotationPoint(2.0f, 1.46667f, -2.266667f);
        claw_L.setTextureSize(128, 64);
        claw_L.mirror = true;
        this.setRotation(claw_L, -0.2792527f, -0.4712389f, 0.0f);
        PixelmonModelRenderer claw_L_2 = new PixelmonModelRenderer(this, 89, 8);
        claw_L_2.addBox(-0.3f, 6.333333f, -3.333333f, 1, 3, 1);
        claw_L_2.setRotationPoint(2.0f, 1.46667f, -2.266667f);
        claw_L_2.setTextureSize(128, 64);
        claw_L_2.mirror = true;
        this.setRotation(claw_L_2, -0.0698132f, -0.4712389f, 0.0f);
        LeftArm.addChild(arm_1_L);
        LeftArm.addChild(arm_2_L);
        LeftArm.addChild(arm_3_L);
        LeftArm.addChild(arm_4_L);
        LeftArm.addChild(claw_L);
        LeftArm.addChild(claw_L_2);
        PixelmonModelRenderer RightArm = new PixelmonModelRenderer(this, "Right Arm");
        RightArm.setRotationPoint(-2.0f, -5.0f, -3.0f);
        PixelmonModelRenderer arm_1_R = new PixelmonModelRenderer(this, 81, 3);
        arm_1_R.addBox(-2.0f, 0.0f, -0.6666667f, 2, 2, 2);
        arm_1_R.setTextureSize(128, 64);
        arm_1_R.mirror = true;
        this.setRotation(arm_1_R, 0.0f, 0.1919862f, 0.0f);
        PixelmonModelRenderer arm_2_R = new PixelmonModelRenderer(this, 82, 0);
        arm_2_R.addBox(-1.1f, 0.0f, -1.333333f, 1, 1, 2);
        arm_2_R.setRotationPoint(-1.0f, 1.0f, -1.0f);
        arm_2_R.setTextureSize(128, 64);
        arm_2_R.mirror = true;
        this.setRotation(arm_2_R, 0.2617994f, 0.2792527f, 0.0f);
        PixelmonModelRenderer arm_3_R = new PixelmonModelRenderer(this, 89, 0);
        arm_3_R.addBox(-1.766667f, -0.6666667f, -0.6f, 3, 5, 2);
        arm_3_R.setRotationPoint(-1.0f, 1.46667f, -2.266667f);
        arm_3_R.setTextureSize(128, 64);
        arm_3_R.mirror = true;
        this.setRotation(arm_3_R, -0.7330383f, 0.4712389f, 0.0f);
        PixelmonModelRenderer arm_4_R = new PixelmonModelRenderer(this, 100, 0);
        arm_4_R.addBox(-2.4f, 2.333333f, -0.6f, 4, 5, 2);
        arm_4_R.setRotationPoint(-2.0f, 1.46667f, -2.266667f);
        arm_4_R.setTextureSize(128, 64);
        arm_4_R.mirror = true;
        this.setRotation(arm_4_R, -0.5235988f, 0.4712389f, 0.0f);
        PixelmonModelRenderer claw_R = new PixelmonModelRenderer(this, 94, 8);
        claw_R.addBox(-0.7f, 6.333333f, -1.4f, 1, 3, 1);
        claw_R.setRotationPoint(-2.0f, 1.46667f, -2.266667f);
        claw_R.setTextureSize(128, 64);
        claw_R.mirror = true;
        this.setRotation(claw_R, -0.2792527f, 0.4712389f, 0.0f);
        PixelmonModelRenderer claw_R_2 = new PixelmonModelRenderer(this, 89, 8);
        claw_R_2.addBox(-0.7f, 6.333333f, -3.333333f, 1, 3, 1);
        claw_R_2.setRotationPoint(-2.0f, 1.46667f, -2.266667f);
        claw_R_2.setTextureSize(128, 64);
        claw_R_2.mirror = true;
        this.setRotation(claw_R_2, -0.0698132f, 0.4712389f, 0.0f);
        RightArm.addChild(arm_1_R);
        RightArm.addChild(arm_2_R);
        RightArm.addChild(arm_3_R);
        RightArm.addChild(arm_4_R);
        RightArm.addChild(claw_R);
        RightArm.addChild(claw_R_2);
        this.Body.addChild(Head);
        this.Body.addChild(Tail);
        this.Body.addChild(LeftArm);
        this.Body.addChild(RightArm);
        this.Body.addChild(LeftLeg);
        this.Body.addChild(RightLeg);
        ModuleArm leftArmModule = new ModuleArm(LeftArm, EnumArm.Left, EnumRotation.x, 1.0f, 0.5f);
        ModuleArm rightArmModule = new ModuleArm(RightArm, EnumArm.Right, EnumRotation.x, 1.0f, 0.5f);
        ModuleHead headModule = new ModuleHead(Head);
        float legspeed = 0.5f;
        float legRotationLimit = 1.4f;
        ModuleLeg leftLegModule = new ModuleLeg(LeftLeg, EnumLeg.FrontLeft, EnumPhase.InPhase, EnumRotation.x, legRotationLimit, legspeed);
        ModuleLeg rightLegModule = new ModuleLeg(RightLeg, EnumLeg.FrontRight, EnumPhase.InPhase, EnumRotation.x, legRotationLimit, legspeed);
        ModuleTailBasic tailModule = new ModuleTailBasic(Tail, 0.2f, 0.05f, legspeed);
        this.skeleton = new SkeletonBiped(this.Body, headModule, leftArmModule, rightArmModule, leftLegModule, rightLegModule, tailModule);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5);
        this.Body.render(f5);
    }

    private void setRotation(PixelmonModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5) {
    }
}

