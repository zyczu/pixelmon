/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations;

import com.pixelmongenerations.client.models.animations.IModulized;
import com.pixelmongenerations.client.models.animations.Module;

public abstract class ModuleTailComplex
extends Module {
    IModulized tailpiece;
    float TailPieceRotationLimitY;
    float TailPieceRotationLimitZ;
    float TailPieceSpeed;
    float TurningAngle;
}

