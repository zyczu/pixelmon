/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations.serpent;

import com.pixelmongenerations.client.models.animations.EnumGeomData;
import com.pixelmongenerations.client.models.animations.IModulized;
import com.pixelmongenerations.client.models.animations.ModuleHead;
import com.pixelmongenerations.client.models.animations.SkeletonBase;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

public class SkeletonSerpentFloating
extends SkeletonBase {
    public IModulized[] bodyParts;
    public int[] lengths;
    float animationAngle;
    float animationSpeed;
    float currentAngleX;
    float currentAngleZ;
    float topAngle;
    float dampeningFactor;
    float phaseoffset;

    public SkeletonSerpentFloating(ModelRenderer body, ModuleHead headModule, float animationAngle, float topAngle, float dampeningFactor, float animationSpeed, float phaseoffset, IModulized ... bodyArgs) {
        super(body);
        this.modules.add(headModule);
        this.animationAngle = animationAngle;
        this.topAngle = topAngle;
        this.dampeningFactor = dampeningFactor;
        this.animationSpeed = animationSpeed;
        this.phaseoffset = phaseoffset;
        this.bodyParts = bodyArgs;
        this.lengths = new int[this.bodyParts.length];
    }

    @Override
    public void walk(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        for (int i = 0; i < this.bodyParts.length; ++i) {
            float zrD;
            float xrD;
            if (i == 0) {
                xrD = MathHelper.cos((float)Math.toRadians(this.animationAngle)) * (float)Math.toRadians(this.topAngle) * MathHelper.cos(f2 * this.animationSpeed);
                zrD = MathHelper.sin((float)Math.toRadians(this.animationAngle)) * (float)Math.toRadians(this.topAngle) * MathHelper.cos(f2 * this.animationSpeed);
                this.bodyParts[i].setValue(xrD, EnumGeomData.xrot);
                this.bodyParts[i].setValue(zrD, EnumGeomData.zrot);
                this.currentAngleX = xrD;
                this.currentAngleZ = zrD;
                continue;
            }
            xrD = MathHelper.cos((float)Math.toRadians(this.animationAngle)) * ((float)Math.exp(this.dampeningFactor * (float)i) * (float)Math.toRadians(this.topAngle) * MathHelper.cos(f2 * this.animationSpeed + (float)i * this.phaseoffset) - this.currentAngleX);
            zrD = MathHelper.sin((float)Math.toRadians(this.animationAngle)) * ((float)Math.exp(this.dampeningFactor * (float)i) * (float)Math.toRadians(this.topAngle) * MathHelper.cos(f2 * this.animationSpeed + (float)i * this.phaseoffset) - this.currentAngleZ);
            this.bodyParts[i].setValue(xrD, EnumGeomData.xrot);
            this.bodyParts[i].setValue(zrD, EnumGeomData.zrot);
            this.currentAngleX = xrD + this.currentAngleX;
            this.currentAngleZ = zrD + this.currentAngleZ;
        }
    }

    @Override
    public void fly(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        for (int i = 0; i < this.bodyParts.length; ++i) {
            float zrD;
            float xrD;
            if (i == 0) {
                xrD = MathHelper.cos((float)Math.toRadians(this.animationAngle)) * (float)Math.toRadians(this.topAngle) * MathHelper.cos(f2 * this.animationSpeed);
                zrD = MathHelper.sin((float)Math.toRadians(this.animationAngle)) * (float)Math.toRadians(this.topAngle) * MathHelper.cos(f2 * this.animationSpeed);
                this.bodyParts[i].setValue(xrD, EnumGeomData.xrot);
                this.bodyParts[i].setValue(zrD, EnumGeomData.zrot);
                this.currentAngleX = xrD;
                this.currentAngleZ = zrD;
                continue;
            }
            xrD = MathHelper.cos((float)Math.toRadians(this.animationAngle)) * ((float)Math.exp(this.dampeningFactor * (float)i) * (float)Math.toRadians(this.topAngle) * MathHelper.cos(f2 * this.animationSpeed + (float)i * this.phaseoffset) - this.currentAngleX);
            zrD = MathHelper.sin((float)Math.toRadians(this.animationAngle)) * ((float)Math.exp(this.dampeningFactor * (float)i) * (float)Math.toRadians(this.topAngle) * MathHelper.cos(f2 * this.animationSpeed + (float)i * this.phaseoffset) - this.currentAngleZ);
            this.bodyParts[i].setValue(xrD, EnumGeomData.xrot);
            this.bodyParts[i].setValue(zrD, EnumGeomData.zrot);
            this.currentAngleX = xrD + this.currentAngleX;
            this.currentAngleZ = zrD + this.currentAngleZ;
        }
    }
}

