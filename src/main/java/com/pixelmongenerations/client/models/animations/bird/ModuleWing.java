/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations.bird;

import com.pixelmongenerations.client.models.animations.EnumGeomData;
import com.pixelmongenerations.client.models.animations.IModulized;
import com.pixelmongenerations.client.models.animations.Module;
import com.pixelmongenerations.client.models.animations.ModulizedRenderWrapper;
import com.pixelmongenerations.client.models.animations.bird.EnumWing;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

public class ModuleWing
extends Module {
    IModulized wing;
    float WingRotationLimit;
    float WingSpeed;
    float WingInitY;
    float WingInitZ;
    float WingDirection;
    float WingOrientation;
    EnumWing WingVariable;

    public ModuleWing(IModulized wing, EnumWing WingVariable, float WingOrientation, float WingRotationLimit, float WingSpeed) {
        this.wing = wing;
        this.WingSpeed = WingSpeed;
        this.WingRotationLimit = WingRotationLimit;
        this.WingOrientation = WingOrientation;
        this.WingInitY = wing.getValue(EnumGeomData.yrot);
        this.WingInitZ = wing.getValue(EnumGeomData.zrot);
        this.WingVariable = WingVariable;
        this.WingDirection = WingVariable == EnumWing.Right ? 1.0f : -1.0f;
    }

    public ModuleWing(ModelRenderer wing, EnumWing WingVariable, float WingOrientation, float WingRotationLimit, float WingSpeed) {
        this(new ModulizedRenderWrapper(wing), WingVariable, WingOrientation, WingRotationLimit, WingSpeed);
    }

    @Override
    public void walk(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        this.yrD = MathHelper.cos((float)Math.toRadians(this.WingOrientation)) * this.WingDirection * MathHelper.cos(f2 * this.WingSpeed) * (float)Math.PI * this.WingRotationLimit;
        this.zrD = MathHelper.sin((float)Math.toRadians(this.WingOrientation)) * this.WingDirection * MathHelper.cos(f2 * this.WingSpeed) * (float)Math.PI * this.WingRotationLimit;
        this.wing.setValue(this.yrD, EnumGeomData.yrot);
        this.wing.setValue(this.zrD, EnumGeomData.zrot);
    }

    @Override
    public void fly(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        this.walk(entity, f, f1, f2, f3, f4);
    }
}

