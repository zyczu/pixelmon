/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations;

import com.pixelmongenerations.client.models.animations.Module;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import java.util.ArrayList;
import net.minecraft.client.model.ModelRenderer;

public class SkeletonBase {
    protected float toDegrees = 57.29578f;
    protected float toRadians = 1.0f / this.toDegrees;
    protected ArrayList<Module> modules = new ArrayList();
    public ModelRenderer body;

    public SkeletonBase(ModelRenderer body) {
        this.body = body;
    }

    public void add(Module module) {
        this.modules.add(module);
    }

    public void walk(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        for (Module m : this.modules) {
            m.walk(entity, f, f1, f2, f3, f4);
        }
    }

    public void fly(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        for (Module m : this.modules) {
            m.fly(entity, f, f1, f2, f3, f4);
        }
    }

    public void swim(Entity2HasModel pixelmon, float f, float f1, float f2, float f3, float f4) {
    }
}

