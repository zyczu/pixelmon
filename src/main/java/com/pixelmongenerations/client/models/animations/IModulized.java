/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations;

import com.pixelmongenerations.client.models.animations.EnumGeomData;

public interface IModulized {
    public float setValue(float var1, EnumGeomData var2);

    public float getValue(EnumGeomData var1);
}

