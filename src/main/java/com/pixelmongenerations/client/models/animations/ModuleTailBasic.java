/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.models.animations;

import com.pixelmongenerations.client.models.animations.EnumGeomData;
import com.pixelmongenerations.client.models.animations.IModulized;
import com.pixelmongenerations.client.models.animations.Module;
import com.pixelmongenerations.client.models.animations.ModulizedRenderWrapper;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

public class ModuleTailBasic
extends Module {
    IModulized tail;
    float TailRotationLimitY;
    float TailRotationLimitX;
    float TailSpeed;
    float TailInitY;
    float TailInitX;
    float TailOrientation;
    float TurningSpeed;
    float TurningAngle;

    public ModuleTailBasic(IModulized tail, float TailRotationLimitY, float TailRotationLimitX, float TailSpeed) {
        this.tail = tail;
        this.TailSpeed = TailSpeed;
        this.TailRotationLimitY = TailRotationLimitY;
        this.TailRotationLimitX = TailRotationLimitX;
        this.TailInitY = this.tail.getValue(EnumGeomData.yrot);
        this.TailInitX = this.tail.getValue(EnumGeomData.xrot);
    }

    public ModuleTailBasic(ModelRenderer tail, float TailRotationLimitY, float TailRotationLimitX, float TailSpeed) {
        this(new ModulizedRenderWrapper(tail), TailRotationLimitY, TailRotationLimitX, TailSpeed);
    }

    @Override
    public void walk(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
        this.TurningSpeed = Math.abs(entity.rotationYaw - entity.prevRotationYaw);
        this.yrD = MathHelper.cos(f * this.TailSpeed) * (float)Math.PI * f1 * this.TailRotationLimitY + this.TurningAngle;
        this.xrD = MathHelper.cos(f * this.TailSpeed * 2.0f) * (float)Math.PI * f1 * this.TailRotationLimitX;
        this.tail.setValue(this.yrD, EnumGeomData.yrot);
        this.tail.setValue(this.xrD, EnumGeomData.xrot);
        for (Module m : this.modules) {
            m.walk(entity, f, f1, f2, f3, f4);
        }
    }

    @Override
    public void fly(Entity2HasModel entity, float f, float f1, float f2, float f3, float f4) {
    }
}

