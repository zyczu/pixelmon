/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.event;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.world.ModBiomes;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.ActiveRenderInfo;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.client.event.EntityViewRenderEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class BiomeFog {
    @SubscribeEvent
    public void checkFog(EntityViewRenderEvent.FogColors event) {
        Biome biome;
        if (!(event.getEntity() instanceof EntityPlayer)) {
            return;
        }
        Entity entity = event.getEntity();
        if (entity.getRidingEntity() != null && entity.getRidingEntity() instanceof EntityPixelmon) {
            EntityPixelmon p = (EntityPixelmon)entity.getRidingEntity();
            IBlockState blockstate = ActiveRenderInfo.getBlockStateAtEntityViewpoint(entity.world, entity, (float)event.getRenderPartialTicks());
            if (p.baseStats != null && p.baseStats.canSurf && blockstate.getMaterial() == Material.WATER) {
                event.setBlue(event.getBlue() * 6.0f + 0.3f);
                event.setRed(event.getRed() * 6.0f + 0.3f);
                event.setGreen(event.getGreen() * 6.0f + 0.3f);
            }
        }
        if ((biome = event.getEntity().getEntityWorld().getBiome(event.getEntity().getPosition())) == ModBiomes.ultraforest) {
            event.setRed(0.251f);
            event.setGreen(0.139f);
            event.setBlue(0.215f);
        } else if (biome == ModBiomes.ultraburst) {
            event.setRed(0.179f);
            event.setBlue(0.179f);
        } else if (biome == ModBiomes.ultradarkforest) {
            event.setRed(0.205f);
            event.setGreen(0.034f);
            event.setBlue(0.25f);
        } else if (biome == ModBiomes.ultrajungle) {
            event.setRed(0.102f);
        } else if (biome == ModBiomes.ultradesert) {
            event.setGreen(0.255f);
            event.setBlue(0.255f);
        } else if (biome == ModBiomes.ultradeepsea) {
            event.setBlue(0.255f);
        } else if (biome == ModBiomes.ultraswamp) {
            event.setBlue(0.255f);
            event.setGreen(0.128f);
        } else if (biome == ModBiomes.ultraruin) {
            event.setRed(0.255f);
            event.setBlue(0.163f);
            event.setGreen(0.26f);
        }
    }

    @SubscribeEvent
    public void checkFogDensity(EntityViewRenderEvent.FogDensity event) {
        Biome biome;
        if (!(event.getEntity() instanceof EntityPlayer)) {
            return;
        }
        Entity entity = event.getEntity();
        if (entity instanceof EntityPlayer && entity.getRidingEntity() != null && entity.getRidingEntity() instanceof EntityPixelmon) {
            EntityPixelmon p = (EntityPixelmon)entity.getRidingEntity();
            IBlockState blockstate = ActiveRenderInfo.getBlockStateAtEntityViewpoint(entity.world, entity, (float)event.getRenderPartialTicks());
            if (p.baseStats != null && p.baseStats.canSurf && blockstate.getMaterial() == Material.WATER) {
                GlStateManager.setFog(GlStateManager.FogMode.EXP);
                event.setDensity(0.01f);
                event.setCanceled(true);
            }
        }
        if ((biome = event.getEntity().getEntityWorld().getBiome(event.getEntity().getPosition())) == ModBiomes.ultraforest) {
            event.setDensity(1000.0f);
        }
    }
}

