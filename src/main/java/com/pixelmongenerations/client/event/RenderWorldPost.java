/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.event;

import com.pixelmongenerations.api.board.DisplayBoard;
import com.pixelmongenerations.api.world.BoxZone;
import com.pixelmongenerations.client.ClientBoardManager;
import com.pixelmongenerations.client.util.RenderUtil;
import com.pixelmongenerations.common.item.ItemZoneWand;
import java.awt.Color;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class RenderWorldPost {
    @SubscribeEvent
    public void onWorldRenderLast(RenderWorldLastEvent event) {
        Minecraft mc = Minecraft.getMinecraft();
        float partialTicks = event.getPartialTicks();
        EntityPlayerSP player = Minecraft.getMinecraft().player;
        Vec3d eyePos = mc.getRenderViewEntity().getPositionEyes(event.getPartialTicks());
        Vec3d lookPos = mc.getRenderViewEntity().getLook(event.getPartialTicks());
        Vec3d eyePosForward = eyePos.add(lookPos.x * 15.0, lookPos.y * 15.0, lookPos.z * 15.0);
        for (DisplayBoard board : ClientBoardManager.instance().boards()) {
            RenderUtil.drawBoard(board, partialTicks, eyePos, eyePosForward);
        }
        if (ItemZoneWand.isValidNBTWand(player)) {
            ItemStack heldStack = player.getHeldItem(EnumHand.MAIN_HAND);
            NBTTagCompound tag = heldStack.getTagCompound();
            try {
                Vec3d pos1 = ItemZoneWand.getPosition(tag, ItemZoneWand.SelectPoint.One);
                Vec3d pos2 = ItemZoneWand.getPosition(tag, ItemZoneWand.SelectPoint.Two);
                Entity entity = Minecraft.getMinecraft().getRenderViewEntity();
                double d0 = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * (double)event.getPartialTicks();
                double d1 = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * (double)event.getPartialTicks();
                double d2 = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * (double)event.getPartialTicks();
                Vec3d min = new Vec3d(Math.min(pos1.x, pos2.x), Math.min(pos1.y, pos2.y), Math.min(pos1.z, pos2.z));
                Vec3d max = new Vec3d(Math.max(pos1.x, pos2.x), Math.max(pos1.y, pos2.y), Math.max(pos1.z, pos2.z));
                Tessellator.getInstance().getBuffer().setTranslation(-d0, -d1, -d2);
                RenderUtil.drawBoxZone(BoxZone.of("Zone Selection", new Color(255, 255, 255, 150), min.add(0.0, 0.01, 0.0), max.add(1.0, 1.0, 1.0)));
                Tessellator.getInstance().getBuffer().setTranslation(0.0, 0.0, 0.0);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

