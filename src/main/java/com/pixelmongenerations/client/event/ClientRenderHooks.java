/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.event;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.render.tileEntities.RenderTileEntityPokeChest;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(value={Side.CLIENT}, modid="pixelmon")
public class ClientRenderHooks {
    public static float partialTicks;

    @SubscribeEvent
    public static void renderTick(TickEvent.RenderTickEvent event) {
        if (event.phase == TickEvent.Phase.START) {
            partialTicks = event.renderTickTime;
        }
    }

    @SubscribeEvent
    public static void renderTick(TickEvent.ClientTickEvent event) {
        if (event.phase == TickEvent.Phase.START) {
            RenderTileEntityPokeChest.frame += 1.0f * partialTicks;
            GuiPixelmonOverlay.updateOverlays();
        }
    }
}

