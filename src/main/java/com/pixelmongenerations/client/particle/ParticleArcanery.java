/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle;

import com.pixelmongenerations.client.particle.ParticleEffect;
import com.pixelmongenerations.client.particle.ParticleEvents;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(value=Side.CLIENT)
public class ParticleArcanery
extends Particle {
    private static final List<ParticleArcanery> queue = new ArrayList<ParticleArcanery>();
    public ParticleEffect effect;
    public float rotationX;
    public float rotationZ;
    public float rotationYZ;
    public float rotationXY;
    public float rotationXZ;

    public ParticleArcanery(World world, double x, double y, double z, double vx, double vy, double vz, ParticleEffect effect) {
        this(world, x, y, z, vx, vy, vz, 0.5f, effect);
    }

    public ParticleArcanery(World world, double x, double y, double z, double vx, double vy, double vz, float size, ParticleEffect effect) {
        super(world, x, y, z, vx, vy, vz);
        this.effect = effect;
        effect.init(this, world, x, y, z, vx, vy, vz, size);
        this.onUpdate();
    }

    public void setRGBA(float r, float g, float b, float a) {
        this.particleRed = r;
        this.particleGreen = g;
        this.particleBlue = b;
        this.particleAlpha = a;
    }

    public double getMotionX() {
        return this.motionX;
    }

    public double getMotionY() {
        return this.motionY;
    }

    public double getMotionZ() {
        return this.motionZ;
    }

    public void setMotion(double x, double y, double z) {
        this.motionX = x;
        this.motionY = y;
        this.motionZ = z;
    }

    public void setOnGround() {
        this.onGround = true;
    }

    @Override
    public void setMaxAge(int a) {
        this.particleMaxAge = a;
    }

    public int getMaxAge() {
        return this.particleMaxAge;
    }

    public void incrementAge() {
        ++this.particleAge;
    }

    public int getAge() {
        return this.particleAge;
    }

    public void setScale(float s) {
        this.particleScale = s;
    }

    public float getScale() {
        return this.particleScale;
    }

    public double getX() {
        return this.posX;
    }

    public double getY() {
        return this.posY;
    }

    public double getZ() {
        return this.posZ;
    }

    public void setPos(double x, double y, double z) {
        this.posX = x;
        this.posY = y;
        this.posZ = z;
    }

    public double getPrevX() {
        return this.prevPosX;
    }

    public double getPrevY() {
        return this.prevPosY;
    }

    public double getPrevZ() {
        return this.prevPosZ;
    }

    public void setPrevPos(double x, double y, double z) {
        this.prevPosX = x;
        this.prevPosY = y;
        this.prevPosZ = z;
    }

    public void setHeading(double x, double y, double z, float velocity, float inaccuracy) {
        float f = MathHelper.sqrt(x * x + y * y + z * z);
        x /= (double)f;
        y /= (double)f;
        z /= (double)f;
        x += this.rand.nextGaussian() * (double)0.0075f * (double)inaccuracy;
        y += this.rand.nextGaussian() * (double)0.0075f * (double)inaccuracy;
        z += this.rand.nextGaussian() * (double)0.0075f * (double)inaccuracy;
        this.motionX = x *= (double)velocity;
        this.motionY = y *= (double)velocity;
        this.motionZ = z *= (double)velocity;
    }

    public boolean onGround() {
        return this.onGround;
    }

    public static void dispatch(Tessellator tessellator, float ticks) {
        ParticleEvents.arcaneryParticleCount = 0;
        while (!queue.isEmpty()) {
            ParticleArcanery particle = queue.get(0);
            if (particle == null) continue;
            if (particle.getEffect().customRenderer()) {
                particle.getEffect().preRender(particle, ticks);
                particle.getEffect().render(particle, tessellator, ticks);
                particle.getEffect().postRender(particle, ticks);
                GlStateManager.resetColor();
                GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            } else {
                Minecraft mc = Minecraft.getMinecraft();
                ResourceLocation texture = particle.getTexture();
                if (texture != null) {
                    mc.getTextureManager().bindTexture(particle.getTexture());
                }
                GlStateManager.resetColor();
                GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
                particle.getEffect().preRender(particle, ticks);
                GlStateManager.enableAlpha();
                GlStateManager.enableBlend();
                GlStateManager.blendFunc(770, 771);
                tessellator.getBuffer().begin(7, DefaultVertexFormats.POSITION_TEX_LMAP_COLOR);
                particle.render(tessellator, ticks);
                tessellator.draw();
                particle.getEffect().postRender(particle, ticks);
            }
            queue.remove(0);
        }
    }

    @Override
    public void renderParticle(BufferBuilder buffer, Entity entityIn, float partialTicks, float rotationX, float rotationZ, float rotationYZ, float rotationXY, float rotationXZ) {
        this.rotationX = rotationX;
        this.rotationZ = rotationZ;
        this.rotationYZ = rotationYZ;
        this.rotationXY = rotationXY;
        this.rotationXZ = rotationXZ;
        queue.add(this);
    }

    public float getAngle() {
        return this.particleAngle;
    }

    public float getPrevAngle() {
        return this.prevParticleAngle;
    }

    public float getAlphaF() {
        return this.particleAlpha;
    }

    public ResourceLocation getTexture() {
        return this.effect.texture();
    }

    public void render(Tessellator tessellator, float ticks) {
        ++ParticleEvents.arcaneryParticleCount;
        float f4 = this.particleScale;
        int combined = 0xF000F0;
        int k3 = combined >> 16 & 0xFFFF;
        int l3 = combined & 0xFFFF;
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        float f5 = (float)(this.prevPosX + (this.posX - this.prevPosX) * (double)ticks - interpPosX);
        float f6 = (float)(this.prevPosY + (this.posY - this.prevPosY) * (double)ticks - interpPosY);
        float f7 = (float)(this.prevPosZ + (this.posZ - this.prevPosZ) * (double)ticks - interpPosZ);
        Vec3d[] avec3d = new Vec3d[]{new Vec3d(-this.rotationX * f4 - this.rotationXY * f4, -this.rotationZ * f4, -this.rotationYZ * f4 - this.rotationXZ * f4), new Vec3d(-this.rotationX * f4 + this.rotationXY * f4, this.rotationZ * f4, -this.rotationYZ * f4 + this.rotationXZ * f4), new Vec3d(this.rotationX * f4 + this.rotationXY * f4, this.rotationZ * f4, this.rotationYZ * f4 + this.rotationXZ * f4), new Vec3d(this.rotationX * f4 - this.rotationXY * f4, -this.rotationZ * f4, this.rotationYZ * f4 - this.rotationXZ * f4)};
        if (this.particleAngle != 0.0f) {
            float f8 = this.particleAngle + (this.particleAngle - this.prevParticleAngle) * ticks;
            float f9 = MathHelper.cos(f8 * 0.5f);
            float f10 = MathHelper.sin(f8 * 0.5f) * (float)ParticleArcanery.cameraViewDir.x;
            float f11 = MathHelper.sin(f8 * 0.5f) * (float)ParticleArcanery.cameraViewDir.y;
            float f12 = MathHelper.sin(f8 * 0.5f) * (float)ParticleArcanery.cameraViewDir.z;
            Vec3d vec3d = new Vec3d(f10, f11, f12);
            for (int l = 0; l < 4; ++l) {
                avec3d[l] = vec3d.scale(2.0 * avec3d[l].dotProduct(vec3d)).add(avec3d[l].scale((double)(f9 * f9) - vec3d.dotProduct(vec3d))).add(vec3d.crossProduct(avec3d[l]).scale(2.0f * f9));
            }
        }
        tessellator.getBuffer().pos((double)f5 + avec3d[0].x, (double)f6 + avec3d[0].y, (double)f7 + avec3d[0].z).tex(0.0, 1.0).lightmap(k3, l3).color(this.particleRed, this.particleGreen, this.particleBlue, this.particleAlpha).endVertex();
        tessellator.getBuffer().pos((double)f5 + avec3d[1].x, (double)f6 + avec3d[1].y, (double)f7 + avec3d[1].z).tex(1.0, 1.0).lightmap(k3, l3).color(this.particleRed, this.particleGreen, this.particleBlue, this.particleAlpha).endVertex();
        tessellator.getBuffer().pos((double)f5 + avec3d[2].x, (double)f6 + avec3d[2].y, (double)f7 + avec3d[2].z).tex(1.0, 0.0).lightmap(k3, l3).color(this.particleRed, this.particleGreen, this.particleBlue, this.particleAlpha).endVertex();
        tessellator.getBuffer().pos((double)f5 + avec3d[3].x, (double)f6 + avec3d[3].y, (double)f7 + avec3d[3].z).tex(0.0, 0.0).lightmap(k3, l3).color(this.particleRed, this.particleGreen, this.particleBlue, this.particleAlpha).endVertex();
    }

    @Override
    public boolean shouldDisableDepth() {
        return true;
    }

    @Override
    public int getFXLayer() {
        return 3;
    }

    public ParticleEffect getEffect() {
        return this.effect;
    }

    @Override
    public void onUpdate() {
        this.effect.update(this);
    }

    public void setAngle(float theta) {
        this.prevParticleAngle = this.particleAngle;
        this.particleAngle = theta;
    }

    public Random getRand() {
        return this.rand;
    }

    public World getWorld() {
        return this.world;
    }
}

