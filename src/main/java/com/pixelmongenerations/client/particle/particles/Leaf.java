/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle.particles;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleEffect;
import java.util.Random;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class Leaf
extends ParticleEffect {
    private double swingArc;
    private short timeOnGround = 0;
    private double currentRot = 0.0;
    private boolean rotIncreasing = true;
    private float scale;
    private boolean shiny;
    private static final ResourceLocation tex1 = new ResourceLocation("pixelmon", "textures/particles/petal.png");
    private static final ResourceLocation tex2 = new ResourceLocation("pixelmon", "textures/particles/leaf.png");

    public Leaf(double swingArc, float scale, boolean shiny) {
        this.swingArc = swingArc;
        this.scale = scale;
        this.shiny = shiny;
    }

    @Override
    public void preRender(ParticleArcanery particle, float partialTicks) {
        GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
    }

    @Override
    public boolean customRenderer() {
        return true;
    }

    @Override
    public void render(ParticleArcanery particle, Tessellator tessellator, float partialTicks) {
        Minecraft mc = Minecraft.getMinecraft();
        mc.getTextureManager().bindTexture(particle.getTexture());
        tessellator.getBuffer().begin(7, DefaultVertexFormats.POSITION_TEX_LMAP_COLOR);
        float f4 = particle.getScale();
        int combined = 0xF000F0;
        int k3 = combined >> 16 & 0xFFFF;
        int l3 = combined & 0xFFFF;
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        float f5 = (float)(particle.getPrevX() + (particle.getX() - particle.getPrevX()) * (double)partialTicks - ParticleArcanery.interpPosX);
        float f6 = (float)(particle.getPrevY() + (particle.getY() - particle.getPrevY()) * (double)partialTicks - ParticleArcanery.interpPosY);
        float f7 = (float)(particle.getPrevZ() + (particle.getZ() - particle.getPrevZ()) * (double)partialTicks - ParticleArcanery.interpPosZ);
        particle.getBrightnessForRender(partialTicks);
        Vec3d[] avec3d = new Vec3d[]{new Vec3d(-particle.rotationX * f4 - particle.rotationXY * f4, -particle.rotationZ * f4, -particle.rotationYZ * f4 - particle.rotationXZ * f4), new Vec3d(-particle.rotationX * f4 + particle.rotationXY * f4, particle.rotationZ * f4, -particle.rotationYZ * f4 + particle.rotationXZ * f4), new Vec3d(particle.rotationX * f4 + particle.rotationXY * f4, particle.rotationZ * f4, particle.rotationYZ * f4 + particle.rotationXZ * f4), new Vec3d(particle.rotationX * f4 - particle.rotationXY * f4, -particle.rotationZ * f4, particle.rotationYZ * f4 - particle.rotationXZ * f4)};
        if (particle.getAngle() != 0.0f) {
            float f8 = particle.getAngle() + (particle.getAngle() - particle.getPrevAngle()) * partialTicks;
            float f9 = MathHelper.cos(f8 * 0.5f);
            float f10 = MathHelper.sin(f8 * 0.5f) * (float)ParticleArcanery.cameraViewDir.x;
            float f11 = MathHelper.sin(f8 * 0.5f) * (float)ParticleArcanery.cameraViewDir.y;
            float f12 = MathHelper.sin(f8 * 0.5f) * (float)ParticleArcanery.cameraViewDir.z;
            Vec3d vec3d = new Vec3d(f10, f11, f12);
            for (int l = 0; l < 4; ++l) {
                avec3d[l] = vec3d.scale(2.0 * avec3d[l].dotProduct(vec3d)).add(avec3d[l].scale((double)(f9 * f9) - vec3d.dotProduct(vec3d))).add(vec3d.crossProduct(avec3d[l]).scale(2.0f * f9));
            }
        }
        tessellator.getBuffer().pos((double)f5 + avec3d[0].x, (double)f6 + avec3d[0].y, (double)f7 + avec3d[0].z).tex(0.0, 1.0).lightmap(k3, l3).color(particle.getRedColorF(), particle.getGreenColorF(), particle.getBlueColorF(), particle.getAlphaF()).endVertex();
        tessellator.getBuffer().pos((double)f5 + avec3d[1].x, (double)f6 + avec3d[1].y, (double)f7 + avec3d[1].z).tex(1.0, 1.0).lightmap(k3, l3).color(particle.getRedColorF(), particle.getGreenColorF(), particle.getBlueColorF(), particle.getAlphaF()).endVertex();
        tessellator.getBuffer().pos((double)f5 + avec3d[2].x, (double)f6 + avec3d[2].y, (double)f7 + avec3d[2].z).tex(1.0, 0.0).lightmap(k3, l3).color(particle.getRedColorF(), particle.getGreenColorF(), particle.getBlueColorF(), particle.getAlphaF()).endVertex();
        tessellator.getBuffer().pos((double)f5 + avec3d[3].x, (double)f6 + avec3d[3].y, (double)f7 + avec3d[3].z).tex(0.0, 0.0).lightmap(k3, l3).color(particle.getRedColorF(), particle.getGreenColorF(), particle.getBlueColorF(), particle.getAlphaF()).endVertex();
        tessellator.draw();
    }

    @Override
    public void init(ParticleArcanery particle, World world, double x, double y, double z, double vx, double vy, double vz, float size) {
        new Random();
        particle.setRGBA(1.0f, 1.0f, 1.0f, 1.0f);
        particle.setMotion(particle.getMotionX() + vx, particle.getMotionY() + vy, particle.getMotionZ() + vz);
        particle.setScale(this.scale / 3.0f);
        particle.setMaxAge(40);
    }

    @Override
    public void update(ParticleArcanery particle) {
        particle.setPrevPos(particle.getX(), particle.getY(), particle.getZ());
        particle.incrementAge();
        if (particle.getAge() > 15 && Minecraft.getMinecraft().world.getBlockState(new BlockPos(particle.getX(), particle.getY() - 0.3, particle.getZ())).isTopSolid()) {
            particle.setOnGround();
        }
        if (particle.onGround()) {
            this.timeOnGround = (short)(this.timeOnGround + 1);
            particle.setRGBA(1.0f, 1.0f - (float)this.timeOnGround / (float)particle.getMaxAge() / 2.5f, 1.0f - (float)this.timeOnGround / (float)particle.getMaxAge(), 1.0f - (float)this.timeOnGround / (float)particle.getMaxAge());
        } else {
            if (particle.getMotionY() < 0.0) {
                if (this.rotIncreasing) {
                    this.currentRot += 0.2 * Math.max(0.2, Math.abs(this.swingArc + this.currentRot));
                    particle.setAngle((float)this.currentRot);
                    if ((double)particle.getAngle() >= this.swingArc) {
                        this.rotIncreasing = false;
                    }
                } else {
                    this.currentRot -= 0.2 * Math.max(0.2, Math.abs(this.swingArc - this.currentRot));
                    particle.setAngle((float)this.currentRot);
                    if ((double)particle.getAngle() <= -this.swingArc) {
                        this.rotIncreasing = true;
                    }
                }
            }
            particle.move(particle.getMotionX(), particle.getMotionY(), particle.getMotionZ());
            particle.setMotion(particle.getMotionX() / 2.0, Math.max(-0.15, particle.getMotionY() - 0.1), particle.getMotionZ() / 2.0);
        }
        if (this.timeOnGround >= particle.getMaxAge()) {
            particle.setExpired();
        }
    }

    @Override
    public ResourceLocation texture() {
        return this.shiny ? tex1 : tex2;
    }
}

