/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle.particles;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleEffect;
import java.util.Random;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class ShadowBlob
extends ParticleEffect {
    private double random;
    private static final ResourceLocation tex1 = new ResourceLocation("pixelmon", "textures/particles/shadow1.png");
    private static final ResourceLocation tex2 = new ResourceLocation("pixelmon", "textures/particles/shadow2.png");
    private static final ResourceLocation tex3 = new ResourceLocation("pixelmon", "textures/particles/shadow3.png");

    public ShadowBlob(double random) {
        this.random = random;
    }

    @Override
    public void preRender(ParticleArcanery particle, float partialTicks) {
        GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
    }

    @Override
    public void init(ParticleArcanery particle, World world, double x, double y, double z, double vx, double vy, double vz, float size) {
        Random rand = new Random();
        particle.setRGBA(1.0f - rand.nextFloat() / 10.0f, 1.0f - rand.nextFloat() / 10.0f, 1.0f - rand.nextFloat() / 10.0f, 1.0f - rand.nextFloat() / 10.0f);
        particle.setMotion(particle.getMotionX() * (double)0.1f + this.random * (rand.nextGaussian() - 0.5), particle.getMotionY() * (double)0.1f + this.random * (rand.nextGaussian() - 0.5), particle.getMotionZ() * (double)0.1f + this.random * (rand.nextGaussian() - 0.5));
        particle.setScale(0.15f);
        particle.setMaxAge(6);
    }

    @Override
    public void update(ParticleArcanery particle) {
        particle.setPrevPos(particle.getX(), particle.getY(), particle.getZ());
        particle.incrementAge();
        if (particle.getAge() >= particle.getMaxAge()) {
            particle.setExpired();
        }
        particle.setAlphaF((float)particle.getAge() / (float)particle.getMaxAge());
        particle.move(particle.getMotionX(), particle.getMotionY(), particle.getMotionZ());
        particle.setMotion(particle.getMotionX() * (double)0.7f, particle.getMotionY() * (double)0.7f, particle.getMotionZ() * (double)0.7f);
    }

    @Override
    public ResourceLocation texture() {
        switch (new Random().nextInt(3)) {
            case 0: {
                return tex1;
            }
            case 1: {
                return tex2;
            }
        }
        return tex3;
    }
}

