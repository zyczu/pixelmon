/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle.particles;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleEffect;
import java.util.Random;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class CycloneBlob
extends ParticleEffect {
    private double random;
    private double mx;
    private double mz;
    private double theta;
    private double wu;
    private double r;
    private boolean yellow;
    private static final ResourceLocation tex = new ResourceLocation("pixelmon", "textures/particles/purple_magic.png");

    public CycloneBlob(double random, double theta, double wu, double r, double y) {
        this.random = random;
        this.theta = theta;
        this.wu = wu;
        this.r = r;
        this.yellow = y == 1.0;
    }

    @Override
    public void preRender(ParticleArcanery particle, float partialTicks) {
        GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
    }

    @Override
    public void init(ParticleArcanery particle, World world, double x, double y, double z, double vx, double vy, double vz, float size) {
        Random rand = new Random();
        particle.setRGBA(1.0f, this.yellow ? 1.0f : 0.0f, this.yellow ? 1.0f : 0.0f, 0.4f);
        particle.setMotion(particle.getMotionX() * (double)0.1f + this.random * (rand.nextGaussian() - 0.5), particle.getMotionY() * (double)0.1f, particle.getMotionZ() * (double)0.1f + this.random * (rand.nextGaussian() - 0.5));
        particle.setMotion(particle.getMotionX() + vx, particle.getMotionY() + vy, particle.getMotionZ() + vz);
        this.mx = vx;
        this.mz = vz;
        particle.setScale(0.55f);
        particle.setMaxAge(10);
    }

    @Override
    public void update(ParticleArcanery particle) {
        particle.setPrevPos(particle.getX(), particle.getY(), particle.getZ());
        particle.incrementAge();
        if (particle.getAge() >= particle.getMaxAge()) {
            particle.setExpired();
        }
        particle.setScale(particle.getScale() / 45.0f * (float)(45 - particle.getAge()));
        particle.move(particle.getMotionX(), particle.getMotionY(), particle.getMotionZ());
        this.theta += this.wu * (double)particle.getAge() / 2.0;
        this.mx = this.r * Math.cos(this.theta) / 6.0;
        this.mz = this.r * Math.sin(this.theta) / 6.0;
        particle.setMotion(this.mx, particle.getMotionY(), this.mz);
        particle.setRGBA(Math.min(0.1f + 0.8f / ((float)particle.getAge() / 2.5f), 1.0f), this.yellow ? Math.min(0.1f + 0.8f / ((float)particle.getAge() / 2.5f), 1.0f) : 0.0f, this.yellow ? Math.min(0.1f + 0.8f / ((float)particle.getAge() / 2.5f), 1.0f) : 0.0f, 0.4f);
    }

    @Override
    public ResourceLocation texture() {
        return tex;
    }
}

