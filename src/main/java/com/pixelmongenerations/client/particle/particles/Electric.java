/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.primitives.Doubles
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.particle.particles;

import com.google.common.primitives.Doubles;
import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleEffect;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;

public class Electric
extends ParticleEffect {
    private int age;
    private boolean parent;
    private float pitch;
    private float yaw;
    private float velocity;
    private float innaccuracy;
    private float r;
    private float g;
    private float b;
    private float a;
    private static final ResourceLocation tex = new ResourceLocation("pixelmon", "textures/particles/lightning4.png");

    public Electric(int age, boolean parent, float pitch, float yaw, float velocity, float innaccuracy, float r, float g, float b) {
        this.age = age;
        this.parent = parent;
        this.pitch = pitch;
        this.yaw = yaw;
        this.velocity = velocity;
        this.innaccuracy = innaccuracy;
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = 1.0f;
    }

    public Electric(int age, boolean parent, float pitch, float yaw, float velocity, float innaccuracy, float r, float g, float b, float a) {
        this.age = age;
        this.parent = parent;
        this.pitch = pitch;
        this.yaw = yaw;
        this.velocity = velocity;
        this.innaccuracy = innaccuracy;
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    @Override
    public void render(ParticleArcanery particle, Tessellator tessellator, float partialTicks) {
        if (!this.parent) {
            Minecraft mc = Minecraft.getMinecraft();
            GlStateManager.pushMatrix();
            GlStateManager.disableTexture2D();
            GlStateManager.enableBlend();
            GlStateManager.blendFunc(770, 1);
            GlStateManager.translate(-mc.player.posX, -mc.player.posY, -mc.player.posZ);
            GlStateManager.color(this.r, this.g, this.b, 0.75f * this.a * (1.0f - (float)particle.getAge() / (float)particle.getMaxAge()));
            GL11.glEnable((int)2848);
            GL11.glLineWidth((float)3.0f);
            GL11.glHint((int)3154, (int)4354);
            GL11.glBegin((int)3);
            GL11.glVertex3d((double)particle.getPrevX(), (double)particle.getPrevY(), (double)particle.getPrevZ());
            GL11.glVertex3d((double)particle.getX(), (double)particle.getY(), (double)particle.getZ());
            GL11.glEnd();
            GL11.glLineWidth((float)8.0f);
            GlStateManager.color(this.r, this.g, this.b, Math.max(0.75f * this.a * (1.0f - (float)particle.getAge() / (float)particle.getMaxAge()) - 0.3f, 0.0f));
            GL11.glBegin((int)3);
            GL11.glVertex3d((double)particle.getPrevX(), (double)particle.getPrevY(), (double)particle.getPrevZ());
            GL11.glVertex3d((double)particle.getX(), (double)particle.getY(), (double)particle.getZ());
            GL11.glEnd();
            GL11.glDisable((int)2848);
            GlStateManager.disableBlend();
            GlStateManager.enableTexture2D();
            GlStateManager.popMatrix();
        }
    }

    @Override
    public boolean customRenderer() {
        return true;
    }

    @Override
    public void init(ParticleArcanery particle, World world, double x, double y, double z, double vx, double vy, double vz, float size) {
        particle.setRGBA(1.0f, 1.0f, 1.0f, 0.8f * this.a);
        particle.setScale(0.05f);
        particle.setMaxAge(this.age);
        particle.setPos(x, y, z);
        particle.setPrevPos(vx, vy, vz);
        this.setAim(this.pitch, this.yaw, this.velocity, this.innaccuracy, particle);
    }

    @Override
    public void update(ParticleArcanery particle) {
        if (this.parent) {
            particle.setPrevPos(particle.getX(), particle.getY(), particle.getZ());
            particle.move(particle.getMotionX(), particle.getMotionY(), particle.getMotionZ());
            particle.move((particle.getRand().nextDouble() - 0.5) * Doubles.constrainToRange((double)(0.15 + (double)particle.getAge() / 3.0), (double)0.0, (double)1.75), (particle.getRand().nextDouble() - 0.5) * Doubles.constrainToRange((double)(0.15 + (double)particle.getAge() / 3.0), (double)0.0, (double)1.75), (particle.getRand().nextDouble() - 0.5) * Doubles.constrainToRange((double)(0.15 + (double)particle.getAge() / 3.0), (double)0.0, (double)1.75));
            ParticleArcanery child = new ParticleArcanery(particle.getWorld(), particle.getX(), particle.getY(), particle.getZ(), particle.getPrevX(), particle.getPrevY(), particle.getPrevZ(), new Electric(8, false, 0.0f, 0.0f, 0.0f, 0.0f, this.r, this.g, this.b, this.a));
            Minecraft.getMinecraft().effectRenderer.addEffect(child);
        }
        particle.incrementAge();
        if (particle.getAge() >= particle.getMaxAge()) {
            particle.setExpired();
        }
    }

    @Override
    public ResourceLocation texture() {
        return tex;
    }

    private void setAim(float pitch, float yaw, float velocity, float inaccuracy, ParticleArcanery particle) {
        float f = -MathHelper.sin(yaw * ((float)Math.PI / 180)) * MathHelper.cos(pitch * ((float)Math.PI / 180));
        float f1 = -MathHelper.sin(pitch * ((float)Math.PI / 180));
        float f2 = MathHelper.cos(yaw * ((float)Math.PI / 180)) * MathHelper.cos(pitch * ((float)Math.PI / 180));
        this.setHeading(f, f1, f2, velocity, inaccuracy, particle);
    }

    private void setHeading(double x, double y, double z, float velocity, float inaccuracy, ParticleArcanery particle) {
        float f = MathHelper.sqrt(x * x + y * y + z * z);
        x /= (double)f;
        y /= (double)f;
        z /= (double)f;
        x += particle.getRand().nextGaussian() * (double)0.0075f * (double)inaccuracy;
        y += particle.getRand().nextGaussian() * (double)0.0075f * (double)inaccuracy;
        z += particle.getRand().nextGaussian() * (double)0.0075f * (double)inaccuracy;
        particle.setMotion(x *= (double)velocity, y *= (double)velocity, z *= (double)velocity);
    }
}

