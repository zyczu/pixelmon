/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle.particles;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleEffect;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class SlingRing
extends ParticleEffect {
    private boolean parent;
    private int maxAge;
    private static final ResourceLocation tex = new ResourceLocation("pixelmon", "textures/particles/sling_ring.png");

    public SlingRing(boolean parent, int maxAge) {
        this.parent = parent;
        this.maxAge = maxAge;
    }

    @Override
    public void init(ParticleArcanery particle, World world, double x, double y, double z, double vx, double vy, double vz, float size) {
        particle.setRGBA(1.0f, 1.0f, 1.0f, this.parent ? 0.0f : 0.5f);
        particle.setScale(0.75f);
        particle.setMaxAge(this.maxAge);
    }

    @Override
    public void update(ParticleArcanery particle) {
        particle.setPrevPos(particle.getX(), particle.getY(), particle.getZ());
        particle.incrementAge();
        if (particle.getAge() >= particle.getMaxAge()) {
            particle.setExpired();
        }
        if (this.parent) {
            ParticleArcanery child = new ParticleArcanery(particle.getWorld(), particle.getX(), particle.getY(), particle.getZ(), 0.0, 0.0, 0.0, new SlingRing(false, 5));
            Minecraft.getMinecraft().effectRenderer.addEffect(child);
        } else {
            particle.setMotion(0.2 * (particle.getRand().nextDouble() * 2.0 - 1.0), 0.2 * (particle.getRand().nextDouble() * 2.0 - 1.0), 0.2 * (particle.getRand().nextDouble() * 2.0 - 1.0));
            particle.move(particle.getMotionX(), particle.getMotionY(), particle.getMotionZ());
            particle.setRGBA(1.0f, 1.0f, 1.0f, 1.0f - (float)particle.getAge() / (float)particle.getMaxAge());
            particle.setScale(0.75f - (float)particle.getAge() / (float)particle.getMaxAge() * 0.75f);
        }
    }

    @Override
    public ResourceLocation texture() {
        return tex;
    }
}

