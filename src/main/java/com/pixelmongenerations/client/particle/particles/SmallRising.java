/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle.particles;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleEffect;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class SmallRising
extends ParticleEffect {
    float r;
    float g;
    float b;
    float a;
    private static final ResourceLocation tex = new ResourceLocation("pixelmon", "textures/particles/yellow_magic.png");

    public SmallRising(float r, float g, float b, float a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    @Override
    public void init(ParticleArcanery particle, World world, double x, double y, double z, double vx, double vy, double vz, float size) {
        particle.setRGBA(this.r, this.g, this.b, this.a);
        particle.setMotion(particle.getMotionX() * (double)0.1f, particle.getMotionY() * (double)0.1f, particle.getMotionZ() * (double)0.1f);
        particle.setMotion(particle.getMotionX() + vx * 0.4, particle.getMotionY() + vy * 0.4, particle.getMotionZ() + vz * 0.4);
        particle.setScale(0.1f);
        particle.setMaxAge(50);
    }

    @Override
    public void update(ParticleArcanery particle) {
        particle.setPrevPos(particle.getX(), particle.getY(), particle.getZ());
        particle.incrementAge();
        if (particle.getAge() >= particle.getMaxAge()) {
            particle.setExpired();
        }
        particle.setScale(particle.getScale() / 50.0f * (float)(50 - particle.getAge()));
        particle.move(particle.getMotionX(), particle.getMotionY(), particle.getMotionZ());
        particle.setMotion(particle.getMotionX() * (double)0.7f, particle.getMotionY() + (double)0.02f, particle.getMotionZ() * (double)0.7f);
    }

    @Override
    public ResourceLocation texture() {
        return tex;
    }
}

