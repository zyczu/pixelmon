/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle.systems;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleSystem;
import com.pixelmongenerations.client.particle.particles.Electric;
import net.minecraft.client.Minecraft;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class RadialThunder
extends ParticleSystem {
    @Override
    @SideOnly(value=Side.CLIENT)
    public void execute(Minecraft mc, World w, double x, double y, double z, float scale, boolean shiny, double ... args) {
        double radius = 4.5;
        for (int i = 0; i < 1000; ++i) {
            double posX = x + 0.5 + w.rand.nextDouble() * radius * 2.0 - radius;
            double posY = y + w.rand.nextDouble() - 0.5;
            double posZ = z + 0.5 + w.rand.nextDouble() * radius * 2.0 - radius;
            if (!(Math.sqrt((x - posX) * (x - posX) + (z - posZ) * (z - posZ)) <= radius)) continue;
            ParticleArcanery parent = new ParticleArcanery(w, posX, posY, posZ, posX, posY + 8.0, posZ, new Electric(5 + w.rand.nextInt(10), false, -90.0f, 0.0f, 6.0f, 0.0f, (float)args[0], (float)args[1], (float)args[2]));
            Minecraft.getMinecraft().effectRenderer.addEffect(parent);
        }
    }
}

