/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.particle.systems;

import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class SlingRing
extends ParticleSystem {
    @Override
    @SideOnly(value=Side.CLIENT)
    public void execute(Minecraft mc, World w, double x, double y, double z, float scale, boolean shiny, double ... args) {
        int totalPoints = 16;
        int totalHeight = 5;
        double radius = 2.0;
        x -= 0.5;
        z -= 0.5;
        for (double height = 0.0; height < (double)totalHeight; height += 0.1) {
            for (int i = 1; i <= totalPoints; ++i) {
                double theta = Math.PI * 2 / (double)totalPoints;
                double angle = theta * (double)i;
                double dx = radius * Math.cos(angle) + mc.world.rand.nextDouble() * 1.5;
                double dz = radius * Math.sin(angle) + mc.world.rand.nextDouble() * 1.5;
                mc.effectRenderer.addEffect(new ParticleArcanery(w, x + dx, y + height + mc.world.rand.nextDouble() * 1.5, z + dz, 0.0, 0.0, 0.0, new com.pixelmongenerations.client.particle.particles.SlingRing(true, 30 - mc.world.rand.nextInt(10))));
            }
        }
    }
}

