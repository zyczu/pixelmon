/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client;

import com.pixelmongenerations.client.GenerationsMovingSound;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;

public class SoundHelper {
    public static void playButtonPressSound() {
        SoundHelper.playSound(SoundEvents.UI_BUTTON_CLICK);
    }

    public static void playSound(SoundEvent event) {
        SoundHandler soundHandler = Minecraft.getMinecraft().getSoundHandler();
        soundHandler.playSound(PositionedSoundRecord.getMasterRecord(event, 1.0f));
    }

    public static void playSound(ResourceLocation soundLocation) {
        SoundHandler soundHandler = Minecraft.getMinecraft().getSoundHandler();
        soundHandler.playSound(new GenerationsMovingSound(soundLocation));
    }
}

