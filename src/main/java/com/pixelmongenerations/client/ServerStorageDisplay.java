/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.client;

import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonUpdateData;
import com.pixelmongenerations.core.network.packetHandlers.StarterListPacket;
import com.pixelmongenerations.core.network.packetHandlers.itemDrops.ItemDropPacket;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ServerStorageDisplay {
    public static StarterListPacket starterListPacket;
    public static ItemDropPacket bossDrops;
    public static int NPCInteractId;
    private static PixelmonData[] pokemon;
    private static PixelmonData[] battlePokemon;
    public static List<PixelmonData> editedPokemon;

    public static PixelmonData[] getPokemon() {
        return ServerStorageDisplay.getPokemon(ClientProxy.battleManager.isBattling());
    }

    public static PixelmonData[] getPokemon(boolean inBattle) {
        return inBattle ? battlePokemon : pokemon;
    }

    public static void add(PixelmonData data) {
        PixelmonData[] currentPokemon = ServerStorageDisplay.getPokemon(data.inBattle);
        if (data.order >= 0 && data.order < currentPokemon.length) {
            currentPokemon[data.order] = data;
        }
    }

    public static void clearDuplicates(PixelmonData[] currentPokemon) {
        for (int i = 0; i < currentPokemon.length; ++i) {
            PixelmonData data = currentPokemon[i];
            if (data == null) continue;
            for (int j = 0; j < currentPokemon.length; ++j) {
                if (j == i || currentPokemon[j] == null || currentPokemon[j].pokemonID[0] != data.pokemonID[0] || currentPokemon[j].pokemonID[1] != data.pokemonID[1]) continue;
                currentPokemon[j] = null;
            }
        }
    }

    public static int count() {
        PixelmonData[] currentPokemon = ServerStorageDisplay.getPokemon();
        int count = 0;
        for (PixelmonData aPokemon : currentPokemon) {
            if (aPokemon == null) continue;
            ++count;
        }
        return count;
    }

    public static int countNonEgg() {
        PixelmonData[] currentPokemon = ServerStorageDisplay.getPokemon();
        int count = 0;
        for (PixelmonData aPokemon : currentPokemon) {
            if (aPokemon == null || aPokemon.isEgg) continue;
            ++count;
        }
        return count;
    }

    public static void update(ByteBuf dataStream) {
        PixelmonData packet = new PixelmonData();
        packet.decodeInto(dataStream);
        PixelmonData[] currentPokemon = ServerStorageDisplay.getPokemon(packet.inBattle);
        currentPokemon[packet.order] = packet;
    }

    public static void clear() {
        ServerStorageDisplay.clear(ServerStorageDisplay.getPokemon());
    }

    private static void clear(PixelmonData[] currentPokemon) {
        Arrays.fill(currentPokemon, null);
    }

    public static void clearBattle() {
        ServerStorageDisplay.clear(battlePokemon);
    }

    public static boolean contains(int[] pokemonId) {
        for (PixelmonData aPokemon : ServerStorageDisplay.getPokemon()) {
            if (aPokemon == null || !PixelmonMethods.isIDSame(aPokemon.pokemonID, pokemonId)) continue;
            return true;
        }
        return false;
    }

    public static PixelmonData get(int[] id) {
        for (PixelmonData p : ServerStorageDisplay.getPokemon()) {
            if (p == null || !PixelmonMethods.isIDSame(p.pokemonID, id)) continue;
            return p;
        }
        return null;
    }

    public static void remove(int[] id) {
        PixelmonData[] currentPokemon = ServerStorageDisplay.getPokemon();
        for (int i = 0; i < currentPokemon.length; ++i) {
            if (currentPokemon[i] == null || !PixelmonMethods.isIDSame(currentPokemon[i].pokemonID, id)) continue;
            currentPokemon[i] = null;
        }
    }

    public static PixelmonData getNextFromPos(int pos) {
        PixelmonData[] currentPokemon = ServerStorageDisplay.getPokemon();
        if (++pos >= 6) {
            pos = 0;
        }
        while (currentPokemon[pos] == null) {
            if (++pos < 6) continue;
            pos = 0;
        }
        return currentPokemon[pos];
    }

    public static PixelmonData getPrevFromPos(int pos) {
        PixelmonData[] currentPokemon = ServerStorageDisplay.getPokemon();
        if (--pos < 0 || pos >= currentPokemon.length) {
            pos = 5;
        }
        while (currentPokemon[pos] == null) {
            if (--pos >= 0) continue;
            pos = 5;
        }
        return currentPokemon[pos];
    }

    public static void update(PixelmonUpdateData p) {
        for (PixelmonData aPokemon : ServerStorageDisplay.getPokemon(p.inBattle)) {
            if (aPokemon == null || !PixelmonMethods.isIDSame(aPokemon.pokemonID, p.pokemonID)) continue;
            aPokemon.update(p);
        }
    }

    public static void changePokemon(int pos, PixelmonData pkt) {
        boolean inBattle = pkt != null && pkt.inBattle;
        PixelmonData[] currentPokemon = ServerStorageDisplay.getPokemon(inBattle);
        if (pos >= currentPokemon.length) {
            pos = currentPokemon.length - 1;
        }
        currentPokemon[pos] = pkt;
    }

    public static boolean has(int[] id) {
        for (PixelmonData p : ServerStorageDisplay.getPokemon()) {
            if (p == null || !PixelmonMethods.isIDSame(p.pokemonID, id)) continue;
            return true;
        }
        return false;
    }

    static {
        pokemon = new PixelmonData[6];
        battlePokemon = new PixelmonData[6];
        editedPokemon = new ArrayList<PixelmonData>(6);
    }
}

