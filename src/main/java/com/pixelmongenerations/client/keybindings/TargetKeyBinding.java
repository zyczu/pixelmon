/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.keybindings;

import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;

public class TargetKeyBinding
extends KeyBinding {
    protected boolean targetLiquids = false;

    public TargetKeyBinding(String p_i45001_1_, int p_i45001_2_, String p_i45001_3_) {
        super(p_i45001_1_, p_i45001_2_, p_i45001_3_);
    }

    public static RayTraceResult getTarget(boolean targetLiquids) {
        return TargetKeyBinding.getTarget(targetLiquids, 10.0);
    }

    public static RayTraceResult getTarget(boolean targetLiquids, double range) {
        Minecraft mc = Minecraft.getMinecraft();
        Entity pointedEntity = null;
        float partialTick = 1.0f;
        Vec3d Vec3d2 = mc.getRenderViewEntity().getPositionEyes(partialTick);
        Vec3d Vec3d1 = mc.getRenderViewEntity().getLook(partialTick);
        Vec3d Vec3d22 = Vec3d2.add(Vec3d1.x * range, Vec3d1.y * range, Vec3d1.z * range);
        RayTraceResult objectMouseOver = mc.world.rayTraceBlocks(Vec3d2, Vec3d22, targetLiquids, false, true);
        double d1 = range;
        Vec3d2 = mc.getRenderViewEntity().getPositionEyes(partialTick);
        if (objectMouseOver != null) {
            d1 = objectMouseOver.hitVec.distanceTo(Vec3d2);
        }
        Vec3d Vec3d3 = null;
        float f1 = 1.0f;
        List<Entity> list = mc.world.getEntitiesWithinAABBExcludingEntity(mc.getRenderViewEntity(), mc.getRenderViewEntity().getEntityBoundingBox().expand(Vec3d1.x * range, Vec3d1.y * range, Vec3d1.z * range).expand(f1, f1, f1));
        double d2 = d1;
        for (Entity aList : list) {
            if (!aList.canBeCollidedWith()) continue;
            float f2 = aList.getCollisionBorderSize();
            AxisAlignedBB axisalignedbb = aList.getEntityBoundingBox().expand(f2, f2, f2);
            RayTraceResult movingobjectposition = axisalignedbb.calculateIntercept(Vec3d2, Vec3d22);
            if (axisalignedbb.contains(Vec3d2)) {
                if (0.0 >= d2 && d2 != 0.0) continue;
                pointedEntity = aList;
                Vec3d3 = movingobjectposition == null ? Vec3d2 : movingobjectposition.hitVec;
                d2 = 0.0;
                continue;
            }
            if (movingobjectposition == null) continue;
            double d3 = Vec3d2.distanceTo(movingobjectposition.hitVec);
            if (d3 >= d2 && d2 != 0.0) continue;
            if (aList == mc.getRenderViewEntity().getControllingPassenger() && !aList.canRiderInteract()) {
                if (d2 != 0.0) continue;
                pointedEntity = aList;
                Vec3d3 = movingobjectposition.hitVec;
                continue;
            }
            pointedEntity = aList;
            Vec3d3 = movingobjectposition.hitVec;
            d2 = d3;
        }
        if (pointedEntity != null && (d2 < d1 || objectMouseOver == null)) {
            objectMouseOver = new RayTraceResult(pointedEntity, Vec3d3);
        }
        return objectMouseOver;
    }
}

