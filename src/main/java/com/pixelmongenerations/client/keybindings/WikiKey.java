/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.keybindings;

import com.pixelmongenerations.client.keybindings.TargetKeyBinding;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.util.helper.ArrayHelper;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class WikiKey
extends KeyBinding {
    public static final String PIXELMON_WIKI = "https://wiki.pixelmongenerations.com/index.php?title=";
    private static final String MINECRAFT_WIKI_START = "http://minecraft";
    private static final String MINECRAFT_WIKI_END = ".gamepedia.com/";
    private static final String[] TRANSLATED_MINECRAFT_WIKI = new String[]{"de", "el", "es", "fr", "hu", "it", "ja", "ko", "nl", "pl", "pt", "ru", "zh"};

    public WikiKey() {
        super("key.wiki", 37, "key.categories.pixelmon");
    }

    @SubscribeEvent
    public void keyDown(InputEvent.KeyInputEvent event) {
        if (this.isPressed()) {
            Minecraft mc = Minecraft.getMinecraft();
            if (mc.world == null) {
                return;
            }
            try {
                TargetKeyBinding.getTarget(false);
                ItemStack itemStack = mc.player.inventory.getCurrentItem();
                Item item = itemStack.getItem();
                ResourceLocation id = item.getRegistryName();
                String name = item == PixelmonItems.itemPixelmonSprite ? I18n.format("item.pixelmon_sprite.name", new Object[0]) : item.getItemStackDisplayName(itemStack);
                if (id != null) {
                    name = name.trim().replace(":", "%3A").replace(" ", "_").replace("\u00e9", "e");
                    String resourceDomain = id.getNamespace();
                    String baseURL = null;
                    if (resourceDomain.equalsIgnoreCase("pixelmon")) {
                        baseURL = PIXELMON_WIKI;
                    } else if (resourceDomain.equalsIgnoreCase("minecraft")) {
                        baseURL = MINECRAFT_WIKI_START;
                        String langCode = mc.getLanguageManager().getCurrentLanguage().getLanguageCode();
                        if (!(langCode = langCode.substring(0, 2)).equals("en") && ArrayHelper.contains(TRANSLATED_MINECRAFT_WIKI, langCode)) {
                            if (langCode.equals("pt")) {
                                langCode = "br";
                            }
                            baseURL = baseURL + '-' + langCode;
                        }
                        baseURL = baseURL + MINECRAFT_WIKI_END;
                    }
                    if (baseURL != null) {
                        Desktop.getDesktop().browse(new URI(baseURL + name));
                    }
                }
            }
            catch (UnsupportedOperationException itemStack) {
            }
            catch (IOException | RuntimeException | URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }
}

