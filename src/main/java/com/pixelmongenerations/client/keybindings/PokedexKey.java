/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.keybindings;

import com.pixelmongenerations.client.gui.pokedex.ClientPokedexManager;
import com.pixelmongenerations.client.keybindings.TargetKeyBinding;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.pokedex.OpenPokedex;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class PokedexKey
extends KeyBinding {
    public static final String pixelmonWiki = "https://wiki.pixelmongenerations.com/index.php?title=";

    public PokedexKey() {
        super("key.pokedex", 23, "key.categories.pixelmon");
    }

    @SubscribeEvent
    public void keyDown(InputEvent.KeyInputEvent event) {
        if (this.isPressed()) {
            Minecraft mc = Minecraft.getMinecraft();
            if (mc.world == null) {
                return;
            }
            try {
                RayTraceResult objectMouseOver = TargetKeyBinding.getTarget(false);
                if (objectMouseOver.typeOfHit == RayTraceResult.Type.ENTITY && objectMouseOver.entityHit instanceof EntityPixelmon) {
                    EntityPixelmon pokemon = (EntityPixelmon)objectMouseOver.entityHit;
                    Pixelmon.NETWORK.sendToServer(new OpenPokedex(pokemon.baseStats.pokemon));
                    return;
                }
                Pixelmon.NETWORK.sendToServer(new OpenPokedex(ClientPokedexManager.pokedex == null));
            }
            catch (UnsupportedOperationException unsupportedOperationException) {
                // empty catch block
            }
        }
    }
}

