/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.keybindings;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.client.gui.overlay.PartyOverlay;
import com.pixelmongenerations.client.keybindings.TargetKeyBinding;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.EnumPixelmonKeyType;
import com.pixelmongenerations.core.network.packetHandlers.KeyPacket;
import com.pixelmongenerations.core.storage.playerData.ExternalMoveData;
import com.pixelmongenerations.core.util.PixelmonMethods;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class ExternalMoveKey
extends TargetKeyBinding {
    public ExternalMoveKey() {
        super("key.pokemonexternal", 34, "key.categories.pixelmon");
    }

    @SubscribeEvent
    public void keyDown(InputEvent.KeyInputEvent event) {
        if (this.isPressed()) {
            WorldClient world = Minecraft.getMinecraft().world;
            if (world == null) {
                return;
            }
            PartyOverlay overlay = (PartyOverlay)GuiPixelmonOverlay.getOverlay(OverlayType.PARTY);
            PixelmonData poke = overlay.getSelectedPokemon();
            if (poke == null || !poke.outside) {
                return;
            }
            ExternalMoveData[] moves = poke.getExternalMoves();
            if (moves.length > 0) {
                short selectedIndex = poke.selectedMoveIndex;
                if (selectedIndex >= moves.length) {
                    selectedIndex = 0;
                    poke.selectedMoveIndex = 0;
                }
                if (moves[selectedIndex].getBaseExternalMove().targetsBlocks()) {
                    this.targetLiquids = true;
                }
            }
            RayTraceResult objectMouseOver = ExternalMoveKey.getTarget(this.targetLiquids);
            boolean isSamePokemon = false;
            if (objectMouseOver == null) {
                return;
            }
            if (objectMouseOver.entityHit instanceof EntityPixelmon && PixelmonMethods.isIDSame((EntityPixelmon)objectMouseOver.entityHit, poke.pokemonID)) {
                isSamePokemon = true;
            }
            if (moves.length > 0) {
                short selectedIndex = poke.selectedMoveIndex;
                ExternalMoveBase selectedMove = moves[selectedIndex].getBaseExternalMove();
                if (world.getTotalWorldTime() < moves[selectedIndex].timeLastUsed + (long)selectedMove.getCooldown(poke)) {
                    return;
                }
                if (selectedMove.isTargetted()) {
                    if (objectMouseOver.typeOfHit == RayTraceResult.Type.ENTITY && !isSamePokemon) {
                        Entity entity = objectMouseOver.entityHit;
                        Pixelmon.NETWORK.sendToServer(new KeyPacket(poke.pokemonID, (int)selectedIndex, entity.getEntityId()));
                    } else if (objectMouseOver.typeOfHit == RayTraceResult.Type.BLOCK) {
                        Pixelmon.NETWORK.sendToServer(new KeyPacket(poke.pokemonID, selectedIndex, objectMouseOver.getBlockPos(), objectMouseOver.sideHit));
                    } else {
                        Pixelmon.NETWORK.sendToServer(new KeyPacket(poke.pokemonID, -1, EnumPixelmonKeyType.ActionKeyEntity));
                    }
                } else {
                    Pixelmon.NETWORK.sendToServer(new KeyPacket(poke.pokemonID, (int)selectedIndex, EnumPixelmonKeyType.ExternalMove));
                }
                poke.outsideEntity.playLivingSound();
            }
        }
    }
}

