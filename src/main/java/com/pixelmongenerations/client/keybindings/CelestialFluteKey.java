/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.keybindings;

import com.pixelmongenerations.client.gui.mount.ClientMountDataManager;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.network.packetHandlers.OpenMountSelection;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class CelestialFluteKey
extends KeyBinding {
    public CelestialFluteKey() {
        super("key.flute", 33, "key.categories.pixelmon");
    }

    @SubscribeEvent
    public void keyDown(InputEvent.KeyInputEvent event) {
        if (PixelmonServerConfig.allowCelestialFlute && this.isPressed()) {
            Minecraft mc = Minecraft.getMinecraft();
            if (mc.world == null) {
                return;
            }
            Pixelmon.NETWORK.sendToServer(new OpenMountSelection(ClientMountDataManager.mountData == null));
        }
    }
}

