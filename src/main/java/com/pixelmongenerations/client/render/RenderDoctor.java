/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render;

import com.pixelmongenerations.common.entity.npcs.EntityDoctor;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderDoctor
extends RenderLiving<EntityDoctor> {
    public RenderDoctor(RenderManager manager, float par2) {
        super(manager, null, par2);
    }

    @Override
    public void doRender(EntityDoctor entityLiving, double d, double d1, double d2, float f, float f1) {
        this.mainModel = entityLiving.getModel();
        if (this.mainModel == null) {
            return;
        }
        super.doRender(entityLiving, d, d1, d2, f, f1);
    }

    @Override
    protected ResourceLocation getEntityTexture(EntityDoctor entity) {
        return new ResourceLocation(entity.getTexture());
    }

    protected void renderLivingLabel(String par2Str, double par3, double par5, double par7) {
        FontRenderer var12 = this.getFontRendererFromRenderManager();
        float var13 = 1.6f;
        float var14 = 0.016666668f * var13;
        GlStateManager.pushMatrix();
        GlStateManager.translate((float)par3 + 0.0f, (float)par5 + 1.1f + 1.4f, (float)par7);
        GL11.glNormal3f((float)0.0f, (float)1.0f, (float)0.0f);
        GlStateManager.rotate(-this.renderManager.playerViewY, 0.0f, 1.0f, 0.0f);
        GlStateManager.rotate(this.renderManager.playerViewX, 1.0f, 0.0f, 0.0f);
        GlStateManager.scale(-var14, -var14, var14);
        GlStateManager.disableLighting();
        GlStateManager.depthMask(false);
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(770, 771);
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder vertexbuffer = tessellator.getBuffer();
        int var16 = 0;
        GlStateManager.disableTexture2D();
        int var17 = var12.getStringWidth(par2Str) / 2;
        vertexbuffer.begin(7, DefaultVertexFormats.POSITION_COLOR);
        vertexbuffer.pos(-var17 - 1, -1 + var16, 0.0).color(0.0f, 0.0f, 0.0f, 0.25f).endVertex();
        vertexbuffer.pos(-var17 - 1, 8 + var16, 0.0).color(0.0f, 0.0f, 0.0f, 0.25f).endVertex();
        vertexbuffer.pos(var17 + 1, 8 + var16, 0.0).color(0.0f, 0.0f, 0.0f, 0.25f).endVertex();
        vertexbuffer.pos(var17 + 1, -1 + var16, 0.0).color(0.0f, 0.0f, 0.0f, 0.25f).endVertex();
        tessellator.draw();
        GlStateManager.enableTexture2D();
        var12.drawString(par2Str, -var12.getStringWidth(par2Str) / 2, var16, 0x20FFFFFF);
        GlStateManager.depthMask(true);
        var12.drawString(par2Str, -var12.getStringWidth(par2Str) / 2, var16, -1);
        GlStateManager.enableLighting();
        GlStateManager.disableBlend();
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.popMatrix();
    }
}

