/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableMap
 *  com.google.common.collect.Lists
 *  javax.vecmath.Matrix4f
 *  org.apache.commons.lang3.tuple.Pair
 */
package com.pixelmongenerations.client.render.item;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.pixelmongenerations.client.util.RenderUtil;
import java.util.ArrayList;
import java.util.List;
import javax.vecmath.Matrix4f;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ItemOverrideList;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.client.model.PerspectiveMapWrapper;
import net.minecraftforge.common.model.TRSRTransformation;
import org.apache.commons.lang3.tuple.Pair;

public class FadedOrbItemRenderer
extends PerspectiveMapWrapper {
    IBakedModel model;
    private final String fadedTexture;
    private final String unFadedTexture;
    private float percent;
    private float height;

    public FadedOrbItemRenderer(IBakedModel model, String fadedTexture, String unFadedTexture) {
        super(model, TRSRTransformation.identity());
        this.model = model;
        this.fadedTexture = fadedTexture;
        this.unFadedTexture = unFadedTexture;
    }

    @Override
    public boolean isBuiltInRenderer() {
        return false;
    }

    @Override
    public TextureAtlasSprite getParticleTexture() {
        return this.model.getParticleTexture();
    }

    @Override
    public boolean isGui3d() {
        return this.model.isGui3d();
    }

    @Override
    public ItemCameraTransforms getItemCameraTransforms() {
        return this.model.getItemCameraTransforms();
    }

    @Override
    public ItemOverrideList getOverrides() {
        return new ItemOverrideList(Lists.newArrayList()){

            @Override
            public IBakedModel handleItemState(IBakedModel originalModel, ItemStack stack, World world, EntityLivingBase entity) {
                if (originalModel instanceof FadedOrbItemRenderer) {
                    FadedOrbItemRenderer orb = (FadedOrbItemRenderer)originalModel;
                    orb.height = (float)stack.getItemDamage() / 500.0f;
                    orb.percent = orb.height * 16.0f;
                }
                return originalModel;
            }
        };
    }

    @Override
    public boolean isAmbientOcclusion() {
        return this.model.isAmbientOcclusion();
    }

    @Override
    public List<BakedQuad> getQuads(IBlockState state, EnumFacing side, long rand) {
        ArrayList<BakedQuad> quadList = new ArrayList<BakedQuad>(6);
        TextureAtlasSprite orbTexture = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(this.fadedTexture);
        RenderUtil.addQuad(quadList, orbTexture, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 16.0f, 16.0f);
        orbTexture = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(this.unFadedTexture);
        RenderUtil.addQuad(quadList, orbTexture, 1.0f, this.height, 0.001f, 0.0f, 16.0f - this.percent, 16.0f, 16.0f);
        return quadList;
    }

    @Override
    public Pair<? extends IBakedModel, Matrix4f> handlePerspective(ItemCameraTransforms.TransformType cameraTransformType) {
        ImmutableMap<ItemCameraTransforms.TransformType, TRSRTransformation> map = PerspectiveMapWrapper.getTransforms(TRSRTransformation.identity());
        return FadedOrbItemRenderer.handlePerspective((IBakedModel)this, map, cameraTransformType);
    }
}

