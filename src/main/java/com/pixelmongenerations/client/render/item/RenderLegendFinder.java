/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.item;

import com.pixelmongenerations.common.entity.EntityLegendFinder;
import com.pixelmongenerations.core.config.PixelmonItems;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderSnowball;

public class RenderLegendFinder<T extends EntityLegendFinder>
extends RenderSnowball {
    public RenderLegendFinder(RenderManager render) {
        super(render, PixelmonItems.legendFinder, Minecraft.getMinecraft().getRenderItem());
    }
}

