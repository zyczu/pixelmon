/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render;

import com.pixelmongenerations.client.models.pokeballs.ModelPokeballs;
import com.pixelmongenerations.common.entity.pokeballs.EntityPokeBall;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.core.util.Quarternion;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;

public class RenderPokeball
extends Render<EntityPokeBall> {
    ModelPokeballs model;

    public RenderPokeball(RenderManager renderManager) {
        super(renderManager);
    }

    @Override
    public void doRender(EntityPokeBall pokeball, double x, double y, double z, float f, float f1) {
        GlStateManager.pushMatrix();
        GlStateManager.translate(x, y, z);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        Quarternion q1 = Quarternion.toQuarternion(-pokeball.getInitialPitch(), -((float)Math.cos((double)pokeball.getInitialYaw() * Math.PI / 180.0)), 0.0f, -((float)Math.sin((double)pokeball.getInitialYaw() * Math.PI / 180.0)));
        Quarternion q2 = Quarternion.toQuarternion(pokeball.rotationYaw, 0.0f, 1.0f, 0.0f);
        q1.multiplyBy(q2);
        q1.fromQuarternion(q1);
        GlStateManager.rotate(q1.w, q1.x, q1.y, q1.z);
        this.model = pokeball.getModel();
        this.model.theModel.setAnimation(pokeball.getAnimation());
        String path = "pixelmon:textures/pokeballs/";
        this.bindTexture(new ResourceLocation(path + pokeball.getType().getTexture()));
        if (!pokeball.getIsWaiting()) {
            pokeball.premierFlash();
        }
        if (pokeball.getMode() == EnumPokeBallMode.EMPTY && pokeball.getAnimation().equalsIgnoreCase("bounceOpen")) {
            pokeball.onCaptureAttemptEffect();
        }
        if ((pokeball.isCriticalCapture() && pokeball.shakeCount == 1 || pokeball.shakeCount == 4) && pokeball.getIsWaiting() && pokeball.waitTimer < 1) {
            GlStateManager.color(0.006f, 0.006f, 0.006f, 1.0f);
            this.bindTexture(new ResourceLocation(path + pokeball.getType().getFlashRedTexture()));
            pokeball.successfulCaptureEffect();
        }
        if (pokeball.getMode() == EnumPokeBallMode.FULL && pokeball.getAnimation().equalsIgnoreCase("bounceClose")) {
            pokeball.releaseEffect();
        }
        GlStateManager.pushMatrix();
        GlStateManager.enableRescaleNormal();
        this.model.render(pokeball, EntityPokeBall.scale);
        GlStateManager.disableRescaleNormal();
        GlStateManager.popMatrix();
        GlStateManager.popMatrix();
    }

    @Override
    protected ResourceLocation getEntityTexture(EntityPokeBall entity) {
        return null;
    }
}

