/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render;

import com.pixelmongenerations.common.entity.projectiles.EntityHook;
import com.pixelmongenerations.common.item.ItemFishingRod;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderFish;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityFishHook;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;

@SideOnly(value=Side.CLIENT)
public class RenderHook
extends RenderFish {
    private static final ResourceLocation FISH_PARTICLES = new ResourceLocation("textures/particle/particles.png");

    public RenderHook(RenderManager renderManager) {
        super(renderManager);
    }

    public void doRenderHook(EntityHook hook, double x, double y, double z, float p_180558_9_) {
        int state;
        GlStateManager.pushMatrix();
        GlStateManager.translate((float)x, (float)y, (float)z);
        GlStateManager.enableRescaleNormal();
        GlStateManager.scale(0.5f, 0.5f, 0.5f);
        this.bindEntityTexture(hook);
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder vertexbuffer = tessellator.getBuffer();
        int b0 = 1;
        int b1 = 2;
        float f2 = (float)(b0 * 8) / 128.0f;
        float f3 = (float)(b0 * 8 + 8) / 128.0f;
        float f4 = (float)(b1 * 8) / 128.0f;
        float f5 = (float)(b1 * 8 + 8) / 128.0f;
        float f6 = 1.0f;
        float f7 = 0.5f;
        float f8 = 0.5f;
        GlStateManager.rotate(180.0f - this.renderManager.playerViewY, 0.0f, 1.0f, 0.0f);
        GlStateManager.rotate(-this.renderManager.playerViewX, 1.0f, 0.0f, 0.0f);
        vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX_NORMAL);
        vertexbuffer.pos(0.0f - f7, 0.0f - f8, 0.0).tex(f2, f5).normal(0.0f, 1.0f, 0.0f).endVertex();
        vertexbuffer.pos(f6 - f7, 0.0f - f8, 0.0).tex(f3, f5).normal(0.0f, 1.0f, 0.0f).endVertex();
        vertexbuffer.pos(f6 - f7, 1.0f - f8, 0.0).tex(f3, f4).normal(0.0f, 1.0f, 0.0f).endVertex();
        vertexbuffer.pos(0.0f - f7, 1.0f - f8, 0.0).tex(f2, f4).normal(0.0f, 1.0f, 0.0f).endVertex();
        tessellator.draw();
        GlStateManager.disableRescaleNormal();
        GlStateManager.popMatrix();
        if (hook.angler != null) {
            this.renderRodAndHook(hook, x, y, z, p_180558_9_);
        }
        if ((state = hook.getDataManager().get(EntityHook.DATA_HOOK_STATE).intValue()) != -1 && Minecraft.getMinecraft().player == hook.angler) {
            FontRenderer fontRenderer = this.getFontRendererFromRenderManager();
            float var13 = 1.6f;
            float var14 = 0.02f * var13;
            GlStateManager.pushMatrix();
            GlStateManager.translate((float)x, (float)y + 1.0f, (float)z);
            GL11.glNormal3f((float)0.0f, (float)1.0f, (float)0.0f);
            GlStateManager.rotate(-this.renderManager.playerViewY, 0.0f, 1.0f, 0.0f);
            GlStateManager.rotate(this.renderManager.playerViewX, 1.0f, 0.0f, 0.0f);
            GlStateManager.scale(-var14, -var14, var14);
            GlStateManager.disableLighting();
            GlStateManager.enableDepth();
            GlStateManager.disableAlpha();
            GlStateManager.enableTexture2D();
            StringBuilder text = new StringBuilder(". . .");
            if (state == 0) {
                text = new StringBuilder(hook.dotsShowing == 0 ? "." : (hook.dotsShowing == 1 ? ". ." : ". . ."));
            } else if (state > 0) {
                text = new StringBuilder("!");
                for (int i = 1; i < state; ++i) {
                    text.append(" !");
                }
            }
            fontRenderer.drawString(text.toString(), -fontRenderer.getStringWidth(text.toString()) / 2 + 1, 0, 0x20FFFFFF);
            GlStateManager.depthMask(true);
            GlStateManager.enableLighting();
            GlStateManager.disableBlend();
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GlStateManager.popMatrix();
        }
    }

    public void renderRodAndHook(EntityHook entity, double x, double y, double z, float partialTicks) {
        EntityPlayer entityplayer = entity.getAngler();
        if (!this.renderOutlines) {
            double d7;
            double d6;
            double d5;
            double d4;
            GlStateManager.pushMatrix();
            GlStateManager.translate((float)x, (float)y, (float)z);
            GlStateManager.enableRescaleNormal();
            GlStateManager.scale(0.5f, 0.5f, 0.5f);
            this.bindEntityTexture(entity);
            Tessellator tessellator = Tessellator.getInstance();
            BufferBuilder bufferbuilder = tessellator.getBuffer();
            GlStateManager.rotate(180.0f - this.renderManager.playerViewY, 0.0f, 1.0f, 0.0f);
            GlStateManager.rotate((float)(this.renderManager.options.thirdPersonView == 2 ? -1 : 1) * -this.renderManager.playerViewX, 1.0f, 0.0f, 0.0f);
            if (this.renderOutlines) {
                GlStateManager.enableColorMaterial();
                GlStateManager.enableOutlineMode(this.getTeamColor(entity));
            }
            bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX_NORMAL);
            bufferbuilder.pos(-0.5, -0.5, 0.0).tex(0.0625, 0.1875).normal(0.0f, 1.0f, 0.0f).endVertex();
            bufferbuilder.pos(0.5, -0.5, 0.0).tex(0.125, 0.1875).normal(0.0f, 1.0f, 0.0f).endVertex();
            bufferbuilder.pos(0.5, 0.5, 0.0).tex(0.125, 0.125).normal(0.0f, 1.0f, 0.0f).endVertex();
            bufferbuilder.pos(-0.5, 0.5, 0.0).tex(0.0625, 0.125).normal(0.0f, 1.0f, 0.0f).endVertex();
            tessellator.draw();
            if (this.renderOutlines) {
                GlStateManager.disableOutlineMode();
                GlStateManager.disableColorMaterial();
            }
            GlStateManager.disableRescaleNormal();
            GlStateManager.popMatrix();
            int k = entityplayer.getPrimaryHand() == EnumHandSide.RIGHT ? 1 : -1;
            ItemStack itemstack = entityplayer.getHeldItemMainhand();
            if (!(itemstack.getItem() instanceof ItemFishingRod)) {
                k = -k;
            }
            float f7 = entityplayer.getSwingProgress(partialTicks);
            float f8 = MathHelper.sin(MathHelper.sqrt(f7) * (float)Math.PI);
            float f9 = (entityplayer.prevRenderYawOffset + (entityplayer.renderYawOffset - entityplayer.prevRenderYawOffset) * partialTicks) * ((float)Math.PI / 180);
            double d0 = MathHelper.sin(f9);
            double d1 = MathHelper.cos(f9);
            double d2 = (double)k * 0.35;
            if ((this.renderManager.options == null || this.renderManager.options.thirdPersonView <= 0) && entityplayer == Minecraft.getMinecraft().player) {
                float f10 = this.renderManager.options.fovSetting;
                Vec3d vec3d = new Vec3d((double)k * -0.36 * (double)f10, -0.045 * (double)(f10 / 100.0f), 0.4);
                vec3d = vec3d.rotatePitch((-entityplayer.prevRotationPitch + (entityplayer.rotationPitch - entityplayer.prevRotationPitch) * partialTicks) * ((float)Math.PI / 180));
                vec3d = vec3d.rotateYaw((-entityplayer.prevRotationYaw + (entityplayer.rotationYaw - entityplayer.prevRotationYaw) * partialTicks) * ((float)Math.PI / 180));
                vec3d = vec3d.rotateYaw(f8 * 0.5f);
                vec3d = vec3d.rotatePitch(-f8 * 0.7f);
                d4 = entityplayer.prevPosX + (entityplayer.posX - entityplayer.prevPosX) * (double)partialTicks + vec3d.x;
                d5 = entityplayer.prevPosY + (entityplayer.posY - entityplayer.prevPosY) * (double)partialTicks + vec3d.y;
                d6 = entityplayer.prevPosZ + (entityplayer.posZ - entityplayer.prevPosZ) * (double)partialTicks + vec3d.z;
                d7 = entityplayer.getEyeHeight();
            } else {
                d4 = entityplayer.prevPosX + (entityplayer.posX - entityplayer.prevPosX) * (double)partialTicks - d1 * d2 - d0 * 0.8;
                d5 = entityplayer.prevPosY + (double)entityplayer.getEyeHeight() + (entityplayer.posY - entityplayer.prevPosY) * (double)partialTicks - 0.45;
                d6 = entityplayer.prevPosZ + (entityplayer.posZ - entityplayer.prevPosZ) * (double)partialTicks - d0 * d2 + d1 * 0.8;
                d7 = entityplayer.isSneaking() ? -0.1875 : 0.0;
            }
            double d13 = entity.prevPosX + (entity.posX - entity.prevPosX) * (double)partialTicks;
            double d8 = entity.prevPosY + (entity.posY - entity.prevPosY) * (double)partialTicks + 0.25;
            double d9 = entity.prevPosZ + (entity.posZ - entity.prevPosZ) * (double)partialTicks;
            double d10 = (float)(d4 - d13);
            double d11 = (double)((float)(d5 - d8)) + d7 - 0.05;
            double d12 = (float)(d6 - d9);
            GlStateManager.disableTexture2D();
            GlStateManager.disableLighting();
            bufferbuilder.begin(3, DefaultVertexFormats.POSITION_COLOR);
            for (int i1 = 0; i1 <= 16; ++i1) {
                float f11 = (float)i1 / 16.0f;
                bufferbuilder.pos(x + d10 * (double)f11, y + d11 * (double)(f11 * f11 + f11) * 0.5 + 0.25, z + d12 * (double)f11).color(0, 0, 0, 255).endVertex();
            }
            tessellator.draw();
            GlStateManager.enableLighting();
            GlStateManager.enableTexture2D();
        }
    }

    @Override
    protected ResourceLocation getEntityTexture(EntityFishHook par1Entity) {
        return FISH_PARTICLES;
    }

    @Override
    public void doRender(EntityFishHook par1Entity, double par2, double par4, double par6, float par8, float par9) {
        this.doRenderHook((EntityHook)par1Entity, par2, par4, par6, par9);
    }
}

