/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.models.PixelmonModelBase;
import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.client.models.PixelmonModelSmd;
import com.pixelmongenerations.common.entity.pixelmon.EntityStatue;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumStatueTextureType;
import com.pixelmongenerations.core.proxy.ClientProxy;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import org.lwjgl.opengl.GL11;

public class RenderStatue
extends RenderLiving<EntityStatue> {
    private int defaultNameRenderDistance = 8;
    private int defaultBossNameRenderDistanceExtension = 8;
    private int configNameRenderMultiplier = Math.max(1, Math.min(PixelmonConfig.nameplateRangeModifier, 3));
    private int nameRenderDistanceNormal = this.defaultNameRenderDistance * this.configNameRenderMultiplier;
    private int nameRenderDistanceBoss = this.nameRenderDistanceNormal + this.defaultBossNameRenderDistanceExtension;

    public RenderStatue(RenderManager manager) {
        super(manager, null, 0.5f);
    }

    @Override
    public void doRender(EntityStatue statue, double d, double d1, double d2, float f, float f1) {
        float renderdistance;
        GlStateManager.alphaFunc(516, 0.1f);
        if (!statue.isInitialised) {
            statue.init(statue.getPokemonName());
        }
        float distance = statue.getDistance(this.renderManager.renderViewEntity);
        if (statue.getModel() != null) {
            this.renderStatue(statue, d, d1, d2, f1, false);
        }
        boolean owned = ServerStorageDisplay.contains(statue.getPokemonId());
        float f2 = renderdistance = statue.getBossMode() != EnumBossMode.NotBoss ? (float)this.nameRenderDistanceBoss : (float)this.nameRenderDistanceNormal;
        if (distance <= renderdistance || owned) {
            this.drawNameTag(statue, d, d1, d2, owned);
        }
    }

    public void renderStatue(EntityStatue pixelmon, double x, double y, double z, float partialTicks, boolean fromPokedex) {
        GlStateManager.pushMatrix();
        GlStateManager.disableCull();
        this.mainModel = pixelmon.getModel();
        this.mainModel.swingProgress = this.getSwingProgress(pixelmon, partialTicks);
        this.mainModel.isRiding = pixelmon.isRiding();
        this.mainModel.isChild = pixelmon.isChild();
        if (this.mainModel instanceof PixelmonModelSmd && ((PixelmonModelSmd)this.mainModel).theModel.hasAnimations()) {
            ((PixelmonModelSmd)this.mainModel).setupForRender(pixelmon);
            if (!pixelmon.isAnimated()) {
                int frame = MathHelper.clamp(pixelmon.getAnimationFrame(), 0, pixelmon.getFrameCount() - 1);
                ((PixelmonModelSmd)this.mainModel).theModel.currentAnimation.setCurrentFrame(frame);
            }
            pixelmon.world.profiler.startSection("statue_animate");
            ((PixelmonModelSmd)this.mainModel).theModel.animate();
            pixelmon.world.profiler.endSection();
        }
        try {
            float f2 = this.interpolateRotation(pixelmon.prevRenderYawOffset, pixelmon.renderYawOffset, partialTicks);
            float f3 = this.interpolateRotation(pixelmon.prevRotationYawHead, pixelmon.rotationYawHead, partialTicks);
            float f4 = f3 - f2;
            float f9 = pixelmon.prevRotationPitch + (pixelmon.rotationPitch - pixelmon.prevRotationPitch) * partialTicks;
            this.renderLivingAt(pixelmon, x, y, z);
            float f5 = 0.0f;
            this.applyRotations(pixelmon, f5, f2, partialTicks);
            GlStateManager.enableRescaleNormal();
            GlStateManager.scale(-1.0f, -1.0f, 1.0f);
            this.preRenderCallback(pixelmon, partialTicks, fromPokedex);
            GlStateManager.translate(0.0f, -1.5078125f, 0.0f);
            float f7 = pixelmon.prevLimbSwingAmount + (pixelmon.limbSwingAmount - pixelmon.prevLimbSwingAmount) * partialTicks;
            float f8 = pixelmon.limbSwing - pixelmon.limbSwingAmount * (1.0f - partialTicks);
            GlStateManager.enableAlpha();
            this.mainModel.setLivingAnimations(pixelmon, f8, f7, partialTicks);
            this.mainModel.setRotationAngles(f8, f7, f5, f4, f9, 0.0625f, pixelmon);
            if (pixelmon.getBossMode() != EnumBossMode.NotBoss) {
                GlStateManager.color(pixelmon.getBossMode().r, pixelmon.getBossMode().g, pixelmon.getBossMode().b, 1.0f);
            } else {
                GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            }
            boolean flag = this.setDoRenderBrightness(pixelmon, partialTicks);
            this.renderModel(pixelmon, f8, f7, f5, 0.0f, 0.0f, 0.0625f);
            if (flag) {
                this.unsetBrightness();
            }
            GlStateManager.depthMask(true);
            GlStateManager.disableRescaleNormal();
        }
        catch (Exception var25) {
            var25.printStackTrace();
        }
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        if (!fromPokedex) {
            GlStateManager.enableTexture2D();
        }
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
        GlStateManager.enableCull();
        GlStateManager.popMatrix();
        this.renderName(pixelmon, x, y, z);
    }

    @Override
    protected void renderModel(EntityStatue entitylivingbaseIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor) {
        boolean flag1;
        boolean flag = this.isVisible(entitylivingbaseIn);
        boolean bl = flag1 = !flag && !entitylivingbaseIn.isInvisibleToPlayer(Minecraft.getMinecraft().player);
        if (flag || flag1) {
            if (!this.bindEntityTexture(entitylivingbaseIn)) {
                return;
            }
            if (flag1) {
                GlStateManager.enableBlendProfile(GlStateManager.Profile.TRANSPARENT_MODEL);
            }
            this.mainModel.render(entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor);
            if (flag1) {
                GlStateManager.disableBlendProfile(GlStateManager.Profile.TRANSPARENT_MODEL);
            }
        }
    }

    public void drawNameTag(EntityStatue entityPixelmon, double par2, double par4, double par6, boolean owned) {
        if (Minecraft.isGuiEnabled()) {
            String s = entityPixelmon.getLabel();
            if (!entityPixelmon.isSneaking() && !s.isEmpty()) {
                this.renderLabel(entityPixelmon, s, par2, par4, par6, owned);
            }
        }
    }

    protected void renderLabel(EntityStatue pixelmon, String name, double x, double y, double z, boolean owned) {
        pixelmon.getDistanceSq(this.renderManager.renderViewEntity);
        FontRenderer var12 = this.getFontRendererFromRenderManager();
        float var13 = 1.6f;
        float baseScale = 0.016666668f * var13;
        GlStateManager.pushMatrix();
        float scaleFactor = PixelmonConfig.scaleModelsUp ? 1.3f : 1.0f;
        GlStateManager.translate((float)x + 0.0f, (float)y + 0.7f + pixelmon.height * pixelmon.getPixelmonScale() * scaleFactor * pixelmon.getScaleFactor(), (float)z);
        GL11.glNormal3f((float)0.0f, (float)1.0f, (float)0.0f);
        GlStateManager.rotate(-this.renderManager.playerViewY, 0.0f, 1.0f, 0.0f);
        GlStateManager.rotate(this.renderManager.playerViewX, 1.0f, 0.0f, 0.0f);
        GlStateManager.scale(-baseScale, -baseScale, baseScale);
        GlStateManager.disableLighting();
        GlStateManager.depthMask(false);
        if (owned) {
            GlStateManager.disableDepth();
        }
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(770, 771);
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder vertexbuffer = tessellator.getBuffer();
        int var16 = 0;
        GlStateManager.disableTexture2D();
        int var17 = var12.getStringWidth(name) / 2;
        vertexbuffer.begin(7, DefaultVertexFormats.POSITION_COLOR);
        vertexbuffer.pos(-var17 - 1, -1 + var16, 0.0).color(0.0f, 0.0f, 0.0f, 0.25f).endVertex();
        vertexbuffer.pos(-var17 - 1, 8 + var16, 0.0).color(0.0f, 0.0f, 0.0f, 0.25f).endVertex();
        vertexbuffer.pos(var17 + 1, 8 + var16, 0.0).color(0.0f, 0.0f, 0.0f, 0.25f).endVertex();
        vertexbuffer.pos(var17 + 1, -1 + var16, 0.0).color(0.0f, 0.0f, 0.0f, 0.25f).endVertex();
        tessellator.draw();
        GlStateManager.enableTexture2D();
        var12.drawString(name, -var12.getStringWidth(name) / 2, var16, 0x20FFFFFF);
        if (owned) {
            GlStateManager.enableDepth();
        }
        GlStateManager.depthMask(true);
        var12.drawString(name, -var12.getStringWidth(name) / 2, var16, pixelmon.getBossMode().getColourInt());
        GlStateManager.enableLighting();
        GlStateManager.disableBlend();
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.popMatrix();
    }

    protected void preRenderScale(EntityStatue entity, float f, boolean fromGui) {
        float scaleFactor = PixelmonConfig.scaleModelsUp ? 1.3f : 1.0f;
        scaleFactor *= entity.getScaleFactor();
        if (entity.getModel() instanceof PixelmonModelBase) {
            scaleFactor *= ((PixelmonModelBase)entity.getModel()).getScale();
        }
        if (fromGui) {
            GlStateManager.scale(scaleFactor * entity.baseStats.giScale, scaleFactor * entity.baseStats.giScale, scaleFactor * entity.baseStats.giScale);
        } else {
            GlStateManager.scale(scaleFactor * entity.getPixelmonScale() * entity.baseStats.giScale, scaleFactor * entity.getPixelmonScale() * entity.baseStats.giScale, scaleFactor * entity.getPixelmonScale() * entity.baseStats.giScale);
            if (entity.baseStats.doesHover) {
                GlStateManager.translate(0.0f, -1.0f * entity.baseStats.hoverHeight, 0.0f);
            }
        }
    }

    @Override
    protected void preRenderCallback(EntityStatue entityStatue, float f) {
        this.preRenderScale(entityStatue, f, false);
    }

    protected void preRenderCallback(EntityStatue entityLiving, float f, boolean fromGui) {
        this.preRenderScale(entityLiving, f, fromGui);
    }

    @Override
    protected boolean bindEntityTexture(EntityStatue entity) {
        ResourceLocation resourcelocation = this.getEntityTexture(entity);
        if (resourcelocation == null) {
            return false;
        }
        try {
            if (entity.isGmaxModel() && entity.getSpecies().hasGmaxForm()) {
                if (entity.getSpecies() == EnumSpecies.Alcremie) {
                    this.bindTexture(new ResourceLocation("pixelmon", entity.getTextureType().equals((Object)EnumStatueTextureType.Shiny) ? "textures/pokemon/pokemon-shiny/shinyalcremie-gigantamax.png" : "textures/pokemon/alcremie-gigantamax.png"));
                } else {
                    String pokeTexture = resourcelocation.getPath();
                    if (!entity.getSpecialTexture().isEmpty()) {
                        pokeTexture = pokeTexture.replaceAll(entity.getSpecialTexture(), "");
                    }
                    pokeTexture = pokeTexture.contains("-") ? (PixelmonModelRegistry.gmaxModels.get((Object)entity.getSpecies()).size() == 1 ? pokeTexture.replaceAll(".png", "-gigantamax.png") : pokeTexture.replaceAll(".png", "-gigantamax.png")) : pokeTexture.replaceAll(".png", "-gigantamax.png");
                    this.bindTexture(new ResourceLocation("pixelmon", pokeTexture));
                }
                return true;
            }
        }
        catch (Exception pokeTexture) {
            // empty catch block
        }
        String[] splits = resourcelocation.getPath().split("/");
        String textureName = splits[splits.length - 1].replaceAll(".png", "");
        TextureResource dynTexture = ClientProxy.TEXTURE_STORE.getObject(textureName);
        if (dynTexture != null) {
            dynTexture.bindTexture();
        } else {
            this.bindTexture(resourcelocation);
        }
        return true;
    }

    @Override
    protected ResourceLocation getEntityTexture(EntityStatue entityStatue) {
        return entityStatue.getTexture();
    }

    @Override
    protected float interpolateRotation(float par1, float par2, float par3) {
        float f3;
        for (f3 = par2 - par1; f3 < -180.0f; f3 += 360.0f) {
        }
        while (f3 >= 180.0f) {
            f3 -= 360.0f;
        }
        return par1 + par3 * f3;
    }
}

