/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.client.render;

import com.pixelmongenerations.client.particle.ParticleRedstone;
import com.pixelmongenerations.common.entity.SpawningEntity;
import java.awt.Color;
import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.Vec3d;

public class RenderSpawningEntity
extends Render<SpawningEntity> {
    public RenderSpawningEntity(RenderManager renderManager) {
        super(renderManager);
    }

    @Override
    public void doRender(SpawningEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {
        this.processAnimation(entity);
    }

    public void processAnimation(SpawningEntity entity) {
        Vec3d center = entity.getPositionVector();
        double theta = (double)entity.getTick() / (double)entity.getMaxTick();
        double t = (1.0 - theta) * 2.0 * Math.PI;
        double radius = 1.0;
        double dx = 0.5 * t * radius * Math.cos(4.0 * t);
        double dy = 0.5 * t * radius * Math.sin(4.0 * t);
        double dz = theta * 2.0 * Math.PI;
        List<Color> color = entity.getColors();
        for (int i = 0; i < 6; ++i) {
            Color c = color.get(i % color.size());
            double r = (double)i / 6.0 * 2.0 * Math.PI;
            Minecraft.getMinecraft().effectRenderer.addEffect(new ParticleRedstone(entity.getEntityWorld(), center.x + RenderSpawningEntity.rotateX(r, dx, dy), center.y + dz, center.z + RenderSpawningEntity.rotateY(r, dx, dy), (float)c.getRed() / 255.0f, (float)c.getGreen() / 255.0f, (float)c.getBlue() / 255.0f));
        }
    }

    public static double rotateX(double r, double x, double y) {
        return Math.cos(r) * x - Math.sin(r) * y;
    }

    public static double rotateY(double r, double x, double y) {
        return Math.sin(r) * x + Math.cos(r) * y;
    }

    @Override
    @Nullable
    protected ResourceLocation getEntityTexture(SpawningEntity entity) {
        return null;
    }
}

