/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.decorative.BlockShopFront;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShopFront;
import com.pixelmongenerations.core.enums.EnumShopFrontType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderTileEntityShopFront
extends TileEntityRenderer<TileEntityShopFront> {
    private static final ResourceLocation TEXTURE = new ResourceLocation("pixelmon:textures/blocks/shopfront.png");

    public RenderTileEntityShopFront() {
        this.scale = 1.0f;
        this.disableCulling = true;
    }

    @Override
    public void renderTileEntity(TileEntityShopFront te, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        if (te.isCulled()) {
            return;
        }
        GL11.glPushMatrix();
        if (state.getBlock() instanceof BlockShopFront) {
            this.bindTexture(TEXTURE);
            BlockShopFront block = (BlockShopFront)state.getBlock();
            EnumShopFrontType type = block.getShopFrontType();
            if (type == EnumShopFrontType.SHELF_1) {
                GL11.glTranslatef((float)-0.45f, (float)-1.0f, (float)0.08f);
            } else if (type == EnumShopFrontType.SHELF_2) {
                GL11.glTranslatef((float)-0.5f, (float)-1.0f, (float)0.08f);
            } else if (type == EnumShopFrontType.ROUND_1) {
                GL11.glTranslatef((float)-0.51f, (float)-0.5f, (float)-0.06f);
            } else if (type == EnumShopFrontType.ROUND_2) {
                GL11.glTranslatef((float)-0.54f, (float)-0.5f, (float)-0.06f);
            } else if (type == EnumShopFrontType.CASE_1) {
                GL11.glTranslatef((float)-0.47f, (float)-0.5f, (float)0.0f);
            } else {
                GL11.glTranslatef((float)-0.54f, (float)-0.5f, (float)0.0f);
            }
            GL11.glRotatef((float)180.0f, (float)0.0f, (float)1.0f, (float)0.0f);
            GlStateManager.disableNormalize();
            GlStateManager.shadeModel(7425);
            GlStateManager.disableCull();
            type.getModel().renderAll();
            if (type.hasGlass()) {
                GlStateManager.enableBlend();
                GlStateManager.blendFunc(770, 771);
                GlStateManager.depthMask(false);
                GL11.glColor4f((float)1.0f, (float)1.0f, (float)1.0f, (float)0.3f);
                GL11.glTranslatef((float)0.0f, (float)-0.35f, (float)0.0f);
                type.getGlassModel().renderAll();
                GlStateManager.disableBlend();
                GlStateManager.depthMask(true);
            }
        }
        GL11.glPopMatrix();
    }
}

