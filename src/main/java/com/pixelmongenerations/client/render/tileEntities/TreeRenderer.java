/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityTree;
import java.util.HashMap;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;

public class TreeRenderer
extends TileEntityRenderer<TileEntityTree> {
    private static final HashMap<Integer, BlockModelHolder<GenericSmdModel>> trees;
    private static final ResourceLocation texture;

    public TreeRenderer() {
        this.correctionAngles = 180;
    }

    @Override
    public void renderTileEntity(TileEntityTree tree, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        BlockModelHolder<GenericSmdModel> model;
        int treeType = tree.getTreeType();
        if (treeType > 0 && (model = trees.get(treeType)) != null) {
            this.bindTexture(texture);
            model.render();
        } else if (treeType == 0) {
            tree.setTreeType(1);
        } else {
            System.out.println("Tree type not saved. Listed number: " + treeType);
        }
    }

    static {
        texture = new ResourceLocation("pixelmon", "textures/blocks/trees/tree1.png");
        trees = new HashMap();
        trees.put(1, new BlockModelHolder("pixelutilities/tree/tree1.pqc"));
        trees.put(2, new BlockModelHolder("pixelutilities/tree/tree2.pqc"));
        trees.put(3, new BlockModelHolder("pixelutilities/tree/tree3.pqc"));
        trees.put(4, new BlockModelHolder("pixelutilities/tree/tree4.pqc"));
    }
}

