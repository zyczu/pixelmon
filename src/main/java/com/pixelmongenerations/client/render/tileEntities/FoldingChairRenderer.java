/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.ModelFoldingChair;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityGreenFoldingChair;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;

public class FoldingChairRenderer
extends TileEntityRenderer<TileEntityGreenFoldingChair> {
    private static final BlockModelHolder<ModelFoldingChair> model = new BlockModelHolder<ModelFoldingChair>(ModelFoldingChair.class);
    private static final ResourceLocation texture = new ResourceLocation("pixelmon", "textures/blocks/GreenFoldingChairModel.png");

    public FoldingChairRenderer() {
        this.scale = 0.0625f;
        this.yOffset = 1.5f;
    }

    @Override
    public void renderTileEntity(TileEntityGreenFoldingChair te, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        this.bindTexture(texture);
        model.render();
    }
}

