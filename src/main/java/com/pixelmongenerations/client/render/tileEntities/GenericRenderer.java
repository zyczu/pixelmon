/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Maps
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.google.common.collect.Maps;
import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import java.util.HashMap;
import java.util.Map;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class GenericRenderer<G extends GenericRenderer<G, T>, T extends TileEntity>
extends TileEntitySpecialRenderer<T> {
    protected ResourceLocation texture;
    public ModelBase model;
    public HashMap<GenericSmdModel, ResourceLocation> childModels = Maps.newHashMap();
    protected int correctionAngles = 0;
    protected int rotateDegrees = 0;
    protected float scale = 0.0f;
    protected float yOffset = 0.0f;
    public boolean blend = false;
    public boolean disableCulling = false;
    public boolean disableLighting = false;

    public GenericRenderer(String textureFileName, ModelBase model) {
        this.texture = new ResourceLocation("pixelmon:textures/blocks/" + textureFileName);
        this.model = model;
    }

    public GenericRenderer(String textureFileName, ModelBase model, int correctionAngles) {
        this.texture = new ResourceLocation("pixelmon:textures/blocks/" + textureFileName);
        this.model = model;
        this.correctionAngles = correctionAngles;
    }

    public void setTexture(ResourceLocation texture) {
        this.texture = texture;
    }

    @Override
    public void render(T te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        IBlockState state = this.getWorld().getBlockState(((TileEntity)te).getPos());
        Block block = state.getBlock();
        if (block instanceof MultiBlock || block instanceof GenericRotatableModelBlock) {
            EnumFacing facing;
            EnumFacing enumFacing = facing = block instanceof MultiBlock ? state.getValue(MultiBlock.FACING) : state.getValue(GenericRotatableModelBlock.FACING);
            this.rotateDegrees = facing == EnumFacing.EAST ? 270 : (facing == EnumFacing.NORTH ? 0 : (facing == EnumFacing.WEST ? 90 : (facing == EnumFacing.SOUTH ? 180 : 0)));
        } else {
            this.rotateDegrees = 0;
        }
        this.bindTexture(this.texture);
        GlStateManager.enableRescaleNormal();
        GlStateManager.pushMatrix();
        if (this.blend) {
            GlStateManager.enableBlend();
            GlStateManager.blendFunc(770, 771);
            GlStateManager.depthMask(false);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        }
        if (this.disableCulling) {
            GlStateManager.disableCull();
        }
        if (this.disableLighting) {
            GlStateManager.disableLighting();
        }
        GlStateManager.translate((float)x + 0.5f, (float)y + this.yOffset, (float)z + 0.5f);
        GlStateManager.rotate(this.rotateDegrees + this.correctionAngles, 0.0f, 1.0f, 0.0f);
        GlStateManager.rotate(180.0f, 0.0f, 0.0f, 1.0f);
        if (this.scale != 0.0f) {
            GlStateManager.scale(this.scale, this.scale, this.scale);
        }
        this.model.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 1.0f);
        for (Map.Entry<GenericSmdModel, ResourceLocation> childEntry : this.childModels.entrySet()) {
            this.bindTexture(childEntry.getValue());
            childEntry.getKey().render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 1.0f);
        }
        if (this.blend) {
            GlStateManager.disableBlend();
            GlStateManager.depthMask(true);
        }
        if (this.disableCulling) {
            GlStateManager.enableCull();
        }
        if (this.disableLighting) {
            GlStateManager.enableLighting();
        }
        GlStateManager.disableRescaleNormal();
        GlStateManager.popMatrix();
    }
}

