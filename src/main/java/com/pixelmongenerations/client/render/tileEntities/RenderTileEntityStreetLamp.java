/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityStreetLamp;
import net.minecraft.block.state.IBlockState;
import org.lwjgl.opengl.GL11;

public class RenderTileEntityStreetLamp
extends TileEntityRenderer<TileEntityStreetLamp> {
    public RenderTileEntityStreetLamp() {
        this.scale = 1.0f;
        this.disableCulling = true;
    }

    @Override
    public void renderTileEntity(TileEntityStreetLamp te, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        this.bindTexture(te.getTexture());
        GL11.glPushMatrix();
        GL11.glScaled((double)1.5, (double)1.5, (double)1.5);
        te.getModel().render();
        GL11.glPopMatrix();
    }
}

