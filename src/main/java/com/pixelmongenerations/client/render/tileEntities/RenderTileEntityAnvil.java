/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.ModelAnvil;
import com.pixelmongenerations.client.models.discs.ModelDiscFlat;
import com.pixelmongenerations.client.models.discs.ModelDiscHemiSphere;
import com.pixelmongenerations.client.models.discs.ModelDiscStage1;
import com.pixelmongenerations.client.models.discs.ModelDiscStage2;
import com.pixelmongenerations.client.models.plates.ModelIngot;
import com.pixelmongenerations.client.models.plates.ModelPlate;
import com.pixelmongenerations.client.models.plates.ModelPlateStage2;
import com.pixelmongenerations.client.models.plates.ModelPlateStage3;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.machines.BlockAnvil;
import com.pixelmongenerations.common.block.tileEntities.TileEntityAnvil;
import com.pixelmongenerations.common.item.ItemPokeballDisc;
import com.pixelmongenerations.common.item.ItemPokeballLid;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsPokeballs;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class RenderTileEntityAnvil
extends TileEntityRenderer<TileEntityAnvil> {
    private static final ResourceLocation anvil_texture = new ResourceLocation("pixelmon:textures/blocks/anvil.png");
    private static final ResourceLocation aluminiumIngot_texture = new ResourceLocation("pixelmon:textures/blocks/aluminium/ingot.png");
    private static final ResourceLocation ironDisc_texture = new ResourceLocation("pixelmon:textures/pokeballs/irondisc.png");
    private static final BlockModelHolder<ModelAnvil> model = new BlockModelHolder<ModelAnvil>(ModelAnvil.class);
    private static final BlockModelHolder<ModelDiscFlat> modelDiscFlat = new BlockModelHolder<ModelDiscFlat>(ModelDiscFlat.class);
    private static final BlockModelHolder<ModelDiscHemiSphere> modelDiscHemiSphere = new BlockModelHolder<ModelDiscHemiSphere>(ModelDiscHemiSphere.class);
    private static final BlockModelHolder<ModelDiscStage1> modelDiscStage1 = new BlockModelHolder<ModelDiscStage1>(ModelDiscStage1.class);
    private static final BlockModelHolder<ModelDiscStage2> modelDiscStage2 = new BlockModelHolder<ModelDiscStage2>(ModelDiscStage2.class);
    private static final BlockModelHolder<ModelIngot> modelPlateIngot = new BlockModelHolder<ModelIngot>(ModelIngot.class);
    private static final BlockModelHolder<ModelPlate> modelPlate = new BlockModelHolder<ModelPlate>(ModelPlate.class);
    private static final BlockModelHolder<ModelPlateStage2> modelPlateStage2 = new BlockModelHolder<ModelPlateStage2>(ModelPlateStage2.class);
    private static final BlockModelHolder<ModelPlateStage3> modelPlateStage3 = new BlockModelHolder<ModelPlateStage3>(ModelPlateStage3.class);

    public RenderTileEntityAnvil() {
        this.yOffset = 1.5f;
        this.correctionAngles = 90;
    }

    @Override
    public void renderTileEntity(TileEntityAnvil anvil, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        this.bindTexture(anvil_texture);
        ((ModelAnvil)model.getModel()).renderModel(0.0625f);
        if (anvil.itemOnAnvil != null) {
            Item item = anvil.itemOnAnvil;
            if (item == PixelmonItems.aluminiumIngot) {
                this.bindTexture(aluminiumIngot_texture);
                if (anvil.state == 0) {
                    ((ModelIngot)modelPlateIngot.getModel()).renderModel(0.0625f);
                } else if (anvil.state == 1) {
                    ((ModelPlateStage2)modelPlateStage2.getModel()).renderModel(0.0625f);
                } else if (anvil.state == 2) {
                    ((ModelPlateStage3)modelPlateStage3.getModel()).renderModel(0.0625f);
                }
            } else if (item == PixelmonItems.aluminiumPlate) {
                this.bindTexture(aluminiumIngot_texture);
                ((ModelPlate)modelPlate.getModel()).renderModel(0.0625f);
            } else if (item instanceof ItemPokeballDisc || item == PixelmonItemsPokeballs.ironDisc || item == PixelmonItemsPokeballs.aluDisc) {
                if (item == PixelmonItemsPokeballs.ironDisc || item == PixelmonItemsPokeballs.aluDisc) {
                    this.bindTexture(ironDisc_texture);
                } else {
                    this.bindTexture(new ResourceLocation("pixelmon:textures/pokeballs/anvil/" + ((ItemPokeballDisc)item).pokeball.getTexture()));
                }
                if (anvil.state == 0) {
                    ((ModelDiscFlat)modelDiscFlat.getModel()).renderModel(0.0625f);
                } else if (anvil.state == 1) {
                    ((ModelDiscStage1)modelDiscStage1.getModel()).renderModel(0.0625f);
                } else if (anvil.state == 2) {
                    ((ModelDiscStage2)modelDiscStage2.getModel()).renderModel(0.0625f);
                }
            } else if (item instanceof ItemPokeballLid || item == PixelmonItemsPokeballs.ironBase || item == PixelmonItemsPokeballs.aluBase) {
                if (item == PixelmonItemsPokeballs.ironBase || item == PixelmonItemsPokeballs.aluBase) {
                    this.bindTexture(ironDisc_texture);
                } else {
                    this.bindTexture(new ResourceLocation("pixelmon:textures/pokeballs/anvil/" + ((ItemPokeballLid)item).pokeball.getTexture()));
                }
                ((ModelDiscHemiSphere)modelDiscHemiSphere.getModel()).renderModel(0.0625f);
            }
        }
    }

    @Override
    protected int getRotation(IBlockState state) {
        if (state.getBlock() instanceof BlockAnvil) {
            EnumFacing facing = state.getValue(BlockAnvil.FACING);
            if (facing == EnumFacing.EAST) {
                return 270;
            }
            if (facing == EnumFacing.NORTH) {
                return 0;
            }
            if (facing == EnumFacing.WEST) {
                return 90;
            }
            if (facing == EnumFacing.SOUTH) {
                return 180;
            }
        }
        return 0;
    }
}

