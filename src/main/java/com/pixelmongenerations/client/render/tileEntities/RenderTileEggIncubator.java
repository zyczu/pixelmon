/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityEggIncubator;
import com.pixelmongenerations.core.Pixelmon;
import java.text.DecimalFormat;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderTileEggIncubator
extends TileEntityRenderer<TileEntityEggIncubator> {
    private static final DecimalFormat df2 = new DecimalFormat(" #,##0'%'");
    private static final ResourceLocation textureMain = new ResourceLocation("pixelmon", "textures/blocks/egg_incubator/main.png");
    private static final ResourceLocation textureGlass = new ResourceLocation("pixelmon", "textures/blocks/egg_incubator/glass.png");
    private static final ResourceLocation textureEgg = new ResourceLocation("pixelmon", "textures/pokemon/pokemon-egg/regular.png");
    private static final BlockModelHolder<GenericSmdModel> incubatorMain = new BlockModelHolder("blocks/egg_incubator/incubator_main.pqc");
    private static final BlockModelHolder<GenericSmdModel> incubatorGlass = new BlockModelHolder("blocks/egg_incubator/incubator_glass.pqc");
    public static final BlockModelHolder<GenericSmdModel> incubatorEgg = new BlockModelHolder("blocks/egg_incubator/incubator_egg.pqc");

    public RenderTileEggIncubator() {
        this.correctionAngles = 180;
        ((GenericSmdModel)RenderTileEggIncubator.incubatorGlass.getModel()).modelRenderer.setTransparent(0.5f);
    }

    @Override
    public void renderTileEntity(TileEntityEggIncubator incubator, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        GlStateManager.pushMatrix();
        if (incubator.renderPass == 1) {
            GlStateManager.pushMatrix();
            GlStateManager.enableBlend();
            GlStateManager.blendFunc(770, 771);
            GlStateManager.depthMask(false);
            GL11.glColor4f((float)1.0f, (float)1.0f, (float)1.0f, (float)0.3f);
            this.bindTexture(textureGlass);
            incubatorGlass.render();
            GlStateManager.disableBlend();
            GlStateManager.depthMask(true);
            GlStateManager.popMatrix();
        } else {
            this.bindTexture(textureMain);
            incubatorMain.render();
            this.renderCompletionLevel(df2.format(Math.min((double)incubator.percent, 1.0) * 100.0));
            if (!incubator.eggName.isEmpty()) {
                GlStateManager.pushMatrix();
                GlStateManager.rotate(0.0f, 1.0f, 0.0f, 0.0f);
                GlStateManager.rotate(incubator.angle, 0.0f, 1.0f, 0.0f);
                ResourceLocation location = new ResourceLocation("pixelmon", "textures/pokemon/pokemon-egg/" + incubator.eggName + ".png");
                if (!Pixelmon.PROXY.resourceLocationExists(location)) {
                    location = textureEgg;
                }
                this.bindTexture(location);
                incubatorEgg.render();
                GlStateManager.popMatrix();
            }
        }
        GlStateManager.popMatrix();
    }

    private void renderCompletionLevel(String text) {
        FontRenderer fontRenderer = this.getFontRenderer();
        float textScale = 0.0175f;
        GlStateManager.pushMatrix();
        GlStateManager.translate(0.0, (double)-0.9f, -0.4);
        GL11.glNormal3f((float)0.0f, (float)1.0f, (float)0.0f);
        GlStateManager.scale(textScale, textScale, textScale);
        GlStateManager.disableLighting();
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(770, 771);
        int stringWidth = fontRenderer.getStringWidth(text) / 2;
        fontRenderer.drawString(text, -stringWidth, 0, 0xFFFFFF);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.enableLighting();
        GlStateManager.disableBlend();
        GlStateManager.popMatrix();
    }
}

