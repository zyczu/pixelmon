/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.models.obj.WavefrontObject;
import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityBerryTree;
import com.pixelmongenerations.core.enums.EnumBerry;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.BiomeColorHelper;

public class RenderTileEntityBerryTree
extends TileEntitySpecialRenderer<TileEntityBerryTree> {
    private static ResourceLocation weeds = new ResourceLocation(GuiResources.prefix + "berries/weed.png");

    @Override
    public void render(TileEntityBerryTree te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        try {
            EnumBerry berry = te.getBerry();
            if (berry.models[te.getStage() - 1].getModel() != null) {
                this.renderWeeds(te, x, y, z);
                IBlockState state = te.getWorld().getBlockState(te.getPos());
                EnumFacing facing = state.getValue(GenericRotatableModelBlock.FACING);
                int rotateDegrees = facing == EnumFacing.EAST ? 0 : (facing == EnumFacing.NORTH ? 90 : (facing == EnumFacing.WEST ? 180 : (facing == EnumFacing.SOUTH ? 270 : 0)));
                GlStateManager.pushMatrix();
                GlStateManager.translate((float)x + 0.5f, (float)y + 0.5f, (float)z + 0.5f);
                GlStateManager.translate(0.0f, -0.5f, 0.0f);
                GlStateManager.scale(1.0f, 1.0f, 1.0f);
                Minecraft.getMinecraft().renderEngine.bindTexture(te.getTexture());
                GlStateManager.rotate(rotateDegrees, 0.0f, 1.0f, 0.0f);
                float scale = berry.scale;
                if (te.getStage() < 3) {
                    scale = 0.5f;
                }
                if (te.isInfested()) {
                    GlStateManager.color(0.6941f, 0.3176f, 0.7294f);
                }
                GlStateManager.scale(scale, scale, scale);
                GlStateManager.disableNormalize();
                GlStateManager.shadeModel(7425);
                ((WavefrontObject)berry.models[te.getStage() - 1].getModel()).renderAll();
                GlStateManager.popMatrix();
                GlStateManager.color(1.0f, 1.0f, 1.0f);
            }
        }
        catch (Exception exception) {
            // empty catch block
        }
    }

    private void renderWeeds(TileEntityBerryTree te, double x, double y, double z) {
        if (te.isWeeded()) {
            int color = BiomeColorHelper.getFoliageColorAtPos(te.getWorld(), te.getPos());
            float r = (float)(color >> 16 & 0xFF) / 255.0f;
            float g = (float)(color >> 8 & 0xFF) / 255.0f;
            float b = (float)(color & 0xFF) / 255.0f;
            Minecraft.getMinecraft().getTextureManager().bindTexture(weeds);
            GlStateManager.disableCull();
            GlStateManager.pushMatrix();
            GlStateManager.translate(x + 0.5, y + 0.5, z + 0.5);
            for (EnumFacing f : EnumFacing.HORIZONTALS) {
                GlStateManager.pushMatrix();
                GlStateManager.rotate(f.getHorizontalAngle(), 0.0f, 1.0f, 0.0f);
                GlStateManager.pushMatrix();
                GlStateManager.translate(-0.5, -0.5, -0.5);
                BufferBuilder builder = Tessellator.getInstance().getBuffer();
                builder.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR_NORMAL);
                builder.pos(0.0, 0.0, 0.0).tex(1.0, 1.0).color(r, g, b, 1.0f).normal(f.getXOffset(), f.getYOffset(), f.getZOffset()).endVertex();
                builder.pos(0.0, 1.0, 0.0).tex(1.0, 0.0).color(r, g, b, 1.0f).normal(f.getXOffset(), f.getYOffset(), f.getZOffset()).endVertex();
                builder.pos(1.0, 1.0, 0.0).tex(0.0, 0.0).color(r, g, b, 1.0f).normal(f.getXOffset(), f.getYOffset(), f.getZOffset()).endVertex();
                builder.pos(1.0, 0.0, 0.0).tex(0.0, 1.0).color(r, g, b, 1.0f).normal(f.getXOffset(), f.getYOffset(), f.getZOffset()).endVertex();
                Tessellator.getInstance().draw();
                GlStateManager.popMatrix();
                GlStateManager.popMatrix();
            }
            GlStateManager.popMatrix();
            GlStateManager.enableCull();
        }
    }
}

