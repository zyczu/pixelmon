/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities.shrines;

import com.pixelmongenerations.client.particle.ParticleRedstone;
import com.pixelmongenerations.client.render.tileEntities.GenericSmdRenderer;
import com.pixelmongenerations.common.block.tileEntities.IShrineBlock;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.SpawnColors;
import java.awt.Color;
import java.util.Collections;
import java.util.List;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.Vec3d;

public class RenderTileEntityShrine<T extends TileEntity>
extends GenericSmdRenderer<T> {
    private static List<Color> DEFAULT_COLOR = Collections.singletonList(SpawnColors.GOLD);

    public RenderTileEntityShrine(String pqcPath, String texture) {
        super(pqcPath, texture);
        this.correctionAngles = -90;
    }

    @Override
    public void renderTileEntity(T shrine, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        super.renderTileEntity(shrine, state, x, y, z, partialTicks, destroyStage);
        if (shrine instanceof IShrineBlock && ((IShrineBlock)shrine).getTick() > 0) {
            this.processAnimation((IShrineBlock)shrine, ((IShrineBlock)shrine).getActiveGroup() != null ? ((IShrineBlock)shrine).getActiveGroup().getSpecies() : null);
        }
    }

    public void processAnimation(IShrineBlock shrineBlock, EnumSpecies species) {
        Vec3d center = new Vec3d(shrineBlock.getSpawnPos()).add(0.5, 0.5, 0.5);
        double theta = (double)shrineBlock.getTick() / (double)shrineBlock.maxTick();
        double t = (1.0 - theta) * 2.0 * Math.PI;
        double radius = 1.0;
        double dx = 0.5 * t * radius * Math.cos(4.0 * t);
        double dy = 0.5 * t * radius * Math.sin(4.0 * t);
        double dz = theta * 2.0 * Math.PI;
        List<Color> color = shrineBlock.getColors(species);
        for (int i = 0; i < 6; ++i) {
            Color c = color.get(i % color.size());
            double r = (double)i / 6.0 * 2.0 * Math.PI;
            Minecraft.getMinecraft().effectRenderer.addEffect(new ParticleRedstone(this.getWorld(), center.x + RenderTileEntityShrine.rotateX(r, dx, dy), center.y + dz, center.z + RenderTileEntityShrine.rotateY(r, dx, dy), (float)c.getRed() / 255.0f, (float)c.getGreen() / 255.0f, (float)c.getBlue() / 255.0f));
        }
    }

    public static double rotateX(double r, double x, double y) {
        return Math.cos(r) * x - Math.sin(r) * y;
    }

    public static double rotateY(double r, double x, double y) {
        return Math.sin(r) * x + Math.cos(r) * y;
    }
}

