/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.mojang.authlib.minecraft.MinecraftProfileTexture
 *  com.mojang.authlib.minecraft.MinecraftProfileTexture$Type
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.SharedModels;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityTradeMachine;
import com.pixelmongenerations.common.pokedex.Pokedex;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.DefaultPlayerSkin;
import net.minecraft.client.resources.SkinManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

public class RenderTileEntityTradingMachine
extends TileEntityRenderer<TileEntityTradeMachine> {
    private static final ResourceLocation bracket = new ResourceLocation("pixelmon", "textures/blocks/trademachine/Bracket.png");
    private static final ResourceLocation placeholder = new ResourceLocation("pixelmon", "textures/blocks/placeholder.png");
    private static final BlockModelHolder<GenericSmdModel> model = new BlockModelHolder("blocks/trade_machine/trader.pqc");
    private static final BlockModelHolder<GenericSmdModel> arrows = new BlockModelHolder("blocks/trade_machine/arrows.pqc");
    private static final BlockModelHolder<GenericSmdModel> poke1Poke = new BlockModelHolder("blocks/trade_machine/poke1poke.pqc");
    private static final BlockModelHolder<GenericSmdModel> poke1Bracket = new BlockModelHolder("blocks/trade_machine/poke1bracket.pqc");
    private static final BlockModelHolder<GenericSmdModel> poke1Player = new BlockModelHolder("blocks/trade_machine/poke1player.pqc");
    private static final BlockModelHolder<GenericSmdModel> poke1HeldItem = new BlockModelHolder("blocks/trade_machine/poke1helditem.pqc");
    private static final BlockModelHolder<GenericSmdModel> poke1Ready = new BlockModelHolder("blocks/trade_machine/poke1ready.pqc");
    private static final BlockModelHolder<GenericSmdModel> poke2Poke = new BlockModelHolder("blocks/trade_machine/poke2poke.pqc");
    private static final BlockModelHolder<GenericSmdModel> poke2Bracket = new BlockModelHolder("blocks/trade_machine/poke2bracket.pqc");
    private static final BlockModelHolder<GenericSmdModel> poke2Player = new BlockModelHolder("blocks/trade_machine/poke2player.pqc");
    private static final BlockModelHolder<GenericSmdModel> poke2HeldItem = new BlockModelHolder("blocks/trade_machine/poke2helditem.pqc");
    private static final BlockModelHolder<GenericSmdModel> poke2Ready = new BlockModelHolder("blocks/trade_machine/poke2ready.pqc");

    public RenderTileEntityTradingMachine() {
        this.correctionAngles = 180;
    }

    @Override
    public void renderTileEntity(TileEntityTradeMachine tradeMachine, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        this.bindTexture(tradeMachine.getTexture());
        model.render();
        this.bindTexture(bracket);
        arrows.render();
        poke1Bracket.render();
        poke2Bracket.render();
        SkinManager skinManager = Minecraft.getMinecraft().getSkinManager();
        if (tradeMachine.user1 != null && !tradeMachine.user1.isEmpty() || tradeMachine.user2 != null && !tradeMachine.user2.isEmpty()) {
            String[] users = new String[]{tradeMachine.user1, tradeMachine.user2};
            for (int i = 0; i < 2; ++i) {
                if (users[i] != null && !users[i].isEmpty()) {
                    String username = users[i];
                    EntityPlayer player = this.getWorld().getPlayerEntityByName(username);
                    if (player != null) {
                        ResourceLocation resourcelocation;
                        Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> map = skinManager.loadSkinFromCache(player.getGameProfile());
                        if (map.containsKey((Object)MinecraftProfileTexture.Type.SKIN)) {
                            resourcelocation = skinManager.loadSkin(map.get((Object)MinecraftProfileTexture.Type.SKIN), MinecraftProfileTexture.Type.SKIN);
                        } else {
                            UUID uuid = EntityPlayer.getUUID(player.getGameProfile());
                            resourcelocation = DefaultPlayerSkin.getDefaultSkin(uuid);
                        }
                        this.bindTexture(resourcelocation);
                    } else {
                        this.bindTexture(placeholder);
                    }
                    (i == 0 ? poke1Player : poke2Player).render();
                } else {
                    this.bindTexture(placeholder);
                }
                (i == 0 ? poke1Player : poke2Player).render();
            }
            NBTTagCompound[] pokemon = new NBTTagCompound[]{tradeMachine.poke1, tradeMachine.poke2};
            for (int i = 0; i < 2; ++i) {
                if (pokemon[i] == null) continue;
                String name = pokemon[i].getString("Name");
                boolean isEgg = pokemon[i].getBoolean("isEgg");
                int eggCycles = pokemon[i].getInteger("eggCycles");
                int variant = pokemon[i].getInteger("Variant");
                boolean isShiny = pokemon[i].getBoolean("IsShiny");
                int pokeball = pokemon[i].getInteger("CaughtBall");
                int number = Pokedex.nameToID(name);
                EnumPokeball pokeballs = EnumPokeball.getFromIndex(pokeball);
                this.bindTexture(pokeballs.getTextureLocation());
                GlStateManager.popMatrix();
                GlStateManager.pushMatrix();
                int rotation = this.getRotation(state);
                if (i == 0) {
                    if (rotation == 270) {
                        GlStateManager.translate(x + 0.5, y + 0.93, z + 0.4);
                    } else if (rotation == 180) {
                        GlStateManager.translate(x + 0.6, y + 0.93, z + 0.5);
                    } else if (rotation == 90) {
                        GlStateManager.translate(x + 0.5, y + 0.93, z + 0.6);
                    } else {
                        GlStateManager.translate(x + 0.4, y + 0.93, z + 0.5);
                    }
                } else if (rotation == 270) {
                    GlStateManager.translate(x + 0.5, y + 0.93, z + 2.2 + 0.4);
                } else if (rotation == 180) {
                    GlStateManager.translate(x - 2.2 + 0.6, y + 0.93, z + 0.5);
                } else if (rotation == 90) {
                    GlStateManager.translate(x + 0.5, y + 0.93, z - 2.2 + 0.6);
                } else {
                    GlStateManager.translate(x + 2.2 + 0.4, y + 0.93, z + 0.5);
                }
                GlStateManager.rotate(this.getRotation(state), 0.0f, 1.0f, 0.0f);
                SharedModels.getPokeballModel(pokeballs).render(0.0022f);
                GlStateManager.popMatrix();
                GlStateManager.pushMatrix();
                GlStateManager.translate((float)x + 0.5f, (float)y + this.yOffset, (float)z + 0.5f);
                GlStateManager.rotate(this.getRotation(state) + this.correctionAngles, 0.0f, 1.0f, 0.0f);
                GlStateManager.rotate(180.0f, 0.0f, 0.0f, 1.0f);
                if (pokemon[i].hasKey("HeldItemStack")) {
                    ItemStack heldItem = new ItemStack(pokemon[i].getCompoundTag("HeldItemStack"));
                    if (heldItem.isEmpty()) {
                        this.bindTexture(placeholder);
                        (i == 0 ? poke1HeldItem : poke2HeldItem).render();
                    } else {
                        this.bindTexture(new ResourceLocation("pixelmon", "textures/helditem.png"));
                        (i == 0 ? poke1HeldItem : poke2HeldItem).render();
                    }
                } else {
                    this.bindTexture(placeholder);
                    (i == 0 ? poke1HeldItem : poke2HeldItem).render();
                }
                this.bindTexture(new ResourceLocation("pixelmon", this.getSpriteFromID(number, name, isShiny, isEgg, eggCycles, variant)));
                (i == 0 ? poke1Poke : poke2Poke).render();
            }
            if (tradeMachine.ready1) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/trademachine/Ready.png"));
                poke1Ready.render();
            } else {
                this.bindTexture(placeholder);
                poke1Ready.render();
            }
            if (tradeMachine.ready2) {
                this.bindTexture(new ResourceLocation("pixelmon", "textures/blocks/trademachine/Ready.png"));
                poke2Ready.render();
            } else {
                this.bindTexture(placeholder);
                poke2Ready.render();
            }
        }
    }

    private String getSpriteFromID(int nationalPokedexNumber, String pokemonName, boolean isShiny, boolean isEgg, int eggCycles, int variant) {
        String basePath = "textures/sprites/pokemon/";
        if (isShiny) {
            basePath = "textures/sprites/shinypokemon/";
        } else if (isEgg) {
            basePath = "textures/sprites/eggs/";
        }
        if (isEgg) {
            String eggType = "egg";
            if (nationalPokedexNumber == 175) {
                eggType = "togepi";
            } else if (nationalPokedexNumber == 490) {
                eggType = "manaphy";
            }
            if (eggCycles > 10) {
                return basePath + eggType + "1.png";
            }
            if (eggCycles > 5) {
                return basePath + eggType + "2.png";
            }
            return basePath + eggType + "3.png";
        }
        Optional<EnumSpecies> optional = EnumSpecies.getFromName(pokemonName);
        if (optional.isPresent()) {
            EnumSpecies pokemon = optional.get();
            return basePath + pokemon.getNationalPokedexNumber() + pokemon.getFormEnum(variant).getSpriteSuffix() + ".png";
        }
        return basePath + String.format("%03d", nationalPokedexNumber) + ".png";
    }
}

