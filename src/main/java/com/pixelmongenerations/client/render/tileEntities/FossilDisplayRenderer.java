/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityFossilDisplay;
import com.pixelmongenerations.common.item.ItemFossil;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;

public class FossilDisplayRenderer
extends TileEntityRenderer<TileEntityFossilDisplay> {
    private static final BlockModelHolder<GenericSmdModel> fossilDisplayCase = new BlockModelHolder("pixelutilities/fossil_display/fossil_display.pqc");
    private static final GenericSmdModel fossilDisplayGlass = new GenericSmdModel("models/pixelutilities/fossil_display", "fossil_display_glass.pqc");

    public FossilDisplayRenderer() {
        this.correctionAngles = 180;
        FossilDisplayRenderer.fossilDisplayGlass.modelRenderer.setTransparent(0.5f);
    }

    @Override
    public void renderTileEntity(TileEntityFossilDisplay fossilDisplay, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        if (fossilDisplay.renderPass == 1) {
            this.bindTexture(fossilDisplay.getTexture());
            fossilDisplayGlass.setFrame(fossilDisplay.frame);
            fossilDisplayGlass.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 1.0f);
        } else {
            ItemFossil fossil = fossilDisplay.getItemInDisplay();
            this.bindTexture(fossilDisplay.getTexture());
            fossilDisplayCase.render();
            if (fossil != null) {
                GlStateManager.translate(0.0, -0.85, 0.0);
                this.bindTexture(fossil.getFossil().getTexture());
                ((BlockModelHolder)fossil.getFossil().getModel()).render();
            }
        }
    }
}

