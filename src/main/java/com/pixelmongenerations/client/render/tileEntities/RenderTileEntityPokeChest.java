/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.tileEntities.SharedModels;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.enums.EnumPokeChestType;
import com.pixelmongenerations.common.block.enums.EnumPokechestVisibility;
import com.pixelmongenerations.common.block.loot.BlockPokeChest;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPokeChest;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderTileEntityPokeChest
extends TileEntityRenderer<TileEntityPokeChest> {
    private static final ResourceLocation pokeball = new ResourceLocation("pixelmon", "textures/pokeballs/pokeball.png");
    private static final ResourceLocation ultraball = new ResourceLocation("pixelmon", "textures/pokeballs/ultraball.png");
    private static final ResourceLocation masterball = new ResourceLocation("pixelmon", "textures/pokeballs/masterball.png");
    private static final ResourceLocation beastball = new ResourceLocation("pixelmon", "textures/pokeballs/beast_ball_loot.png");
    private static final ResourceLocation gsball = new ResourceLocation("pixelmon", "textures/pokeballs/gsball.png");
    private static final ResourceLocation pokestop = new ResourceLocation("pixelmon", "textures/blocks/pokestop.png");
    public static float frame;

    public RenderTileEntityPokeChest() {
        this.scale = 0.1f;
        this.yOffset = 0.165f;
        this.flip = false;
    }

    @Override
    public void renderTileEntity(TileEntityPokeChest chest, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        if (chest.getVisibility() == EnumPokechestVisibility.Hidden) {
            return;
        }
        if (chest.getType() == EnumPokeChestType.MASTERBALL) {
            this.bindTexture(masterball);
        } else if (chest.getType() == EnumPokeChestType.ULTRABALL) {
            this.bindTexture(ultraball);
        } else if (chest.getType() == EnumPokeChestType.SPECIAL) {
            this.bindTexture(gsball);
        } else if (chest.getType() == EnumPokeChestType.BEASTBALL) {
            this.bindTexture(beastball);
        } else if (chest.getType() == EnumPokeChestType.POKESTOP) {
            this.bindTexture(pokestop);
        } else {
            this.bindTexture(pokeball);
        }
        GlStateManager.rotate(0.0f, 0.0f, 0.0f, 1.0f);
        if (chest.getType() == EnumPokeChestType.BEASTBALL) {
            ((GenericSmdModel)SharedModels.pokechestBeast.getModel()).renderModel(0.0625f);
            return;
        }
        if (chest.getType() == EnumPokeChestType.POKESTOP) {
            GL11.glTranslatef((float)0.0f, (float)-1.6f, (float)0.2f);
            GL11.glRotatef((float)90.0f, (float)1.0f, (float)0.0f, (float)0.0f);
            GenericSmdModel model = (GenericSmdModel)SharedModels.pokeStop.getModel();
            chest.frame = (int)frame;
            model.setFrame(chest.frame);
            model.renderModel(44.0f);
            return;
        }
        ((GenericSmdModel)(chest.getType() == EnumPokeChestType.MASTERBALL ? SharedModels.pokechestMaster : SharedModels.pokechestBase).getModel()).renderModel(0.0625f);
    }

    @Override
    protected int getRotation(IBlockState state) {
        if (state.getBlock() instanceof BlockPokeChest) {
            EnumFacing facing = state.getValue(BlockPokeChest.FACING);
            if (facing == EnumFacing.WEST) {
                return 90;
            }
            if (facing == EnumFacing.SOUTH) {
                return 180;
            }
            if (facing == EnumFacing.EAST) {
                return 270;
            }
            return 0;
        }
        return 0;
    }
}

