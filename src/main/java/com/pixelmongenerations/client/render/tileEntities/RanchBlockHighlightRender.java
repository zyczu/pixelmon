/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.ranch.BlockRanchBlock;
import com.pixelmongenerations.common.block.ranch.RanchBounds;
import com.pixelmongenerations.common.block.tileEntities.TileEntityRanchBlock;
import com.pixelmongenerations.core.util.Bounds;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class RanchBlockHighlightRender {
    private static ResourceLocation ranchOverlay = new ResourceLocation("pixelmon:textures/blocks/ranchOverlay.png");
    private TileEntityRanchBlock lastBlock;
    private int tick = 0;

    @SubscribeEvent
    public void onHighlight(DrawBlockHighlightEvent event) {
        if (event.getTarget().typeOfHit == RayTraceResult.Type.BLOCK) {
            World world = event.getPlayer().world;
            Block block = world.getBlockState(event.getTarget().getBlockPos()).getBlock();
            if (block instanceof BlockRanchBlock) {
                try {
                    IBlockState state = world.getBlockState(event.getTarget().getBlockPos());
                    MultiBlock mb = (MultiBlock)state.getBlock();
                    BlockPos loc = mb.findBaseBlock(world, new BlockPos.MutableBlockPos(event.getTarget().getBlockPos()), state);
                    TileEntityRanchBlock rb = (TileEntityRanchBlock)world.getTileEntity(loc);
                    if (event.getPlayer().getUniqueID().equals(rb.getOwnerUUID()) || event.getPlayer().getDisplayNameString().equals(rb.getPlayerName())) {
                        if (rb == this.lastBlock) {
                            if (this.tick >= 40) {
                                this.renderBounds(rb, event.getPlayer(), world, event.getPartialTicks());
                            } else {
                                ++this.tick;
                            }
                        } else {
                            ++this.tick;
                        }
                    } else {
                        this.tick = 0;
                    }
                    this.lastBlock = rb;
                }
                catch (Exception exception) {}
            } else {
                this.tick = 0;
            }
        } else {
            this.tick = 0;
        }
    }

    private void renderBounds(TileEntityRanchBlock te, EntityPlayer player, World world, float partialTicks) {
        Bounds b = te.getBounds();
        RanchBounds bounds = null;
        if (b instanceof RanchBounds) {
            bounds = (RanchBounds)b;
        }
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder vertexBuffer = tessellator.getBuffer();
        vertexBuffer.begin(7, DefaultVertexFormats.POSITION_TEX);
        GlStateManager.enableBlend();
        GlStateManager.depthMask(true);
        GlStateManager.enableDepth();
        GlStateManager.blendFunc(770, 771);
        Minecraft.getMinecraft().renderEngine.bindTexture(ranchOverlay);
        for (int x = b.left; x <= b.right; ++x) {
            for (int z = b.bottom; z <= b.top; ++z) {
                if (x == te.getPos().getX() && z == te.getPos().getZ()) continue;
                BlockPos xz = new BlockPos(x, 0, z);
                BlockPos pos = bounds == null ? world.getTopSolidOrLiquidBlock(xz) : bounds.getTopBlock(world, xz);
                Block block = world.getBlockState(pos).getBlock();
                float f1 = 0.002f;
                double d0 = player.lastTickPosX + (player.posX - player.lastTickPosX) * (double)partialTicks;
                double d1 = player.lastTickPosY + (player.posY - player.lastTickPosY) * (double)partialTicks;
                double d2 = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * (double)partialTicks;
                this.drawBlockOverlay(tessellator, block.getSelectedBoundingBox(world.getBlockState(pos), world, pos).expand(f1, f1, f1).offset(-d0, -d1, -d2));
            }
        }
        tessellator.draw();
        GlStateManager.disableBlend();
    }

    private void drawBlockOverlay(Tessellator tessellator, AxisAlignedBB box) {
        tessellator.getBuffer().pos(box.minX + 0.15, box.minY + (double)0.05f, box.maxZ - 0.15).tex(0.0, 1.0).endVertex();
        tessellator.getBuffer().pos(box.maxX - 0.15, box.minY + (double)0.05f, box.maxZ - 0.15).tex(1.0, 1.0).endVertex();
        tessellator.getBuffer().pos(box.maxX - 0.15, box.minY + (double)0.05f, box.minZ + 0.15).tex(1.0, 0.0).endVertex();
        tessellator.getBuffer().pos(box.minX + 0.15, box.minY + (double)0.05f, box.minZ + 0.15).tex(0.0, 0.0).endVertex();
    }
}

