/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityDisplayball;
import net.minecraft.block.state.IBlockState;

public class RenderTileEntityDisplayball
extends TileEntityRenderer<TileEntityDisplayball> {
    public RenderTileEntityDisplayball() {
        this.scale = 1.0f;
        this.disableCulling = true;
    }

    @Override
    public void renderTileEntity(TileEntityDisplayball te, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        this.bindTexture(te.getTexture());
        te.getModel().render();
    }
}

