/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.models.blocks.ModelBrokenCloningMachine;
import com.pixelmongenerations.client.models.blocks.ModelCloningMachine;
import com.pixelmongenerations.client.models.blocks.ModelEntityBlock;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCloningMachine;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumGrowth;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.opengl.GL11;

public class RenderTileEntityCloningMachine
extends TileEntityRenderer<TileEntityCloningMachine> {
    private static final ResourceLocation cloningMachineTex = new ResourceLocation("pixelmon:textures/blocks/cloner.png");
    private static final ResourceLocation cloningMachineBlocks = new ResourceLocation("pixelmon:textures/gui/clonerBlocks.png");
    private static final BlockModelHolder<ModelCloningMachine> model = new BlockModelHolder<ModelCloningMachine>(ModelCloningMachine.class);
    private static final BlockModelHolder<ModelBrokenCloningMachine> brokenModel = new BlockModelHolder<ModelBrokenCloningMachine>(ModelBrokenCloningMachine.class);
    private EntityPixelmon pokemon;

    public RenderTileEntityCloningMachine() {
        this.flip = false;
    }

    @Override
    public void renderTileEntity(TileEntityCloningMachine machine, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        this.bindTexture(cloningMachineTex);
        ((ModelEntityBlock)(machine.isBroken ? brokenModel : model).getModel()).renderTileEntity(machine, 0.0625f);
        GlStateManager.popMatrix();
        EnumFacing facing = EnumFacing.NORTH;
        int rotation = super.getRotation(state);
        if (state.getBlock() instanceof MultiBlock) {
            facing = state.getValue(MultiBlock.FACING);
        }
        GlStateManager.pushMatrix();
        if (machine.hasMew && !machine.isBroken && !machine.isFinished) {
            if (this.pokemon == null) {
                this.pokemon = (EntityPixelmon)PixelmonEntityList.createEntityByName("Mew", machine.getWorld());
                this.pokemon.setGrowth(EnumGrowth.Runt);
                this.pokemon.getLvl().setLevel(1);
                this.pokemon.initAnimation();
            }
            GlStateManager.pushMatrix();
            if (this.pokemon != null) {
                GlStateManager.translate((float)x + 0.5f, (float)y + 1.0f, (float)z + 0.5f);
                this.pokemon.setWorld(machine.getWorld());
                GlStateManager.rotate(rotation, 0.0f, 1.0f, 0.0f);
                this.pokemon.setLocationAndAngles(x, y, z, 0.0f, 0.0f);
                Minecraft.getMinecraft().getRenderManager().renderEntity(this.pokemon, 0.0, 0.0, 0.0, 0.0f, partialTicks, false);
            }
            GlStateManager.popMatrix();
        }
        GlStateManager.pushMatrix();
        GlStateManager.translate((float)x + 0.5f, (float)y, (float)z + 0.5f);
        GlStateManager.rotate(rotation, 0.0f, 1.0f, 0.0f);
        GlStateManager.rotate(180.0f, 1.0f, 0.0f, 0.0f);
        GlStateManager.scale(1.0f, -1.0f, -1.0f);
        GlStateManager.enableBlend();
        GlStateManager.depthMask(false);
        GlStateManager.blendFunc(770, 771);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.bindTexture(cloningMachineTex);
        if (machine.isBroken) {
            ((ModelBrokenCloningMachine)brokenModel.getModel()).renderGlass();
        } else {
            ((ModelCloningMachine)model.getModel()).renderGlass();
        }
        GlStateManager.depthMask(true);
        GlStateManager.disableBlend();
        GlStateManager.popMatrix();
        if (machine.isBroken) {
            this.renderLeftDisplay(I18n.translateToLocal("pixelmon.blocks.cloningmachine.error"), facing, x, y, z, false, -1);
        } else {
            if (machine.isFinished) {
                this.renderLeftDisplay(I18n.translateToLocal("pixelmon.blocks.cloningmachine.finished"), facing, x, y, z, false, -1);
            } else if (machine.hasMew) {
                this.renderLeftDisplay(I18n.translateToLocal("pixelmon.blocks.cloningmachine.scanning"), facing, x, y, z, false, -1);
            } else {
                this.renderLeftDisplay(I18n.translateToLocal("pixelmon.blocks.cloningmachine.mew"), facing, x, y, z, false, -1);
            }
            if (machine.hasMew) {
                this.renderRightDisplay(machine, I18n.translateToLocal("pixelmon.blocks.cloningmachine.catalyst"), facing, x, y, z, false, -1);
            }
            if (!machine.pokemonName.equals("")) {
                if (machine.pixelmon == null) {
                    machine.pixelmon = (EntityPixelmon)PixelmonEntityList.createEntityByName(machine.pokemonName, machine.getWorld());
                    machine.pixelmon.setGrowth(EnumGrowth.Runt);
                    machine.pixelmon.setShiny(machine.isShiny);
                    machine.pixelmon.getLvl().setLevel(1);
                    machine.pixelmon.initAnimation();
                }
                GlStateManager.pushMatrix();
                GlStateManager.translate((float)x + 0.5f + machine.xboost, (float)y + 0.8f, (float)z + 0.5f + machine.zboost);
                machine.pixelmon.setWorld(machine.getWorld());
                GlStateManager.rotate(rotation, 0.0f, 1.0f, 0.0f);
                GlStateManager.scale(0.0f + (float)machine.pokemonProgress / 200.0f, 0.0f + (float)machine.pokemonProgress / 200.0f, 0.0f + (float)machine.pokemonProgress / 200.0f);
                Minecraft.getMinecraft().getRenderManager().renderEntity(machine.pixelmon, 0.0, 0.0, 0.0, 0.0f, 0.0f, false);
                GlStateManager.popMatrix();
            }
        }
    }

    private void renderLeftDisplay(String par2Str, EnumFacing facing, double par3, double par5, double par7, boolean par10, int par11) {
        int j = 0;
        if (facing == EnumFacing.EAST) {
            j = 270;
        } else if (facing == EnumFacing.WEST) {
            j = 90;
        } else if (facing == EnumFacing.SOUTH) {
            j = 180;
        }
        FontRenderer var12 = this.getFontRenderer();
        float var13 = 1.6f;
        float var14 = 0.011666667f * var13;
        GlStateManager.pushMatrix();
        if (facing == EnumFacing.NORTH) {
            GlStateManager.translate((float)par3 + 0.5f, (float)par5 + 0.9f, (float)par7 + 1.22f);
        } else if (facing == EnumFacing.WEST) {
            GlStateManager.translate((float)par3 + 1.23f, (float)par5 + 0.9f, (float)par7 + 0.5f);
        } else if (facing == EnumFacing.SOUTH) {
            GlStateManager.translate((float)par3 + 0.5f, (float)par5 + 0.9f, (float)par7 - 0.23f);
        } else {
            GlStateManager.translate((float)par3 - 0.23f, (float)par5 + 0.9f, (float)par7 + 0.51f);
        }
        GlStateManager.rotate(j + 180, 0.0f, 1.0f, 0.0f);
        GL11.glNormal3f((float)0.0f, (float)1.0f, (float)0.0f);
        if (!par10) {
            GlStateManager.scale(-var14, -var14, var14);
        } else {
            GlStateManager.scale(-var14 + 0.012f, -var14 + 0.012f, var14 + 0.012f);
        }
        GlStateManager.disableLighting();
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(770, 771);
        int var16 = 0;
        GlStateManager.scale(0.5f, 0.5f, 0.5f);
        GlStateManager.disableTexture2D();
        GlStateManager.enableTexture2D();
        var12.drawString(par2Str, -var12.getStringWidth(par2Str) / 2, var16, par11);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.enableLighting();
        GlStateManager.disableBlend();
        GlStateManager.popMatrix();
    }

    private void renderRightDisplay(TileEntityCloningMachine tile, String par2Str, EnumFacing facing, double par3, double par5, double par7, boolean par10, int par11) {
        int j = 0;
        if (facing == EnumFacing.EAST) {
            j = 270;
        } else if (facing == EnumFacing.NORTH) {
            j = 0;
        } else if (facing == EnumFacing.WEST) {
            j = 90;
        } else if (facing == EnumFacing.SOUTH) {
            j = 180;
        }
        FontRenderer var12 = this.getFontRenderer();
        float var13 = 1.6f;
        float var14 = 0.011666667f * var13;
        GlStateManager.pushMatrix();
        if (facing == EnumFacing.NORTH) {
            GlStateManager.translate((float)par3 + 3.87f, (float)par5 + 0.9f, (float)par7 + 1.22f);
        } else if (facing == EnumFacing.WEST) {
            GlStateManager.translate((float)par3 + 1.23f, (float)par5 + 0.9f, (float)par7 - 2.87f);
        } else if (facing == EnumFacing.SOUTH) {
            GlStateManager.translate((float)par3 - 2.87f, (float)par5 + 0.9f, (float)par7 - 0.23f);
        } else {
            GlStateManager.translate((float)par3 - 0.23f, (float)par5 + 0.9f, (float)par7 + 3.87f);
        }
        GlStateManager.rotate(j + 180, 0.0f, 1.0f, 0.0f);
        GL11.glNormal3f((float)0.0f, (float)1.0f, (float)0.0f);
        if (!par10) {
            GlStateManager.scale(-var14, -var14, var14);
        } else {
            GlStateManager.scale(-var14 + 0.012f, -var14 + 0.012f, var14 + 0.012f);
        }
        GlStateManager.disableLighting();
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(770, 771);
        int var16 = 0;
        GlStateManager.scale(0.3f, 0.3f, 0.3f);
        GlStateManager.disableTexture2D();
        var12.getStringWidth(par2Str);
        GlStateManager.enableTexture2D();
        if (tile.isFinished) {
            par2Str = I18n.translateToLocal("pixelmon.blocks.cloningmachine.retrieve");
        } else if (tile.processingClone || tile.growingPokemon) {
            par2Str = I18n.translateToLocal("pixelmon.blocks.cloningmachine.processing");
        }
        var12.drawString(par2Str, -var12.getStringWidth(par2Str) / 2, var16, par11);
        if (!(tile.processingClone || tile.growingPokemon || tile.isFinished)) {
            this.bindTexture(cloningMachineBlocks);
            GuiHelper.drawImageQuad(-30.0, 10.0, 60.0, 20.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
            par2Str = I18n.translateToLocal("pixelmon.blocks.cloningmachine.boost") + " " + tile.boostCount + "/3";
            var12.drawString(par2Str, -var12.getStringWidth(par2Str) / 2, var16 + 40, par11);
            Tessellator tessellator = Tessellator.getInstance();
            BufferBuilder worldRenderer = tessellator.getBuffer();
            int x = -30;
            int w = 60;
            int y = 35;
            int h = 17;
            int zLevel = 0;
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GlStateManager.disableTexture2D();
            float boostPercent = (float)tile.boostLevel / 40.0f;
            worldRenderer.begin(7, DefaultVertexFormats.POSITION_COLOR);
            worldRenderer.pos(x, y + h, (double)zLevel + 0.01).color(0.2f + 0.3f * (1.0f - boostPercent), 0.2f + 0.3f * boostPercent, 0.2f, 1.0f).endVertex();
            worldRenderer.pos(x + w, y + h, (double)zLevel + 0.01).color(0.2f + 0.3f * (1.0f - boostPercent), 0.2f + 0.3f * boostPercent, 0.2f, 1.0f).endVertex();
            worldRenderer.pos(x + w, y, (double)zLevel + 0.01).color(0.2f + 0.3f * (1.0f - boostPercent), 0.2f + 0.3f * boostPercent, 0.2f, 1.0f).endVertex();
            worldRenderer.pos(x, y, (double)zLevel + 0.01).color(0.2f + 0.3f * (1.0f - boostPercent), 0.2f + 0.3f * boostPercent, 0.2f, 1.0f).endVertex();
            tessellator.draw();
            GlStateManager.enableTexture2D();
        }
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.enableLighting();
        GlStateManager.disableBlend();
        GlStateManager.popMatrix();
    }
}

