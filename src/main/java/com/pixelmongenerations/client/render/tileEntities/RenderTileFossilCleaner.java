/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityFossilCleaner;
import com.pixelmongenerations.common.item.ItemCoveredFossil;
import com.pixelmongenerations.common.item.ItemFossil;
import com.pixelmongenerations.core.Pixelmon;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

public class RenderTileFossilCleaner
extends TileEntityRenderer<TileEntityFossilCleaner> {
    private static final ResourceLocation texture = new ResourceLocation("pixelmon", "textures/blocks/fossilCleaner.png");
    private static final BlockModelHolder<GenericSmdModel> fossilCleaner = new BlockModelHolder("blocks/fossil_cleaner/fossil_cleaner.pqc");
    private static final BlockModelHolder<GenericSmdModel> fossilCleanerArm = new BlockModelHolder("blocks/fossil_cleaner/fossil_cleaner_arm.pqc");
    private static final GenericSmdModel fossilCleanerGlass = new GenericSmdModel("models/blocks/fossil_cleaner", "fossil_cleaner_glass.pqc");
    private static final BlockModelHolder<GenericSmdModel> coveredHelixFossil = new BlockModelHolder("fossils/covered/covered_helix.pqc");
    private static final BlockModelHolder<GenericSmdModel> coveredDomeFossil = new BlockModelHolder("fossils/covered/covered_dome.pqc");
    private static final BlockModelHolder<GenericSmdModel> coveredOldAmber = new BlockModelHolder("fossils/covered/covered_oldamber.pqc");
    private static final BlockModelHolder<GenericSmdModel> coveredClawFossil = new BlockModelHolder("fossils/covered/covered_claw.pqc");
    private static final BlockModelHolder<GenericSmdModel> coveredSkullFossil = new BlockModelHolder("fossils/covered/covered_skull.pqc");
    private static final BlockModelHolder<GenericSmdModel> coveredArmorFossil = new BlockModelHolder("fossils/covered/covered_armor.pqc");
    private static final BlockModelHolder<GenericSmdModel> coveredCoverFossil = new BlockModelHolder("fossils/covered/covered_cover.pqc");
    private static final BlockModelHolder<GenericSmdModel> coveredPlumeFossil = new BlockModelHolder("fossils/covered/covered_plume.pqc");
    private static final BlockModelHolder<GenericSmdModel> coveredJawFossil = new BlockModelHolder("fossils/covered/covered_jaw.pqc");
    private static final BlockModelHolder<GenericSmdModel> coveredSailFossil = new BlockModelHolder("fossils/covered/covered_sail.pqc");
    private static final BlockModelHolder<GenericSmdModel> coveredBirdFossil = new BlockModelHolder("fossils/covered/covered_bird.pqc");
    private static final BlockModelHolder<GenericSmdModel> coveredFishFossil = new BlockModelHolder("fossils/covered/covered_fish.pqc");
    private static final BlockModelHolder<GenericSmdModel> coveredDinoFossil = new BlockModelHolder("fossils/covered/covered_dino.pqc");
    private static final BlockModelHolder<GenericSmdModel> coveredDrakeFossil = new BlockModelHolder("fossils/covered/covered_drake.pqc");

    public RenderTileFossilCleaner() {
        this.correctionAngles = 180;
        RenderTileFossilCleaner.fossilCleanerGlass.modelRenderer.setTransparent(0.5f);
    }

    @Override
    public void renderTileEntity(TileEntityFossilCleaner cleaner, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        if (cleaner.renderPass == 1) {
            this.bindTexture(texture);
            fossilCleanerGlass.render(null, 0.0f, 0.0f, -0.1f, 0.0f, 0.0f, 1.0f);
            return;
        }
        int frame = cleaner.timer >= 360 ? 0 : cleaner.timer * 2;
        this.bindTexture(texture);
        ((GenericSmdModel)fossilCleaner.getModel()).setFrame(frame);
        fossilCleaner.render();
        ((GenericSmdModel)fossilCleanerArm.getModel()).setFrame(frame);
        fossilCleanerArm.render();
        Item itemInside = cleaner.getItemInCleaner();
        if (itemInside != null) {
            BlockModelHolder<GenericSmdModel> fossilModel = null;
            if (itemInside instanceof ItemCoveredFossil) {
                ItemCoveredFossil coveredFossil = (ItemCoveredFossil)itemInside;
                switch (coveredFossil.cleanedFossil.getFossil()) {
                    case HELIX: 
                    case ROOT: {
                        this.bindTexture(new ResourceLocation("pixelmon", "textures/fossils/covered/covered_helix_fossilmodel.png"));
                        fossilModel = coveredHelixFossil;
                        break;
                    }
                    case DOME: {
                        this.bindTexture(new ResourceLocation("pixelmon", "textures/fossils/covered/covered_dome_fossilmodel.png"));
                        fossilModel = coveredDomeFossil;
                        break;
                    }
                    case OLD_AMBER: {
                        this.bindTexture(new ResourceLocation("pixelmon", "textures/fossils/covered/covered_old_amber_fossilmodel.png"));
                        fossilModel = coveredOldAmber;
                        break;
                    }
                    case CLAW: {
                        this.bindTexture(new ResourceLocation("pixelmon", "textures/fossils/covered/covered_claw_fossilmodel.png"));
                        fossilModel = coveredClawFossil;
                        break;
                    }
                    case SKULL: {
                        this.bindTexture(new ResourceLocation("pixelmon", "textures/fossils/covered/covered_skull_fossilmodel.png"));
                        fossilModel = coveredSkullFossil;
                        break;
                    }
                    case ARMOR: {
                        this.bindTexture(new ResourceLocation("pixelmon", "textures/fossils/covered/covered_armor_fossilmodel.png"));
                        fossilModel = coveredArmorFossil;
                        break;
                    }
                    case COVER: {
                        this.bindTexture(new ResourceLocation("pixelmon", "textures/fossils/covered/covered_cover_fossilmodel.png"));
                        fossilModel = coveredCoverFossil;
                        break;
                    }
                    case PLUME: {
                        this.bindTexture(new ResourceLocation("pixelmon", "textures/fossils/covered/covered_plume_fossilmodel.png"));
                        fossilModel = coveredPlumeFossil;
                        break;
                    }
                    case JAW: {
                        this.bindTexture(new ResourceLocation("pixelmon", "textures/fossils/covered/covered_jaw_fossilmodel.png"));
                        fossilModel = coveredJawFossil;
                        break;
                    }
                    case SAIL: {
                        this.bindTexture(new ResourceLocation("pixelmon", "textures/fossils/covered/covered_sail_fossilmodel.png"));
                        fossilModel = coveredSailFossil;
                        break;
                    }
                    case BIRD: {
                        this.bindTexture(new ResourceLocation("pixelmon", "textures/fossils/covered/covered_bird_fossilmodel.png"));
                        fossilModel = coveredBirdFossil;
                        break;
                    }
                    case FISH: {
                        this.bindTexture(new ResourceLocation("pixelmon", "textures/fossils/covered/covered_fish_fossilmodel.png"));
                        fossilModel = coveredFishFossil;
                        break;
                    }
                    case DINO: {
                        this.bindTexture(new ResourceLocation("pixelmon", "textures/fossils/covered/covered_dino_fossilmodel.png"));
                        fossilModel = coveredDinoFossil;
                        break;
                    }
                    case DRAKE: {
                        this.bindTexture(new ResourceLocation("pixelmon", "textures/fossils/covered/covered_drake_fossilmodel.png"));
                        fossilModel = coveredDrakeFossil;
                        break;
                    }
                    default: {
                        Pixelmon.LOGGER.warn("Unknown covered Fossil present. Name: " + coveredFossil);
                        break;
                    }
                }
            } else if (itemInside instanceof ItemFossil) {
                ItemFossil fossil = (ItemFossil)itemInside;
                this.bindTexture(fossil.getFossil().getTexture());
                fossilModel = (BlockModelHolder<GenericSmdModel>)fossil.getFossil().getModel();
            } else {
                Pixelmon.LOGGER.warn("Unrecognised item type in Fossil cleaner: " + itemInside.getTranslationKey());
            }
            if (fossilModel != null) {
                if (itemInside instanceof ItemCoveredFossil) {
                    ((GenericSmdModel)fossilModel.getModel()).setFrame(frame);
                } else {
                    GlStateManager.translate(0.0, -0.555, 0.0);
                }
                fossilModel.render();
            }
        }
    }
}

