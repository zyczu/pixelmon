/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.SharedModels;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.machines.BlockFossilMachine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityFossilMachine;
import com.pixelmongenerations.common.entity.pixelmon.EntityStatue;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumStatueTextureType;
import com.pixelmongenerations.core.enums.items.EnumFossils;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.opengl.GL11;

public class RenderTileFossilMachine
extends TileEntityRenderer<TileEntityFossilMachine> {
    private static final ResourceLocation MACHINE_TEXTURE = new ResourceLocation("pixelmon", "textures/blocks/Fossil_Extractor.png");
    private static final BlockModelHolder<GenericSmdModel> machineBase = new BlockModelHolder("blocks/fossil_machine/fossil_extractor.pqc");
    private static final GenericSmdModel machineGlass = new GenericSmdModel("models/blocks/fossil_machine", "fossil_extractor_glass.pqc");

    public RenderTileFossilMachine() {
        RenderTileFossilMachine.machineGlass.modelRenderer.setTransparent(0.5f);
        this.correctionAngles = 180;
    }

    @Override
    public void renderTileEntity(TileEntityFossilMachine machine, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        this.bindTexture(MACHINE_TEXTURE);
        if (machine.renderPass == 1) {
            machineGlass.renderModel(1.0f);
            return;
        }
        ((GenericSmdModel)machineBase.getModel()).renderModel(1.0f);
        GlStateManager.disableRescaleNormal();
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        int rotate = this.getRotation(state);
        EnumFacing facing = EnumFacing.NORTH;
        if (state.getBlock() instanceof BlockFossilMachine) {
            facing = state.getValue(MultiBlock.FACING);
        }
        this.renderCompletionLevel(machine.completionRate + "%", facing, rotate, x, y, z, false, -1);
        if (machine.staticFlicker && machine.completionRate == 100 && machine.pokeball == null) {
            this.renderCompletionLevel(I18n.translateToLocal("pixelmon.blocks.fossilmachine.pokeball"), facing, rotate, x, y + (double)0.08f, z, true, -65536);
        } else if (machine.completionRate > 0 && machine.completionRate < 100) {
            this.renderCompletionLevel(I18n.translateToLocal("pixelmon.blocks.fossilmachine.working") + machine.dots, facing, rotate, x, y + (double)0.08f, z, true, -16711936);
        } else if (machine.staticFlicker && machine.completionRate == 0 && machine.currentFossil == null) {
            this.renderCompletionLevel(I18n.translateToLocal("pixelmon.blocks.fossilmachine.fossil"), facing, rotate, x, y + (double)0.08f, z, true, -65536);
        } else if (machine.staticFlicker && machine.completionRate == 100) {
            this.renderCompletionLevel(I18n.translateToLocal("pixelmon.blocks.fossilmachine.retrieve"), facing, rotate, x, y + (double)0.08f, z, true, -16711936);
        }
        this.renderBarLevel(machine, facing, rotate, x, y, z);
        if (machine.currentFossil != null) {
            this.renderFossil(machine.currentFossil.getFossil(), machine, rotate, x, y, z, facing);
        }
        if (machine.pokeball != null) {
            this.renderPokeball(machine.pokeball, rotate, x, y, z);
        }
        if (!(machine.statue != null && machine.statue.getName().equalsIgnoreCase(machine.currentPokemon) || machine.currentPokemon.equals("") || !EnumSpecies.hasPokemon(machine.currentPokemon))) {
            machine.statue = new EntityStatue(this.getWorld());
            machine.statue.init(machine.currentPokemon);
            machine.statue.setGrowth(EnumGrowth.Small);
            if (machine.isShiny) {
                machine.statue.setTextureType(EnumStatueTextureType.Shiny);
            }
        }
        if (machine.statue != null) {
            GlStateManager.pushMatrix();
            GlStateManager.translate((float)x + 0.5f, (float)y + 0.9f, (float)z + 0.5f);
            GlStateManager.rotate(rotate, 0.0f, 1.0f, 0.0f);
            float scale = machine.currentPokemon.equals("Aerodactyl") ? Math.max(machine.pokemonProgress / 1000.0f / 4.0f - 0.55f, 0.0f) : Math.max(machine.pokemonProgress / 1000.0f / 4.0f - 0.15f, 0.0f);
            GlStateManager.scale(scale, scale, scale);
            machine.statue.setLocationAndAngles(x, y, z, 0.0f, 0.0f);
            Minecraft.getMinecraft().getRenderManager().renderEntity(machine.statue, 0.0, 0.0, 0.0, 0.0f, partialTicks, false);
            GlStateManager.popMatrix();
        }
    }

    @Override
    protected int getRotation(IBlockState state) {
        if (state.getBlock() instanceof BlockFossilMachine) {
            EnumFacing facing = state.getValue(MultiBlock.FACING);
            if (facing == EnumFacing.WEST) {
                return 90;
            }
            if (facing == EnumFacing.SOUTH) {
                return 180;
            }
            if (facing == EnumFacing.EAST) {
                return 270;
            }
            return 0;
        }
        return 0;
    }

    private void renderBarLevel(TileEntityFossilMachine tile, EnumFacing facing, int rotate, double x1, double y1, double z1) {
        GlStateManager.pushMatrix();
        float f2 = 1.5f;
        float f3 = 0.00666667f * f2;
        float x = (float)x1;
        float y = (float)y1 + 0.32f;
        float z = (float)z1;
        if (facing == EnumFacing.NORTH) {
            GlStateManager.translate(x + 0.5f, y, z + 0.97f);
        } else if (facing == EnumFacing.WEST) {
            GlStateManager.translate(x + 0.97f, y, z + 0.5f);
        } else if (facing == EnumFacing.SOUTH) {
            GlStateManager.translate(x + 0.5f, y, z + 0.03f);
        } else {
            GlStateManager.translate(x + 0.03f, y, z + 0.5f);
        }
        GL11.glNormal3f((float)0.0f, (float)1.0f, (float)0.0f);
        GlStateManager.rotate(rotate + 180, 0.0f, 1.0f, 0.0f);
        GlStateManager.scale(-f3, -f3, f3);
        GlStateManager.disableLighting();
        GlStateManager.depthMask(false);
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(770, 771);
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        int byte0 = -20;
        GlStateManager.disableTexture2D();
        float f8 = tile.completionRate / 2;
        buffer.begin(7, DefaultVertexFormats.POSITION_COLOR);
        buffer.pos(-25.0f + f8, -7 + byte0, 0.0).color(0.0039f, 0.03137f, 0.4196f, 1.0f).endVertex();
        buffer.pos(-25.0f + f8, -6 + byte0, 0.0).color(0.0039f, 0.03137f, 0.4196f, 1.0f).endVertex();
        buffer.pos(25.0, -6 + byte0, 0.0).color(0.0039f, 0.03137f, 0.4196f, 1.0f).endVertex();
        buffer.pos(25.0, -7 + byte0, 0.0).color(0.0039f, 0.03137f, 0.4196f, 1.0f).endVertex();
        buffer.pos(-25.0, -7 + byte0, 0.0).color(0.0f, 0.8901f, 0.8901f, 1.0f).endVertex();
        buffer.pos(-25.0, -6 + byte0, 0.0).color(0.0f, 0.8901f, 0.8901f, 1.0f).endVertex();
        buffer.pos(f8 - 25.0f, -6 + byte0, 0.0).color(0.0f, 0.8901f, 0.8901f, 1.0f).endVertex();
        buffer.pos(f8 - 25.0f, -7 + byte0, 0.0).color(0.0f, 0.8901f, 0.8901f, 1.0f).endVertex();
        tessellator.draw();
        GlStateManager.enableTexture2D();
        GlStateManager.depthMask(true);
        GlStateManager.enableLighting();
        GlStateManager.disableBlend();
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.popMatrix();
    }

    private void renderCompletionLevel(String text, EnumFacing facing, int rotation, double x1, double y1, double z1, boolean par10, int color) {
        FontRenderer fontRenderer = this.getFontRenderer();
        GlStateManager.pushMatrix();
        float var13 = 1.5f;
        float var14 = 0.011666667f * var13;
        float x = (float)x1;
        float y = (float)y1 + 0.75f;
        float z = (float)z1;
        if (facing == EnumFacing.NORTH) {
            GlStateManager.translate(x + 0.5f, y, z + 0.97f);
        } else if (facing == EnumFacing.WEST) {
            GlStateManager.translate(x + 0.97f, y, z + 0.5f);
        } else if (facing == EnumFacing.SOUTH) {
            GlStateManager.translate(x + 0.5f, y, z + 0.03f);
        } else {
            GlStateManager.translate(x + 0.03f, y, z + 0.5f);
        }
        GlStateManager.rotate(rotation + 180, 0.0f, 1.0f, 0.0f);
        GL11.glNormal3f((float)0.0f, (float)1.0f, (float)0.0f);
        if (!par10) {
            GlStateManager.scale(-var14, -var14, var14);
        } else {
            GlStateManager.scale(-var14 + 0.012f, -var14 + 0.012f, var14 + 0.012f);
        }
        GlStateManager.disableLighting();
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(770, 771);
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        GlStateManager.disableTexture2D();
        int stringWidth = fontRenderer.getStringWidth(text) / 2;
        GlStateManager.enableTexture2D();
        fontRenderer.drawString(text, -stringWidth, 0, color);
        buffer.begin(7, DefaultVertexFormats.POSITION);
        buffer.pos(-stringWidth - 1, -1.0, 0.0).endVertex();
        buffer.pos(-stringWidth - 1, 8.0, 0.0).endVertex();
        buffer.pos(stringWidth + 1, 8.0, 0.0).endVertex();
        buffer.pos(stringWidth + 1, -1.0, 0.0).endVertex();
        tessellator.draw();
        fontRenderer.drawString(text, -stringWidth, 0, color);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.enableLighting();
        GlStateManager.disableBlend();
        GlStateManager.popMatrix();
    }

    private void renderFossil(EnumFossils fossil, TileEntityFossilMachine machine, int rotate, double x, double y, double z, EnumFacing facing) {
        GlStateManager.pushMatrix();
        GlStateManager.translate((float)x + 0.5f, (float)y + 1.0f + machine.fossilJitter, (float)z + 0.5f);
        GlStateManager.scale(0.8f - machine.fossilProgress / 1000.0f / 2.0f, -0.8f + machine.fossilProgress / 1000.0f / 2.0f, -0.8f + machine.fossilProgress / 1000.0f / 2.0f);
        if (facing == EnumFacing.NORTH || facing == EnumFacing.SOUTH) {
            GlStateManager.rotate(rotate, 0.0f, 1.0f, 0.0f);
        } else {
            GlStateManager.rotate((float)rotate + 180.0f, 0.0f, 0.5f, 0.0f);
        }
        this.bindTexture(fossil.getTexture());
        ((BlockModelHolder)fossil.getModel()).render(1.0f);
        GlStateManager.popMatrix();
    }

    private void renderPokeball(EnumPokeball pokeBall, int rotate, double x, double y, double z) {
        GlStateManager.pushMatrix();
        if (rotate == 270) {
            GlStateManager.translate(x - 0.1, y + (double)0.42f, z + 0.86);
        } else if (rotate == 180) {
            GlStateManager.translate(x + 0.14, y + (double)0.42f, z - 0.1);
        } else if (rotate == 90) {
            GlStateManager.translate(x + 1.1, y + (double)0.42f, z + 0.14);
        } else {
            GlStateManager.translate(x + 0.86, y + (double)0.42f, z + 1.1);
        }
        GlStateManager.rotate(rotate, 0.0f, 1.0f, 0.0f);
        this.bindTexture(pokeBall.getTextureLocation());
        SharedModels.getPokeballModel(pokeBall).render(0.002f);
        GlStateManager.popMatrix();
    }
}

