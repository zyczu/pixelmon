/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render.tileEntities;

import com.pixelmongenerations.client.models.blocks.ModelChair;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.client.render.tileEntities.TileEntityRenderer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityChair;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;

public class ChairRenderer
extends TileEntityRenderer<TileEntityChair> {
    private static final BlockModelHolder<ModelChair> model = new BlockModelHolder<ModelChair>(ModelChair.class);
    private static final ResourceLocation texture = new ResourceLocation("pixelmon", "textures/blocks/ChairModel.png");

    public ChairRenderer() {
        this.scale = 0.0625f;
        this.yOffset = 1.5f;
    }

    @Override
    public void renderTileEntity(TileEntityChair te, IBlockState state, double x, double y, double z, float partialTicks, int destroyStage) {
        this.bindTexture(texture);
        model.render();
    }
}

