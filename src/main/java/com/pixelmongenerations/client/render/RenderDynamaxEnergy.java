/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render;

import com.pixelmongenerations.common.entity.EntityDynamaxEnergy;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Random;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import org.lwjgl.opengl.GL11;

public class RenderDynamaxEnergy
extends RenderLiving<EntityDynamaxEnergy> {
    private float ticks = 0.0f;
    private static final ResourceLocation DYNAMAX_TEXTURE = new ResourceLocation("pixelmon", "textures/dynamaxbeam.png");
    public static final ResourceLocation TEXTURE_BEACON_BEAM = new ResourceLocation("textures/entity/beacon_beam.png");
    private static float lightmapLastX;
    private static float lightmapLastY;
    private static boolean optifineBreak;
    private static boolean glowing;
    Random rand = RandomHelper.rand;

    public RenderDynamaxEnergy(RenderManager renderManagerIn) {
        super(renderManagerIn, null, 0.5f);
        this.shadowSize = 0.0f;
    }

    @Override
    protected ResourceLocation getEntityTexture(EntityDynamaxEnergy entity) {
        return DYNAMAX_TEXTURE;
    }

    @Override
    public void doRender(EntityDynamaxEnergy entity, double x, double y, double z, float entityYaw, float partialTicks) {
        GlStateManager.pushMatrix();
        GlStateManager.disableFog();
        RenderDynamaxEnergy.glowOn();
        GlStateManager.alphaFunc(516, 0.1f);
        this.bindTexture(this.getEntityTexture(entity));
        BlockPos blockPos = entity.getPosition();
        x = (double)blockPos.getX() - TileEntityRendererDispatcher.staticPlayerX;
        y = (double)blockPos.getY() - TileEntityRendererDispatcher.staticPlayerY;
        z = (double)blockPos.getZ() - TileEntityRendererDispatcher.staticPlayerZ;
        RenderDynamaxEnergy.renderBeamSegment(x, y, z, partialTicks, 1.0, entity.world.getTotalWorldTime(), 0, 20, new float[]{0.96f, 0.17f, 0.4f});
        RenderDynamaxEnergy.glowOff();
        GlStateManager.enableFog();
        GlStateManager.popMatrix();
    }

    public static void glowOn() {
        RenderDynamaxEnergy.glowOn(15);
    }

    public static void glowOn(int glow) {
        glowing = true;
        GL11.glPushAttrib((int)64);
        GL11.glEnable((int)3042);
        GL11.glBlendFunc((int)1, (int)1);
        try {
            lightmapLastX = OpenGlHelper.lastBrightnessX;
            lightmapLastY = OpenGlHelper.lastBrightnessY;
        }
        catch (NoSuchFieldError e) {
            optifineBreak = true;
        }
        float glowRatioX = Math.min((float)glow / 15.0f * 240.0f + lightmapLastX, 240.0f);
        float glowRatioY = Math.min((float)glow / 15.0f * 240.0f + lightmapLastY, 240.0f);
        if (!optifineBreak) {
            OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, glowRatioX, glowRatioY);
        }
    }

    public static void glowOff() {
        GL11.glEnable((int)2896);
        if (!optifineBreak) {
            OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, lightmapLastX, lightmapLastY);
        }
        GL11.glPopAttrib();
        glowing = false;
    }

    public static void renderBeamSegment(double x, double y, double z, double partialTicks, double textureScale, double totalWorldTime, int yOffset, int height, float[] colors) {
        RenderDynamaxEnergy.renderBeamSegment(x, y, z, partialTicks, textureScale, totalWorldTime, yOffset, height, colors, 0.2, 0.25);
    }

    public static void renderBeamSegment(double x, double y, double z, double partialTicks, double textureScale, double totalWorldTime, int yOffset, int height, float[] colors, double beamRadius, double glowRadius) {
        int i = yOffset + height;
        GlStateManager.glTexParameteri(3553, 10242, 10497);
        GlStateManager.glTexParameteri(3553, 10243, 10497);
        GlStateManager.disableLighting();
        GlStateManager.disableCull();
        GlStateManager.disableBlend();
        GlStateManager.depthMask(true);
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        double d0 = totalWorldTime + partialTicks;
        double d1 = height < 0 ? d0 : -d0;
        double d2 = MathHelper.frac(d1 * 0.2 - (double)MathHelper.floor(d1 * 0.1));
        float f = colors[0];
        float f1 = colors[1];
        float f2 = colors[2];
        double d3 = d0 * 0.025 * -1.5;
        double d4 = 0.5 + Math.cos(d3 + 2.356194490192345) * beamRadius;
        double d5 = 0.5 + Math.sin(d3 + 2.356194490192345) * beamRadius;
        double d6 = 0.5 + Math.cos(d3 + 0.7853981633974483) * beamRadius;
        double d7 = 0.5 + Math.sin(d3 + 0.7853981633974483) * beamRadius;
        double d8 = 0.5 + Math.cos(d3 + 3.9269908169872414) * beamRadius;
        double d9 = 0.5 + Math.sin(d3 + 3.9269908169872414) * beamRadius;
        double d10 = 0.5 + Math.cos(d3 + 5.497787143782138) * beamRadius;
        double d11 = 0.5 + Math.sin(d3 + 5.497787143782138) * beamRadius;
        double d14 = -1.0 + d2;
        double d15 = (double)height * textureScale * (0.5 / beamRadius) + d14;
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
        bufferbuilder.pos(x + d4, y + (double)i, z + d5).tex(1.0, d15).color(f, f1, f2, 1.0f).endVertex();
        bufferbuilder.pos(x + d4, y + (double)yOffset, z + d5).tex(1.0, d14).color(f, f1, f2, 1.0f).endVertex();
        bufferbuilder.pos(x + d6, y + (double)yOffset, z + d7).tex(0.0, d14).color(f, f1, f2, 1.0f).endVertex();
        bufferbuilder.pos(x + d6, y + (double)i, z + d7).tex(0.0, d15).color(f, f1, f2, 1.0f).endVertex();
        bufferbuilder.pos(x + d10, y + (double)i, z + d11).tex(1.0, d15).color(f, f1, f2, 1.0f).endVertex();
        bufferbuilder.pos(x + d10, y + (double)yOffset, z + d11).tex(1.0, d14).color(f, f1, f2, 1.0f).endVertex();
        bufferbuilder.pos(x + d8, y + (double)yOffset, z + d9).tex(0.0, d14).color(f, f1, f2, 1.0f).endVertex();
        bufferbuilder.pos(x + d8, y + (double)i, z + d9).tex(0.0, d15).color(f, f1, f2, 1.0f).endVertex();
        bufferbuilder.pos(x + d6, y + (double)i, z + d7).tex(1.0, d15).color(f, f1, f2, 1.0f).endVertex();
        bufferbuilder.pos(x + d6, y + (double)yOffset, z + d7).tex(1.0, d14).color(f, f1, f2, 1.0f).endVertex();
        bufferbuilder.pos(x + d10, y + (double)yOffset, z + d11).tex(0.0, d14).color(f, f1, f2, 1.0f).endVertex();
        bufferbuilder.pos(x + d10, y + (double)i, z + d11).tex(0.0, d15).color(f, f1, f2, 1.0f).endVertex();
        bufferbuilder.pos(x + d8, y + (double)i, z + d9).tex(1.0, d15).color(f, f1, f2, 1.0f).endVertex();
        bufferbuilder.pos(x + d8, y + (double)yOffset, z + d9).tex(1.0, d14).color(f, f1, f2, 1.0f).endVertex();
        bufferbuilder.pos(x + d4, y + (double)yOffset, z + d5).tex(0.0, d14).color(f, f1, f2, 1.0f).endVertex();
        bufferbuilder.pos(x + d4, y + (double)i, z + d5).tex(0.0, d15).color(f, f1, f2, 1.0f).endVertex();
        tessellator.draw();
        GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
        GlStateManager.depthMask(false);
        d3 = 0.5 - glowRadius;
        d4 = 0.5 - glowRadius;
        d5 = 0.5 + glowRadius;
        d6 = 0.5 - glowRadius;
        d7 = 0.5 - glowRadius;
        d8 = 0.5 + glowRadius;
        d9 = 0.5 + glowRadius;
        d10 = 0.5 + glowRadius;
        double d13 = -1.0 + d2;
        d14 = (double)height * textureScale + d13;
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
        bufferbuilder.pos(x + d3, y + (double)i, z + d4).tex(1.0, d14).color(f, f1, f2, 0.125f).endVertex();
        bufferbuilder.pos(x + d3, y + (double)yOffset, z + d4).tex(1.0, d13).color(f, f1, f2, 0.125f).endVertex();
        bufferbuilder.pos(x + d5, y + (double)yOffset, z + d6).tex(0.0, d13).color(f, f1, f2, 0.125f).endVertex();
        bufferbuilder.pos(x + d5, y + (double)i, z + d6).tex(0.0, d14).color(f, f1, f2, 0.125f).endVertex();
        bufferbuilder.pos(x + d9, y + (double)i, z + d10).tex(1.0, d14).color(f, f1, f2, 0.125f).endVertex();
        bufferbuilder.pos(x + d9, y + (double)yOffset, z + d10).tex(1.0, d13).color(f, f1, f2, 0.125f).endVertex();
        bufferbuilder.pos(x + d7, y + (double)yOffset, z + d8).tex(0.0, d13).color(f, f1, f2, 0.125f).endVertex();
        bufferbuilder.pos(x + d7, y + (double)i, z + d8).tex(0.0, d14).color(f, f1, f2, 0.125f).endVertex();
        bufferbuilder.pos(x + d5, y + (double)i, z + d6).tex(1.0, d14).color(f, f1, f2, 0.125f).endVertex();
        bufferbuilder.pos(x + d5, y + (double)yOffset, z + d6).tex(1.0, d13).color(f, f1, f2, 0.125f).endVertex();
        bufferbuilder.pos(x + d9, y + (double)yOffset, z + d10).tex(0.0, d13).color(f, f1, f2, 0.125f).endVertex();
        bufferbuilder.pos(x + d9, y + (double)i, z + d10).tex(0.0, d14).color(f, f1, f2, 0.125f).endVertex();
        bufferbuilder.pos(x + d7, y + (double)i, z + d8).tex(1.0, d14).color(f, f1, f2, 0.125f).endVertex();
        bufferbuilder.pos(x + d7, y + (double)yOffset, z + d8).tex(1.0, d13).color(f, f1, f2, 0.125f).endVertex();
        bufferbuilder.pos(x + d3, y + (double)yOffset, z + d4).tex(0.0, d13).color(f, f1, f2, 0.125f).endVertex();
        bufferbuilder.pos(x + d3, y + (double)i, z + d4).tex(0.0, d14).color(f, f1, f2, 0.125f).endVertex();
        tessellator.draw();
        GlStateManager.enableLighting();
        GlStateManager.enableTexture2D();
        GlStateManager.depthMask(true);
    }

    static {
        optifineBreak = false;
        glowing = false;
    }
}

