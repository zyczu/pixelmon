/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render;

import com.pixelmongenerations.common.entity.EntityWishingStar;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;

public class RenderWishingStar
extends RenderLiving<EntityWishingStar> {
    private float ticks = 0.0f;
    private static final ResourceLocation DYNAMAX_TEXTURE = new ResourceLocation("pixelmon", "textures/dynamaxbeam.png");
    public static final ResourceLocation TEXTURE_BEACON_BEAM = new ResourceLocation("textures/entity/beacon_beam.png");
    private static float lightmapLastX;
    private static float lightmapLastY;
    private static boolean optifineBreak;
    private static boolean glowing;

    public RenderWishingStar(RenderManager renderManagerIn) {
        super(renderManagerIn, null, 0.5f);
        this.shadowSize = 0.0f;
    }

    @Override
    protected ResourceLocation getEntityTexture(EntityWishingStar entity) {
        return DYNAMAX_TEXTURE;
    }

    @Override
    public void doRender(EntityWishingStar entity, double x, double y, double z, float entityYaw, float partialTicks) {
    }

    static {
        optifineBreak = false;
        glowing = false;
    }
}

