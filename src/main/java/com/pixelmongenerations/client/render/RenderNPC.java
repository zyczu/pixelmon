/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.vecmath.Vector3f
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.render;

import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.proxy.ClientProxy;
import javax.vecmath.Vector3f;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.opengl.GL11;

public class RenderNPC
extends RenderLiving<EntityNPC> {
    private int defaultNameRenderDistance = 8;
    private int defaultBossNameRenderDistanceExtension = 8;
    private int configNameRenderMultiplier = Math.max(1, Math.min(PixelmonConfig.nameplateRangeModifier, 3));
    private int nameRenderDistanceNormal = this.defaultNameRenderDistance * this.configNameRenderMultiplier;
    private int nameRenderDistanceBoss = this.nameRenderDistanceNormal + this.defaultBossNameRenderDistanceExtension;

    public RenderNPC(RenderManager manager) {
        super(manager, null, 0.5f);
    }

    @Override
    public void doRender(EntityNPC npc, double d, double d1, double d2, float f, float f1) {
        float renderdistance;
        this.mainModel = npc.getModel();
        if (this.mainModel == null) {
            return;
        }
        float var10 = npc.getDistance(this.renderManager.renderViewEntity);
        float f2 = renderdistance = npc.getBossMode() != EnumBossMode.NotBoss ? (float)this.nameRenderDistanceBoss : (float)this.nameRenderDistanceNormal;
        if (var10 <= renderdistance) {
            this.drawNameTag(npc, d, d1, d2);
        }
        super.doRender(npc, d, d1, d2, f, f1);
    }

    public void drawNameTag(EntityLiving entityliving, double x, double y, double z) {
        if (Minecraft.isGuiEnabled()) {
            try {
                EntityNPC npc = (EntityNPC)entityliving;
                String displayText = npc.getDisplayText();
                String subtitleText = npc.getSubTitleText();
                if (!displayText.equals("")) {
                    this.renderLivingLabel(npc, displayText, subtitleText, x, y, z);
                }
            }
            catch (Exception exception) {
                // empty catch block
            }
        }
    }

    protected void renderLivingLabel(EntityNPC npc, String displayText, String subtitleText, double x, double y, double z) {
        FontRenderer fontRenderer = this.getFontRendererFromRenderManager();
        float var13 = 1.6f;
        float var14 = 0.016666668f * var13;
        GlStateManager.pushMatrix();
        if (npc.getScale().y < 1.0f) {
            GlStateManager.translate((float)x, (float)y + 1.1f + 1.0f, (float)z);
        } else {
            GlStateManager.translate((float)x, (float)y + 1.1f + 1.4f, (float)z);
        }
        GL11.glNormal3f((float)0.0f, (float)1.0f, (float)0.0f);
        GlStateManager.rotate(-this.renderManager.playerViewY, 0.0f, 1.0f, 0.0f);
        GlStateManager.rotate(this.renderManager.playerViewX, 1.0f, 0.0f, 0.0f);
        GlStateManager.scale(-var14, -var14, var14);
        GlStateManager.disableLighting();
        GlStateManager.enableDepth();
        GlStateManager.disableAlpha();
        GlStateManager.enableTexture2D();
        if (npc instanceof NPCTrainer && npc.getBossMode() != EnumBossMode.NotBoss) {
            EnumBossMode enumBossMode = npc.getBossMode();
            String bossTag = I18n.translateToLocal("gui.boss.text");
            String bossMode = enumBossMode.getBossText();
            int bossTagWidth = fontRenderer.getStringWidth(bossMode);
            int bossTagpos = bossTagWidth / 2 * -1;
            GlStateManager.pushMatrix();
            GlStateManager.translate(-2.5 + (double)bossTagpos, -4.5, 0.0);
            GlStateManager.scale(0.5, 0.5, 0.5);
            fontRenderer.drawString(bossTag, 0, 0, 0x20FFFFFF);
            GlStateManager.popMatrix();
            fontRenderer.drawString(bossMode, bossTagpos, 0, enumBossMode.getColourInt());
        } else {
            if (subtitleText != null) {
                int displayTextWidth = fontRenderer.getStringWidth(displayText);
                int displayTextPos = displayTextWidth / 2 * -1;
                GlStateManager.pushMatrix();
                GlStateManager.translate(-2.5 + (double)displayTextPos, -4.5, 0.0);
                GlStateManager.scale(0.5, 0.5, 0.5);
                fontRenderer.drawString(subtitleText, 0, 0, 0x20FFFFFF);
                GlStateManager.popMatrix();
                fontRenderer.drawString(displayText, displayTextPos, 0, 0x20FFFFFF);
            }
            fontRenderer.drawString(displayText, -fontRenderer.getStringWidth(displayText) / 2, 0, 0x20FFFFFF);
            GlStateManager.depthMask(true);
        }
        GlStateManager.enableLighting();
        GlStateManager.disableBlend();
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.popMatrix();
    }

    @Override
    protected boolean bindEntityTexture(EntityNPC entity) {
        String texture;
        TextureResource dynTexture;
        if (entity.getCustomSteveTexture() != null && (dynTexture = ClientProxy.TEXTURE_STORE.getObject(texture = entity.getCustomSteveTexture())) != null) {
            dynTexture.bindTexture();
            return true;
        }
        if (!entity.bindTexture()) {
            return super.bindEntityTexture(entity);
        }
        return true;
    }

    @Override
    protected ResourceLocation getEntityTexture(EntityNPC entity) {
        String texture = entity.getTexture().replaceAll(".png.png", ".png");
        return new ResourceLocation(texture);
    }

    @Override
    protected void preRenderCallback(EntityNPC npc, float partialTickTime) {
        Vector3f scale = npc.getScale();
        GlStateManager.scale(scale.x, scale.y, scale.z);
        super.preRenderCallback(npc, partialTickTime);
    }
}

