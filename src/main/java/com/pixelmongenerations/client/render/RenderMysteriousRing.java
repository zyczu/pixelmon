/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render;

import com.pixelmongenerations.client.models.MysteriousRingModelSmd;
import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.common.entity.EntityMysteriousRing;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;

public class RenderMysteriousRing
extends RenderLiving<EntityMysteriousRing> {
    public RenderMysteriousRing(RenderManager rendermanagerIn) {
        super(rendermanagerIn, new GenericSmdModel("models/mysterious_ring", "mysterious_ring.pqc"), 0.5f);
    }

    @Override
    public void doRender(EntityMysteriousRing ring, double x, double y, double z, float yaw, float partialTicks) {
        GlStateManager.alphaFunc(516, 0.1f);
        this.renderMysteriousRing(ring, x, y, z, yaw, partialTicks);
    }

    public void renderMysteriousRing(EntityMysteriousRing ring, double x, double y, double z, float yaw, float partialTicks) {
        GlStateManager.pushMatrix();
        GlStateManager.disableCull();
        this.mainModel.swingProgress = this.getSwingProgress(ring, partialTicks);
        this.mainModel.isRiding = ring.isRiding();
        this.mainModel.isChild = ring.isChild();
        try {
            if (this.mainModel instanceof MysteriousRingModelSmd && ((MysteriousRingModelSmd)this.mainModel).theModel.hasAnimations()) {
                ((MysteriousRingModelSmd)this.mainModel).setupForRender(ring);
            }
            float f2 = this.interpolateRotation(ring.prevRenderYawOffset, ring.renderYawOffset, partialTicks);
            float f3 = this.interpolateRotation(ring.prevRotationYawHead, ring.rotationYawHead, partialTicks);
            float f4 = f3 - f2;
            float f9 = ring.prevRotationPitch + (ring.rotationPitch - ring.prevRotationPitch) * partialTicks;
            this.renderLivingAt(ring, x, y + 2.0, z + 1.6);
            float f5 = 0.0f;
            this.applyRotations(ring, f5, f2, partialTicks);
            GlStateManager.enableRescaleNormal();
            GlStateManager.scale(-15.0f, -15.0f, -15.0f);
            GlStateManager.rotate(90.0f, 0.0f, 0.0f, 90.0f);
            this.preRenderCallback(ring, partialTicks);
            float f7 = ring.prevLimbSwingAmount + (ring.limbSwingAmount - ring.prevLimbSwingAmount) * partialTicks;
            float f8 = ring.limbSwing - ring.limbSwingAmount * (1.0f - partialTicks);
            GlStateManager.enableAlpha();
            this.mainModel.setLivingAnimations(ring, f8, f7, partialTicks);
            this.mainModel.setRotationAngles(f8, f7, f5, f4, f9, 0.0625f, ring);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            boolean flag = this.setDoRenderBrightness(ring, partialTicks);
            this.bindEntityTexture(ring);
            this.renderModel(ring, f8, f7, f5, 0.0f, 0.0f, 10.5f);
            if (flag) {
                this.unsetBrightness();
            }
            GlStateManager.depthMask(true);
            GlStateManager.disableRescaleNormal();
        }
        catch (Exception exc) {
            exc.printStackTrace();
        }
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GlStateManager.enableTexture2D();
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
        GlStateManager.enableCull();
        GlStateManager.popMatrix();
        this.renderName(ring, x, y, z);
    }

    @Override
    protected ResourceLocation getEntityTexture(EntityMysteriousRing entity) {
        return new ResourceLocation("pixelmon:textures/mysterious_ring.png");
    }
}

