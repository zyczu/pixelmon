/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.render;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.common.entity.EntitySpaceTimeDistortion;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.culling.ICamera;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.jetbrains.annotations.Nullable;

@SideOnly(value=Side.CLIENT)
public class RenderSpaceTimeDistortion
extends RenderLiving<EntitySpaceTimeDistortion> {
    public RenderSpaceTimeDistortion(RenderManager rendermanagerIn) {
        super(rendermanagerIn, new GenericSmdModel("models/spacetimedistortions", "spacetimedistortions.pqc"), 0.5f);
    }

    @Override
    public void doRender(EntitySpaceTimeDistortion statue, double x, double y, double z, float yaw, float partialTicks) {
        GlStateManager.alphaFunc(516, 0.1f);
        this.renderstd(statue, x, y, z, partialTicks);
    }

    public void renderstd(EntitySpaceTimeDistortion std, double x, double y, double z, float partialTicks) {
        GlStateManager.pushMatrix();
        GlStateManager.disableCull();
        this.mainModel.swingProgress = this.getSwingProgress(std, partialTicks);
        this.mainModel.isRiding = std.isRiding();
        this.mainModel.isChild = std.isChild();
        try {
            float f2 = this.interpolateRotation(std.prevRenderYawOffset, std.renderYawOffset, partialTicks);
            float f3 = this.interpolateRotation(std.prevRotationYawHead, std.rotationYawHead, partialTicks);
            float f4 = f3 - f2;
            float f9 = std.prevRotationPitch + (std.rotationPitch - std.prevRotationPitch) * partialTicks;
            this.renderLivingAt(std, x, y, z);
            float f5 = 0.0f;
            this.applyRotations(std, f5 + std.rotationPitch, f2, partialTicks);
            GlStateManager.enableRescaleNormal();
            GlStateManager.scale(-15.0f, -15.0f, 15.0f);
            this.preRenderCallback(std, partialTicks);
            float f7 = std.prevLimbSwingAmount + (std.limbSwingAmount - std.prevLimbSwingAmount) * partialTicks;
            float f8 = std.limbSwing - std.limbSwingAmount * (1.0f - partialTicks);
            GlStateManager.enableAlpha();
            this.mainModel.setLivingAnimations(std, f8, f7, partialTicks);
            this.mainModel.setRotationAngles(f8, f7, f5, f4, f9, 0.0625f, std);
            if (std.maxSize) {
                GlStateManager.color(2.0f, 0.5f, 0.5f, 1.0f);
            } else {
                GlStateManager.color(0.5f, 0.5f, 2.0f, 1.0f);
            }
            boolean flag = this.setDoRenderBrightness(std, partialTicks);
            this.bindEntityTexture(std);
            this.renderModel(std, f8, f7, f5, std.rotationYaw, std.rotationPitch, 0.75f + std.height / 2.0f);
            if (flag) {
                this.unsetBrightness();
            }
            GlStateManager.depthMask(true);
            GlStateManager.disableRescaleNormal();
        }
        catch (Exception exc) {
            exc.printStackTrace();
        }
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GlStateManager.enableTexture2D();
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
        GlStateManager.enableCull();
        GlStateManager.popMatrix();
        this.renderName(std, x, y, z);
    }

    @Override
    @Nullable
    protected ResourceLocation getEntityTexture(EntitySpaceTimeDistortion entity) {
        return new ResourceLocation("pixelmon:textures/std.png");
    }

    @Override
    public boolean shouldRender(EntitySpaceTimeDistortion livingEntity, ICamera camera, double camX, double camY, double camZ) {
        return true;
    }
}

