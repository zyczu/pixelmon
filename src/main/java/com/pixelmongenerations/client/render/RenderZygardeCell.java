/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.client.render;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.common.entity.EntityZygardeCell;
import javax.annotation.Nullable;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(value=Side.CLIENT)
public class RenderZygardeCell
extends RenderLiving<EntityZygardeCell> {
    public ResourceLocation texture = new ResourceLocation("pixelmon:textures/zygardecell.png");

    public RenderZygardeCell(RenderManager manager) {
        super(manager, new GenericSmdModel("models/zygardecell", "zygardecell.pqc"), 0.5f);
    }

    @Override
    public void doRender(EntityZygardeCell statue, double x, double y, double z, float yaw, float partialTicks) {
        GlStateManager.alphaFunc(516, 0.1f);
        this.renderZygardeCell(statue, x, y, z, partialTicks);
    }

    public void renderZygardeCell(EntityZygardeCell zygardecell, double x, double y, double z, float partialTicks) {
        GlStateManager.pushMatrix();
        GlStateManager.disableCull();
        this.mainModel.swingProgress = this.getSwingProgress(zygardecell, partialTicks);
        this.mainModel.isRiding = zygardecell.isRiding();
        this.mainModel.isChild = zygardecell.isChild();
        try {
            float f2 = this.interpolateRotation(zygardecell.prevRenderYawOffset, zygardecell.renderYawOffset, partialTicks);
            float f3 = this.interpolateRotation(zygardecell.prevRotationYawHead, zygardecell.rotationYawHead, partialTicks);
            float f4 = f3 - f2;
            float f9 = zygardecell.prevRotationPitch + (zygardecell.rotationPitch - zygardecell.prevRotationPitch) * partialTicks;
            this.renderLivingAt(zygardecell, x, y, z);
            float f5 = 0.0f;
            this.applyRotations(zygardecell, f5, f2, partialTicks);
            GlStateManager.enableRescaleNormal();
            GlStateManager.scale(-15.0f, -15.0f, 15.0f);
            this.preRenderCallback(zygardecell, partialTicks);
            float f7 = zygardecell.prevLimbSwingAmount + (zygardecell.limbSwingAmount - zygardecell.prevLimbSwingAmount) * partialTicks;
            float f8 = zygardecell.limbSwing - zygardecell.limbSwingAmount * (1.0f - partialTicks);
            GlStateManager.enableAlpha();
            this.mainModel.setLivingAnimations(zygardecell, f8, f7, partialTicks);
            this.mainModel.setRotationAngles(f8, f7, f5, f4, f9, 0.0625f, zygardecell);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            boolean flag = this.setDoRenderBrightness(zygardecell, partialTicks);
            this.bindEntityTexture(zygardecell);
            this.renderModel(zygardecell, f8, f7, f5, 0.0f, 0.0f, 0.02f);
            if (flag) {
                this.unsetBrightness();
            }
            GlStateManager.depthMask(true);
            GlStateManager.disableRescaleNormal();
        }
        catch (Exception exc) {
            exc.printStackTrace();
        }
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GlStateManager.enableTexture2D();
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
        GlStateManager.enableCull();
        GlStateManager.popMatrix();
        this.renderName(zygardecell, x, y, z);
    }

    @Override
    @Nullable
    protected ResourceLocation getEntityTexture(EntityZygardeCell entity) {
        return this.texture;
    }
}

