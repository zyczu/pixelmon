/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client;

import com.pixelmongenerations.client.camera.EntityCamera;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;
import org.jetbrains.annotations.NotNull;

public class RenderInvisibleCamera
extends Render<EntityCamera> {
    public RenderInvisibleCamera(RenderManager renderManager) {
        super(renderManager);
    }

    @Override
    public void doRender(@NotNull EntityCamera var1, double var2, double var4, double var6, float var8, float var9) {
    }

    @Override
    protected ResourceLocation getEntityTexture(@NotNull EntityCamera entity) {
        return null;
    }
}

