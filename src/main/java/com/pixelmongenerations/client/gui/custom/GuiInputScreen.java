/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 */
package com.pixelmongenerations.client.gui.custom;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumMovement;
import com.pixelmongenerations.core.network.packetHandlers.EnumMovementType;
import com.pixelmongenerations.core.network.packetHandlers.Movement;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import org.lwjgl.input.Keyboard;

public class GuiInputScreen
extends GuiScreen {
    int jump = 0;
    int forward = 0;
    int strafe = 0;

    @Override
    public void initGui() {
        super.initGui();
        this.allowUserInput = true;
    }

    @Override
    protected void keyTyped(char keyChar, int code) {
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        if (Keyboard.isKeyDown((int)Minecraft.getMinecraft().gameSettings.keyBindJump.getKeyCode())) {
            ++this.jump;
        }
        if (Keyboard.isKeyDown((int)Minecraft.getMinecraft().gameSettings.keyBindForward.getKeyCode())) {
            ++this.forward;
        }
        if (Keyboard.isKeyDown((int)Minecraft.getMinecraft().gameSettings.keyBindBack.getKeyCode())) {
            --this.forward;
        }
        if (Keyboard.isKeyDown((int)Minecraft.getMinecraft().gameSettings.keyBindLeft.getKeyCode())) {
            --this.strafe;
        }
        if (Keyboard.isKeyDown((int)Minecraft.getMinecraft().gameSettings.keyBindRight.getKeyCode())) {
            ++this.strafe;
        }
        int numMovements = 0;
        if (this.jump != 0) {
            ++numMovements;
        }
        if (this.forward != 0) {
            ++numMovements;
        }
        if (this.strafe != 0) {
            ++numMovements;
        }
        EnumMovement[] movements = new EnumMovement[numMovements];
        int i = 0;
        if (this.jump > 0) {
            movements[i++] = EnumMovement.Jump;
        }
        if (this.forward < 0) {
            movements[i++] = EnumMovement.Back;
        }
        if (this.forward > 0) {
            movements[i++] = EnumMovement.Forward;
        }
        if (this.strafe < 0) {
            movements[i++] = EnumMovement.Left;
        }
        if (this.strafe > 0) {
            movements[i++] = EnumMovement.Right;
        }
        if (numMovements > 0) {
            Pixelmon.NETWORK.sendToServer(new Movement(movements, EnumMovementType.Custom));
        }
        this.strafe = 0;
        this.forward = 0;
        this.jump = 0;
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }
}

