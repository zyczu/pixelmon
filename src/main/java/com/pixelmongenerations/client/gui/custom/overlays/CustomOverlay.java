/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.custom.overlays;

import com.pixelmongenerations.client.gui.custom.overlays.CustomNoticeOverlay;
import com.pixelmongenerations.client.gui.custom.overlays.CustomScoreboardOverlay;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class CustomOverlay {
    private Minecraft minecraft = Minecraft.getMinecraft();

    @SubscribeEvent
    public void onDrawOverlay(RenderGameOverlayEvent.Post event) {
        ScaledResolution scaledResolution = new ScaledResolution(this.minecraft);
        if (event.getType() != RenderGameOverlayEvent.ElementType.ALL) {
            return;
        }
        if (CustomNoticeOverlay.isEnabled() && !this.minecraft.gameSettings.keyBindPlayerList.isKeyDown()) {
            CustomNoticeOverlay.draw(scaledResolution);
        }
        if (CustomScoreboardOverlay.isEnabled()) {
            CustomScoreboardOverlay.draw(scaledResolution);
        }
    }
}

