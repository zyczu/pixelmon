/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.machines.mechanicalanvil;

import com.pixelmongenerations.client.gui.machines.mechanicalanvil.SlotObeyTE;
import com.pixelmongenerations.common.block.tileEntities.TileEntityMechanicalAnvil;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnaceOutput;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContainerMechanicalAnvil
extends Container {
    private TileEntityMechanicalAnvil tileMechanicalAnvil;
    private int lastHammerTime;
    private int lastRunTime;
    private int lastItemRunTime;

    public ContainerMechanicalAnvil(InventoryPlayer inventoryPlayer, TileEntityMechanicalAnvil tileMechanicalAnvil) {
        int i;
        this.tileMechanicalAnvil = tileMechanicalAnvil;
        this.addSlotToContainer(new SlotObeyTE(tileMechanicalAnvil, 0, 56, 17));
        this.addSlotToContainer(new SlotObeyTE(tileMechanicalAnvil, 1, 56, 53));
        this.addSlotToContainer(new SlotFurnaceOutput(inventoryPlayer.player, tileMechanicalAnvil, 2, 116, 35));
        for (i = 0; i < 3; ++i) {
            for (int j = 0; j < 9; ++j) {
                this.addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }
        for (i = 0; i < 9; ++i) {
            this.addSlotToContainer(new Slot(inventoryPlayer, i, 8 + i * 18, 142));
        }
    }

    @Override
    public void detectAndSendChanges() {
        super.detectAndSendChanges();
        for (Object crafter : this.listeners) {
            IContainerListener icrafting = (IContainerListener)crafter;
            if (this.lastHammerTime != this.tileMechanicalAnvil.anvilHammerTime) {
                icrafting.sendWindowProperty(this, 0, this.tileMechanicalAnvil.anvilHammerTime);
            }
            if (this.lastRunTime != this.tileMechanicalAnvil.anvilRunTime) {
                icrafting.sendWindowProperty(this, 1, this.tileMechanicalAnvil.anvilRunTime);
            }
            if (this.lastItemRunTime == this.tileMechanicalAnvil.currentItemRunTime) continue;
            icrafting.sendWindowProperty(this, 2, this.tileMechanicalAnvil.currentItemRunTime);
        }
        this.lastHammerTime = this.tileMechanicalAnvil.anvilHammerTime;
        this.lastRunTime = this.tileMechanicalAnvil.anvilRunTime;
        this.lastItemRunTime = this.tileMechanicalAnvil.currentItemRunTime;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void updateProgressBar(int p_75137_1_, int p_75137_2_) {
        if (p_75137_1_ == 0) {
            this.tileMechanicalAnvil.anvilHammerTime = p_75137_2_;
        }
        if (p_75137_1_ == 1) {
            this.tileMechanicalAnvil.anvilRunTime = p_75137_2_;
        }
        if (p_75137_1_ == 2) {
            this.tileMechanicalAnvil.currentItemRunTime = p_75137_2_;
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer player) {
        return this.tileMechanicalAnvil.isUsableByPlayer(player);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slotIndex) {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(slotIndex);
        if (slot != null && slot.getHasStack()) {
            ItemStack slotStack = slot.getStack();
            itemstack = slotStack.copy();
            if (slotIndex == 2) {
                if (!this.mergeItemStack(slotStack, 3, 39, true)) {
                    return ItemStack.EMPTY;
                }
                slot.onSlotChange(slotStack, itemstack);
            } else if (slotIndex != 1 && slotIndex != 0 ? (!TileEntityMechanicalAnvil.getHammerResult(slotStack).isEmpty() ? !this.mergeItemStack(slotStack, 0, 1, false) : (TileEntityFurnace.isItemFuel(slotStack) ? !this.mergeItemStack(slotStack, 1, 2, false) : (slotIndex < 30 ? !this.mergeItemStack(slotStack, 30, 39, false) : slotIndex < 39 && !this.mergeItemStack(slotStack, 3, 30, false)))) : !this.mergeItemStack(slotStack, 3, 39, false)) {
                return ItemStack.EMPTY;
            }
            if (slotStack.getCount() == 0) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
            if (slotStack.getCount() == itemstack.getCount()) {
                return ItemStack.EMPTY;
            }
            slot.onTake(player, slotStack);
        } else {
            itemstack = ItemStack.EMPTY;
        }
        return itemstack;
    }
}

