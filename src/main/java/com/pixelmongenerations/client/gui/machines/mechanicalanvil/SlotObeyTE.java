/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.machines.mechanicalanvil;

import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotObeyTE
extends Slot {
    public SlotObeyTE(ISidedInventory inventory, int slotID, int x, int y) {
        super(inventory, slotID, x, y);
    }

    @Override
    public boolean isItemValid(ItemStack itemStack) {
        return this.inventory.isItemValidForSlot(this.getSlotIndex(), itemStack);
    }
}

