/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.ClientTradingManager;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonStatsData;
import com.pixelmongenerations.core.network.packetHandlers.trading.ServerTrades;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.RegexPatterns;
import java.io.IOException;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import org.lwjgl.opengl.GL11;

public class GuiTrading
extends GuiContainer {
    public boolean ready = false;
    private int selected = -1;
    private BlockPos tradeMachine;
    PixelmonData[] p = ServerStorageDisplay.getPokemon();

    public GuiTrading(int x, int y, int z) {
        super(new ContainerEmpty());
        this.tradeMachine = new BlockPos(x, y, z);
    }

    @Override
    public void onGuiClosed() {
        Pixelmon.NETWORK.sendToServer(ServerTrades.getDeRegisterPacket(this.tradeMachine));
        ClientTradingManager.reset();
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
        PixelmonData[] party;
        ScaledResolution var5 = new ScaledResolution(this.mc);
        var5.getScaledWidth();
        var5.getScaledHeight();
        this.mc.renderEngine.bindTexture(GuiResources.tradeGui);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.drawTexturedModalRect((this.width - 256) / 2, (this.height - 206) / 2, 0, 0, 256, 204);
        for (PixelmonData p : party = ServerStorageDisplay.getPokemon()) {
            if (p == null) continue;
            I18n.format("pixelmon." + p.name.toLowerCase() + ".name", new Object[0]);
            int i = p.order;
            GuiHelper.bindPokemonSprite(p, this.mc);
            GuiHelper.drawImageQuad(this.width / 2 - 93 + 25 * i, this.height / 2 + 68, 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            if (p.heldItem.isEmpty()) continue;
            this.mc.renderEngine.bindTexture(GuiResources.heldItem);
            GuiHelper.drawImageQuad(this.width / 2 - 97 + 25 * i + 18, this.height / 2 + 68 + 18, 6.0, 6.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        }
        this.drawPlayer(this.mc.player, 0, 0);
        this.drawPlayer(ClientTradingManager.tradePartner, 270, 90);
        GlStateManager.pushMatrix();
        GlStateManager.scale(0.5f, 0.5f, 0.0f);
        PixelmonData playerPokemon = null;
        if (this.selected != -1) {
            playerPokemon = party[this.selected];
        }
        this.drawStats(ClientTradingManager.selectedStats, playerPokemon, 0);
        this.drawStats(ClientTradingManager.tradeTargetStats, ClientTradingManager.tradeTarget, 270);
        GlStateManager.popMatrix();
        this.drawPokemonDetails(playerPokemon, -117, -212, -180, -75);
        this.drawPokemonDetails(ClientTradingManager.tradeTarget, 18, 58, 90, 15);
        this.drawString(this.mc.fontRenderer, I18n.format("gui.trading.ready", new Object[0]), (this.width + 130) / 2, (this.height + 157) / 2, 0xFFFFFF);
        this.drawButtonReady(var2, var3);
        if (ClientTradingManager.player1Ready && ClientTradingManager.player2Ready) {
            this.drawString(this.mc.fontRenderer, I18n.format("gui.trading.trade", new Object[0]), (this.width - 30) / 2, (this.height + 38) / 2, 0xFFFFFF);
            this.drawButtonTrade(var2, var3);
        } else {
            this.drawString(this.mc.fontRenderer, I18n.format("gui.trading.notready", new Object[0]), (this.width - 45) / 2, (this.height + 38) / 2, 0xFFFFFF);
        }
        this.drawPokemonSelection(var2, var3);
        GlStateManager.pushMatrix();
        GlStateManager.scale(0.5f, 0.5f, 0.0f);
        this.drawString(this.mc.fontRenderer, RegexPatterns.$_D_VAR.matcher(I18n.format("gui.trading.want", new Object[0])).replaceAll(I18n.format(this.mc.player.getDisplayName().getFormattedText(), new Object[0])), this.width - 235, this.height - 178, 0xFFFFFF);
        if (ClientTradingManager.tradePartner != null) {
            this.drawString(this.mc.fontRenderer, RegexPatterns.$_D_VAR.matcher(I18n.format("gui.trading.want", new Object[0])).replaceAll(I18n.format(ClientTradingManager.tradePartner.getName(), new Object[0])), this.width + 35, this.height - 178, 0xFFFFFF);
        } else {
            this.drawString(this.mc.fontRenderer, I18n.format("gui.trading.nuf", new Object[0]), this.width + 35, this.height - 178, 0xFFFFFF);
        }
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        this.mc.renderEngine.bindTexture(GuiResources.tradeGui);
        if (ClientTradingManager.tradePartner != null && !ClientTradingManager.player2Ready) {
            GlStateManager.color(1.0f, 0.0f, 0.0f);
            this.drawTexturedModalRect((this.width + 45) / 2, (this.height + 85) / 2, 61, 242, 90, 14);
        } else if (!ClientTradingManager.player2Ready) {
            GlStateManager.color(1.0f, 0.0f, 0.0f);
            this.drawTexturedModalRect((this.width + 65) / 2, (this.height + 85) / 2, 153, 242, 72, 14);
        } else if (ClientTradingManager.player2Ready) {
            GlStateManager.color(0.0f, 1.0f, 0.0f);
            this.drawTexturedModalRect((this.width + 75) / 2, (this.height + 85) / 2, 1, 242, 58, 14);
        }
        if (!ClientTradingManager.player1Ready) {
            GlStateManager.color(1.0f, 0.0f, 0.0f);
            this.drawTexturedModalRect((this.width - 225) / 2, (this.height + 85) / 2, 61, 242, 90, 14);
        } else if (ClientTradingManager.player1Ready) {
            GlStateManager.color(0.0f, 1.0f, 0.0f);
            this.drawTexturedModalRect((this.width - 195) / 2, (this.height + 85) / 2, 1, 242, 58, 14);
        }
        GlStateManager.popMatrix();
        if (this.selected != -1) {
            GlStateManager.pushMatrix();
            GlStateManager.color(0.0f, 1.0f, 0.0f);
            this.mc.renderEngine.bindTexture(GuiResources.tradeGui);
            this.drawTexturedModalRect((this.width - 190 + this.selected * 50) / 2, (this.height + 140) / 2, 1, 206, 26, 24);
            GlStateManager.popMatrix();
        }
        this.drawButtonClose(var2, var3);
        GlStateManager.pushMatrix();
        GlStateManager.scale(0.5f, 0.5f, 0.0f);
        this.mc.fontRenderer.drawSplitString(I18n.format("gui.trading.select", new Object[0]), (this.width / 2 - 118) * 2, (this.height / 2 + 77) * 2, 50, 0xFFFFFF);
        GlStateManager.popMatrix();
    }

    private void drawPlayer(EntityPlayer player, int playerOffset, int questionOffset) {
        if (player != null) {
            GuiHelper.drawEntity(player, (this.width - 210 + playerOffset) / 2, (this.height - 82) / 2, 20.0f, 0.0f, 0.0f);
        } else {
            GlStateManager.pushMatrix();
            this.mc.renderEngine.bindTexture(GuiResources.tradeGui);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GlStateManager.scale(1.5f, 1.5f, 0.0f);
            this.drawTexturedModalRect(this.width - 75 + questionOffset, this.height - 265, 227, 242, 10, 14);
            GlStateManager.popMatrix();
        }
    }

    private void drawStats(PixelmonStatsData stats, PixelmonData pokemon, int xOffset) {
        String[] statNames = new String[]{"hp", "attack", "defense", "spattack", "spdefense", "speed"};
        int[] statHeights = new int[6];
        int textPosition = this.width - 183 + xOffset;
        for (int i = 0; i < statNames.length; ++i) {
            int statHeight;
            String statLang = "nbt." + statNames[i] + ".name";
            statHeights[i] = statHeight = this.height - 156 + 16 * i;
            this.drawString(this.mc.fontRenderer, I18n.format(statLang, new Object[0]), textPosition, statHeight, 0xFFFFFF);
        }
        int statPosition = this.width - 54 + xOffset;
        boolean isEgg = pokemon != null && pokemon.isEgg;
        boolean bl = isEgg;
        if (isEgg || stats == null) {
            String questionMarks = I18n.format("???", new Object[0]);
            for (int statHeight : statHeights) {
                this.drawString(this.mc.fontRenderer, questionMarks, statPosition, statHeight, 0xFFFFFF);
            }
        } else {
            int[] statValues = new int[]{stats.HP, stats.Attack, stats.Defence, stats.SpecialAttack, stats.SpecialDefence, stats.Speed};
            for (int i = 0; i < statValues.length; ++i) {
                this.drawString(this.mc.fontRenderer, String.valueOf(statValues[i]), statPosition, statHeights[i], 0xFFFFFF);
            }
        }
    }

    private void drawPokemonDetails(PixelmonData p, int spriteOffset, int textOffset, int descriptionOffset, int emptyOffset) {
        if (p != null) {
            String descriptionLang;
            String nameText;
            String levelText;
            GuiHelper.bindPokemonSprite(p, this.mc);
            GuiHelper.drawImageQuad(this.width / 2 + spriteOffset, this.height / 2 - 33, 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            if (!p.heldItem.isEmpty()) {
                this.mc.renderEngine.bindTexture(GuiResources.heldItem);
                GuiHelper.drawImageQuad(this.width / 2 + spriteOffset + 18, this.height / 2 - 33 + 18, 6.0, 6.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            }
            boolean hasST = false;
            if (p.customTexture != null && !p.customTexture.isEmpty() || p.specialTexture != 0 && !p.isEgg) {
                this.mc.renderEngine.bindTexture(GuiResources.st);
                GuiHelper.drawImageQuad(this.width / 2 + spriteOffset - 3, this.height / 2 - 16, 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                hasST = true;
            }
            if (p.isShiny && !p.isEgg) {
                this.mc.renderEngine.bindTexture(GuiResources.shiny);
                GuiHelper.drawImageQuad(this.width / 2 + spriteOffset - 3, this.height / 2 - 16 - (hasST ? 7 : 0), 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            }
            GlStateManager.pushMatrix();
            GlStateManager.scale(0.5f, 0.5f, 0.0f);
            if (p.isEgg) {
                levelText = "???";
                nameText = I18n.format("pixelmon.egg.name", new Object[0]);
                descriptionLang = "gui.trader.eggDescription";
            } else {
                levelText = " " + p.lvl;
                nameText = p.getNickname();
                descriptionLang = "pixelmon." + p.name.toLowerCase() + ".description";
            }
            this.drawCenteredString(this.mc.fontRenderer, I18n.format("gui.screenpokechecker.lvl", new Object[0]) + levelText, this.width + textOffset, this.height - 62, 0xFFFFFF);
            this.drawCenteredString(this.mc.fontRenderer, nameText, this.width + textOffset, this.height - 10, 0xFFFFFF);
            this.mc.fontRenderer.drawSplitString(I18n.format(descriptionLang, new Object[0]), this.width + descriptionOffset, this.height - 55, 150, 0xFFFFFF);
            GlStateManager.popMatrix();
        } else {
            GlStateManager.pushMatrix();
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GlStateManager.scale(1.5f, 1.5f, 0.0f);
            this.mc.renderEngine.bindTexture(GuiResources.tradeGui);
            this.drawTexturedModalRect(this.width / 3 + emptyOffset, this.height / 3 - 20, 227, 242, 10, 14);
            GlStateManager.popMatrix();
        }
    }

    public int drawPokemonSelection(int par1, int par2) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.mc.renderEngine.bindTexture(GuiResources.tradeGui);
        if (par1 >= (this.width - 190) / 2 && par1 <= (this.width - 140) / 2 && par2 >= (this.height + 140) / 2 && par2 <= (this.height + 190) / 2 && this.p[0] != null) {
            this.drawTexturedModalRect((this.width - 190) / 2, (this.height + 140) / 2, 1, 206, 26, 24);
            return 0;
        }
        if (par1 >= (this.width - 140) / 2 && par1 <= (this.width - 90) / 2 && par2 >= (this.height + 140) / 2 && par2 <= (this.height + 190) / 2 && this.p[1] != null) {
            this.drawTexturedModalRect((this.width - 140) / 2, (this.height + 140) / 2, 1, 206, 26, 24);
            return 1;
        }
        if (par1 >= (this.width - 90) / 2 && par1 <= (this.width - 40) / 2 && par2 >= (this.height + 140) / 2 && par2 <= (this.height + 190) / 2 && this.p[2] != null) {
            this.drawTexturedModalRect((this.width - 90) / 2, (this.height + 140) / 2, 1, 206, 26, 24);
            return 2;
        }
        if (par1 >= (this.width - 40) / 2 && par1 <= (this.width + 10) / 2 && par2 >= (this.height + 140) / 2 && par2 <= (this.height + 190) / 2 && this.p[3] != null) {
            this.drawTexturedModalRect((this.width - 40) / 2, (this.height + 140) / 2, 1, 206, 26, 24);
            return 3;
        }
        if (par1 >= (this.width + 10) / 2 && par1 <= (this.width + 60) / 2 && par2 >= (this.height + 140) / 2 && par2 <= (this.height + 190) / 2 && this.p[4] != null) {
            this.drawTexturedModalRect((this.width + 10) / 2, (this.height + 140) / 2, 1, 206, 26, 24);
            return 4;
        }
        if (par1 >= (this.width + 60) / 2 && par1 <= (this.width + 110) / 2 && par2 >= (this.height + 140) / 2 && par2 <= (this.height + 190) / 2 && this.p[5] != null) {
            this.drawTexturedModalRect((this.width + 60) / 2, (this.height + 140) / 2, 1, 206, 26, 24);
            return 5;
        }
        return -1;
    }

    public boolean drawButtonTrade(int par1, int par2) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.mc.renderEngine.bindTexture(GuiResources.tradeGui);
        if (par1 >= (this.width - 72) / 2 && par1 <= (this.width + 70) / 2 && par2 >= (this.height + 26) / 2 && par2 <= (this.height + 62) / 2) {
            this.drawTexturedModalRect((this.width - 72) / 2, (this.height + 26) / 2, 28, 205, 72, 19);
            this.drawString(this.mc.fontRenderer, I18n.format("gui.trading.trade", new Object[0]), (this.width - 30) / 2, (this.height + 38) / 2, 0xFFFFA0);
            return true;
        }
        return false;
    }

    public boolean drawButtonClose(int par1, int par2) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.mc.renderEngine.bindTexture(GuiResources.tradeGui);
        if (par1 >= (this.width + 213) / 2 && par1 <= (this.width + 248) / 2 && par2 <= (this.height + 199) / 2 && par2 >= (this.height + 170) / 2) {
            this.drawTexturedModalRect((this.width + 214) / 2, (this.height + 170) / 2, 67, 225, 17, 15);
            return true;
        }
        return false;
    }

    public boolean drawButtonReady(int par1, int par2) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.mc.renderEngine.bindTexture(GuiResources.tradeGui);
        if (par1 >= (this.width + 123) / 2 && par1 <= (this.width + 197) / 2 && par2 <= (this.height + 179) / 2 && par2 >= (this.height + 148) / 2) {
            this.drawTexturedModalRect((this.width + 122) / 2, (this.height + 148) / 2, 28, 225, 38, 16);
            this.drawString(this.mc.fontRenderer, I18n.format("gui.trading.ready", new Object[0]), (this.width + 130) / 2, (this.height + 157) / 2, 0xFFFFA0);
            return true;
        }
        return false;
    }

    @Override
    protected void mouseClicked(int par1, int par2, int par3) throws IOException {
        super.mouseClicked(par1, par2, par3);
        if (this.drawButtonReady(par1, par2) && this.selected >= 0 && ClientTradingManager.tradeTarget != null) {
            this.mc.world.playSound(this.mc.player, this.mc.player.getPosition(), SoundEvents.BLOCK_LEVER_CLICK, SoundCategory.AMBIENT, 0.7f, 1.0f);
            ClientTradingManager.player1Ready = !ClientTradingManager.player1Ready;
            Pixelmon.NETWORK.sendToServer(new ServerTrades(ClientTradingManager.player1Ready, this.tradeMachine));
        }
        if (this.drawButtonTrade(par1, par2) && ClientTradingManager.player1Ready && ClientTradingManager.player2Ready) {
            this.mc.world.playSound(this.mc.player, this.mc.player.getPosition(), SoundEvents.BLOCK_LEVER_CLICK, SoundCategory.AMBIENT, 0.7f, 1.0f);
            Pixelmon.NETWORK.sendToServer(ServerTrades.getTradePacket(this.tradeMachine));
        }
        if (this.drawPokemonSelection(par1, par2) != -1) {
            this.mc.world.playSound(this.mc.player, this.mc.player.getPosition(), SoundEvents.BLOCK_LEVER_CLICK, SoundCategory.AMBIENT, 0.7f, 1.0f);
            this.selected = this.drawPokemonSelection(par1, par2);
            Pixelmon.NETWORK.sendToServer(new ServerTrades(this.selected, this.tradeMachine));
            ClientTradingManager.player1Ready = false;
            ClientTradingManager.player2Ready = false;
        }
        if (this.drawButtonClose(par1, par2)) {
            this.mc.world.playSound(this.mc.player, this.mc.player.getPosition(), SoundEvents.BLOCK_LEVER_CLICK, SoundCategory.AMBIENT, 0.7f, 1.0f);
            this.mc.player.closeScreen();
            if (!ClientProxy.battleManager.evolveList.isEmpty()) {
                this.mc.player.openGui(Pixelmon.INSTANCE, EnumGui.Evolution.getIndex(), this.mc.world, 0, 0, 0);
            }
        }
    }

    @Override
    public void drawGuiContainerForegroundLayer(int par1, int par2) {
        GL11.glNormal3f((float)0.0f, (float)-1.0f, (float)0.0f);
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        super.drawScreen(par1, par2, par3);
    }
}

