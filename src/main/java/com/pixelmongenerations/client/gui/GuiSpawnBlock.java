/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 */
package com.pixelmongenerations.client.gui;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import java.io.IOException;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import org.lwjgl.input.Keyboard;

public class GuiSpawnBlock
extends GuiContainer {
    GuiTextField tbAreaName;

    public GuiSpawnBlock() {
        super(new ContainerEmpty());
    }

    @Override
    public void initGui() {
        super.initGui();
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
    }

    @Override
    protected void keyTyped(char key, int par2) throws IOException {
        if (Keyboard.getEventKey() == 1 || Keyboard.getEventKey() == 28) {
            this.closeScreen();
            return;
        }
    }

    private void closeScreen() {
        GuiHelper.closeScreen();
        this.mc.setIngameFocus();
    }
}

