/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.cookingpot;

import com.pixelmongenerations.client.gui.cookingpot.CurryResultSlot;
import com.pixelmongenerations.client.gui.cookingpot.PredicateSlotItemHandler;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCookingPot;
import com.pixelmongenerations.common.item.CurryIngredient;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.core.config.PixelmonItems;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.oredict.OreDictionary;

public class ContainerCookingPot
extends Container {
    public final TileEntityCookingPot tileentity;

    public ContainerCookingPot(InventoryPlayer player, TileEntityCookingPot tileentity) {
        this.tileentity = tileentity;
        tileentity.markDirty();
        IItemHandler handler = tileentity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 0, 25, 19, ContainerCookingPot::isBerryOrMaxMushrooms));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 1, 44, 19, ContainerCookingPot::isBerryOrMaxMushrooms));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 2, 62, 19, ContainerCookingPot::isBerryOrMaxMushrooms));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 3, 80, 37, ContainerCookingPot::isBerryOrMaxMushrooms));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 4, 80, 55, ContainerCookingPot::isBerryOrMaxMushrooms));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 5, 62, 73, ContainerCookingPot::isBerryOrMaxMushrooms));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 6, 44, 73, ContainerCookingPot::isBerryOrMaxMushrooms));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 7, 26, 73, ContainerCookingPot::isBerryOrMaxMushrooms));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 8, 8, 55, ContainerCookingPot::isBerryOrMaxMushrooms));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 9, 8, 37, ContainerCookingPot::isBerryOrMaxMushrooms));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 10, 35, 46, ContainerCookingPot::isBowl));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 11, 53, 46, ContainerCookingPot::isCurryIngredientOrMaxHoney));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 12, 108, 68, ContainerCookingPot::islog));
        this.addSlotToContainer(new CurryResultSlot(handler, 13, 142, 46));
        for (int y = 0; y < 3; ++y) {
            for (int x = 0; x < 9; ++x) {
                this.addSlotToContainer(new Slot(player, x + y * 9 + 9, 8 + x * 18, 84 + y * 18 + 11 + 9));
            }
        }
        for (int x = 0; x < 9; ++x) {
            this.addSlotToContainer(new Slot(player, x, 8 + x * 18, 162));
        }
    }

    private static boolean isBowl(ItemStack itemStack) {
        return itemStack.getItem() == Items.BOWL;
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return this.tileentity.isUsableByPlayer(playerIn);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
        ItemStack stack = ItemStack.EMPTY;
        Slot slot = (Slot)this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack slotStack = slot.getStack();
            stack = slotStack.copy();
            if (index < 14) {
                if (!this.mergeItemStack(slotStack, 14, 49, false)) {
                    return ItemStack.EMPTY;
                }
                if (index == 13) {
                    slot.onSlotChange(slotStack, stack);
                }
            } else if (index < 50 && (ContainerCookingPot.isBerryOrMaxMushrooms(stack) ? !this.mergeItemStack(slotStack, 0, 10, false) : (ContainerCookingPot.isBowl(stack) ? !this.mergeItemStack(slotStack, 10, 11, false) : (ContainerCookingPot.isCurryIngredientOrMaxHoney(stack) ? !this.mergeItemStack(slotStack, 11, 12, false) : ContainerCookingPot.islog(stack) && !this.mergeItemStack(slotStack, 12, 13, false))))) {
                return ItemStack.EMPTY;
            }
            if (slotStack.getCount() == 0) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
            if (slotStack.getCount() == stack.getCount()) {
                return ItemStack.EMPTY;
            }
            slot.onTake(playerIn, stack);
        }
        return stack;
    }

    public static boolean islog(ItemStack stack) {
        int[] ids;
        for (int id : ids = OreDictionary.getOreIDs(stack)) {
            String oreName = OreDictionary.getOreName(id);
            if (!oreName.startsWith("logWood")) continue;
            return true;
        }
        return false;
    }

    public static boolean isBerryOrMaxMushrooms(ItemStack stack) {
        return stack.getItem() instanceof ItemBerry || stack.getItem() == PixelmonItems.maxMushrooms;
    }

    public static boolean isCurryIngredientOrMaxHoney(ItemStack stack) {
        return stack.getItem() instanceof CurryIngredient || stack.getItem() == PixelmonItems.maxHoney;
    }
}

