/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.cookingpot;

import com.pixelmongenerations.client.gui.cookingpot.ContainerCookingPot;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCookingPot;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.cookingpot.StartCooking;
import java.io.IOException;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiCookingPot
extends GuiContainer {
    public static final ResourceLocation cookingPotGuiTextures = new ResourceLocation("pixelmon:textures/gui/cooking_pot.png");
    private TileEntityCookingPot tileCookingPot;

    public GuiCookingPot(InventoryPlayer inventoryPlayer, TileEntityCookingPot tileCookingPot) {
        super(new ContainerCookingPot(inventoryPlayer, tileCookingPot));
        this.tileCookingPot = tileCookingPot;
        this.ySize = 186;
    }

    @Override
    public void initGui() {
        super.initGui();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        this.fontRenderer.drawString("Cooking Pot", this.xSize / 2 - this.fontRenderer.getStringWidth("Cooking Pot") / 2, 6, 0x404040);
        this.fontRenderer.drawString(I18n.format("container.inventory", new Object[0]), 8, this.ySize - 94, 0x404040);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.mc.getTextureManager().bindTexture(cookingPotGuiTextures);
        this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
        int l = this.getCookProgressScaled(24);
        this.drawTexturedModalRect(this.guiLeft + 105, this.guiTop + 24, 176, 18, l + 1, 16);
        if (this.tileCookingPot.isCooking()) {
            this.drawTexturedModalRect(this.guiLeft + 106, this.guiTop + 45, 176, 0, 20, 18);
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        if (GuiCookingPot.findPoint(this.guiLeft + 106, this.guiTop + 45, this.guiLeft + 126, this.guiTop + 63, mouseX, mouseY)) {
            Pixelmon.NETWORK.sendToServer(new StartCooking(this.tileCookingPot));
        }
    }

    public static boolean findPoint(int x1, int y1, int x2, int y2, int x, int y) {
        return x > x1 && x < x2 && y > y1 && y < y2;
    }

    private int getCookProgressScaled(int pixels) {
        int i = this.tileCookingPot.getCookTime();
        return i != 0 ? i * pixels / 200 : 0;
    }
}

