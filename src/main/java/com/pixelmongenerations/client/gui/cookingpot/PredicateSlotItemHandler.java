/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.cookingpot;

import java.util.function.Predicate;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import org.jetbrains.annotations.NotNull;

public class PredicateSlotItemHandler
extends SlotItemHandler {
    private Predicate<ItemStack> predicate;

    public PredicateSlotItemHandler(IItemHandler itemHandler, int index, int xPosition, int yPosition, Predicate<ItemStack> predicate) {
        super(itemHandler, index, xPosition, yPosition);
        this.predicate = predicate;
    }

    @Override
    public boolean isItemValid(@NotNull ItemStack stack) {
        return this.predicate.test(stack);
    }
}

