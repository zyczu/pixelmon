/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui;

import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonStatsData;
import java.util.List;
import java.util.UUID;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;

public class ClientTradingManager {
    public static EntityPlayer tradePartner;
    public static PixelmonData tradeTarget;
    public static PixelmonStatsData tradeTargetStats;
    public static PixelmonStatsData selectedStats;
    public static boolean player1Ready;
    public static boolean player2Ready;

    public static void findTradePartner(UUID uuid) {
        tradePartner = null;
        if (uuid != null) {
            List<EntityPlayer> playerList = Minecraft.getMinecraft().world.playerEntities;
            playerList.stream().filter(p -> p.getUniqueID().equals(uuid)).forEach(p -> {
                tradePartner = p;
            });
        }
    }

    public static void reset() {
        tradePartner = null;
        tradeTarget = null;
        tradeTargetStats = null;
        selectedStats = null;
        player1Ready = false;
        player2Ready = false;
    }

    static {
        player1Ready = false;
        player2Ready = false;
    }
}

