/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui;

import com.pixelmongenerations.client.SoundHelper;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumShinyItem;
import com.pixelmongenerations.core.event.EntityPlayerExtension;
import com.pixelmongenerations.core.network.packetHandlers.SetShinyItem;
import com.pixelmongenerations.core.storage.ClientData;
import com.pixelmongenerations.core.util.PixelSounds;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import org.lwjgl.opengl.GL11;

public class GuiShinyItem
extends GuiContainer {
    private boolean firstTime;

    public GuiShinyItem(boolean firstTime) {
        super(new ContainerEmpty());
        this.firstTime = firstTime;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float renderPartialTicks, int mouseX, int mouseY) {
        if (this.firstTime) {
            this.mc.renderEngine.bindTexture(GuiResources.shinyCharmUnlock);
            int imageWidth = (int)((double)this.width / 1.9);
            int imageHeight = (int)((double)this.height / 1.9);
            GuiHelper.drawImageQuad(this.width / 2 - imageWidth / 2, this.height / 2 - imageHeight / 2, imageWidth, imageHeight, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        } else {
            this.mc.renderEngine.bindTexture(GuiResources.shinyCharmEquip);
            int bgImageWidth = (int)((double)this.width / 1.9);
            int bgImageHeight = (int)((double)this.height / 1.9);
            GuiHelper.drawImageQuad(this.width / 2 - bgImageWidth / 2, this.height / 2 - bgImageHeight / 2, bgImageWidth, bgImageHeight, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            int bImageWidth = (int)((double)bgImageWidth / 4.75);
            int bImageHeight = (int)((double)bgImageHeight / 7.5);
            int bImageX = this.width / 2 - bImageWidth / 2;
            int bImageY = (int)((double)this.height / 1.43 - (double)(bImageHeight / 2));
            EnumShinyItem shinyItem = EntityPlayerExtension.getPlayerShinyItem(Minecraft.getMinecraft().player);
            if (shinyItem != EnumShinyItem.Disabled && shinyItem != EnumShinyItem.None) {
                this.mc.renderEngine.bindTexture(GuiResources.buttonUnequip);
            } else {
                this.mc.renderEngine.bindTexture(GuiResources.buttonEquip);
            }
            if (mouseX >= bImageX && mouseX <= bImageX + bImageWidth && mouseY >= bImageY && mouseY <= bImageY + bImageHeight) {
                GL11.glColor3f((float)0.8f, (float)0.8f, (float)0.8f);
            }
            GuiHelper.drawImageQuad(bImageX, bImageY, bImageWidth, bImageHeight, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            GL11.glColor3f((float)1.0f, (float)1.0f, (float)1.0f);
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        int bgImageWidth = (int)((double)this.width / 1.9);
        int bgImageHeight = (int)((double)this.height / 1.9);
        int bImageWidth = (int)((double)bgImageWidth / 4.75);
        int bImageHeight = (int)((double)bgImageHeight / 7.5);
        int bImageX = this.width / 2 - bImageWidth / 2;
        int bImageY = (int)((double)this.height / 1.43 - (double)(bImageHeight / 2));
        if (mouseX >= bImageX && mouseX <= bImageX + bImageWidth && mouseY >= bImageY && mouseY <= bImageY + bImageHeight) {
            EnumShinyItem shinyItem = EntityPlayerExtension.getPlayerShinyItem(Minecraft.getMinecraft().player);
            if (shinyItem != EnumShinyItem.Disabled && shinyItem != EnumShinyItem.None) {
                Pixelmon.NETWORK.sendToServer(new SetShinyItem(EnumShinyItem.None));
                this.mc.player.closeScreen();
            } else {
                Pixelmon.NETWORK.sendToServer(new SetShinyItem(EnumShinyItem.ShinyCharm));
                this.mc.player.closeScreen();
            }
            SoundHelper.playSound(PixelSounds.ui_click);
        }
    }

    @Override
    public void onGuiClosed() {
        ClientData.openShinyItemGui = false;
        super.onGuiClosed();
    }
}

