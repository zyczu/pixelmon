/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.ranchblock;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.ranch.EnumRanchServerPacketMode;
import com.pixelmongenerations.core.network.packetHandlers.ranch.RanchBlockServerPacket;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.translation.I18n;

public class GuiRanchBlock
extends GuiContainer {
    public static int[] pos;
    public static ArrayList<PixelmonData> pokemon;
    public static PixelmonData egg;

    public GuiRanchBlock() {
        super(new ContainerEmpty());
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.add(new GuiButton(0, this.width / 2 - 50, this.height * 2 / 3 - 40, 100, 20, I18n.translateToLocal("gui.ranch.managePokemon")));
        if (egg != null) {
            this.buttonList.add(new GuiButton(2, this.width / 2 - 20, this.height * 2 / 3, 110, 20, I18n.translateToLocal("gui.ranch.claimEgg")));
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float par3, int par1, int par2) {
        if (pokemon.isEmpty()) {
            String s = I18n.translateToLocal("gui.ranch.empty");
            this.fontRenderer.drawString(s, this.width / 2 - this.fontRenderer.getStringWidth(s) / 2, this.height / 3 - 45, 0xFFFFFF);
        }
        for (int i = 0; i < pokemon.size(); ++i) {
            boolean isGen6Sprite = pokemon.get(i).isGen6Sprite();
            if (i < 3) {
                GuiHelper.bindPokemonSprite(pokemon.get(i), this.mc);
                GuiHelper.drawImageQuad(this.width / 2 - 100 + i * 75, this.height / 3 - 65 - (isGen6Sprite ? 3 : 0), 50.0, 50.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                continue;
            }
            GuiHelper.bindPokemonSprite(pokemon.get(i), this.mc);
            GuiHelper.drawImageQuad(this.width / 2 - 100 + (i - 3) * 75, this.height / 3 - 15 - (isGen6Sprite ? 3 : 0), 50.0, 50.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        }
        Minecraft.getMinecraft().world.getTileEntity(new BlockPos(pos[0], pos[1], pos[2]));
        if (egg != null) {
            GuiHelper.bindPokemonSprite(egg, this.mc);
            GuiHelper.drawImageQuad(this.width / 2 - 100, this.height * 2 / 3 - 40, 80.0, 80.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        }
    }

    @Override
    public void actionPerformed(GuiButton button) {
        int b = button.id;
        if (b == 0) {
            this.mc.player.openGui(Pixelmon.INSTANCE, EnumGui.PCNoParty.getIndex(), this.mc.world, 0, 0, 0);
        }
        if (b == 2 && egg != null) {
            Pixelmon.NETWORK.sendToServer(new RanchBlockServerPacket(pos[0], pos[1], pos[2], EnumRanchServerPacketMode.CollectEgg));
            this.mc.player.closeScreen();
        }
    }
}

