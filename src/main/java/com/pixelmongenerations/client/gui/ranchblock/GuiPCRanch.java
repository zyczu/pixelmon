/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.ranchblock;

import com.pixelmongenerations.client.gui.pc.GuiPC;
import com.pixelmongenerations.client.gui.ranchblock.GuiRanchBlock;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.ranch.EnumRanchServerPacketMode;
import com.pixelmongenerations.core.network.packetHandlers.ranch.RanchBlockServerPacket;
import com.pixelmongenerations.core.storage.PCClientStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.List;

public class GuiPCRanch
extends GuiPC {
    @Override
    protected PixelmonData getPartyAt(double index) {
        int xInd = (int)Math.floor(index);
        if (xInd >= GuiRanchBlock.pokemon.size()) {
            return null;
        }
        return GuiRanchBlock.pokemon.get(xInd);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
    }

    @Override
    protected void mouseClicked(int x, int y, int par3) throws IOException {
        PixelmonData p = this.getPCAt(x, y);
        if (!this.searchText.getText().isEmpty()) {
            List<PixelmonData> matches = this.searchPokemon(this.searchText.getText());
            double xInd = ((double)x - (double)this.pcLeft) / (double)this.slotWidth;
            double yInd = ((double)y - ((double)this.pcTop + 5.0)) / (double)this.slotHeight;
            int ind = this.boxNumber * PlayerComputerStorage.boxCount + (int)Math.floor(yInd) * 6 + (int)Math.floor(xInd);
            PixelmonData pixelmonData = p = ind < matches.size() && ind >= 0 && yInd < 5.0 && xInd < 6.0 ? matches.get(ind) : null;
        }
        if (p != null && !p.isEgg && p.boxNumber != -1 && !p.isInRanch && GuiRanchBlock.pokemon.size() < 6 && p.isBreedable()) {
            p.isInRanch = true;
            Pixelmon.NETWORK.sendToServer(new RanchBlockServerPacket(GuiRanchBlock.pos[0], GuiRanchBlock.pos[1], GuiRanchBlock.pos[2], p.pokemonID, EnumRanchServerPacketMode.AddPokemon));
            GuiRanchBlock.pokemon.add(p);
        }
        if ((p = this.getPartyAt(x, y)) != null && !p.isEgg) {
            p.isInRanch = false;
            PixelmonData pcP = this.pcClient.getByID(p.pokemonID);
            if (pcP != null) {
                pcP.isInRanch = false;
            }
            Pixelmon.NETWORK.sendToServer(new RanchBlockServerPacket(GuiRanchBlock.pos[0], GuiRanchBlock.pos[1], GuiRanchBlock.pos[2], p.pokemonID, EnumRanchServerPacketMode.RemovePokemon));
            GuiRanchBlock.pokemon.remove(p);
        }
        int xSizeBox = 226;
        int ySizeBox = 207;
        int xSizeParty = 103;
        int pcBoxTopLeft = this.width / 2 - xSizeBox / 2 + xSizeParty / 2;
        int ySizeParty = 207;
        int pcBoxTop = (int)((double)this.height * 0.45 - (double)(ySizeParty / 2));
        if (new Rectangle(pcBoxTopLeft + 2, pcBoxTop, 23, 32).contains(x, y)) {
            this.boxNumber = this.boxNumber == 0 ? PlayerComputerStorage.boxCount - 1 : (this.boxNumber = this.boxNumber - 1);
            this.mc.fontRenderer.drawString(PCClientStorage.getBoxName(this.boxNumber), this.width / 2 - 33, this.height / 6 - 20, 0xFFFFFF);
        } else if (new Rectangle(pcBoxTopLeft + 201, pcBoxTop, 23, 32).contains(x, y)) {
            this.boxNumber = this.boxNumber == PlayerComputerStorage.boxCount - 1 ? 0 : (this.boxNumber = this.boxNumber + 1);
            this.mc.fontRenderer.drawString(PCClientStorage.getBoxName(this.boxNumber), this.width / 2 - 33, this.height / 6 - 20, 0xFFFFFF);
        }
    }

    @Override
    protected void mouseReleased(int mouseX, int mouseY, int state) {
    }

    @Override
    protected int getPartySize() {
        return GuiRanchBlock.pokemon.size();
    }

    @Override
    protected PixelmonData getPartyPokemon(int pos) {
        return GuiRanchBlock.pokemon.get(pos);
    }
}

