/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.trashcan;

import com.pixelmongenerations.common.block.tileEntities.TileEntityTrashcan;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;

public class ContainerTrashcan
extends Container {
    public ContainerTrashcan(InventoryPlayer player) {
        this.addSlotToContainer(new SlotItemHandler(TileEntityTrashcan.VoidItemStackHandler.instance, 0, 80, 35));
        for (int y = 0; y < 3; ++y) {
            for (int x = 0; x < 9; ++x) {
                this.addSlotToContainer(new Slot(player, x + y * 9 + 9, 8 + x * 18, 84 + y * 18));
            }
        }
        for (int x = 0; x < 9; ++x) {
            this.addSlotToContainer(new Slot(player, x, 8 + x * 18, 142));
        }
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
        Slot sourceSlot = (Slot)this.inventorySlots.get(index);
        if (sourceSlot != null) {
            sourceSlot.putStack(ItemStack.EMPTY);
        }
        return ItemStack.EMPTY;
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return true;
    }
}

