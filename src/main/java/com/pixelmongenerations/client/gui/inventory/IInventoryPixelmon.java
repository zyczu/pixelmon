/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.inventory;

import java.util.List;
import net.minecraft.client.gui.GuiButton;

interface IInventoryPixelmon {
    public void superDrawScreen(int var1, int var2, float var3);

    public float getZLevel();

    public int getGUILeft();

    public void offsetGUILeft(int var1);

    public void subDrawGradientRect(int var1, int var2, int var3, int var4, int var5, int var6);

    public List<GuiButton> getButtonList();
}

