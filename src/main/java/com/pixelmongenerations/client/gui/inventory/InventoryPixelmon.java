/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui.inventory;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.SoundHelper;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.inventory.IInventoryPixelmon;
import com.pixelmongenerations.client.gui.inventory.SlotInventoryPixelmon;
import com.pixelmongenerations.client.gui.pokechecker.GuiPokeCheckerTabs;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.OpenEquipmentMenu;
import com.pixelmongenerations.core.network.packetHandlers.SetHeldItem;
import com.pixelmongenerations.core.storage.ClientData;
import com.pixelmongenerations.core.storage.PCClient;
import com.pixelmongenerations.core.storage.PCPos;
import com.pixelmongenerations.core.util.PixelSounds;
import java.awt.Rectangle;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.InventoryEffectRenderer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.opengl.GL11;

class InventoryPixelmon<T extends InventoryEffectRenderer> {
    private T gui;
    private int partyWidth;
    private SlotInventoryPixelmon[] pixelmonSlots;
    private boolean pixelmonMenuOpen;
    private int menuX;
    private int menuY;
    private GuiPokeCheckerTabs pMenuButtonSumm;
    private GuiPokeCheckerTabs pMenuButtonMove;
    private GuiPokeCheckerTabs pMenuButtonStat;
    private PixelmonData selected;
    private PCClient pcClient = new PCClient();
    private Rectangle buttonBounds;
    private Rectangle buttonBoundsMoves;
    private Rectangle buttonBoundsStat;
    private boolean hasActivePotionEffects;
    int ticksTillClick = 0;

    InventoryPixelmon(T gui, int partyWidth) {
        this.gui = gui;
        this.pixelmonMenuOpen = false;
        this.pixelmonSlots = new SlotInventoryPixelmon[6];
        this.pcClient.unselectAll();
        this.partyWidth = partyWidth;
    }

    private void drawButtonContainer() {
        if (this.pixelmonMenuOpen) {
            this.gui.mc.renderEngine.bindTexture(GuiResources.pokecheckerPopup);
            ((Gui)this.gui).drawTexturedModalRect(this.menuX + ((IInventoryPixelmon)this.gui).getGUILeft() - 73, this.menuY - 10, 0, 0, 67, 76);
            int xOffset = this.menuX + ((IInventoryPixelmon)this.gui).getGUILeft() - 63;
            this.pMenuButtonSumm.setXPosition(xOffset);
            this.pMenuButtonMove.setXPosition(xOffset);
            this.pMenuButtonStat.setXPosition(xOffset);
            this.buttonBounds.x = xOffset;
            this.buttonBoundsMoves.x = xOffset;
            this.buttonBoundsStat.x = xOffset;
        }
    }

    void initGui() {
        this.fillSlots();
    }

    private void fillSlots() {
        Arrays.fill(this.pixelmonSlots, null);
        for (PixelmonData p : ServerStorageDisplay.getPokemon()) {
            if (p == null) continue;
            int i = p.order;
            int x = ((IInventoryPixelmon)this.gui).getGUILeft() - this.partyWidth + 9;
            int y = ((InventoryEffectRenderer)this.gui).height / 2 + i * 18 - 65;
            this.pixelmonSlots[i] = new SlotInventoryPixelmon(x, y, p);
        }
    }

    void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.hasActivePotionEffects = ((InventoryEffectRenderer)this.gui).hasActivePotionEffects;
        boolean recipeBookVisible = this.gui instanceof GuiInventory && ((GuiInventory)this.gui).func_194310_f().isVisible();
        boolean bl = recipeBookVisible;
        if (((InventoryEffectRenderer)this.gui).hasActivePotionEffects && !recipeBookVisible) {
            ((InventoryEffectRenderer)this.gui).hasActivePotionEffects = false;
            ((IInventoryPixelmon)this.gui).superDrawScreen(mouseX, mouseY, partialTicks);
            ((InventoryEffectRenderer)this.gui).hasActivePotionEffects = true;
        } else {
            ((IInventoryPixelmon)this.gui).superDrawScreen(mouseX, mouseY, partialTicks);
        }
        if (recipeBookVisible) {
            return;
        }
        GlStateManager.disableLighting();
        if (this.pixelmonMenuOpen) {
            ((Gui)this.gui).drawCenteredString(this.gui.mc.fontRenderer, this.selected.getNickname(), this.menuX + ((IInventoryPixelmon)this.gui).getGUILeft() - 40, this.menuY - 8, 0xFFFFFF);
        } else {
            this.gui.mc.fontRenderer.setUnicodeFlag(true);
            for (SlotInventoryPixelmon s : this.pixelmonSlots) {
                if (s == null) continue;
                if (s.getBounds().contains(mouseX, mouseY)) {
                    this.drawPokemonInfo(mouseX, mouseY, s);
                }
                if (s.pokemonData.isEgg || !s.getHeldItemBounds().contains(mouseX, mouseY) || !this.heldItemQualifies(s) || this.ticksTillClick > 0) continue;
                this.gui.mc.renderEngine.bindTexture(GuiResources.pixelmonCreativeInventory);
                GuiHelper.drawImageQuad(s.heldItemX - 2, s.heldItemY - 2, 20.0, 20.0f, 0.2265625, 0.72265625, 0.3046875, 0.80078125, ((IInventoryPixelmon)this.gui).getZLevel());
                if (s.pokemonData.heldItem.isEmpty()) continue;
                int x = Math.max(s.x - 84, 0);
                ((IInventoryPixelmon)this.gui).subDrawGradientRect(x, s.y - 2, x + 60, s.y + 20, -13158600, -13158600);
                this.gui.mc.fontRenderer.drawString(s.pokemonData.heldItem.getDisplayName(), x + 2, s.y, 0xFFFFFF);
            }
            this.gui.mc.fontRenderer.setUnicodeFlag(false);
        }
    }

    void drawGuiContainerBackgroundLayer(float renderPartialTicks, int mouseX, int mouseY) {
        GlStateManager.disableLighting();
        GlStateManager.disableFog();
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.gui.mc.entityRenderer.setupOverlayRendering();
        this.gui.mc.fontRenderer.setUnicodeFlag(true);
        for (SlotInventoryPixelmon slot : this.pixelmonSlots) {
            PixelmonData p;
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            if (slot == null || (p = slot.pokemonData) == null) continue;
            int x = ((IInventoryPixelmon)this.gui).getGUILeft() - this.partyWidth + 9;
            slot.setX(x);
            GuiHelper.bindPokemonSprite(p, ((InventoryEffectRenderer)this.gui).mc);
            GlStateManager.disableLighting();
            GuiHelper.drawImageQuad(slot.x, slot.y - (p.isGen6Sprite() ? 0 : 3), 16.0, 16.0f, 0.0, 0.0, 1.0, 1.0, ((IInventoryPixelmon)this.gui).getZLevel());
            if (!p.isEgg) {
                if (p.heldItem != ItemStack.EMPTY) {
                    ((InventoryEffectRenderer)this.gui).mc.getRenderItem().renderItemIntoGUI(p.heldItem, slot.heldItemX, slot.heldItemY);
                } else {
                    this.gui.mc.renderEngine.bindTexture(GuiResources.heldItem);
                    GuiHelper.drawImageQuad(slot.heldItemX + 3, slot.heldItemY + 3, 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, ((IInventoryPixelmon)this.gui).getZLevel());
                }
            }
            if (!p.selected) continue;
            this.gui.mc.renderEngine.bindTexture(GuiResources.pcBox);
            GuiHelper.drawImageQuad(slot.x, slot.y - 1, 16.0, 17.0f, 0.0, 0.55859375, 0.12109375, 0.68359375, ((IInventoryPixelmon)this.gui).getZLevel());
        }
        int slotleft = ((IInventoryPixelmon)this.gui).getGUILeft() - this.partyWidth + 25;
        int slottop = ((InventoryEffectRenderer)this.gui).height / 2 + 49;
        int equipmentX = slotleft - 8;
        int equipmentY = slottop - 1;
        this.gui.mc.renderEngine.bindTexture(GuiResources.equipmentSprite);
        if (mouseX >= equipmentX && mouseX <= equipmentX + 17 && mouseY >= equipmentY && mouseY <= equipmentY + 17) {
            GL11.glColor3f((float)0.8f, (float)0.8f, (float)0.8f);
        }
        GuiHelper.drawImageQuad(slotleft - 7, slottop - 1, 16.0, 16.0f, 0.0, 0.0, 1.0, 1.0, ((IInventoryPixelmon)this.gui).getZLevel());
        GL11.glColor3f((float)1.0f, (float)1.0f, (float)1.0f);
        String money = NumberFormat.getInstance().format(ClientData.playerMoney);
        GuiHelper.drawStringRightAligned(money, ((IInventoryPixelmon)this.gui).getGUILeft() - this.partyWidth + 44, ((InventoryEffectRenderer)this.gui).height / 2 + 70, 0, false);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.gui.mc.fontRenderer.setUnicodeFlag(false);
        this.drawButtonContainer();
        RenderHelper.disableStandardItemLighting();
        GlStateManager.disableLighting();
        GlStateManager.depthMask(true);
        GlStateManager.enableDepth();
    }

    private boolean heldItemQualifies(SlotInventoryPixelmon s) {
        InventoryPlayer inv = this.gui.mc.player.inventory;
        ItemStack stack = inv.getItemStack();
        if (stack.isEmpty() && !s.pokemonData.heldItem.isEmpty()) {
            return true;
        }
        if (stack.isEmpty()) {
            return false;
        }
        return stack.getItem() instanceof ItemHeld;
    }

    private void drawPokemonInfo(int mouseX, int mouseY, SlotInventoryPixelmon s) {
        if (s == null) {
            return;
        }
        int x = Math.max(s.x - 84, 0);
        ((IInventoryPixelmon)this.gui).subDrawGradientRect(x, s.y - 2, x + 82, s.y + 20, -13158600, -13158600);
        PixelmonData p = s.pokemonData;
        String displayName = p.getNickname();
        this.gui.mc.fontRenderer.drawString(displayName, x + 2, s.y, 0xFFFFFF);
        this.gui.mc.renderEngine.bindTexture(GuiResources.pixelmonOverlay);
        if (!p.isEgg) {
            if (p.gender == Gender.Male) {
                Minecraft.getMinecraft().renderEngine.bindTexture(GuiResources.male);
                GuiHelper.drawImageQuad(this.gui.mc.fontRenderer.getStringWidth(displayName) + x + 3, s.y, 5.0, 8.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
            } else if (p.gender == Gender.Female) {
                Minecraft.getMinecraft().renderEngine.bindTexture(GuiResources.female);
                GuiHelper.drawImageQuad(this.gui.mc.fontRenderer.getStringWidth(displayName) + x + 3, s.y, 5.0, 8.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
            }
            String levelString = I18n.translateToLocal("gui.screenpokechecker.lvl") + " " + p.lvl;
            this.gui.mc.fontRenderer.drawString(levelString, x + 3, s.y + this.gui.mc.fontRenderer.FONT_HEIGHT + 1, 0xFFFFFF);
            if (p.health <= 0) {
                this.gui.mc.fontRenderer.drawString(I18n.translateToLocal("gui.creativeinv.fainted"), x + 7 + this.gui.mc.fontRenderer.getStringWidth(levelString), s.y + this.gui.mc.fontRenderer.FONT_HEIGHT + 1, 0xFFFFFF);
            } else {
                this.gui.mc.fontRenderer.drawString(I18n.translateToLocal("gui.creativeinv.hp") + " " + p.health + "/" + p.hp, x + 7 + this.gui.mc.fontRenderer.getStringWidth(levelString), s.y + this.gui.mc.fontRenderer.FONT_HEIGHT + 1, 0xFFFFFF);
            }
            int icons = 0;
            if (p.hasGmaxFactor) {
                this.gui.mc.renderEngine.bindTexture(GuiResources.gmaxIcon);
                GuiHelper.drawImageQuad(this.gui.mc.fontRenderer.getStringWidth(displayName) + x + 10, s.y - 1, 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                x += 9;
                ++icons;
            }
            if (p.isShiny) {
                this.gui.mc.renderEngine.bindTexture(GuiResources.shiny);
                GuiHelper.drawImageQuad(this.gui.mc.fontRenderer.getStringWidth(displayName) + x + 10, s.y - 1, 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                x += 8;
                ++icons;
            }
            if (p.customTexture != null && !p.customTexture.isEmpty() || p.specialTexture != 0) {
                this.gui.mc.renderEngine.bindTexture(GuiResources.st);
                GuiHelper.drawImageQuad(this.gui.mc.fontRenderer.getStringWidth(displayName) + x + 10, s.y - 1, 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
            }
        }
    }

    boolean mouseClicked(int x, int y, int mouseButton) throws IOException {
        List<GuiButton> buttonList = ((IInventoryPixelmon)this.gui).getButtonList();
        if (mouseButton == 0) {
            if (this.pixelmonMenuOpen && this.buttonBounds.contains(x, y)) {
                SoundHelper.playButtonPressSound();
                this.gui.mc.player.openGui(Pixelmon.INSTANCE, EnumGui.PokeChecker.getIndex(), this.gui.mc.world, this.selected.pokemonID[0], this.selected.pokemonID[1], 0);
            }
            if (this.pixelmonMenuOpen && this.buttonBoundsMoves.contains(x, y)) {
                SoundHelper.playButtonPressSound();
                this.gui.mc.player.openGui(Pixelmon.INSTANCE, EnumGui.PokeCheckerMoves.getIndex(), this.gui.mc.world, this.selected.pokemonID[0], this.selected.pokemonID[1], 0);
            }
            if (this.pixelmonMenuOpen && this.buttonBoundsStat.contains(x, y)) {
                SoundHelper.playButtonPressSound();
                this.gui.mc.player.openGui(Pixelmon.INSTANCE, EnumGui.PokeCheckerStats.getIndex(), this.gui.mc.world, this.selected.pokemonID[0], this.selected.pokemonID[1], 0);
            }
            if (this.pixelmonMenuOpen) {
                buttonList.remove(this.pMenuButtonSumm);
                buttonList.remove(this.pMenuButtonMove);
                buttonList.remove(this.pMenuButtonStat);
                this.pMenuButtonSumm = null;
                this.pMenuButtonMove = null;
                this.pMenuButtonStat = null;
                this.pixelmonMenuOpen = false;
                this.selected = null;
            }
        }
        int slotleft = ((IInventoryPixelmon)this.gui).getGUILeft() - this.partyWidth + 25;
        int slottop = ((InventoryEffectRenderer)this.gui).height / 2 + 49;
        int equipmentX = slotleft - 8;
        int equipmentY = slottop - 1;
        if (x >= equipmentX && x <= equipmentX + 17 && y > equipmentY && equipmentY <= equipmentY + 17) {
            Pixelmon.NETWORK.sendToServer(new OpenEquipmentMenu());
            SoundHelper.playSound(PixelSounds.ui_click);
            return false;
        }
        for (int i = 0; i < this.pixelmonSlots.length; ++i) {
            SlotInventoryPixelmon s = this.pixelmonSlots[i];
            int xSlot = ((IInventoryPixelmon)this.gui).getGUILeft() - this.partyWidth + 9;
            int ySlot = ((InventoryEffectRenderer)this.gui).height / 2 + i * 18 - 65;
            if (x >= xSlot && x <= xSlot + 16 && y >= ySlot && y <= ySlot + 16 && mouseButton == 0) {
                if (s != null && s.pokemonData.selected) {
                    s.pokemonData.selected = false;
                } else if (s != null && this.pcClient.numSelected() < 1) {
                    s.pokemonData.selected = true;
                } else {
                    if (s != null) {
                        this.pcClient.swapPokemonWithSelected(s.pokemonData);
                    } else {
                        this.pcClient.swapPositionWithSelected(new PCPos(-1, i));
                    }
                    this.fillSlots();
                }
            }
            if (s == null) continue;
            if (s.getBounds().contains(x, y) && mouseButton == 1) {
                if (this.pixelmonMenuOpen) {
                    buttonList.remove(this.pMenuButtonSumm);
                    buttonList.remove(this.pMenuButtonMove);
                    buttonList.remove(this.pMenuButtonStat);
                    this.pMenuButtonSumm = null;
                    this.pMenuButtonMove = null;
                    this.pMenuButtonStat = null;
                    this.pixelmonMenuOpen = false;
                    this.selected = null;
                }
                int xOffset = x - ((IInventoryPixelmon)this.gui).getGUILeft() - 63;
                this.pMenuButtonSumm = new GuiPokeCheckerTabs(6, 3, xOffset, y + 5, 47, 13, I18n.translateToLocal("gui.screenpokechecker.summary"));
                this.pMenuButtonMove = new GuiPokeCheckerTabs(6, 4, xOffset, y + 24, 47, 13, I18n.translateToLocal("gui.screenpokechecker.moves"));
                this.pMenuButtonStat = new GuiPokeCheckerTabs(6, 5, xOffset, y + 43, 47, 13, I18n.translateToLocal("gui.screenpokechecker.stats"));
                this.menuX = x - ((IInventoryPixelmon)this.gui).getGUILeft();
                this.menuY = y;
                this.buttonBounds = new Rectangle(xOffset, y + 5, 47, 13);
                this.buttonBoundsMoves = new Rectangle(xOffset, y + 24, 47, 13);
                this.buttonBoundsStat = new Rectangle(xOffset, y + 43, 47, 13);
                buttonList.add(this.pMenuButtonSumm);
                buttonList.add(this.pMenuButtonMove);
                buttonList.add(this.pMenuButtonStat);
                this.pixelmonMenuOpen = true;
                this.selected = s.pokemonData;
                return false;
            }
            if (s.pokemonData.isEgg || !s.getHeldItemBounds().contains(x, y) || !this.heldItemQualifies(s) || this.ticksTillClick > 0) continue;
            SetHeldItem packet = new SetHeldItem(s.pokemonData.pokemonID);
            InventoryPlayer inventory = this.gui.mc.player.inventory;
            ItemStack currentItem = inventory.getItemStack();
            ItemStack oldItem = s.pokemonData.heldItem == null ? ItemStack.EMPTY : s.pokemonData.heldItem;
            ItemStack itemStack = oldItem;
            if (this.gui.mc.player.capabilities.isCreativeMode) {
                if (!currentItem.isEmpty()) {
                    ItemStack singleItem = currentItem.copy();
                    singleItem.setCount(1);
                    s.pokemonData.heldItem = singleItem;
                    packet.setItem(currentItem.getItem());
                } else {
                    s.pokemonData.heldItem = ItemStack.EMPTY;
                    packet.setItem(null);
                }
                s.pokemonData.resetMoves = true;
                Pixelmon.NETWORK.sendToServer(packet);
            } else {
                ItemStack singleItem;
                if (oldItem.isEmpty()) {
                    if (!currentItem.isEmpty()) {
                        singleItem = currentItem.copy();
                        singleItem.setCount(1);
                        s.pokemonData.heldItem = singleItem;
                        if (currentItem.getCount() <= 1) {
                            inventory.setItemStack(ItemStack.EMPTY);
                        } else {
                            currentItem.shrink(1);
                        }
                    }
                } else if (currentItem.isEmpty()) {
                    s.pokemonData.heldItem = ItemStack.EMPTY;
                    inventory.setItemStack(oldItem);
                } else if (oldItem.getItem() == currentItem.getItem()) {
                    s.pokemonData.heldItem = ItemStack.EMPTY;
                    currentItem.grow(1);
                } else if (currentItem.getCount() <= 1) {
                    singleItem = currentItem.copy();
                    singleItem.setCount(1);
                    s.pokemonData.heldItem = singleItem;
                    inventory.setItemStack(oldItem);
                } else {
                    singleItem = currentItem.copy();
                    singleItem.setCount(1);
                    s.pokemonData.heldItem = singleItem;
                    currentItem.shrink(1);
                    inventory.addItemStackToInventory(oldItem);
                }
                s.pokemonData.resetMoves = true;
                Pixelmon.NETWORK.sendToServer(packet);
                this.ticksTillClick = 10;
                ItemStack playerItem = inventory.getItemStack();
                if (!playerItem.isEmpty() && playerItem.getCount() > 64) {
                    playerItem.setCount(64);
                }
            }
            return false;
        }
        return true;
    }

    public void tick() {
        if (this.ticksTillClick > 0) {
            --this.ticksTillClick;
        }
    }

    public boolean isPokeInfoOpen() {
        return this.pixelmonMenuOpen;
    }
}

