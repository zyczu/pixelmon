/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pokechecker;

import com.pixelmongenerations.client.gui.GuiResources;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(value=Side.CLIENT)
public class GuiRenameButtons
extends GuiButton {
    protected int width;
    protected int height;
    public int xPosition;
    public int yPosition;
    public String displayString;
    public int id;
    public boolean enabled = true;
    public boolean drawButton = true;
    protected boolean field_82253_i;

    public GuiRenameButtons(int par1, int par2, int par3, String par4Str) {
        this(par1, par2, par3, 82, 25, par4Str);
    }

    public GuiRenameButtons(int par1, int par2, int par3, int par4, int par5, String par6Str) {
        super(par1, par2, par3, par4, par5, par6Str);
        this.id = par1;
        this.xPosition = par2;
        this.yPosition = par3;
        this.width = 49;
        this.height = 25;
        this.displayString = par6Str;
    }

    @Override
    public int getHoverState(boolean par1) {
        int var2 = 1;
        if (!this.enabled) {
            var2 = 0;
        } else if (par1) {
            var2 = 2;
        }
        return var2;
    }

    @Override
    public void drawButton(Minecraft minecraft, int mouseX, int mouseY, float random) {
        if (this.drawButton) {
            FontRenderer var4 = minecraft.fontRenderer;
            minecraft.renderEngine.bindTexture(GuiResources.rename);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            this.field_82253_i = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
            this.getHoverState(this.field_82253_i);
            if (this.enabled && this.getHoverState(this.field_82253_i) == 2) {
                this.drawTexturedModalRect(this.xPosition, this.yPosition, 0, 115, 49, this.height);
            }
            this.mouseDragged(minecraft, mouseX, mouseY);
            int var6 = 0xFFFFFF;
            if (!this.enabled) {
                var6 = -6250336;
            } else if (this.field_82253_i) {
                var6 = 0xFFFFA0;
            }
            this.drawCenteredString(var4, this.displayString, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, var6);
        }
    }

    @Override
    protected void mouseDragged(Minecraft par1Minecraft, int par2, int par3) {
    }

    @Override
    public void mouseReleased(int par1, int par2) {
    }

    @Override
    public boolean mousePressed(Minecraft par1Minecraft, int par2, int par3) {
        return this.enabled && this.drawButton && par2 >= this.xPosition && par3 >= this.yPosition && par2 < this.xPosition + this.width && par3 < this.yPosition + this.height;
    }

    public boolean func_82252_a() {
        return this.field_82253_i;
    }

    public void func_82251_b(int par1, int par2) {
    }
}

