/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui.pokechecker;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.SoundHelper;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.battleScreens.ChooseAttackScreen;
import com.pixelmongenerations.client.gui.pc.GuiPC;
import com.pixelmongenerations.client.gui.pokechecker.GuiPokeCheckerTabs;
import com.pixelmongenerations.client.gui.pokechecker.GuiRenamePokemon;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeCheckerMoves;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeCheckerStats;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeCheckerWarningLevel;
import com.pixelmongenerations.client.gui.pokedex.GuiPokedexInfo;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.common.item.ItemPokeMint;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.enums.EnumMark;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.network.PixelmonData;
import java.io.IOException;
import java.util.ArrayList;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.opengl.GL11;

public class GuiScreenPokeChecker
extends GuiContainer {
    protected PixelmonData targetPacket;
    GuiButton nameButton;
    boolean renameButton;
    boolean isPC;
    int box;

    public GuiScreenPokeChecker(PixelmonData PixelmonData2, boolean b) {
        super(new ContainerEmpty());
        this.targetPacket = PixelmonData2;
        this.isPC = b;
    }

    public GuiScreenPokeChecker(PixelmonData selected, boolean b, int box) {
        this(selected, b);
        this.box = box;
    }

    @Override
    public boolean doesGuiPauseGame() {
        return true;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.clear();
        this.buttonList.add(new GuiPokeCheckerTabs(3, 0, this.width / 2 + 93, this.height / 2 + 79, 33, 16, ""));
        this.buttonList.add(new GuiPokeCheckerTabs(1, 1, this.width / 2 - 33, this.height / 2 + 79, 62, 16, I18n.translateToLocal("gui.screenpokechecker.moves")));
        this.buttonList.add(new GuiPokeCheckerTabs(2, 2, this.width / 2 + 30, this.height / 2 + 79, 62, 16, I18n.translateToLocal("gui.screenpokechecker.stats")));
        this.buttonList.add(new GuiPokeCheckerTabs(4, 4, this.width / 2 - 43, this.height / 2 - 91, 9, 9, "", this.targetPacket));
        this.buttonList.add(new GuiPokeCheckerTabs(7, 5, this.width / 2 - 43, this.height / 2 - 14, 9, 8, "", this.targetPacket));
    }

    @Override
    public void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 0: {
                this.closeScreen();
                break;
            }
            case 1: {
                this.mc.displayGuiScreen(new GuiScreenPokeCheckerMoves(this.targetPacket, this.isPC, this.box));
                break;
            }
            case 2: {
                this.mc.displayGuiScreen(new GuiScreenPokeCheckerStats(this.targetPacket, this.isPC, this.box));
                break;
            }
            case 3: {
                if (!PixelmonConfig.allowNicknames || this.targetPacket.isEgg) break;
                this.mc.displayGuiScreen(new GuiRenamePokemon(this.targetPacket, this));
                break;
            }
            case 4: {
                this.mc.displayGuiScreen(new GuiScreenPokeCheckerWarningLevel(this, this.targetPacket));
                break;
            }
            case 5: {
                if (!PixelmonConfig.allowNicknames || this.targetPacket.isEgg) break;
                this.mc.displayGuiScreen(new GuiRenamePokemon(this.targetPacket, this));
            }
        }
    }

    private void closeScreen() {
        if (!this.isPC) {
            this.mc.player.closeScreen();
        } else {
            GuiPC gui = new GuiPC(this.targetPacket, this.box);
            this.mc.displayGuiScreen(gui);
        }
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
    }

    @Override
    protected void keyTyped(char par1, int par2) {
        if (par2 == 1) {
            this.closeScreen();
        }
    }

    @Override
    protected void mouseClicked(int x, int y, int par3) throws IOException {
        ScaledResolution var5 = new ScaledResolution(this.mc);
        int var6 = var5.getScaledWidth();
        int var7 = var5.getScaledHeight();
        super.mouseClicked(x, y, par3);
        if (x > var6 / 2 - 125 && x < var6 / 2 - 40 && y > var7 / 2 - 15 && y < var7 / 2 + 5) {
            if (par3 == 1 && !this.renameButton) {
                this.nameButton = new GuiButton(3, x, y, 50, 20, I18n.translateToLocal("gui.screenpokechecker.rename"));
                this.buttonList.add(this.nameButton);
                this.renameButton = true;
            } else if (par3 != 1 && this.renameButton) {
                this.buttonList.remove(this.nameButton);
                this.renameButton = false;
            } else if (par3 == 1 && this.renameButton) {
                this.buttonList.remove(this.nameButton);
                this.nameButton = new GuiButton(3, x, y, 50, 20, I18n.translateToLocal("gui.screenpokechecker.rename"));
                this.buttonList.add(this.nameButton);
            }
        }
        this.arrowsMouseClicked(x, y);
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    @Override
    public void drawGuiContainerForegroundLayer(int par1, int par2) {
        String heldItemName;
        GL11.glNormal3f((float)0.0f, (float)-1.0f, (float)0.0f);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.screenpokechecker.summary"), -13, 166, 0xFFFFFF);
        this.mc.fontRenderer.drawString("Dynamax Lv.", -22, 84, 0x353535);
        this.mc.fontRenderer.drawString("Nature", -10, 116, 0x353535);
        if (!this.targetPacket.isEgg) {
            String natureName;
            this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.screenpokechecker.lvl") + " " + this.targetPacket.lvl, 58, -19, 0xFFFFFF);
            String dexText = I18n.translateToLocal("gui.screenpokechecker.number") + " " + String.valueOf(this.targetPacket.getNationalPokedexNumber());
            this.drawString(this.mc.fontRenderer, dexText, 58, -4, 0xFFFFFF);
            this.drawString(this.mc.fontRenderer, this.targetPacket.getSpecies().getPokemonName(), 58 + this.mc.fontRenderer.getStringWidth(dexText) + 20, -4, 0xFFFFFF);
            if (this.targetPacket.pokeball != null) {
                this.itemRender.renderItemIntoGUI(new ItemStack(this.targetPacket.pokeball.getItem()), -39, -24);
            }
            if (this.targetPacket.pseudoNature != null && this.targetPacket.pseudoNature != this.targetPacket.nature) {
                natureName = this.targetPacket.pseudoNature.getLocalizedName();
                this.mc.fontRenderer.drawString(natureName, -40 + (46 - this.mc.fontRenderer.getStringWidth(natureName) / 2), 132, 0x595959);
                Item item = ItemPokeMint.mints.get((Object)this.targetPacket.pseudoNature);
                if (item != null) {
                    this.itemRender.renderItemIntoGUI(new ItemStack(item), -40 + (46 - this.mc.fontRenderer.getStringWidth(natureName) / 2 - 20), 128);
                }
                natureName = this.targetPacket.nature.getLocalizedName();
                this.mc.fontRenderer.drawString(natureName, -40 + (46 - this.mc.fontRenderer.getStringWidth(natureName) / 2), 148, 0x595959);
            } else {
                natureName = this.targetPacket.nature.getLocalizedName();
                this.mc.fontRenderer.drawString(natureName, -40 + (46 - this.mc.fontRenderer.getStringWidth(natureName) / 2), 132, 0x595959);
            }
            if (!this.targetPacket.isFainted) {
                // empty if block
            }
        } else {
            this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.screenpokechecker.lvl") + " ???", 58, -19, 0xFFFFFF);
            this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.screenpokechecker.number") + " ???", 58, -4, 0xFFFFFF);
            this.drawCenteredString(this.mc.fontRenderer, "???", 140, -4, 0xDDDDDD);
            this.mc.fontRenderer.drawString("???", -40 + (46 - this.mc.fontRenderer.getStringWidth("???") / 2), 132, 0x595959);
        }
        int hexColor = 0x353535;
        int hexDecrease = 0xFF3030;
        int hexIncrease = 65280;
        int hexWhite = 0x595959;
        String strHP = String.valueOf(this.targetPacket.HP);
        if (this.targetPacket.isEgg) {
            strHP = "???";
        }
        this.mc.fontRenderer.drawString("HP", 82, 36, hexColor == hexWhite ? 0x353535 : hexColor);
        this.mc.fontRenderer.drawString(strHP, 150 - strHP.length() * 3, 36, hexColor);
        hexColor = this.targetPacket.nature != this.targetPacket.pseudoNature ? (this.targetPacket.pseudoNature.increasedStat == StatsType.Attack && !this.targetPacket.isEgg ? hexIncrease : (this.targetPacket.pseudoNature.decreasedStat == StatsType.Attack && !this.targetPacket.isEgg ? hexDecrease : hexWhite)) : (this.targetPacket.nature.increasedStat == StatsType.Attack && !this.targetPacket.isEgg ? hexIncrease : (this.targetPacket.nature.decreasedStat == StatsType.Attack && !this.targetPacket.isEgg ? hexDecrease : hexWhite));
        String strATK = String.valueOf(this.targetPacket.Attack);
        if (this.targetPacket.isEgg) {
            strATK = "???";
        }
        this.mc.fontRenderer.drawString("Attack", 72, 52, hexColor == hexWhite ? 0x353535 : hexColor);
        this.mc.fontRenderer.drawString(strATK, 150 - strATK.length() * 3, 52, hexColor);
        hexColor = this.targetPacket.nature != this.targetPacket.pseudoNature ? (this.targetPacket.pseudoNature.increasedStat == StatsType.Defence && !this.targetPacket.isEgg ? hexIncrease : (this.targetPacket.pseudoNature.decreasedStat == StatsType.Defence && !this.targetPacket.isEgg ? hexDecrease : hexWhite)) : (this.targetPacket.nature.increasedStat == StatsType.Defence && !this.targetPacket.isEgg ? hexIncrease : (this.targetPacket.nature.decreasedStat == StatsType.Defence && !this.targetPacket.isEgg ? hexDecrease : hexWhite));
        String strDEF = String.valueOf(this.targetPacket.Defence);
        if (this.targetPacket.isEgg) {
            strDEF = "???";
        }
        this.mc.fontRenderer.drawString("Defense", 66, 68, hexColor == hexWhite ? 0x353535 : hexColor);
        this.mc.fontRenderer.drawString(strDEF, 150 - strDEF.length() * 3, 68, hexColor);
        hexColor = this.targetPacket.nature != this.targetPacket.pseudoNature ? (this.targetPacket.pseudoNature.increasedStat == StatsType.SpecialAttack && !this.targetPacket.isEgg ? hexIncrease : (this.targetPacket.pseudoNature.decreasedStat == StatsType.SpecialAttack && !this.targetPacket.isEgg ? hexDecrease : hexWhite)) : (this.targetPacket.nature.increasedStat == StatsType.SpecialAttack && !this.targetPacket.isEgg ? hexIncrease : (this.targetPacket.nature.decreasedStat == StatsType.SpecialAttack && !this.targetPacket.isEgg ? hexDecrease : hexWhite));
        String strSATK = String.valueOf(this.targetPacket.SpecialAttack);
        if (this.targetPacket.isEgg) {
            strSATK = "???";
        }
        this.mc.fontRenderer.drawString("Sp. Atk", 70, 84, hexColor == hexWhite ? 0x353535 : hexColor);
        this.mc.fontRenderer.drawString(strSATK, 150 - strSATK.length() * 3, 84, hexColor);
        hexColor = this.targetPacket.nature != this.targetPacket.pseudoNature ? (this.targetPacket.pseudoNature.increasedStat == StatsType.SpecialDefence && !this.targetPacket.isEgg ? hexIncrease : (this.targetPacket.pseudoNature.decreasedStat == StatsType.SpecialDefence && !this.targetPacket.isEgg ? hexDecrease : hexWhite)) : (this.targetPacket.nature.increasedStat == StatsType.SpecialDefence && !this.targetPacket.isEgg ? hexIncrease : (this.targetPacket.nature.decreasedStat == StatsType.SpecialDefence && !this.targetPacket.isEgg ? hexDecrease : hexWhite));
        String strSDEF = String.valueOf(this.targetPacket.SpecialDefence);
        if (this.targetPacket.isEgg) {
            strSDEF = "???";
        }
        this.mc.fontRenderer.drawString("Sp. Def", 70, 100, hexColor == hexWhite ? 0x353535 : hexColor);
        this.mc.fontRenderer.drawString(strSDEF, 150 - strSDEF.length() * 3, 100, hexColor);
        hexColor = this.targetPacket.nature != this.targetPacket.pseudoNature ? (this.targetPacket.pseudoNature.increasedStat == StatsType.Speed && !this.targetPacket.isEgg ? hexIncrease : (this.targetPacket.pseudoNature.decreasedStat == StatsType.Speed && !this.targetPacket.isEgg ? hexDecrease : hexWhite)) : (this.targetPacket.nature.increasedStat == StatsType.Speed && !this.targetPacket.isEgg ? hexIncrease : (this.targetPacket.nature.decreasedStat == StatsType.Speed && !this.targetPacket.isEgg ? hexDecrease : hexWhite));
        String strSPD = String.valueOf(this.targetPacket.Speed);
        if (this.targetPacket.isEgg) {
            strSPD = "???";
        }
        this.mc.fontRenderer.drawString("Speed", 72, 116, hexColor == hexWhite ? 0x353535 : hexColor);
        this.mc.fontRenderer.drawString(strSPD, 150 - strSPD.length() * 3, 116, hexColor);
        if (PixelmonServerConfig.showIVsEVs) {
            String strHPIV = String.valueOf(this.targetPacket.ivs[0]);
            if (this.targetPacket.isEgg) {
                strHPIV = "???";
            }
            this.mc.fontRenderer.drawString(strHPIV, 175 - strHPIV.length() * 3, 36, 0);
            String strAtkIV = String.valueOf(this.targetPacket.ivs[1]);
            if (this.targetPacket.isEgg) {
                strAtkIV = "???";
            }
            this.mc.fontRenderer.drawString(strAtkIV, 175 - strAtkIV.length() * 3, 52, 0);
            String strDefIV = String.valueOf(this.targetPacket.ivs[2]);
            if (this.targetPacket.isEgg) {
                strDefIV = "???";
            }
            this.mc.fontRenderer.drawString(strDefIV, 175 - strDefIV.length() * 3, 68, 0);
            String strSAtkIV = String.valueOf(this.targetPacket.ivs[3]);
            if (this.targetPacket.isEgg) {
                strSAtkIV = "???";
            }
            this.mc.fontRenderer.drawString(strSAtkIV, 175 - strSAtkIV.length() * 3, 84, 0);
            Object strSDefIV = String.valueOf(this.targetPacket.ivs[4]);
            if (this.targetPacket.isEgg) {
                strSDefIV = "???";
            }
            this.mc.fontRenderer.drawString((String)strSDefIV, 175 - ((String)strSDefIV).length() * 3, 100, 0);
            String strSpeedIV = String.valueOf(this.targetPacket.ivs[5]);
            if (this.targetPacket.isEgg) {
                strSpeedIV = "???";
            }
            this.mc.fontRenderer.drawString(strSpeedIV, 175 - strSpeedIV.length() * 3, 116, 0);
            String strHPEV = String.valueOf(this.targetPacket.evs[0]);
            if (this.targetPacket.isEgg) {
                strHPEV = "???";
            }
            this.mc.fontRenderer.drawString(strHPEV, 196 - strHPEV.length() * 3, 36, 0);
            String strAtkEV = String.valueOf(this.targetPacket.evs[1]);
            if (this.targetPacket.isEgg) {
                strAtkEV = "???";
            }
            this.mc.fontRenderer.drawString(strAtkEV, 196 - strAtkEV.length() * 3, 52, 0);
            String strDefEV = String.valueOf(this.targetPacket.evs[2]);
            if (this.targetPacket.isEgg) {
                strDefEV = "???";
            }
            this.mc.fontRenderer.drawString(strDefEV, 196 - strDefEV.length() * 3, 68, 0);
            String strSAtkEV = String.valueOf(this.targetPacket.evs[3]);
            if (this.targetPacket.isEgg) {
                strSAtkEV = "???";
            }
            this.mc.fontRenderer.drawString(strSAtkEV, 196 - strSAtkEV.length() * 3, 84, 0);
            String strSDefEV = String.valueOf(this.targetPacket.evs[4]);
            if (this.targetPacket.isEgg) {
                strSDefEV = "???";
            }
            this.mc.fontRenderer.drawString(strSDefEV, 196 - strSDefEV.length() * 3, 100, 0);
            String strSpeedEV = String.valueOf(this.targetPacket.evs[5]);
            if (this.targetPacket.isEgg) {
                strSpeedEV = "???";
            }
            this.mc.fontRenderer.drawString(strSpeedEV, 196 - strSpeedEV.length() * 3, 116, 0);
        }
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.screenpokechecker.helditem"), 64, 149, 0x353535);
        String string = heldItemName = this.targetPacket.heldItem != null && !this.targetPacket.heldItem.isEmpty() ? this.targetPacket.heldItem.getDisplayName() : "Empty";
        if (!heldItemName.equals("Empty")) {
            RenderHelper.enableGUIStandardItemLighting();
            this.itemRender.renderItemAndEffectIntoGUI(this.targetPacket.heldItem, 118, 144);
            this.mc.fontRenderer.drawString(heldItemName, 136, 149, 0x595959);
        } else {
            this.mc.fontRenderer.drawString(heldItemName, 123, 149, 0x595959);
        }
        this.drawBasePokemonInfo();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.screenpokechecker.ability"), 72, 132, 0x353535);
        if (this.targetPacket.isEgg) {
            this.mc.fontRenderer.drawString(I18n.translateToLocal("ability.Egg.name"), 123, 131, 0x595959);
            return;
        }
        try {
            this.mc.fontRenderer.drawString(I18n.translateToLocal("ability." + this.targetPacket.ability + ".name"), 123, 132, 0x595959);
            if (!GuiScreen.isShiftKeyDown()) return;
            GlStateManager.pushMatrix();
            String text = I18n.translateToLocal("ability." + this.targetPacket.ability + ".description");
            ArrayList<String> tooltip = ChooseAttackScreen.wrapString(text, 50);
            tooltip.add(0, "Ability: ");
            GlStateManager.disableRescaleNormal();
            RenderHelper.disableStandardItemLighting();
            GlStateManager.disableLighting();
            GlStateManager.disableDepth();
            int i = 0;
            for (String s : tooltip) {
                int j = this.fontRenderer.getStringWidth(s);
                if (j <= i) continue;
                i = j;
            }
            int l1 = 60;
            int i2 = 132;
            int k = 8;
            if (tooltip.size() > 1) {
                k += 2 + (tooltip.size() - 1) * 10;
            }
            if (l1 + i > this.width) {
                l1 -= 28 + i;
            }
            if (i2 + k + 6 > this.height) {
                i2 = this.height - k - 6;
            }
            this.zLevel = 300.0f;
            this.itemRender.zLevel = 300.0f;
            int l = -267386864;
            this.drawGradientRect(l1 - 3, i2 - 4, l1 + i + 3, i2 - 3, -267386864, -267386864);
            this.drawGradientRect(l1 - 3, i2 + k + 3, l1 + i + 3, i2 + k + 4, -267386864, -267386864);
            this.drawGradientRect(l1 - 3, i2 - 3, l1 + i + 3, i2 + k + 3, -267386864, -267386864);
            this.drawGradientRect(l1 - 4, i2 - 3, l1 - 3, i2 + k + 3, -267386864, -267386864);
            this.drawGradientRect(l1 + i + 3, i2 - 3, l1 + i + 4, i2 + k + 3, -267386864, -267386864);
            int i1 = 0x505000FF;
            int j1 = 1344798847;
            this.drawGradientRect(l1 - 3, i2 - 3 + 1, l1 - 3 + 1, i2 + k + 3 - 1, 0x505000FF, 1344798847);
            this.drawGradientRect(l1 + i + 2, i2 - 3 + 1, l1 + i + 3, i2 + k + 3 - 1, 0x505000FF, 1344798847);
            this.drawGradientRect(l1 - 3, i2 - 3, l1 + i + 3, i2 - 3 + 1, 0x505000FF, 0x505000FF);
            this.drawGradientRect(l1 - 3, i2 + k + 2, l1 + i + 3, i2 + k + 3, 1344798847, 1344798847);
            int k1 = 0;
            while (true) {
                if (k1 >= tooltip.size()) {
                    this.zLevel = 0.0f;
                    this.itemRender.zLevel = 0.0f;
                    GlStateManager.enableLighting();
                    GlStateManager.enableDepth();
                    RenderHelper.enableStandardItemLighting();
                    GlStateManager.enableRescaleNormal();
                    GlStateManager.popMatrix();
                    return;
                }
                String s1 = tooltip.get(k1);
                this.fontRenderer.drawStringWithShadow(s1, l1, i2, -1);
                if (k1 == 0) {
                    i2 += 2;
                }
                i2 += 10;
                ++k1;
            }
        }
        catch (Exception e) {
            this.mc.fontRenderer.drawString(I18n.translateToLocal("ability.ComingSoon.name"), 123, 131, 0x595959);
            return;
        }
    }

    protected void drawBasePokemonInfo() {
        GuiHelper.bindPokemonSprite(this.targetPacket, this.mc);
        if (this.targetPacket.isGen6Sprite()) {
            GL11.glColor3f((float)1.0f, (float)1.0f, (float)1.0f);
            GuiHelper.drawImageQuad(-33.0, 1.0, 76.0, 76.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        } else {
            GL11.glColor3f((float)1.0f, (float)1.0f, (float)1.0f);
            GuiHelper.drawImageQuad(-33.0, -4.0, 84.0, 84.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        }
        if (!this.targetPacket.isEgg) {
            int pokerus;
            if (!(this instanceof GuiScreenPokeCheckerMoves)) {
                EnumType type1 = this.targetPacket.getType1();
                EnumType type2 = this.targetPacket.getType2();
                int x = 60;
                int y = 11;
                ResourceLocation typeTexture = GuiPokedexInfo.typeToImage(type1);
                this.mc.renderEngine.bindTexture(typeTexture);
                GuiHelper.drawImageQuad(x, y, 45.0, 15.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                if (type2 != null) {
                    typeTexture = GuiPokedexInfo.typeToImage(type2);
                    this.mc.renderEngine.bindTexture(typeTexture);
                    GuiHelper.drawImageQuad(x + 50, y, 45.0, 15.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                }
                if (!(this instanceof GuiScreenPokeCheckerStats) && PixelmonServerConfig.showIVsEVs) {
                    String ivString = "IVs";
                    this.mc.fontRenderer.drawString(ivString, 178 - ivString.length() * 3, 16, 0);
                    String evString = "EVs";
                    this.mc.fontRenderer.drawString(evString, 199 - evString.length() * 3, 16, 0);
                }
            }
            if ((pokerus = this.targetPacket.pokerus) > 0) {
                ResourceLocation pokerusType = pokerus == 1 ? GuiResources.pokeRusInfected : GuiResources.pokeRusCured;
                this.mc.renderEngine.bindTexture(pokerusType);
                GuiHelper.drawImageQuad(195.0, -24.0, 16.0, 16.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            }
            if (this.targetPacket.mark != EnumMark.None) {
                this.mc.renderEngine.bindTexture(this.targetPacket.mark.getIconLocation());
                GuiHelper.drawImageQuad(92.0, -24.0, 16.0, 16.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            }
            if (this.targetPacket.hasGmaxFactor) {
                this.mc.renderEngine.bindTexture(GuiResources.gmaxIcon);
                GuiHelper.drawImageQuad(165.0, -24.0, 16.0, 16.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            }
        }
        if (this.targetPacket.isShiny) {
            this.mc.renderEngine.bindTexture(GuiResources.shiny);
            GuiHelper.drawImageQuad(-23.0, -20.0, 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        }
    }

    @Override
    public void drawGuiContainerBackgroundLayer(float f, int i, int i1) {
        ScaledResolution var5 = new ScaledResolution(this.mc);
        var5.getScaledWidth();
        var5.getScaledHeight();
        this.mc.renderEngine.bindTexture(GuiResources.summarySummary);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.drawTexturedModalRect((this.width - this.xSize) / 2 - 40, (this.height - this.ySize) / 2 - 25, 0, 0, 256, 205);
        this.drawTexturedModalRect((this.width - this.xSize) / 2 - 15, (this.height - this.ySize) / 2 + 127, 23, 225, 44, 28);
        this.drawTexturedModalRect(this.width / 2 - 126, this.height / 2 + 79, 2, 205, 92, 16);
        int dynamaxLevel = this.targetPacket.dynamaxLevel;
        this.drawTexturedModalRect(this.width / 2 - 105, this.height / 2 + 14, 162, 222, 49, 12);
        if (dynamaxLevel > 0) {
            this.drawTexturedModalRect(this.width / 2 - 105, this.height / 2 + 14, 162, 207, dynamaxLevel * 5, 12);
        }
        float xpBarFill = this.targetPacket.getExpFraction() * 10.0f;
        this.drawTexturedModalRect(this.width / 2 + 25, this.height / 2 - 105, 162, 222, 49, 12);
        if (xpBarFill >= 1.0f) {
            this.drawTexturedModalRect(this.width / 2 + 25, this.height / 2 - 105, 162, 237, (int)xpBarFill * 5, 12);
        }
        this.drawPokemonName();
        this.drawArrows(i, i1);
    }

    protected void drawPokemonName() {
        if (this.targetPacket.isEgg) {
            this.drawCenteredStringWithoutShadow(Entity1Base.getLocalizedName("Egg"), (this.width - this.xSize) / 2 - 19, (this.height - this.ySize) / 2 + -19, 0xFFFFFF);
        } else {
            int offset = this.targetPacket.isShiny ? 7 : 0;
            this.mc.fontRenderer.drawString(this.targetPacket.getNickname(), (this.width - this.xSize) / 2 - 19 + offset, (this.height - this.ySize) / 2 + -19, 0xFFFFFF);
        }
    }

    public void arrowsMouseClicked(int x, int y) {
        if (this.isPC) {
            return;
        }
        int l1 = (this.width - this.xSize) / 2 + 220;
        int l2 = (this.width - this.xSize) / 2 - 62;
        int w = 16;
        int t = (this.height - this.ySize) / 2;
        int h = 21;
        if (y > t && y < t + h) {
            if (x > l1 && x < l1 + w) {
                SoundHelper.playButtonPressSound();
                this.getNextPokemon();
            }
            if (x > l2 && x < l2 + w) {
                SoundHelper.playButtonPressSound();
                this.getPrevPokemon();
            }
        }
    }

    private void getPrevPokemon() {
        if (!this.isPC) {
            int pos = this.targetPacket.order;
            this.targetPacket = ServerStorageDisplay.getPrevFromPos(pos);
        }
    }

    private void getNextPokemon() {
        if (!this.isPC) {
            int pos = this.targetPacket.order;
            this.targetPacket = ServerStorageDisplay.getNextFromPos(pos);
        }
    }

    public void drawArrows(int mouseX, int mouseY) {
        if (this.isPC) {
            return;
        }
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.mc.renderEngine.bindTexture(GuiResources.summaryMoves);
        int l1 = (this.width - this.xSize) / 2 + 220;
        int l2 = (this.width - this.xSize) / 2 - 62;
        int w = 16;
        int t = (this.height - this.ySize) / 2;
        int h = 21;
        this.drawTexturedModalRect(l1, t, 24, 207, w, h);
        this.drawTexturedModalRect(l2, t, 42, 207, w, h);
        if (mouseY > t && mouseY < t + h) {
            if (mouseX > l1 && mouseX < l1 + w) {
                this.drawTexturedModalRect(l1, t, 60, 207, w, h);
            }
            if (mouseX > l2 && mouseX < l2 + w) {
                this.drawTexturedModalRect(l2, t, 78, 207, w, h);
            }
        }
    }

    public void drawCenteredStringWithoutShadow(String par2Str, int par3, int par4, int par5) {
        this.mc.fontRenderer.drawString(par2Str, par3 - this.mc.fontRenderer.getStringWidth(par2Str) / 2, par4, par5);
    }

    public void drawSplitString(String par2str, int par3, int par4, int par5, int par6) {
        this.mc.fontRenderer.drawSplitString(par2str, par3, par4, par5, par6);
    }
}

