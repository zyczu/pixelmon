/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pokechecker;

import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.core.network.PixelmonData;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(value=Side.CLIENT)
public class GuiPokeCheckerTabs extends GuiButton {

    public boolean drawButton;
    public int tabType;
    PixelmonData targetPacket;

    public GuiPokeCheckerTabs(int type, int par1, int par2, int par3, String par4Str) {
        this(type, par1, par2, par3, 90, 15, par4Str);
    }

    public GuiPokeCheckerTabs(int type, int buttonId, int x, int y, int width, int height, String text) {
        super(buttonId, x, y, width, height, text);
        this.enabled = true;
        this.drawButton = true;
        this.id = buttonId;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.displayString = text;
        this.tabType = type;
    }

    public GuiPokeCheckerTabs(int type, int buttonId, int x, int y, int width, int height, String text, PixelmonData pixelmonData) {
        super(buttonId, x, y, width, height, text);
        this.enabled = true;
        this.drawButton = true;
        this.id = buttonId;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.displayString = text;
        this.tabType = type;
        this.targetPacket = pixelmonData;
    }

    public void setXPosition(int xPosition) {
        this.x = xPosition;
    }

    @Override
    public int getHoverState(boolean par1) {
        int var2 = 1;
        if (!this.enabled) {
            var2 = 0;
        } else if (par1) {
            var2 = 2;
        }
        return var2;
    }

    @Override
    public void drawButton(Minecraft minecraft, int mouseX, int mouseY, float random) {
        if (this.drawButton) {
            FontRenderer fontRenderer = minecraft.fontRenderer;
            if (this.tabType <= 4 || this.tabType == 7) {
                minecraft.renderEngine.bindTexture(GuiResources.summarySummary);
            } else if (this.tabType == 5) {
                minecraft.renderEngine.bindTexture(GuiResources.yesNo);
            } else if (this.tabType == 6) {
                minecraft.renderEngine.bindTexture(GuiResources.pokecheckerPopup);
            }
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            this.hovered = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
            boolean bl = this.hovered;
            if (this.getHoverState(this.hovered) == 2) {
                if (this.tabType == 4) {
                    this.drawTexturedModalRect(this.x, this.y, 88, 228, this.width, this.height);
                } else if (this.tabType == 7) {
                    this.drawTexturedModalRect(this.x, this.y, 79, 229, this.width, this.height);
                } else if (this.tabType == 3) {
                    this.drawTexturedModalRect(this.x, this.y, 221, 205, this.width, this.height);
                } else if (this.tabType == 2) {
                    this.drawTexturedModalRect(this.x, this.y, 95, 205, this.width, this.height);
                } else if (this.tabType == 1) {
                    this.drawTexturedModalRect(this.x, this.y, 95, 205, this.width, this.height);
                } else if (this.tabType == 0) {
                    this.drawTexturedModalRect(this.x, this.y, 2, 205, this.width, this.height);
                } else if (this.tabType == 5) {
                    this.drawTexturedModalRect(this.x, this.y, 154, 102, this.width, this.height);
                } else if (this.tabType == 6) {
                    this.drawTexturedModalRect(this.x, this.y, 1, 76, this.width, this.height);
                }
            }
            if (this.getHoverState(this.hovered) != 2 && this.tabType == 4 && !this.targetPacket.doesLevel) {
                this.drawTexturedModalRect(this.x, this.y, 88, 239, this.width, this.height);
            }
            this.mouseDragged(minecraft, this.x, this.y);
            int var6 = 0;
            if (this.tabType <= 4 || this.tabType == 7) {
                var6 = 0xE5E5E5;
                if (!this.enabled) {
                    var6 = -6250336;
                } else if (this.hovered) {
                    var6 = 0xFFFFFF;
                }
                fontRenderer.drawString(this.displayString, this.x + this.width / 2 - fontRenderer.getStringWidth(this.displayString) / 2, this.y + (this.height - 8) / 2, var6);
            } else {
                var6 = 0xFFFFFF;
                if (!this.enabled) {
                    var6 = -6250336;
                } else if (this.hovered) {
                    var6 = 0xFFFFA0;
                }
                this.drawCenteredString(fontRenderer, this.displayString, this.x + this.width / 2, this.y + (this.height - 8) / 2, var6);
            }
        }
    }

    @Override
    protected void mouseDragged(Minecraft par1Minecraft, int par2, int par3) {
    }

    @Override
    public void mouseReleased(int par1, int par2) {
    }

    @Override
    public boolean mousePressed(Minecraft par1Minecraft, int par2, int par3) {
        return this.enabled && this.drawButton && par2 >= this.x && par3 >= this.y && par2 < this.x + this.width && par3 < this.y + this.height;
    }
}

