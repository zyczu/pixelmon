/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

public class ProgressBar {
    private int value;
    private int maxValue = 100;

    public void setProgress(int progress) {
        this.value = progress;
    }

    public void draw(int x, int y, int height, int width, int screenWidth, int screenHeight) {
        GlStateManager.enableRescaleNormal();
        GlStateManager.enableColorMaterial();
        GlStateManager.pushMatrix();
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder vertexbuffer = tessellator.getBuffer();
        GlStateManager.disableTexture2D();
        vertexbuffer.begin(7, DefaultVertexFormats.POSITION_COLOR);
        vertexbuffer.pos(x - width / 2, y, 0.0).color(0.0f, 0.0f, 0.0f, 1.0f).endVertex();
        vertexbuffer.pos(x - width / 2, y + height, 0.0).color(0.0f, 0.0f, 0.0f, 1.0f).endVertex();
        vertexbuffer.pos(x + width / 2, y + height, 0.0).color(0.0f, 0.0f, 0.0f, 1.0f).endVertex();
        vertexbuffer.pos(x + width / 2, y, 0.0).color(0.0f, 0.0f, 0.0f, 1.0f).endVertex();
        int barWidth = (int)((float)this.value / (float)this.maxValue * ((float)width - 6.0f));
        vertexbuffer.pos(x - width / 2 + 3, y + 3, 0.0).color(1.0f - (float)this.value / (float)this.maxValue * 0.8f, 0.2f + (float)this.value / (float)this.maxValue * 0.8f, 0.2f, 1.0f).endVertex();
        vertexbuffer.pos(x - width / 2 + 3, y + height - 3, 0.0).color(1.0f - (float)this.value / (float)this.maxValue * 0.8f, 0.2f + (float)this.value / (float)this.maxValue * 0.8f, 0.2f, 1.0f).endVertex();
        vertexbuffer.pos(x - width / 2 + 3 + barWidth, y + height - 3, 0.0).color(1.0f - (float)this.value / (float)this.maxValue * 0.8f, 0.2f + (float)this.value / (float)this.maxValue * 0.8f, 0.2f, 1.0f).endVertex();
        vertexbuffer.pos(x - width / 2 + 3 + barWidth, y + 3, 0.0).color(1.0f - (float)this.value / (float)this.maxValue * 0.8f, 0.2f + (float)this.value / (float)this.maxValue * 0.8f, 0.2f, 1.0f).endVertex();
        tessellator.draw();
        GlStateManager.popMatrix();
        GlStateManager.enableTexture2D();
        GlStateManager.disableRescaleNormal();
        GlStateManager.disableColorMaterial();
    }
}

