/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui.starter;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.packetHandlers.ChooseStarter;
import java.io.IOException;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.opengl.GL11;

public class GuiChooseStarter
extends GuiScreen {
    private float scale = 2.0f;
    private Integer hoveredDex = 0;
    private int hoveredCol = 0;
    private int hoveredRow = 0;

    @Override
    public void initGui() {
        if (ServerStorageDisplay.starterListPacket == null || ServerStorageDisplay.starterListPacket.pokemonList == null) {
            return;
        }
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        Optional<EnumSpecies> pokemonOpt;
        this.mc.renderEngine.bindTexture(GuiResources.starterBackground);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(0.0, 0.0, this.width, this.height, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        GlStateManager.enableBlend();
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        int guiScale = Minecraft.getMinecraft().gameSettings.guiScale;
        if (guiScale == 0) {
            this.scale = 1.5f;
        }
        ScaledResolution scaledresolution = new ScaledResolution(Minecraft.getMinecraft());
        int trademarkTextY = (int)((double)scaledresolution.getScaledHeight() / 6.7);
        float textScale = 1.0f;
        int textGap = 24;
        String trademark = I18n.translateToLocal("gui.starter.trademark4");
        int textHeight = this.toInt((float)this.fontRenderer.FONT_HEIGHT * textScale + (float)textGap);
        int textHeightY = this.toInt((float)textHeight * textScale);
        while ((double)textHeightY > (double)scaledresolution.getScaledHeight() / 21.1) {
            textHeightY = this.toInt((float)textHeight * (textScale -= 0.05f));
            textGap = this.toInt(textHeightY / 3);
        }
        int textWidthX = this.toInt((float)this.fontRenderer.getStringWidth(trademark) * textScale);
        while ((double)textWidthX > (double)scaledresolution.getScaledWidth() / 2.34) {
            textWidthX = this.toInt((float)this.fontRenderer.getStringWidth(trademark) * (textScale -= 0.05f));
            textGap = this.toInt(textHeightY / 3);
        }
        textGap = (int)((float)textGap + (float)textGap * this.scale);
        textHeightY = (int)((float)textHeightY + this.scale / 5.0f);
        textWidthX = (int)((float)textWidthX + this.scale / 5.0f);
        trademarkTextY = (int)((float)trademarkTextY + this.scale / 5.0f);
        GlStateManager.pushMatrix();
        GlStateManager.scale(textScale += this.scale / 5.0f, textScale, textScale);
        this.fontRenderer.drawString(trademark, this.toInt(((float)(scaledresolution.getScaledWidth() / 2) - (float)(this.fontRenderer.getStringWidth(trademark) / 2) * textScale) / textScale), this.toInt((float)trademarkTextY / textScale), 0xFFFFFF);
        trademark = I18n.translateToLocal("gui.starter.trademark5");
        this.fontRenderer.drawString(trademark, this.toInt(((float)(scaledresolution.getScaledWidth() / 2) - (float)(this.fontRenderer.getStringWidth(trademark) / 2) * textScale) / textScale), this.toInt((float)(trademarkTextY + textGap) / textScale), 0xFFFFFF);
        trademark = I18n.translateToLocal("gui.starter.trademark6");
        this.fontRenderer.drawString(trademark, this.toInt(((float)(scaledresolution.getScaledWidth() / 2) - (float)(this.fontRenderer.getStringWidth(trademark) / 2) * textScale) / textScale), this.toInt((float)(trademarkTextY + textGap * 2) / textScale), 0xFFFFFF);
        GlStateManager.popMatrix();
        int paddingSplitX = this.toInt((double)scaledresolution.getScaledWidth() / 15.2 / 2.0);
        int paddingSplitY = this.toInt((double)scaledresolution.getScaledHeight() / 3.1);
        int maxX = scaledresolution.getScaledWidth() - paddingSplitX;
        int maxY = scaledresolution.getScaledHeight() - paddingSplitY / 3;
        int widthGap = this.toInt(((float)maxX - 24.0f * this.scale) / 6.0f);
        widthGap += widthGap / 6;
        int sideBuffer = this.toInt((double)widthGap * 0.03);
        widthGap -= sideBuffer;
        int heightGap = this.toInt(((float)maxY - 24.0f * this.scale) / 6.0f);
        heightGap += heightGap / 6;
        int heightBuffer = this.toInt((double)heightGap * 0.03);
        heightGap -= heightBuffer;
        GlStateManager.pushMatrix();
        GlStateManager.scale(this.scale, this.scale, this.scale);
        boolean withinBounds = false;
        int index = 0;
        block2: for (int row = 0; row < 5; ++row) {
            for (int col = 0; col < 6; ++col) {
                GlStateManager.color(1.0f, 1.0f, 1.0f);
                int indexNum = index++;
                if (indexNum > ServerStorageDisplay.starterListPacket.pokemonList.length - 1) continue block2;
                PokemonForm form = ServerStorageDisplay.starterListPacket.pokemonList[indexNum];
                if (form == null) continue;
                int x = sideBuffer + paddingSplitX + widthGap * col;
                int y = heightBuffer + paddingSplitY + heightGap * row;
                boolean isHovering = this.isWithinBounds(mouseX, mouseY, x, y, this.toInt(24.0f * this.scale));
                if (isHovering && (this.hoveredDex == 0 || this.hoveredDex.intValue() == form.pokemon.getNationalPokedexInteger())) {
                    this.hoveredDex = form.pokemon.getNationalPokedexInteger();
                    this.hoveredCol = col;
                    this.hoveredRow = row;
                    withinBounds = true;
                    this.mc.renderEngine.bindTexture(GuiResources.starterButtonHover);
                    GuiHelper.drawImageQuad((float)x / this.scale, (float)y / this.scale, 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                } else {
                    this.mc.renderEngine.bindTexture(GuiResources.starterButton);
                    GuiHelper.drawImageQuad((float)x / this.scale, (float)y / this.scale, 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                }
                GuiHelper.bindPokemonSprite(form.pokemon.getNationalPokedexInteger(), form.form, form.gender, ServerStorageDisplay.starterListPacket.isShiny, this.mc);
                GuiHelper.drawImageQuad((float)x / this.scale, (float)y / this.scale, 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            }
        }
        if (!withinBounds) {
            this.hoveredDex = 0;
            this.hoveredCol = 0;
            this.hoveredRow = 0;
        }
        GlStateManager.popMatrix();
        if (this.hoveredDex != 0 && (pokemonOpt = EnumSpecies.getFromDex(this.hoveredDex)).isPresent()) {
            String displayText = String.format("Gen #%s: %s", EntityPixelmon.getGenerationFrom(pokemonOpt.get()), pokemonOpt.get().name);
            int offsetX = this.fontRenderer.getStringWidth(displayText) / 2;
            offsetX = (int)((float)offsetX * textScale);
            int textX = this.toInt((float)(sideBuffer + (paddingSplitX - offsetX) + widthGap * this.hoveredCol) + 24.0f * this.scale / 2.0f);
            int textY = this.toInt(heightBuffer + paddingSplitY + heightGap * this.hoveredRow - 9);
            GL11.glPushMatrix();
            GlStateManager.scale(textScale, textScale, textScale);
            this.fontRenderer.drawString(displayText, this.toInt((float)textX / textScale), this.toInt((float)textY / textScale), 4013114);
            GL11.glPopMatrix();
        }
    }

    @Override
    protected void mouseClicked(int par1, int par2, int par3) throws IOException {
        Optional<EnumSpecies> pokemonOpt;
        if (ServerStorageDisplay.starterListPacket == null || ServerStorageDisplay.starterListPacket.pokemonList == null) {
            return;
        }
        if (this.hoveredDex != 0 && (pokemonOpt = EnumSpecies.getFromDex(this.hoveredDex)).isPresent()) {
            EnumSpecies poke = pokemonOpt.get();
            for (int i = 0; i < ServerStorageDisplay.starterListPacket.pokemonList.length; ++i) {
                PokemonForm form = ServerStorageDisplay.starterListPacket.pokemonList[i];
                if (form == null || form.pokemon != poke) continue;
                Pixelmon.NETWORK.sendToServer(new ChooseStarter(i));
                try {
                    this.mc.player.closeScreen();
                }
                catch (NullPointerException nullPointerException) {}
                break;
            }
            ServerStorageDisplay.starterListPacket = null;
        }
    }

    private int toInt(double value) {
        return (int)value;
    }

    private boolean isWithinBounds(int mouseX, int mouseY, int minX, int minY, int size) {
        return mouseX >= minX && mouseX <= minX + size && mouseY >= minY && mouseY <= minY + size;
    }
}

