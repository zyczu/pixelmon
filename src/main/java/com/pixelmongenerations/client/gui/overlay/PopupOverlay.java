/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui.overlay;

import com.pixelmongenerations.api.ui.Popup;
import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.overlay.BaseOverlay;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.client.GuiIngameForge;
import org.lwjgl.opengl.GL11;

public class PopupOverlay
extends BaseOverlay {
    private Queue<Popup> popupQueue = new ConcurrentLinkedQueue<Popup>();
    private Popup popup;
    private PopupStage stage;
    private double progress = 0.0;

    @Override
    public boolean isActive() {
        return this.popup != null;
    }

    @Override
    public boolean canUpdate() {
        return this.popup != null || this.popupQueue.size() > 0;
    }

    @Override
    public void update() {
        if (this.popup == null && this.popupQueue.size() > 0) {
            this.popup = this.popupQueue.poll();
            this.stage = PopupStage.ENTERING;
            this.progress = 0.0;
            GuiIngameForge.renderBossHealth = false;
            return;
        }
        switch (this.stage) {
            case ENTERING: {
                this.progress += 0.07;
                if (!(this.progress > 1.0)) break;
                this.stage = PopupStage.WAITING;
                this.progress = 1.0;
                break;
            }
            case WAITING: {
                this.progress += 1.0;
                if (!(this.progress > (double)this.popup.showTicks())) break;
                this.stage = PopupStage.EXITING;
                this.progress = 1.0;
                break;
            }
            case EXITING: {
                this.progress -= 0.07;
                if (!(this.progress < 0.0)) break;
                this.popup = null;
                this.stage = null;
                this.progress = 0.0;
                if (this.popupQueue.peek() != null) break;
                GuiIngameForge.renderBossHealth = true;
            }
        }
    }

    @Override
    public void render(int screenWidth, int screenHeight, Minecraft mc, FontRenderer fontRenderer) {
        if (this.popup == null) {
            return;
        }
        GL11.glColor3f((float)1.0f, (float)1.0f, (float)1.0f);
        TextureResource texture = ClientProxy.TEXTURE_STORE.getObject(this.popup.texture());
        if (texture != null) {
            texture.bindTexture();
            double x = screenWidth / 2 - this.popup.width() / 2;
            double y = MathHelper.clampedLerp(-this.popup.height() + -3, 3.0, this.progress);
            GuiHelper.drawImageQuad(x, y, this.popup.width(), this.popup.height(), 0.0, 0.0, 1.0, 1.0, this.zLevel);
            if (!this.popup.text().isEmpty()) {
                fontRenderer.setUnicodeFlag(false);
                fontRenderer.drawString(this.popup.text(), (int)x + this.popup.textOffsetX(), (int)y + this.popup.textOffsetY(), this.popup.textColor(), this.popup.textOutline());
            }
        }
    }

    public void enqueuePopup(Popup popup) {
        this.popupQueue.add(popup);
    }

    private static enum PopupStage {
        ENTERING,
        WAITING,
        EXITING;

    }
}

