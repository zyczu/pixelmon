/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.overlay;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.overlay.BaseOverlay;
import com.pixelmongenerations.core.proxy.ClientProxy;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.settings.GameSettings;

public class IconsOverlay
extends BaseOverlay {
    @Override
    public void render(int scaledWidth, int scaledHeight, Minecraft mc, FontRenderer fontRenderer) {
        if (ClientProxy.cosmeticsKeyBind.getKeyCode() != 0) {
            fontRenderer.drawString(GameSettings.getKeyDisplayString(ClientProxy.cosmeticsKeyBind.getKeyCode()), scaledWidth - 60, scaledHeight - 13, 0xFFFFFF);
            mc.renderEngine.bindTexture(GuiResources.cosmeticKeyIcon);
            GuiHelper.drawImageQuad(scaledWidth - 80, scaledHeight - 30, 20.0, 20.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        }
        if (ClientProxy.wikiKeyBind.getKeyCode() != 0) {
            fontRenderer.drawString(GameSettings.getKeyDisplayString(ClientProxy.wikiKeyBind.getKeyCode()), scaledWidth - 35, scaledHeight - 13, 0xFFFFFF);
            mc.renderEngine.bindTexture(GuiResources.wikiItemIcon);
            GuiHelper.drawImageQuad(scaledWidth - 55, scaledHeight - 30, 20.0, 20.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        }
        if (ClientProxy.pokedexKeyBind.getKeyCode() != 0) {
            fontRenderer.drawString(GameSettings.getKeyDisplayString(ClientProxy.pokedexKeyBind.getKeyCode()), scaledWidth - 10, scaledHeight - 13, 0xFFFFFF);
            mc.renderEngine.bindTexture(GuiResources.pokedexItemIcon);
            GuiHelper.drawImageQuad(scaledWidth - 30, scaledHeight - 30, 20.0, 20.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        }
    }
}

