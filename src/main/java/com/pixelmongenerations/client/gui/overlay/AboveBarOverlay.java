/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.overlay;

import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.overlay.BaseOverlay;
import com.pixelmongenerations.core.network.packetHandlers.customOverlays.UpdateAboveBarOverlayPacket;
import com.pixelmongenerations.core.proxy.ClientProxy;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;

public class AboveBarOverlay
extends BaseOverlay {
    public UpdateAboveBarOverlayPacket packet;

    @Override
    public boolean isActive() {
        return this.packet != null;
    }

    @Override
    public void render(int screenWidth, int screenHeight, Minecraft mc, FontRenderer fontRenderer) {
        int left = screenWidth / 2 + 73;
        int top = screenHeight - 50;
        fontRenderer.setUnicodeFlag(false);
        fontRenderer.drawString(this.packet.text, left - fontRenderer.getStringWidth(this.packet.text), top, this.packet.textColor);
        fontRenderer.setUnicodeFlag(true);
        if (this.packet.icon.isEmpty()) {
            return;
        }
        TextureResource texture = ClientProxy.TEXTURE_STORE.getObject(this.packet.icon);
        if (texture != null) {
            texture.bindTexture();
            GuiHelper.drawImageQuad(left + 1, top - 1, 9.0, 9.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        }
    }
}

