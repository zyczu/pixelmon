/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.overlay;

public enum OverlayType {
    CAMERA,
    PARTY,
    CURRENT_POKEMON,
    SPECTATE,
    ICONS,
    POPUP,
    WELCOME,
    BAR,
    ABOVE_BAR;

}

