/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.chooseMoveset;

import com.pixelmongenerations.client.gui.chooseMoveset.IMoveClicked;
import com.pixelmongenerations.client.gui.elements.GuiSlotBase;
import com.pixelmongenerations.common.battle.attacks.Attack;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;

public class GuiMoveList
extends GuiSlotBase {
    private final Minecraft mc;
    private IMoveClicked owner;
    private ArrayList<Attack> list;

    public GuiMoveList(IMoveClicked owner, ArrayList<Attack> list, int width, int height, int top, int left, Minecraft mc) {
        super(top, left, width, height, true);
        this.owner = owner;
        this.list = list;
        this.mc = mc;
    }

    @Override
    protected int getSize() {
        return this.list.size();
    }

    @Override
    protected void elementClicked(int index, boolean doubleClicked) {
        this.owner.elementClicked(this.list, index);
    }

    @Override
    protected boolean isSelected(int element) {
        return false;
    }

    @Override
    protected void drawSlot(int index, int x, int yTop, int yMiddle, Tessellator tessellator) {
        if (index < 0 || index >= this.list.size()) {
            return;
        }
        Attack a = this.list.get(index);
        if (a != null) {
            this.mc.fontRenderer.drawString(a.getAttackBase().getLocalizedName(), x + 2, yTop - 1, 0);
        }
    }

    @Override
    protected float[] get1Color() {
        return new float[]{1.0f, 1.0f, 1.0f};
    }

    @Override
    protected int[] getSelectionColor() {
        return new int[]{128, 128, 128};
    }
}

