/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.chooseMoveset;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.chooseMoveset.GuiMoveList;
import com.pixelmongenerations.client.gui.chooseMoveset.IMoveClicked;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.chooseMoveset.ChooseMoveset;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.util.text.translation.I18n;

public class GuiChooseMoveset
extends GuiContainer
implements IMoveClicked {
    PixelmonData pokemon;
    public static ArrayList<Attack> mainAttackList;
    public static ArrayList<Attack> chosenAttackList;
    int listTop;
    int listHeight;
    int listWidth;
    GuiMoveList mainList;
    GuiMoveList chosenList;

    public GuiChooseMoveset(PixelmonData pokemon) {
        super(new ContainerEmpty());
        this.pokemon = pokemon;
        mainAttackList = DatabaseMoves.getMovesetUpToLevel(pokemon.getBaseStats().baseFormID, pokemon.lvl);
        chosenAttackList = new ArrayList();
        this.listHeight = 150;
        this.listWidth = 90;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.listTop = this.height / 2 - 58;
        this.mainList = new GuiMoveList(this, mainAttackList, this.listWidth, this.listHeight, this.listTop, this.width / 2 - 3, this.mc);
        this.chosenList = new GuiMoveList(this, chosenAttackList, this.listWidth, this.listHeight, this.listTop, this.width / 2 - 103, this.mc);
        this.buttonList.add(new GuiButton(1, this.width / 2 + 155, this.height / 2 + 90, 30, 20, I18n.translateToLocal("gui.guiItemDrops.ok")));
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float mFloat, int mouseX, int mouseY) {
        int i;
        this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
        GuiHelper.drawImageQuad(0.0, 0.0, this.width, this.height, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        String text = I18n.translateToLocal("gui.choosemoveset.title");
        this.mc.fontRenderer.drawString(text, this.width / 2 - this.fontRenderer.getStringWidth(text) / 2, this.height / 2 - 110, 0);
        text = I18n.translateToLocal("gui.choosemoveset.chosen");
        this.mc.fontRenderer.drawString(text, this.width / 2 - 65 - this.fontRenderer.getStringWidth(text) / 2, this.height / 2 - 92, 0);
        text = I18n.translateToLocal("gui.choosemoveset.full");
        this.mc.fontRenderer.drawString(text, this.width / 2 + 35 - this.fontRenderer.getStringWidth(text) / 2, this.height / 2 - 92, 0);
        this.mainList.drawScreen(mouseX, mouseY, mFloat);
        this.chosenList.drawScreen(mouseX, mouseY, mFloat);
        GuiHelper.bindPokemonSprite(this.pokemon, this.mc);
        GlStateManager.color(1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(this.width / 2 - 211, this.height / 2 - 98 - (this.pokemon.isGen6Sprite() ? 0 : 3), 84.0, 84.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
        GuiHelper.drawAttackInfoBox(this.zLevel, this.width, this.height);
        for (i = 0; i < mainAttackList.size(); ++i) {
            if (!this.mainList.isMouseOver(i, mouseX, mouseY)) continue;
            GuiHelper.drawAttackInfoList(mainAttackList.get(i), this.width, this.height);
        }
        for (i = 0; i < chosenAttackList.size(); ++i) {
            if (!this.chosenList.isMouseOver(i, mouseX, mouseY)) continue;
            GuiHelper.drawAttackInfoList(chosenAttackList.get(i), this.width, this.height);
        }
    }

    @Override
    public void elementClicked(ArrayList<Attack> list, int index) {
        if (list == mainAttackList) {
            if (chosenAttackList.size() >= 4) {
                return;
            }
            Attack a = mainAttackList.get(index);
            mainAttackList.remove(a);
            chosenAttackList.add(a);
        } else {
            Attack a = chosenAttackList.get(index);
            chosenAttackList.remove(a);
            mainAttackList.add(a);
        }
    }

    @Override
    protected void keyTyped(char p_73869_1_, int p_73869_2_) {
        if (p_73869_2_ == 1 || p_73869_2_ == 28) {
            this.actionPerformed(null);
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        Pixelmon.NETWORK.sendToServer(new ChooseMoveset(this.pokemon, chosenAttackList));
        this.mc.player.closeScreen();
    }

    public static Minecraft getMinecraft(GuiChooseMoveset gui) {
        return gui.mc;
    }
}

