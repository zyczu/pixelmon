/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Mouse
 */
package com.pixelmongenerations.client.gui.pokedex;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.pokedex.ClientPokedexManager;
import com.pixelmongenerations.client.gui.pokedex.GuiPokedexInfo;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.common.pokedex.Pokedex;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.SpriteHelper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.input.Mouse;

public class GuiPokedexHome
extends GuiContainer {
    private int currentPage = 1;
    private Integer targetDexNum;
    private Pokedex pokedex;
    protected GuiTextField searchText;
    private ArrayList<EnumSpecies> searchResults;
    private boolean searching = false;
    private String targetSearch;
    private final int pokesPerPage = 30;
    private final int rowCount = 5;
    private final int rowPokeCount = 6;
    private final int spriteSize = 24;
    private final float scale = 2.0f;
    private Integer hoveredDex = 0;
    private int hoveredCol = 0;
    private int hoveredRow = 0;

    public GuiPokedexHome(int targetDexNum) {
        super(new ContainerEmpty());
        this.targetDexNum = targetDexNum;
    }

    public GuiPokedexHome(String targetSearch) {
        super(new ContainerEmpty());
        this.targetSearch = targetSearch;
    }

    public GuiPokedexHome() {
        super(new ContainerEmpty());
    }

    @Override
    public void initGui() {
        this.pokedex = ClientPokedexManager.pokedex;
        this.searching = false;
        this.searchResults = new ArrayList();
        this.searchText = new GuiTextField(2, this.fontRenderer, this.width / 2 - 100, this.toInt((double)this.height / 1.0999999999999999) - 20, 200, 20);
        this.searchText.setMaxStringLength(32500);
        this.searchText.setFocused(true);
        if (this.targetSearch != null) {
            this.searchText.setText(this.targetSearch);
            this.searching = true;
            this.updateSearchResults();
        }
        if (this.targetDexNum != null) {
            this.currentPage = this.getPageFor(this.targetDexNum);
            this.targetDexNum = null;
        }
        super.initGui();
    }

    @Override
    public void drawGuiContainerBackgroundLayer(float renderPartialTicks, int mouseX, int mouseY) {
        Optional<EnumSpecies> pokemonOpt;
        int row;
        this.mc.renderEngine.bindTexture(GuiResources.pokedexHomeBackground);
        GuiHelper.drawImageQuad(0.0, 0.0, this.width, this.height, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        String pageInfo = "Page #" + this.currentPage + " " + this.getDexRange(this.currentPage);
        this.fontRenderer.drawString(pageInfo, this.width / 2 - this.fontRenderer.getStringWidth(pageInfo) / 2, this.toInt((double)this.height / 1.085), 0xFFFFFF);
        this.searchText.drawTextBox();
        int paddingSplitX = this.toInt((double)this.width / 15.2 / 2.0);
        int paddingSplitY = this.toInt(this.height / 3 / 2);
        int maxX = this.width - paddingSplitX;
        int maxY = this.height - paddingSplitY;
        int widthGap = this.toInt(((float)maxX - 48.0f) / 6.0f);
        widthGap += widthGap / 6;
        int sideBuffer = this.toInt((double)widthGap * 0.03);
        widthGap -= sideBuffer;
        int heightGap = this.toInt(((float)maxY - 48.0f) / 6.0f);
        heightGap += heightGap / 6;
        int heightBuffer = this.toInt((double)heightGap * 0.03);
        heightGap -= heightBuffer;
        GlStateManager.pushMatrix();
        float textScale = 2.0f;
        int textWidth = this.toInt((float)this.fontRenderer.getStringWidth(Integer.toString(this.pokedex.countCaught())) * textScale);
        while (textWidth > this.width / 28) {
            textWidth = this.toInt((float)this.fontRenderer.getStringWidth(Integer.toString(this.pokedex.countCaught())) * (textScale -= 0.05f));
        }
        GlStateManager.scale(textScale, textScale, textScale);
        String caught = String.format("%03d", this.pokedex.countCaught());
        this.fontRenderer.drawString(caught, this.toInt((double)this.width / 4.7 / (double)textScale), this.toInt((float)(this.height / 17) / textScale), 16413256);
        String seen = String.format("%03d", this.pokedex.seenCaught());
        this.fontRenderer.drawString(seen, this.toInt((double)this.width / 2.38 / (double)textScale), this.toInt((float)(this.height / 17) / textScale), 16413256);
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.scale(2.0f, 2.0f, 2.0f);
        boolean withinBounds = false;
        if (!this.searching) {
            int dex = this.currentPage * 30 - 30 + 1;
            block1: for (row = 0; row < 5; ++row) {
                for (int col = 0; col < 6; ++col) {
                    GlStateManager.color(1.0f, 1.0f, 1.0f);
                    int dexNum = dex++;
                    if (dexNum > 905) continue block1;
                    Optional<EnumSpecies> pokemonOpt2 = EnumSpecies.getFromDexUnsafe(dexNum);
                    if (this.pokedex.hasSeen(dexNum) && pokemonOpt2.isPresent()) {
                        this.bindPokeSprite(pokemonOpt2.get());
                        int x = sideBuffer + paddingSplitX + widthGap * col;
                        int y = heightBuffer + paddingSplitY + heightGap * row;
                        if (this.isWithinBounds(mouseX, mouseY, x, y, this.toInt(48.0)) && (this.hoveredDex == 0 || this.hoveredDex == dexNum)) {
                            this.hoveredDex = dexNum;
                            this.hoveredCol = col;
                            this.hoveredRow = row;
                            withinBounds = true;
                            GlStateManager.color(0.75f, 0.75f, 0.75f);
                        } else if (!this.pokedex.hasCaught(dexNum)) {
                            GlStateManager.color(0.4f, 0.4f, 0.4f);
                        }
                        GuiHelper.drawImageQuad((float)x / 2.0f, (float)y / 2.0f, 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                        continue;
                    }
                    int textX = this.toInt((double)(sideBuffer + paddingSplitX + widthGap * col) + 10.909090909090908);
                    int textY = this.toInt((float)(heightBuffer + paddingSplitY + heightGap * row) + 24.0f);
                    this.fontRenderer.drawString(String.format("%03d", dexNum), this.toInt((float)textX / 2.0f), this.toInt((float)textY / 2.0f), 3511710);
                }
            }
        } else {
            int counter = this.currentPage * 30 - 30;
            block3: for (row = 0; row < 5; ++row) {
                for (int col = 0; col < 6; ++col) {
                    GlStateManager.color(1.0f, 1.0f, 1.0f);
                    int indexNum = counter++;
                    if (indexNum > this.searchResults.size() - 1) continue block3;
                    int dex = this.searchResults.get(indexNum).getNationalPokedexInteger();
                    Optional<EnumSpecies> pokemonOpt3 = EnumSpecies.getFromDexUnsafe(dex);
                    if (this.pokedex.hasSeen(dex) && pokemonOpt3.isPresent()) {
                        this.bindPokeSprite(pokemonOpt3.get());
                        int x = sideBuffer + paddingSplitX + widthGap * col;
                        int y = heightBuffer + paddingSplitY + heightGap * row;
                        if (this.isWithinBounds(mouseX, mouseY, x, y, this.toInt(48.0)) && (this.hoveredDex == 0 || this.hoveredDex == dex)) {
                            this.hoveredDex = dex;
                            this.hoveredCol = col;
                            this.hoveredRow = row;
                            withinBounds = true;
                            GlStateManager.color(0.75f, 0.75f, 0.75f);
                        } else if (!this.pokedex.hasCaught(dex)) {
                            GlStateManager.color(0.4f, 0.4f, 0.4f);
                        }
                        GuiHelper.drawImageQuad((float)x / 2.0f, (float)y / 2.0f, 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                        continue;
                    }
                    int textX = this.toInt((double)(sideBuffer + paddingSplitX + widthGap * col) + 10.909090909090908);
                    int textY = this.toInt((float)(heightBuffer + paddingSplitY + heightGap * row) + 24.0f);
                    this.fontRenderer.drawString(String.format("%03d", dex), this.toInt((float)textX / 2.0f), this.toInt((float)textY / 2.0f), 3511710);
                }
            }
        }
        if (!withinBounds) {
            this.hoveredDex = 0;
            this.hoveredCol = 0;
            this.hoveredRow = 0;
        }
        GlStateManager.popMatrix();
        if (this.hoveredDex != 0 && (pokemonOpt = EnumSpecies.getFromDexUnsafe(this.hoveredDex)).isPresent()) {
            String displayText = String.format("No. %03d %s", this.hoveredDex, pokemonOpt.get().name);
            int offsetX = this.fontRenderer.getStringWidth(displayText) / 2;
            int textX = this.toInt((float)(sideBuffer + (paddingSplitX - offsetX) + widthGap * this.hoveredCol) + 24.0f);
            int textY = this.toInt(heightBuffer + paddingSplitY + heightGap * this.hoveredRow - 2);
            this.fontRenderer.drawString(displayText, textX, textY, 9501938);
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        Optional<EnumSpecies> pokemonOpt;
        this.searchText.mouseClicked(mouseX, mouseY, mouseButton);
        if (this.hoveredDex != 0 && (pokemonOpt = EnumSpecies.getFromDexUnsafe(this.hoveredDex)).isPresent()) {
            GuiPokedexInfo pokedexInfo = new GuiPokedexInfo(pokemonOpt.get());
            if (!this.searchText.getText().isEmpty()) {
                pokedexInfo.setPrevSearchText(this.searchText.getText());
            }
            Minecraft.getMinecraft().displayGuiScreen(pokedexInfo);
        }
        super.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        if (this.searchText.isFocused()) {
            if (keyCode == 1) {
                this.searchText.setFocused(false);
                return;
            }
            String beforeText = this.searchText.getText();
            this.searchText.textboxKeyTyped(typedChar, keyCode);
            this.searching = !this.searchText.getText().isEmpty();
            this.updateSearchResults();
            if (!beforeText.equalsIgnoreCase(this.searchText.getText())) {
                this.currentPage = 1;
            }
            return;
        }
        super.keyTyped(typedChar, keyCode);
        if (Arrays.asList(203, 205, 208, 200, 17, 32, 30, 31).contains(keyCode)) {
            int amount = Arrays.asList(203, 208, 30, 31).contains(keyCode) ? 1 : -1;
            int futurePage = this.currentPage -= amount;
            this.currentPage = futurePage > this.getMaxPages() ? 1 : (futurePage < 1 ? this.getMaxPages() : futurePage);
        }
    }

    private void updateSearchResults() {
        this.searchResults.clear();
        if (this.searching) {
            this.targetSearch = this.searchText.getText();
            for (EnumSpecies poke : EnumSpecies.values()) {
                if (!poke.name.toLowerCase().contains(this.targetSearch.toLowerCase())) continue;
                this.searchResults.add(poke);
            }
        } else {
            this.targetSearch = null;
        }
    }

    private void bindPokeSprite(EnumSpecies pokemon) {
        byte form = pokemon.getFormEnum(0).getForm();
        String filePath = String.format("%03d", pokemon.getNationalPokedexInteger());
        filePath = filePath + SpriteHelper.getSpriteExtra(pokemon.name, form);
        this.mc.renderEngine.bindTexture(GuiHelper.checkSpecialTexture(filePath, "", false));
    }

    @Override
    public void handleMouseInput() throws IOException {
        this.handleMouseScroll();
        super.handleMouseInput();
    }

    protected void handleMouseScroll() {
        int j = Mouse.getEventDWheel();
        if (j != 0) {
            int futurePage = this.currentPage -= (j = j > 0 ? 1 : -1);
            this.currentPage = futurePage > this.getMaxPages() ? 1 : (futurePage < 1 ? this.getMaxPages() : futurePage);
        }
    }

    public int getMaxPages() {
        if (this.searching) {
            return this.toInt(Math.ceil((double)this.searchResults.size() / 30.0));
        }
        return this.toInt(Math.ceil(30.166666666666668));
    }

    public int getPageFor(int targetDex) {
        for (int i = 1; i < this.getMaxPages() + 1; ++i) {
            int minDex = i * 30 - 30 + 1;
            int maxDex = minDex + 30 - 1;
            if (targetDex < minDex || targetDex > maxDex) continue;
            return i;
        }
        return 1;
    }

    public String getDexRange(int page) {
        int minDex = page * 30 - 30 + 1;
        int maxDex = minDex + 30 - 1;
        if (maxDex > 905) {
            maxDex = 905;
        }
        if (this.searching) {
            if (this.searchResults.isEmpty()) {
                minDex = 0;
            }
            maxDex = this.searchResults.size();
        }
        return String.format("(%s-%s)", minDex, maxDex);
    }

    private int toInt(double value) {
        return (int)value;
    }

    private boolean isWithinBounds(int mouseX, int mouseY, int minX, int minY, int size) {
        return mouseX >= minX && mouseX <= minX + size && mouseY >= minY && mouseY <= minY + size;
    }
}

