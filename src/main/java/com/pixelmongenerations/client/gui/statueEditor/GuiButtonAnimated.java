/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.statueEditor;

import com.pixelmongenerations.client.gui.elements.GuiButtonToggle;
import java.util.function.Consumer;

public class GuiButtonAnimated
extends GuiButtonToggle {
    public GuiButtonAnimated(int buttonID, int x, int y, int width, int height, boolean on, Consumer<Boolean> consumer) {
        super(buttonID, x, y, width, height, "gui.statue.animated", "gui.statue.static", on, consumer);
    }
}

