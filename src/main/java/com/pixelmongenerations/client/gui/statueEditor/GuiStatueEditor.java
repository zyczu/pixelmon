/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 */
package com.pixelmongenerations.client.gui.statueEditor;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.elements.GuiButtonToggle;
import com.pixelmongenerations.client.gui.elements.GuiContainerDropDown;
import com.pixelmongenerations.client.gui.elements.GuiDropDown;
import com.pixelmongenerations.client.gui.statueEditor.GuiButtonAnimated;
import com.pixelmongenerations.client.gui.statueEditor.GuiButtonGmaxModel;
import com.pixelmongenerations.client.render.RenderStatue;
import com.pixelmongenerations.common.entity.pixelmon.EntityStatue;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumStatueTextureType;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.network.packetHandlers.statueEditor.EnumStatuePacketMode;
import com.pixelmongenerations.core.network.packetHandlers.statueEditor.StatuePacketServer;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.input.Keyboard;

public class GuiStatueEditor
extends GuiContainerDropDown {
    public static int[] statueId;
    private final int entityid;
    private final boolean adminPlaced;
    private EntityStatue statue;
    GuiButton animationGuiButton;
    GuiButton modelGuiButton;
    GuiTextField tbName;
    GuiTextField tbLabel;
    GuiTextField tbFrame;
    int controlWidth = 320;
    int controlLeft;
    int controlHeight = 114;
    int controlTop;
    ModelBase lastModel;
    int lastFrame;
    static float spinCount;
    private EntityStatue renderTarget;
    private GuiTextField customSpecial;

    public GuiStatueEditor(int entityid, boolean adminPlaced) {
        EntityStatue statue;
        this.entityid = entityid;
        this.adminPlaced = adminPlaced;
        Keyboard.enableRepeatEvents((boolean)true);
        Entity entity = Minecraft.getMinecraft().world.getEntityByID(entityid);
        if (entity instanceof EntityStatue && PixelmonMethods.isIDSame((statue = (EntityStatue)entity).getPokemonId(), statueId)) {
            this.statue = statue;
        }
    }

    @Override
    public void initGui() {
        if (this.statue == null) {
            GuiHelper.closeScreen();
            return;
        }
        super.initGui();
        this.controlLeft = this.width / 2 - this.controlWidth / 2;
        this.controlTop = this.height - this.controlHeight - 20;
        int buttonHeight = 20;
        int textFieldHeight = 17;
        this.buttonList.add(new GuiButton(0, this.controlLeft + 2, this.height - buttonHeight - 22, 80, buttonHeight, I18n.translateToLocal("gui.update.text")));
        ArrayList<EnumStatueTextureType> textures = new ArrayList<EnumStatueTextureType>(Arrays.asList(EnumStatueTextureType.values()));
        BaseStats stats = this.statue.initializeBaseStatsIfNull();
        if (stats == null) {
            GuiHelper.closeScreen();
            return;
        }
        this.addDropDown(new GuiDropDown<EnumStatueTextureType>(textures, this.statue.getTextureType(), this.controlLeft + 228, this.controlTop + 20, 80, 150).setGetOptionString(EnumStatueTextureType::getLocalizedName).setOnSelected(texture -> Pixelmon.NETWORK.sendToServer(new StatuePacketServer(EnumStatuePacketMode.SetTextureType, statueId, texture.toString()))).setInactiveTop(this.controlTop + 20));
        this.addDropDown(new GuiDropDown<IEnumSpecialTexture>(this.statue.getSpecies().getSpecialTextures(this.statue.getForm()), this.statue.getSpecies().getSpecialTexture(this.statue.getFormEnum(), this.statue.getSpecialTextureId()), this.controlLeft + 228, this.controlTop + 44, 80, 150).setGetOptionString(IEnumSpecialTexture::name).setOnSelected(specialTexture -> Pixelmon.NETWORK.sendToServer(new StatuePacketServer(EnumStatuePacketMode.SetSpecial, statueId, specialTexture.getId()))).setInactiveTop(this.controlTop + 44));
        List forms = this.statue.getSpecies().getAllForms().stream().map(this.statue.getSpecies()::getFormEnum).collect(Collectors.toList());
        if (EnumSpecies.hasNoFormForm(this.statue.getSpecies())) {
            forms.add(EnumForms.NoForm);
        }
        this.addDropDown(new GuiDropDown<IEnumForm>(forms, this.statue.getFormEnum(), this.controlLeft + 228, this.controlTop + 68, 80, 150).setGetOptionString(iEnumForm -> iEnumForm == EnumForms.NoForm ? "None" : iEnumForm.getProperName()).setOnSelected(f -> {
            byte form = f.getForm();
            Pixelmon.NETWORK.sendToServer(new StatuePacketServer(EnumStatuePacketMode.SetForm, statueId, form));
            boolean hasMega = this.statue.baseStats.pokemon.hasMega();
            if (hasMega && form > 0 && this.statue.getIsFlying()) {
                this.changeFlying(false);
            }
            this.statue.setForm(form);
        }).setInactiveTop(this.controlTop + 68));
        this.addDropDown(new GuiDropDown<EnumGrowth>(EnumGrowth.getProperOrder(), this.statue.getGrowth(), this.controlLeft + 45, this.controlTop + 4, 80, 150).setGetOptionString(EnumGrowth::getLocalizedName).setOnSelected(growth -> Pixelmon.NETWORK.sendToServer(new StatuePacketServer(EnumStatuePacketMode.SetGrowth, statueId, growth.toString()))).setInactiveTop(this.controlTop + 35));
        this.buttonList.add(new GuiButton(1, this.controlLeft + 85, this.height - 22 - buttonHeight, 50, buttonHeight, I18n.translateToLocal("gui.save.text")));
        this.tbFrame = null;
        if (this.statue.isSmd() && this.statue.getFrameCount() > 0) {
            int animTop = this.controlTop + 73;
            List<String> anims = this.statue.getAllAnimations();
            this.addDropDown(new GuiDropDown<String>(anims, this.statue.getAnimation(), this.controlLeft + 48, animTop + Math.max(-16, (anims.size() - 1) * -10), 60, 100).setGetOptionString(animation -> I18n.translateToLocal("gui.model." + animation)).setOnSelected(this::selectAnimation).setInactiveTop(this.controlTop + 73));
            this.tbFrame = new GuiTextField(9, this.mc.fontRenderer, this.controlLeft + 158, this.controlTop + 70, 40, textFieldHeight);
            this.tbFrame.setText(this.statue.getAnimationFrame() + "");
            this.lastFrame = this.statue.getAnimationFrame();
        }
        if (this.statue.hasFlyingModel()) {
            String extra = this.statue.getIsFlying() ? "flying" : "standing";
            this.modelGuiButton = new GuiButton(10, this.controlLeft + 137, this.controlTop + 30, 80, buttonHeight, I18n.translateToLocal("gui.model." + extra));
            this.buttonList.add(this.modelGuiButton);
        }
        this.tbName = new GuiTextField(6, this.mc.fontRenderer, this.controlLeft + 45, this.controlTop + 10, 90, textFieldHeight);
        this.tbName.setText(this.statue.getLocalizedName());
        this.tbLabel = new GuiTextField(7, this.mc.fontRenderer, this.controlLeft + 45, this.controlTop + 51, 180, textFieldHeight);
        this.tbLabel.setText(this.statue.getLabel());
        this.lastModel = this.statue.getModel();
        this.customSpecial = new GuiTextField(12, this.fontRenderer, this.controlLeft + this.controlWidth - 92, this.height - 39, 90, textFieldHeight);
        this.customSpecial.setText(this.statue.getSpecText());
        this.buttonList.add(new GuiButtonAnimated(13, this.controlLeft + 157, this.controlTop + 2, 50, buttonHeight, this.statue.isAnimated(), a -> Pixelmon.NETWORK.sendToServer(new StatuePacketServer(EnumStatuePacketMode.SetAnimated, statueId, (boolean)a))));
        if (this.statue != null && this.statue.getSpecies().hasGmaxForm()) {
            this.buttonList.add(new GuiButtonGmaxModel(14, this.controlLeft + 157, this.controlTop + 6 + buttonHeight, 50, buttonHeight, this.statue.isGmaxModel(), a -> Pixelmon.NETWORK.sendToServer(new StatuePacketServer(EnumStatuePacketMode.SetGmaxModel, statueId, (boolean)a))));
        }
    }

    private void selectAnimation(String nextAnim) {
        this.statue.setAnimation(nextAnim);
        Pixelmon.NETWORK.sendToServer(new StatuePacketServer(EnumStatuePacketMode.SetAnimation, statueId, nextAnim));
    }

    @Override
    protected void keyTyped(char key, int keyCode) throws IOException {
        int eventKey = Keyboard.getEventKey();
        if (eventKey == 1 || eventKey == 28) {
            this.closeScreen();
            return;
        }
        if (this.tbName == null) {
            this.initGui();
        }
        ArrayList<GuiTextField> guiTextFields = new ArrayList<GuiTextField>(4);
        this.tbName.textboxKeyTyped(key, keyCode);
        guiTextFields.add(this.tbName);
        this.tbLabel.textboxKeyTyped(key, keyCode);
        guiTextFields.add(this.tbLabel);
        if (this.customSpecial != null) {
            this.customSpecial.textboxKeyTyped(key, keyCode);
            guiTextFields.add(this.customSpecial);
        }
        if (this.tbFrame != null) {
            this.tbFrame.textboxKeyTyped(key, keyCode);
            guiTextFields.add(this.tbFrame);
            if (this.tbFrame.isFocused()) {
                int frame = 0;
                if (!this.tbFrame.getText().isEmpty()) {
                    try {
                        frame = Integer.parseInt(this.tbFrame.getText());
                    }
                    catch (NumberFormatException exc) {
                        this.tbFrame.setText("");
                    }
                }
                if (frame != this.lastFrame) {
                    if (frame < 0) {
                        frame = 0;
                        this.tbFrame.setText("");
                    } else if (frame >= this.statue.getFrameCount()) {
                        frame = this.statue.getFrameCount() - 1;
                        this.tbFrame.setText(String.valueOf(frame));
                    }
                    if (frame != this.lastFrame) {
                        this.statue.setAnimationFrame(frame);
                        Pixelmon.NETWORK.sendToServer(new StatuePacketServer(EnumStatuePacketMode.SetAnimationFrame, statueId, frame));
                        this.lastFrame = frame;
                    }
                }
            }
        }
        GuiTextField[] GuiTextFieldsArray = new GuiTextField[guiTextFields.size()];
        GuiHelper.switchFocus(keyCode, guiTextFields.toArray(GuiTextFieldsArray));
    }

    private void closeScreen() {
        EnumSpecies e;
        if (!this.tbName.getText().equalsIgnoreCase(this.statue.getPokemonName()) && (e = EnumSpecies.getFromNameAnyCase(this.tbName.getText())) != null) {
            Pixelmon.NETWORK.sendToServer(new StatuePacketServer(EnumStatuePacketMode.SetName, statueId, e.name));
        }
        if (!this.tbLabel.getText().equalsIgnoreCase(this.statue.getLabel())) {
            Pixelmon.NETWORK.sendToServer(new StatuePacketServer(EnumStatuePacketMode.SetLabel, statueId, this.tbLabel.getText()));
        }
        if (this.customSpecial != null && !this.customSpecial.getText().equalsIgnoreCase(this.statue.getSpecText())) {
            Pixelmon.NETWORK.sendToServer(new StatuePacketServer(EnumStatuePacketMode.SetCustomSpecial, statueId, this.customSpecial.getText()));
        }
        Pixelmon.NETWORK.sendToServer(new StatuePacketServer(EnumStatuePacketMode.SetAdminPlaced, statueId, this.adminPlaced));
        GuiHelper.closeScreen();
        this.mc.setIngameFocus();
    }

    @Override
    protected void mouseClickedUnderMenus(int x, int y, int mouseButton) throws IOException {
        if (this.tbName == null) {
            this.initGui();
        }
        this.tbName.mouseClicked(x, y, mouseButton);
        this.tbLabel.mouseClicked(x, y, mouseButton);
        if (this.tbFrame != null) {
            this.tbFrame.mouseClicked(x, y, mouseButton);
        }
        if (this.customSpecial != null) {
            this.customSpecial.mouseClicked(x, y, mouseButton);
        }
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) throws IOException {
        super.actionPerformed(guiButton);
        if (this.tbName == null) {
            this.initGui();
        }
        if (guiButton.id == 0) {
            EnumSpecies e = EnumSpecies.getFromNameAnyCase(this.tbName.getText());
            if (e != null) {
                Pixelmon.NETWORK.sendToServer(new StatuePacketServer(EnumStatuePacketMode.SetName, statueId, e.name));
            } else {
                this.tbName.setText(I18n.translateToLocal("pixelmon." + this.statue.getPokemonName().toLowerCase() + ".name"));
            }
        } else if (guiButton.id == 1) {
            this.closeScreen();
        } else if (guiButton.id == 10) {
            this.changeFlying(!this.statue.getIsFlying());
        } else if (guiButton instanceof GuiButtonToggle) {
            ((GuiButtonToggle)guiButton).toggle();
        }
    }

    private void changeFlying(boolean isFlying) {
        String extra;
        this.statue.setIsFlying(isFlying);
        Pixelmon.NETWORK.sendToServer(new StatuePacketServer(EnumStatuePacketMode.SetModelStanding, statueId, isFlying));
        String string = extra = isFlying ? "flying" : "standing";
        if (this.modelGuiButton != null) {
            this.modelGuiButton.displayString = I18n.translateToLocal("gui.model." + extra);
        }
    }

    @Override
    protected void drawBackgroundUnderMenus(float p_146976_1_, int p_146976_2_, int par3) {
        EntityStatue ep;
        this.drawGradientRect(this.controlLeft, this.controlTop, this.controlLeft + this.controlWidth, this.controlTop + this.controlHeight, -1713512995, -1713512995);
        if (this.statue.getModel() != this.lastModel) {
            ScaledResolution scaledresolution = new ScaledResolution(this.mc);
            int i = scaledresolution.getScaledWidth();
            int j = scaledresolution.getScaledHeight();
            this.setWorldAndResolution(this.mc, i, j);
            this.lastModel = this.statue.getModel();
        }
        String text = I18n.translateToLocal("gui.trainereditor.name");
        this.mc.fontRenderer.drawString(text, this.controlLeft + 40 - this.mc.fontRenderer.getStringWidth(text), this.controlTop + 16, 0x555555);
        text = I18n.translateToLocal("gui.trainereditor.growth");
        this.mc.fontRenderer.drawString(text, this.controlLeft + 40 - this.mc.fontRenderer.getStringWidth(text), this.controlTop + 36, 0x555555);
        if (this.tbName != null) {
            this.tbName.drawTextBox();
        }
        text = I18n.translateToLocal("gui.label.text");
        this.mc.fontRenderer.drawString(text, this.controlLeft + 40 - this.mc.fontRenderer.getStringWidth(text), this.controlTop + 56, 0x555555);
        if (this.tbLabel != null) {
            this.tbLabel.drawTextBox();
        }
        text = I18n.translateToLocal("gui.texture.text");
        this.mc.fontRenderer.drawString(text, this.controlLeft + 268 - this.mc.fontRenderer.getStringWidth(text) / 2, this.controlTop + 10, 0x555555);
        text = I18n.translateToLocal("gui.special.text");
        this.mc.fontRenderer.drawString(text, this.controlLeft + 268 - this.mc.fontRenderer.getStringWidth(text) / 2, this.controlTop + 34, 0x555555);
        text = I18n.translateToLocal("gui.trainereditor.form");
        this.mc.fontRenderer.drawString(text, this.controlLeft + 268 - this.mc.fontRenderer.getStringWidth(text) / 2, this.controlTop + 58, 0x555555);
        text = I18n.translateToLocal("gui.customspecial.text");
        this.mc.fontRenderer.drawString(text, this.controlLeft + 226 - this.mc.fontRenderer.getStringWidth(text), this.controlTop + 100, 0x555555);
        if (this.tbFrame != null) {
            text = I18n.translateToLocal("gui.model.animation");
            this.mc.fontRenderer.drawString(text, this.controlLeft + 46 - this.mc.fontRenderer.getStringWidth(text), this.controlTop + 74, 0x555555);
            this.tbFrame.drawTextBox();
            text = I18n.translateToLocal("gui.model.frame");
            this.mc.fontRenderer.drawString(text, this.controlLeft + 154 - this.mc.fontRenderer.getStringWidth(text), this.controlTop + 74, 0x555555);
            text = "/" + (this.statue.getFrameCount() - 1);
            this.mc.fontRenderer.drawString(text, this.controlLeft + 200, this.controlTop + 74, 0x555555);
        }
        if (this.customSpecial != null) {
            this.customSpecial.drawTextBox();
        }
        if ((ep = this.statue) != null && ep.getModel() != null) {
            GuiStatueEditor.drawEntityToScreen(this.width / 2 - 10, this.height / 2 + 20, 200, 200, ep, par3, true);
        }
    }

    public static void drawEntityToScreen(int x, int y, int w, int l, EntityStatue e, float pt, boolean spin) {
        GlStateManager.pushMatrix();
        GlStateManager.enableColorMaterial();
        GlStateManager.enableDepth();
        GlStateManager.translate(x, y, 100.0f);
        float eheight = (float)l / e.height / 4.0f;
        float ewidth = (float)l / e.width / 4.0f;
        float scalar = Math.max(eheight, ewidth);
        GlStateManager.scale(scalar, scalar, scalar);
        GlStateManager.rotate(180.0f, 0.0f, 0.0f, 1.0f);
        if (spin) {
            GlStateManager.rotate(spinCount += 0.66f, 0.0f, 1.0f, 0.0f);
        }
        RenderHelper.enableStandardItemLighting();
        try {
            RenderManager renderManager = Minecraft.getMinecraft().getRenderManager();
            Render entityClassRenderObject = renderManager.getEntityClassRenderObject(EntityStatue.class);
            RenderStatue rp = (RenderStatue)entityClassRenderObject;
            rp.renderStatue(e, 0.0, e.getYOffset(), 0.0, pt, true);
            renderManager.playerViewY = 180.0f;
        }
        catch (Exception exception) {
            // empty catch block
        }
        GlStateManager.popMatrix();
    }

    public EntityStatue getRenderTarget(String name) {
        if (this.renderTarget != null) {
            return this.renderTarget;
        }
        if (name == null || name.equals("???") || !EnumSpecies.hasPokemon(name)) {
            return null;
        }
        this.renderTarget = new EntityStatue(this.mc.world);
        this.renderTarget.init(name);
        this.renderTarget.setGrowth(EnumGrowth.Ordinary);
        return this.renderTarget;
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
    }

    @Override
    public void drawDefaultBackground() {
        this.drawGradientRect(0, 0, this.width, this.height, -1725816286, -1725816286);
    }

    @Override
    public void drawBackground(int tint) {
        super.drawBackground(tint);
    }

    static {
        spinCount = 0.0f;
    }
}

