/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.npc;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.chooseMoveset.GuiMoveList;
import com.pixelmongenerations.client.gui.chooseMoveset.IMoveClicked;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCRelearner;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonMovesetData;
import com.pixelmongenerations.core.network.packetHandlers.LearnMove;
import java.util.ArrayList;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;

public class GuiRelearner
extends GuiContainer
implements IMoveClicked {
    PixelmonData pokemon;
    NPCRelearner relearner;
    ItemStack cost;
    public static ArrayList<Attack> attackList;
    int listTop;
    int listLeft;
    int listHeight;
    int listWidth;
    GuiMoveList attackListGui;

    public GuiRelearner(PixelmonData pokemon) {
        super(new ContainerEmpty());
        Optional<NPCRelearner> entityNPCOptional = EntityNPC.locateNPCClient(Minecraft.getMinecraft().world, ServerStorageDisplay.NPCInteractId, NPCRelearner.class);
        if (!entityNPCOptional.isPresent()) {
            GuiHelper.closeScreen();
            return;
        }
        this.relearner = entityNPCOptional.get();
        this.cost = this.relearner.getCost();
        this.pokemon = pokemon;
        attackList = new ArrayList();
        for (int eggMoveID : pokemon.eggMoves) {
            attackList.add(new Attack(eggMoveID));
        }
        ArrayList<Attack> levelUpList = DatabaseMoves.getMovesetUpToLevel(pokemon.getBaseStats().id, 100).isEmpty() ? DatabaseMoves.getMovesetUpToLevel(pokemon.getBaseStats().form, pokemon.lvl, false) : DatabaseMoves.getMovesetUpToLevel(pokemon.getBaseStats().id, pokemon.lvl, false);
        for (int i = levelUpList.size() - 1; i >= 0; --i) {
            Attack move = levelUpList.get(i);
            if (attackList.contains(move)) continue;
            attackList.add(move);
        }
        for (PixelmonMovesetData data : pokemon.moveset) {
            if (data == null) continue;
            attackList.remove(data.getAttack());
        }
        this.listHeight = 150;
        this.listWidth = 90;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.listTop = this.height / 2 - 62;
        this.listLeft = this.width / 2 - 40;
        this.attackListGui = new GuiMoveList(this, attackList, this.listWidth, this.listHeight, this.listTop, this.listLeft, this.mc);
        this.buttonList.add(new GuiButton(1, this.width / 2 + 155, this.height / 2 + 90, 50, 20, I18n.translateToLocal("gui.cancel.text")));
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float mFloat, int mouseX, int mouseY) {
        this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
        GuiHelper.drawImageQuad(0.0, 0.0, this.width, this.height, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        String text = I18n.translateToLocal("gui.choosemoveset.relearnmove");
        this.mc.fontRenderer.drawString(text, this.width / 2 - this.fontRenderer.getStringWidth(text) / 2, this.height / 2 - 110, 0);
        text = I18n.translateToLocal("gui.choosemoveset.choosemove");
        this.mc.fontRenderer.drawString(text, this.width / 2 - 15 - this.fontRenderer.getStringWidth(text) / 2, this.height / 2 - 92, 0);
        this.attackListGui.drawScreen(mouseX, mouseY, mFloat);
        GuiHelper.bindPokemonSprite(this.pokemon, this.mc);
        GlStateManager.color(1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(this.width / 2 - 211, this.height / 2 - 98 - (this.pokemon.isGen6Sprite() ? 3 : 0), 84.0, 84.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
        GuiHelper.drawAttackInfoBox(this.zLevel, this.width, this.height);
        for (int i = 0; i < attackList.size(); ++i) {
            if (!this.attackListGui.isMouseOver(i, mouseX, mouseY)) continue;
            GuiHelper.drawAttackInfoList(attackList.get(i), this.width, this.height);
        }
        if (this.cost != null) {
            text = I18n.translateToLocal("gui.choosemoveset.cost");
            this.mc.fontRenderer.drawString(text, this.width / 2 + 130 - this.fontRenderer.getStringWidth(text), this.height / 2 + 60, 0);
            this.itemRender.renderItemAndEffectIntoGUI(this.cost, this.width / 2 + 130, this.height / 2 + 55);
            this.itemRender.renderItemOverlayIntoGUI(this.mc.fontRenderer, this.cost, this.width / 2 + 130, this.height / 2 + 55, null);
        }
    }

    @Override
    public void elementClicked(ArrayList<Attack> list, int index) {
        Attack a = attackList.get(index);
        this.mc.player.closeScreen();
        Pixelmon.NETWORK.sendToServer(new LearnMove(this.pokemon.pokemonID, a.getAttackBase().attackIndex, this.relearner.getId()));
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        this.mc.player.closeScreen();
    }
}

