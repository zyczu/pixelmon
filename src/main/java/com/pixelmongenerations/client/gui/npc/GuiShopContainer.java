/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Mouse
 */
package com.pixelmongenerations.client.gui.npc;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.npc.ClientShopItem;
import com.pixelmongenerations.common.entity.npcs.registry.EnumBuySell;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.storage.ClientData;
import java.io.IOException;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.input.Mouse;

public abstract class GuiShopContainer
extends GuiContainer {
    public static ArrayList<ClientShopItem> buyItems;
    public static ArrayList<ClientShopItem> sellItems;
    protected int selectedItem = -1;
    protected int quantity = 1;
    protected float floatQuantity = 1.0f;
    private float incrementInterval = 8.0f;
    private static final float START_INTERVAL = 8.0f;
    protected int buyStartIndex = 0;
    protected int sellStartIndex = 0;
    protected boolean allowMultiple = true;
    EnumBuySell currentTab = EnumBuySell.Buy;

    public GuiShopContainer() {
        super(new ContainerEmpty());
    }

    @Override
    public void updateScreen() {
    }

    protected void handleMouseScroll() {
        int j = Mouse.getEventDWheel();
        if (j != 0) {
            if (j > 0) {
                j = 1;
            }
            if (j < 0) {
                j = -1;
            }
            if (this.currentTab == EnumBuySell.Buy) {
                this.buyStartIndex -= j;
                if (this.buyStartIndex + 6 >= buyItems.size()) {
                    this.buyStartIndex = buyItems.size() - 6;
                }
                if (this.buyStartIndex < 0) {
                    this.buyStartIndex = 0;
                }
            } else {
                this.sellStartIndex -= j;
                if (this.sellStartIndex + 6 >= sellItems.size()) {
                    this.sellStartIndex = sellItems.size() - 6;
                }
                if (this.sellStartIndex < 0) {
                    this.sellStartIndex = 0;
                }
            }
        }
    }

    protected boolean isBuyMiniScreenVisible() {
        return this.selectedItem != -1;
    }

    @Override
    public void drawBackground(int par1) {
    }

    @Override
    public void drawDefaultBackground() {
    }

    protected void clickBuyScreen(int mouseX, int mouseY) {
        if (mouseX > this.width / 2 + 51 && mouseX < this.width / 2 + 51 + 16 && mouseY > this.height / 2 - 61 && mouseY < this.height / 2 - 61 + 10) {
            if (this.currentTab == EnumBuySell.Buy) {
                if (this.buyStartIndex > 0) {
                    --this.buyStartIndex;
                    return;
                }
            } else if (this.sellStartIndex > 0) {
                --this.sellStartIndex;
                return;
            }
        }
        if (mouseX > this.width / 2 + 51 && mouseX < this.width / 2 + 51 + 16 && mouseY > this.height / 2 + 78 && mouseY < this.height / 2 + 78 + 10) {
            if (this.currentTab == EnumBuySell.Buy) {
                if (this.buyStartIndex + 6 < buyItems.size()) {
                    ++this.buyStartIndex;
                    return;
                }
            } else if (this.sellStartIndex + 6 < sellItems.size()) {
                ++this.sellStartIndex;
                return;
            }
        }
        if (mouseX > this.width / 2 + 139 && mouseX < this.width / 2 + 139 + 14 && mouseY > this.height / 2 + 79 && mouseY < this.height / 2 + 79 + 17) {
            this.closeScreen();
        } else {
            float h;
            int buyPrice;
            int leftLimit = this.width / 2 - 28;
            if (this.currentTab == EnumBuySell.Buy) {
                for (buyPrice = this.buyStartIndex; buyPrice < 6 + this.buyStartIndex; ++buyPrice) {
                    if (buyPrice >= buyItems.size()) continue;
                    h = 21.0f;
                    float topLimit = (float)(this.height / 2 - 49) + (float)(buyPrice - this.buyStartIndex) * h;
                    int w = 174;
                    if (mouseX <= leftLimit || mouseX >= leftLimit + w || (float)mouseY <= topLimit || (float)mouseY >= topLimit + h || this.selectedItem == buyPrice) continue;
                    this.selectedItem = buyPrice;
                    this.quantity = 1;
                }
            } else {
                for (buyPrice = this.sellStartIndex; buyPrice < 6 + this.sellStartIndex; ++buyPrice) {
                    if (buyPrice >= sellItems.size()) continue;
                    h = 21.0f;
                    float topLimit = (float)(this.height / 2 - 49) + (float)(buyPrice - this.sellStartIndex) * h;
                    int w = 174;
                    if (mouseX <= leftLimit || mouseX >= leftLimit + w || (float)mouseY <= topLimit || (float)mouseY >= topLimit + h || this.selectedItem == buyPrice) continue;
                    this.selectedItem = buyPrice;
                    this.quantity = 1;
                }
            }
            this.clickBuyMiniScreen(mouseX, mouseY, true);
            if (this.isBuyMiniScreenVisible() && mouseX > this.width / 2 - 94 && mouseX < this.width / 2 - 94 + 49 && mouseY > this.height / 2 - 29 && mouseY < this.height / 2 - 29 + 18) {
                if (this.currentTab == EnumBuySell.Buy) {
                    if (this.selectedItem < buyItems.size() && this.quantity * (buyPrice = buyItems.get(this.selectedItem).getBuy()) <= ClientData.playerMoney) {
                        this.sendBuyPacket();
                        this.selectedItem = -1;
                    }
                } else if (this.selectedItem < sellItems.size()) {
                    ClientShopItem sellItem = sellItems.get(this.selectedItem);
                    ClientData.playerMoney += this.quantity * sellItem.getSell();
                    this.sendSellPacket();
                    sellItem.amount -= this.quantity;
                    if (sellItem.amount == 0) {
                        sellItems.remove(this.selectedItem);
                    }
                    this.selectedItem = -1;
                }
            }
        }
    }

    protected void clickBuyMiniScreen(int mouseX, int mouseY, boolean isInstant) {
        if (this.floatQuantity > 0.0f && !isInstant) {
            this.floatQuantity -= 1.0f / this.incrementInterval;
        } else if (this.isBuyMiniScreenVisible() && this.allowMultiple) {
            if (mouseX > this.width / 2 - 71 && mouseX < this.width / 2 - 71 + 25 && mouseY > this.height / 2 - 58 && mouseY < this.height / 2 - 58 + 6) {
                this.increase(1);
            }
            if (mouseX > this.width / 2 - 71 && mouseX < this.width / 2 - 71 + 25 && mouseY > this.height / 2 - 42 && mouseY < this.height / 2 - 42 + 7) {
                this.decrease(1);
            }
        }
        if (isInstant) {
            this.incrementInterval = 8.0f;
        }
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        if (this.selectedItem == -1) {
            return;
        }
        switch (keyCode) {
            case 17: 
            case 200: {
                this.increase(1);
                break;
            }
            case 31: 
            case 208: {
                this.decrease(1);
                break;
            }
            case 32: 
            case 203: {
                this.decrease(10);
                break;
            }
            case 30: 
            case 205: {
                this.increase(10);
            }
        }
    }

    public void increase(int amount) {
        if (this.currentTab == EnumBuySell.Buy) {
            if ((this.quantity + amount) * buyItems.get(this.selectedItem).getBuy() <= ClientData.playerMoney) {
                this.quantity += amount;
                this.floatQuantity = 2.0f;
                this.decreaseIncrementInterval();
            } else {
                this.quantity = 1;
            }
        } else if (this.quantity + amount <= GuiShopContainer.sellItems.get((int)this.selectedItem).amount) {
            this.quantity += amount;
            this.floatQuantity = 2.0f;
            this.decreaseIncrementInterval();
        } else {
            this.quantity = GuiShopContainer.sellItems.get((int)this.selectedItem).amount;
        }
    }

    public void decrease(int amount) {
        this.quantity -= amount;
        this.floatQuantity = 2.0f;
        if (this.quantity < 1) {
            int buyPrice;
            int n = this.currentTab == EnumBuySell.Buy ? ((buyPrice = buyItems.get(this.selectedItem).getBuy()) > 0 ? (int)Math.floor(ClientData.playerMoney / buyPrice) : 64) : (this.quantity = GuiShopContainer.sellItems.get((int)this.selectedItem).amount);
            if (this.quantity == 0) {
                this.quantity = 1;
            }
        }
        this.decreaseIncrementInterval();
    }

    private void decreaseIncrementInterval() {
        this.incrementInterval = Math.max(0.5f, this.incrementInterval * 0.75f);
    }

    protected void closeScreen() {
        Minecraft.getMinecraft().player.closeScreen();
    }

    protected void sendBuyPacket() {
    }

    protected void sendSellPacket() {
    }

    protected void renderBuyScreen(int mouseX, int mouseY) {
        this.mc.renderEngine.bindTexture(GuiResources.shopkeeper);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(this.width / 2 - 40, this.height / 2 - 102, 197.0, 201.0f, 0.23046875, 0.0, 1.0, 0.78515625, this.zLevel);
        if (this.isBuyMiniScreenVisible() && mouseX > this.width / 2 + 139 && mouseX < this.width / 2 + 139 + 14 && mouseY > this.height / 2 + 79 && mouseY < this.height / 2 + 79 + 17) {
            GuiHelper.drawImageQuad(this.width / 2 + 139, this.height / 2 + 79, 14.0, 17.0f, 0.9296875, 0.87890625, 0.984375, 0.9453125, this.zLevel);
        }
        String moneyLabel = I18n.translateToLocal("gui.shopkeeper.money");
        this.drawString(this.mc.fontRenderer, moneyLabel, this.width / 2 + 118 - this.mc.fontRenderer.getStringWidth(moneyLabel) / 2, this.height / 2 - 90, 0xFFFFFF);
        String playerMoneyLabel = "" + ClientData.playerMoney;
        this.mc.renderEngine.bindTexture(GuiResources.pokedollar);
        GuiHelper.drawImageQuad(this.width / 2 + 118 - this.mc.fontRenderer.getStringWidth(playerMoneyLabel + 8) / 2, this.height / 2 - 78, 6.0, 9.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        this.drawString(this.mc.fontRenderer, playerMoneyLabel, this.width / 2 + 118 - this.mc.fontRenderer.getStringWidth(playerMoneyLabel + 8) / 2 + 8, this.height / 2 - 77, 0xFFFFFF);
        if (this.currentTab == EnumBuySell.Buy) {
            this.mc.renderEngine.bindTexture(GuiResources.shopkeeper);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GuiHelper.drawImageQuad(this.width / 2 - 31, this.height / 2 - 93, 58.0, 30.0f, 0.265625, 0.8828125, 0.4921875, 1.0, this.zLevel);
        } else {
            this.mc.renderEngine.bindTexture(GuiResources.shopkeeper);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GuiHelper.drawImageQuad(this.width / 2 + 28, this.height / 2 - 93, 58.0, 30.0f, 0.265625, 0.8828125, 0.4921875, 1.0, this.zLevel);
        }
        if (this.selectedItem != -1) {
            ClientShopItem clientItem = this.currentTab == EnumBuySell.Buy ? buyItems.get(this.selectedItem) : sellItems.get(this.selectedItem);
            this.mc.renderEngine.bindTexture(new ResourceLocation("pixelmon:textures/gui/shopkeeperitemdesc.png"));
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GuiHelper.drawImageQuad(this.width / 2 - 210, this.height / 2, 170.0, 71.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            String locale = I18n.translateToLocal("gui.shopkeeper." + clientItem.getItemStack().getTranslationKey().toLowerCase());
            if (locale.contains("gui.shopkeeper")) {
                locale = I18n.translateToLocalFormatted("gui.shopkeeper.item.notfound", clientItem.getItemStack().getTranslationKey().toLowerCase());
            }
            this.fontRenderer.drawSplitString(locale, this.width / 2 - 210 + 6, this.height / 2 + 6, 160, 0xFFFFFF);
        }
        this.renderMenu(mouseX, mouseY);
    }

    protected void renderMenu(int mouseX, int mouseY) {
        ItemStack item;
        int i;
        int colour = 0xFFFFFF;
        int leftLimit = this.width / 2 - 28;
        int startIndex = this.currentTab == EnumBuySell.Buy ? this.buyStartIndex : this.sellStartIndex;
        ArrayList<ClientShopItem> listItems = this.currentTab == EnumBuySell.Buy ? buyItems : sellItems;
        for (i = startIndex; i < 6 + startIndex; ++i) {
            if (i >= listItems.size()) continue;
            this.mc.renderEngine.bindTexture(GuiResources.shopkeeper);
            float h = 21.0f;
            float topLimit = (float)(this.height / 2 - 49) + (float)(i - startIndex) * h;
            int w = 174;
            if (mouseX > leftLimit && mouseX < leftLimit + w && (float)mouseY > topLimit && (float)mouseY < topLimit + h || this.selectedItem == i) {
                GuiHelper.drawImageQuad(leftLimit, topLimit, w, 20.0f, 0.27734375, 0.79296875, 0.95703125, 0.87109375, this.zLevel);
            }
            int itemLeft = this.width / 2;
            int itemRight = this.width / 2 + 140;
            item = listItems.get(i).getItemStack();
            String itemName = item.getDisplayName();
            if (this.currentTab == EnumBuySell.Sell) {
                itemName = itemName + " x" + listItems.get((int)i).amount;
            }
            this.drawString(this.mc.fontRenderer, itemName, itemLeft, (int)(topLimit + 7.0f), 0xFFFFFF);
            this.mc.renderEngine.bindTexture(GuiResources.pokedollar);
            String cost = "";
            cost = cost + (this.currentTab == EnumBuySell.Buy ? listItems.get(i).getBuy() : listItems.get(i).getSell());
            int costWidth = this.mc.fontRenderer.getStringWidth(cost);
            GuiHelper.drawImageQuad(itemRight - costWidth - 8, (int)(topLimit + 6.0f), 6.0, 9.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            colour = 0xDDDDDD;
            if (this.currentTab == EnumBuySell.Buy && listItems.get(i).getBuy() > ClientData.playerMoney) {
                colour = 0xFF4444;
            }
            this.mc.fontRenderer.drawString(cost, itemRight - costWidth, (int)(topLimit + 7.0f), colour);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        }
        this.mc.renderEngine.bindTexture(GuiResources.shopkeeper);
        if (startIndex > 0) {
            if (mouseX > this.width / 2 + 51 && mouseX < this.width / 2 + 51 + 16 && mouseY > this.height / 2 - 61 && mouseY < this.height / 2 - 61 + 10) {
                GuiHelper.drawImageQuad(this.width / 2 + 51, this.height / 2 - 61, 17.0, 10.0f, 0.58203125, 0.8828125, 0.6484375, 0.921875, this.zLevel);
            } else {
                GuiHelper.drawImageQuad(this.width / 2 + 51, this.height / 2 - 61, 17.0, 10.0f, 0.65625, 0.8828125, 0.72265625, 0.921875, this.zLevel);
            }
        }
        if (startIndex + 6 < listItems.size()) {
            if (mouseX > this.width / 2 + 51 && mouseX < this.width / 2 + 51 + 16 && mouseY > this.height / 2 + 78 && mouseY < this.height / 2 + 78 + 10) {
                GuiHelper.drawImageQuad(this.width / 2 + 51, this.height / 2 + 78, 17.0, 10.0f, 0.58203125, 0.9375, 0.6484375, 0.9765625, this.zLevel);
            } else {
                GuiHelper.drawImageQuad(this.width / 2 + 51, this.height / 2 + 78, 17.0, 10.0f, 0.65625, 0.9375, 0.72265625, 0.9765625, this.zLevel);
            }
        }
        if (this.isBuyMiniScreenVisible()) {
            this.mc.renderEngine.bindTexture(GuiResources.shopkeeper);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GuiHelper.drawImageQuad(this.width / 2 - 99, this.height / 2 - 102, 59.0, 97.0f, 0.0, 0.0, 0.23046875, 0.37890625, this.zLevel);
            String priceLabel = I18n.translateToLocal("gui.shopkeeper.price");
            this.drawString(this.mc.fontRenderer, priceLabel, this.width / 2 - 69 - this.mc.fontRenderer.getStringWidth(priceLabel) / 2, this.height / 2 - 96, 0xFFFFFF);
            int price = this.currentTab == EnumBuySell.Buy ? listItems.get(this.selectedItem).getBuy() : listItems.get(this.selectedItem).getSell();
            String priceAmount = "" + this.quantity * price;
            this.mc.renderEngine.bindTexture(GuiResources.pokedollar);
            GuiHelper.drawImageQuad(this.width / 2 - 70 - (this.mc.fontRenderer.getStringWidth(priceAmount) + 8) / 2 - 4, this.height / 2 - 84, 6.0, 9.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            if (this.currentTab == EnumBuySell.Buy && this.quantity * price > ClientData.playerMoney) {
                colour = 0xFF4444;
            }
            colour = 0xDDDDDD;
            this.mc.fontRenderer.drawString(priceAmount, this.width / 2 - 69 - (this.mc.fontRenderer.getStringWidth(priceAmount) + 8) / 2 + 4, this.height / 2 - 84, colour);
            String quantityLabel = I18n.translateToLocal("gui.shopkeeper.quantity");
            this.drawString(this.mc.fontRenderer, quantityLabel, this.width / 2 - 69 - this.mc.fontRenderer.getStringWidth(quantityLabel) / 2, this.height / 2 - 69, 0xFFFFFF);
            String quantityAmount = "" + this.quantity;
            this.drawString(this.mc.fontRenderer, quantityAmount, this.width / 2 - 58 - this.mc.fontRenderer.getStringWidth(quantityAmount) / 2, this.height / 2 - 51, 0xFFFFFF);
            if (mouseX > this.width / 2 - 94 && mouseX < this.width / 2 - 94 + 49 && mouseY > this.height / 2 - 29 && mouseY < this.height / 2 - 29 + 18) {
                boolean bl = this.quantity <= listItems.get(this.selectedItem).amount;
                boolean validTransaction = this.currentTab == EnumBuySell.Buy ? this.quantity * price <= ClientData.playerMoney : bl;
                if (validTransaction) {
                    this.mc.renderEngine.bindTexture(GuiResources.shopkeeper);
                    GuiHelper.drawImageQuad(this.width / 2 - 94, this.height / 2 - 29, 49.0, 18.0f, 0.01953125, 0.796875, 0.2109375, 0.8671875, this.zLevel);
                }
            }
            colour = 0xFFFFFF;
            if (this.currentTab == EnumBuySell.Buy && this.quantity * price > ClientData.playerMoney) {
                colour = 0x777777;
            }
            String buyLabel = I18n.translateToLocal(this.currentTab == EnumBuySell.Buy ? "gui.shopkeeper.buy" : "gui.shopkeeper.sell");
            this.mc.fontRenderer.drawString(buyLabel, this.width / 2 - 69 - this.mc.fontRenderer.getStringWidth(buyLabel) / 2, this.height / 2 - 24, colour);
            this.mc.renderEngine.bindTexture(GuiResources.shopkeeper);
            if (this.allowMultiple) {
                if (mouseX > this.width / 2 - 72 && mouseX < this.width / 2 - 72 + 25 && mouseY > this.height / 2 - 58 && mouseY < this.height / 2 - 58 + 6) {
                    GuiHelper.drawImageQuad(this.width / 2 - 63, this.height / 2 - 58, 9.0, 6.0f, 0.09765625, 0.8828125, 0.1328125, 0.90625, this.zLevel);
                } else {
                    GuiHelper.drawImageQuad(this.width / 2 - 63, this.height / 2 - 58, 9.0, 6.0f, 0.140625, 0.8828125, 0.17578125, 0.90625, this.zLevel);
                }
                if (mouseX > this.width / 2 - 72 && mouseX < this.width / 2 - 72 + 25 && mouseY > this.height / 2 - 42 && mouseY < this.height / 2 - 42 + 7) {
                    GuiHelper.drawImageQuad(this.width / 2 - 63, this.height / 2 - 42, 9.0, 6.0f, 0.09765625, 0.9140625, 0.1328125, 0.9375, this.zLevel);
                } else {
                    GuiHelper.drawImageQuad(this.width / 2 - 63, this.height / 2 - 42, 9.0, 6.0f, 0.140625, 0.9140625, 0.17578125, 0.9375, this.zLevel);
                }
            }
            item = listItems.get(this.selectedItem).getItemStack();
            RenderHelper.enableGUIStandardItemLighting();
            this.itemRender.renderItemIntoGUI(item, this.width / 2 - 91, this.height / 2 - 55);
            RenderHelper.disableStandardItemLighting();
        }
        for (i = startIndex; i < 6 + startIndex; ++i) {
            float h = 21.0f;
            float topLimit = (float)(this.height / 2 - 49) + (float)(i - startIndex) * h;
            if (i >= listItems.size()) continue;
            ItemStack item2 = listItems.get(i).getItemStack();
            RenderHelper.enableGUIStandardItemLighting();
            this.itemRender.renderItemIntoGUI(item2, leftLimit + 2, (int)(topLimit + 2.0f));
            RenderHelper.disableStandardItemLighting();
        }
    }
}

