/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.npcEditor;

import com.pixelmongenerations.client.gui.npcEditor.GuiTrainerEditor;
import com.pixelmongenerations.client.gui.npcEditor.GuiTrainerEditorPartyScreen;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiIndividualEditorBase;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.npc.DeleteTrainerPokemon;
import com.pixelmongenerations.core.network.packetHandlers.npc.NPCServerPacket;
import com.pixelmongenerations.core.network.packetHandlers.npc.UpdateTrainerPokemon;
import java.util.List;

public class GuiTrainerEditorPokemonScreen
extends GuiIndividualEditorBase {
    public GuiTrainerEditorPokemonScreen(int index, String titleText) {
        super(GuiTrainerEditor.pokemonList.get(index), titleText);
        int numPokemon = GuiTrainerEditor.pokemonList.size();
        if (index >= numPokemon && (index = numPokemon - 1) < 0) {
            return;
        }
        this.p.order = index;
        this.p.boxNumber = GuiTrainerEditor.currentTrainerID;
    }

    @Override
    protected boolean showDeleteButton() {
        return GuiTrainerEditor.pokemonList.size() > 1;
    }

    @Override
    protected void changePokemon(EnumSpecies newPokemon) {
        Pixelmon.NETWORK.sendToServer(new NPCServerPacket(GuiTrainerEditor.currentTrainerID, newPokemon, this.p.order));
    }

    @Override
    protected void deletePokemon() {
        Pixelmon.NETWORK.sendToServer(new DeleteTrainerPokemon(GuiTrainerEditor.currentTrainerID, this.p.order));
        this.mc.displayGuiScreen(new GuiTrainerEditorPartyScreen());
    }

    @Override
    protected void saveAndClose() {
        Pixelmon.NETWORK.sendToServer(new UpdateTrainerPokemon(this.p));
        this.mc.displayGuiScreen(new GuiTrainerEditorPartyScreen());
    }

    @Override
    public List<PixelmonData> getPokemonList() {
        return GuiTrainerEditor.pokemonList;
    }
}

