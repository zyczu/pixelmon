/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.npcEditor;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.battles.rules.GuiBattleRulesBase;
import com.pixelmongenerations.client.gui.npcEditor.GuiTrainerEditor;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.npc.NPCServerPacket;
import java.io.IOException;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.text.translation.I18n;

public class GuiBattleRulesNPCEditor
extends GuiBattleRulesBase {
    private static final int BUTTON_ID_OK = 100;

    public GuiBattleRulesNPCEditor() {
        Optional<NPCTrainer> entityNPCOptional = EntityNPC.locateNPCClient(Minecraft.getMinecraft().world, GuiTrainerEditor.currentTrainerID, NPCTrainer.class);
        if (!entityNPCOptional.isPresent()) {
            GuiHelper.closeScreen();
            return;
        }
        this.rules = entityNPCOptional.get().battleRules;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.add(new GuiButton(100, this.centerX + 155, this.centerY + 90, 30, 20, I18n.translateToLocal("gui.guiItemDrops.ok")));
    }

    @Override
    protected void keyTyped(char key, int keyCode) {
        super.keyTyped(key, keyCode);
        if (keyCode == 1 || keyCode == 28) {
            this.closeScreen();
        }
    }

    @Override
    protected void drawBackgroundUnderMenus(float partialTicks, int mouseX, int mouseY) {
        super.drawBackgroundUnderMenus(partialTicks, mouseX, mouseY);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if (button.id == 100) {
            this.closeScreen();
        }
    }

    private void closeScreen() {
        this.registerRules();
        Pixelmon.NETWORK.sendToServer(new NPCServerPacket(GuiTrainerEditor.currentTrainerID, this.rules));
        this.mc.displayGuiScreen(new GuiTrainerEditor(GuiTrainerEditor.currentTrainerID));
    }
}

