/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.npcEditor;

import com.pixelmongenerations.client.gui.elements.GuiSlotBase;
import com.pixelmongenerations.client.gui.npcEditor.GuiTrainerEditorMore;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.item.ItemStack;

public class GuiTrainerDropListSlot
extends GuiSlotBase {
    GuiTrainerEditorMore gui;

    public GuiTrainerDropListSlot(GuiTrainerEditorMore gui) {
        super(gui.listTop, gui.listLeft, 120, gui.listHeight, false);
        this.gui = gui;
    }

    @Override
    protected int getSize() {
        return this.gui.getDropListCount();
    }

    @Override
    protected void elementClicked(int var1, boolean var2) {
        this.gui.removeFromList(var1);
    }

    @Override
    protected boolean isSelected(int var1) {
        return false;
    }

    @Override
    protected float[] get1Color() {
        return new float[]{0.7f, 0.7f, 0.7f};
    }

    @Override
    protected int[] get255Color() {
        return new int[]{120, 120, 120};
    }

    @Override
    protected void drawSlot(int var1, int var2, int var3, int var4, Tessellator var5) {
        ItemStack itemStack = this.gui.getDropListEntry(var1);
        if (itemStack != null) {
            Minecraft.getMinecraft().fontRenderer.drawString(itemStack.getDisplayName(), var2 + 2, var3 - 1, 0, false);
        }
    }
}

