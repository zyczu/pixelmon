/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.npcEditor;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.network.packetHandlers.npc.NPCServerPacket;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.translation.I18n;

public class GuiChooseNPC
extends GuiScreen {
    BlockPos pos;

    public GuiChooseNPC(BlockPos pos) {
        this.pos = pos;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.add(new GuiButton(EnumNPCType.Trainer.ordinal(), this.width / 2 - 60, this.height / 2 - 80, I18n.translateToLocal("gui.chooseNPC.trainer")));
        this.buttonList.add(new GuiButton(EnumNPCType.Trader.ordinal(), this.width / 2 - 60, this.height / 2 - 60, I18n.translateToLocal("gui.chooseNPC.trader")));
        this.buttonList.add(new GuiButton(EnumNPCType.ChattingNPC.ordinal(), this.width / 2 - 60, this.height / 2 - 40, I18n.translateToLocal("gui.chooseNPC.chatting")));
        this.buttonList.add(new GuiButton(EnumNPCType.Relearner.ordinal(), this.width / 2 - 60, this.height / 2 - 20, I18n.translateToLocal("gui.chooseNPC.relearner")));
        this.buttonList.add(new GuiButton(EnumNPCType.Tutor.ordinal(), this.width / 2 - 60, this.height / 2, I18n.translateToLocal("gui.chooseNPC.tutor")));
        this.buttonList.add(new GuiButton(EnumNPCType.NurseJoy.ordinal(), this.width / 2 - 60, this.height / 2 + 20, I18n.translateToLocal("gui.chooseNPC.nursejoy")));
        this.buttonList.add(new GuiButton(EnumNPCType.Shopkeeper.ordinal(), this.width / 2 - 60, this.height / 2 + 40, I18n.translateToLocal("gui.chooseNPC.shopkeeper")));
        this.buttonList.add(new GuiButton(EnumNPCType.Groomer.ordinal(), this.width / 2 - 60, this.height / 2 + 60, I18n.translateToLocal("gui.chooseNPC.groomer")));
        this.buttonList.add(new GuiButton(EnumNPCType.Ultra.ordinal(), this.width / 2 - 60, this.height / 2 + 80, I18n.translateToLocal("gui.chooseNPC.ultra")));
        this.buttonList.add(new GuiButton(EnumNPCType.Damos.ordinal(), this.width / 2 - 60, this.height / 2 + 100, I18n.translateToLocal("gui.chooseNPC.damos")));
        this.buttonList.add(new GuiButton(EnumNPCType.Sticker.ordinal(), this.width / 2 - 60, this.height / 2 + 120, I18n.translateToLocal("gui.chooseNPC.sticker")));
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        Pixelmon.NETWORK.sendToServer(new NPCServerPacket(EnumNPCType.getFromOrdinal((short)button.id), this.pos, Minecraft.getMinecraft().player.rotationYawHead));
        this.mc.player.closeScreen();
    }
}

