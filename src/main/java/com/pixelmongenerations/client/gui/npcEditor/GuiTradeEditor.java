/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 */
package com.pixelmongenerations.client.gui.npcEditor;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.elements.GuiContainerDropDown;
import com.pixelmongenerations.client.gui.npcEditor.TextureEditorNPC;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTrader;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.packetHandlers.npc.DeleteNPC;
import com.pixelmongenerations.core.network.packetHandlers.npc.NPCServerPacket;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.input.Keyboard;

public class GuiTradeEditor
extends GuiContainerDropDown {
    public int top;
    public int left;
    GuiTextField offerName;
    GuiTextField exchangeName;
    GuiTextField lvl;
    NPCTrader trader;
    String offer;
    String exchange;
    public int level;
    private TextureEditorNPC textureEditor;
    private GuiTextField formText;
    private int form = -1;

    public GuiTradeEditor(int traderId) {
        Keyboard.enableRepeatEvents((boolean)true);
        this.xSize = 256;
        this.ySize = 226;
        Optional<NPCTrader> entityNPCOptional = EntityNPC.locateNPCClient(Minecraft.getMinecraft().world, traderId, NPCTrader.class);
        if (!entityNPCOptional.isPresent()) {
            GuiHelper.closeScreen();
        } else {
            this.trader = entityNPCOptional.get();
            if (this.trader != null) {
                this.offer = this.trader.getOffer();
                this.exchange = this.trader.getExchange();
            }
        }
    }

    @Override
    public void initGui() {
        super.initGui();
        if (this.trader == null) {
            GuiHelper.closeScreen();
        } else {
            this.trader.updateTradePair();
            this.left = (this.width - this.xSize) / 2;
            this.top = (this.height - this.ySize) / 2;
            this.offerName = new GuiTextField(6, this.mc.fontRenderer, this.width / 2 - 45, this.height / 2 + 10, 90, 17);
            this.offerName.setText(Entity1Base.getLocalizedName(this.offer));
            this.exchangeName = new GuiTextField(7, this.mc.fontRenderer, this.width / 2 + 100, this.height / 2 + 10, 90, 17);
            this.exchangeName.setText(Entity1Base.getLocalizedName(this.exchange));
            this.lvl = new GuiTextField(8, this.mc.fontRenderer, this.width / 2 + 15, this.height / 2 + 28, 30, 17);
            this.lvl.setText(String.valueOf(this.trader.getLevel()));
            this.buttonList.add(new GuiButton(1, this.width / 2 + 155, this.height / 2 + 90, 30, 20, I18n.translateToLocal("gui.guiItemDrops.ok")));
            this.buttonList.add(new GuiButton(2, this.width / 2 + 100, this.height / 2 - 110, 80, 20, I18n.translateToLocal("gui.tradereditor.delete")));
            this.buttonList.add(new GuiButton(3, this.width / 2 - 46, this.height / 2 + 28, 20, 20, I18n.translateToLocal("gui.tradereditor.shiny")));
            this.buttonList.add(new GuiButton(4, this.width / 2 - 40, this.height / 2 - 110, 80, 20, I18n.translateToLocal("gui.tradereditor.random")));
            this.buttonList.add(new GuiButton(5, this.width / 2 + 58, this.height / 2 + 9, 30, 20, "<->"));
            this.textureEditor = new TextureEditorNPC(this, this.trader, this.width / 2 - 190, this.height / 2 + 50, 130, -23);
            this.formText = new GuiTextField(9, this.mc.fontRenderer, this.width / 2 + 25, this.height / 2 + 46, 20, 17);
            this.formText.setText(Integer.toString(this.trader.getForm()));
            this.checkOfferForm();
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
        GuiHelper.drawImageQuad(this.width / 2 - 200, this.height / 2 - 120, 400.0, 240.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        GuiHelper.drawEntity(this.trader, this.width / 2 - 125, this.height / 2 + 40, 50.0f, 0.0f, 0.0f);
        if (this.trader.getIsShiny()) {
            this.offerName.setTextColor(0xFFFF33);
        } else {
            this.offerName.setTextColor(0xFFFFFF);
        }
        this.offerName.drawTextBox();
        this.exchangeName.drawTextBox();
        this.lvl.drawTextBox();
        String s = I18n.translateToLocal("gui.tradereditor.offer");
        String s2 = I18n.translateToLocal("gui.tradereditor.exchange");
        String s3 = I18n.translateToLocal("gui.screenpokechecker.lvl");
        this.mc.fontRenderer.drawString(s, this.left + 85, this.top + 110, 0);
        this.mc.fontRenderer.drawString(s2, this.left + 225, this.top + 110, 0);
        this.mc.fontRenderer.drawString(s3, this.left + 120, this.top + 145, 0);
        this.textureEditor.drawCustomTextBox();
        if (this.formText.getVisible()) {
            this.formText.drawTextBox();
            this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.form"), this.left + 120, this.top + 165, 0);
        }
    }

    @Override
    protected void drawBackgroundUnderMenus(float var1, int var2, int var3) {
    }

    @Override
    protected void keyTyped(char key, int keyCode) {
        ArrayList<GuiTextField> textFields = new ArrayList<GuiTextField>(4);
        this.offerName.textboxKeyTyped(key, keyCode);
        textFields.add(this.offerName);
        this.exchangeName.textboxKeyTyped(key, keyCode);
        textFields.add(this.exchangeName);
        this.lvl.textboxKeyTyped(key, keyCode);
        textFields.add(this.lvl);
        if (this.formText.getVisible()) {
            this.formText.textboxKeyTyped(key, keyCode);
            textFields.add(this.formText);
        }
        this.textureEditor.keyTyped(key, keyCode, textFields.toArray(new GuiTextField[textFields.size()]));
        if (keyCode == 1 || keyCode == 28) {
            this.saveFields();
        }
        if (this.offerName.isFocused()) {
            this.checkOfferForm();
        }
    }

    @Override
    protected void mouseClickedUnderMenus(int x, int y, int mouseButton) throws IOException {
        this.offerName.mouseClicked(x, y, mouseButton);
        this.exchangeName.mouseClicked(x, y, mouseButton);
        this.lvl.mouseClicked(x, y, mouseButton);
        this.textureEditor.mouseClicked(x, y, mouseButton);
        this.formText.mouseClicked(x, y, mouseButton);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if (button.enabled) {
            if (button.id == 1) {
                this.saveFields();
            } else if (button.id == 2) {
                GuiHelper.closeScreen();
                Minecraft.getMinecraft().setRenderViewEntity(Minecraft.getMinecraft().player);
                Pixelmon.NETWORK.sendToServer(new DeleteNPC(this.trader.getId()));
            } else if (button.id == 3) {
                this.trader.setIsShiny(!this.trader.getIsShiny());
            } else if (button.id == 4) {
                EnumSpecies[] list = EnumSpecies.values();
                String poke2 = list[RandomHelper.getRandomNumberBetween((int)0, (int)(list.length - 1))].name;
                String poke1 = list[RandomHelper.getRandomNumberBetween((int)0, (int)(list.length - 1))].name;
                if (poke2.equalsIgnoreCase(poke1)) {
                    poke2 = list[RandomHelper.getRandomNumberBetween((int)0, (int)(list.length - 1))].name;
                }
                this.offerName.setText(Entity1Base.getLocalizedName(poke1));
                this.exchangeName.setText(Entity1Base.getLocalizedName(poke2));
                this.lvl.setText(String.valueOf(RandomHelper.getRandomNumberBetween(1, PixelmonServerConfig.maxLevel)));
                this.checkOfferForm();
            } else if (button.id == 5) {
                String text = this.offerName.getText();
                this.offerName.setText(this.exchangeName.getText());
                this.exchangeName.setText(text);
                this.checkOfferForm();
            }
        }
    }

    private void saveFields() {
        try {
            this.level = Integer.parseInt(this.lvl.getText());
        }
        catch (NumberFormatException var3) {
            this.level = 10;
            return;
        }
        if (this.level >= 1 && this.level <= PixelmonServerConfig.maxLevel) {
            try {
                this.form = Short.parseShort(this.formText.getText());
            }
            catch (NumberFormatException var2) {
                this.form = 0;
            }
            if (EnumSpecies.hasPokemon(this.exchangeName.getText()) && EnumSpecies.hasPokemon(this.offerName.getText())) {
                this.setNewTradePokemon();
                GuiHelper.closeScreen();
                Minecraft.getMinecraft().setRenderViewEntity(Minecraft.getMinecraft().player);
            }
            this.textureEditor.saveCustomTexture();
        }
    }

    private void setNewTradePokemon() {
        if (EnumSpecies.hasPokemonAnyCase(this.exchangeName.getText()) && EnumSpecies.hasPokemonAnyCase(this.offerName.getText())) {
            EnumSpecies exchangeEnum = EnumSpecies.getFromNameAnyCase(this.exchangeName.getText());
            EnumSpecies offerEnum = EnumSpecies.getFromNameAnyCase(this.offerName.getText());
            if (exchangeEnum != null && offerEnum != null) {
                String exchangeText = exchangeEnum.name;
                String offerText = offerEnum.name;
                boolean shiny = this.trader.getIsShiny();
                this.trader.updateTrade(exchangeText, offerText, this.level, shiny, this.form);
                this.trader.updateTradePair();
                Pixelmon.NETWORK.sendToServer(new NPCServerPacket(this.trader.getId(), exchangeText, offerText, this.level, shiny, this.form));
            }
        }
    }

    private void checkOfferForm() {
        if (Entity3HasStats.hasForms(this.offerName.getText())) {
            this.formText.setVisible(true);
            if (this.formText.getText().equals("-1")) {
                this.formText.setText("0");
            }
        } else {
            this.formText.setVisible(false);
            this.formText.setText("-1");
        }
    }
}

