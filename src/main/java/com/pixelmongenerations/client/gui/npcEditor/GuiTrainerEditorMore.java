/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.npcEditor;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.npcEditor.GuiTrainerDropListSlot;
import com.pixelmongenerations.client.gui.npcEditor.GuiTrainerEditor;
import com.pixelmongenerations.common.battle.rules.BattleRules;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.network.SetTrainerData;
import com.pixelmongenerations.core.network.packetHandlers.npc.StoreTrainerData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;

public class GuiTrainerEditorMore
extends GuiContainer {
    GuiTextField tfGreeting;
    GuiTextField tfWin;
    GuiTextField tfLose;
    GuiTextField tfDrop;
    GuiTextField tfWinMoney;
    protected int listTop;
    protected int listLeft;
    protected int listHeight;
    protected int listWidth;
    ArrayList<ItemStack> dropList = new ArrayList();
    GuiTrainerDropListSlot list;
    int lastWidth;
    int lastHeight;

    public GuiTrainerEditorMore() {
        super(new ContainerEmpty());
        this.dropList.clear();
        if (GuiTrainerEditor.trainerData == null) {
            GuiTrainerEditor.trainerData = new SetTrainerData("", "", "", "", 0, new ItemStack[0], new BattleRules());
        }
        Collections.addAll(this.dropList, GuiTrainerEditor.trainerData.winnings);
    }

    private void initList() {
        this.lastWidth = this.width;
        this.lastHeight = this.height;
        this.listTop = this.height / 2 + 30;
        this.listLeft = this.width / 2 - 160;
        this.listHeight = 80;
        this.listWidth = 100;
        this.list = new GuiTrainerDropListSlot(this);
    }

    @Override
    public void initGui() {
        super.initGui();
        this.initList();
        this.buttonList.add(new GuiButton(1, this.width / 2 + 155, this.height / 2 + 90, 30, 20, I18n.translateToLocal("gui.guiItemDrops.ok")));
        this.buttonList.add(new GuiButton(2, this.width / 2 - 30, this.height / 2 + 70, 80, 20, I18n.translateToLocal("gui.trainereditor.additem")));
        this.tfGreeting = new GuiTextField(3, this.mc.fontRenderer, this.width / 2 - 120, this.height / 2 - 70, 280, 17);
        this.tfGreeting.setMaxStringLength(2000);
        this.tfGreeting.setText(GuiTrainerEditor.trainerData.greeting);
        this.tfWin = new GuiTextField(4, this.mc.fontRenderer, this.width / 2 - 120, this.height / 2 - 50, 280, 17);
        this.tfWin.setMaxStringLength(2000);
        this.tfWin.setText(GuiTrainerEditor.trainerData.win);
        this.tfLose = new GuiTextField(5, this.mc.fontRenderer, this.width / 2 - 120, this.height / 2 - 30, 280, 17);
        this.tfLose.setMaxStringLength(2000);
        this.tfLose.setText(GuiTrainerEditor.trainerData.lose);
        this.tfDrop = new GuiTextField(7, this.mc.fontRenderer, this.width / 2 - 30, this.height / 2 + 50, 80, 17);
        this.tfWinMoney = new GuiTextField(9, this.fontRenderer, this.width / 2 + 110, this.height / 2 + 50, 80, 20);
        this.tfWinMoney.setMaxStringLength(2000);
        this.tfWinMoney.setText(String.valueOf(GuiTrainerEditor.trainerData.winMoney));
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float mfloat, int mouseX, int mouseY) {
        this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
        GuiHelper.drawImageQuad(this.width / 2 - 200, this.height / 2 - 120, 400.0, 240.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.moreinfo"), this.width / 2 - this.mc.fontRenderer.getStringWidth(I18n.translateToLocal("gui.trainereditor.moreinfo")) / 2, this.height / 2 - 90, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.greeting"), this.width / 2 - 180, this.height / 2 - 65, 0);
        this.tfGreeting.drawTextBox();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.win"), this.width / 2 - 180, this.height / 2 - 45, 0);
        this.tfWin.drawTextBox();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.lose"), this.width / 2 - 180, this.height / 2 - 25, 0);
        this.tfLose.drawTextBox();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.winningdrops"), this.width / 2 - 157, this.height / 2 + 15, 0);
        this.list.drawScreen(mouseX, mouseY, mfloat);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.adddrops"), this.width / 2 - 30, this.height / 2 + 15, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.enteritemname"), this.width / 2 - 30, this.height / 2 + 30, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.winmoney"), this.width / 2 + 110, this.height / 2 + 30, 0);
        this.tfDrop.drawTextBox();
        this.tfWinMoney.drawTextBox();
    }

    @Override
    protected void keyTyped(char key, int par2) {
        this.tfGreeting.textboxKeyTyped(key, par2);
        this.tfWin.textboxKeyTyped(key, par2);
        this.tfLose.textboxKeyTyped(key, par2);
        this.tfDrop.textboxKeyTyped(key, par2);
        this.tfWinMoney.textboxKeyTyped(key, par2);
        GuiHelper.switchFocus(par2, this.tfGreeting, this.tfWin, this.tfLose, this.tfDrop, this.tfWinMoney);
        if (par2 == 1 || par2 == 28) {
            this.saveFields();
        }
    }

    @Override
    protected void mouseClicked(int x, int y, int z) throws IOException {
        super.mouseClicked(x, y, z);
        this.tfGreeting.mouseClicked(x, y, z);
        this.tfWin.mouseClicked(x, y, z);
        this.tfLose.mouseClicked(x, y, z);
        this.tfDrop.mouseClicked(x, y, z);
        this.tfWinMoney.mouseClicked(x, y, z);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if (button.id == 1) {
            this.saveFields();
        } else if (button.id == 2) {
            String itemString = this.tfDrop.getText();
            try {
                int id = Integer.parseInt(itemString);
                if (Item.getItemById(id) != null || this.isInItems(id)) {
                    this.dropList.add(new ItemStack(Item.getItemById(id)));
                    return;
                }
            }
            catch (Exception exception) {
                // empty catch block
            }
            if (PixelmonItems.getItemFromName(itemString) != null) {
                this.dropList.add(new ItemStack(PixelmonItems.getItemFromName(itemString)));
            } else if (Item.REGISTRY.getObject(new ResourceLocation(itemString)) != null) {
                this.dropList.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(itemString))));
            }
        }
    }

    private void saveFields() {
        ItemStack[] drops = new ItemStack[this.dropList.size()];
        for (int i = 0; i < this.dropList.size(); ++i) {
            drops[i] = this.dropList.get(i);
        }
        int winMoneyValue = GuiTrainerEditor.trainerData.winMoney;
        try {
            winMoneyValue = Math.max(0, Integer.parseInt(this.tfWinMoney.getText()));
        }
        catch (NumberFormatException numberFormatException) {
            // empty catch block
        }
        SetTrainerData p = new SetTrainerData(GuiTrainerEditor.trainerData.name, this.tfGreeting.getText(), this.tfWin.getText(), this.tfLose.getText(), winMoneyValue, drops);
        p.id = GuiTrainerEditor.currentTrainerID;
        if (!(this.tfGreeting.getText().equals(GuiTrainerEditor.trainerData.greeting) && this.tfWin.getText().equals(GuiTrainerEditor.trainerData.win) && this.tfLose.getText().equals(GuiTrainerEditor.trainerData.lose) && winMoneyValue == GuiTrainerEditor.trainerData.winMoney)) {
            Pixelmon.NETWORK.sendToServer(new StoreTrainerData(GuiTrainerEditor.currentTrainerID, p));
        }
        Pixelmon.NETWORK.sendToServer(new StoreTrainerData(GuiTrainerEditor.currentTrainerID, drops));
        GuiTrainerEditor.trainerData = p;
        this.mc.displayGuiScreen(new GuiTrainerEditor(GuiTrainerEditor.currentTrainerID));
    }

    private boolean isInItems(int id) {
        return Item.getItemById(id) != null;
    }

    public int getDropListCount() {
        return this.dropList.size();
    }

    public void removeFromList(int ind) {
        this.dropList.remove(ind);
    }

    public ItemStack getDropListEntry(int ind) {
        if (ind < this.dropList.size() && ind >= 0) {
            return this.dropList.get(ind);
        }
        return null;
    }
}

