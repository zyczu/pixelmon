/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.npcEditor;

import com.pixelmongenerations.client.gui.npcEditor.GuiTrainerEditor;
import com.pixelmongenerations.client.gui.npcEditor.GuiTrainerEditorPokemonScreen;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiPartyEditorBase;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.npc.AddTrainerPokemon;
import com.pixelmongenerations.core.network.packetHandlers.npc.RandomiseTrainerPokemon;
import com.pixelmongenerations.core.network.packetHandlers.npc.UpdateTrainerParty;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class GuiTrainerEditorPartyScreen
extends GuiPartyEditorBase {
    public GuiTrainerEditorPartyScreen() {
        super(GuiTrainerEditor.pokemonList);
    }

    @Override
    public String getTitle() {
        return I18n.translateToLocal("gui.trainereditor.pokemoneditor");
    }

    @Override
    protected void exitScreen() {
        this.mc.displayGuiScreen(new GuiTrainerEditor(GuiTrainerEditor.currentTrainerID));
    }

    @Override
    protected void randomizeParty() {
        Pixelmon.NETWORK.sendToServer(new RandomiseTrainerPokemon(GuiTrainerEditor.currentTrainerID));
    }

    @Override
    protected void addPokemon(int partySlot) {
        Pixelmon.NETWORK.sendToServer(new AddTrainerPokemon(GuiTrainerEditor.currentTrainerID));
    }

    @Override
    protected void editPokemon(int partySlot) {
        this.mc.displayGuiScreen(new GuiTrainerEditorPokemonScreen(partySlot, this.getTitle()));
    }

    @Override
    protected IMessage getImportSavePacket() {
        return new UpdateTrainerParty(this.pokemonList);
    }
}

