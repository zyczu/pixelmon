/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 */
package com.pixelmongenerations.client.gui.npcEditor;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.chooseMoveset.GuiMoveList;
import com.pixelmongenerations.client.gui.chooseMoveset.IMoveClicked;
import com.pixelmongenerations.client.gui.elements.GuiContainerDropDown;
import com.pixelmongenerations.client.gui.npc.GuiTutor;
import com.pixelmongenerations.client.gui.npcEditor.TextureEditorNPC;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiImportExport;
import com.pixelmongenerations.client.gui.pokemoneditor.IImportableContainer;
import com.pixelmongenerations.client.gui.pokemoneditor.ImportExportConverter;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTutor;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.network.packetHandlers.npc.DeleteNPC;
import com.pixelmongenerations.core.network.packetHandlers.npc.StoreTutorData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import org.lwjgl.input.Keyboard;

public class GuiTutorEditor extends GuiContainerDropDown implements IMoveClicked, IImportableContainer {

    private NPCTutor tutor;
    private ArrayList<ItemStack> cost;
    private int listTop;
    private int listLeft;
    private int listHeight;
    private int listWidth;
    private GuiMoveList attackListGui;
    private int selectedMove = -1;
    private GuiTextField newMove;
    private GuiTextField newItem;
    private GuiTextField newItemAmount;
    private GuiTextField newItemDamage;
    private GuiButton addMove;
    private GuiButton deleteMove;
    private GuiButton addItem;
    private GuiButton[] deleteCost = new GuiButton[4];
    private GuiButton deleteTutor;
    private TextureEditorNPC textureEditor;
    private GuiButton importExport;
    private int middle_y;
    private int middle_x;

    public GuiTutorEditor(int tutorID) {
        Keyboard.enableRepeatEvents((boolean)true);
        Optional<NPCTutor> entityNPCOptional = EntityNPC.locateNPCClient(Minecraft.getMinecraft().world, tutorID, NPCTutor.class);
        if (!entityNPCOptional.isPresent()) {
            GuiHelper.closeScreen();
            return;
        }
        this.tutor = entityNPCOptional.get();
        this.listHeight = 150;
        this.listWidth = 90;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.middle_y = this.height / 2 - 20;
        this.middle_x = this.width / 2 - 20;
        this.listTop = this.middle_y - 62;
        this.listLeft = this.middle_x - 63;
        this.attackListGui = new GuiMoveList(this, GuiTutor.attackList, this.listWidth, this.listHeight, this.listTop, this.listLeft, this.mc);
        this.buttonList.add(new GuiButton(1, this.middle_x - 25, this.middle_y + 98, 50, 20, I18n.translateToLocal("gui.guiItemDrops.ok")));
        this.newMove = new GuiTextField(1, this.mc.fontRenderer, this.middle_x + 74, this.middle_y - 100, 90, 20);
        this.newItem = new GuiTextField(2, this.mc.fontRenderer, this.middle_x + 84, this.middle_y + 30, 70, 20);
        this.newItemAmount = new GuiTextField(3, this.mc.fontRenderer, this.middle_x + 84, this.middle_y + 50, 70, 20);
        this.newItemDamage = new GuiTextField(4, this.mc.fontRenderer, this.middle_x + 84, this.middle_y + 70, 70, 20);
        this.addMove = new GuiButton(2, this.middle_x + 89, this.middle_y - 76, 60, 20, I18n.translateToLocal("gui.tutor.addmove"));
        this.deleteMove = new GuiButton(3, this.middle_x + 84, this.middle_y - 45, 70, 20, I18n.translateToLocal("gui.tutor.deletemove"));
        this.addItem = new GuiButton(4, this.middle_x + 89, this.middle_y + 98, 60, 20, I18n.translateToLocal("gui.tutor.addcost"));
        if (this.selectedMove > -1) {
            this.buttonList.add(this.addItem);
            this.buttonList.add(this.deleteMove);
        }
        for (int i = 0; i < this.deleteCost.length; ++i) {
            this.deleteCost[i] = new GuiButton(5 + i, this.middle_x + 94 + 20 * i, this.middle_y + 8, 20, 20, "X");
        }
        this.deleteTutor = new GuiButton(9, this.middle_x - 180, this.middle_y + 98, 80, 20, I18n.translateToLocal("gui.tutor.deletetutor"));
        this.importExport = new GuiButton(10, this.addItem.x + this.addItem.width / 2 - 50, this.addItem.y + this.addItem.height + 5, 100, 20, I18n.translateToLocal("gui.pokemoneditor.importexport"));
        this.textureEditor = new TextureEditorNPC(this, this.tutor, this.middle_x - 210, this.middle_y + 63, 130, -28);
        this.buttonList.add(this.addMove);
        this.buttonList.add(this.importExport);
        this.buttonList.add(this.deleteTutor);
    }

    @Override
    protected void drawBackgroundUnderMenus(float mFloat, int mouseX, int mouseY) {
        this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
        GuiHelper.drawImageQuad(0.0, 0.0, this.width, this.height, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        String text = I18n.translateToLocal("gui.choosemoveset.choosemove");
        this.mc.fontRenderer.drawString(text, this.middle_x - 20 - this.fontRenderer.getStringWidth(text) / 2, this.middle_y - 74, 0xFFFFFF);
        if (GuiTutor.attackList != null) {
            this.attackListGui.drawScreen(mouseX, mouseY, mFloat);
        }
        GuiHelper.drawEntity(this.tutor, this.middle_x - 133, this.middle_y + 50, 50.0f, 0.0f, 0.0f);
        this.newMove.drawTextBox();
        if (this.selectedMove >= 0) {
            text = GuiTutor.attackList.get(this.selectedMove).getAttackBase().getLocalizedName();
            this.mc.fontRenderer.drawString(text, this.middle_x + 119 - this.fontRenderer.getStringWidth(text) / 2, this.middle_y - 20, 0);
            text = I18n.translateToLocal("gui.choosemoveset.cost");
            this.mc.fontRenderer.drawString(text, this.middle_x + 94 - this.fontRenderer.getStringWidth(text), this.middle_y - 5, 0);
            if (this.cost.size() < 4) {
                this.newItem.drawTextBox();
                this.newItemAmount.drawTextBox();
                this.newItemDamage.drawTextBox();
                text = I18n.translateToLocal("gui.tutor.itemname");
                this.mc.fontRenderer.drawString(text, this.middle_x + 79 - this.fontRenderer.getStringWidth(text), this.middle_y + 35, 0);
                text = I18n.translateToLocal("gui.tutor.itemamount");
                this.mc.fontRenderer.drawString(text, this.middle_x + 79 - this.fontRenderer.getStringWidth(text), this.middle_y + 55, 0);
                text = I18n.translateToLocal("gui.tutor.itemdamage");
                this.mc.fontRenderer.drawString(text, this.middle_x + 79 - this.fontRenderer.getStringWidth(text), this.middle_y + 75, 0);
            }
        }
        this.textureEditor.drawCustomTextBox();
        if (this.cost != null && !this.cost.isEmpty()) {
            int i = 0;
            for (ItemStack item : this.cost) {
                this.itemRender.renderItemAndEffectIntoGUI(item, this.middle_x + 96 + i * 20, this.middle_y - 10);
                this.itemRender.renderItemOverlayIntoGUI(this.mc.fontRenderer, item, this.middle_x + 96 + i * 20, this.middle_y - 10, null);
                if (i < this.deleteCost.length && !this.buttonList.contains(this.deleteCost[i])) {
                    this.buttonList.add(this.deleteCost[i]);
                }
                ++i;
            }
        }
    }

    @Override
    protected void mouseClickedUnderMenus(int x, int y, int mouseButton) throws IOException {
        this.newMove.mouseClicked(x, y, mouseButton);
        this.newItem.mouseClicked(x, y, mouseButton);
        this.newItemAmount.mouseClicked(x, y, mouseButton);
        this.newItemDamage.mouseClicked(x, y, mouseButton);
        this.textureEditor.mouseClicked(x, y, mouseButton);
    }

    @Override
    public void elementClicked(ArrayList<Attack> list, int index) {
        if (GuiTutor.attackList != null && GuiTutor.costs != null && index < GuiTutor.attackList.size()) {
            this.selectedMove = index;
            this.cost = GuiTutor.costs.get(index);
            if (!this.buttonList.contains(this.deleteMove)) {
                this.buttonList.add(this.deleteMove);
            }
            if (!this.buttonList.contains(this.addItem) && this.cost.size() < 4) {
                this.buttonList.add(this.addItem);
            }
            for (GuiButton deleteCostButton : this.deleteCost) {
                this.buttonList.remove(deleteCostButton);
            }
        }
    }

    @Override
    protected void keyTyped(char key, int keyCode) {
        this.newMove.textboxKeyTyped(key, keyCode);
        ArrayList<GuiTextField> textFields = new ArrayList<GuiTextField>();
        textFields.add(this.newMove);
        if (this.selectedMove >= 0 && this.cost.size() < 4) {
            this.newItem.textboxKeyTyped(key, keyCode);
            this.newItemAmount.textboxKeyTyped(key, keyCode);
            this.newItemDamage.textboxKeyTyped(key, keyCode);
            textFields.addAll(Arrays.asList(this.newItem, this.newItemAmount, this.newItemDamage));
        }
        this.textureEditor.keyTyped(key, keyCode, textFields.toArray(new GuiTextField[textFields.size()]));
        if (keyCode == 1 || keyCode == 28) {
            this.saveFields();
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.id == 1) {
            this.saveFields();
        } else if (button.id == 2) {
            Attack newAttack = DatabaseMoves.getAttack(this.newMove.getText());
            if (newAttack != null && !GuiTutor.attackList.contains(newAttack)) {
                GuiTutor.attackList.add(newAttack);
                GuiTutor.costs.add(new ArrayList());
            }
        } else if (button.id == 3) {
            if (this.selectedMove >= 0 && this.selectedMove < GuiTutor.attackList.size()) {
                GuiTutor.attackList.remove(this.selectedMove);
                GuiTutor.costs.remove(this.selectedMove);
                this.cost = null;
                this.selectedMove = -1;
                this.buttonList.remove(this.addItem);
                this.buttonList.remove(this.deleteMove);
                for (GuiButton deleteCostButton : this.deleteCost) {
                    this.buttonList.remove(deleteCostButton);
                }
            }
        } else if (button.id == 4) {
            if (this.cost.size() < 4) {
                Item item = Item.getByNameOrId(this.newItem.getText());
                if (item == null) {
                    item = PixelmonItems.getItemFromName(this.newItem.getText());
                }
                if (item != null) {
                    int amount = 1;
                    int damage = 0;
                    try {
                        amount = Math.max(amount, Integer.parseInt(this.newItemAmount.getText()));
                    }
                    catch (NumberFormatException deleteCostButton) {
                        // empty catch block
                    }
                    try {
                        damage = Math.max(damage, Integer.parseInt(this.newItemDamage.getText()));
                    }
                    catch (NumberFormatException deleteCostButton) {
                        // empty catch block
                    }
                    ItemStack newItem = new ItemStack(item, amount);
                    if (amount > newItem.getMaxStackSize()) {
                        newItem.setCount(newItem.getMaxStackSize());
                    }
                    newItem.setItemDamage(damage);
                    this.cost.add(newItem);
                    if (this.cost.size() >= 4) {
                        this.buttonList.remove(this.addItem);
                    }
                }
            }
        } else if (button.id >= 5 && button.id <= 8) {
            int deleteButtonIndex = button.id - 5;
            if (this.cost.size() > deleteButtonIndex) {
                this.cost.remove(deleteButtonIndex);
                for (GuiButton deleteCostButton : this.deleteCost) {
                    this.buttonList.remove(deleteCostButton);
                }
                if (!this.buttonList.contains(this.addItem)) {
                    this.buttonList.add(this.addItem);
                }
            }
        } else if (button.id == 9) {
            Pixelmon.NETWORK.sendToServer(new DeleteNPC(this.tutor.getId()));
            GuiHelper.closeScreen();
        } else if (button.id == 10) {
            this.mc.displayGuiScreen(new GuiImportExport(this, "Tutor Editor"));
        }
    }

    private void saveFields() {
        if (!GuiTutor.attackList.isEmpty()) {
            if (this.tutor != null) {
                Pixelmon.NETWORK.sendToServer(new StoreTutorData(this.tutor.getId()));
                this.textureEditor.saveCustomTexture();
            }
            GuiHelper.closeScreen();
        }
    }

    @Override
    public String getExportText() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < GuiTutor.attackList.size(); ++i) {
            ImportExportConverter.addLine(builder, GuiTutor.attackList.get(i).toString());
            List<ItemStack> itemStacks = GuiTutor.costs.get(i);
            for (ItemStack stack : itemStacks) {
                builder.append("- ").append(stack.getItem().getRegistryName().toString()).append(" ").append(stack.getCount()).append(" ").append(stack.getMetadata()).append("\n");
            }
        }
        return builder.toString();
    }

    @Override
    public String importText(String importText) {
        ArrayList<Attack> attackList = new ArrayList<Attack>();
        ArrayList costs = new ArrayList();
        ArrayList<ItemStack> costList = null;
        for (String line : importText.split("\n")) {
            if (line == null || line.isEmpty()) continue;
            if (line.startsWith("- ")) {
                ItemStack stack;
                String[] temp;
                if (costList == null) {
                    return "Cost entry not succeeding a move entry";
                }
                if (costList.size() == 4 || (temp = line.substring(2).split(" ")).length != 3 || (stack = this.createItemStack(temp[0], Integer.parseInt(temp[1]), Integer.parseInt(temp[2]))) == null) continue;
                costList.add(stack);
                continue;
            }
            Attack attack = Attack.getAttackBase(line).map(Attack::new).orElse(null);
            if (attack == null) continue;
            attackList.add(attack);
            costList = new ArrayList<ItemStack>();
            costs.add(costList);
        }
        GuiTutor.attackList = attackList;
        GuiTutor.costs = costs;
        return null;
    }

    @Override
    public GuiScreen getScreen() {
        return this;
    }

    private ItemStack createItemStack(String itemName, int stackSize, int meta) {
        if (itemName == null) {
            return null;
        }
        Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(itemName));
        if (item == null) {
            return null;
        }
        return new ItemStack(item, stackSize, meta);
    }
}

