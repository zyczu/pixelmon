/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 */
package com.pixelmongenerations.client.gui.npcEditor;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.elements.GuiContainerDropDown;
import com.pixelmongenerations.client.gui.elements.GuiDropDown;
import com.pixelmongenerations.client.gui.npcEditor.GuiBattleRulesNPCEditor;
import com.pixelmongenerations.client.gui.npcEditor.GuiTrainerEditorMore;
import com.pixelmongenerations.client.gui.npcEditor.GuiTrainerEditorPartyScreen;
import com.pixelmongenerations.client.gui.npcEditor.TextureEditorNPC;
import com.pixelmongenerations.common.battle.rules.BattleRules;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.BaseTrainer;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumEncounterMode;
import com.pixelmongenerations.core.enums.EnumTrainerAI;
import com.pixelmongenerations.core.enums.battle.EnumBattleAIMode;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.SetTrainerData;
import com.pixelmongenerations.core.network.packetHandlers.npc.DeleteNPC;
import com.pixelmongenerations.core.network.packetHandlers.npc.EnumNPCServerPacketType;
import com.pixelmongenerations.core.network.packetHandlers.npc.NPCServerPacket;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.input.Keyboard;

public class GuiTrainerEditor
extends GuiContainerDropDown {
    public static ArrayList<PixelmonData> pokemonList = new ArrayList();
    public static int currentTrainerID;
    public static SetTrainerData trainerData;
    NPCTrainer trainer;
    GuiTextField nameBox;
    private TextureEditorNPC textureEditor;
    String oldName = "";
    private static final int BUTTON_ID_RULES = 11;
    private static final int DROP_DOWN_HEIGHT_OFFSET = 25;
    BaseTrainer model;

    public GuiTrainerEditor(int trainerId) {
        Keyboard.enableRepeatEvents((boolean)true);
        Optional<NPCTrainer> entityNPCOptional = EntityNPC.locateNPCClient(Minecraft.getMinecraft().world, trainerId, NPCTrainer.class);
        if (!entityNPCOptional.isPresent()) {
            GuiHelper.closeScreen();
            return;
        }
        this.trainer = entityNPCOptional.get();
        currentTrainerID = trainerId;
        if (trainerData == null) {
            trainerData = new SetTrainerData("", "", "", "", 0, new ItemStack[0], new BattleRules());
        }
        this.trainer.update(trainerData);
    }

    @Override
    public void initGui() {
        super.initGui();
        if (this.trainer == null) {
            GuiHelper.closeScreen();
            return;
        }
        this.buttonList.add(new GuiButton(1, this.width / 2 + 155, this.height / 2 + 90, 30, 20, I18n.translateToLocal("gui.guiItemDrops.ok")));
        this.textureEditor = new TextureEditorNPC(this, this.trainer, this.width / 2 - 20, this.height / 2 - 84, 180);
        this.buttonList.add(new GuiButton(3, this.width / 2 - 80, this.height / 2 + 90, 100, 20, I18n.translateToLocal("gui.trainereditor.edit")));
        int dropDownLeft = this.width / 2 + 40;
        int dropDownHeightStart = this.height / 2 + 20;
        int dropDownWidth = 100;
        int dropDownHeight = 100;
        this.addDropDown(new GuiDropDown<EnumBossMode>(Arrays.asList(EnumBossMode.values()), this.trainer.getBossMode(), dropDownLeft, dropDownHeightStart, dropDownWidth, dropDownHeight).setGetOptionString(EnumBossMode::getLocalizedName).setOnSelected(this::selectBossMode));
        this.addDropDown(new GuiDropDown<EnumTrainerAI>(Arrays.asList(EnumTrainerAI.values()), this.trainer.getAIMode(), dropDownLeft, dropDownHeightStart + 25, dropDownWidth, dropDownHeight).setGetOptionString(EnumTrainerAI::getLocalizedName).setOnSelected(aiMode -> this.trainer.setAIMode((EnumTrainerAI)((Object)aiMode))));
        int battleAITop = dropDownHeightStart + 50;
        this.addDropDown(new GuiDropDown<EnumBattleAIMode>(Arrays.asList(EnumBattleAIMode.values()), this.trainer.getBattleAIMode(), dropDownLeft, battleAITop - 10, dropDownWidth, dropDownHeight).setGetOptionString(EnumBattleAIMode::getLocalizedName).setOnSelected(this::selectBattleAIMode).setInactiveTop(battleAITop));
        int encounterModeTop = dropDownHeightStart + 75;
        this.addDropDown(new GuiDropDown<EnumEncounterMode>(Arrays.asList(EnumEncounterMode.values()), this.trainer.getEncounterMode(), dropDownLeft, encounterModeTop - 35, dropDownWidth, dropDownHeight).setGetOptionString(EnumEncounterMode::getLocalizedName).setOnSelected(this::selectEncounterMode).setInactiveTop(encounterModeTop));
        this.buttonList.add(new GuiButton(11, this.width / 2 + 40, this.height / 2 - 18, 100, 20, I18n.translateToLocal("gui.battlerules.title")));
        this.buttonList.add(new GuiButton(7, this.width / 2 - 190, this.height / 2 + 90, 100, 20, I18n.translateToLocal("gui.trainereditor.more")));
        this.buttonList.add(new GuiButton(8, this.width / 2 - 180, this.height / 2 - 120, 80, 20, I18n.translateToLocal("gui.trainereditor.delete")));
        this.nameBox = new GuiTextField(12, this.mc.fontRenderer, this.width / 2 - 20, this.height / 2 - 116, 180, 20);
        if (trainerData != null) {
            this.nameBox.setText(GuiTrainerEditor.trainerData.name);
            this.oldName = GuiTrainerEditor.trainerData.name;
        }
    }

    private void selectBossMode(EnumBossMode bossMode) {
        this.trainer.setBossMode(bossMode);
        Pixelmon.NETWORK.sendToServer(new NPCServerPacket(currentTrainerID, bossMode));
    }

    private void selectBattleAIMode(EnumBattleAIMode battleAI) {
        this.trainer.setBattleAIMode(battleAI);
        Pixelmon.NETWORK.sendToServer(new NPCServerPacket(currentTrainerID, battleAI));
    }

    private void selectEncounterMode(EnumEncounterMode encounterMode) {
        this.trainer.setEncounterMode(encounterMode);
        Pixelmon.NETWORK.sendToServer(new NPCServerPacket(currentTrainerID, encounterMode));
    }

    @Override
    protected void drawBackgroundUnderMenus(float f, int i, int j) {
        if (this.trainer == null) {
            GuiHelper.closeScreen();
            return;
        }
        if (trainerData != null && !GuiTrainerEditor.trainerData.name.equals(this.oldName)) {
            this.oldName = GuiTrainerEditor.trainerData.name;
            this.nameBox.setText(GuiTrainerEditor.trainerData.name);
        }
        this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
        GuiHelper.drawImageQuad(this.width / 2 - 200, this.height / 2 - 120, 400.0, 240.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        GuiHelper.drawEntity(this.trainer, this.width / 2 - 140, this.height / 2 + 60, 60.0f, 0.0f, 0.0f);
        int dropDownLabelX = this.width / 2 + 90;
        int dropDownHeightStart = this.height / 2 + 10;
        GuiHelper.drawCenteredString(I18n.translateToLocal("gui.trainereditor.bosstype"), dropDownLabelX, dropDownHeightStart, 0);
        GuiHelper.drawCenteredString(I18n.translateToLocal("gui.trainereditor.aimode"), dropDownLabelX, dropDownHeightStart + 25, 0);
        GuiHelper.drawCenteredString(I18n.translateToLocal("gui.trainereditor.battleaimode"), dropDownLabelX, dropDownHeightStart + 50, 0);
        GuiHelper.drawCenteredString(I18n.translateToLocal("gui.trainereditor.encountermode"), dropDownLabelX, dropDownHeightStart + 75, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.name"), this.width / 2 - 60, this.height / 2 - 110, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.model"), this.width / 2 - 60, this.height / 2 - 83, 0);
        this.nameBox.drawTextBox();
        this.textureEditor.drawCustomTextBox();
        this.drawPokemonList();
    }

    private void drawPokemonList() {
        int top = this.height / 2 + 20;
        int ySize = 68;
        int left = this.width / 2 - 82;
        int xSize = 104;
        GuiTrainerEditor.drawRect(left - 1, top - 1, left + xSize, top + ySize, -16777215);
        GuiTrainerEditor.drawRect(left, top, left + xSize, top + ySize, -6777215);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.partypokemon"), left + 1, top - 11, 0);
        for (int n = 0; n < pokemonList.size(); ++n) {
            PixelmonData p = pokemonList.get(n);
            if (p == null) continue;
            this.mc.fontRenderer.drawString(Entity1Base.getLocalizedName(p.name), left + 4, top + 4 + n * 10, 0);
            this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.lvl") + " " + p.lvl, left + 65, top + 4 + n * 10, 0);
        }
    }

    @Override
    protected void keyTyped(char key, int keyCode) {
        this.nameBox.textboxKeyTyped(key, keyCode);
        this.textureEditor.keyTyped(key, keyCode, this.nameBox);
        if (keyCode == 1 || keyCode == 28) {
            this.saveFields();
        }
    }

    @Override
    protected void mouseClickedUnderMenus(int x, int y, int mouseButton) throws IOException {
        if (this.nameBox == null) {
            this.initGui();
        }
        this.nameBox.mouseClicked(x, y, mouseButton);
        this.textureEditor.mouseClicked(x, y, mouseButton);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if (button.enabled) {
            if (button.id == 1) {
                this.saveFields();
            } else if (button.id == 3) {
                this.mc.displayGuiScreen(new GuiTrainerEditorPartyScreen());
            } else if (button.id == 7) {
                this.mc.displayGuiScreen(new GuiTrainerEditorMore());
            } else if (button.id == 8) {
                Pixelmon.NETWORK.sendToServer(new DeleteNPC(currentTrainerID));
                GuiHelper.closeScreen();
            } else if (button.id == 11) {
                this.mc.displayGuiScreen(new GuiBattleRulesNPCEditor());
            }
        }
    }

    private void saveFields() {
        if (this.checkFields()) {
            GuiHelper.closeScreen();
        }
    }

    private boolean checkFields() {
        if (this.nameBox.getText().equals("") || trainerData == null) {
            return false;
        }
        if (currentTrainerID <= 0) {
            currentTrainerID = this.trainer.getId();
        }
        if (!this.nameBox.getText().equals(GuiTrainerEditor.trainerData.name)) {
            Pixelmon.NETWORK.sendToServer(new NPCServerPacket(currentTrainerID, EnumNPCServerPacketType.Name, this.nameBox.getText()));
        }
        Pixelmon.NETWORK.sendToServer(new NPCServerPacket(currentTrainerID, this.trainer.getAIMode()));
        this.textureEditor.saveCustomTexture();
        return true;
    }
}

