/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.feeder;

import com.pixelmongenerations.client.gui.cookingpot.PredicateSlotItemHandler;
import com.pixelmongenerations.common.block.tileEntities.TileEntityFeeder;
import com.pixelmongenerations.common.item.ItemPokeblock;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

public class ContainerFeeder
extends Container {
    public final TileEntityFeeder tileentity;

    public ContainerFeeder(InventoryPlayer player, TileEntityFeeder tileentity) {
        this.tileentity = tileentity;
        IItemHandler handler = tileentity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 0, 52, 21, ContainerFeeder::isPokeBlock));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 1, 44, 53, ContainerFeeder::isPokeBlock));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 2, 62, 53, ContainerFeeder::isPokeBlock));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 3, 80, 53, ContainerFeeder::isPokeBlock));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 4, 98, 53, ContainerFeeder::isPokeBlock));
        this.addSlotToContainer(new PredicateSlotItemHandler(handler, 5, 116, 53, ContainerFeeder::isPokeBlock));
        for (int y = 0; y < 3; ++y) {
            for (int x = 0; x < 9; ++x) {
                this.addSlotToContainer(new Slot(player, x + y * 9 + 9, 8 + x * 18, 64 + y * 18 + 11 + 9));
            }
        }
        for (int x = 0; x < 9; ++x) {
            this.addSlotToContainer(new Slot(player, x, 8 + x * 18, 142));
        }
    }

    private static boolean isPokeBlock(ItemStack itemStack) {
        return itemStack.getItem() instanceof ItemPokeblock;
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return this.tileentity.isUsableByPlayer(playerIn);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
        ItemStack stack = ItemStack.EMPTY;
        Slot slot = (Slot)this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack slotStack = slot.getStack();
            stack = slotStack.copy();
            if (index < 6 ? !this.mergeItemStack(slotStack, 6, 41, false) : index < 41 && !this.mergeItemStack(slotStack, 0, 5, false)) {
                return ItemStack.EMPTY;
            }
            if (slotStack.getCount() == 0) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
            if (slotStack.getCount() == stack.getCount()) {
                return ItemStack.EMPTY;
            }
            slot.onTake(playerIn, stack);
        }
        return stack;
    }
}

