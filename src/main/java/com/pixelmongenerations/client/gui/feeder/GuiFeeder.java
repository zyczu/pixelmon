/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.feeder;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.cookingpot.GuiCookingPot;
import com.pixelmongenerations.client.gui.feeder.ContainerFeeder;
import com.pixelmongenerations.common.block.tileEntities.TileEntityFeeder;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.FeederSpawnPokemon;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;

public class GuiFeeder
extends GuiContainer {
    public static final ResourceLocation feederGuiTextures = new ResourceLocation("pixelmon:textures/gui/machines/feeder.png");
    public static EntityPixelmon pokemon;
    private final BlockPos pos;

    public GuiFeeder(InventoryPlayer inventoryPlayer, TileEntityFeeder tileFeeder) {
        super(new ContainerFeeder(inventoryPlayer, tileFeeder));
        this.pos = tileFeeder.getPos();
        this.xSize = 176;
        this.ySize = 166;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        if (pokemon != null) {
            PixelmonData data = new PixelmonData(pokemon);
            if (pokemon.getPokemonName().equalsIgnoreCase("lilligant") || pokemon.getPokemonName().equalsIgnoreCase("arcanine") || pokemon.getPokemonName().equalsIgnoreCase("electrode") || pokemon.getPokemonName().equalsIgnoreCase("avalugg")) {
                data.form = (short)11;
            }
            this.drawPokemonSprite(data, Minecraft.getMinecraft());
        }
        this.renderHoveredToolTip(mouseX, mouseY);
    }

    private void drawPokemonSprite(PixelmonData p, Minecraft mc) {
        RenderHelper.enableGUIStandardItemLighting();
        GlStateManager.disableLighting();
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.bindPokemonSprite(p, mc);
        GuiHelper.drawImageQuad(this.guiLeft + 108, this.guiTop + 21, 16.0, 16.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        GlStateManager.enableLighting();
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        String tileName = I18n.format("container.feeder", new Object[0]);
        this.fontRenderer.drawString(tileName, this.xSize / 2 - this.fontRenderer.getStringWidth(tileName) / 2 + 3, 8, 0x404040);
        this.fontRenderer.drawString(I18n.format("container.inventory", new Object[0]), 7, 72, 0x404040);
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        if (GuiCookingPot.findPoint(this.guiLeft + 107, this.guiTop + 21, this.guiLeft + 124, this.guiTop + 37, mouseX, mouseY)) {
            Pixelmon.NETWORK.sendToServer(new FeederSpawnPokemon(this.pos));
        }
    }

    public static boolean contains(int x, int y, int width, int height, int xPos, int yPos) {
        return x >= xPos && x <= xPos + width && y >= yPos && y <= yPos + height;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.mc.getTextureManager().bindTexture(feederGuiTextures);
        this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
    }
}

