/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.spawner;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.elements.GuiSlotBase;
import com.pixelmongenerations.client.gui.spawner.GuiPixelmonCustomGrass;
import com.pixelmongenerations.common.block.machines.PokemonRarity;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;

public class GuiPokemonGrassList
extends GuiSlotBase {
    private final Minecraft mc;
    GuiPixelmonCustomGrass gui;

    public GuiPokemonGrassList(GuiPixelmonCustomGrass gui, int width, int height, int top, Minecraft mc) {
        super(top, 0, width, height, true);
        this.mc = mc;
        this.gui = gui;
        this.slotHeight = 24;
    }

    @Override
    protected void elementClicked(int index, boolean doubleClicked) {
        this.gui.removeFromList(index);
    }

    @Override
    protected void drawSlot(int entryID, int x, int yTop, int yMiddle, Tessellator tessellator) {
        PokemonRarity r = this.gui.getPokemonListEntry(entryID);
        if (r != null) {
            GlStateManager.pushMatrix();
            GlStateManager.color(1.0f, 1.0f, 1.0f);
            GuiHelper.bindPokeSprite(r.pokemon, false, r.form, r.texture);
            GuiHelper.drawImageQuad(x + 4, yTop - 4, 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
            this.mc.fontRenderer.drawString(Entity1Base.getLocalizedName(r.pokemon.name), x + 34, yTop + 6, 13094, false);
            this.mc.fontRenderer.drawString("R:" + r.rarity + " F:" + r.form + " T:" + r.texture + " S:" + r.shinyChance, x + 106, yTop + 6, 13094, false);
            GlStateManager.popMatrix();
        }
    }

    @Override
    protected float[] get1Color() {
        return new float[]{1.0f, 1.0f, 1.0f};
    }

    @Override
    protected int getSize() {
        return this.gui.getPokemonListCount();
    }

    @Override
    protected boolean isSelected(int element) {
        return false;
    }

    @Override
    protected int[] getSelectionColor() {
        return new int[]{128, 128, 128};
    }
}

