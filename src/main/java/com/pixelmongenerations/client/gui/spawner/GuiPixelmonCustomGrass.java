/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 */
package com.pixelmongenerations.client.gui.spawner;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.spawner.GuiPokemonGrassList;
import com.pixelmongenerations.common.block.enums.EnumSpawnerAggression;
import com.pixelmongenerations.common.block.machines.PokemonRarity;
import com.pixelmongenerations.common.block.spawning.TileEntityPixelmonGrass;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.PixelmonGrassData;
import com.pixelmongenerations.core.network.packetHandlers.UpdateGrass;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.input.Keyboard;

public class GuiPixelmonCustomGrass
extends GuiContainer {
    TileEntityPixelmonGrass ps;
    int listTop;
    int listLeft;
    int listHeight;
    int listWidth;
    GuiTextField pokemonNameBox;
    GuiTextField rarityBox;
    GuiTextField formBox;
    GuiTextField textureBox;
    GuiTextField shinyChanceBox;
    GuiTextField levelMinBox;
    GuiTextField levelMaxBox;
    GuiTextField bossRatioBox;
    GuiPokemonGrassList list;

    public GuiPixelmonCustomGrass(int x, int y, int z) {
        super(new ContainerEmpty());
        Keyboard.enableRepeatEvents((boolean)true);
        this.ps = (TileEntityPixelmonGrass)Minecraft.getMinecraft().world.getTileEntity(new BlockPos(x, y, z));
        this.listTop = 58;
        this.listLeft = 30;
        this.listHeight = 100;
        this.listWidth = 100;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.list = new GuiPokemonGrassList(this, 200, this.listHeight, this.listTop - 40, this.mc);
        this.buttonList.add(new GuiButton(0, this.listLeft + this.listWidth + 30, this.listTop + this.listHeight + 20, 30, 20, I18n.translateToLocal("gui.trainereditor.add")));
        this.buttonList.add(new GuiButton(1, 370, 190, 30, 20, I18n.translateToLocal("gui.guiItemDrops.ok")));
        this.buttonList.add(new GuiButton(3, 270, 180, 80, 20, this.ps.aggression.getLocalizedName()));
        this.buttonList.add(new GuiButton(4, 270, 200, 80, 20, this.ps.spawnLocation.getLocalizedName()));
        this.pokemonNameBox = new GuiTextField(5, this.mc.fontRenderer, this.listLeft, this.listTop + this.listHeight + 20, 80, 20);
        this.pokemonNameBox.setText("");
        this.rarityBox = new GuiTextField(6, this.mc.fontRenderer, this.listLeft + 85, this.listTop + this.listHeight + 20, 40, 20);
        this.rarityBox.setText("");
        this.formBox = new GuiTextField(6, this.mc.fontRenderer, this.listLeft + 85, this.listTop + this.listHeight + 55, 40, 20);
        this.formBox.setText("-1");
        this.shinyChanceBox = new GuiTextField(6, this.mc.fontRenderer, this.listLeft, this.listTop + this.listHeight + 55, 40, 20);
        this.shinyChanceBox.setText("4096");
        this.textureBox = new GuiTextField(6, this.mc.fontRenderer, this.listLeft + 46, this.listTop + this.listHeight + 55, 40, 20);
        this.textureBox.setText("0");
        this.formBox = new GuiTextField(6, this.mc.fontRenderer, this.listLeft + 90, this.listTop + this.listHeight + 55, 40, 20);
        this.formBox.setText("-1");
        this.levelMinBox = new GuiTextField(10, this.mc.fontRenderer, 350, this.listTop + 60, 40, 20);
        this.levelMinBox.setText("" + this.ps.levelMin);
        this.levelMaxBox = new GuiTextField(11, this.mc.fontRenderer, 350, this.listTop + 80, 40, 20);
        this.levelMaxBox.setText("" + this.ps.levelMax);
        this.bossRatioBox = new GuiTextField(12, this.mc.fontRenderer, 350, this.listTop + 100, 40, 20);
        this.bossRatioBox.setText("" + this.ps.bossRatio);
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        this.pokemonNameBox.textboxKeyTyped(typedChar, keyCode);
        this.rarityBox.textboxKeyTyped(typedChar, keyCode);
        this.formBox.textboxKeyTyped(typedChar, keyCode);
        this.levelMinBox.textboxKeyTyped(typedChar, keyCode);
        this.levelMaxBox.textboxKeyTyped(typedChar, keyCode);
        this.bossRatioBox.textboxKeyTyped(typedChar, keyCode);
        this.textureBox.textboxKeyTyped(typedChar, keyCode);
        this.shinyChanceBox.textboxKeyTyped(typedChar, keyCode);
        GuiHelper.switchFocus(keyCode, this.levelMinBox, this.levelMaxBox, this.bossRatioBox, this.pokemonNameBox, this.rarityBox, this.formBox);
        if (keyCode == 1 || keyCode == 28) {
            this.saveFields();
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.pokemonNameBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.rarityBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.formBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.levelMinBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.levelMaxBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.bossRatioBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.textureBox.mouseClicked(mouseX, mouseY, mouseButton);
        this.shinyChanceBox.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float mfloat, int mouseX, int mouseY) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.mc.renderEngine.bindTexture(GuiResources.spawnerBackground);
        GuiHelper.drawImageQuad(0.0, 0.0, this.width, this.height, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmongrassspawner.pcms"), 250, 10, 13094);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.pokemonlist"), 50, 5, 13094);
        this.list.drawScreen(mouseX, mouseY, mfloat);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.name"), this.listLeft + 2, this.listTop + this.listHeight + 10, 13094);
        this.pokemonNameBox.drawTextBox();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.rarity"), this.listLeft + 87, this.listTop + this.listHeight + 10, 13094);
        this.rarityBox.drawTextBox();
        GuiHelper.drawCenteredString(I18n.translateToLocal("gui.pixelmonspawner.shinyChance"), this.shinyChanceBox.x + 20, this.shinyChanceBox.y - 10, 13094);
        this.shinyChanceBox.drawTextBox();
        GuiHelper.drawCenteredString(I18n.translateToLocal("gui.pixelmonspawner.texture"), this.textureBox.x + 20, this.textureBox.y - 10, 13094);
        this.textureBox.drawTextBox();
        GuiHelper.drawCenteredString(I18n.translateToLocal("gui.pixelmonspawner.form"), this.formBox.x + 20, this.formBox.y - 10, 13094);
        this.formBox.drawTextBox();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.minlevel"), 275, this.listTop + 66, 13094);
        this.levelMinBox.drawTextBox();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.maxlevel"), 275, this.listTop + 86, 13094);
        this.levelMaxBox.drawTextBox();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.bossratio"), 275, this.listTop + 106, 13094);
        this.bossRatioBox.drawTextBox();
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.aggression"), 205, this.listTop + 128, 13094);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.pixelmonspawner.spawnlocation"), 185, this.listTop + 148, 13094);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if (button.enabled) {
            switch (button.id) {
                case 0: {
                    int shinyChance;
                    int texture;
                    int form;
                    int rarity;
                    EnumSpecies pokemon = EnumSpecies.getFromNameAnyCase(this.pokemonNameBox.getText());
                    try {
                        rarity = Integer.parseInt(this.rarityBox.getText());
                        form = Integer.parseInt(this.formBox.getText());
                        texture = Integer.parseInt(this.textureBox.getText());
                        shinyChance = Integer.parseInt(this.shinyChanceBox.getText());
                    }
                    catch (NumberFormatException e) {
                        return;
                    }
                    if (pokemon == null || rarity <= 0) break;
                    this.ps.pokemonList.add(new PokemonRarity(pokemon, rarity, form, texture, shinyChance));
                    break;
                }
                case 1: {
                    this.saveFields();
                    break;
                }
                case 3: {
                    this.ps.aggression = EnumSpawnerAggression.nextAggression(this.ps.aggression);
                    button.displayString = this.ps.aggression.getLocalizedName();
                    break;
                }
                case 4: {
                    this.ps.spawnLocation = SpawnLocation.nextLocation(this.ps.spawnLocation);
                    button.displayString = this.ps.spawnLocation.getLocalizedName();
                }
            }
        }
    }

    private void saveFields() {
        if (this.checkFields()) {
            GuiHelper.closeScreen();
        }
    }

    private boolean checkFields() {
        try {
            int levelMin = Integer.parseInt(this.levelMinBox.getText());
            int levelMax = Integer.parseInt(this.levelMaxBox.getText());
            int bossRatio = Integer.parseInt(this.bossRatioBox.getText());
            if (levelMin > 0 && levelMin <= PixelmonServerConfig.maxLevel && levelMax >= levelMin && levelMax <= PixelmonServerConfig.maxLevel) {
                this.ps.levelMin = levelMin;
                this.ps.levelMax = levelMax;
                this.ps.bossRatio = bossRatio;
                NBTTagCompound nbt = new NBTTagCompound();
                this.ps.writeToNBT(nbt);
                PixelmonGrassData p = new PixelmonGrassData(this.ps.getPos(), nbt);
                Pixelmon.NETWORK.sendToServer(new UpdateGrass(p));
                return true;
            }
        }
        catch (Exception exception) {
            // empty catch block
        }
        return false;
    }

    public PokemonRarity getPokemonListEntry(int ind) {
        if (ind < this.ps.pokemonList.size() && ind >= 0) {
            return this.ps.pokemonList.get(ind);
        }
        return null;
    }

    public int getPokemonListCount() {
        return this.ps.pokemonList.size();
    }

    public void removeFromList(int var1) {
        this.ps.pokemonList.remove(var1);
    }

    public static Minecraft getMinecraft(GuiPixelmonCustomGrass gui) {
        return gui.mc;
    }
}

