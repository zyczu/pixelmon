/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.common.block.tileEntities.TileEntityHealer;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.math.BlockPos;

public class GuiHealer
extends GuiContainer {
    boolean isNurse = false;
    TileEntityHealer healer;

    public GuiHealer() {
        super(new ContainerEmpty());
    }

    public GuiHealer(int x, int y, int z) {
        this();
        this.isNurse = true;
        this.healer = (TileEntityHealer)Minecraft.getMinecraft().world.getTileEntity(new BlockPos(x, y, z));
    }

    @Override
    public void initGui() {
        super.initGui();
    }

    @Override
    public void updateScreen() {
    }

    @Override
    public void actionPerformed(GuiButton b) {
    }

    @Override
    public void drawBackground(int par1) {
    }

    @Override
    public void drawDefaultBackground() {
    }

    @Override
    public void handleKeyboardInput() {
    }

    @Override
    public void drawGuiContainerBackgroundLayer(float f, int i, int i1) {
        if (this.isNurse) {
            this.mc.renderEngine.bindTexture(GuiResources.evo);
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            GuiHelper.drawImageQuad(this.width / 2 - 120, this.height / 4 - 40, 240.0, 80.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            String name = I18n.format("gui.nursejoy.name", new Object[0]);
            String chat = I18n.format("gui.nursejoy.healing", new Object[0]);
            this.mc.fontRenderer.drawString(name, this.width / 2 - 107, this.height / 4 - 32, 0xFFFFFF);
            ArrayList<String> chatText = new ArrayList<String>();
            chatText.add(chat);
            for (int index = 0; index < chatText.size(); ++index) {
                while (this.mc.fontRenderer.getStringWidth((String)chatText.get(index)) > 200) {
                    String page = (String)chatText.get(index);
                    int li = page.lastIndexOf(" ");
                    if (index + 1 == chatText.size()) {
                        chatText.add(page.substring(li + 1));
                    } else {
                        chatText.set(index + 1, page.substring(li + 1) + " " + (String)chatText.get(index + 1));
                    }
                    chatText.set(index, page.substring(0, li));
                }
                this.mc.fontRenderer.drawString((String)chatText.get(index), this.width / 2 - this.mc.fontRenderer.getStringWidth((String)chatText.get(index)) / 2, this.height / 4 - 20 + index * 14, 0xFFFFFF);
            }
        }
    }
}

