/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.mount;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.mount.ClientMountDataManager;
import com.pixelmongenerations.client.util.ClientUtils;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.SummonMountPacket;
import com.pixelmongenerations.core.storage.playerData.MountData;
import java.io.IOException;
import java.util.Map;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;

public class GuiMountSelection
extends GuiContainer {
    public GuiMountSelection() {
        super(new ContainerEmpty());
        this.xSize = 505;
        this.ySize = 264;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.mc.getTextureManager().bindTexture(GuiResources.fluteMenu);
        ClientUtils.drawImageQuad(this.guiLeft, this.guiTop, this.xSize, this.ySize, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        this.fontRenderer.drawString("Celestial Flute", this.guiLeft + (this.xSize - this.fontRenderer.getStringWidth("Celestial Flute")) / 2, this.guiTop + 40, 16704021, false);
        this.fontRenderer.drawString("Click a Pokemon to summon", this.guiLeft + (this.xSize - this.fontRenderer.getStringWidth("Click a Pokemon to summon")) / 2, this.guiTop + 58, 16704021, false);
        MountData data = ClientMountDataManager.getPlayerMountData();
        int count = 0;
        for (Map.Entry<PokemonSpec, Boolean> entry : data.mountRegistry.entrySet()) {
            int x = this.guiLeft + 14 + count * 99;
            int y = this.guiTop + 85;
            int textColor = 0xFFFFFF;
            float tint = 1.0f;
            if (GuiMountSelection.containsPoint(x, y, 84, 125, mouseX, mouseY)) {
                textColor = 16572537;
                tint = 0.7f;
            }
            if (!entry.getValue().booleanValue()) {
                tint = 0.3f;
                textColor = 16566079;
            }
            this.drawSelection(x, y, entry.getKey(), textColor, tint);
            ++count;
        }
    }

    private void drawSelection(int x, int y, PokemonSpec spec, int color, float tint) {
        if (spec == null) {
            return;
        }
        RenderHelper.enableGUIStandardItemLighting();
        GlStateManager.disableLighting();
        this.fontRenderer.drawString(spec.name, (float)(x + 42) - (float)this.fontRenderer.getStringWidth(spec.name) / 2.0f, y + 20, color, false);
        GlStateManager.color(tint, tint, tint, 1.0f);
        GuiHelper.bindPokemonSprite(spec, Minecraft.getMinecraft());
        GuiHelper.drawImageQuad(x + 8, y + 49, 68.0, 68.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        GlStateManager.enableLighting();
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int button) throws IOException {
        MountData data = ClientMountDataManager.getPlayerMountData();
        int count = 0;
        for (Map.Entry<PokemonSpec, Boolean> entry : data.mountRegistry.entrySet()) {
            if (entry.getKey() != null && entry.getValue().booleanValue() && GuiMountSelection.containsPoint(this.guiLeft + 14 + count * 99, this.guiTop + 85, 84, 125, mouseX, mouseY)) {
                this.mc.player.closeScreen();
                Pixelmon.NETWORK.sendToServer(new SummonMountPacket(entry.getKey()));
                return;
            }
            ++count;
        }
    }

    public static boolean containsPoint(int x, int y, int width, int height, int xValue, int yValue) {
        return xValue >= x && xValue <= x + width && yValue >= y && yValue <= y + height;
    }
}

