/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 *  org.lwjgl.opengl.GL11
 *  org.lwjgl.util.vector.Vector4f
 */
package com.pixelmongenerations.client.gui;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonMovesetData;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.helper.SpriteHelper;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector4f;

public class GuiHelper {
    public static boolean isMouseOverButton(GuiButton button, int mouseX, int mouseY) {
        return mouseX >= button.x && mouseY >= button.y && mouseX < button.x + button.width && mouseY < button.y + button.height;
    }

    public static void drawStringRightAligned(String text, float x, float y, int color) {
        GuiHelper.drawStringRightAligned(text, x, y, color, false);
    }

    public static void drawStringRightAligned(String text, float x, float y, int color, boolean dropShadow) {
        FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
        fontRenderer.drawString(text, x -= (float)fontRenderer.getStringWidth(text), y, color, dropShadow);
    }

    public static void drawImageQuad(double x, double y, double w, float h, double us, double vs, double ue, double ve, float zLevel) {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        buffer.begin(7, DefaultVertexFormats.POSITION_TEX);
        buffer.pos(x, y + (double)h, zLevel).tex(us, ve).endVertex();
        buffer.pos(x + w, y + (double)h, zLevel).tex(ue, ve).endVertex();
        buffer.pos(x + w, y, zLevel).tex(ue, vs).endVertex();
        buffer.pos(x, y, zLevel).tex(us, vs).endVertex();
        tessellator.draw();
    }

    public static void drawRect(double x, double y, double w, float h, Color color, float zLevel) {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        buffer.begin(7, DefaultVertexFormats.POSITION_COLOR);
        buffer.pos(x, y + (double)h, zLevel).color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()).endVertex();
        buffer.pos(x + w, y + (double)h, zLevel).color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()).endVertex();
        buffer.pos(x + w, y, zLevel).color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()).endVertex();
        buffer.pos(x, y, zLevel).color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()).endVertex();
        tessellator.draw();
    }

    public static void bindPokemonSprite(PokemonSpec spec, Minecraft mc) {
        if (spec == null || spec.getSpecies() == null) {
            return;
        }
        GuiHelper.bindPokemonSprite(spec.getSpecies().getNationalPokedexInteger(), spec.form != null ? spec.form : 0, spec.gender != null ? Gender.getGender(spec.gender.shortValue()) : Gender.None, 0, spec.customTexture != null ? spec.customTexture : "", spec.shiny != null ? spec.shiny : false, false, 0, mc);
    }

    public static void bindPokemonSprite(PixelmonData pkt, Minecraft mc) {
        if (pkt == null) {
            return;
        }
        GuiHelper.bindPokemonSprite(pkt.getNationalPokedexNumber(), pkt.form, pkt.gender, pkt.specialTexture, pkt.customTexture, pkt.isShiny, pkt.isEgg, pkt.eggCycles, mc);
    }

    public static void bindPokemonSprite(int pokedexNumber, int form, Gender gender, int specialTexture, boolean isShiny, boolean isEgg, int eggCycles, Minecraft mc) {
        GuiHelper.bindPokemonSprite(pokedexNumber, form, gender, specialTexture, null, isShiny, isEgg, eggCycles, mc);
    }

    public static void bindPokemonSprite(int pokedexNumber, int form, Gender gender, int specialTexture, String customTexture, boolean isShiny, boolean isEgg, int eggCycles, Minecraft mc) {
        EnumSpecies pokemon = EnumSpecies.getFromDex(pokedexNumber).get();
        if (isEgg) {
            if (pokemon.name.equalsIgnoreCase("Togepi")) {
                if (eggCycles > 10) {
                    mc.renderEngine.bindTexture(GuiResources.eggTogepi1);
                } else if (eggCycles > 5) {
                    mc.renderEngine.bindTexture(GuiResources.eggTogepi2);
                } else {
                    mc.renderEngine.bindTexture(GuiResources.eggTogepi3);
                }
            } else if (pokemon.name.equalsIgnoreCase("Manaphy")) {
                if (eggCycles > 10) {
                    mc.renderEngine.bindTexture(GuiResources.eggManaphy1);
                } else if (eggCycles > 5) {
                    mc.renderEngine.bindTexture(GuiResources.eggManaphy2);
                } else {
                    mc.renderEngine.bindTexture(GuiResources.eggManaphy3);
                }
            } else if (eggCycles > 10) {
                mc.renderEngine.bindTexture(GuiResources.eggNormal1);
            } else if (eggCycles > 5) {
                mc.renderEngine.bindTexture(GuiResources.eggNormal2);
            } else {
                mc.renderEngine.bindTexture(GuiResources.eggNormal3);
            }
        } else {
            GuiHelper.bindPokemonSprite(pokedexNumber, form, gender, specialTexture, customTexture, isShiny, mc);
        }
    }

    public static void bindPokemonSprite(int pokedexNumber, int form, Gender gender, boolean isShiny, Minecraft mc) {
        GuiHelper.bindPokemonSprite(pokedexNumber, form, gender, 0, isShiny, mc);
    }

    public static void bindPokemonSprite(int pokedexNumber, int form, Gender gender, int specialTexture, boolean isShiny, Minecraft mc) {
        GuiHelper.bindPokemonSprite(pokedexNumber, isShiny, mc, SpriteHelper.getSpriteExtra(EnumSpecies.getFromDex((int)pokedexNumber).get().name, form, gender, specialTexture, null));
    }

    public static void bindPokemonSprite(int pokedexNumber, int form, Gender gender, int specialTexture, String customTexture, boolean isShiny, Minecraft mc) {
        String numString = String.format("%03d", pokedexNumber);
        String extra = SpriteHelper.getSpriteExtra(EnumSpecies.getFromDex((int)pokedexNumber).get().name, form, gender, specialTexture, customTexture);
        TextureResource dynTexture = ClientProxy.TEXTURE_STORE.getObject(numString + "-" + extra);
        if (dynTexture == null) {
            extra = SpriteHelper.getSpriteExtra(EnumSpecies.getFromDex((int)pokedexNumber).get().name, form, gender, specialTexture, null);
        }
        GuiHelper.bindPokemonSprite(pokedexNumber, isShiny, mc, extra);
    }

    public static void bindPokeSprite(EnumSpecies pokemon, boolean shiny, int form, int texture) {
        byte formAdjusted = pokemon.getFormEnum(form).getForm();
        String filePath = String.format("%03d", pokemon.getNationalPokedexInteger());
        filePath = filePath + SpriteHelper.getSpriteExtra(pokemon.name, formAdjusted, Gender.None, texture, null);
        Minecraft mc = Minecraft.getMinecraft();
        mc.renderEngine.bindTexture(GuiHelper.checkSpecialTexture(filePath, "", shiny));
    }

    public static void bindPokemonSprite(int npn, boolean isShiny, Minecraft mc) {
        GuiHelper.bindPokemonSprite(npn, isShiny, mc, "");
    }

    public static void bindPokemonSprite(int npn, boolean isShiny, Minecraft mc, String extra) {
        String numString = String.format("%03d", npn);
        TextureResource dynTexture = ClientProxy.TEXTURE_STORE.getObject(numString + "-" + extra);
        if (dynTexture != null) {
            dynTexture.bindTexture();
            return;
        }
        mc.renderEngine.bindTexture(GuiHelper.checkSpecialTexture(numString, extra, isShiny));
    }

    private static ResourceLocation getResourceLocation(String detail, boolean shiny) {
        return shiny ? GuiResources.shinySprite(detail) : GuiResources.sprite(detail);
    }

    public static ResourceLocation checkSpecialTexture(String numString, String extra, boolean shiny) {
        ResourceLocation location = GuiHelper.getResourceLocation(numString + extra, shiny);
        if (Pixelmon.PROXY.resourceLocationExists(location)) {
            return location;
        }
        location = GuiHelper.getResourceLocation(numString + extra, !shiny);
        if (Pixelmon.PROXY.resourceLocationExists(location)) {
            return location;
        }
        return GuiHelper.getResourceLocation(numString, shiny);
    }

    public static void drawScoreboard(int top, int right, int alpha, String title, Collection<String> lines, Collection<String> scores) {
        FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
        int tempWidth = GuiHelper.getLongestStringWidth(lines);
        if (scores != null) {
            tempWidth += GuiHelper.getLongestStringWidth(scores) + 20;
        }
        if (fontRenderer.getStringWidth(title) > tempWidth) {
            tempWidth = fontRenderer.getStringWidth(title);
        }
        int left = right - tempWidth - 3;
        int boxHeight = (lines.size() + 1) * 10;
        Gui.drawRect(left, top, right, top + boxHeight, alpha);
        GuiHelper.drawCenteredString(title, left + tempWidth / 2, top, -1);
        int y = top;
        for (String line : lines) {
            fontRenderer.drawStringWithShadow(line, left + 3, y += 10, -1);
        }
        y = top;
        if (scores != null) {
            for (String line : scores) {
                fontRenderer.drawStringWithShadow(line, right - 3 - fontRenderer.getStringWidth(line), y += 10, -1);
            }
        }
    }

    public static int getLongestStringWidth(Collection<String> lines) {
        FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
        int longestStringWidth = 0;
        for (String line : lines) {
            int currentStringWidth = fontRenderer.getStringWidth(line);
            if (currentStringWidth <= longestStringWidth) continue;
            longestStringWidth = currentStringWidth;
        }
        return longestStringWidth;
    }

    public static Optional<int[]> renderTooltip(int x, int y, int gradient1, int background, int alpha, boolean centerBox, boolean centerText, String ... tooltipData) {
        List<String> tooltipCollection = Arrays.asList(tooltipData);
        return GuiHelper.renderTooltip(x, y, tooltipCollection, gradient1, background, alpha, centerBox, centerText);
    }

    public static Optional<int[]> renderTooltip(int x, int y, Collection<String> tooltipData, int gradient1, int background, int alpha, boolean centerBox, boolean centerText) {
        if (tooltipData.isEmpty()) {
            return Optional.empty();
        }
        boolean lighting = GL11.glGetBoolean((int)2896);
        if (lighting) {
            RenderHelper.disableStandardItemLighting();
        }
        FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
        int longestStringWidth = GuiHelper.getLongestStringWidth(tooltipData);
        int left = x;
        int top = y;
        if (centerBox) {
            left -= longestStringWidth / 2;
        }
        int boxHeight = 8;
        if (tooltipData.size() > 1) {
            boxHeight += (tooltipData.size() - 1) * 10;
        }
        float z = 300.0f;
        GlStateManager.pushMatrix();
        GlStateManager.translate(0.0f, 0.0f, 1.0f);
        background = background & 0xFFFFFF | alpha << 24;
        Gui.drawRect(left - 3, top - 4, left + longestStringWidth + 3, top - 3, background);
        Gui.drawRect(left - 3, top + boxHeight + 3, left + longestStringWidth + 3, top + boxHeight + 4, background);
        Gui.drawRect(left - 3, top - 3, left + longestStringWidth + 3, top + boxHeight + 3, background);
        Gui.drawRect(left - 4, top - 3, left - 3, top + boxHeight + 3, background);
        Gui.drawRect(left + longestStringWidth + 3, top - 3, left + longestStringWidth + 4, top + boxHeight + 3, background);
        int gradient2 = (gradient1 & 0xFFFFFF) >> 1 | gradient1 & 0xFF000000;
        GuiHelper.drawGradientRect(left - 3, top - 3 + 1, z, left - 3 + 1, top + boxHeight + 3 - 1, gradient1, gradient2);
        GuiHelper.drawGradientRect(left + longestStringWidth + 2, top - 3 + 1, z, left + longestStringWidth + 3, top + boxHeight + 3 - 1, gradient1, gradient2);
        GuiHelper.drawGradientRect(left - 3, top - 3, z, left + longestStringWidth + 3, top - 3 + 1, gradient1, gradient1);
        GuiHelper.drawGradientRect(left - 3, top + boxHeight + 2, z, left + longestStringWidth + 3, top + boxHeight + 3, gradient2, gradient2);
        for (String line : tooltipData) {
            if (!centerText) {
                fontRenderer.drawStringWithShadow(line, left, top, -1);
            } else {
                GuiHelper.drawCenteredString(line, left + longestStringWidth / 2, top, -1);
            }
            top += 10;
        }
        GlStateManager.popMatrix();
        if (!lighting) {
            RenderHelper.disableStandardItemLighting();
        }
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        return Optional.of(new int[]{left - 4, top - 4, left + longestStringWidth + 4, top + boxHeight + 4});
    }

    public static void drawCenteredString(String text, float x, float y, int color) {
        GuiHelper.drawCenteredString(text, x, y, color, false);
    }

    public static void drawCenteredString(String text, float x, float y, int color, boolean dropShadow) {
        FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
        fontRenderer.drawString(text, x - (float)(fontRenderer.getStringWidth(text) / 2), y, color, dropShadow);
    }

    public static void drawCenteredSplitString(String text, float x, float y, int maxLength, int color) {
        GuiHelper.drawCenteredSplitString(text, x, y, maxLength, color, true);
    }

    public static void drawCenteredSplitString(String text, float x, float y, int maxLength, int color, boolean dropShadow) {
        FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
        int textWidth = fontRenderer.getStringWidth(text);
        int numLines = Math.max(1, (int)Math.ceil((float)textWidth / (float)maxLength));
        int lineWidth = text.length() / numLines;
        ArrayList<String> splitStrings = new ArrayList<String>(numLines);
        int lastIndex = 0;
        for (int i = 0; i < numLines - 1; ++i) {
            int spaceIndex = text.indexOf(" ", lastIndex + lineWidth);
            if (spaceIndex <= -1) continue;
            splitStrings.add(text.substring(lastIndex, spaceIndex + 1));
            lastIndex = spaceIndex + 1;
        }
        if (lastIndex < text.length()) {
            splitStrings.add(text.substring(lastIndex));
        }
        numLines = splitStrings.size();
        int currentY = (int)y - 6 * (numLines - 1);
        for (int i = 0; i < numLines; ++i) {
            GuiHelper.drawCenteredString((String)splitStrings.get(i), x, currentY, color, dropShadow);
            currentY += 12;
        }
    }

    public static void drawCenteredLimitedString(String text, float x, float y, int maxLength, int color) {
        GuiHelper.drawCenteredString(GuiHelper.getLimitedString(text, maxLength), x, y, color, true);
    }

    public static String getLimitedString(String text, int maxLength) {
        String ellipses = "...";
        if (maxLength <= 3) {
            return ellipses;
        }
        FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
        int textLength = text.length();
        String drawText = text;
        if (textLength >= 4) {
            int textWidth = 0;
            for (int i = 0; i < text.length(); ++i) {
                if ((textWidth += fontRenderer.getCharWidth(text.charAt(i))) <= maxLength) continue;
                drawText = text.substring(0, i - 3) + ellipses;
                break;
            }
        }
        return drawText;
    }

    public static List<String> splitString(String text, int maxLength) {
        ArrayList<String> splitString = new ArrayList<String>();
        if (text.isEmpty()) {
            splitString.add("");
        } else {
            FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
            int lineLength = 0;
            int lineStart = 0;
            int textLength = text.length();
            for (int i = 0; i < text.length(); ++i) {
                char currentChar = text.charAt(i);
                if (currentChar == '\n') {
                    splitString.add(text.substring(lineStart, i + 1));
                    lineStart = i + 1;
                    lineLength = 0;
                    continue;
                }
                int charWidth = fontRenderer.getCharWidth(currentChar);
                if ((lineLength += charWidth) <= maxLength) continue;
                splitString.add(text.substring(lineStart, i));
                lineStart = i;
                lineLength = charWidth;
            }
            if (lineStart < textLength || text.charAt(textLength - 1) == '\n') {
                splitString.add(text.substring(lineStart, textLength));
            }
        }
        return splitString;
    }

    public static String removeNewLine(String string) {
        int length = string.length();
        if (length > 0 && string.charAt(length - 1) == '\n') {
            return string.substring(0, length - 1);
        }
        return string;
    }

    public static void drawGradientRect(int left, int top, float zLevel, int right, int bottom, int startColour, int endColour) {
        GuiHelper.drawGradientRect(left, top, zLevel, right, bottom, startColour, endColour, false);
    }

    public static void drawGradientRect(int left, int top, float zLevel, int right, int bottom, int startColour, int endColour, boolean horizontal) {
        float startAlpha = (float)(startColour >> 24 & 0xFF) / 255.0f;
        float startRed = (float)(startColour >> 16 & 0xFF) / 255.0f;
        float startGreen = (float)(startColour >> 8 & 0xFF) / 255.0f;
        float startBlue = (float)(startColour & 0xFF) / 255.0f;
        float endAlpha = (float)(endColour >> 24 & 0xFF) / 255.0f;
        float endRed = (float)(endColour >> 16 & 0xFF) / 255.0f;
        float endGreen = (float)(endColour >> 8 & 0xFF) / 255.0f;
        float endBlue = (float)(endColour & 0xFF) / 255.0f;
        GuiHelper.drawGradientRect(left, top, zLevel, right, bottom, new Vector4f(startRed, startGreen, startBlue, startAlpha), new Vector4f(endRed, endGreen, endBlue, endAlpha), horizontal);
    }

    public static void drawGradientRect(int left, int top, float zLevel, int right, int bottom, Vector4f startColour, Vector4f endColour, boolean horizontal) {
        float startAlpha = startColour.getW();
        float startRed = startColour.getX();
        float startGreen = startColour.getY();
        float startBlue = startColour.getZ();
        float endAlpha = endColour.getW();
        float endRed = endColour.getX();
        float endGreen = endColour.getY();
        float endBlue = endColour.getZ();
        GlStateManager.disableTexture2D();
        GlStateManager.enableBlend();
        GlStateManager.disableAlpha();
        GlStateManager.blendFunc(770, 771);
        GlStateManager.shadeModel(7425);
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        buffer.begin(7, DefaultVertexFormats.POSITION_COLOR);
        buffer.pos(left, top, zLevel).color(startRed, startGreen, startBlue, startAlpha).endVertex();
        if (horizontal) {
            buffer.pos(left, bottom, zLevel).color(startRed, startGreen, startBlue, startAlpha).endVertex();
        } else {
            buffer.pos(left, bottom, zLevel).color(endRed, endGreen, endBlue, endAlpha).endVertex();
        }
        buffer.pos(right, bottom, zLevel).color(endRed, endGreen, endBlue, endAlpha).endVertex();
        if (horizontal) {
            buffer.pos(right, top, zLevel).color(endRed, endGreen, endBlue, endAlpha).endVertex();
        } else {
            buffer.pos(right, top, zLevel).color(startRed, startGreen, startBlue, startAlpha).endVertex();
        }
        tessellator.draw();
        GlStateManager.shadeModel(7424);
        GlStateManager.disableBlend();
        GlStateManager.enableAlpha();
        GlStateManager.enableTexture2D();
    }

    public static void drawEntity(EntityLivingBase ent, int x, int y, float scale, float rotationYaw, float rotationPitch) {
        GlStateManager.enableColorMaterial();
        GlStateManager.pushMatrix();
        GlStateManager.translate(x, y, 50.0f);
        GlStateManager.scale(-scale, scale, scale);
        GlStateManager.rotate(180.0f, 0.0f, 0.0f, 1.0f);
        float f2 = ent.renderYawOffset;
        float f3 = ent.rotationYaw;
        float f4 = ent.rotationPitch;
        float f5 = ent.prevRotationYawHead;
        float f6 = ent.rotationYawHead;
        GlStateManager.rotate(135.0f, 0.0f, 1.0f, 0.0f);
        RenderHelper.enableStandardItemLighting();
        GlStateManager.rotate(-135.0f, 0.0f, 1.0f, 0.0f);
        GlStateManager.rotate(-((float)Math.atan(rotationPitch / 40.0f)) * 20.0f, 1.0f, 0.0f, 0.0f);
        ent.renderYawOffset = (float)Math.atan(rotationYaw / 40.0f) * 20.0f;
        ent.rotationYaw = (float)Math.atan(rotationYaw / 40.0f) * 40.0f;
        ent.rotationPitch = -((float)Math.atan(rotationPitch / 40.0f)) * 20.0f;
        ent.rotationYawHead = ent.rotationYaw;
        ent.prevRotationYawHead = ent.rotationYaw;
        GlStateManager.translate(0.0f, (float)ent.getYOffset(), 0.0f);
        RenderManager renderManager = Minecraft.getMinecraft().getRenderManager();
        renderManager.playerViewY = 180.0f;
        renderManager.renderEntity(ent, 0.0, 0.0, 0.0, 0.0f, 1.0f, false);
        ent.renderYawOffset = f2;
        ent.rotationYaw = f3;
        ent.rotationPitch = f4;
        ent.prevRotationYawHead = f5;
        ent.rotationYawHead = f6;
        GlStateManager.popMatrix();
        RenderHelper.disableStandardItemLighting();
        GlStateManager.disableRescaleNormal();
        GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GlStateManager.disableTexture2D();
        GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit);
    }

    public static void drawPokemonInfoPC(int x, int y, PixelmonData p, float zLevel) {
        if (p == null) {
            return;
        }
        GuiHelper.drawGradientRect(x - 104, y - 2, zLevel, x + 2, y + 20, -13158600, -13158600);
        String displayName = p.getNickname();
        Minecraft minecraft = Minecraft.getMinecraft();
        minecraft.fontRenderer.drawString(displayName, x - 102, y, 0xFFFFFF);
        minecraft.renderEngine.bindTexture(GuiResources.pixelmonOverlay);
        if (!p.isEgg) {
            if (p.gender != Gender.None) {
                if (p.gender == Gender.Male) {
                    minecraft.renderEngine.bindTexture(GuiResources.male);
                } else if (p.gender == Gender.Female) {
                    minecraft.renderEngine.bindTexture(GuiResources.female);
                }
                GuiHelper.drawImageQuad(minecraft.fontRenderer.getStringWidth(displayName) + x - 101, y, 5.0, 8.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
            }
            String levelString = I18n.format("gui.screenpokechecker.lvl", new Object[0]) + " " + p.lvl;
            minecraft.fontRenderer.drawString(levelString, x + -101, y + minecraft.fontRenderer.FONT_HEIGHT + 1, 0xFFFFFF);
            String healthString = "";
            healthString = p.health <= 0 ? I18n.format("gui.creativeinv.fainted", new Object[0]) : I18n.format("nbt.hp.name", new Object[0]) + " " + p.health + "/" + p.hp;
            minecraft.fontRenderer.drawString(healthString, x - 97 + minecraft.fontRenderer.getStringWidth(levelString), y + minecraft.fontRenderer.FONT_HEIGHT + 1, 0xFFFFFF);
            int icons = 0;
            if (p.hasGmaxFactor) {
                minecraft.renderEngine.bindTexture(GuiResources.gmaxIcon);
                GuiHelper.drawImageQuad(minecraft.fontRenderer.getStringWidth(displayName) + x - 95, y - 1, 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                x += 9;
                ++icons;
            }
            if (p.isShiny) {
                minecraft.renderEngine.bindTexture(GuiResources.shiny);
                GuiHelper.drawImageQuad(minecraft.fontRenderer.getStringWidth(displayName) + x - 95, y - 1, 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
                x += 8;
                ++icons;
            }
            if (p.customTexture != null && !p.customTexture.isEmpty() || p.specialTexture != 0) {
                minecraft.renderEngine.bindTexture(GuiResources.st);
                GuiHelper.drawImageQuad(minecraft.fontRenderer.getStringWidth(displayName) + x - 95, y - 1, 10.0, 10.0f, 0.0, 0.0, 1.0, 1.0, 0.0f);
            }
        }
    }

    public static void drawPokemonInfoChooseMove(PixelmonData pokemon, int width, int height, float zLevel) {
        Minecraft mc = Minecraft.getMinecraft();
        GuiHelper.bindPokemonSprite(EnumSpecies.getFromName(pokemon.name).get().getNationalPokedexInteger(), pokemon.form, pokemon.gender, pokemon.isShiny, mc);
        GuiHelper.drawImageQuad(width / 2 - 114, height / 2 - 76, 64.0, 64.0f, 0.0, 0.0, 1.0, 1.0, zLevel);
        GuiHelper.drawCenteredString(pokemon.getNickname(), width / 2 - 82, height / 2 + 8, 0xFFFFFF);
        EnumType type1 = pokemon.getType1();
        EnumType type2 = pokemon.getType2();
        float x = type1.textureX;
        float y = type1.textureY;
        float x1 = 0.0f;
        float y1 = 0.0f;
        if (type2 != null) {
            x1 = type2.textureX;
            y1 = type2.textureY;
        }
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        mc.renderEngine.bindTexture(GuiResources.types);
        if (type2 != null && type2 != EnumType.Mystery) {
            GuiHelper.drawImageQuad(width / 2 - 83, height / 2 - 84, 21.0, 21.0f, x1 / 495.0f, y1 / 392.0f, (x1 + 98.0f) / 495.0f, (y1 + 98.0f) / 392.0f, zLevel);
            GuiHelper.drawImageQuad(width / 2 - 107, height / 2 - 84, 21.0, 21.0f, x / 495.0f, y / 392.0f, (x + 98.0f) / 495.0f, (y + 98.0f) / 392.0f, zLevel);
        } else {
            GuiHelper.drawImageQuad(width / 2 - 93, height / 2 - 84, 21.0, 21.0f, x / 495.0f, y / 392.0f, (x + 98.0f) / 495.0f, (y + 98.0f) / 392.0f, zLevel);
        }
        mc.fontRenderer.drawString(I18n.format("gui.screenpokechecker.lvl", new Object[0]) + " " + pokemon.lvl, width / 2 - 80, height / 2 - 94, 0xFFFFFF);
        mc.fontRenderer.drawString(I18n.format("gui.screenpokechecker.number", new Object[0]) + " " + pokemon.getNationalPokedexNumber(), width / 2 - 122, height / 2 - 94, 0xFFFFFF);
    }

    public static void closeScreen() {
        Keyboard.enableRepeatEvents((boolean)false);
        Minecraft.getMinecraft().player.closeScreen();
    }

    public static void switchFocus(int keyInt, List<GuiTextField> textFields) {
        GuiTextField[] textFieldArray = new GuiTextField[textFields.size()];
        GuiHelper.switchFocus(keyInt, textFields.toArray(textFieldArray));
    }

    public static void switchFocus(int keyInt, GuiTextField ... textFields) {
        if (keyInt == 15) {
            for (int i = 0; i < textFields.length; ++i) {
                GuiTextField textField = textFields[i];
                if (!textField.isFocused()) continue;
                textField.setFocused(false);
                textFields[(i + 1) % textFields.length].setFocused(true);
                return;
            }
            textFields[0].setFocused(true);
        }
    }

    public static void drawAttackInfoBox(float zLevel, int width, int height) {
        Minecraft mc = Minecraft.getMinecraft();
        mc.renderEngine.bindTexture(GuiResources.cwPanel);
        GlStateManager.color(0.5f, 0.5f, 0.5f);
        GuiHelper.drawImageQuad(width / 2 + 100, height / 2 - 90, 100.0, 140.0f, 0.0, 0.0, 1.0, 1.0, zLevel);
        GlStateManager.color(1.0f, 1.0f, 1.0f);
        String text = I18n.format("gui.choosemoveset.attackdetails", new Object[0]);
        mc.fontRenderer.drawString(text, width + 150 - mc.fontRenderer.getStringWidth(text) / 2, height - 92, 0);
    }

    public static void drawAttackInfoList(Attack attack, int width, int height) {
        FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
        int y = height / 2 - 85;
        int x = width / 2 + 105;
        String powerString = I18n.format("gui.choosemoveset.power", new Object[0]) + ": ";
        powerString = attack.getAttackBase().basePower > 0 ? powerString + attack.getAttackBase().basePower : powerString + "--";
        fontRenderer.drawString(powerString, x, y + 3, 0);
        String accuracyString = I18n.format("gui.battle.accuracy", new Object[0]) + ": ";
        accuracyString = attack.getAttackBase().accuracy > 0 ? accuracyString + attack.getAttackBase().accuracy : accuracyString + "--";
        fontRenderer.drawString(accuracyString, x, y + 13, 0);
        fontRenderer.drawString(I18n.format("nbt.pp.name", new Object[0]) + " " + attack.pp + "/" + attack.ppBase, x, y + 23, 0);
        String typeString = I18n.format("gui.battle.type", new Object[0]) + " ";
        fontRenderer.drawString(typeString, x, y + 33, 0);
        fontRenderer.drawString(attack.getAttackBase().attackType.toString(), x + fontRenderer.getStringWidth(typeString), y + 33, attack.getAttackBase().attackType.getColor());
        String category = attack.getAttackCategory().getLocalizedName();
        fontRenderer.drawString(category, x, y + 43, 0);
        fontRenderer.drawSplitString(attack.getAttackBase().getLocalizedDescription(), x, y + 58, 95, 0);
    }

    public static void drawAttackInfoMoveset(Attack attack, int y, int width, int height) {
        FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
        fontRenderer.drawString(I18n.format("gui.replaceattack.power", new Object[0]), width / 2 - 120, height / 2 + 60, 0xFFFFFF);
        fontRenderer.drawString(I18n.format("gui.replaceattack.accuracy", new Object[0]), width / 2 - 120, height / 2 + 70, 0xFFFFFF);
        int bpextra = 0;
        int acextra = 0;
        if (attack.getAttackBase().basePower >= 100) {
            bpextra = fontRenderer.getCharWidth('0');
        }
        if (attack.getAttackBase().accuracy >= 100) {
            acextra = fontRenderer.getCharWidth('0');
        }
        String powerString = attack.getAttackBase().basePower > 0 ? "" + attack.getAttackBase().basePower : "--";
        fontRenderer.drawString(powerString, width / 2 - 60 - bpextra, height / 2 + 60, 0xFFFFFF);
        String accuracyString = attack.getAttackBase().accuracy > 0 ? "" + attack.getAttackBase().accuracy : "--";
        fontRenderer.drawString(accuracyString, width / 2 - 60 - acextra, height / 2 + 70, 0xFFFFFF);
        fontRenderer.drawString(attack.getAttackCategory().getLocalizedName(), width / 2 - 120, height / 2 + 80, 0xFFFFFF);
        fontRenderer.drawSplitString(I18n.format("attack." + attack.getAttackBase().getUnlocalizedName().toLowerCase() + ".description", new Object[0]), width / 2 - 20, y, 135, 0xFFFFFF);
    }

    public static void drawMoveset(PixelmonData pokemon, int width, int height, float zLevel) {
        Minecraft mc = Minecraft.getMinecraft();
        for (int i = 0; i < pokemon.numMoves; ++i) {
            PixelmonMovesetData move = pokemon.moveset[i];
            mc.fontRenderer.drawString(move.getAttack().getAttackBase().getLocalizedName(), width / 2 + 11, height / 2 - 85 + 22 * i, 0xFFFFFF);
            mc.fontRenderer.drawString(move.pp + "/" + move.ppBase, width / 2 + 90, height / 2 - 83 + 22 * i, 0xFFFFFF);
            float x = move.type.textureX;
            float y = move.type.textureY;
            mc.renderEngine.bindTexture(GuiResources.types);
            GuiHelper.drawImageQuad(width / 2 - 23, height / 2 - 92 + 22 * i, 21.0, 21.0f, x / 495.0f, y / 392.0f, (x + 98.0f) / 495.0f, (y + 98.0f) / 392.0f, zLevel);
        }
    }

    public static void textboxKeyTypedLimited(GuiTextField textField, char key, int keyCode, int limit) {
        textField.textboxKeyTyped(key, keyCode);
        String newText = textField.getText();
        if (newText.length() > limit) {
            newText = newText.substring(0, limit);
            textField.setText(newText);
        }
    }

    public static int toColourValue(float red, float green, float blue, float alpha) {
        int r = (int)(red * 255.0f) & 0xFF;
        int g = (int)(green * 255.0f) & 0xFF;
        int b = (int)(blue * 255.0f) & 0xFF;
        int a = (int)(alpha * 255.0f) & 0xFF;
        return (a << 24) + (r << 16) + (b << 8) + g;
    }

    public static EntityPixelmon getEntity(int[] pokemonID) {
        Minecraft mc = Minecraft.getMinecraft();
        for (int i = 0; i < mc.world.loadedEntityList.size(); ++i) {
            EntityPixelmon pokemon;
            int[] existingID;
            Entity e = (Entity)mc.world.loadedEntityList.get(i);
            if (!(e instanceof EntityPixelmon) || (existingID = (pokemon = (EntityPixelmon)e).getPokemonId())[0] == -1 || !PixelmonMethods.isIDSame(pokemon, pokemonID)) continue;
            return pokemon;
        }
        return null;
    }

    public static void drawChatBox(GuiContainer container, String name, List<String> chatText, float zLevel) {
        container.mc.renderEngine.bindTexture(GuiResources.evo);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(container.width / 2 - 120, container.height / 4 - 40, 240.0, 80.0f, 0.0, 0.0, 1.0, 1.0, zLevel);
        container.mc.fontRenderer.drawString(name, container.width / 2 - 107, container.height / 4 - 32, 0xFFFFFF);
        for (int index = 0; index < chatText.size(); ++index) {
            String page;
            int li;
            while (container.mc.fontRenderer.getStringWidth(chatText.get(index)) > 200 && (li = (page = chatText.get(index)).lastIndexOf(" ")) >= 0) {
                if (index + 1 == chatText.size()) {
                    chatText.add(page.substring(li + 1));
                } else {
                    chatText.set(index + 1, page.substring(li + 1) + " " + chatText.get(index + 1));
                }
                chatText.set(index, page.substring(0, li));
            }
            String currentText = chatText.get(index);
            container.mc.fontRenderer.drawString(currentText, container.width / 2 - container.mc.fontRenderer.getStringWidth(currentText) / 2, container.height / 4 - 20 + index * 14, 0xFFFFFF);
        }
    }

    public static void drawBattleTimer(GuiContainer gui, int time) {
        time = Math.max(time, 0);
        String timerString = I18n.format("battlecontroller.afktimer", new Object[0]) + time;
        int color = time <= 5 ? 0xFF0000 : 0xFFFFFF;
        gui.drawString(gui.mc.fontRenderer, timerString, gui.width - gui.mc.fontRenderer.getStringWidth(timerString) - 5, 5, color);
    }

    public static void drawString(FontRenderer fontRendererIn, String text, int x, int y, float size, int color, boolean shadow) {
        GL11.glScalef((float)size, (float)size, (float)size);
        float mSize = (float)Math.pow(size, -1.0);
        fontRendererIn.drawString(text, Math.round((float)x / size), Math.round((float)y / size), color, shadow);
        GL11.glScalef((float)mSize, (float)mSize, (float)mSize);
    }
}

