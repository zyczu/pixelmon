/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui;

import com.pixelmongenerations.client.SoundHelper;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import java.io.IOException;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import org.lwjgl.opengl.GL11;

public abstract class GuiWarning
extends GuiContainer {
    private boolean overY = false;
    private boolean overN = false;
    protected GuiContainer previousScreen;

    protected GuiWarning(GuiContainer previousScreen) {
        super(new ContainerEmpty());
        this.previousScreen = previousScreen;
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.clear();
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int par3) throws IOException {
        super.mouseClicked(mouseX, mouseY, par3);
        if (this.overY) {
            this.confirmAction();
            this.pressButton();
        } else if (this.overN) {
            this.pressButton();
        }
    }

    protected abstract void confirmAction();

    public void pressButton() {
        SoundHelper.playButtonPressSound();
        this.closeScreen();
    }

    public void closeScreen() {
        this.mc.displayGuiScreen(this.previousScreen);
    }

    @Override
    public void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        GL11.glNormal3f((float)0.0f, (float)-1.0f, (float)0.0f);
        this.drawWarningText();
    }

    protected abstract void drawWarningText();

    @Override
    public void drawGuiContainerBackgroundLayer(float f, int mouseX, int mouseY) {
        int noColor;
        int yesColor;
        this.mc.renderEngine.bindTexture(GuiResources.yesNo);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(this.width / 2 - 128, this.height / 2 - 50, 256.0, 100.0f, 0.0, 0.0, 1.0, 0.78125, this.zLevel);
        this.overY = mouseX > this.width / 2 + 63 && mouseX < this.width / 2 + 108 && mouseY > this.height / 2 - 33 && mouseY < this.height / 2 - 7;
        this.overN = mouseX > this.width / 2 + 63 && mouseX < this.width / 2 + 108 && mouseY > this.height / 2 + 5 && mouseY < this.height / 2 + 31;
        boolean bl = this.overN;
        if (this.overY) {
            yesColor = 0xFFFFA0;
            GuiHelper.drawImageQuad(this.width / 2 + 63, this.height / 2 - 33, 45.0, 26.0f, 0.6015625, 0.7890625, 0.77734375, 0.9921875, this.zLevel);
        } else {
            yesColor = 0xFFFFFF;
        }
        if (this.overN) {
            noColor = 0xFFFFA0;
            GuiHelper.drawImageQuad(this.width / 2 + 63, this.height / 2 + 5, 45.0, 26.0f, 0.6015625, 0.7890625, 0.77734375, 0.9921875, this.zLevel);
        } else {
            noColor = 0xFFFFFF;
        }
        this.drawString(this.mc.fontRenderer, I18n.format("gui.yesno.yes", new Object[0]), this.width / 2 + 76, this.height / 2 - 23, yesColor);
        this.drawString(this.mc.fontRenderer, I18n.format("gui.yesno.no", new Object[0]), this.width / 2 + 80, this.height / 2 + 15, noColor);
    }

    protected void drawCenteredSplitText(String text) {
        GuiHelper.drawCenteredSplitString(text, 60.0f, 78.0f, 200, 0xFFFFFF);
    }
}

