/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.util.vector.Vector4f
 */
package com.pixelmongenerations.client.gui;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.packetHandlers.battles.BattleGuiClosed;
import com.pixelmongenerations.core.network.packetHandlers.itemDrops.ItemDropMode;
import com.pixelmongenerations.core.network.packetHandlers.itemDrops.ItemDropPacket;
import com.pixelmongenerations.core.network.packetHandlers.itemDrops.ServerItemDropPacket;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.storage.ClientData;
import com.pixelmongenerations.core.util.RegexPatterns;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.resources.I18n;
import org.lwjgl.util.vector.Vector4f;

public class GuiItemDrops
extends GuiContainer {
    String s;
    ItemDropPacket drops = ServerStorageDisplay.bossDrops;
    int mouseOverIndex = -1;
    boolean isFirst = true;

    public GuiItemDrops() {
        super(new ContainerEmpty());
        ServerStorageDisplay.bossDrops = null;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.add(new GuiButton(0, (this.width - 280) / 2 + 50, this.height - 91 + 100, I18n.format("gui.guiItemDrops.takeAll", new Object[0])));
        this.buttonList.add(new GuiButton(0, (this.width - 280) / 2 + 50, this.height - 91 + 100, I18n.format("gui.guiItemDrops.discard", new Object[0])));
    }

    @Override
    public void drawBackground(int par1) {
    }

    @Override
    public void drawDefaultBackground() {
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        Pixelmon.NETWORK.sendToServer(new BattleGuiClosed());
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        if (keyCode == 1) {
            Pixelmon.NETWORK.sendToServer(new ServerItemDropPacket(ServerItemDropPacket.PacketMode.TakeAllItems));
            this.closeScreen();
            return;
        }
    }

    @Override
    protected void mouseClicked(int i, int j, int par3) {
        int xPos = (this.width - 280) / 2 + 190;
        int yPos = (this.height - 182) / 2 + 150;
        int buttonWidth = 80;
        int buttonHeight = 20;
        if (i >= xPos && i <= xPos + buttonWidth && j >= yPos && j <= yPos + buttonHeight) {
            Pixelmon.NETWORK.sendToServer(new ServerItemDropPacket(ServerItemDropPacket.PacketMode.TakeAllItems));
            this.closeScreen();
            return;
        }
        xPos = (this.width - 280) / 2 + 10;
        yPos = (this.height - 182) / 2 + 150;
        if (i >= xPos && i <= xPos + buttonWidth && j >= yPos && j <= yPos + buttonHeight) {
            Pixelmon.NETWORK.sendToServer(new ServerItemDropPacket(ServerItemDropPacket.PacketMode.DropAllItems));
            this.closeScreen();
            return;
        }
        xPos = (this.width - 280) / 2 + 100;
        yPos = (this.height - 182) / 2 + 150;
        if (i >= xPos && i <= xPos + buttonWidth && j >= yPos && j <= yPos + buttonHeight) {
            Pixelmon.NETWORK.sendToServer(new ServerItemDropPacket(ServerItemDropPacket.PacketMode.DiscardAllItems));
            this.closeScreen();
            return;
        }
        if (this.mouseOverIndex != -1) {
            Pixelmon.NETWORK.sendToServer(new ServerItemDropPacket(this.drops.items[this.mouseOverIndex].id));
            this.drops.items[this.mouseOverIndex] = null;
            this.mouseOverIndex = -1;
            int count = 0;
            for (int ind = 0; ind < this.drops.items.length; ++ind) {
                if (this.drops.items[ind] == null) continue;
                ++count;
            }
            if (count == 0) {
                this.closeScreen();
                return;
            }
        }
    }

    private void closeScreen() {
        if (!ClientProxy.battleManager.evolveList.isEmpty()) {
            Minecraft mc = Minecraft.getMinecraft();
            mc.player.openGui(Pixelmon.INSTANCE, EnumGui.Evolution.getIndex(), mc.world, 0, 0, 0);
        } else if (ClientData.openMegaItemGui) {
            Minecraft mc = Minecraft.getMinecraft();
            mc.player.openGui(Pixelmon.INSTANCE, EnumGui.MegaItem.getIndex(), mc.world, 1, 0, 0);
        } else {
            GuiHelper.closeScreen();
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {
        int itemSpacingX = 40;
        int itemSpacingY = 30;
        int itemWidth = 24;
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.enableBlend();
        Vector4f colour = new Vector4f(0.1f, 0.1f, 0.1f, 0.6f);
        GuiHelper.drawGradientRect((this.width - 280) / 2, (this.height - 182) / 2, this.zLevel, (this.width - 280) / 2 + 280, (this.height - 182) / 2 + 182, colour, colour, false);
        colour = new Vector4f(0.1f, 0.2f, 0.23f, 0.6f);
        Vector4f colour2 = new Vector4f(0.1f, 0.14f, 0.2f, 0.6f);
        GuiHelper.drawGradientRect((this.width - 280) / 2, (this.height - 182) / 2, this.zLevel, (this.width - 280) / 2 + 280, (this.height - 182) / 2 + 30, colour, colour2, false);
        if (this.drops.hasCustomTitle) {
            this.s = this.drops.customTitle.getUnformattedText();
        } else if (this.drops.mode == ItemDropMode.Boss) {
            this.s = RegexPatterns.$_P_VAR.matcher(I18n.format("gui.guiItemDrops.beatBossPixelmon1", new Object[0])).replaceAll(Entity1Base.getLocalizedName(ClientProxy.battleManager.displayedEnemyPokemon[0].pokemonName));
        } else if (this.drops.mode == ItemDropMode.Totem) {
            this.s = RegexPatterns.$_P_VAR.matcher(I18n.format("gui.guiItemDrops.beatTotemPixelmon1", new Object[0])).replaceAll(Entity1Base.getLocalizedName(ClientProxy.battleManager.displayedEnemyPokemon[0].pokemonName));
        } else if (this.drops.mode == ItemDropMode.NormalTrainer) {
            this.s = I18n.format("gui.guiItemDrops.beatTrainer1", new Object[0]);
        }
        this.mc.fontRenderer.drawString(this.s, (this.width - 280) / 2 + 10, (this.height - 182) / 2 + 15, 0xFFFFFF);
        int xPos = (this.width - 280) / 2 + 190;
        int yPos = (this.height - 182) / 2 + 150;
        int buttonWidth = 80;
        int buttonHeight = 20;
        colour = i >= xPos && i <= xPos + buttonWidth && j >= yPos && j <= yPos + buttonWidth ? new Vector4f(1.0f, 1.0f, 1.0f, 1.4f) : new Vector4f(0.0f, 0.0f, 0.0f, 0.4f);
        GuiHelper.drawGradientRect(xPos, yPos, this.zLevel, xPos + buttonWidth, yPos + buttonHeight, colour, colour, false);
        this.s = I18n.format("gui.guiItemDrops.takeAll", new Object[0]);
        GuiHelper.drawCenteredString(this.s, xPos + buttonWidth / 2, yPos + 7, 0xFFFFFF);
        xPos = (this.width - 280) / 2 + 10;
        yPos = (this.height - 182) / 2 + 150;
        colour = i >= xPos && i <= xPos + buttonWidth && j >= yPos && j <= yPos + buttonWidth ? new Vector4f(1.0f, 1.0f, 1.0f, 1.4f) : new Vector4f(0.0f, 0.0f, 0.0f, 0.4f);
        GuiHelper.drawGradientRect(xPos, yPos, this.zLevel, xPos + buttonWidth, yPos + buttonHeight, colour, colour, false);
        this.s = I18n.format("gui.guiItemDrops.drop", new Object[0]);
        GuiHelper.drawCenteredString(this.s, xPos + buttonWidth / 2, yPos + 7, 0xFFFFFF);
        xPos = (this.width - 280) / 2 + 100;
        yPos = (this.height - 182) / 2 + 150;
        colour = i >= xPos && i <= xPos + buttonWidth && j >= yPos && j <= yPos + buttonWidth ? new Vector4f(1.0f, 1.0f, 1.0f, 1.4f) : new Vector4f(0.0f, 0.0f, 0.0f, 0.4f);
        GuiHelper.drawGradientRect(xPos, yPos, this.zLevel, xPos + buttonWidth, yPos + buttonHeight, colour, colour, false);
        this.s = I18n.format("Discard All", new Object[0]);
        GuiHelper.drawCenteredString(this.s, xPos + buttonWidth / 2, yPos + 7, 0xFFFFFF);
        int x = 0;
        int y = 0;
        colour = new Vector4f(0.1f, 0.1f, 0.1f, 0.3f);
        x = (this.width - 280) / 2 + 5;
        y = (this.height - 182) / 2 + 35;
        GuiHelper.drawGradientRect(x, y, this.zLevel, x + 165, y + 105, colour, colour, false);
        y = 0;
        x = 0;
        this.mouseOverIndex = -1;
        for (int d = 0; d < this.drops.items.length; ++d) {
            xPos = (this.width - 280) / 2 + 15 + x * itemSpacingX;
            yPos = (this.height - 182) / 2 + 45 + y * itemSpacingY;
            if (i >= xPos && i <= xPos + itemWidth && j >= yPos && j <= yPos + itemWidth) {
                if (this.drops.items[d] != null) {
                    colour = new Vector4f(1.0f, 1.0f, 1.0f, 1.4f);
                    this.mouseOverIndex = d;
                }
            } else {
                colour = new Vector4f(0.0f, 0.0f, 0.0f, 0.4f);
            }
            GuiHelper.drawGradientRect(xPos, yPos, this.zLevel, xPos + itemWidth, yPos + itemWidth, colour, colour, false);
            if (++x <= 3) continue;
            x = 0;
            ++y;
        }
        colour = new Vector4f(0.1f, 0.1f, 0.1f, 0.7f);
        int itemXPos = (this.width - 280) / 2 + 175;
        int itemYPos = (this.height - 182) / 2 + 35;
        GuiHelper.drawGradientRect(itemXPos, itemYPos, this.zLevel, itemXPos + 100, itemYPos + 105, colour, colour, false);
        if (this.mouseOverIndex != -1) {
            this.s = this.drops.items[this.mouseOverIndex].itemStack.getDisplayName();
            GuiHelper.drawCenteredString(this.s, (this.width - 280) / 2 + 225, itemYPos + 5, 0xFFFFFF);
            GuiHelper.drawCenteredString(I18n.format("gui.guiItemDrops.click", new Object[0]), (this.width - 280) / 2 + 225, itemYPos + 90, 0xFFFFFF);
            itemXPos = (this.width / 3 - 93) / 2 + 69;
            itemYPos = (this.height / 3 - 60) / 2 + 21;
            GlStateManager.scale(3.0f, 3.0f, 3.0f);
            RenderHelper.enableGUIStandardItemLighting();
            this.itemRender.renderItemAndEffectIntoGUI(this.drops.items[this.mouseOverIndex].itemStack, itemXPos, itemYPos);
            this.itemRender.renderItemOverlayIntoGUI(this.mc.fontRenderer, this.drops.items[this.mouseOverIndex].itemStack, itemXPos, itemYPos, null);
            GlStateManager.scale(0.33333334f, 0.33333334f, 0.33333334f);
            this.isFirst = false;
        } else if (this.isFirst) {
            this.s = I18n.format("gui.guiItemDrops.mouse", new Object[0]);
            this.mc.fontRenderer.drawString(this.s, (this.width - 280) / 2 + 228 - this.mc.fontRenderer.getStringWidth(this.s) / 2, (this.height - 182) / 2 + 100, 0x888888);
            this.s = I18n.format("gui.guiItemDrops.see", new Object[0]);
            this.mc.fontRenderer.drawString(this.s, (this.width - 280) / 2 + 228 - this.mc.fontRenderer.getStringWidth(this.s) / 2, (this.height - 182) / 2 + 114, 0x888888);
            this.s = I18n.format("gui.guiItemDrops.details", new Object[0]);
            this.mc.fontRenderer.drawString(this.s, (this.width - 280) / 2 + 228 - this.mc.fontRenderer.getStringWidth(this.s) / 2, (this.height - 182) / 2 + 128, 0x888888);
        }
        y = 0;
        x = 0;
        for (int d = 0; d < this.drops.items.length; ++d) {
            if (this.drops.items[d] != null) {
                xPos = (this.width - 280) / 2 + 15 + x * itemSpacingX;
                yPos = (this.height - 182) / 2 + 45 + y * itemSpacingY;
                RenderHelper.enableGUIStandardItemLighting();
                this.itemRender.renderItemAndEffectIntoGUI(this.drops.items[d].itemStack, xPos + 4, yPos + 3);
                this.itemRender.renderItemOverlayIntoGUI(this.mc.fontRenderer, this.drops.items[d].itemStack, xPos + 4, yPos + 3, null);
            }
            if (++x <= 3) continue;
            x = 0;
            ++y;
        }
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        this.drawDefaultBackground();
        this.drawGuiContainerBackgroundLayer(par3, par1, par2);
        GlStateManager.disableRescaleNormal();
        this.drawGuiContainerForegroundLayer(par1, par2);
        GlStateManager.enableLighting();
    }
}

