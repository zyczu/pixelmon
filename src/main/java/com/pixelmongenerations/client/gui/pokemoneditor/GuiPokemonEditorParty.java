/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 */
package com.pixelmongenerations.client.gui.pokemoneditor;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiPartyEditorBase;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiPokemonEditorIndividual;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.ChangePokemon;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.RandomizePokemon;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.RequestCloseEditedPlayer;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.UpdatePlayerParty;
import java.util.UUID;
import net.minecraft.client.Minecraft;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import org.lwjgl.input.Keyboard;

public class GuiPokemonEditorParty
extends GuiPartyEditorBase {
    public static UUID editedPlayerUUID;
    public static String editedPlayerName;

    public GuiPokemonEditorParty() {
        super(ServerStorageDisplay.editedPokemon);
        Keyboard.enableRepeatEvents((boolean)true);
    }

    @Override
    public String getTitle() {
        return I18n.translateToLocal("gui.pokemoneditor.title");
    }

    @Override
    protected void exitScreen() {
        if (!Minecraft.getMinecraft().player.getUniqueID().equals(editedPlayerUUID)) {
            Pixelmon.NETWORK.sendToServer(new RequestCloseEditedPlayer(editedPlayerUUID));
        }
        this.mc.player.closeScreen();
    }

    @Override
    protected void randomizeParty() {
        Pixelmon.NETWORK.sendToServer(new RandomizePokemon(editedPlayerUUID));
    }

    @Override
    protected void addPokemon(int partySlot) {
        Pixelmon.NETWORK.sendToServer(new ChangePokemon(partySlot, EnumSpecies.Magikarp));
    }

    @Override
    protected void editPokemon(int partySlot) {
        PixelmonData pokemon = (PixelmonData)this.pokemonList.get(partySlot);
        if (pokemon != null) {
            this.mc.displayGuiScreen(new GuiPokemonEditorIndividual(pokemon, this.getTitle()));
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {
        super.drawGuiContainerBackgroundLayer(f, i, j);
        this.pokemonList = ServerStorageDisplay.editedPokemon;
    }

    @Override
    protected IMessage getImportSavePacket() {
        return new UpdatePlayerParty(this.pokemonList);
    }
}

