/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pokemoneditor;

import com.pixelmongenerations.core.enums.EnumSpecies;

class FormData {
    EnumSpecies species;
    short form;

    FormData(EnumSpecies species, short form) {
        this.species = species;
        this.form = form;
    }
}

