/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pokemoneditor;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.elements.GuiTextFieldMultipleLine;
import com.pixelmongenerations.client.gui.pokemoneditor.IImportableContainer;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import java.io.IOException;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.util.text.translation.I18n;

public class GuiImportExport
extends GuiContainer {
    private static final int BUTTON_SAVE = 1;
    private static final int BUTTON_RESET = 2;
    private static final int BUTTON_CANCEL = 3;
    private String titleText;
    private IImportableContainer previousScreen;
    private GuiTextFieldMultipleLine importText;
    private int errorTextTimer;
    private String errorText = "";

    public GuiImportExport(IImportableContainer previousScreen, String titleText) {
        super(new ContainerEmpty());
        this.previousScreen = previousScreen;
        this.titleText = titleText;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.clear();
        this.buttonList.add(new GuiButton(1, this.width / 2 + 100, this.height / 2 + 90, 40, 20, I18n.translateToLocal("gui.pokemoneditor.save")));
        this.buttonList.add(new GuiButton(2, this.width / 2 + 150, this.height / 2 - 110, 40, 20, I18n.translateToLocal("gui.pokemoneditor.reset")));
        this.buttonList.add(new GuiButton(3, this.width / 2 + 150, this.height / 2 + 90, 40, 20, I18n.translateToLocal("gui.pokemoneditor.cancel")));
        this.importText = new GuiTextFieldMultipleLine(3, this.width / 2 - 80, this.height / 2 - 65, 160, 180);
        this.resetText();
    }

    private void resetText() {
        this.importText.setText(this.previousScreen.getExportText());
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float renderPartialTicks, int mouseX, int mouseY) {
        this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
        GuiHelper.drawImageQuad(this.width / 2 - 200, this.height / 2 - 120, 400.0, 240.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        this.mc.fontRenderer.drawString(this.titleText, this.width / 2 - this.mc.fontRenderer.getStringWidth(this.titleText) / 2, this.height / 2 - 90, 0);
        GuiHelper.drawCenteredString(I18n.translateToLocal("gui.pokemoneditor.importexport"), this.width / 2, this.height / 2 - 75, 0);
        this.importText.drawTextBox(mouseX, mouseY);
        if (this.errorTextTimer > 0) {
            --this.errorTextTimer;
            GuiHelper.drawCenteredString(I18n.translateToLocal("gui.pokemoneditor.errorimport"), this.width / 2 + 145, this.height / 2 + 65, 0xFF0000);
            GuiHelper.drawCenteredString(this.errorText, this.width / 2 + 145, this.height / 2 + 75, 0xFF0000);
        }
    }

    @Override
    protected void keyTyped(char key, int keyCode) throws IOException {
        if (keyCode == 1 || keyCode == 28 && !this.importText.isFocused()) {
            this.saveAndClose();
        }
        this.importText.textboxKeyTyped(key, keyCode);
    }

    @Override
    protected void mouseClicked(int x, int y, int z) throws IOException {
        super.mouseClicked(x, y, z);
        this.importText.mouseClicked(x, y, z);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if (button.enabled) {
            if (button.id == 1) {
                this.saveAndClose();
            } else if (button.id == 2) {
                this.resetText();
            } else if (button.id == 3) {
                this.closeScreen();
            }
        }
    }

    private void saveAndClose() {
        if (this.save()) {
            this.closeScreen();
        } else {
            this.errorTextTimer = 60;
        }
    }

    protected boolean save() {
        this.errorText = this.previousScreen.importText(this.importText.getText());
        return this.errorText == null;
    }

    private void closeScreen() {
        this.mc.displayGuiScreen(this.previousScreen.getScreen());
    }
}

