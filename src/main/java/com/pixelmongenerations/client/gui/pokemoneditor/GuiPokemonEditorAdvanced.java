/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.mojang.authlib.GameProfile
 */
package com.pixelmongenerations.client.gui.pokemoneditor;

import com.mojang.authlib.GameProfile;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.elements.GuiCheckBox;
import com.pixelmongenerations.client.gui.elements.GuiContainerDropDown;
import com.pixelmongenerations.client.gui.elements.GuiDropDown;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiIndividualEditorBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.EVsStore;
import com.pixelmongenerations.common.entity.pixelmon.stats.FriendShip;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.database.DatabaseStats;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.client.FMLClientHandler;

public class GuiPokemonEditorAdvanced
extends GuiContainerDropDown {
    private GuiIndividualEditorBase previousScreen;
    private PixelmonData data;
    private GuiTextField[] evText = new GuiTextField[6];
    private GuiTextField[] ivText = new GuiTextField[6];
    private GuiCheckBox[] ivCapToggle = new GuiCheckBox[6];
    private StatsType[] stats;
    private GuiTextField heldText;
    private ItemStack heldItem;
    private GuiTextField natureText;
    private GuiTextField friendshipText;
    private GuiTextField originalTrainerText;
    private GuiDropDown<String> abilityDropDown;
    private GuiTextField[] allText;
    private static final int BUTTON_OKAY = 1;
    private static final int BUTTON_MAX_IVS = 3;
    private static final int BUTTON_MIN_IVS = 4;
    private static final int BUTTON_RANDOM_IVS = 5;
    private static final int BUTTON_RESET_EVS = 6;
    private static final int BUTTON_CAP_ROOT = 7;
    private int xCenter;
    private int yCenter;
    private int xCenterOffset;

    public GuiPokemonEditorAdvanced() {
    }

    public GuiPokemonEditorAdvanced(GuiIndividualEditorBase previousScreen) {
        this.stats = new StatsType[]{StatsType.HP, StatsType.Attack, StatsType.Defence, StatsType.SpecialAttack, StatsType.SpecialDefence, StatsType.Speed};
        this.heldItem = ItemStack.EMPTY;
        this.previousScreen = previousScreen;
        this.data = this.previousScreen.p;
    }

    @Override
    public void initGui() {
        this.xCenter = this.width / 2;
        this.yCenter = this.height / 2;
        this.xCenterOffset = this.xCenter - 15;
        if (this.abilityDropDown != null) {
            this.checkFields();
        }
        super.initGui();
        this.buttonList.add(new GuiButton(1, this.xCenter + 155, this.yCenter + 90, 30, 20, I18n.translateToLocal("gui.guiItemDrops.ok")));
        Optional<BaseStats> stats = DatabaseStats.getBaseStats(this.data.name, this.data.form);
        String[] allAbilities = null;
        if (stats.isPresent()) {
            allAbilities = stats.get().abilities;
        }
        if (allAbilities == null) {
            allAbilities = this.data.getBaseStats().abilities;
        }
        List abilityList = Arrays.stream(allAbilities).filter(Objects::nonNull).collect(Collectors.toList());
        this.abilityDropDown = new GuiDropDown<String>(abilityList, this.data.ability, this.xCenter - 130, this.yCenter - 40, 80, 50).setGetOptionString(ability -> I18n.translateToLocal("ability." + ability + ".name"));
        this.addDropDown(this.abilityDropDown);
        this.buttonList.add(new GuiButton(3, this.xCenter + 125, this.yCenter - 50, 60, 20, I18n.translateToLocal("gui.trainereditor.maxivs")));
        this.buttonList.add(new GuiButton(4, this.xCenter + 125, this.yCenter - 25, 60, 20, I18n.translateToLocal("gui.trainereditor.minivs")));
        this.buttonList.add(new GuiButton(5, this.xCenter + 125, this.yCenter, 60, 20, I18n.translateToLocal("gui.trainereditor.randomivs")));
        this.buttonList.add(new GuiButton(6, this.xCenter + 125, this.yCenter + 35, 60, 20, I18n.translateToLocal("gui.trainereditor.resetevs")));
        this.allText = new GuiTextField[16];
        for (int i = 0; i < this.stats.length; ++i) {
            this.evText[i] = new GuiTextField(i, this.mc.fontRenderer, this.xCenterOffset + 45, this.yCenter - 55 + i * 25, 30, 20);
            this.evText[i].setText(String.valueOf(this.data.evs[i]));
            this.ivText[i] = new GuiTextField(i + this.evText.length, this.mc.fontRenderer, this.xCenterOffset + 85, this.yCenter - 55 + i * 25, 30, 20);
            this.ivText[i].setText(String.valueOf(this.data.ivs[i]));
            this.ivCapToggle[i] = new GuiCheckBox(7 + i, this.ivText[i].x + this.ivText[i].width + 5, this.ivText[i].y + 4, 14, 14, this.data.caps[i]);
            this.allText[i] = this.evText[i];
            this.allText[i + this.evText.length] = this.ivText[i];
        }
        this.heldText = new GuiTextField(13, this.mc.fontRenderer, this.xCenterOffset - 130, this.yCenter - 4, 80, 20);
        if (!this.data.heldItem.isEmpty()) {
            this.heldText.setText(this.data.heldItem.getDisplayName());
            this.updateHeldItem();
        }
        this.natureText = new GuiTextField(14, this.mc.fontRenderer, this.xCenterOffset - 130, this.yCenter + 34, 80, 20);
        this.natureText.setText(this.data.nature.getLocalizedName());
        this.friendshipText = new GuiTextField(14, this.mc.fontRenderer, this.xCenterOffset - 130, this.yCenter + 70, 80, 20);
        this.friendshipText.setText(String.valueOf(this.data.friendship));
        this.originalTrainerText = new GuiTextField(15, this.mc.fontRenderer, this.xCenterOffset - 87, this.yCenter + 97, 150, 20);
        this.originalTrainerText.setText(this.data.OT);
        this.allText[12] = this.heldText;
        this.allText[13] = this.natureText;
        this.allText[14] = this.friendshipText;
        this.allText[15] = this.originalTrainerText;
    }

    @Override
    protected void drawBackgroundUnderMenus(float renderPartialTicks, int mouseX, int mouseY) {
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
        GuiHelper.drawImageQuad(this.xCenter - 200, this.yCenter - 120, 400.0, 240.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        GuiHelper.drawCenteredString(this.previousScreen.titleText, this.xCenterOffset, this.yCenter - 90, 0, false);
        for (GuiTextField guiTextField : this.allText) {
            guiTextField.drawTextBox();
        }
        for (Gui gui : this.ivCapToggle) {
            ((GuiCheckBox)gui).drawButton(this.mc, mouseX, mouseY, partialTicks);
        }
        for (int i = 0; i < this.stats.length; ++i) {
            GuiHelper.drawStringRightAligned(this.stats[i].getLocalizedName(), this.xCenterOffset + 40, this.yCenter - 50 + i * 25, 0, false);
        }
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.evs"), this.xCenterOffset + 50, this.yCenter - 70, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.ivs"), this.xCenterOffset + 90, this.yCenter - 70, 0);
        GuiHelper.drawCenteredString(I18n.translateToLocal("gui.screenpokechecker.ability"), this.xCenterOffset - 90, this.abilityDropDown.getTop() - 10, 0, false);
        GuiHelper.drawCenteredString(I18n.translateToLocal("gui.trainereditor.helditem"), this.xCenterOffset - 90, this.heldText.y - 10, 0, false);
        GuiHelper.drawCenteredString(I18n.translateToLocal("gui.screenpokechecker.nature"), this.xCenterOffset - 90, this.natureText.y - 10, 0, false);
        GuiHelper.drawCenteredString(I18n.translateToLocal("gui.screenpokechecker.happiness"), this.xCenterOffset - 90, this.friendshipText.y - 10, 0, false);
        GuiHelper.drawStringRightAligned(I18n.translateToLocal("gui.screenpokechecker.originalTrainer"), this.originalTrainerText.x - 3, this.originalTrainerText.y + 6, 0, false);
        if (!this.heldItem.isEmpty()) {
            this.itemRender.renderItemAndEffectIntoGUI(this.heldItem, this.xCenterOffset - 150, this.heldText.y);
        }
        GuiHelper.bindPokemonSprite(this.data, this.mc);
        GlStateManager.color(1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(this.xCenterOffset - 157, this.yCenter - 73 - (this.data.isGen6Sprite() ? -3 : 0), 20.0, 20.0f, 0.0, 0.0, 1.0, 1.0, 1.0f);
    }

    @Override
    protected void keyTyped(char key, int keyCode) {
        for (GuiTextField textField : this.allText) {
            textField.textboxKeyTyped(key, keyCode);
        }
        GuiHelper.switchFocus(keyCode, this.allText);
        if (this.heldText.isFocused()) {
            this.updateHeldItem();
        }
        if (keyCode == 1 || keyCode == 28) {
            this.saveFields();
        }
    }

    @Override
    protected void mouseClickedUnderMenus(int mouseX, int mouseY, int clickedButton) {
        for (GuiTextField guiTextField : this.allText) {
            guiTextField.mouseClicked(mouseX, mouseY, clickedButton);
        }
        for (Gui gui : this.ivCapToggle) {
            if (!((GuiButton)gui).mousePressed(this.mc, mouseX, mouseY)) continue;
            ((GuiCheckBox)gui).setStateTriggered(!((GuiCheckBox)gui).isStateTriggered());
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        switch (button.id) {
            case 1: {
                this.saveFields();
                break;
            }
            case 3: {
                for (GuiTextField ev : this.ivText) {
                    ev.setText("31");
                }
                break;
            }
            case 4: {
                for (GuiTextField ev : this.ivText) {
                    ev.setText("0");
                }
                break;
            }
            case 5: {
                for (GuiTextField ev : this.ivText) {
                    ev.setText(String.valueOf(RandomHelper.getRandomNumberBetween(0, 31)));
                }
                break;
            }
            case 6: {
                for (GuiTextField ev : this.evText) {
                    ev.setText("0");
                }
                break;
            }
        }
    }

    private void saveFields() {
        if (this.checkFields()) {
            this.previousScreen.p = this.data;
            this.mc.displayGuiScreen(this.previousScreen);
        }
    }

    private void updateHeldItem() {
        Item newItem = PixelmonItems.getItemFromName(this.heldText.getText());
        this.heldItem = newItem instanceof ItemHeld ? new ItemStack(newItem) : ItemStack.EMPTY;
    }

    private boolean checkFields() {
        boolean isValid = true;
        String OT = "";
        String OTUUID = "";
        try {
            GameProfile profile = FMLClientHandler.instance().getServer().getPlayerProfileCache().getGameProfileForUsername(this.originalTrainerText.getText());
            if (profile != null) {
                OT = profile.getName();
                OTUUID = profile.getId().toString();
            }
        }
        catch (Exception profile) {
            // empty catch block
        }
        for (GuiTextField iv : this.ivText) {
            try {
                int ivNumber = Integer.parseInt(iv.getText());
                if (ivNumber < 0) {
                    isValid = false;
                    iv.setText("0");
                    continue;
                }
                if (ivNumber <= 31) continue;
                isValid = false;
                iv.setText("31");
            }
            catch (NumberFormatException var11) {
                isValid = false;
                iv.setText("0");
            }
        }
        int evTotal = 0;
        for (GuiTextField ev : this.evText) {
            int evNumber;
            try {
                evNumber = Integer.parseInt(ev.getText());
                if (evNumber < 0) {
                    isValid = false;
                    ev.setText("0");
                    evNumber = 0;
                } else if (evNumber > EVsStore.MAX_EVS) {
                    isValid = false;
                    ev.setText(EVsStore.MAX_EVS + "");
                    evNumber = EVsStore.MAX_EVS;
                }
            }
            catch (NumberFormatException var10) {
                isValid = false;
                ev.setText("0");
                evNumber = 0;
            }
            if ((evTotal += evNumber) <= EVsStore.MAX_TOTAL_EVS) continue;
            isValid = false;
            ev.setText(String.valueOf(evNumber - (evTotal - EVsStore.MAX_TOTAL_EVS)));
            evTotal = EVsStore.MAX_TOTAL_EVS;
        }
        EnumNature newNature = EnumNature.natureFromString(this.natureText.getText());
        if (newNature == null) {
            isValid = false;
            this.natureText.setText(this.data.nature.getLocalizedName());
        }
        this.updateHeldItem();
        if (this.heldItem.isEmpty() && !this.heldText.getText().equals("")) {
            this.heldText.setText("");
            isValid = false;
        }
        int newFriendship = this.data.friendship;
        try {
            newFriendship = Integer.parseInt(this.friendshipText.getText());
            int i = FriendShip.getMaxFriendship();
            if (newFriendship > i) {
                this.friendshipText.setText(String.valueOf(i));
                isValid = false;
            } else if (newFriendship < 0) {
                this.friendshipText.setText("0");
                isValid = false;
            }
        }
        catch (NumberFormatException i) {
            // empty catch block
        }
        if (isValid) {
            try {
                for (int i = 0; i < this.stats.length; ++i) {
                    this.data.evs[i] = Integer.parseInt(this.evText[i].getText());
                    this.data.ivs[i] = Integer.parseInt(this.ivText[i].getText());
                    this.data.caps[i] = this.ivCapToggle[i].isStateTriggered();
                }
                this.data.nature = newNature;
                this.data.pseudoNature = newNature;
                this.data.heldItem = this.heldItem;
                this.data.ability = this.abilityDropDown.getSelected();
                this.data.friendship = newFriendship;
                this.data.OT = OT;
                this.data.OTUUID = OTUUID;
            }
            catch (NumberFormatException var12) {
                isValid = false;
            }
        } else {
            System.out.println("Not Valid.");
        }
        return isValid;
    }
}

