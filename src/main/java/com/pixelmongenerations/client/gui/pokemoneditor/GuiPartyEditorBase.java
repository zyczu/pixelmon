/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pokemoneditor;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiImportExport;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiIndividualEditorBase;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiPokemonEditorParty;
import com.pixelmongenerations.client.gui.pokemoneditor.IImportableContainer;
import com.pixelmongenerations.client.gui.pokemoneditor.ImportExportConverter;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public abstract class GuiPartyEditorBase
extends GuiContainer
implements IImportableContainer {
    private static final int BUTTON_OKAY = 1;
    private static final int BUTTON_RANDOM = 10;
    private static final int BUTTON_IMPORT_EXPORT = 11;
    public List<PixelmonData> pokemonList;
    private List<GuiButton> pokemonButtons = new ArrayList<GuiButton>();

    protected GuiPartyEditorBase(List<PixelmonData> pokemonList) {
        super(new ContainerEmpty());
        this.pokemonList = pokemonList;
    }

    @Override
    public void initGui() {
        this.buttonList.add(new GuiButton(1, this.width / 2 + 155, this.height / 2 + 90, 30, 20, I18n.translateToLocal("gui.guiItemDrops.ok")));
        this.buttonList.add(new GuiButton(10, this.width / 2 - 160, this.height / 2 + 90, 100, 20, I18n.translateToLocal("gui.trainereditor.randomise")));
        this.buttonList.add(new GuiButton(11, this.width / 2 + 20, this.height / 2 + 90, 100, 20, I18n.translateToLocal("gui.pokemoneditor.importexport")));
        super.initGui();
        for (int i = 0; i < 6; ++i) {
            GuiButton pokeButton = new GuiButton(i + 2, this.width / 2 + 40, this.height / 2 - 50 + i * 20, 80, 20, I18n.translateToLocal("gui.trainereditor.edit"));
            this.buttonList.add(pokeButton);
            this.pokemonButtons.add(pokeButton);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {
        int k;
        for (int z = 0; z < 6; ++z) {
            GuiButton pokemonButton = this.pokemonButtons.get(z);
            pokemonButton.displayString = z < this.pokemonList.size() && this.pokemonList.get(z) != null ? I18n.translateToLocal("gui.trainereditor.edit") : I18n.translateToLocal("gui.trainereditor.add");
        }
        this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
        GuiHelper.drawImageQuad(this.width / 2 - 200, this.height / 2 - 120, 400.0, 240.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        RenderHelper.disableStandardItemLighting();
        String title = this.getTitle();
        this.mc.fontRenderer.drawString(title, this.width / 2 - this.mc.fontRenderer.getStringWidth(title) / 2, this.height / 2 - 90, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.name"), this.width / 2 - 130, this.height / 2 - 65, 0);
        this.mc.fontRenderer.drawString(I18n.translateToLocal("gui.trainereditor.lvl"), this.width / 2 - 40, this.height / 2 - 65, 0);
        for (k = 0; k < 6; ++k) {
            if (k % 2 != 1) continue;
            GuiPartyEditorBase.drawRect(this.width / 2 - 160, this.height / 2 - 50 + k * 20, this.width / 2 + 40, this.height / 2 - 30 + k * 20, -10526880);
        }
        for (k = 0; k < this.pokemonList.size(); ++k) {
            PixelmonData p = this.pokemonList.get(k);
            if (p == null) continue;
            this.mc.fontRenderer.drawString(p.getNickname(), this.width / 2 - 130, this.height / 2 - 44 + k * 20, 0);
            this.mc.fontRenderer.drawString("" + p.lvl, this.width / 2 - 40, this.height / 2 - 44 + k * 20, 0);
            GuiHelper.bindPokemonSprite(p, this.mc);
            GlStateManager.color(1.0f, 1.0f, 1.0f);
            GuiHelper.drawImageQuad(this.width / 2 - 157, this.height / 2 - 53 - (p.isGen6Sprite() ? -3 : 0) + k * 20, 20.0, 20.0f, 0.0, 0.0, 1.0, 1.0, 1.0f);
        }
    }

    public abstract String getTitle();

    @Override
    protected void keyTyped(char key, int par2) throws IOException {
        if (par2 == 1 || par2 == 28) {
            this.exitScreen();
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if (button.enabled) {
            if (button.id == 1) {
                this.exitScreen();
            } else if (button.id == 10) {
                this.randomizeParty();
            } else if (button.id >= 2 && button.id < 8) {
                int partySlot = button.id - 2;
                if (this.pokemonButtons.get((int)partySlot).displayString.equalsIgnoreCase(I18n.translateToLocal("gui.trainereditor.add"))) {
                    this.addPokemon(partySlot);
                } else {
                    this.editPokemon(partySlot);
                }
            } else if (button.id == 11) {
                this.mc.displayGuiScreen(new GuiImportExport(this, this.getTitle()));
            }
        }
    }

    protected abstract void exitScreen();

    protected abstract void randomizeParty();

    protected abstract void addPokemon(int var1);

    protected abstract void editPokemon(int var1);

    public static void editPokemonPacket(int partySlot) {
        GuiIndividualEditorBase individualEditor;
        PixelmonData currentPokemon;
        Minecraft mc = Minecraft.getMinecraft();
        if (mc.currentScreen instanceof GuiPartyEditorBase) {
            GuiPartyEditorBase partyEditor = (GuiPartyEditorBase)mc.currentScreen;
            partyEditor.editPokemon(partySlot);
        } else if (mc.currentScreen instanceof GuiIndividualEditorBase && (currentPokemon = (individualEditor = (GuiIndividualEditorBase)mc.currentScreen).getPokemonList().get(partySlot)) != null) {
            individualEditor.p = currentPokemon;
            individualEditor.initGui();
        }
    }

    @Override
    public String getExportText() {
        StringBuilder exportText = new StringBuilder();
        for (int i = 0; i < this.pokemonList.size(); ++i) {
            PixelmonData data = this.pokemonList.get(i);
            if (data == null) continue;
            if (i > 0) {
                exportText.append("\n");
            }
            exportText.append(ImportExportConverter.getExportText(data));
        }
        return exportText.toString();
    }

    @Override
    public String importText(String importText) {
        String[] pokemonSplit = (importText = importText.trim()).split("\n\n");
        if (pokemonSplit.length < 1 || pokemonSplit.length > 6) {
            return "";
        }
        PixelmonData[] importData = new PixelmonData[6];
        for (int i = 0; i < importData.length; ++i) {
            if (i >= pokemonSplit.length) continue;
            importData[i] = new PixelmonData();
            String errorText = ImportExportConverter.importText(pokemonSplit[i], importData[i]);
            if (errorText == null) continue;
            return i + 1 + ", " + errorText;
        }
        try {
            this.pokemonList.clear();
        }
        catch (UnsupportedOperationException i) {
            // empty catch block
        }
        for (int i = 0; i < importData.length; ++i) {
            PixelmonData oldPokemon = null;
            int pokemonListSize = this.pokemonList.size();
            if (pokemonListSize > i) {
                oldPokemon = this.pokemonList.get(i);
            }
            if (importData[i] != null) {
                int[] arrn;
                PixelmonData pixelmonData = importData[i];
                if (oldPokemon == null) {
                    int[] arrn2 = new int[2];
                    arrn2[0] = -1;
                    arrn = arrn2;
                    arrn2[1] = -1;
                } else {
                    arrn = oldPokemon.pokemonID;
                }
                pixelmonData.pokemonID = arrn;
                importData[i].order = i;
                importData[i].OT = GuiPokemonEditorParty.editedPlayerName;
                if (importData[i].OT == null) {
                    importData[i].OT = "";
                }
            }
            if (pokemonListSize > i) {
                this.pokemonList.set(i, importData[i]);
                continue;
            }
            this.pokemonList.add(importData[i]);
        }
        Pixelmon.NETWORK.sendToServer(this.getImportSavePacket());
        return null;
    }

    protected abstract IMessage getImportSavePacket();

    @Override
    public GuiScreen getScreen() {
        return this;
    }
}

