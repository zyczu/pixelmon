/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.rules;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.battles.rules.GuiBattleRulesBase;
import com.pixelmongenerations.client.gui.battles.rules.GuiTeamSelect;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.io.IOException;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.translation.I18n;

public class GuiBattleRulesFixed
extends GuiBattleRulesBase {
    private GuiButton okayButton;

    public GuiBattleRulesFixed() {
        this.editingEnabled = false;
        this.rules = ClientProxy.battleManager.rules;
    }

    @Override
    public void initGui() {
        super.initGui();
        int buttonWidth = 30;
        this.okayButton = new GuiButton(0, this.centerX - buttonWidth / 2, this.centerY + 60, buttonWidth, 20, I18n.translateToLocal("gui.guiItemDrops.ok"));
        this.buttonList.add(this.okayButton);
    }

    @Override
    protected void keyTyped(char key, int keyCode) {
    }

    @Override
    protected void drawBackgroundUnderMenus(float partialTicks, int mouseX, int mouseY) {
        super.drawBackgroundUnderMenus(partialTicks, mouseX, mouseY);
        this.dimScreen();
        this.highlightButtons(40, 67);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if (button == this.okayButton) {
            if (GuiTeamSelect.teamSelectPacket.isAllDisabled()) {
                this.mc.player.sendMessage(new TextComponentTranslation("gui.battlerules.cancelselectyou", new Object[0]));
                GuiHelper.closeScreen();
            } else {
                this.mc.displayGuiScreen(new GuiTeamSelect());
            }
        }
    }
}

