/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens.choosePokemon;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.choosePokemon.ChoosePokemon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.packetHandlers.npc.NPCLearnMove;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.util.Collections;
import net.minecraft.client.Minecraft;

public class ChooseLearnMove
extends ChoosePokemon {
    public ChooseLearnMove(GuiBattle parent, BattleMode mode) {
        super(parent, mode);
    }

    @Override
    public void click(int width, int height, int mouseX, int mouseY) {
        if (mouseX > width / 2 + 63 && mouseX < width / 2 + 63 + 48 && mouseY > height - 27 && mouseY < height - 27 + 17) {
            this.closeScreen();
            return;
        }
        int slot = this.choosePokemonSlot(width, height, mouseX, mouseY);
        if (slot != -1) {
            ClientProxy.battleManager.choosingPokemon = false;
            EnumNPCType npcType = null;
            if (this.mode == BattleMode.ChooseRelearnMove) {
                npcType = EnumNPCType.Relearner;
            } else if (this.mode == BattleMode.ChooseTutor) {
                npcType = EnumNPCType.Tutor;
            } else {
                this.closeScreen();
            }
            if (npcType != null) {
                Pixelmon.NETWORK.sendToServer(new NPCLearnMove(slot, npcType));
            }
        }
    }

    private void closeScreen() {
        ClientProxy.battleManager.choosingPokemon = false;
        Minecraft.getMinecraft().player.closeScreen();
    }

    @Override
    protected void fillParty() {
        this.inBattle.clear();
        this.inParty.clear();
        Collections.addAll(this.inParty, ServerStorageDisplay.getPokemon());
    }
}

