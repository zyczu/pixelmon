/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens.yesNo;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.yesNo.YesNoDialogue;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.packetHandlers.battles.Flee;
import net.minecraft.util.text.translation.I18n;

public class YesNoForfeit
extends YesNoDialogue {
    public YesNoForfeit(GuiBattle parent) {
        super(parent, BattleMode.YesNoForfeit);
    }

    @Override
    protected void drawConfirmText(int width, int height) {
        GuiHelper.drawCenteredSplitString(I18n.translateToLocal("battlecontroller.forfeitask"), width / 2 - 30, height / 2 - 5, 70, 0xFFFFFF);
    }

    @Override
    protected void confirm() {
        Pixelmon.NETWORK.sendToServer(new Flee(this.bm.getUserPokemonPacket().pokemonID));
        this.bm.mode = BattleMode.Waiting;
    }
}

