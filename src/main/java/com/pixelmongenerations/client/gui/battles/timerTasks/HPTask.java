/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.timerTasks;

import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.client.gui.battles.timerTasks.BattleTask;
import com.pixelmongenerations.common.battle.attacks.IBattleMessage;
import com.pixelmongenerations.core.proxy.ClientProxy;

public abstract class HPTask
extends BattleTask {
    float healthDifference;
    float originalHealth;
    float currentHealth;
    float interval;
    int[] id;
    PixelmonInGui pokemon;

    public HPTask(IBattleMessage message, float healthDifference, int[] id) {
        super(message);
        this.healthDifference = healthDifference;
        this.id = id;
        this.pokemon = ClientProxy.battleManager.getPokemon(id);
        if (this.pokemon != null) {
            this.currentHealth = this.originalHealth = this.pokemon.health;
            this.boundsCheck();
        }
        this.interval = this.healthDifference / 100.0f;
    }

    protected abstract void boundsCheck();

    @Override
    public void run() {
        if (this.pokemon == null) {
            this.cancel();
            return;
        }
        this.currentHealth += this.interval;
        if (this.isDone()) {
            this.pokemon.health = Math.max(0.0f, this.originalHealth + this.healthDifference);
            this.cancel();
            return;
        }
        this.pokemon.health = this.currentHealth;
    }

    protected abstract boolean isDone();
}

