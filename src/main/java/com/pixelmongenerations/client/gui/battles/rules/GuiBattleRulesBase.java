/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 */
package com.pixelmongenerations.client.gui.battles.rules;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.rules.GuiClauseList;
import com.pixelmongenerations.client.gui.elements.EnumTextAlign;
import com.pixelmongenerations.client.gui.elements.GuiButtonOnOff;
import com.pixelmongenerations.client.gui.elements.GuiButtonToggle;
import com.pixelmongenerations.client.gui.elements.GuiContainerDropDown;
import com.pixelmongenerations.client.gui.elements.GuiDropDown;
import com.pixelmongenerations.client.gui.elements.GuiTextDescriptive;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiImportExport;
import com.pixelmongenerations.client.gui.pokemoneditor.IImportableContainer;
import com.pixelmongenerations.common.battle.rules.BattleRules;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClauseRegistry;
import com.pixelmongenerations.common.battle.rules.clauses.tiers.EnumTier;
import com.pixelmongenerations.common.battle.rules.clauses.tiers.Tier;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.input.Keyboard;

public abstract class GuiBattleRulesBase
extends GuiContainerDropDown
implements IImportableContainer {
    protected BattleRules rules = new BattleRules();
    private String titleText;
    protected List<GuiTextField> textFields = new ArrayList<GuiTextField>();
    private GuiTextField levelCapField;
    private GuiTextField numPokemonField;
    private GuiTextField turnTimeField;
    private GuiTextField teamSelectTimeField;
    protected boolean editingEnabled = true;
    protected GuiButton importExportButton;
    protected GuiButtonToggle raiseToCapButton;
    protected GuiButton battleTypeButton;
    protected GuiButtonToggle fullHealButton;
    protected GuiButtonToggle teamPreviewButton;
    protected GuiButton[] editButtons;
    protected List<GuiTextDescriptive> ruleLabels;
    protected List<BattleClause> selectedClauses = new ArrayList<BattleClause>();
    private GuiClauseList allClauseList;
    private GuiClauseList selectedClauseList;
    protected int centerX;
    protected int centerY;
    protected int rectBottom;
    protected int yChange;
    protected int clauseListHeight = 70;
    protected GuiDropDown<Tier> tierMenu;

    public GuiBattleRulesBase() {
        Keyboard.enableRepeatEvents((boolean)true);
        this.mc = Minecraft.getMinecraft();
        this.titleText = I18n.translateToLocal("gui.battlerules.title");
    }

    @Override
    public void initGui() {
        if (!this.textFields.isEmpty()) {
            this.registerRules();
        }
        this.textFields.clear();
        super.initGui();
        this.centerX = this.width / 2;
        this.centerY = this.height / 2;
        this.rectBottom = this.centerY - 120 + this.getBackgroundHeight();
        int textID = 0;
        int textHeight = 20;
        int leftFieldX = this.centerX - 130;
        int centerFieldX = this.centerX - 20;
        int rightFieldX = this.centerX + 120;
        int topY = this.centerY - 76 + this.yChange;
        int yOffset = 25;
        int centerY = topY + yOffset;
        int bottomY = topY + yOffset * 2;
        int numberFieldWidth = 30;
        this.levelCapField = this.createRuleField(textID++, leftFieldX, topY, numberFieldWidth, textHeight);
        this.numPokemonField = this.createRuleField(textID++, centerFieldX, centerY, numberFieldWidth, textHeight);
        this.turnTimeField = this.createRuleField(textID++, rightFieldX, topY, numberFieldWidth, textHeight);
        this.teamSelectTimeField = this.createRuleField(textID++, rightFieldX, centerY, numberFieldWidth, textHeight);
        this.textFields.addAll(Arrays.asList(this.levelCapField, this.numPokemonField, this.turnTimeField, this.teamSelectTimeField));
        int buttonID = 0;
        int onOffButtonWidth = 30;
        int buttonHeight = 20;
        int leftButton = leftFieldX - 1;
        this.importExportButton = new GuiButton(buttonID++, this.centerX + 90, this.centerY - 120, 100, buttonHeight, I18n.translateToLocal("gui.pokemoneditor.importexport"));
        this.raiseToCapButton = new GuiButtonOnOff(buttonID, leftButton, centerY, onOffButtonWidth, buttonHeight, this.rules.raiseToCap);
        this.fullHealButton = new GuiButtonOnOff(buttonID, leftButton, bottomY, onOffButtonWidth, buttonHeight, this.rules.fullHeal);
        this.teamPreviewButton = new GuiButtonOnOff(buttonID, rightFieldX - 1, bottomY, onOffButtonWidth, buttonHeight, this.rules.teamPreview);
        this.battleTypeButton = new GuiButton(buttonID++, centerFieldX - 1, topY, 40, buttonHeight, this.rules.battleType.getLocalizedName());
        this.editButtons = new GuiButton[]{this.importExportButton, this.battleTypeButton, this.raiseToCapButton, this.fullHealButton, this.teamPreviewButton};
        this.buttonList.addAll(Arrays.asList(this.editButtons));
        this.ruleLabels = new ArrayList<GuiTextDescriptive>();
        int textOffset = 5;
        int leftTextX = leftFieldX - textOffset;
        int centerTextX = centerFieldX - textOffset;
        int rightTextX = rightFieldX - textOffset;
        int topTextY = topY + textOffset;
        int centerTextY = topTextY + yOffset;
        int bottomTextY = topTextY + yOffset * 2;
        int tierTextX = this.centerX - 62;
        this.ruleLabels.add(this.createRuleLabel("levelcap", leftTextX, topTextY, EnumTextAlign.Right));
        this.ruleLabels.add(this.createRuleLabel("raisetocap", leftTextX, centerTextY, EnumTextAlign.Right));
        this.ruleLabels.add(this.createRuleLabel("fullheal", leftTextX, bottomTextY, EnumTextAlign.Right));
        this.ruleLabels.add(new GuiTextDescriptive(I18n.translateToLocal("gui.trainereditor.battletype"), I18n.translateToLocal("gui.battlerules.description.battletype"), centerTextX, topTextY, EnumTextAlign.Right));
        this.ruleLabels.add(this.createRuleLabel("numpokemon", centerTextX, centerTextY, EnumTextAlign.Right));
        this.ruleLabels.add(this.createRuleLabel("tier", tierTextX, bottomTextY, EnumTextAlign.Right));
        this.ruleLabels.add(this.createRuleLabel("turntime", rightTextX, topTextY, EnumTextAlign.Right));
        this.ruleLabels.add(this.createRuleLabel("teamselecttime", rightTextX, centerTextY, EnumTextAlign.Right));
        this.ruleLabels.add(this.createRuleLabel("teampreview", rightTextX, bottomTextY, EnumTextAlign.Right));
        int clauseListWidth = 120;
        int clauseListY = topTextY + yOffset * 2 + 30;
        int clauseListX = centerTextX - clauseListWidth / 2 + 10;
        this.allClauseList = new GuiClauseList(this, BattleClauseRegistry.getClauseRegistry().getClauseList(), clauseListY, clauseListX, clauseListWidth, this.clauseListHeight);
        this.selectedClauseList = new GuiClauseList(this, this.selectedClauses, clauseListY, clauseListX + clauseListWidth + 10, clauseListWidth, this.clauseListHeight);
        BattleClauseRegistry<Tier> tierRegistry = BattleClauseRegistry.getTierRegistry();
        ArrayList<Tier> tiers = new ArrayList<Tier>();
        for (EnumTier tier : EnumTier.values()) {
            tiers.add(tierRegistry.getClause(tier.getTierID()));
        }
        List<Tier> customTiers = tierRegistry.getCustomClauses();
        Collections.sort(customTiers);
        tiers.addAll(customTiers);
        this.tierMenu = new GuiDropDown<Tier>(tiers, this.rules.tier, tierTextX + 10, bottomTextY, 70, 100);
        this.addDropDown(this.tierMenu);
        this.setRules(this.rules);
    }

    private GuiTextField createRuleField(int componentID, int x, int y, int width, int height) {
        return new GuiTextField(componentID, this.mc.fontRenderer, x, y, width, height);
    }

    private GuiTextDescriptive createRuleLabel(String langKey, int x, int y, EnumTextAlign align) {
        return new GuiTextDescriptive(I18n.translateToLocal("gui.battlerules." + langKey), I18n.translateToLocal("gui.battlerules.description." + langKey), x, y, align);
    }

    public void setRules(BattleRules rules) {
        this.rules = rules;
        this.setText(this.levelCapField, this.rules.levelCap);
        this.setText(this.numPokemonField, this.rules.numPokemon);
        this.setBlankableText(this.turnTimeField, this.rules.turnTime);
        this.setBlankableText(this.teamSelectTimeField, this.rules.teamSelectTime);
        this.raiseToCapButton.setOn(this.rules.raiseToCap);
        this.fullHealButton.setOn(this.rules.fullHeal);
        this.teamPreviewButton.setOn(this.rules.teamPreview);
        this.battleTypeButton.displayString = this.rules.battleType.getLocalizedName();
        this.selectedClauses.clear();
        this.selectedClauses.addAll(this.rules.getClauseList());
        this.tierMenu.setSelected(this.rules.tier);
    }

    private void setText(GuiTextField field, int number) {
        field.setText(Integer.toString(number));
    }

    private void setBlankableText(GuiTextField field, int number) {
        String fieldText = "";
        if (number > 0) {
            fieldText = Integer.toString(number);
        }
        field.setText(fieldText);
    }

    @Override
    protected void drawBackgroundUnderMenus(float partialTicks, int mouseX, int mouseY) {
        GuiClauseList[] renderLists;
        GuiClauseList[] arrguiClauseList;
        this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
        int textColor = 0;
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(this.centerX - 200, this.centerY - 120, 400.0, this.getBackgroundHeight(), 0.0, 0.0, 1.0, 1.0, this.zLevel);
        for (GuiTextField textField : this.textFields) {
            textField.drawTextBox();
        }
        for (GuiButton button : this.editButtons) {
            button.enabled = this.editingEnabled;
        }
        this.importExportButton.visible = this.editingEnabled;
        GuiHelper.drawCenteredString(this.titleText, this.centerX, this.centerY - 90 + this.yChange, textColor);
        if (this.editingEnabled) {
            GuiHelper.drawCenteredString(I18n.translateToLocal("gui.battlerules.clauses"), this.allClauseList.getCenterX(), this.centerY + this.yChange, textColor);
        }
        GuiHelper.drawCenteredString(I18n.translateToLocal("gui.battlerules.selectedclauses"), this.selectedClauseList.getCenterX(), this.centerY + this.yChange, textColor);
        String description = "";
        for (GuiTextDescriptive text : this.ruleLabels) {
            text.draw();
            if (!text.isHovering(mouseX, mouseY)) continue;
            description = text.getDescription();
        }
        if (this.editingEnabled) {
            GuiClauseList[] arrguiClauseList2 = new GuiClauseList[2];
            arrguiClauseList2[0] = this.allClauseList;
            arrguiClauseList = arrguiClauseList2;
            arrguiClauseList2[1] = this.selectedClauseList;
        } else {
            GuiClauseList[] arrguiClauseList3 = new GuiClauseList[1];
            arrguiClauseList = arrguiClauseList3;
            arrguiClauseList3[0] = this.selectedClauseList;
        }
        for (GuiClauseList list : renderLists = arrguiClauseList) {
            int hoverIndex;
            list.drawScreen(mouseX, mouseY, partialTicks);
            if (!description.isEmpty() || (hoverIndex = list.getMouseOverIndex(mouseX, mouseY)) <= -1) continue;
            description = list.getElement(hoverIndex).getDescription();
        }
        if (!description.isEmpty()) {
            int descX = this.centerX - 190;
            int descY = this.centerY + 10 + this.yChange;
            int textWidth = 100;
            int boxOffset = 5;
            this.mc.renderEngine.bindTexture(GuiResources.cwPanel);
            GlStateManager.color(0.65f, 0.65f, 0.65f);
            GuiHelper.drawImageQuad(descX - boxOffset, descY - boxOffset, textWidth + boxOffset * 2, 70.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            this.mc.fontRenderer.drawSplitString(description, descX, descY, textWidth, textColor);
        }
    }

    protected int getBackgroundHeight() {
        return 240;
    }

    @Override
    protected void keyTyped(char key, int keyCode) {
        if (this.editingEnabled) {
            for (GuiTextField textField : this.textFields) {
                textField.textboxKeyTyped(key, keyCode);
            }
            GuiHelper.switchFocus(keyCode, this.textFields);
        }
    }

    @Override
    protected void mouseClickedUnderMenus(int x, int y, int mouseButton) throws IOException {
        if (this.editingEnabled) {
            for (GuiTextField textField : this.textFields) {
                textField.mouseClicked(x, y, mouseButton);
            }
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        if (button == this.importExportButton) {
            this.registerRules();
            this.mc.displayGuiScreen(new GuiImportExport(this, this.titleText));
        } else if (button == this.battleTypeButton) {
            this.rules.battleType = this.rules.battleType.next();
            this.battleTypeButton.displayString = this.rules.battleType.getLocalizedName();
        } else if (button instanceof GuiButtonToggle) {
            ((GuiButtonToggle)button).toggle();
        }
    }

    protected void registerRules() {
        try {
            this.rules.levelCap = Integer.parseInt(this.levelCapField.getText());
        }
        catch (NumberFormatException numberFormatException) {
            // empty catch block
        }
        this.rules.raiseToCap = this.raiseToCapButton.isOn();
        try {
            this.rules.numPokemon = Integer.parseInt(this.numPokemonField.getText());
        }
        catch (NumberFormatException numberFormatException) {
            // empty catch block
        }
        try {
            this.rules.turnTime = this.parseBlankableText(this.turnTimeField.getText());
        }
        catch (NumberFormatException numberFormatException) {
            // empty catch block
        }
        try {
            this.rules.teamSelectTime = this.parseBlankableText(this.teamSelectTimeField.getText());
        }
        catch (NumberFormatException numberFormatException) {
            // empty catch block
        }
        this.rules.fullHeal = this.fullHealButton.isOn();
        this.rules.teamPreview = this.teamPreviewButton.isOn();
        this.rules.setNewClauses(this.selectedClauses);
        this.rules.tier = this.tierMenu.getSelected();
        this.rules.validateRules();
        this.setRules(this.rules);
    }

    private int parseBlankableText(String text) {
        if (text.trim().isEmpty()) {
            return 0;
        }
        return Integer.parseInt(text);
    }

    @Override
    public String getExportText() {
        return this.rules.exportText();
    }

    @Override
    public String importText(String importText) {
        String result = this.rules.importText(importText);
        this.setRules(this.rules);
        return result;
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        Keyboard.enableRepeatEvents((boolean)false);
    }

    void clauseListSelected(List<BattleClause> clauses, int index) {
        if (!this.editingEnabled) {
            return;
        }
        BattleClause clause = clauses.get(index);
        if (this.selectedClauses.contains(clause)) {
            this.selectedClauses.remove(clause);
        } else {
            this.selectedClauses.add(clause);
            Collections.sort(this.selectedClauses);
        }
    }

    boolean isClauseSelected(List<BattleClause> clauses, int index) {
        if (clauses == this.selectedClauses) {
            return false;
        }
        return this.selectedClauses.contains(clauses.get(index));
    }

    protected void dimScreen() {
        Gui.drawRect(this.centerX - 200, this.centerY - 120, this.centerX + 200, this.rectBottom, 0x5F000000);
    }

    protected void highlightButtons(int highlightOffsetX, int bottomOffset) {
        Gui.drawRect(this.centerX - highlightOffsetX, this.rectBottom - bottomOffset, this.centerX + highlightOffsetX, this.rectBottom - bottomOffset + 35, -1);
    }

    @Override
    public GuiScreen getScreen() {
        return this;
    }

    @Override
    protected boolean disableMenus() {
        return !this.editingEnabled;
    }
}

