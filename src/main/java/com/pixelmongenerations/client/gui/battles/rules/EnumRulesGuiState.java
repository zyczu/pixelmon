/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.rules;

public enum EnumRulesGuiState {
    Propose,
    Accept,
    WaitPropose,
    WaitAccept,
    WaitChange;


    boolean isWaiting() {
        return this == WaitPropose || this == WaitAccept || this == WaitChange;
    }

    public static EnumRulesGuiState getFromOrdinal(int ordinal) {
        EnumRulesGuiState[] values = EnumRulesGuiState.values();
        if (ordinal >= 0 && ordinal < values.length) {
            return values[ordinal];
        }
        return WaitChange;
    }
}

