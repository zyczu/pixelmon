/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.BattleScreen;
import com.pixelmongenerations.client.gui.battles.battleScreens.MainMenu;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.battle.status.Intimidated;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.item.heldItems.ItemAdrenalineOrb;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.battle.BagSection;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.packetHandlers.battles.BagPacket;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;

public class UseBag
extends BattleScreen {
    public UseBag(GuiBattle parent) {
        super(parent, BattleMode.UseBag);
    }

    @Override
    public void drawScreen(int width, int height, int mouseX, int mouseY) {
        Item item;
        int i;
        this.mc.renderEngine.bindTexture(GuiResources.itemGui2);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(width / 2 - 128, height / 2 - 102, 256.0, 205.0f, 0.0, 0.0, 1.0, 0.80078125, this.zLevel);
        if (mouseX > width / 2 + 63 && mouseX < width / 2 + 111 && mouseY > height / 2 - 91 && mouseY < height / 2 - 74) {
            GuiHelper.drawImageQuad(width / 2 + 63, height / 2 - 91, 48.0, 17.0f, 0.7734375, 0.9140625, 0.9609375, 0.98046875, this.zLevel);
        }
        this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.battle.back"), width / 2 + 76, height / 2 - 85, 0xFFFFFF);
        this.drawCenteredString(this.mc.fontRenderer, this.bm.bagSection.displayString, width / 2 - 40, height / 2 - 80, 0xFFFFFF);
        for (i = this.bm.startIndex; i < 6 + this.bm.startIndex; ++i) {
            if (i >= this.bm.bagStore.size()) continue;
            this.mc.renderEngine.bindTexture(GuiResources.itemGui2);
            if (mouseX > width / 2 - 98 && mouseX < width / 2 - 98 + 187 && mouseY > height / 2 - 44 + (i - this.bm.startIndex) * 21 && mouseY < height / 2 - 24 + (i - this.bm.startIndex) * 21) {
                GuiHelper.drawImageQuad(width / 2 - 98, height / 2 - 44 + (i - this.bm.startIndex) * 21, 187.0, 20.0f, 0.01171875, 0.8046875, 0.7578125, 0.87890625, this.zLevel);
            } else {
                GuiHelper.drawImageQuad(width / 2 - 98, height / 2 - 44 + (i - this.bm.startIndex) * 21, 187.0, 20.0f, 0.01171875, 0.88671875, 0.7578125, 0.9609375, this.zLevel);
            }
            item = Item.getItemById(this.bm.bagStore.get((int)i).id);
            this.drawString(this.mc.fontRenderer, item.getItemStackDisplayName(null), width / 2 - 55, height / 2 - 38 + (i - this.bm.startIndex) * 21, 0xFFFFFF);
            this.drawString(this.mc.fontRenderer, "x" + this.bm.bagStore.get((int)i).count, width / 2 + 55, height / 2 - 38 + (i - this.bm.startIndex) * 21, 0xFFFFFF);
        }
        this.mc.renderEngine.bindTexture(GuiResources.itemGui2);
        if (this.bm.startIndex > 0) {
            if (mouseX > width / 2 - 11 && mouseX < width / 2 + 6 && mouseY > height / 2 - 55 && mouseY < height / 2 - 45) {
                GuiHelper.drawImageQuad(width / 2 - 11, height / 2 - 55, 17.0, 10.0f, 0.82421875, 0.859375, 0.890625, 0.8984375, this.zLevel);
            }
        } else {
            GuiHelper.drawImageQuad(width / 2 - 11, height / 2 - 55, 17.0, 10.0f, 0.0390625, 0.0390625, 0.10546875, 0.078125, this.zLevel);
        }
        if (this.bm.startIndex + 6 < this.bm.bagStore.size()) {
            if (mouseX > width / 2 - 11 && mouseX < width / 2 + 6 && mouseY > height / 2 + 82 && mouseY < height / 2 + 92) {
                GuiHelper.drawImageQuad(width / 2 - 11, height / 2 + 82, 17.0, 10.0f, 0.921875, 0.859375, 0.98828125, 0.8984375, this.zLevel);
            }
        } else {
            GuiHelper.drawImageQuad(width / 2 - 11, height / 2 + 82, 17.0, 10.0f, 0.0390625, 0.0390625, 0.10546875, 0.078125, this.zLevel);
        }
        for (i = this.bm.startIndex; i < 6 + this.bm.startIndex; ++i) {
            if (i >= this.bm.bagStore.size()) continue;
            item = Item.getItemById(this.bm.bagStore.get((int)i).id);
            this.itemRender.renderItemIntoGUI(new ItemStack(item), width / 2 - 85, height / 2 - 42 + (i - this.bm.startIndex) * 21);
        }
    }

    @Override
    public void click(int width, int height, int mouseX, int mouseY) {
        if (mouseX > width / 2 - 11 && mouseX < width / 2 + 6 && mouseY > height / 2 - 55 && mouseY < height / 2 - 45 && this.bm.startIndex > 0) {
            --this.bm.startIndex;
            return;
        }
        if (mouseX > width / 2 - 11 && mouseX < width / 2 + 6 && mouseY > height / 2 + 82 && mouseY < height / 2 + 92 && this.bm.startIndex + 6 < this.bm.bagStore.size()) {
            ++this.bm.startIndex;
            return;
        }
        if (mouseX > width / 2 + 63 && mouseX < width / 2 + 111 && mouseY > height / 2 - 91 && mouseY < height / 2 - 74) {
            this.bm.mode = BattleMode.ChooseBag;
            return;
        }
        for (int i = this.bm.startIndex; i < 6 + this.bm.startIndex; ++i) {
            if (i >= this.bm.bagStore.size() || mouseX <= width / 2 - 98 || mouseX >= width / 2 - 98 + 187 || mouseY <= height / 2 - 44 + (i - this.bm.startIndex) * 21 || mouseY >= height / 2 - 24 + (i - this.bm.startIndex) * 21) continue;
            this.bm.itemToUse = this.bm.bagStore.get(i);
            Item item = Item.getItemById(this.bm.bagStore.get((int)i).id);
            if (item != null && item == PixelmonItems.guardSpec) {
                this.bm.selectedActions.clear();
                Pixelmon.NETWORK.sendToServer(new BagPacket(this.bm.getUserPokemonPacket().pokemonID, this.bm.bagStore.get((int)i).id, this.bm.battleControllerIndex, 0));
                this.bm.mode = BattleMode.Waiting;
                this.bm.selectedMove();
                continue;
            }
            if (this.bm.bagSection == BagSection.Pokeballs) {
                this.bm.selectedActions.clear();
                Pixelmon.NETWORK.sendToServer(new BagPacket(this.bm.getUserPokemonPacket().pokemonID, this.bm.bagStore.get((int)i).id, this.bm.battleControllerIndex, 0));
                MainMenu.lastId = this.bm.bagStore.get((int)i).id;
                this.bm.mode = BattleMode.Waiting;
                continue;
            }
            if (item instanceof ItemAdrenalineOrb) {
                BattleControllerBase bcb;
                this.bm.selectedActions.clear();
                if (this.bm.canCatchOpponent() && !(bcb = BattleRegistry.getBattle(this.bm.getViewPlayer())).isHordeBattle()) {
                    WildPixelmonParticipant wpp = bcb.participants.get(0) instanceof WildPixelmonParticipant ? (WildPixelmonParticipant)bcb.participants.get(0) : (WildPixelmonParticipant)bcb.participants.get(1);
                    PixelmonWrapper target = wpp.getPokemon();
                    if (!target.hasStatus(StatusType.Intimidated)) {
                        Intimidated.becomeIntimidated(target);
                    }
                }
                Pixelmon.NETWORK.sendToServer(new BagPacket(this.bm.getUserPokemonPacket().pokemonID, this.bm.bagStore.get((int)i).id, this.bm.battleControllerIndex, 0));
                MainMenu.lastId = this.bm.bagStore.get((int)i).id;
                this.bm.mode = BattleMode.Waiting;
                continue;
            }
            this.bm.mode = BattleMode.ApplyToPokemon;
        }
    }
}

