/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens;

import com.pixelmongenerations.client.gui.battles.ClientBattleManager;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.proxy.ClientProxy;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public abstract class BattleScreen
extends GuiScreen {
    protected GuiBattle parent;
    protected ClientBattleManager bm;
    protected BattleMode mode;

    public abstract void drawScreen(int var1, int var2, int var3, int var4);

    public abstract void click(int var1, int var2, int var3, int var4);

    public BattleScreen(GuiBattle parent, BattleMode mode) {
        this.mc = Minecraft.getMinecraft();
        this.itemRender = this.mc.getRenderItem();
        this.fontRenderer = this.mc.fontRenderer;
        this.mode = mode;
        this.parent = parent;
        this.bm = ClientProxy.battleManager;
    }

    public boolean isScreen() {
        return this.bm.mode == this.mode;
    }
}

