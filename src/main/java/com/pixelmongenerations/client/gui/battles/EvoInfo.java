/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles;

public class EvoInfo {
    public int[] pokemonID;
    public String evolveInto;

    public EvoInfo(int[] pokemonID, String evolveInto) {
        this.pokemonID = pokemonID;
        this.evolveInto = evolveInto;
    }
}

