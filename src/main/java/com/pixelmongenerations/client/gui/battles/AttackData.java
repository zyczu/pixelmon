/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles;

import com.pixelmongenerations.common.battle.attacks.Attack;

public class AttackData {
    public int[] pokemonID;
    public Attack attack;
    public int level;
    public boolean checkEvo;

    public AttackData(int[] pokemonID, Attack attack, int level, boolean checkEvo) {
        this.pokemonID = pokemonID;
        this.attack = attack;
        this.level = level;
        this.checkEvo = checkEvo;
    }

    public boolean equals(Object compare) {
        if (compare instanceof AttackData) {
            AttackData compareData = (AttackData)compare;
            for (int i = 0; i < this.pokemonID.length; ++i) {
                if (compareData.pokemonID[i] == this.pokemonID[i]) continue;
                return false;
            }
            return compareData.attack.equals(this.attack);
        }
        return false;
    }
}

