/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui.battles;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.camera.GuiChattableCamera;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.ClientBattleManager;
import com.pixelmongenerations.client.gui.battles.battleScreens.BattleScreen;
import com.pixelmongenerations.client.gui.battles.battleScreens.ChooseAttackScreen;
import com.pixelmongenerations.client.gui.battles.battleScreens.ChooseBag;
import com.pixelmongenerations.client.gui.battles.battleScreens.ChooseTargets;
import com.pixelmongenerations.client.gui.battles.battleScreens.Dynamax;
import com.pixelmongenerations.client.gui.battles.battleScreens.LevelUpScreen;
import com.pixelmongenerations.client.gui.battles.battleScreens.MainMenu;
import com.pixelmongenerations.client.gui.battles.battleScreens.MegaEvolution;
import com.pixelmongenerations.client.gui.battles.battleScreens.UltraBurst;
import com.pixelmongenerations.client.gui.battles.battleScreens.UseBag;
import com.pixelmongenerations.client.gui.battles.battleScreens.chooseMove.ChooseEther;
import com.pixelmongenerations.client.gui.battles.battleScreens.chooseMove.ChoosePPUp;
import com.pixelmongenerations.client.gui.battles.battleScreens.chooseMove.ReplaceAttack;
import com.pixelmongenerations.client.gui.battles.battleScreens.choosePokemon.ApplyToPokemon;
import com.pixelmongenerations.client.gui.battles.battleScreens.choosePokemon.ChooseLearnMove;
import com.pixelmongenerations.client.gui.battles.battleScreens.choosePokemon.ChoosePokemon;
import com.pixelmongenerations.client.gui.battles.battleScreens.choosePokemon.EnforcedSwitch;
import com.pixelmongenerations.client.gui.battles.battleScreens.yesNo.YesNoForfeit;
import com.pixelmongenerations.client.gui.battles.battleScreens.yesNo.YesNoReplaceMove;
import com.pixelmongenerations.client.gui.battles.pokemonOverlays.Overlay1v1;
import com.pixelmongenerations.client.gui.battles.pokemonOverlays.Overlay1vMany;
import com.pixelmongenerations.client.gui.battles.pokemonOverlays.Overlay2v2;
import com.pixelmongenerations.client.gui.battles.pokemonOverlays.OverlayBase;
import com.pixelmongenerations.client.gui.elements.GuiChatExtension;
import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.TargetingInfo;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.GlobalStatusController;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import com.pixelmongenerations.core.network.packetHandlers.battles.BattleGuiClosed;
import com.pixelmongenerations.core.network.packetHandlers.battles.RemoveSpectator;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.storage.ClientData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class GuiBattle
extends GuiChattableCamera {
    private int guiWidth = 300;
    private int guiHeight = 60;
    public ClientBattleManager bm;
    public ArrayList<BattleScreen> screenList = new ArrayList();
    BattleScreen currentScreen = null;
    public OverlayBase pokemonOverlay = null;
    private GuiButton stopSpectateButton = null;
    private Queue<String> messageLog = new LinkedList<String>();
    private static final int NUM_MESSAGES = 4;
    private boolean isLevelScreenDrawn;
    boolean first = true;
    int flashCount = 0;
    public int mouseOverButton = 0;

    public GuiBattle() {
        EntityPlayerSP player = Minecraft.getMinecraft().player;
        player.setJumping(false);
        player.setSneaking(false);
        player.moveStrafing = 0.0f;
        player.moveForward = 0.0f;
        GuiPixelmonOverlay.isVisible = false;
        this.bm = ClientProxy.battleManager;
        this.screenList.add(new ApplyToPokemon(this));
        this.screenList.add(new ChooseAttackScreen(this));
        this.screenList.add(new ChooseBag(this));
        this.screenList.add(new ChooseEther(this));
        this.screenList.add(new ChooseLearnMove(this, BattleMode.ChooseRelearnMove));
        this.screenList.add(new ChooseLearnMove(this, BattleMode.ChooseTutor));
        this.screenList.add(new ChoosePokemon(this));
        this.screenList.add(new ChooseTargets(this));
        this.screenList.add(new EnforcedSwitch(this));
        this.screenList.add(new LevelUpScreen(this));
        this.screenList.add(new MainMenu(this));
        this.screenList.add(new MegaEvolution(this));
        this.screenList.add(new Dynamax(this));
        this.screenList.add(new UltraBurst(this));
        this.screenList.add(new ReplaceAttack(this));
        this.screenList.add(new UseBag(this));
        this.screenList.add(new YesNoForfeit(this));
        this.screenList.add(new YesNoReplaceMove(this));
        this.screenList.add(new ChoosePPUp(this));
        try {
            if (this.bm.teamPokemon != null && this.bm.displayedEnemyPokemon != null) {
                if (this.bm.rules.battleType == EnumBattleType.SOS || this.bm.battleType == EnumBattleType.SOS) {
                    this.pokemonOverlay = new Overlay1vMany(this);
                } else if (this.bm.teamPokemon.length == 1 && this.bm.displayedEnemyPokemon.length == 1) {
                    this.pokemonOverlay = new Overlay1v1(this);
                } else if (this.bm.teamPokemon.length == 2 && this.bm.displayedEnemyPokemon.length == 2) {
                    this.pokemonOverlay = new Overlay2v2(this);
                } else if (this.bm.teamPokemon.length < 2 && this.bm.displayedEnemyPokemon.length >= 2) {
                    this.pokemonOverlay = new Overlay1vMany(this);
                }
            }
        }
        catch (NullPointerException er) {
            this.pokemonOverlay = new Overlay1vMany(this);
        }
    }

    @Override
    public void initGui() {
        super.initGui();
        if (this.bm.isSpectating) {
            this.addStopSpectateButton();
            this.messageLog.clear();
        }
    }

    private void addStopSpectateButton() {
        this.stopSpectateButton = new GuiButton(0, this.width / 2 - 50, 0, 100, 20, I18n.translateToLocal("gui.spectate.stopspectate"));
        this.buttonList.add(this.stopSpectateButton);
    }

    public void restoreSettingsAndClose() {
        this.bm.restoreSettingsAndClose();
        this.mc.player.closeScreen();
        GuiPixelmonOverlay.isVisible = true;
        if (ServerStorageDisplay.bossDrops != null) {
            Minecraft.getMinecraft().player.openGui(Pixelmon.INSTANCE, EnumGui.ItemDrops.getIndex(), Minecraft.getMinecraft().world, 0, 0, 0);
        } else if (!this.bm.evolveList.isEmpty()) {
            Minecraft.getMinecraft().player.openGui(Pixelmon.INSTANCE, EnumGui.Evolution.getIndex(), Minecraft.getMinecraft().world, 0, 0, 0);
        } else if (ClientData.openMegaItemGui) {
            Minecraft.getMinecraft().player.openGui(Pixelmon.INSTANCE, EnumGui.MegaItem.getIndex(), Minecraft.getMinecraft().world, 1, 0, 0);
        } else {
            Pixelmon.NETWORK.sendToServer(new BattleGuiClosed());
        }
    }

    private void selectScreen() {
        for (BattleScreen bs : this.screenList) {
            if (!bs.isScreen()) continue;
            this.currentScreen = bs;
            break;
        }
    }

    public void selectScreenImmediate(BattleMode mode) {
        this.bm.mode = mode;
        this.selectScreen();
    }

    private void drawPokemonOverlays() {
        if (this.pokemonOverlay != null) {
            this.pokemonOverlay.draw(this.width, this.height, this.guiWidth, this.guiHeight);
        }
    }

    private void drawMessageScreen() {
        this.mc.renderEngine.bindTexture(GuiResources.battleGui3);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        int guiWidth = this.getGuiWidth();
        GuiHelper.drawImageQuad(this.width / 2 - guiWidth / 2, this.height - this.guiHeight, guiWidth, this.guiHeight, 0.0, 0.0, 1.0, 0.30416667461395264, this.zLevel);
        if (this.bm.isSpectating) {
            if (this.bm.hasMoreMessages()) {
                String message = this.bm.getNextMessage();
                this.bm.removeMessage();
                this.addToMessageLog(message);
            }
            if (this.messageLog.isEmpty()) {
                this.drawWaiting();
            } else {
                int i = 4 - this.messageLog.size();
                int messageHeight = (this.guiHeight - 6) / 4;
                for (String message : this.messageLog) {
                    int color = 0xFFFFFF;
                    if (i == 3) {
                        color = 0xFFFF00;
                    }
                    GuiHelper.drawCenteredLimitedString(message, this.width / 2, this.height - (i + 1) * messageHeight, 280, color);
                    ++i;
                }
            }
        } else if (this.bm.hasMoreMessages()) {
            String message = this.bm.getNextMessage();
            GuiHelper.drawCenteredSplitString(message, this.width / 2, this.height - 35, 280, 0xFFFFFF);
            ++this.flashCount;
            if (this.flashCount > 30) {
                int continueTime;
                this.mc.renderEngine.bindTexture(GuiResources.battleGui3);
                GuiHelper.drawImageQuad(this.width / 2 + 130, this.height - 15, 10.0, 6.0f, 0.9546874761581421, 0.31041666865348816, 0.98125f, 0.33125f, this.zLevel);
                int n = continueTime = this.bm.afkActive ? 30 : 480;
                if (this.flashCount > continueTime) {
                    this.continueMessages();
                }
            }
        } else {
            this.drawWaiting();
        }
    }

    private void drawWaiting() {
        ++this.flashCount;
        if (this.flashCount >= 160) {
            this.flashCount = 0;
        }
        String ellipses = this.flashCount < 80 ? "." : (this.flashCount < 120 ? ".." : "...");
        this.drawCenteredString(this.mc.fontRenderer, I18n.translateToLocal("gui.guiDoctor.waiting") + ellipses, this.width / 2, this.height - 35, 0xFFFFFF);
        this.bm.waitingText = true;
    }

    private void addToMessageLog(String message) {
        this.messageLog.add(message);
        if (this.messageLog.size() > 4) {
            this.messageLog.remove();
        }
    }

    public static void drawHealthBar(int x, int y, int width, int height, float health, int maxHealth) {
        float b;
        float g;
        float r;
        GlStateManager.enableRescaleNormal();
        GlStateManager.enableColorMaterial();
        GlStateManager.pushMatrix();
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        GlStateManager.disableTexture2D();
        buffer.begin(7, DefaultVertexFormats.POSITION_COLOR);
        int barWidth = width - 6;
        buffer.pos(x, y, 0.0).color(0.4f, 0.4f, 0.4f, 1.0f).endVertex();
        buffer.pos(x, y + height, 0.0).color(0.4f, 0.4f, 0.4f, 1.0f).endVertex();
        buffer.pos(x + barWidth, y + height, 0.0).color(0.4f, 0.4f, 0.4f, 1.0f).endVertex();
        buffer.pos(x + barWidth, y, 0.0).color(0.4f, 0.4f, 0.4f, 1.0f).endVertex();
        float Percent = health / (float)maxHealth;
        float CurWidth = Percent * (float)barWidth;
        if (health <= (float)(maxHealth / 5)) {
            r = 0.8f;
            g = 0.0f;
            b = 0.0f;
        } else if (health <= (float)(maxHealth / 2)) {
            r = 1.0f;
            g = 1.0f;
            b = 0.4f;
        } else {
            r = 0.2f;
            g = 1.0f;
            b = 0.2f;
        }
        buffer.pos(x, y, 0.0).color(r, g, b, 1.0f).endVertex();
        buffer.pos(x, y + height, 0.0).color(r, g, b, 1.0f).endVertex();
        buffer.pos((float)x + CurWidth, y + height, 0.0).color(r, g, b, 1.0f).endVertex();
        buffer.pos((float)x + CurWidth, y, 0.0).color(r, g, b, 1.0f).endVertex();
        tessellator.draw();
        GlStateManager.popMatrix();
        GlStateManager.enableTexture2D();
        GlStateManager.disableRescaleNormal();
        GlStateManager.disableColorMaterial();
    }

    public void drawButton(BattleMode mode, int x, int y, int buttonWidth, int buttonHeight, String string, int mouseX, int mouseY, int ind) {
        this.drawButton(mode, x, y, buttonWidth, buttonHeight, string, false, mouseX, mouseY, ind, false);
    }

    public void drawButton(BattleMode mode, int x, int y, int buttonWidth, int buttonHeight, String string, boolean isDisabled, int mouseX, int mouseY, int ind) {
        this.drawButton(mode, x, y, buttonWidth, buttonHeight, string, isDisabled, mouseX, mouseY, ind, false);
    }

    public void drawButton(BattleMode mode, int x, int y, int buttonWidth, int buttonHeight, String string, boolean isDisabled, int mouseX, int mouseY, int ind, boolean sparkle) {
        if (mode == BattleMode.MainMenu) {
            if (mouseX > x && mouseX < x + buttonWidth && mouseY > y && mouseY < y + buttonHeight) {
                GuiHelper.drawImageQuad(x, y, buttonWidth, buttonHeight, 0.604687511920929, 0.3291666805744171, 0.7640625238418579, 0.40833333134651184, this.zLevel);
            }
        } else if (mode == BattleMode.ChooseAttack) {
            GuiHelper.drawImageQuad(x, y, buttonWidth, buttonHeight, 0.321875f, 0.3166666626930237, 0.614062488079071, 0.4208333194255829, this.zLevel);
            if (mouseX > x && mouseX < x + buttonWidth && mouseY > y && mouseY < y + buttonHeight) {
                GuiHelper.drawImageQuad(x + 2, y + 2, buttonWidth - 5, buttonHeight - 4, 0.0359375f, 0.3229166567325592, 0.3125, 0.40625, this.zLevel);
                this.mouseOverButton = ind;
            } else if (this.mouseOverButton == ind) {
                this.mouseOverButton = 5;
            }
        }
        if (!isDisabled) {
            float fontScale = 1.0f;
            int textWidth = (int)((float)this.mc.fontRenderer.getStringWidth(string) * fontScale);
            while (textWidth > buttonWidth - 9) {
                fontScale = (float)((double)fontScale - 0.02);
                textWidth = (int)((float)this.mc.fontRenderer.getStringWidth(string) * fontScale);
            }
            GL11.glPushMatrix();
            GL11.glScalef((float)fontScale, (float)fontScale, (float)fontScale);
            this.drawCenteredString(this.mc.fontRenderer, string, (int)((float)(x + buttonWidth / 2) / fontScale), (int)((float)(y + buttonHeight / 2 - 3) / fontScale), 0xFFFFFF);
            GL11.glScalef((float)1.0f, (float)1.0f, (float)1.0f);
            GL11.glPopMatrix();
        } else {
            this.mc.fontRenderer.drawString(string, x + buttonWidth / 2 - this.mc.fontRenderer.getStringWidth(string) / 2, y + buttonHeight / 2 - 3, 0xCCCCCC);
            GlStateManager.color(1.0f, 1.0f, 1.0f);
        }
    }

    @Override
    public void handleKeyboardInput() {
        if (Keyboard.getEventKeyState()) {
            int i = Keyboard.getEventKey();
            char c0 = Keyboard.getEventCharacter();
            this.keyTyped(c0, i);
        }
        try {
            super.handleKeyboardInput();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void keyTyped(char key, int keyCode) {
        try {
            super.keyTyped(key, keyCode);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if (!GuiChatExtension.chatOpen && keyCode == 28 && this.bm.hasMoreMessages()) {
            this.continueMessages();
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        try {
            super.mouseClicked(mouseX, mouseY, mouseButton);
        }
        catch (IOException iOException) {
            // empty catch block
        }
        if (this.bm.hasMoreMessages() && !this.isLevelScreenDrawn) {
            this.continueMessages();
        } else if (this.bm.mode != BattleMode.Waiting && this.currentScreen != null) {
            this.currentScreen.click(this.width, this.height, mouseX, mouseY);
        }
    }

    private boolean isLevelingUp() {
        return this.bm.mode == BattleMode.LevelUp || this.bm.mode == BattleMode.ReplaceAttack || this.bm.mode == BattleMode.YesNoReplaceMove || this.bm.hasNewAttacks();
    }

    protected void continueMessages() {
        this.flashCount = 0;
        this.bm.removeMessage();
        this.bm.checkClearedMessages();
    }

    @Override
    public void drawBackground(int par1) {
    }

    @Override
    public void drawDefaultBackground() {
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float par3) {
        Weather weather;
        GlobalStatusController controller;
        BattleControllerBase bc;
        super.drawScreen(mouseX, mouseY, par3);
        this.drawDefaultBackground();
        if (this.bm.mode != BattleMode.YesNoReplaceMove) {
            if (this.bm.hasLevelUps()) {
                if (this.bm.mode != BattleMode.LevelUp) {
                    this.bm.oldMode = this.bm.mode;
                    this.bm.mode = BattleMode.LevelUp;
                }
            } else if (this.bm.hasNewAttacks() && this.bm.mode != BattleMode.ReplaceAttack) {
                this.bm.oldMode = this.bm.mode;
                this.bm.mode = BattleMode.ReplaceAttack;
            }
        }
        if ((this.bm.mode == BattleMode.MainMenu || this.bm.mode == BattleMode.ChooseAttack || this.bm.mode == BattleMode.ApplyToPokemon) && this.bm.getUserPokemon() != null && (bc = BattleRegistry.getBattle(this.bm.battleControllerIndex)) != null && (controller = bc.globalStatusController) != null && (weather = controller.getWeather()) != null) {
            this.mc.renderEngine.bindTexture(new ResourceLocation("pixelmon:textures/gui/weather/" + weather.type.name().toLowerCase() + ".png"));
            GuiHelper.drawImageQuad(this.width - 35, (double)this.height - 35.0, 32.0, 32.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        }
        this.selectScreen();
        if (this.first) {
            this.first = false;
            if (ClientProxy.camera != null && !this.bm.battleEnded) {
                this.bm.setCameraToPlayer();
            }
        }
        if (!(this.bm.hasMoreMessages() || !this.bm.battleEnded || this.bm.hasLevelUps() || this.bm.hasNewAttacks() || this.bm.choosingPokemon)) {
            this.restoreSettingsAndClose();
        } else {
            RenderHelper.disableStandardItemLighting();
            GlStateManager.enableBlend();
            boolean overlayDrawn = false;
            if (!(this.bm.hasLevelUps() || this.bm.hasNewAttacks() || this.bm.mode != BattleMode.Waiting && this.bm.mode != BattleMode.MainMenu && this.bm.mode != BattleMode.ChooseAttack && this.bm.mode != BattleMode.ChooseTargets && this.bm.mode != BattleMode.EnforcedSwitch)) {
                this.drawPokemonOverlays();
                if (this.pokemonOverlay != null) {
                    overlayDrawn = true;
                }
            }
            if ((!this.bm.hasMoreMessages() || this.isLevelScreenDrawn) && this.bm.mode != BattleMode.Waiting) {
                if (this.currentScreen != null) {
                    this.currentScreen.drawScreen(this.width, this.height, mouseX, mouseY);
                    this.isLevelScreenDrawn = this.isLevelingUp();
                }
            } else {
                this.drawMessageScreen();
                if (!overlayDrawn) {
                    this.drawPokemonOverlays();
                }
            }
            if (this.bm.isSpectating) {
                if (this.stopSpectateButton == null) {
                    this.addStopSpectateButton();
                }
                this.stopSpectateButton.drawButton(this.mc, mouseX, mouseY, 1.0f);
            }
            if (this.bm.afkOn) {
                GuiHelper.drawBattleTimer(this, this.bm.afkTime);
                if (this.bm.afkTime <= 0) {
                    this.bm.afkActive = true;
                    this.bm.clearMessages();
                    this.bm.afkSelectMove();
                    this.bm.resetAFKTime();
                }
            }
            GlStateManager.disableRescaleNormal();
            RenderHelper.disableStandardItemLighting();
            GlStateManager.disableLighting();
            GlStateManager.disableDepth();
        }
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        if (this.currentScreen != null) {
            this.currentScreen.updateScreen();
        }
    }

    public int getGuiWidth() {
        return this.guiWidth;
    }

    public int getGuiHeight() {
        return this.guiHeight;
    }

    public void setTargetting(Attack attack, int opponentTarget, int userTarget) {
        for (int i = 0; i < this.bm.targetted.length; ++i) {
            Arrays.fill(this.bm.targetted[i], false);
        }
        TargetingInfo info = attack.getAttackBase().targetingInfo;
        if (info.hitsAll) {
            if (info.hitsOppositeFoe && this.bm.currentPokemon < this.bm.targetted[1].length) {
                Arrays.fill(this.bm.targetted[1], true);
            }
            if (info.hitsAdjacentFoe) {
                if (this.bm.currentPokemon - 1 >= 0) {
                    this.bm.targetted[1][this.bm.currentPokemon - 1] = true;
                }
                if (this.bm.currentPokemon + 1 < this.bm.targetted[1].length) {
                    this.bm.targetted[1][this.bm.currentPokemon + 1] = true;
                }
            }
            if (info.hitsExtendedFoe) {
                if (this.bm.currentPokemon - 2 >= 0) {
                    this.bm.targetted[1][this.bm.currentPokemon - 2] = true;
                }
                if (this.bm.currentPokemon + 2 < this.bm.targetted[1].length) {
                    this.bm.targetted[1][this.bm.currentPokemon + 2] = true;
                }
            }
            if (info.hitsSelf) {
                this.bm.targetted[0][this.bm.currentPokemon] = true;
            }
            if (info.hitsAdjacentAlly) {
                if (this.bm.currentPokemon - 1 >= 0) {
                    this.bm.targetted[0][this.bm.currentPokemon - 1] = true;
                }
                if (this.bm.currentPokemon + 1 < this.bm.targetted[0].length) {
                    this.bm.targetted[0][this.bm.currentPokemon + 1] = true;
                }
            }
            if (info.hitsExtendedAlly) {
                if (this.bm.currentPokemon - 2 >= 0) {
                    this.bm.targetted[0][this.bm.currentPokemon - 2] = true;
                }
                if (this.bm.currentPokemon + 2 < this.bm.targetted[0].length) {
                    this.bm.targetted[0][this.bm.currentPokemon + 2] = true;
                }
            }
        } else {
            if (userTarget != -1) {
                if (userTarget == this.bm.currentPokemon && info.hitsSelf) {
                    this.bm.targetted[0][this.bm.currentPokemon] = true;
                }
                if (info.hitsAdjacentAlly) {
                    if (userTarget == this.bm.currentPokemon + 1) {
                        this.bm.targetted[0][this.bm.currentPokemon + 1] = true;
                    }
                    if (userTarget == this.bm.currentPokemon - 1) {
                        this.bm.targetted[0][this.bm.currentPokemon - 1] = true;
                    }
                }
                if (info.hitsExtendedAlly) {
                    if (userTarget == this.bm.currentPokemon + 2) {
                        this.bm.targetted[0][this.bm.currentPokemon + 2] = true;
                    }
                    if (userTarget == this.bm.currentPokemon - 2) {
                        this.bm.targetted[0][this.bm.currentPokemon - 2] = true;
                    }
                }
            }
            if (opponentTarget != -1) {
                if (info.hitsOppositeFoe && opponentTarget == this.bm.currentPokemon) {
                    this.bm.targetted[1][opponentTarget] = true;
                }
                if (info.hitsAdjacentFoe) {
                    if (opponentTarget <= this.bm.targetted[1].length) {
                        this.bm.targetted[1][opponentTarget] = true;
                    }
                    if (opponentTarget == this.bm.currentPokemon - 1) {
                        this.bm.targetted[1][this.bm.currentPokemon - 1] = true;
                    }
                }
                if (info.hitsExtendedFoe) {
                    if (opponentTarget == this.bm.currentPokemon + 2) {
                        this.bm.targetted[1][this.bm.currentPokemon + 2] = true;
                    }
                    if (opponentTarget == this.bm.currentPokemon - 2) {
                        this.bm.targetted[1][this.bm.currentPokemon - 2] = true;
                    }
                }
            }
            if (userTarget == -1 && opponentTarget == -1) {
                boolean isCurseUser;
                boolean bl = isCurseUser = attack.isAttack("Curse") && !this.bm.getUserPokemonPacket().hasType(EnumType.Ghost);
                if (!info.hitsSelf && !isCurseUser) {
                    if (info.hitsOppositeFoe && this.bm.currentPokemon < this.bm.targetted[1].length) {
                        this.bm.targetted[1][this.bm.currentPokemon] = true;
                    }
                } else {
                    this.bm.targetted[0][this.bm.currentPokemon] = true;
                }
            }
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        if (button.id == 0) {
            this.bm.endSpectate();
            this.bm.clearMessages();
            Pixelmon.NETWORK.sendToServer(new RemoveSpectator(this.bm.battleControllerIndex, this.mc.player.getUniqueID()));
        }
    }
}

