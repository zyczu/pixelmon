/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.timerTasks;

import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.client.gui.battles.timerTasks.BattleTask;
import com.pixelmongenerations.common.battle.attacks.IBattleMessage;
import com.pixelmongenerations.core.proxy.ClientProxy;

public class SwitchTask
extends BattleTask {
    int ticks = 0;
    int[] pix1ID;
    PixelmonInGui pokemon;
    PixelmonInGui newPokemon;
    String oldName;

    public SwitchTask(IBattleMessage message, int[] pix1ID, PixelmonInGui newPokemon) {
        super(message);
        this.pix1ID = pix1ID;
        this.newPokemon = newPokemon;
    }

    @Override
    public void run() {
        if (this.newPokemon.pokemonID[1] == -1) {
            if (this.ticks < 120) {
                this.loadPokemon(this.pix1ID);
                --this.pokemon.xPos;
            } else {
                this.cancel();
            }
        } else {
            PixelmonInGui initPokemon;
            if (this.ticks < 120 && this.pix1ID[0] == -1 && this.pix1ID[1] == -1) {
                this.ticks = 120;
            } else if (this.ticks == 0 && (initPokemon = ClientProxy.battleManager.getPokemon(this.pix1ID)) != null) {
                initPokemon.pokemonID = this.newPokemon.pokemonID;
            }
            if (this.ticks < 120) {
                this.loadPokemon(this.newPokemon.pokemonID);
                --this.pokemon.xPos;
                if (this.oldName == null) {
                    this.oldName = this.pokemon.nickname;
                }
                this.pokemon.isSwitching = true;
            } else if (this.ticks == 120) {
                this.loadPokemon(this.newPokemon.pokemonID);
                this.pokemon.status = this.newPokemon.status;
                this.pokemon.health = this.newPokemon.health;
                this.pokemon.maxHealth = this.newPokemon.maxHealth;
                this.pokemon.level = this.newPokemon.level;
                this.pokemon.gender = this.newPokemon.gender;
                if (this.pokemon.nickname.equals(this.oldName)) {
                    this.pokemon.nickname = this.newPokemon.nickname;
                }
                this.pokemon.pokemonName = this.newPokemon.pokemonName;
                this.pokemon.shiny = this.newPokemon.shiny;
                this.pokemon.expFraction = this.newPokemon.expFraction;
                this.pokemon.form = this.newPokemon.form;
                this.pokemon.specialTexture = this.newPokemon.specialTexture;
                this.pokemon.xPos = ClientProxy.battleManager.isEnemyPokemon(this.pokemon) ? -120 : 0;
            } else if (this.ticks > 120 && this.ticks < 241) {
                this.loadPokemon(this.newPokemon.pokemonID);
                ++this.pokemon.xPos;
                this.pokemon.isSwitching = false;
            } else if (this.ticks >= 241) {
                this.cancel();
            }
        }
        ++this.ticks;
    }

    private void loadPokemon(int[] pokemonID) {
        if (this.pokemon == null) {
            this.pokemon = ClientProxy.battleManager.getPokemon(pokemonID);
            if (this.pokemon == null) {
                this.cancel();
            }
        }
    }
}

