/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.client.gui.battles.battleScreens.BattleScreen;
import com.pixelmongenerations.common.item.ItemBattleItem;
import com.pixelmongenerations.common.item.ItemData;
import com.pixelmongenerations.common.item.ItemEther;
import com.pixelmongenerations.common.item.ItemMedicine;
import com.pixelmongenerations.common.item.ItemPokeball;
import com.pixelmongenerations.common.item.ItemStatusAilmentHealer;
import com.pixelmongenerations.common.item.heldItems.ItemAdrenalineOrb;
import com.pixelmongenerations.common.item.heldItems.ItemBerryGinema;
import com.pixelmongenerations.common.item.heldItems.ItemBerryLeppa;
import com.pixelmongenerations.common.item.heldItems.ItemBerryRestoreHP;
import com.pixelmongenerations.common.item.heldItems.ItemBerryStatus;
import com.pixelmongenerations.common.item.heldItems.ItemMentalHerb;
import com.pixelmongenerations.common.item.heldItems.ItemWhiteHerb;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.battle.BagSection;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;

public class ChooseBag
extends BattleScreen {
    public ChooseBag(GuiBattle parent) {
        super(parent, BattleMode.ChooseBag);
    }

    @Override
    public void drawScreen(int width, int height, int mouseX, int mouseY) {
        this.mc.renderEngine.bindTexture(GuiResources.itemGui1);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(width / 2 - 128, height / 2 - 76, 256.0, 153.0f, 0.0, 0.0, 1.0, 0.59765625, this.zLevel);
        this.drawCenteredString(this.mc.fontRenderer, I18n.translateToLocal("gui.choosebag.pokeballs"), width / 2 + 53, height / 2 - 36, 0xFFFFFF);
        this.drawCenteredString(this.mc.fontRenderer, I18n.translateToLocal("gui.choosebag.hppp"), width / 2 + 53, height / 2 + 31, 0xFFFFFF);
        this.drawCenteredString(this.mc.fontRenderer, I18n.translateToLocal("gui.choosebag.statusrestore"), width / 2 - 53, height / 2 - 36, 0xFFFFFF);
        this.drawCenteredString(this.mc.fontRenderer, I18n.translateToLocal("gui.choosebag.battleitems"), width / 2 - 53, height / 2 + 31, 0xFFFFFF);
        int x1 = width / 2 - 103;
        int x2 = width / 2 + 3;
        int y1 = height / 2 - 63;
        int y2 = height / 2 + 4;
        this.mc.renderEngine.bindTexture(GuiResources.itemGui1);
        int buttonWidth = 100;
        int buttonHeight = 62;
        if (mouseX > x1 && mouseX < x1 + buttonWidth && mouseY > y1 && mouseY < y1 + buttonHeight) {
            GuiHelper.drawImageQuad(x1, y1, buttonWidth, buttonHeight, 0.109375, 0.6875, 0.5, 0.9296875, this.zLevel);
        }
        if (mouseX > x1 && mouseX < x1 + buttonWidth && mouseY > y2 && mouseY < y2 + buttonHeight) {
            GuiHelper.drawImageQuad(x1, y2, buttonWidth, buttonHeight, 0.109375, 0.6875, 0.5, 0.9296875, this.zLevel);
        }
        if (mouseX > x2 && mouseX < x2 + buttonWidth && mouseY > y1 && mouseY < y1 + buttonHeight) {
            GuiHelper.drawImageQuad(x2, y1, buttonWidth, buttonHeight, 0.109375, 0.6875, 0.5, 0.9296875, this.zLevel);
        }
        if (mouseX > x2 && mouseX < x2 + buttonWidth && mouseY > y2 && mouseY < y2 + buttonHeight) {
            GuiHelper.drawImageQuad(x2, y2, buttonWidth, buttonHeight, 0.109375, 0.6875, 0.5, 0.9296875, this.zLevel);
        }
        if (mouseX > width / 2 + 106 && mouseX < width / 2 + 126 && mouseY > height / 2 + 55 && mouseY < height / 2 + 77) {
            GuiHelper.drawImageQuad(width / 2 + 106, height / 2 + 55, 20.0, 22.0f, 0.9140625, 0.6015625, 0.9921875, 0.6875, this.zLevel);
        }
    }

    @Override
    public void click(int width, int height, int mouseX, int mouseY) {
        if (mouseX > width / 2 + 106 && mouseX < width / 2 + 126 && mouseY > height / 2 + 55 && mouseY < height / 2 + 77) {
            this.bm.mode = BattleMode.MainMenu;
        }
        int x1 = width / 2 - 103;
        int x2 = width / 2 + 3;
        int y1 = height / 2 - 63;
        int y2 = height / 2 + 4;
        int buttonWidth = 100;
        int buttonHeight = 62;
        this.bm.bagSection = null;
        if (mouseX > x1 && mouseX < x1 + buttonWidth && mouseY > y1 && mouseY < y1 + buttonHeight) {
            this.bm.bagSection = BagSection.StatusRestore;
        } else if (mouseX > x1 && mouseX < x1 + buttonWidth && mouseY > y2 && mouseY < y2 + buttonHeight) {
            this.bm.bagSection = BagSection.BattleItems;
        } else if (mouseX > x2 && mouseX < x2 + buttonWidth && mouseY > y1 && mouseY < y1 + buttonHeight) {
            if (this.bm.canCatchOpponent()) {
                if (this.bm.displayedEnemyPokemon.length > 1) {
                    int unfainted = 0;
                    for (PixelmonInGui pokemon : this.bm.displayedEnemyPokemon) {
                        if (pokemon.health <= 0.0f) continue;
                        ++unfainted;
                    }
                    if (unfainted > 1) {
                        this.bm.addMessage(I18n.translateToLocal("gui.choosebag.multiple"));
                    } else {
                        this.bm.bagSection = BagSection.Pokeballs;
                    }
                } else {
                    this.bm.bagSection = BagSection.Pokeballs;
                }
            } else {
                this.bm.addMessage(I18n.translateToLocal("gui.choosebag.nopokeballs"));
            }
        } else if (mouseX > x2 && mouseX < x2 + buttonWidth && mouseY > y2 && mouseY < y2 + buttonHeight) {
            this.bm.bagSection = BagSection.HP;
        }
        if (this.bm.bagSection != null) {
            this.bm.mode = BattleMode.UseBag;
            this.bm.bagStore.clear();
            this.getInventory();
            this.bm.startIndex = 0;
        }
    }

    private void getInventory() {
        InventoryPlayer inventory = this.mc.player.inventory;
        for (ItemStack stack : inventory.mainInventory) {
            if (!stack.isEmpty()) {
                Item item = stack.getItem();
                boolean valid = ChooseBag.checkItem(this.bm.bagSection, item);
                if (valid) {
                    this.checkExists(item, stack.getCount());
                }
            }
        }
        if (this.mc.player.hasItemInSlot(EntityEquipmentSlot.OFFHAND)) {
            Item item = this.mc.player.getHeldItemOffhand().getItem();
            boolean valid = ChooseBag.checkItem(this.bm.bagSection, item);
            if (valid) {
                this.checkExists(item, this.mc.player.getHeldItemOffhand().getCount());
            }
        }
    }

    public static boolean checkItem(BagSection section, Item item) {
        switch (section) {
            case BattleItems: {
                return item instanceof ItemBattleItem || item instanceof ItemAdrenalineOrb;
            }
            case StatusRestore: {
                return item instanceof ItemStatusAilmentHealer || item == PixelmonItems.fullRestore || item instanceof ItemBerryStatus || item instanceof ItemMentalHerb || item instanceof ItemBerryGinema || item instanceof ItemWhiteHerb;
            }
            case Pokeballs: {
                return item instanceof ItemPokeball;
            }
            case HP: {
                return item instanceof ItemBerryRestoreHP || item instanceof ItemMedicine || item instanceof ItemEther || item instanceof ItemBerryLeppa;
            }
        }
        return false;
    }

    private void checkExists(Item item, int count) {
        boolean hasItem = false;
        for (ItemData d : this.bm.bagStore) {
            if (Item.getItemById(d.id) != item) continue;
            hasItem = true;
            d.count += count;
        }
        if (!hasItem) {
            this.bm.bagStore.add(new ItemData(Item.getIdFromItem(item), count));
        }
    }
}

