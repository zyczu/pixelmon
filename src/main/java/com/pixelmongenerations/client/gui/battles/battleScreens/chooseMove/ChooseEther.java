/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens.chooseMove;

import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.chooseMove.ChooseMove;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.packetHandlers.battles.BagPacket;
import com.pixelmongenerations.core.network.packetHandlers.battles.UseEther;

public class ChooseEther
extends ChooseMove {
    public ChooseEther(GuiBattle parent) {
        super(parent, BattleMode.ChooseEther);
    }

    @Override
    protected void clickMove(int moveIndex) {
        int[] pokemonID = this.bm.getUserPokemonPacket().pokemonID;
        if (this.bm.battleEnded) {
            Pixelmon.NETWORK.sendToServer(new UseEther(moveIndex, pokemonID));
            this.bm.choosingPokemon = false;
        } else {
            this.bm.selectedActions.add(new BagPacket(pokemonID, this.bm.itemToUse.id, this.bm.battleControllerIndex, moveIndex));
            this.bm.selectedMove();
        }
    }
}

