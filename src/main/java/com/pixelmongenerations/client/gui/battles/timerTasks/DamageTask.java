/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.timerTasks;

import com.pixelmongenerations.client.gui.battles.timerTasks.HPTask;
import com.pixelmongenerations.common.battle.attacks.IBattleMessage;

public class DamageTask
extends HPTask {
    public DamageTask(IBattleMessage message, float healthDifference, int[] id) {
        super(message, healthDifference, id);
    }

    @Override
    protected void boundsCheck() {
        if (this.currentHealth + this.healthDifference < 0.0f) {
            this.healthDifference = -this.currentHealth;
        }
    }

    @Override
    protected boolean isDone() {
        return this.currentHealth <= this.originalHealth + this.healthDifference;
    }
}

