/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.BattleScreen;
import com.pixelmongenerations.client.gui.battles.battleScreens.ChooseAttackScreen;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.MaxAttackHelper;
import com.pixelmongenerations.common.item.heldItems.ZCrystal;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.packetHandlers.battles.ChooseAttack;
import com.pixelmongenerations.core.util.RegexPatterns;
import java.util.Arrays;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.text.translation.I18n;

public class ChooseTargets
extends BattleScreen {
    public ChooseTargets(GuiBattle parent) {
        super(parent, BattleMode.ChooseTargets);
    }

    @Override
    public void drawScreen(int width, int height, int mouseX, int mouseY) {
        this.mc.renderEngine.bindTexture(GuiResources.battleGui3B);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(width / 2 - this.parent.getGuiWidth() / 2, height - this.parent.getGuiHeight(), this.parent.getGuiWidth(), this.parent.getGuiHeight(), 0.0, 0.0, 1.0, 0.30416667461395264, this.zLevel);
        Attack attack = null;
        if (this.bm.selectedAttack >= 4) {
            int tempButtonId = this.bm.selectedAttack - 4;
            Attack tempMove = this.bm.dynamaxing || Arrays.equals(this.bm.getUserPokemonPacket().pokemonID, this.bm.dynamax) && !this.bm.hasDynamaxed ? MaxAttackHelper.getMaxMove(this.bm.getUserPokemonPacket(), this.bm.getUserPokemonPacket().moveset[tempButtonId].getAttack()) : this.bm.getUserPokemonPacket().moveset[tempButtonId].getAttack();
            if (this.bm.getUserPokemonPacket().heldItem.getItem() instanceof ZCrystal) {
                ZCrystal crystal = (ZCrystal)this.bm.getUserPokemonPacket().heldItem.getItem();
                if (crystal.canEffectMove(new PokemonSpec(this.bm.getUserPokemonPacket().getSpecies().name).setForm(this.bm.getUserPokemonPacket().form), tempMove)) {
                    Attack zAttack = crystal.getUsableAttack(tempMove);
                    if (zAttack != null) {
                        attack = zAttack;
                    } else {
                        this.bm.selectedAttack -= 4;
                    }
                } else {
                    this.bm.selectedAttack -= 4;
                }
            } else if (this.bm.dynamaxing || Arrays.equals(this.bm.getUserPokemonPacket().pokemonID, this.bm.dynamax) && !this.bm.hasDynamaxed) {
                Attack maxAttack = MaxAttackHelper.getMaxMove(this.bm.getUserPokemonPacket(), tempMove);
                attack = maxAttack;
            } else {
                this.bm.selectedAttack -= 4;
            }
        } else {
            attack = this.bm.getUserPokemonPacket().moveset[this.bm.selectedAttack].getAttack();
        }
        if (attack == null) {
            attack = this.bm.getUserPokemonPacket().moveset[this.bm.selectedAttack].getAttack();
        }
        this.drawCenteredString(this.mc.fontRenderer, RegexPatterns.$_A_VAR.matcher(I18n.translateToLocal("battlecontroller.targets")).replaceAll(attack.getAttackBase().getLocalizedName()), width / 2, height - 35, 0xFFFFFF);
        int tempInt = this.parent.pokemonOverlay.mouseOverUserPokemon(width, height, this.parent.getGuiWidth(), this.parent.getGuiHeight(), mouseX, mouseY);
        if (tempInt != -1) {
            this.parent.setTargetting(attack, -1, tempInt);
        }
        if ((tempInt = this.parent.pokemonOverlay.mouseOverEnemyPokemon(this.parent.getGuiWidth(), this.parent.getGuiHeight(), mouseX, mouseY)) != -1) {
            this.parent.setTargetting(attack, tempInt, -1);
        }
        if (mouseX > width / 2 + 137 && mouseX < width / 2 + 148 && mouseY > height - 11 && mouseY < height - 1) {
            this.mc.renderEngine.bindTexture(GuiResources.battleGui3B);
            GuiHelper.drawImageQuad(width / 2 + 137, height - 11, 11.0, 10.0f, 0.957812488079071, 0.31458333134651184, 0.9921875, 0.35625f, this.zLevel);
        }
    }

    @Override
    public void click(int width, int height, int mouseX, int mouseY) {
        if (mouseX > width / 2 + 137 && mouseX < width / 2 + 148 && mouseY > height - 11 && mouseY < height - 1) {
            this.bm.mode = BattleMode.ChooseAttack;
            return;
        }
        int tempInt = this.parent.pokemonOverlay.mouseOverUserPokemon(width, height, this.parent.getGuiWidth(), this.parent.getGuiHeight(), mouseX, mouseY);
        if (tempInt != -1 && this.isValid(tempInt, true)) {
            this.bm.selectedActions.add(new ChooseAttack(this.bm.getUserPokemonPacket().pokemonID, ChooseAttackScreen.clone2D(this.bm.targetted), this.bm.selectedAttack, this.bm.battleControllerIndex, this.bm.evolving, this.bm.dynamaxing));
            this.bm.selectedMove();
        }
        if ((tempInt = this.parent.pokemonOverlay.mouseOverEnemyPokemon(this.parent.getGuiWidth(), this.parent.getGuiHeight(), mouseX, mouseY)) != -1 && this.isValid(tempInt, false)) {
            this.bm.selectedActions.add(new ChooseAttack(this.bm.getUserPokemonPacket().pokemonID, ChooseAttackScreen.clone2D(this.bm.targetted), this.bm.selectedAttack, this.bm.battleControllerIndex, this.bm.evolving, this.bm.dynamaxing));
            this.bm.selectedMove();
        }
    }

    private boolean isValid(int tempInt, boolean isUser) {
        return isUser ? this.bm.targetted[0][tempInt] : this.bm.targetted[1][tempInt];
    }
}

