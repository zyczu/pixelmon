/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.battles.battleScreens.choosePokemon;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.BattleScreen;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.item.ItemRevive;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.battles.SwitchPokemon;
import java.util.ArrayList;
import java.util.Timer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.Item;
import net.minecraft.util.text.translation.I18n;

public class ChoosePokemon
extends BattleScreen {
    protected ArrayList<PixelmonData> inBattle = new ArrayList(6);
    protected ArrayList<PixelmonData> inParty = new ArrayList(6);
    Timer timer;
    protected String backText = "";

    public ChoosePokemon(GuiBattle parent) {
        super(parent, BattleMode.ChoosePokemon);
    }

    public ChoosePokemon(GuiBattle parent, BattleMode mode) {
        super(parent, mode);
    }

    @Override
    public void drawScreen(int width, int height, int mouseX, int mouseY) {
        this.fillParty();
        this.mc.renderEngine.bindTexture(GuiResources.choosePokemon);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(width / 2 - 128, height - 203, 256.0, 203.0f, 0.0, 0.0, 1.0, 0.79296875, this.zLevel);
        if (this.bm.mode == BattleMode.ApplyToPokemon) {
            this.drawCenteredString(this.mc.fontRenderer, I18n.translateToLocal("gui.choosepokemon.select"), width / 2 - 40, height - 23, 0xFFFFFF);
        } else if (this.bm.mode == BattleMode.ChooseRelearnMove || this.bm.mode == BattleMode.ChooseTutor) {
            this.drawCenteredString(this.mc.fontRenderer, I18n.translateToLocal("gui.selectpokemon.message"), width / 2 - 40, height - 23, 0xFFFFFF);
        } else {
            this.drawCenteredString(this.mc.fontRenderer, I18n.translateToLocal("gui.choosepokemon.sendout"), width / 2 - 40, height - 23, 0xFFFFFF);
        }
        this.backText = this.getBackText();
        if (!"".equals(this.backText)) {
            if (mouseX > width / 2 + 63 && mouseX < width / 2 + 63 + 48 && mouseY > height - 27 && mouseY < height - 27 + 17 && !this.bm.isHealing) {
                this.mc.renderEngine.bindTexture(GuiResources.choosePokemon);
                GuiHelper.drawImageQuad(width / 2 + 63, height - 27, 48.0, 17.0f, 0.7734375, 0.8203125, 0.9609375, 0.88671875, this.zLevel);
            }
            this.drawCenteredString(this.mc.fontRenderer, I18n.translateToLocal(this.backText), width / 2 + 87, height - 22, 0xFFFFFF);
        }
        PixelmonData p = !this.inBattle.isEmpty() ? this.inBattle.get(0) : this.inParty.get(0);
        Item item = this.bm.itemToUse != null ? Item.getItemById(this.bm.itemToUse.id) : null;
        Item item2 = item;
        if (p != null) {
            GuiHelper.bindPokemonSprite(p, this.mc);
            GuiHelper.drawImageQuad(width / 2 - 121, height - 176 - (p.isGen6Sprite() ? 0 : 3), 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            GuiBattle.drawHealthBar(width / 2 - 85, height - 135, 56, 9, p.health, p.hp);
            this.mc.renderEngine.bindTexture(GuiResources.choosePokemon);
            GuiHelper.drawImageQuad(width / 2 - 95, height - 135, 61.0, 9.0f, 0.3359375, 0.9375, 0.57421875, 0.97265625, this.zLevel);
            this.drawCenteredString(this.mc.fontRenderer, p.health + "/" + p.hp, width / 2 - 59, height - 123, 0xFFFFFF);
            String name = p.getNickname();
            this.drawString(this.mc.fontRenderer, name, width / 2 - 90, height - 161, 0xFFFFFF);
            this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.overlay1v1.lv") + p.lvl, width / 2 - 90, height - 148, 0xFFFFFF);
            float[] texturePair = p.getStatusTexture();
            if (texturePair[0] != -1.0f) {
                this.mc.renderEngine.bindTexture(GuiResources.status);
                GuiHelper.drawImageQuad(width / 2 - 117, height - 150, 24.0, 10.5f, texturePair[0] / 299.0f, texturePair[1] / 210.0f, (texturePair[0] + 147.0f) / 299.0f, (texturePair[1] + 68.0f) / 210.0f, this.zLevel);
            }
            this.mc.renderEngine.bindTexture(GuiResources.choosePokemon);
            if (p.gender == Gender.Male) {
                GuiHelper.drawImageQuad(width / 2 - 90 + this.mc.fontRenderer.getStringWidth(name), height - 161, 6.0, 9.0f, 0.125, 0.8125, 0.1484375, 0.84765625, this.zLevel);
            } else if (p.gender == Gender.Female) {
                GuiHelper.drawImageQuad(width / 2 - 90 + this.mc.fontRenderer.getStringWidth(name), height - 161, 6.0, 9.0f, 0.125, 0.8515625, 0.1484375, 0.88671875, this.zLevel);
            }
            if (this.bm.mode == BattleMode.ApplyToPokemon && !this.bm.isHealing && mouseX > width / 2 - 120 && mouseX < width / 2 - 21 && mouseY > height - 165 && mouseY < height - 113) {
                boolean valid = false;
                if (item instanceof ItemRevive) {
                    if (p.isFainted) {
                        valid = true;
                    }
                } else if (!p.isFainted) {
                    valid = true;
                }
                if (valid) {
                    this.mc.renderEngine.bindTexture(GuiResources.selectCurrentPokemon);
                    GuiHelper.drawImageQuad(width / 2 - 120, height - 165, 89.0, 52.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
                }
            }
        }
        int inBattleIndex = 1;
        int inPartyIndex = this.inBattle.isEmpty() ? 1 : 0;
        for (int i = 0; i < 5; ++i) {
            PixelmonData pData;
            PixelmonData pixelmonData = pData = inBattleIndex < this.inBattle.size() ? this.inBattle.get(inBattleIndex++) : this.inParty.get(inPartyIndex++);
            if (pData == null) continue;
            GuiHelper.bindPokemonSprite(pData, this.mc);
            GuiHelper.drawImageQuad(width / 2 - 23, height - 192 + i * 30, 24.0, 24.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
            GuiBattle.drawHealthBar(width / 2 + 65, height - 192 + i * 30, 56, 9, pData.health, pData.hp);
            this.mc.renderEngine.bindTexture(GuiResources.choosePokemon);
            GuiHelper.drawImageQuad(width / 2 + 55, height - 192 + i * 30, 61.0, 9.0f, 0.3359375, 0.9375, 0.57421875, 0.97265625, this.zLevel);
            if (!pData.isEgg) {
                this.drawString(this.mc.fontRenderer, pData.health + "/" + pData.hp, width / 2 + 75, height - 180 + i * 30, 0xFFFFFF);
                this.drawString(this.mc.fontRenderer, pData.getNickname(), width / 2 + 5, height - 190 + i * 30, 0xFFFFFF);
                this.drawString(this.mc.fontRenderer, I18n.translateToLocal("gui.overlay1v1.lv") + pData.lvl, width / 2 + 5, height - 176 + i * 30, 0xFFFFFF);
                float[] texturePair = pData.getStatusTexture();
                if (texturePair[0] != -1.0f) {
                    this.mc.renderEngine.bindTexture(GuiResources.status);
                    GuiHelper.drawImageQuad(width / 2 + 48, height - 177 + i * 30, 24.0, 10.5f, texturePair[0] / 299.0f, texturePair[1] / 210.0f, (texturePair[0] + 147.0f) / 299.0f, (texturePair[1] + 68.0f) / 210.0f, this.zLevel);
                }
                this.mc.renderEngine.bindTexture(GuiResources.choosePokemon);
                if (pData.gender == Gender.Male) {
                    GuiHelper.drawImageQuad(width / 2 + 40, height - 176 + i * 30, 6.0, 9.0f, 0.125, 0.8125, 0.1484375, 0.84765625, this.zLevel);
                } else if (pData.gender == Gender.Female) {
                    GuiHelper.drawImageQuad(width / 2 + 40, height - 176 + i * 30, 6.0, 9.0f, 0.125, 0.8515625, 0.1484375, 0.88671875, this.zLevel);
                }
            } else {
                this.drawString(this.mc.fontRenderer, Entity1Base.getLocalizedName("egg"), width / 2 + 5, height - 190 + i * 30, 0xFFFFFF);
            }
            int xPos = width / 2 - 30;
            int yPos = height - 195 + i * 30;
            boolean isCurrent = this.inBattle.contains(pData);
            if (pData.isEgg || (!isCurrent || this.bm.mode != BattleMode.ApplyToPokemon) && isCurrent || mouseX <= xPos || mouseX >= xPos + 150 || mouseY <= yPos + 1 || mouseY >= yPos + 31 || this.bm.isHealing) continue;
            boolean valid = false;
            if (this.bm.mode == BattleMode.ApplyToPokemon && item instanceof ItemRevive) {
                if (pData.isFainted) {
                    valid = true;
                }
            } else if (!pData.isFainted && !pData.selected) {
                valid = true;
            }
            if (!valid) continue;
            GuiHelper.drawImageQuad(xPos, yPos, 150.0, 32.0f, 0.16796875, 0.80078125, 0.7578125, 0.92578125, this.zLevel);
        }
    }

    @Override
    public void click(int width, int height, int mouseX, int mouseY) {
        if (mouseX > width / 2 + 63 && mouseX < width / 2 + 63 + 48 && mouseY > height - 27 && mouseY < height - 27 + 17) {
            this.clickBackButton();
        }
        if (this.bm.mode == BattleMode.MainMenu) {
            return;
        }
        int inBattleIndex = 1;
        int inPartyIndex = 0;
        for (int i = 0; i < 5; ++i) {
            boolean canSelect;
            PixelmonData pData;
            if (inBattleIndex < this.inBattle.size()) {
                pData = this.inBattle.get(inBattleIndex++);
            } else if (inPartyIndex < this.inParty.size()) {
                pData = this.inParty.get(inPartyIndex++);
            } else {
                return;
            }
            if (pData == null) continue;
            int xpos = width / 2 - 30;
            int ypos = height - 195 + i * 30;
            if (mouseX <= xpos || mouseX >= xpos + 150 || mouseY <= ypos + 1 || mouseY >= ypos + 31) continue;
            boolean bl = canSelect = !this.inBattle.contains(pData) && !pData.isEgg;
            if (pData.selected) {
                if (this.bm.selectedActions.isEmpty()) {
                    pData.selected = false;
                } else {
                    canSelect = false;
                }
            }
            if (!canSelect) continue;
            pData.selected = true;
            if (pData.isFainted) continue;
            this.addSwitch(pData.order);
            this.selectedMove();
            return;
        }
    }

    protected void selectedMove() {
        this.bm.selectedMove();
    }

    public int choosePokemonSlot(int width, int height, int mouseX, int mouseY) {
        int pokemonToApplyTo;
        block3: {
            PixelmonData[] party;
            block2: {
                if (mouseX > width / 2 + 63 && mouseX < width / 2 + 63 + 48 && mouseY > height - 27 && mouseY < height - 27 + 17) {
                    Minecraft.getMinecraft().player.closeScreen();
                    return -1;
                }
                pokemonToApplyTo = -1;
                party = this.getParty();
                if (mouseX <= width / 2 - 120 || mouseX >= width / 2 - 21 || mouseY <= height - 165 || mouseY >= height - 113) break block2;
                if (party[0] == null) break block3;
                pokemonToApplyTo = 0;
                break block3;
            }
            for (int i = 0; i < 5; ++i) {
                PixelmonData pdata = party[i + 1];
                if (pdata == null) continue;
                int xpos = width / 2 - 30;
                int ypos = height - 195 + i * 30;
                if (mouseX <= xpos || mouseX >= xpos + 150 || mouseY <= ypos + 1 || mouseY >= ypos + 31) continue;
                pokemonToApplyTo = i + 1;
                break;
            }
        }
        return pokemonToApplyTo;
    }

    protected PixelmonData[] getParty() {
        PixelmonData[] party = new PixelmonData[6];
        int arrayIndex = 0;
        for (PixelmonData data : this.inBattle) {
            if (arrayIndex >= 6) continue;
            party[arrayIndex++] = data;
        }
        for (PixelmonData data : this.inParty) {
            if (arrayIndex >= 6) continue;
            party[arrayIndex++] = data;
        }
        return party;
    }

    protected void fillParty() {
        this.inBattle.clear();
        if (this.bm.teamPokemon != null) {
            for (int[] id : this.bm.teamPokemon) {
                if (!ServerStorageDisplay.has(id)) continue;
                this.inBattle.add(ServerStorageDisplay.get(id));
            }
        }
        this.inParty.clear();
        for (PixelmonData d : ServerStorageDisplay.getPokemon()) {
            if (this.inBattle.contains(d)) continue;
            this.inParty.add(d);
        }
    }

    protected String getBackText() {
        return "gui.battle.back";
    }

    protected void clickBackButton() {
        this.bm.mode = BattleMode.MainMenu;
    }

    protected void addSwitch(int position) {
        this.bm.selectedActions.add(new SwitchPokemon(position, this.bm.battleControllerIndex, this.bm.getUserPokemonPacket().pokemonID, false));
    }
}

