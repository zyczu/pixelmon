/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.elements;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.elements.GuiTextScrollBar;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

public class GuiTextFieldMultipleLine
extends GuiTextField {
    final FontRenderer fontRendererInstance;
    private int enabledColor = 0xE0E0E0;
    private int cursorCounter;
    List<String> splitString = new ArrayList<String>();
    private GuiTextScrollBar scrollBar;
    private int mouseX;
    private int mouseY;
    private boolean scrollToCursor;

    public GuiTextFieldMultipleLine(int componentID, int x, int y, int width, int height) {
        super(componentID, Minecraft.getMinecraft().fontRenderer, x, y, width, height);
        Minecraft mc = Minecraft.getMinecraft();
        this.fontRendererInstance = mc.fontRenderer;
        this.setMaxStringLength(Integer.MAX_VALUE);
        this.scrollBar = new GuiTextScrollBar(this);
    }

    @Override
    public void updateCursorCounter() {
        super.updateCursorCounter();
        ++this.cursorCounter;
    }

    @Override
    public void writeText(String addText) {
        addText.replace("\r\n", "\n").replace("\r", "\n");
        if (addText.contains("\n")) {
            String newText = "";
            int cursorPosition = this.getCursorPosition();
            int selectionEnd = this.getSelectionEnd();
            String text = this.getText();
            int start = cursorPosition < selectionEnd ? cursorPosition : selectionEnd;
            int end = cursorPosition < selectionEnd ? selectionEnd : cursorPosition;
            int length = 0;
            if (!text.isEmpty()) {
                newText = newText + text.substring(0, start);
            }
            newText = newText + addText;
            length = addText.length();
            if (text.length() > 0 && end < text.length()) {
                newText = newText + text.substring(end);
            }
            this.setText(newText);
            this.setCursorPosition(cursorPosition);
            this.moveCursorBy(start - selectionEnd + length);
        } else {
            super.writeText(addText);
        }
    }

    @Override
    public boolean mouseClicked(int x, int y, int buttonClicked) {
        boolean retur = super.mouseClicked(x, y, buttonClicked);
        boolean mouseInTextField = x >= this.x && x < this.x + this.width && y >= this.y && y < this.y + this.height;
        boolean bl = mouseInTextField;
        if (this.isFocused() && mouseInTextField && buttonClicked == 0) {
            int lineOffset = this.fontRendererInstance.FONT_HEIGHT + 2;
            int cursorLine = (y - this.y - 4) / lineOffset + this.scrollBar.getTopIndex();
            this.moveCursor(cursorLine, x);
        }
        return retur;
    }

    private void moveCursor(int line, int x) {
        if (line < 0) {
            this.setCursorPositionZero();
        } else if (line >= this.splitString.size()) {
            this.setCursorPositionEnd();
        } else {
            int newPosition = 0;
            for (int i = 0; i < line; ++i) {
                newPosition += this.splitString.get(i).length();
            }
            String currentLine = this.splitString.get(line);
            int currentWidth = 0;
            int mouseXOffset = x - this.x - 4;
            if (mouseXOffset > 0) {
                int linePosition = -1;
                int currentLineLength = currentLine.length();
                for (int i = 0; i < currentLineLength; ++i) {
                    if (currentWidth > mouseXOffset) {
                        linePosition = i - 1;
                        break;
                    }
                    currentWidth += this.fontRendererInstance.getCharWidth(currentLine.charAt(i));
                }
                if (linePosition == -1) {
                    linePosition = !currentLine.isEmpty() && currentLine.charAt(currentLineLength - 1) == '\n' ? currentLineLength - 1 : currentLineLength;
                }
                newPosition += linePosition;
            }
            this.setCursorPosition(newPosition);
        }
        this.scrollToCursor = true;
    }

    @Override
    public boolean textboxKeyTyped(char key, int keyCode) {
        this.scrollToCursor = true;
        if (key == '\r' || key == '\n' || keyCode == 156) {
            this.writeText("\n");
            return true;
        }
        if (keyCode == 200 || keyCode == 208) {
            int[] cursorStringData = this.getSplitStringPosition(this.getCursorPosition());
            int cursorLine = cursorStringData[0];
            int cursorX = this.getInitialXPosition() + cursorStringData[1];
            if (keyCode == 200) {
                this.moveCursor(cursorLine - 1, cursorX);
            } else {
                this.moveCursor(cursorLine + 1, cursorX);
            }
            return true;
        }
        return super.textboxKeyTyped(key, keyCode);
    }

    public void drawTextBox(int mouseX, int mouseY) {
        this.mouseX = mouseX;
        this.mouseY = mouseY;
        this.drawTextBox();
    }

    private int getInitialXPosition() {
        return this.getEnableBackgroundDrawing() ? this.x + 4 : this.x;
    }

    int getLineHeight() {
        return this.fontRendererInstance.FONT_HEIGHT + 2;
    }

    @Override
    public void drawTextBox() {
        if (this.getVisible()) {
            boolean enableBackgroundDrawing = this.getEnableBackgroundDrawing();
            int cursorPosition = this.getCursorPosition();
            int selectionEnd = this.getSelectionEnd();
            String text = this.getText();
            boolean isFocused = this.isFocused();
            if (enableBackgroundDrawing) {
                GuiTextFieldMultipleLine.drawRect(this.x - 1, this.y - 1, this.x + this.width + 1, this.y + this.height + 1, -6250336);
                GuiTextFieldMultipleLine.drawRect(this.x, this.y, this.x + this.width, this.y + this.height, -16777216);
            }
            int textColor = this.enabledColor;
            boolean cursorInBox = cursorPosition >= 0 && cursorPosition <= text.length();
            boolean cursorVisible = isFocused && this.cursorCounter / 6 % 2 == 0 && cursorInBox;
            int currentXPosition = this.getInitialXPosition();
            int currentYPosition = this.y + this.fontRendererInstance.FONT_HEIGHT;
            if (enableBackgroundDrawing) {
                currentYPosition -= 6;
            }
            int initialYPosition = currentYPosition;
            int cursorX = currentXPosition;
            selectionEnd = Math.min(selectionEnd, text.length());
            int maxWidth = enableBackgroundDrawing ? this.width - 8 : this.width;
            int lineOffset = this.getLineHeight();
            this.splitString = GuiHelper.splitString(text, maxWidth);
            int[] cursorStringData = this.getSplitStringPosition(cursorPosition);
            int cursorLine = cursorStringData[0];
            int endSelectionLine = -1;
            int endSelectionX = 0;
            if (selectionEnd != cursorPosition) {
                int[] endSelectionData = this.getSplitStringPosition(selectionEnd);
                endSelectionLine = endSelectionData[0];
                endSelectionX = endSelectionData[1];
            }
            cursorX += cursorStringData[1];
            if (this.scrollToCursor) {
                if (endSelectionLine > -1 && endSelectionLine != cursorLine) {
                    this.scrollBar.scrollTo(endSelectionLine);
                } else {
                    this.scrollBar.scrollTo(cursorLine);
                }
                this.scrollToCursor = false;
            }
            int topIndex = this.scrollBar.getTopIndex();
            int bottomIndex = this.scrollBar.getBottomIndex();
            for (int i = topIndex; i <= bottomIndex; ++i) {
                this.fontRendererInstance.drawStringWithShadow(GuiHelper.removeNewLine(this.splitString.get(i)), currentXPosition, currentYPosition, textColor);
                currentYPosition += lineOffset;
            }
            currentYPosition -= lineOffset;
            int cursorYPosition = initialYPosition + (cursorLine - topIndex) * lineOffset;
            boolean drawCursorBar = cursorPosition < text.length() || text.length() >= this.getMaxStringLength();
            int selectionStartX = cursorX;
            if (!cursorInBox) {
                selectionStartX = cursorPosition > 0 ? currentXPosition + this.width : currentXPosition;
                int n = selectionStartX;
            }
            if (cursorVisible && cursorLine >= topIndex && cursorLine <= bottomIndex) {
                if (drawCursorBar) {
                    Gui.drawRect(selectionStartX - 1, cursorYPosition - 1, selectionStartX, cursorYPosition + 1 + this.fontRendererInstance.FONT_HEIGHT, -3092272);
                } else {
                    this.fontRendererInstance.drawStringWithShadow("_", selectionStartX, currentYPosition, textColor);
                }
            }
            if (selectionEnd != cursorPosition) {
                int i;
                if (endSelectionLine > cursorLine) {
                    cursorLine = Math.max(cursorLine, topIndex);
                    endSelectionLine = Math.min(endSelectionLine, bottomIndex + 1);
                    for (i = cursorLine; i <= endSelectionLine; ++i) {
                        int left = i == cursorLine ? selectionStartX : currentXPosition;
                        int right = currentXPosition + (i == endSelectionLine ? endSelectionX : this.fontRendererInstance.getStringWidth(GuiHelper.removeNewLine(this.splitString.get(i))));
                        currentYPosition = initialYPosition + (i - topIndex) * lineOffset;
                        this.drawCursorVertical(right, currentYPosition - 1, left, currentYPosition + 1 + this.fontRendererInstance.FONT_HEIGHT);
                    }
                } else {
                    endSelectionLine = Math.max(endSelectionLine, topIndex);
                    cursorLine = Math.min(cursorLine, bottomIndex + 1);
                    for (i = endSelectionLine; i <= cursorLine; ++i) {
                        int left = currentXPosition;
                        if (i == endSelectionLine) {
                            left += endSelectionX;
                        }
                        int right = i == cursorLine ? selectionStartX : currentXPosition + this.fontRendererInstance.getStringWidth(GuiHelper.removeNewLine(this.splitString.get(i)));
                        currentYPosition = initialYPosition + (i - topIndex) * lineOffset;
                        this.drawCursorVertical(right, currentYPosition - 1, left, currentYPosition + 1 + this.fontRendererInstance.FONT_HEIGHT);
                    }
                }
            }
            this.scrollBar.drawScreen(this.mouseX, this.mouseY, 0.0f);
        }
    }

    private int[] getSplitStringPosition(int position) {
        int currentPosition = 0;
        int numLines = this.splitString.size();
        int positionLine = numLines - 1;
        int positionOffset = -1;
        for (int i = 0; i < numLines; ++i) {
            int newPosition = currentPosition + this.splitString.get(i).length();
            if (newPosition > position) {
                positionLine = i;
                positionOffset = position - currentPosition;
                break;
            }
            currentPosition = newPosition;
        }
        if (positionOffset == -1) {
            positionOffset = this.splitString.get(positionLine).length();
        }
        int lineWidth = this.fontRendererInstance.getStringWidth(GuiHelper.removeNewLine(this.splitString.get(positionLine).substring(0, positionOffset)));
        return new int[]{positionLine, lineWidth};
    }

    private void drawCursorVertical(int right, int top, int left, int bottom) {
        if (right < left) {
            int i = right;
            right = left;
            left = i;
        }
        if (top < bottom) {
            int j = top;
            top = bottom;
            bottom = j;
        }
        if (left != right) {
            --left;
        }
        if (left > this.x + this.width) {
            left = this.x + this.width;
        }
        if (right > this.x + this.width) {
            right = this.x + this.width;
        }
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder vertexBuffer = tessellator.getBuffer();
        GlStateManager.color(0.0f, 0.0f, 255.0f, 255.0f);
        GlStateManager.disableTexture2D();
        GlStateManager.enableColorLogic();
        GlStateManager.colorLogicOp(5387);
        vertexBuffer.begin(7, DefaultVertexFormats.POSITION);
        vertexBuffer.pos(right, bottom, 0.0).endVertex();
        vertexBuffer.pos(left, bottom, 0.0).endVertex();
        vertexBuffer.pos(left, top, 0.0).endVertex();
        vertexBuffer.pos(right, top, 0.0).endVertex();
        tessellator.draw();
        GlStateManager.disableColorLogic();
        GlStateManager.enableTexture2D();
    }

    @Override
    public void setTextColor(int newColor) {
        super.setTextColor(newColor);
        this.enabledColor = newColor;
    }
}

