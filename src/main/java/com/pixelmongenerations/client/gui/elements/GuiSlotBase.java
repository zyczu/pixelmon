/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Mouse
 */
package com.pixelmongenerations.client.gui.elements;

import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexFormat;
import org.lwjgl.input.Mouse;

public abstract class GuiSlotBase {
    public final int width;
    public final int height;
    public final int top;
    public final int bottom;
    public final int right;
    public final int left;
    protected int slotHeight;
    static final int DEFAULT_SLOT_HEIGHT = 10;
    int mouseX;
    int mouseY;
    private float initialClickY = -2.0f;
    private float scrollMultiplier;
    public int amountScrolled;
    private int selectedElement = -1;
    private long lastClicked = 0L;
    private final boolean opaque;
    private int hoverSlot = -1;
    private float hoverValue = 0.0f;

    public GuiSlotBase(int top, int left, int width, int height, boolean opaque) {
        this.width = width;
        this.height = height;
        this.top = top;
        this.bottom = this.top + this.height;
        this.slotHeight = 10;
        this.left = left;
        this.right = this.width + this.left;
        this.opaque = opaque;
        this.hoverValue = 255.0f;
    }

    public void setSlotHeight(int height) {
        this.slotHeight = height;
    }

    protected abstract int getSize();

    protected abstract void elementClicked(int var1, boolean var2);

    protected abstract boolean isSelected(int var1);

    protected int getContentHeight() {
        return this.getSize() * this.slotHeight;
    }

    protected abstract void drawSlot(int var1, int var2, int var3, int var4, Tessellator var5);

    protected abstract float[] get1Color();

    protected int[] getSelectionColor() {
        return new int[]{0, 0, 0};
    }

    protected int[] get255Color() {
        float[] color1 = this.get1Color();
        return new int[]{(int)(color1[0] * 255.0f), (int)(color1[1] * 255.0f), (int)(color1[2] * 255.0f)};
    }

    public int func_27256_c(int par1, int par2) {
        if (this.inBounds(par1, par2)) {
            int var4 = this.left + this.width;
            int var5 = par2 - this.top + this.amountScrolled - 4;
            int var6 = var5 / this.slotHeight;
            return par1 >= this.left && par1 <= var4 && var6 >= 0 && var5 >= 0 && var6 < this.getSize() ? var6 : -1;
        }
        return -1;
    }

    public boolean isMouseOver(int element, int par1, int par2) {
        return this.func_27256_c(par1, par2) == element;
    }

    public boolean isElementVisible(int i) {
        return this.getElementPosition(i) == 0;
    }

    public int getTopIndex() {
        return Math.max(0, this.amountScrolled / this.slotHeight);
    }

    public int getBottomIndex() {
        return Math.min(this.getTopIndex() + this.height / this.slotHeight - 1, this.getSize() - 1);
    }

    public int getElementPosition(int i) {
        int ti = this.getTopIndex();
        int bi = this.getBottomIndex();
        if (ti > i) {
            return -1;
        }
        if (bi < i) {
            return 1;
        }
        return 0;
    }

    public void scrollTo(int i) {
        int prevAmountScrolled;
        int pos = this.getElementPosition(i);
        while (pos > 0) {
            prevAmountScrolled = this.amountScrolled;
            this.amountScrolled += this.slotHeight;
            this.bindAmountScrolled();
            pos = this.getElementPosition(i);
            if (prevAmountScrolled != this.amountScrolled) continue;
        }
        while (pos < 0) {
            prevAmountScrolled = this.amountScrolled;
            this.amountScrolled -= this.slotHeight;
            this.bindAmountScrolled();
            pos = this.getElementPosition(i);
            if (prevAmountScrolled != this.amountScrolled) continue;
        }
    }

    public void bindAmountScrolled() {
        int var1 = this.getContentHeight() - (this.bottom - this.top - 4);
        if (var1 < 0) {
            var1 /= 2;
        }
        if (this.amountScrolled < 0) {
            this.amountScrolled = 0;
        }
        if (this.amountScrolled > var1) {
            this.amountScrolled = var1;
        }
    }

    boolean inBounds(int x, int y) {
        return x > this.left && x < this.right && y > this.top && y < this.bottom;
    }

    boolean inBoundsScroll(int x, int y) {
        return x > this.left && x < this.right + 6 && y > this.top && y < this.bottom;
    }

    public void drawScreen(int mousePosX, int mousePosY, float par3) {
        int middleOfSelect;
        int topLeftOfSelect;
        int index;
        int scrollPosY;
        int scrollPosX;
        GlStateManager.pushMatrix();
        this.mouseX = mousePosX;
        this.mouseY = mousePosY;
        int length = this.getSize();
        int posScrollBar = this.left + this.width;
        int posScrollBar2 = posScrollBar + 6;
        GlStateManager.disableDepth();
        GlStateManager.depthMask(false);
        GlStateManager.blendFunc(770, 771);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.enableBlend();
        GlStateManager.enableLighting();
        Minecraft mc = Minecraft.getMinecraft();
        mc.entityRenderer.setupOverlayRendering();
        RenderHelper.enableGUIStandardItemLighting();
        if (Mouse.isButtonDown((int)0)) {
            if (this.initialClickY == -1.0f) {
                boolean flag = true;
                if (mousePosY >= this.top && mousePosY <= this.bottom) {
                    scrollPosX = this.right;
                    scrollPosY = mousePosY - this.top + this.amountScrolled - 4;
                    index = scrollPosY / this.slotHeight;
                    if (this.inBounds(mousePosX, mousePosY) && index >= 0 && scrollPosY >= 0 && index < length) {
                        boolean var12 = index == this.selectedElement && System.currentTimeMillis() - this.lastClicked < 250L;
                        this.elementClicked(index, var12);
                        this.selectedElement = index;
                        this.lastClicked = System.currentTimeMillis();
                    } else if (mousePosX >= this.left && mousePosX <= scrollPosX && scrollPosY < 0) {
                        flag = false;
                    }
                    if (mousePosX >= posScrollBar && mousePosX <= posScrollBar2) {
                        this.scrollMultiplier = -1.0f;
                        topLeftOfSelect = this.getContentHeight() - (this.bottom - this.top - 4);
                        if (topLeftOfSelect < 1) {
                            topLeftOfSelect = 1;
                        }
                        if ((middleOfSelect = (int)((float)((this.bottom - this.top) * (this.bottom - this.top)) / (float)this.getContentHeight())) < 32) {
                            middleOfSelect = 32;
                        }
                        if (middleOfSelect > this.bottom - this.top - 8) {
                            middleOfSelect = this.bottom - this.top - 8;
                        }
                        this.scrollMultiplier /= (float)(this.bottom - this.top - middleOfSelect) / (float)topLeftOfSelect;
                    } else {
                        this.scrollMultiplier = 1.0f;
                    }
                    this.initialClickY = flag ? (float)mousePosY : -2.0f;
                } else {
                    this.initialClickY = -2.0f;
                }
            } else if (this.initialClickY >= 0.0f && this.inBoundsScroll(mousePosX, mousePosY)) {
                this.amountScrolled = (int)((float)this.amountScrolled - ((float)mousePosY - this.initialClickY) * this.scrollMultiplier);
                this.initialClickY = mousePosY;
            }
        } else if (this.inBoundsScroll(mousePosX, mousePosY)) {
            try {
                while (!mc.gameSettings.touchscreen && Mouse.next()) {
                    int scroll = Mouse.getEventDWheel();
                    if (scroll != 0) {
                        scroll = scroll > 0 ? -1 : 1;
                        this.amountScrolled += scroll * this.slotHeight;
                    }
                    mc.currentScreen.handleMouseInput();
                }
                this.initialClickY = -1.0f;
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.bindAmountScrolled();
        GlStateManager.disableLighting();
        GlStateManager.disableFog();
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder vertexBuffer = tessellator.getBuffer();
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        scrollPosX = this.left;
        scrollPosY = this.top + 4 - this.amountScrolled;
        this.drawBackground();
        boolean hovering = false;
        for (index = 0; index < length; ++index) {
            topLeftOfSelect = scrollPosY + index * this.slotHeight;
            middleOfSelect = this.slotHeight - 4;
            if (topLeftOfSelect + 6 > this.bottom || topLeftOfSelect + middleOfSelect - 8 < this.top) continue;
            if (this.isHighlighted(index)) {
                if (this.hoverSlot != index) {
                    this.hoverValue = 255.0f;
                }
                this.drawSelection(index, scrollPosX, middleOfSelect, topLeftOfSelect);
                this.hoverSlot = index;
                hovering = true;
            }
            this.drawSlot(index, scrollPosX, topLeftOfSelect, middleOfSelect, tessellator);
        }
        if (!hovering) {
            this.hoverValue = 255.0f;
        }
        GlStateManager.disableDepth();
        int var20 = 4;
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(770, 771);
        GlStateManager.disableAlpha();
        GlStateManager.shadeModel(7425);
        GlStateManager.disableTexture2D();
        int[] color = this.get255Color();
        if (color != null) {
            vertexBuffer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
            vertexBuffer.pos(this.left, this.top + var20, 0.0).tex(0.0, 1.0).color(color[0], color[1], color[2], 0).endVertex();
            vertexBuffer.pos(this.right, this.top + var20, 0.0).tex(1.0, 1.0).color(color[0], color[1], color[2], 0).endVertex();
            vertexBuffer.pos(this.right, this.top, 0.0).tex(1.0, 0.0).color(color[0], color[1], color[2], 255).endVertex();
            vertexBuffer.pos(this.left, this.top, 0.0).tex(0.0, 0.0).color(color[0], color[1], color[2], 255).endVertex();
            tessellator.draw();
            vertexBuffer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
            vertexBuffer.pos(this.left, (double)this.bottom + 2.0, 0.0).tex(0.0, 1.0).color(color[0], color[1], color[2], 255).endVertex();
            vertexBuffer.pos(this.right, (double)this.bottom + 2.0, 0.0).tex(1.0, 1.0).color(color[0], color[1], color[2], 255).endVertex();
            vertexBuffer.pos(this.right, this.bottom + 2 - var20, 0.0).tex(1.0, 0.0).color(color[0], color[1], color[2], 0).endVertex();
            vertexBuffer.pos(this.left, this.bottom + 2 - var20, 0.0).tex(0.0, 0.0).color(color[0], color[1], color[2], 0).endVertex();
            tessellator.draw();
        }
        if ((topLeftOfSelect = this.getContentHeight() - (this.bottom - this.top - 4)) > 0) {
            int leftish;
            middleOfSelect = (this.bottom - this.top) * (this.bottom - this.top) / this.getContentHeight();
            if (middleOfSelect < 32) {
                middleOfSelect = 32;
            }
            if (middleOfSelect > this.bottom - this.top - 8) {
                middleOfSelect = this.bottom - this.top - 8;
            }
            if ((leftish = this.amountScrolled * (this.bottom - this.top - middleOfSelect) / topLeftOfSelect + this.top) < this.top) {
                leftish = this.top;
            }
            vertexBuffer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
            vertexBuffer.pos(posScrollBar, (double)this.bottom + 2.0, 0.0).tex(0.0, 1.0).color(0, 0, 0, 255).endVertex();
            vertexBuffer.pos(posScrollBar2, (double)this.bottom + 2.0, 0.0).tex(1.0, 1.0).color(0, 0, 0, 255).endVertex();
            vertexBuffer.pos(posScrollBar2, (double)this.top + 2.0, 0.0).tex(1.0, 0.0).color(0, 0, 0, 255).endVertex();
            vertexBuffer.pos(posScrollBar, (double)this.top + 2.0, 0.0).tex(0.0, 0.0).color(0, 0, 0, 255).endVertex();
            tessellator.draw();
            vertexBuffer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
            vertexBuffer.pos(posScrollBar, leftish + middleOfSelect + 2, 0.0).tex(0.0, 1.0).color(128, 128, 128, 255).endVertex();
            vertexBuffer.pos(posScrollBar2, leftish + middleOfSelect + 2, 0.0).tex(1.0, 1.0).color(128, 128, 128, 255).endVertex();
            vertexBuffer.pos(posScrollBar2, (double)leftish + 2.0, 0.0).tex(1.0, 0.0).color(128, 128, 128, 255).endVertex();
            vertexBuffer.pos(posScrollBar, (double)leftish + 2.0, 0.0).tex(0.0, 0.0).color(128, 128, 128, 255).endVertex();
            tessellator.draw();
            vertexBuffer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
            vertexBuffer.pos(posScrollBar, leftish + middleOfSelect - 1 + 2, 0.0).tex(0.0, 1.0).color(192, 192, 192, 255).endVertex();
            vertexBuffer.pos(posScrollBar2 - 1, leftish + middleOfSelect - 1 + 2, 0.0).tex(1.0, 1.0).color(192, 192, 192, 255).endVertex();
            vertexBuffer.pos(posScrollBar2 - 1, (double)leftish + 2.0, 0.0).tex(1.0, 0.0).color(192, 192, 192, 255).endVertex();
            vertexBuffer.pos(posScrollBar, (double)leftish + 2.0, 0.0).tex(0.0, 0.0).color(192, 192, 192, 255).endVertex();
            tessellator.draw();
        }
        GlStateManager.enableTexture2D();
        GlStateManager.enableLighting();
        GlStateManager.shadeModel(7424);
        GlStateManager.enableAlpha();
        GlStateManager.disableBlend();
        GlStateManager.popMatrix();
    }

    private float clampf(float val, float min, float max) {
        if (val < min) {
            val = min;
        }
        if (val > max) {
            val = max;
        }
        return val;
    }

    boolean hasScrollBar() {
        return this.getContentHeight() - (this.bottom - this.top - 4) > 0;
    }

    protected void drawBackground() {
    }

    protected boolean isHighlighted(int index) {
        return this.isSelected(index) || this.isMouseOver(index, this.mouseX, this.mouseY);
    }

    protected void drawSelection(int index, int scrollPosX, int middleOfSelect, int topLeftOfSelect) {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder vertexBuffer = tessellator.getBuffer();
        int leftish = this.left;
        int rightish = this.left + this.width;
        float[] color = this.get1Color();
        this.hoverValue = this.clampf(this.hoverValue -= 5.0f, 1.0f, 255.0f);
        GlStateManager.color(color[0], color[1], color[2], 255.0f);
        GlStateManager.disableTexture2D();
        VertexFormat vertexFormat = this.opaque ? DefaultVertexFormats.POSITION_TEX_COLOR : DefaultVertexFormats.POSITION_TEX;
        vertexBuffer.begin(7, vertexFormat);
        if (this.opaque) {
            vertexBuffer.pos(80.0, topLeftOfSelect + middleOfSelect + 2, 0.0).tex(0.0, 1.0).color(128, 128, 128, 255).endVertex();
            vertexBuffer.pos(80.0, topLeftOfSelect + middleOfSelect + 2, 0.0).tex(1.0, 1.0).color(128, 128, 128, 255).endVertex();
            vertexBuffer.pos(80.0, topLeftOfSelect - 2, 0.0).tex(1.0, 0.0).color(128, 128, 128, 255).endVertex();
            vertexBuffer.pos(80.0, topLeftOfSelect - 2, 0.0).tex(0.0, 0.0).color(128, 128, 128, 255).endVertex();
        } else {
            vertexBuffer.pos(80.0, topLeftOfSelect + middleOfSelect + 2, 0.0).tex(0.0, 1.0).endVertex();
            vertexBuffer.pos(80.0, topLeftOfSelect + middleOfSelect + 2, 0.0).tex(1.0, 1.0).endVertex();
            vertexBuffer.pos(80.0, topLeftOfSelect - 2, 0.0).tex(1.0, 0.0).endVertex();
            vertexBuffer.pos(80.0, topLeftOfSelect - 2, 0.0).tex(0.0, 0.0).endVertex();
        }
        if (this.opaque) {
            int[] selectionColor = this.getSelectionColor();
            if (selectionColor != null) {
                vertexBuffer.pos(leftish + 1, topLeftOfSelect + middleOfSelect + 1, 0.0).tex(0.0, 1.0).color((float)selectionColor[0], (float)selectionColor[1], (float)selectionColor[2], this.hoverValue).endVertex();
                vertexBuffer.pos(rightish - 1, topLeftOfSelect + middleOfSelect + 1, 0.0).tex(1.0, 1.0).color((float)selectionColor[0], (float)selectionColor[1], (float)selectionColor[2], this.hoverValue).endVertex();
                vertexBuffer.pos(rightish - 1, topLeftOfSelect - 1, 0.0).tex(1.0, 0.0).color((float)selectionColor[0], (float)selectionColor[1], (float)selectionColor[2], this.hoverValue).endVertex();
                vertexBuffer.pos(leftish + 1, topLeftOfSelect - 1, 0.0).tex(0.0, 0.0).color((float)selectionColor[0], (float)selectionColor[1], (float)selectionColor[2], this.hoverValue).endVertex();
            }
        } else {
            vertexBuffer.pos(leftish + 1, topLeftOfSelect + middleOfSelect + 1, 0.0).tex(0.0, 1.0).endVertex();
            vertexBuffer.pos(rightish - 1, topLeftOfSelect + middleOfSelect + 1, 0.0).tex(1.0, 1.0).endVertex();
            vertexBuffer.pos(rightish - 1, topLeftOfSelect - 1, 0.0).tex(1.0, 0.0).endVertex();
            vertexBuffer.pos(leftish + 1, topLeftOfSelect - 1, 0.0).tex(0.0, 0.0).endVertex();
        }
        tessellator.draw();
        GlStateManager.enableTexture2D();
    }

    public int getMouseOverIndex(int mouseX, int mouseY) {
        for (int i = this.getTopIndex(); i <= this.getBottomIndex(); ++i) {
            if (!this.isMouseOver(i, mouseX, mouseY)) continue;
            return i;
        }
        return -1;
    }

    public int getCenterX() {
        return this.left + this.width / 2;
    }
}

