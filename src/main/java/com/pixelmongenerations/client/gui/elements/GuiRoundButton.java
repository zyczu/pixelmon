/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.elements;

import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import net.minecraft.client.Minecraft;

public class GuiRoundButton {
    private int leftOffset;
    private int topOffset;
    private String text;
    private int width = 100;
    private int height = 20;

    public GuiRoundButton(int leftOffset, int topOffset, String text) {
        this.leftOffset = leftOffset;
        this.topOffset = topOffset;
        this.setText(text);
    }

    public GuiRoundButton(int leftOffset, int topOffset, String text, int width, int height) {
        this(leftOffset, topOffset, text);
        this.width = width;
        this.height = height;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void drawButton(int leftX, int topY, int mouseX, int mouseY, float zLevel) {
        this.drawButton(leftX, topY, mouseX, mouseY, zLevel, false);
    }

    public void drawButton(int leftX, int topY, int mouseX, int mouseY, float zLevel, boolean forceOutline) {
        Minecraft mc = Minecraft.getMinecraft();
        mc.renderEngine.bindTexture(GuiResources.roundedButton);
        int left = leftX + this.leftOffset;
        int top = topY + this.topOffset;
        GuiHelper.drawImageQuad(left, top, this.width, this.height, 0.0, 0.0, 1.0, 1.0, zLevel);
        mc.renderEngine.bindTexture(GuiResources.roundedButtonOver);
        if (forceOutline) {
            int outside = 5;
            GuiHelper.drawImageQuad(left - outside, top - outside, this.width + outside * 2, this.height + outside * 2, 0.0, 0.0, 1.0, 1.0, zLevel);
        } else if (this.isMouseOver(leftX, topY, mouseX, mouseY)) {
            GuiHelper.drawImageQuad(left, top, this.width, this.height, 0.0, 0.0, 1.0, 1.0, zLevel);
        }
        GuiHelper.drawCenteredString(this.text, left + this.width / 2, top + 6, 0xFFFFFF);
    }

    public boolean isMouseOver(int leftX, int topY, int mouseX, int mouseY) {
        int left = leftX + this.leftOffset;
        int top = topY + this.topOffset;
        return mouseX > left && mouseX < left + this.width && mouseY > top && mouseY < top + this.height;
    }
}

