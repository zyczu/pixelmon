/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.gui.pc;

import com.pixelmongenerations.client.gui.pc.GuiPC;
import com.pixelmongenerations.client.gui.pokechecker.GuiRenamePokemon;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeChecker;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeCheckerMoves;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeCheckerStats;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeCheckerWarningLevel;
import com.pixelmongenerations.core.network.PixelmonData;
import net.minecraft.client.gui.GuiButton;

public class GuiScreenPokeCheckerPC
extends GuiScreenPokeChecker {
    private int box;

    public GuiScreenPokeCheckerPC(PixelmonData packet, int box, int index) {
        super(packet, true);
        this.box = box;
    }

    @Override
    public void initGui() {
        super.initGui();
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
    }

    @Override
    public void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 0: {
                GuiPC gui = new GuiPC(this.targetPacket, this.box);
                this.mc.displayGuiScreen(gui);
                break;
            }
            case 1: {
                this.mc.displayGuiScreen(new GuiScreenPokeCheckerMoves(this.targetPacket, true));
                break;
            }
            case 2: {
                this.mc.displayGuiScreen(new GuiScreenPokeCheckerStats(this.targetPacket, true));
                break;
            }
            case 3: {
                this.mc.displayGuiScreen(new GuiRenamePokemon(this.targetPacket, this));
                break;
            }
            case 4: {
                this.mc.displayGuiScreen(new GuiScreenPokeCheckerWarningLevel(this, this.targetPacket));
                break;
            }
            case 5: {
                this.mc.displayGuiScreen(new GuiRenamePokemon(this.targetPacket, this));
            }
        }
    }

    @Override
    public void keyTyped(char c, int i) {
        if (i == 1) {
            GuiPC gui = new GuiPC(this.targetPacket, this.box);
            this.mc.displayGuiScreen(gui);
        }
    }
}

