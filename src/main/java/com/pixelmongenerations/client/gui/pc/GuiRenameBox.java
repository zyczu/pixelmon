/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 *  org.lwjgl.opengl.GL11
 */
package com.pixelmongenerations.client.gui.pc;

import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.pc.GuiPC;
import com.pixelmongenerations.client.gui.pokechecker.GuiRenameButtons;
import com.pixelmongenerations.client.gui.pokechecker.GuiTextFieldTransparent;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.pcServer.ChangeBoxName;
import com.pixelmongenerations.core.storage.PCClientStorage;
import java.io.IOException;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class GuiRenameBox
extends GuiContainer {
    private int box;
    private GuiScreen parentGuiScreen;
    private GuiTextFieldTransparent theGuiTextField;
    private PixelmonData targetPacket;

    public GuiRenameBox(GuiPC pc, int box) {
        super(new ContainerEmpty());
        this.box = box;
        this.parentGuiScreen = pc;
    }

    @Override
    public void initGui() {
        super.initGui();
        Keyboard.enableRepeatEvents((boolean)true);
        this.buttonList.clear();
        this.buttonList.add(new GuiRenameButtons(0, this.width / 2 - 98, this.height / 4 + 80, I18n.translateToLocal("gui.renamePoke.renamebutton")));
        this.buttonList.add(new GuiRenameButtons(1, this.width / 2 + 48, this.height / 4 + 80, I18n.translateToLocal("gui.renamePoke.cancel")));
        this.buttonList.add(new GuiRenameButtons(2, this.width / 2 - 25, this.height / 4 + 80, I18n.translateToLocal("gui.renamePoke.Reset")));
        this.theGuiTextField = new GuiTextFieldTransparent(this.mc.fontRenderer, this.width / 2 - 68, this.height / 4 + 37, 140, 30);
        this.theGuiTextField.setFocused(true);
        this.theGuiTextField.setText(PCClientStorage.getBoxName(this.box));
        this.theGuiTextField.setMaxStringLength(20);
    }

    @Override
    public void updateScreen() {
        this.theGuiTextField.updateCursorCounter();
    }

    @Override
    public void onGuiClosed() {
        Keyboard.enableRepeatEvents((boolean)false);
    }

    @Override
    protected void actionPerformed(GuiButton par1GuiButton) {
        if (!par1GuiButton.enabled) {
            return;
        }
        switch (par1GuiButton.id) {
            case 2: {
                Pixelmon.NETWORK.sendToServer(new ChangeBoxName(this.box, "Box: " + Integer.toString(this.box + 1)));
                this.parentGuiScreen.initGui();
                this.mc.displayGuiScreen(this.parentGuiScreen);
                break;
            }
            case 1: {
                this.mc.displayGuiScreen(this.parentGuiScreen);
                break;
            }
            case 0: {
                Pixelmon.NETWORK.sendToServer(new ChangeBoxName(this.box, this.theGuiTextField.getText()));
                this.parentGuiScreen.initGui();
                this.mc.displayGuiScreen(this.parentGuiScreen);
            }
        }
    }

    @Override
    protected void keyTyped(char par1, int par2) {
        if (par1 == '%') {
            return;
        }
        this.theGuiTextField.textboxKeyTyped(par1, par2);
        ((GuiButton)this.buttonList.get((int)0)).enabled = !this.theGuiTextField.getText().trim().isEmpty();
        boolean bl = ((GuiButton)this.buttonList.get((int)0)).enabled;
        if (par1 == '\r') {
            this.actionPerformed((GuiButton)this.buttonList.get(0));
        }
    }

    @Override
    protected void mouseClicked(int par1, int par2, int par3) throws IOException {
        super.mouseClicked(par1, par2, par3);
        this.theGuiTextField.mouseClicked(par1, par2, par3);
    }

    @Override
    public void drawGuiContainerBackgroundLayer(float par3, int par1, int par2) {
        GL11.glNormal3f((float)0.0f, (float)-1.0f, (float)0.0f);
        this.mc.renderEngine.bindTexture(GuiResources.rename);
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        this.drawTexturedModalRect((this.width - this.xSize) / 2 - 40, this.height / 4, 0, 0, 256, 114);
        this.drawCenteredString(this.mc.fontRenderer, I18n.translateToLocalFormatted("gui.renameBox.renameBox", Integer.toString(this.box + 1)), this.width / 2, this.height / 4 - 60 + 80, 0xFFFFFF);
        this.theGuiTextField.drawTextBox();
    }
}

