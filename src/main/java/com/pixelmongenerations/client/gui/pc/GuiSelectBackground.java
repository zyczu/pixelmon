/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.input.Keyboard
 */
package com.pixelmongenerations.client.gui.pc;

import com.pixelmongenerations.api.pc.ClientBackground;
import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.client.gui.GuiResources;
import com.pixelmongenerations.client.gui.pc.GuiPC;
import com.pixelmongenerations.client.gui.pokechecker.GuiRenameButtons;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.pcServer.ChangeBoxBackground;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.storage.PCClientStorage;
import java.awt.Rectangle;
import java.io.IOException;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.text.translation.I18n;
import org.lwjgl.input.Keyboard;

public class GuiSelectBackground
extends GuiContainer {
    private int box;
    private GuiScreen parentGuiScreen;
    private PixelmonData targetPacket;
    private int index;
    private ClientBackground background;

    public GuiSelectBackground(GuiPC pc, int box) {
        super(new ContainerEmpty());
        this.box = box;
        this.parentGuiScreen = pc;
        String id = PCClientStorage.getBoxBackground(box);
        this.background = PCClientStorage.getBackgroundById(id);
        this.index = PCClientStorage.getIndexFromBackgroundId(id);
    }

    @Override
    public void initGui() {
        super.initGui();
        Keyboard.enableRepeatEvents((boolean)true);
        this.buttonList.clear();
        int xSizeBox = 226;
        int ySizeBox = 207;
        int xSizeParty = 103;
        int ySizeParty = 207;
        int pcBoxTopLeft = this.width / 2 - xSizeBox / 2 + xSizeParty / 2;
        int pcBoxTop = (int)((double)this.height * 0.45 - (double)(ySizeParty / 2));
        this.buttonList.add(new GuiRenameButtons(0, this.width / 2 - 97, pcBoxTop + ySizeParty - 37, I18n.translateToLocal("gui.renamePoke.select")));
        this.buttonList.add(new GuiRenameButtons(1, this.width / 2 + 49, pcBoxTop + ySizeParty - 37, I18n.translateToLocal("gui.renamePoke.cancel")));
        this.buttonList.add(new GuiRenameButtons(2, this.width / 2 - 24, pcBoxTop + ySizeParty - 37, I18n.translateToLocal("gui.renamePoke.Reset")));
    }

    @Override
    public void onGuiClosed() {
        Keyboard.enableRepeatEvents((boolean)false);
    }

    @Override
    protected void actionPerformed(GuiButton par1GuiButton) {
        if (!par1GuiButton.enabled) {
            return;
        }
        switch (par1GuiButton.id) {
            case 2: {
                Pixelmon.NETWORK.sendToServer(new ChangeBoxBackground(this.box, "box_forest"));
                this.parentGuiScreen.initGui();
                this.mc.displayGuiScreen(this.parentGuiScreen);
                break;
            }
            case 1: {
                this.mc.displayGuiScreen(this.parentGuiScreen);
                break;
            }
            case 0: {
                if (this.background.hasUnlocked(null)) {
                    Pixelmon.NETWORK.sendToServer(new ChangeBoxBackground(this.box, this.background.getId()));
                }
                this.parentGuiScreen.initGui();
                this.mc.displayGuiScreen(this.parentGuiScreen);
            }
        }
    }

    @Override
    protected void mouseClicked(int x, int y, int par3) throws IOException {
        super.mouseClicked(x, y, par3);
        int xSizeBox = 226;
        int ySizeBox = 207;
        int xSizeParty = 103;
        int ySizeParty = 207;
        int pcBoxTopLeft = this.width / 2 - xSizeBox / 2 + xSizeParty / 2;
        int pcBoxTop = (int)((double)this.height * 0.45 - (double)(ySizeParty / 2));
        int displaySizeX = (int)((double)xSizeBox / 1.5);
        int displaySizeY = (int)((double)ySizeBox / 1.5);
        if (new Rectangle(this.width / 2 - displaySizeX / 2, pcBoxTop + 28, 17, 22).contains(x, y)) {
            --this.index;
            if (this.index < 0) {
                this.index = PCClientStorage.getBackgroundCount() - 1;
            }
            this.background = PCClientStorage.getBackgroundFromIndex(this.index);
        } else if (new Rectangle(this.width / 2 - displaySizeX / 2 + displaySizeX - 17, pcBoxTop + 28, 17, 22).contains(x, y)) {
            ++this.index;
            if (this.index >= PCClientStorage.getBackgroundCount()) {
                this.index = 0;
            }
            this.background = PCClientStorage.getBackgroundFromIndex(this.index);
        }
    }

    @Override
    public void drawGuiContainerBackgroundLayer(float par3, int par1, int par2) {
        ((GuiRenameButtons)this.buttonList.get((int)0)).enabled = this.background.hasUnlocked(null);
        int xSizeBox = 226;
        int ySizeBox = 207;
        int xSizeParty = 103;
        int ySizeParty = 207;
        int pcBoxTopLeft = this.width / 2 - xSizeBox / 2 + xSizeParty / 2;
        int pcBoxTop = (int)((double)this.height * 0.45 - (double)(ySizeParty / 2));
        if (this.background.hasUnlocked(null)) {
            this.mc.renderEngine.bindTexture(GuiResources.pcSelectBackground);
        } else {
            this.mc.renderEngine.bindTexture(GuiResources.pcSelectBackgroundLocked);
        }
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GuiHelper.drawImageQuad(pcBoxTopLeft - xSizeParty, (double)this.height * 0.45 - (double)(ySizeParty / 2), xSizeBox + xSizeParty + 1, ySizeBox, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        this.drawCenteredString(this.mc.fontRenderer, I18n.translateToLocalFormatted("gui.renameBox.background", this.background.hasUnlocked(null) ? this.background.getName() : this.background.getName() + " (LOCKED)"), this.width / 2, pcBoxTop + 15, 0xFFFFFF);
        String id = this.background.getId();
        TextureResource texture = ClientProxy.TEXTURE_STORE.getObject(id);
        if (texture != null) {
            texture.bindTexture();
        } else {
            this.mc.renderEngine.bindTexture(GuiResources.background(id));
        }
        int displaySizeX = (int)((double)xSizeBox / 1.5);
        int displaySizeY = (int)((double)ySizeBox / 1.5);
        GuiHelper.drawImageQuad(this.width / 2 - displaySizeX / 2, pcBoxTop + 28, displaySizeX, displaySizeY, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        if (!this.background.hasUnlocked(null)) {
            this.mc.renderEngine.bindTexture(GuiResources.padlock);
            GuiHelper.drawImageQuad(this.width / 2 - displaySizeX / 2 + displaySizeX / 2 - 26, pcBoxTop + 28 + displaySizeY / 2 - 14, 52.0, 52.0f, 0.0, 0.0, 1.0, 1.0, this.zLevel);
        }
    }
}

