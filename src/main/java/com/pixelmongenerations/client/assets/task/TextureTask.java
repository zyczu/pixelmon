/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.assets.task;

import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.data.asset.AssetState;
import com.pixelmongenerations.core.data.asset.ManifestTask;
import com.pixelmongenerations.core.data.asset.entry.TextureAsset;
import com.pixelmongenerations.core.data.asset.manifest.TextureManifest;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import net.minecraft.client.Minecraft;

public class TextureTask
extends TimerTask
implements ManifestTask<TextureManifest> {
    private ExecutorService executorService = Executors.newFixedThreadPool(10);
    private ConcurrentLinkedQueue<TextureResource> buffer;
    private ConcurrentLinkedQueue<TextureResource> cacheQueue;
    private int waitTime = 10;

    @Override
    public void formBuffer(TextureManifest manifest) {
        this.buffer = new ConcurrentLinkedQueue();
        this.cacheQueue = new ConcurrentLinkedQueue();
        for (TextureAsset entry : manifest.getAssets()) {
            if (entry.textureId == null || entry.textureName == null) {
                Pixelmon.LOGGER.error("Prevented Invalid TextureEntry: " + entry);
                continue;
            }
            this.buffer.add(new TextureResource(entry));
        }
        Pixelmon.LOGGER.info(String.format("Running task for %s textures(s) from manifest.", this.getRemainingItems()));
        while (this.buffer.peek() != null) {
            TextureResource resource = this.buffer.poll();
            this.executorService.submit(() -> {
                try {
                    resource.download();
                    this.cacheQueue.add(resource);
                }
                catch (Exception e) {
                    Pixelmon.LOGGER.error("Exception when downloading TextureResource: " + resource.entry);
                    e.printStackTrace();
                }
            });
        }
    }

    @Override
    public int getRemainingItems() {
        return this.buffer.size();
    }

    @Override
    public void run() {
        if (Minecraft.getMinecraft().player == null || Minecraft.getMinecraft().player.connection == null) {
            this.buffer.clear();
            this.executorService.shutdown();
            this.cancel();
            Pixelmon.LOGGER.info("Shutdown TextureTask due to leaving the server.");
            return;
        }
        if (this.buffer.isEmpty() && this.cacheQueue.isEmpty()) {
            if (this.waitTime == 0) {
                this.cancel();
                this.executorService.shutdown();
                Pixelmon.LOGGER.info("Completed loading of the texture manifest.");
                return;
            }
            --this.waitTime;
        } else {
            this.waitTime = 10;
        }
        while (this.cacheQueue.peek() != null) {
            TextureResource resource = this.cacheQueue.peek();
            if (resource.getState() == AssetState.Downloaded || resource.getState() == AssetState.Loaded || resource.getState() == AssetState.Loading) {
                ClientProxy.TEXTURE_STORE.addObject(resource.getKey(), resource);
            }
            if (!resource.getState().shouldRemove()) continue;
            this.cacheQueue.poll();
        }
    }
}

