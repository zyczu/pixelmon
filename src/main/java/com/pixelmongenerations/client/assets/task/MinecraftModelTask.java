/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.assets.task;

import com.pixelmongenerations.client.assets.resource.MinecraftModelResource;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.data.asset.AssetState;
import com.pixelmongenerations.core.data.asset.ManifestTask;
import com.pixelmongenerations.core.data.asset.entry.MinecraftModelAsset;
import com.pixelmongenerations.core.data.asset.manifest.MinecraftModelManifest;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import net.minecraft.client.Minecraft;

public class MinecraftModelTask
extends TimerTask
implements ManifestTask<MinecraftModelManifest> {
    private ExecutorService executorService = Executors.newFixedThreadPool(10);
    private ConcurrentLinkedQueue<MinecraftModelResource> buffer;
    private ConcurrentLinkedQueue<MinecraftModelResource> cacheQueue;
    private int waitTime = 10;

    @Override
    public void formBuffer(MinecraftModelManifest manifest) {
        this.buffer = new ConcurrentLinkedQueue();
        this.cacheQueue = new ConcurrentLinkedQueue();
        for (MinecraftModelAsset entry : manifest.getAssets()) {
            if (entry.modelId == null || entry.modelName == null) {
                Pixelmon.LOGGER.error("Prevented Invalid ModelEntry: " + entry);
                continue;
            }
            this.buffer.add(new MinecraftModelResource(entry));
        }
        Pixelmon.LOGGER.info(String.format("Running task for %s model(s) from manifest.", this.getRemainingItems()));
        while (this.buffer.peek() != null) {
            MinecraftModelResource resource = this.buffer.poll();
            this.executorService.submit(() -> {
                try {
                    resource.download();
                    this.cacheQueue.add(resource);
                }
                catch (Exception e) {
                    Pixelmon.LOGGER.error("Exception when downloading Minecraft Model Asset: " + resource.entry);
                    e.printStackTrace();
                }
            });
        }
    }

    @Override
    public int getRemainingItems() {
        return this.buffer.size();
    }

    @Override
    public void run() {
        if (Minecraft.getMinecraft().player == null || Minecraft.getMinecraft().player.connection == null) {
            this.buffer.clear();
            this.executorService.shutdown();
            this.cancel();
            Pixelmon.LOGGER.info("Shutdown minecraft model task due to leaving the server.");
            return;
        }
        if (this.buffer.isEmpty() && this.cacheQueue.isEmpty()) {
            if (this.waitTime == 0) {
                this.cancel();
                this.executorService.shutdown();
                Pixelmon.LOGGER.info("Completed loading of the minecraft model manifest.");
                return;
            }
            --this.waitTime;
        } else {
            this.waitTime = 10;
        }
        while (this.cacheQueue.peek() != null) {
            MinecraftModelResource resource = this.cacheQueue.peek();
            if (resource.getState() == AssetState.Downloaded || resource.getState() == AssetState.Loading || resource.getState() == AssetState.Loaded) {
                ClientProxy.MINECRAFT_MODEL_STORE.addObject(resource.getKey(), resource);
            }
            if (!resource.getState().shouldRemove()) continue;
            this.cacheQueue.poll();
        }
    }
}

