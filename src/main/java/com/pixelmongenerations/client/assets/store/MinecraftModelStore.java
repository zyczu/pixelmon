/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client.assets.store;

import com.pixelmongenerations.client.assets.resource.MinecraftModelResource;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.data.asset.AssetState;
import com.pixelmongenerations.core.data.asset.ObjectStore;
import com.pixelmongenerations.core.data.asset.entry.MinecraftModelAsset;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class MinecraftModelStore
implements ObjectStore<MinecraftModelResource> {
    private static Map<String, MinecraftModelResource> modelStore = new HashMap<String, MinecraftModelResource>();

    @Override
    public void addObject(String key, MinecraftModelResource texture) {
        modelStore.put(key, texture);
    }

    @Override
    public MinecraftModelResource getObject(String key) {
        MinecraftModelResource resource = modelStore.get(key);
        if (resource != null) {
            if (resource.getState() == AssetState.Downloaded) {
                ClientProxy.RESOURCE_MANAGEMENT_TASK.queueLoad(resource);
            }
            resource.lastAccessed = Instant.now();
        }
        return resource;
    }

    @Override
    public void checkForExpiredObjects() {
        Instant cleanupPoint = Instant.now().minusSeconds(900L);
        int count = 0;
        for (MinecraftModelResource resource : modelStore.values()) {
            if (((MinecraftModelAsset)resource.entry).preLoad || resource.getState() != AssetState.Loaded || !resource.lastAccessed.isBefore(cleanupPoint)) continue;
            resource.unload();
            ++count;
        }
        if (count > 0) {
            Pixelmon.LOGGER.info("[MinecraftModelStore] Unloaded " + count + " recently unused resources.");
        }
    }
}

