/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.client;

import com.pixelmongenerations.api.board.DisplayBoard;
import java.util.ArrayList;

public class ClientBoardManager {
    private ArrayList<DisplayBoard> boards = new ArrayList();
    private static ClientBoardManager INSTANCE;

    public static ClientBoardManager instance() {
        if (INSTANCE == null) {
            INSTANCE = new ClientBoardManager();
        }
        return INSTANCE;
    }

    public ArrayList<DisplayBoard> boards() {
        return this.boards;
    }

    public void insert(DisplayBoard board) {
        this.boards.add(board);
    }
}

