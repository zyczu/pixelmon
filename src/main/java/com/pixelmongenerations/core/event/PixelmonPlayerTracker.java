/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.event;

import com.pixelmongenerations.common.battle.BattleQuery;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.rules.teamselection.TeamSelectionList;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemQueryList;
import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQuery;
import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQueryList;
import com.pixelmongenerations.common.starter.SelectPokemonController;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumMegaItem;
import com.pixelmongenerations.core.enums.battle.EnumBattleEndCause;
import com.pixelmongenerations.core.event.EntityPlayerExtension;
import com.pixelmongenerations.core.network.packetHandlers.ServerConfigList;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.UpdateClientRules;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerNotLoadedException;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.MoveCostList;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

public class PixelmonPlayerTracker {
    @SubscribeEvent
    public void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event) {
        EntityPlayerMP player = (EntityPlayerMP)event.player;
        PixelmonPlayerTracker.removePlayer(player);
        Pixelmon.NETWORK.sendTo(new ServerConfigList(), player);
        try {
            PixelmonStorage.pokeBallManager.refreshPlayerStorage(player);
            PixelmonStorage.computerManager.refreshComputerStorage(player);
        }
        catch (PlayerNotLoadedException var5) {
            var5.printStackTrace();
        }
        if (PixelmonConfig.alwaysHaveMegaRing) {
            try {
                PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage(player).get();
                if (storage.megaData.getMegaItem() == EnumMegaItem.Disabled || storage.megaData.getMegaItem() == EnumMegaItem.None) {
                    storage.megaData.setMegaItem(EnumMegaItem.BraceletORAS, false);
                    EntityPlayerExtension.updatePlayerMegaItem(player, EnumMegaItem.BraceletORAS);
                }
            }
            catch (Exception var6) {
                var6.printStackTrace();
            }
        }
        Pixelmon.NETWORK.sendTo(new UpdateClientRules(), player);
    }

    @SubscribeEvent
    public void onPlayerLogout(PlayerEvent.PlayerLoggedOutEvent event) {
        EntityPlayer player = event.player;
        if (player instanceof EntityPlayerMP) {
            PixelmonPlayerTracker.removePlayer((EntityPlayerMP)player);
            try {
                PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID(player.getUniqueID()).ifPresent(PlayerStorage::recallAllPokemon);
            }
            catch (NullPointerException nullPointerException) {
                // empty catch block
            }
        }
    }

    public static void removePlayer(EntityPlayerMP player) {
        try {
            EvolutionQuery eq;
            SelectPokemonController.removePlayer(player);
            BattleQuery bq = BattleQuery.getQuery(player);
            if (bq != null) {
                bq.declineQuery(player);
            }
            if ((eq = EvolutionQueryList.get(player)) != null) {
                EvolutionQueryList.declineQuery(player, eq.pokemonID);
            }
            DropItemQueryList.removeQuery(player);
            BattleControllerBase bc = BattleRegistry.getBattle(player);
            if (bc != null) {
                if (bc.hasSpectator(player)) {
                    bc.removeSpectator(player);
                } else {
                    bc.endBattle(EnumBattleEndCause.FORCE);
                }
            }
            MoveCostList.removeEntry(player);
            TeamSelectionList.removeSelection(player);
        }
        catch (Exception var4) {
            var4.printStackTrace();
        }
    }
}

