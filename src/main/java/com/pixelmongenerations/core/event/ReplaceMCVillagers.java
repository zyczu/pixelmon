/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.event;

import com.pixelmongenerations.common.entity.npcs.NPCChatting;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.BaseTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.GeneralNPCData;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumEncounterMode;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ReplaceMCVillagers {
    @SubscribeEvent
    public void onVillagerSpawn(EntityJoinWorldEvent event) {
        if (event.getWorld().isRemote) {
            return;
        }
        if (event.getEntity() instanceof EntityVillager) {
            event.setCanceled(true);
            int rand = RandomHelper.getRandomNumberBetween(1, 10);
            if (rand <= 2) {
                NPCTrainer trainer = new NPCTrainer(event.getWorld());
                BaseTrainer base = ServerNPCRegistry.trainers.getRandomBaseWithData();
                trainer.setBaseTrainer(base);
                trainer.init(base);
                trainer.setPosition(event.getEntity().posX, event.getEntity().posY, event.getEntity().posZ);
                trainer.setEncounterMode(EnumEncounterMode.OncePerPlayer);
                trainer.npcLocation = SpawnLocation.LandVillager;
                trainer.ignoreDespawnCounter = true;
                event.getWorld().spawnEntity(trainer);
            } else {
                NPCChatting npc = new NPCChatting(event.getWorld());
                GeneralNPCData data = ServerNPCRegistry.villagers.getRandom();
                npc.init(data);
                npc.setCustomSteveTexture(data.getRandomTexture());
                npc.setTextureIndex(0);
                npc.setPosition(event.getEntity().posX, event.getEntity().posY, event.getEntity().posZ);
                npc.setProfession(0);
                npc.initVilagerAI();
                npc.npcLocation = SpawnLocation.LandVillager;
                npc.ignoreDespawnCounter = true;
                event.getWorld().spawnEntity(npc);
            }
            event.getEntity().setDead();
        }
    }
}

