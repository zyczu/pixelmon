/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.event;

import com.pixelmongenerations.api.events.RepelEvent;
import com.pixelmongenerations.common.item.ItemRepel;
import com.pixelmongenerations.core.network.ChatHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class RepelHandler {
    private static HashMap<UUID, Integer> repelExpiries = new HashMap();

    public static void onTick() {
        ArrayList<UUID> uuids = new ArrayList<UUID>(repelExpiries.keySet());
        for (UUID uuid : uuids) {
            int units = repelExpiries.get(uuid) - 1;
            if (units <= 0) {
                repelExpiries.remove(uuid);
                EntityPlayerMP player = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUUID(uuid);
                ChatHandler.sendFormattedChat(player, TextFormatting.GRAY, "item.repel.expire", new Object[0]);
                continue;
            }
            repelExpiries.put(uuid, units);
        }
    }

    public static boolean hasRepel(UUID uuid) {
        return repelExpiries.containsKey(uuid);
    }

    public static void applyRepel(UUID uuid, ItemRepel.EnumRepel repel) {
        RepelEvent repelEvent = new RepelEvent(uuid, repel);
        MinecraftForge.EVENT_BUS.post(repelEvent);
        if (!repelEvent.isCanceled()) {
            int units = repelEvent.getUnits();
            if (RepelHandler.hasRepel(uuid)) {
                units += repelExpiries.get(uuid).intValue();
            }
            repelExpiries.put(uuid, units);
        }
    }
}

