/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.event;

import com.pixelmongenerations.common.battle.nobles.NobleBattle;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.event.PixelmonPlayerTracker;
import java.util.UUID;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EntityDeath {
    @SubscribeEvent
    public void onPlayerDeath(LivingDeathEvent event) {
        Entity eventEntity = event.getEntity();
        if (event.getEntity() instanceof EntityPlayerMP) {
            EntityPixelmon killer;
            EntityPlayerMP player = (EntityPlayerMP)eventEntity;
            PixelmonPlayerTracker.removePlayer((EntityPlayerMP)eventEntity);
            if (event.getSource().getTrueSource() instanceof EntityPixelmon && (killer = (EntityPixelmon)event.getSource().getTrueSource()).isNoble()) {
                NobleBattle.nobleBattleMap.entrySet().removeIf(entry -> {
                    if (((UUID)entry.getKey()).toString().equalsIgnoreCase(player.getUniqueID().toString())) {
                        NobleBattle battle = (NobleBattle)entry.getValue();
                        battle.getFrenzyGauge().setVisible(false);
                        battle.getFrenzyGauge().removePlayer(battle.getPlayer().getEntityPlayer());
                        battle.getPlayer().getEntityPlayer().sendMessage(new TextComponentString("You lost to " + battle.getPokemon().getEntityPixelmon().getPokemonName() + "!"));
                        battle.getPokemon().getEntityPixelmon().setDead();
                        player.sendMessage(new TextComponentString(battle.getPokemon().getEntityPixelmon().getPokemonName() + " fled!"));
                        return true;
                    }
                    return false;
                });
            }
        }
    }
}

