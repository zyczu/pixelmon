/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.event;

import com.pixelmongenerations.core.enums.EnumDynamaxItem;
import com.pixelmongenerations.core.enums.EnumMegaItem;
import com.pixelmongenerations.core.enums.EnumShinyItem;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

public class EntityPlayerExtension {
    public static DataParameter<String> dwPokeballs = EntityDataManager.createKey(EntityPlayer.class, DataSerializers.STRING);
    public static DataParameter<String> dwEggs = EntityDataManager.createKey(EntityPlayer.class, DataSerializers.STRING);
    public static DataParameter<String> dwCape = EntityDataManager.createKey(EntityPlayer.class, DataSerializers.STRING);
    public static DataParameter<Integer> dwMegaItem = EntityDataManager.createKey(EntityPlayer.class, DataSerializers.VARINT);
    public static DataParameter<Integer> dwShinyItem = EntityDataManager.createKey(EntityPlayer.class, DataSerializers.VARINT);
    public static DataParameter<Integer> dwDynamaxItem = EntityDataManager.createKey(EntityPlayer.class, DataSerializers.VARINT);

    @SubscribeEvent
    public void handleConstruction(EntityEvent.EntityConstructing event) {
        if (event.getEntity() instanceof EntityPlayer) {
            EntityDataManager dw = event.getEntity().getDataManager();
            dw.register(dwPokeballs, "-1,-1,-1,-1,-1,-1,");
            dw.register(dwEggs, "-1,-1,-1,-1,-1,-1,");
            dw.register(dwCape, "");
            dw.register(dwMegaItem, 0);
            dw.register(dwShinyItem, 0);
            dw.register(dwDynamaxItem, 0);
        }
    }

    @SubscribeEvent
    public void onPlayerRespawn(PlayerEvent.PlayerRespawnEvent event) {
        PlayerStorage storage;
        if (event.player instanceof EntityPlayerMP && (storage = PixelmonStorage.pokeBallManager.getIsLoaded((EntityPlayerMP)event.player)) != null) {
            int[] pokeballs = storage.getPokeballList();
            int[] eggs = storage.getEggList();
            EntityPlayerMP player = (EntityPlayerMP)event.player;
            EntityPlayerExtension.updatePlayerPokeballs(player, pokeballs);
            EntityPlayerExtension.updatePlayerEggs(player, eggs);
            EntityPlayerExtension.updatePlayerCape(player, storage.cape);
            EntityPlayerExtension.updatePlayerMegaItem(player, storage.megaData.getMegaItem());
            EntityPlayerExtension.updatePlayerShinyItem(player, storage.shinyData.getShinyItem());
            EntityPlayerExtension.updatePlayerDynamaxItem(player, storage.dynamaxData.getDynamaxItem());
        }
    }

    public static void updatePlayerPokeballs(EntityPlayerMP player, int[] pokeballs) {
        if (player == null) {
            return;
        }
        StringBuilder ballsList = new StringBuilder();
        for (int i : pokeballs) {
            ballsList.append(i).append(",");
        }
        player.getDataManager().set(dwPokeballs, ballsList.toString());
    }

    public static int[] getPlayerPokeballs(EntityPlayer player) {
        String ballsList = player.getDataManager().get(dwPokeballs);
        String[] splits = ballsList.split(",");
        int[] balls = new int[6];
        for (int i = 0; i < 6; ++i) {
            balls[i] = Integer.parseInt(splits[i]);
        }
        return balls;
    }

    public static void updatePlayerEggs(EntityPlayerMP player, int[] eggs) {
        if (player == null) {
            return;
        }
        StringBuilder eggList = new StringBuilder();
        for (int i : eggs) {
            eggList.append(i).append(",");
        }
        player.getDataManager().set(dwEggs, eggList.toString());
    }

    public static int[] getPlayerEggs(EntityPlayer player) {
        String eggList = player.getDataManager().get(dwEggs);
        String[] splits = eggList.split(",");
        int[] eggs = new int[6];
        for (int i = 0; i < 6; ++i) {
            eggs[i] = Integer.parseInt(splits[i]);
        }
        return eggs;
    }

    public static void updatePlayerCape(EntityPlayer player, String texture) {
        if (player == null || texture == null) {
            return;
        }
        player.getDataManager().set(dwCape, texture);
    }

    public static String getPlayerCape(EntityPlayer player) {
        return player.getDataManager().get(dwCape);
    }

    public static void updatePlayerMegaItem(EntityPlayerMP player, EnumMegaItem megaItem) {
        player.getDataManager().set(dwMegaItem, megaItem.ordinal());
    }

    public static void updatePlayerShinyItem(EntityPlayerMP player, EnumShinyItem shinyItem) {
        player.getDataManager().set(dwShinyItem, shinyItem.ordinal());
    }

    public static void updatePlayerDynamaxItem(EntityPlayerMP player, EnumDynamaxItem dynamaxItem) {
        player.getDataManager().set(dwDynamaxItem, dynamaxItem.ordinal());
    }

    public static EnumMegaItem getPlayerMegaItem(EntityPlayer player) {
        return EnumMegaItem.values()[player.getDataManager().get(dwMegaItem)];
    }

    public static EnumShinyItem getPlayerShinyItem(EntityPlayer player) {
        return EnumShinyItem.values()[player.getDataManager().get(dwShinyItem)];
    }

    public static EnumDynamaxItem getPlayerDynamaxItem(EntityPlayer player) {
        return EnumDynamaxItem.values()[player.getDataManager().get(dwDynamaxItem)];
    }
}

