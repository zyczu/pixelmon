/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.event;

import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EntitySpawning {
    @SubscribeEvent
    public void onSpawnInChunk(EntityEvent.EnteringChunk event) {
        if (event.getEntity().posX > (double)(event.getNewChunkX() * 16 + 16) || event.getEntity().posX < (double)(event.getNewChunkX() * 16) || event.getEntity().posZ > (double)(event.getNewChunkZ() * 16 + 16) || event.getEntity().posZ < (double)(event.getNewChunkZ() * 16)) {
            if (PixelmonConfig.printErrors) {
                System.out.println("Fixing entity");
            }
            event.getEntity().posX = RandomHelper.getRandomNumberBetween(0, 15) + event.getNewChunkX() * 16;
            event.getEntity().posZ = RandomHelper.getRandomNumberBetween(0, 15) + event.getNewChunkZ() * 16;
        }
    }
}

