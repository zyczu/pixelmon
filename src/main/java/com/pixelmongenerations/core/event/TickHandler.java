/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.event;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.common.entity.EntityWishingStar;
import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQueryList;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.command.MaxEnergyBeam;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItemsTools;
import com.pixelmongenerations.core.event.RepelHandler;
import com.pixelmongenerations.core.handler.TimeHandler;
import com.pixelmongenerations.core.network.packetHandlers.GuiOpenClose;
import com.pixelmongenerations.core.network.packetHandlers.RequestUpdatedPokemonList;
import com.pixelmongenerations.core.network.packetHandlers.StarterListPacket;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.ArrayList;
import java.util.Optional;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.MobEffects;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.client.GuiIngameForge;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;

public class TickHandler {
    private int ticksSinceSentLogin = 0;
    boolean checkedForUsername = false;
    boolean screenOpen = false;
    boolean initialised = false;
    public int worldCounter;
    private int ticksSinceEggStepCheck = 0;
    static final ArrayList<EntityPlayerMP> playerListForStartMenu = new ArrayList();

    @SubscribeEvent
    public void onPlayerTick(TickEvent.PlayerTickEvent event) {
        Optional<PlayerStorage> optstorage;
        EntityPlayer player = event.player;
        if (event.side == Side.CLIENT) {
            if (!(Minecraft.getMinecraft().currentScreen == null || this.screenOpen && this.initialised || Minecraft.getMinecraft().currentScreen instanceof GuiChat)) {
                Pixelmon.NETWORK.sendToServer(new GuiOpenClose(true));
                this.screenOpen = true;
                this.initialised = true;
            } else if (this.screenOpen && Minecraft.getMinecraft().currentScreen == null || !this.initialised) {
                Pixelmon.NETWORK.sendToServer(new GuiOpenClose(false));
                this.screenOpen = false;
                this.initialised = true;
                if (Minecraft.getMinecraft().gameSettings.hideGUI || !GuiIngameForge.renderHotbar) {
                    Minecraft.getMinecraft().gameSettings.hideGUI = false;
                    GuiIngameForge.renderHotbar = true;
                    GuiIngameForge.renderCrosshairs = true;
                    GuiIngameForge.renderExperiance = true;
                    GuiIngameForge.renderAir = true;
                    GuiIngameForge.renderHealth = true;
                    GuiIngameForge.renderFood = true;
                    GuiIngameForge.renderArmor = true;
                }
            }
        }
        if (event.phase != TickEvent.Phase.START && event.side == Side.SERVER && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player)).isPresent()) {
            PlayerStorage storage = optstorage.get();
            if (!PixelmonConfig.useOptimizedEggStepCalculator || !PixelmonConfig.useOptimizedPokerusTimer) {
                ++storage.eggTick;
            }
            if (!(PixelmonConfig.useOptimizedDynamaxEnergyBeamSpawner && PixelmonConfig.useOptimizedWishingStarSpawner || storage.lastXPos == player.getPosition().getX() && storage.lastZPos == player.getPosition().getZ() || storage.eggTick < 20 || !player.world.canSeeSky(player.getPosition()))) {
                if (!PixelmonConfig.useOptimizedWishingStarSpawner && storage.pokedex.getCaughtPercentage() >= (double)PixelmonConfig.wishingStarDexCompletion && player.world.rand.nextInt(PixelmonConfig.wishingStar) == 1) {
                    EntityWishingStar wishingStar = new EntityWishingStar(player.world);
                    wishingStar.setPosition(player.getPosition().getX(), player.getPosition().getY() + 20, player.getPosition().getZ());
                    player.world.spawnEntity(wishingStar);
                    int direction = player.world.rand.nextInt(4);
                    if (direction == 0) {
                        wishingStar.addVelocity(3.0, 0.0, 0.0);
                    } else if (direction == 1) {
                        wishingStar.addVelocity(-3.0, 0.0, 0.0);
                    } else if (direction == 2) {
                        wishingStar.addVelocity(0.0, 0.0, 3.0);
                    } else {
                        wishingStar.addVelocity(0.0, 0.0, -3.0);
                    }
                }
                if (!PixelmonConfig.useOptimizedDynamaxEnergyBeamSpawner && player.world.rand.nextInt(PixelmonConfig.dynamaxEnergyBeam) == 1) {
                    int z;
                    int y;
                    int spawnRadius = 30;
                    int x = player.getPosition().getX() + player.world.rand.nextInt(spawnRadius * 2 + 1) - spawnRadius;
                    if (!MaxEnergyBeam.attemptEnergySpawn((EntityPlayerMP)player, x, y = player.getPosition().getY(), z = player.getPosition().getZ() + player.world.rand.nextInt(spawnRadius * 2 + 1) - spawnRadius)) {
                        y = player.world.getHeight(x, z);
                        Block block = player.world.getBlockState(new BlockPos(x, y - 1, z)).getBlock();
                        if (block.getRegistryName().toString().contains("leaves")) {
                            y -= 8;
                        }
                        if ((block = player.world.getBlockState(new BlockPos(x, y, z)).getBlock()) == Blocks.AIR) {
                            y -= 5;
                        }
                        MaxEnergyBeam.attemptEnergySpawn((EntityPlayerMP)player, x, y, z);
                    }
                }
            }
            if (!(PixelmonConfig.useOptimizedPokerusTimer && PixelmonConfig.useOptimizedEggStepCalculator || storage.eggTick < 20)) {
                storage.eggTick = 0;
                if (!PixelmonConfig.useOptimizedEggStepCalculator) {
                    storage.calculateWalkedSteps((EntityPlayerMP)player);
                }
                if (!PixelmonConfig.useOptimizedPokerusTimer) {
                    storage.pokeRusTick();
                }
            }
        }
        if (!(!PixelmonConfig.doWitherEffect || player.dimension != 24 || player.getItemStackFromSlot(EntityEquipmentSlot.HEAD).getItem() == PixelmonItemsTools.helmetUltra && player.getItemStackFromSlot(EntityEquipmentSlot.CHEST).getItem() == PixelmonItemsTools.chestplateUltra && player.getItemStackFromSlot(EntityEquipmentSlot.LEGS).getItem() == PixelmonItemsTools.leggingsUltra && player.getItemStackFromSlot(EntityEquipmentSlot.FEET).getItem() == PixelmonItemsTools.bootsUltra || player.getItemStackFromSlot(EntityEquipmentSlot.HEAD).getItem() == PixelmonItemsTools.helmetCrystal && event.player.getItemStackFromSlot(EntityEquipmentSlot.CHEST).getItem() == PixelmonItemsTools.chestplateCrystal && event.player.getItemStackFromSlot(EntityEquipmentSlot.LEGS).getItem() == PixelmonItemsTools.leggingsCrystal && event.player.getItemStackFromSlot(EntityEquipmentSlot.FEET).getItem() == PixelmonItemsTools.bootsCrystal)) {
            player.addPotionEffect(new PotionEffect(MobEffects.WITHER, 10, 2, false, false));
        }
    }

    @SubscribeEvent
    public void tickStart(TickEvent.ClientTickEvent event) {
        if (event.phase == TickEvent.Phase.END || Minecraft.getMinecraft().world == null) {
            return;
        }
        this.checkedForUsername = true;
        if (ServerStorageDisplay.count() == 0) {
            ++this.ticksSinceSentLogin;
            if (this.ticksSinceSentLogin >= 100) {
                this.ticksSinceSentLogin = 0;
                Pixelmon.NETWORK.sendToServer(new RequestUpdatedPokemonList());
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @SubscribeEvent
    public void onServerTick(TickEvent.ServerTickEvent event) {
        if (this.worldCounter % (PixelmonConfig.timeUpdateInterval * 20) == 0) {
            TimeHandler.changeTime();
        }
        if (!PixelmonConfig.useOptimizedRepelHandler && event.phase == TickEvent.Phase.START) {
            RepelHandler.onTick();
        }
        ArrayList<EntityPlayerMP> arrayList = playerListForStartMenu;
        synchronized (arrayList) {
            if (!playerListForStartMenu.isEmpty() && this.worldCounter % 100 == 0) {
                for (EntityPlayerMP player : playerListForStartMenu) {
                    Pixelmon.NETWORK.sendTo(new StarterListPacket(), player);
                }
            }
        }
        ++this.worldCounter;
    }

    @SubscribeEvent
    public void onWorldTick(TickEvent.WorldTickEvent event) {
        if (event.side == Side.SERVER && event.phase == TickEvent.Phase.END) {
            EvolutionQueryList.tick(event.world);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void registerStarterList(EntityPlayerMP player) {
        Pixelmon.NETWORK.sendTo(new StarterListPacket(), player);
        EntityPlayerMP listEntry = null;
        ArrayList<EntityPlayerMP> arrayList = playerListForStartMenu;
        synchronized (arrayList) {
            for (EntityPlayerMP listPlayer : playerListForStartMenu) {
                if (!listPlayer.getUniqueID().equals(player.getUniqueID())) continue;
                listEntry = listPlayer;
                break;
            }
            if (listEntry == null) {
                playerListForStartMenu.add(player);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void deregisterStarterList(EntityPlayerMP player) {
        EntityPlayerMP listEntry = null;
        ArrayList<EntityPlayerMP> arrayList = playerListForStartMenu;
        synchronized (arrayList) {
            for (EntityPlayerMP listPlayer : playerListForStartMenu) {
                if (!listPlayer.getUniqueID().equals(player.getUniqueID())) continue;
                listEntry = listPlayer;
                break;
            }
            playerListForStartMenu.remove(listEntry);
        }
    }
}

