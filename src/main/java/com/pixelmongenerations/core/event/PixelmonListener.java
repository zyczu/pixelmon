/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.primitives.Ints
 *  org.apache.logging.log4j.Level
 */
package com.pixelmongenerations.core.event;

import com.google.common.primitives.Ints;
import com.pixelmongenerations.api.DropQueryFactory;
import com.pixelmongenerations.api.events.AggressionEvent;
import com.pixelmongenerations.api.events.BattleStartEvent;
import com.pixelmongenerations.api.events.BeatWildPixelmonEvent;
import com.pixelmongenerations.api.events.CaptureEvent;
import com.pixelmongenerations.api.events.EvolveEvent;
import com.pixelmongenerations.api.events.HeldItemChangeEvent;
import com.pixelmongenerations.api.events.PixelmonBlockStartingBattleEvent;
import com.pixelmongenerations.api.events.PixelmonReceivedEvent;
import com.pixelmongenerations.api.events.PixelmonSendOutEvent;
import com.pixelmongenerations.api.events.PixelmonTradeEvent;
import com.pixelmongenerations.api.events.PixelmonUpdateEvent;
import com.pixelmongenerations.api.events.PokeballImpactEvent;
import com.pixelmongenerations.api.events.PokerusEvent;
import com.pixelmongenerations.api.events.SpaceTimeDistortionEvent;
import com.pixelmongenerations.api.events.battles.BattleEndEvent;
import com.pixelmongenerations.api.events.battles.DefeatNobleEvent;
import com.pixelmongenerations.api.events.battles.HordeTriggeredEvent;
import com.pixelmongenerations.api.events.npc.BeatTrainerEvent;
import com.pixelmongenerations.api.events.player.PlayerEggHatchEvent;
import com.pixelmongenerations.api.events.player.PlayerPokemonSpawnEvent;
import com.pixelmongenerations.api.events.pokemon.BaseStatsLoadEvent;
import com.pixelmongenerations.api.events.pokemon.MovesetEvent;
import com.pixelmongenerations.api.events.spawning.DespawnEvent;
import com.pixelmongenerations.api.events.spawning.SpawnEvent;
import com.pixelmongenerations.api.pc.BackgroundRegistry;
import com.pixelmongenerations.api.pc.BiomeUnlockedBackground;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.battle.BattleFactory;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.battle.nobles.NobleBattle;
import com.pixelmongenerations.common.battle.nobles.NobleBattlePlayer;
import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Competitive;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.entity.pixelmon.drops.DroppedItem;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.NBTLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.item.ItemPixelmonSprite;
import com.pixelmongenerations.common.item.heldItems.ItemPlate;
import com.pixelmongenerations.common.item.heldItems.MemoryDrive;
import com.pixelmongenerations.common.item.heldItems.TypeEnhancingItems;
import com.pixelmongenerations.common.item.heldItems.ZCrystal;
import com.pixelmongenerations.common.spawning.PlayerTrackingSpawner;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.PixelmonHolidays;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.enums.EnumMark;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.BattleResults;
import com.pixelmongenerations.core.enums.battle.EnumBattleEndCause;
import com.pixelmongenerations.core.enums.forms.EnumArceus;
import com.pixelmongenerations.core.enums.forms.EnumGenesect;
import com.pixelmongenerations.core.enums.forms.EnumGiratina;
import com.pixelmongenerations.core.enums.forms.EnumKeldeo;
import com.pixelmongenerations.core.enums.forms.EnumRotom;
import com.pixelmongenerations.core.enums.forms.EnumSilvally;
import com.pixelmongenerations.core.enums.forms.EnumUnown;
import com.pixelmongenerations.core.enums.forms.EnumVivillon;
import com.pixelmongenerations.core.enums.forms.EnumZacian;
import com.pixelmongenerations.core.enums.forms.EnumZamazenta;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PCServer;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelSounds;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.PixelmonTasks;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Biomes;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import org.apache.logging.log4j.Level;

public class PixelmonListener {
    private static final PokemonSpec UNTRADEABLE = new PokemonSpec("untradeable");

    @SubscribeEvent
    public void onEggHatch(PlayerEggHatchEvent event) {
        EntityPlayerMP player = event.getPlayer();
        if (PixelmonConfig.ovalCharmMaxChance > 0 && RandomHelper.getRandomNumberBetween(1, PixelmonConfig.ovalCharmMaxChance) == 1) {
            DropQueryFactory.newInstance().customTitle(new TextComponentTranslation("pixelmon.loot.ovalcharm", new Object[0])).addDrop(DroppedItem.of(new ItemStack(PixelmonItems.ovalCharm))).addTarget(player).send();
            if (player.getServer() != null) {
                ChatHandler.sendMessageToAllPlayers(player.getServer(), "[" + (Object)((Object)TextFormatting.DARK_PURPLE) + "Pixelmon" + (Object)((Object)TextFormatting.WHITE) + "] " + (Object)((Object)TextFormatting.GREEN) + player.getName() + " has found the rare " + (Object)((Object)TextFormatting.BLUE) + "Oval Charm!");
            }
        }
    }

    @SubscribeEvent
    public void onStartCapture(CaptureEvent.StartCaptureEvent event) {
        EnumSpecies species = event.getPokemon().getSpecies();
        if ((species == EnumSpecies.MissingNo && event.getPokemon().getForm() < 5 || event.getPokemon().isTotem()) && !PixelmonConfig.allowMissingNoCapture) {
            event.setCanceled(true);
        }
        EnumPokeball pokeball = event.getPokeBall().getType();
        if (!(species != EnumSpecies.Eternatus || pokeball != EnumPokeball.MasterBall && pokeball != EnumPokeball.ParkBall || PixelmonConfig.allowMasterBallEternatus)) {
            event.setCanceled(true);
        }
        if (event.getPokemon().isAlpha() && !PixelmonServerConfig.catchAlphasOutsideOfBattle && event.getPokemon().battleController == null) {
            event.setCanceled(true);
        }
        if (!event.getPokemon().isCatchable()) {
            event.setCanceled(true);
        }
    }

    @SubscribeEvent
    public void onSuccessfulCapture(CaptureEvent.SuccessfulCaptureEvent event) {
        EntityPlayerMP player = event.getPlayer();
        Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        EntityPixelmon pokemon = event.getPokemon();
        if (storageOpt.isPresent()) {
            EnumUnown form;
            PlayerStorage storage = storageOpt.get();
            storage.handleCapture(event.getPokemon().getSpecies(), false);
            storage.pokedex.checkDex();
            if (event.getPokemon().getFormEnum() instanceof EnumUnown && !storage.playerData.unownCaught.contains((form = (EnumUnown)event.getPokemon().getFormEnum()).getProperName())) {
                storage.playerData.unownCaught = storage.playerData.unownCaught + form.getProperName();
            }
        }
        if (pokemon.getSpecies().equals((Object)EnumSpecies.Eternatus)) {
            pokemon.setForm(0, true);
            pokemon.stats.HP = pokemon.stats.calculateHP(pokemon.baseStats, 100);
            pokemon.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(pokemon.stats.HP);
            pokemon.setHealth(pokemon.stats.HP);
            pokemon.updateStats();
        }
        if (pokemon.getSpecies() == EnumSpecies.Arceus && PCServer.giveBackground(player, "box_arceus")) {
            ChatHandler.sendChat(player, "pixelmon.backgrounds.unlocked", "Arceus");
        }
        if ((pokemon.getSpecies() == EnumSpecies.Zamazenta || pokemon.getSpecies() == EnumSpecies.Zacian) && PCServer.giveBackground(player, "box_chess")) {
            ChatHandler.sendChat(player, "pixelmon.backgrounds.unlocked", "Chess");
        }
        if ((pokemon.getSpecies() == EnumSpecies.Aegislash || Math.random() <= 0.05) && PCServer.giveBackground(player, "box_aegislash")) {
            ChatHandler.sendChat(player, "pixelmon.backgrounds.unlocked", "Aegislash");
        }
        BackgroundRegistry.getBackgrounds().stream().filter(background -> background instanceof BiomeUnlockedBackground).forEach(background -> ((BiomeUnlockedBackground)background).rollBiomeUnlock(event.getPlayer()));
        if (pokemon.getMark() != EnumMark.None && PixelmonConfig.markCharmMaxChance == 0) {
            if (RandomHelper.getRandomNumberBetween(1, PixelmonConfig.markCharmMaxChance) == 1) {
                DropQueryFactory.newInstance().customTitle(new TextComponentTranslation("pixelmon.loot.markcharm", new Object[0])).addDrop(DroppedItem.of(new ItemStack(PixelmonItems.markCharm))).addTarget(player).send();
                if (player.getServer() != null) {
                    ChatHandler.sendMessageToAllPlayers(player.getServer(), "[" + (Object)((Object)TextFormatting.DARK_PURPLE) + "Pixelmon" + (Object)((Object)TextFormatting.WHITE) + "] " + (Object)((Object)TextFormatting.GREEN) + player.getName() + " has found the rare " + (Object)((Object)TextFormatting.BLUE) + "Mark Charm!");
                }
            }
            if (pokemon.getMark() == EnumMark.SleepyTime && PCServer.giveBackground(player, "box_cloudy_nights")) {
                ChatHandler.sendChat(player, "pixelmon.backgrounds.unlocked", "Cloudy Nights");
            }
        }
        if (pokemon.getSpecies() == EnumSpecies.Rayquaza && pokemon.getForm() == 1) {
            EntityPixelmon newRay = PokemonSpec.from("Rayquaza").create(player.getEntityWorld());
            event.setPokemon(newRay);
        }
        if (pokemon.isAlpha()) {
            pokemon.setHostile(false);
        }
    }

    @SubscribeEvent
    public void onBeatWildPokemon(BeatWildPixelmonEvent event) {
        PixelmonWrapper pokemonWrapper = event.getWildPokemon().getPokemon();
        EnumSpecies species = pokemonWrapper.getSpecies();
        EntityPlayerMP player = event.getPlayer();
        if (species == EnumSpecies.Groudon) {
            if (RandomHelper.getRandomChance()) {
                DropItemHelper.giveItemStackToPlayer(player, new ItemStack(PixelmonItems.fadedRedOrb), true);
            }
        } else if (species == EnumSpecies.Kyogre && RandomHelper.getRandomChance()) {
            DropItemHelper.giveItemStackToPlayer(player, new ItemStack(PixelmonItems.fadedBlueOrb), true);
        }
        if (PixelmonConfig.awardTokens) {
            for (PixelmonWrapper wrapper : event.getWildPokemon().controlledPokemon) {
                EntityPixelmon pokemon = wrapper.pokemon;
                player.inventory.addItemStackToInventory(ItemPixelmonSprite.getPhoto(pokemon));
            }
        }
        PixelmonHolidays.getActiveHolidays().filter(h -> h.isHolidayPokemon(species)).forEach(h -> h.onWildPokemonDefeated(player, pokemonWrapper));
        Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        storageOpt.ifPresent(playerStorage -> playerStorage.handleCapture(species, true));
        if (pokemonWrapper.getWorld().getBiome(pokemonWrapper.getWorldPosition()) == Biomes.MUTATED_FOREST && (pokemonWrapper.type.contains((Object)EnumType.Fairy) || pokemonWrapper.type.contains((Object)EnumType.Psychic) || pokemonWrapper.type.contains((Object)EnumType.Grass))) {
            for (int i = 0; i < player.inventory.getSizeInventory(); ++i) {
                ItemStack itemStack = player.inventory.getStackInSlot(i);
                if (itemStack == ItemStack.EMPTY || itemStack.getItem() != PixelmonItems.timeGlass) continue;
                itemStack.setItemDamage(itemStack.getItemDamage() + 1);
                break;
            }
        }
    }

    @SubscribeEvent
    public void onPokemonSpawn(PlayerPokemonSpawnEvent event) {
        PixelmonHolidays.getActiveHolidays().filter(h -> h.isHolidayPokemon(event.getPokemonSpec().getSpecies())).forEach(h -> h.onWildPokemonSpawn(event.getPlayer(), event.getPokemonSpec()));
        if (event.getPokemonSpec().getSpecies() == EnumSpecies.Eternatus) {
            event.getPokemonSpec().growth = 4;
        }
    }

    @SubscribeEvent
    public void onSpawn(SpawnEvent event) {
        if (event.getSpawner() instanceof PlayerTrackingSpawner) {
            PlayerTrackingSpawner spawner = (PlayerTrackingSpawner)event.getSpawner();
            if (event.getSpawnAction().getOrCreateEntity() instanceof EntityPixelmon) {
                double chance;
                EntityPixelmon pokemon = (EntityPixelmon)event.getSpawnAction().getOrCreateEntity();
                if (spawner.getTrackedPlayer() != null) {
                    EntityPlayerMP player = spawner.getTrackedPlayer();
                    EnumMark mark = EnumMark.rollMark(player, pokemon);
                    pokemon.setMark(mark);
                    pokemon.update(EnumUpdateType.Mark);
                    if (pokemon.isShiny() && PixelmonServerConfig.playShinySoundOnShinySpawn) {
                        player.world.playSound(null, player.getPosition(), PixelSounds.shinySpawn, SoundCategory.BLOCKS, 1.0f, 1.0f);
                    }
                    List<Integer> cosplayForms = Arrays.asList(1, 2, 3, 4, 6);
                    if (pokemon.getSpecies() == EnumSpecies.Pikachu && cosplayForms.contains(pokemon.getForm()) && pokemon.getGender() == Gender.Male) {
                        pokemon.setGender(Gender.Female);
                    }
                }
                if (!pokemon.isAlpha() && PixelmonConfig.allowTotemSpawns && RandomHelper.getRandomChance(chance = PixelmonConfig.totemConversionChance)) {
                    pokemon.setTotem(true);
                }
            }
        }
    }

    @SubscribeEvent
    public void onHeldItemChanged(HeldItemChangeEvent event) {
        EntityLivingBase owner = event.getOwner();
        ItemStack heldItem = event.getItem();
        Optional<EntityPixelmon> pokemonOpt = event.getPokemon();
        Optional<NBTTagCompound> nbtOpt = event.getTagCompound();
        owner.world.getMinecraftServer().addScheduledTask(() -> {
            MemoryDrive drive;
            int newForm;
            int curForm;
            if (heldItem != null && !heldItem.isEmpty() && heldItem.getItem() instanceof ZCrystal) {
                ZCrystal crystal = (ZCrystal)heldItem.getItem();
                EntityPixelmon pixelmon = pokemonOpt.orElseGet(() -> (EntityPixelmon)PixelmonEntityList.createEntityFromNBT((NBTTagCompound)nbtOpt.get(), owner.getEntityWorld()));
                if (!crystal.canBeAppliedToPokemon(new PokemonSpec(pixelmon.getName()).setForm(pixelmon.getForm()))) {
                    event.setCanceled(true);
                    return;
                }
            }
            if (event.getSpecies() == EnumSpecies.Giratina) {
                int curForm2;
                int newForm2 = curForm2 = nbtOpt.map(nbtTagCompound -> nbtTagCompound.getInteger("Variant")).orElseGet(() -> ((EntityPixelmon)pokemonOpt.get()).getForm()).intValue();
                if ((heldItem == null || heldItem.isEmpty()) && curForm2 == EnumGiratina.ORIGIN.getForm()) {
                    newForm2 = EnumGiratina.ALTERED.getForm();
                } else if (heldItem.getItem() == PixelmonItemsHeld.griseous_orb && curForm2 == EnumGiratina.ALTERED.getForm()) {
                    newForm2 = EnumGiratina.ORIGIN.getForm();
                }
                if (curForm2 != newForm2) {
                    PixelmonMethods.changeForm(newForm2, owner, nbtOpt, pokemonOpt);
                    if (owner instanceof EntityPlayerMP) {
                        ChatHandler.sendChat(owner, "pixelmon.abilities.changeform", "Giratina");
                    }
                }
            } else if (event.getSpecies() == EnumSpecies.Zacian) {
                int curForm3;
                int newForm3 = curForm3 = nbtOpt.map(nbtTagCompound -> nbtTagCompound.getInteger("Variant")).orElseGet(() -> ((EntityPixelmon)pokemonOpt.get()).getForm()).intValue();
                if ((heldItem == null || heldItem.isEmpty() || heldItem.getItem() != PixelmonItemsHeld.crownedSword) && curForm3 == EnumZacian.Crowned.getForm()) {
                    newForm3 = EnumZacian.Normal.getForm();
                } else if (heldItem.getItem() == PixelmonItemsHeld.crownedSword && curForm3 == EnumZacian.Normal.getForm()) {
                    newForm3 = EnumZacian.Crowned.getForm();
                }
                if (curForm3 != newForm3) {
                    PixelmonMethods.changeForm(newForm3, owner, nbtOpt, pokemonOpt);
                    if (owner instanceof EntityPlayerMP) {
                        ChatHandler.sendChat(owner, "pixelmon.abilities.changeform", "Zacian");
                        ((EntityPixelmon)pokemonOpt.get()).updateStats();
                    }
                }
            } else if (event.getSpecies() == EnumSpecies.Zamazenta) {
                int curForm4;
                int newForm4 = curForm4 = nbtOpt.map(nbtTagCompound -> nbtTagCompound.getInteger("Variant")).orElseGet(() -> ((EntityPixelmon)pokemonOpt.get()).getForm()).intValue();
                if ((heldItem == null || heldItem.isEmpty() || heldItem.getItem() != PixelmonItemsHeld.crownedShield) && curForm4 == EnumZamazenta.Crowned.getForm()) {
                    newForm4 = EnumZamazenta.Normal.getForm();
                } else if (heldItem.getItem() == PixelmonItemsHeld.crownedShield && curForm4 == EnumZamazenta.Normal.getForm()) {
                    newForm4 = EnumZamazenta.Crowned.getForm();
                }
                if (curForm4 != newForm4) {
                    PixelmonMethods.changeForm(newForm4, owner, nbtOpt, pokemonOpt);
                    if (owner instanceof EntityPlayerMP) {
                        ChatHandler.sendChat(owner, "pixelmon.abilities.changeform", "Zamazenta");
                        ((EntityPixelmon)pokemonOpt.get()).updateStats();
                    }
                }
            } else if (event.getSpecies() == EnumSpecies.Arceus) {
                ZCrystal zCrystal;
                int newForm5;
                int curForm5 = nbtOpt.isPresent() ? ((NBTTagCompound)nbtOpt.get()).getInteger("Variant") : ((EntityPixelmon)pokemonOpt.get()).getForm();
                TypeEnhancingItems plate = heldItem != null && !heldItem.isEmpty() && heldItem.getItem() instanceof ItemPlate ? (ItemPlate)heldItem.getItem() : null;
                if (plate == null && curForm5 != (newForm5 = EnumArceus.getForm((zCrystal = heldItem != null && !heldItem.isEmpty() && heldItem.getItem() instanceof ZCrystal ? (ZCrystal)heldItem.getItem() : null) == null ? EnumType.Normal : zCrystal.crystalType.getType()).ordinal())) {
                    PixelmonMethods.changeForm(newForm5, owner, nbtOpt, pokemonOpt);
                    ChatHandler.sendChat(owner, "pixelmon.abilities.changeform", "Arceus");
                    return;
                }
                newForm = EnumArceus.getForm(plate == null ? EnumType.Normal : plate.getType()).ordinal();
                if (curForm5 != newForm) {
                    PixelmonMethods.changeForm(newForm, owner, nbtOpt, pokemonOpt);
                    ChatHandler.sendChat(owner, "pixelmon.abilities.changeform", "Arceus");
                }
            } else if (event.getSpecies() == EnumSpecies.Genesect) {
                int curForm6 = nbtOpt.isPresent() ? ((NBTTagCompound)nbtOpt.get()).getInteger("Variant") : ((EntityPixelmon)pokemonOpt.get()).getForm();
                byte newForm6 = EnumGenesect.NORMAL.getForm();
                if (heldItem != null && !heldItem.isEmpty()) {
                    Item item = heldItem.getItem();
                    if (item == PixelmonItemsHeld.burnDrive) {
                        newForm6 = EnumGenesect.BURN.getForm();
                    } else if (item == PixelmonItemsHeld.chillDrive) {
                        newForm6 = EnumGenesect.CHILL.getForm();
                    } else if (item == PixelmonItemsHeld.douseDrive) {
                        newForm6 = EnumGenesect.DOUSE.getForm();
                    } else if (item == PixelmonItemsHeld.shockDrive) {
                        newForm6 = EnumGenesect.SHOCK.getForm();
                    }
                }
                if (curForm6 != newForm6) {
                    PixelmonMethods.changeForm(newForm6, owner, nbtOpt, pokemonOpt);
                    if (owner instanceof EntityPlayerMP) {
                        ChatHandler.sendChat(owner, "pixelmon.abilities.changeform", "Genesect");
                    }
                }
            } else if (event.getSpecies() == EnumSpecies.Silvally && (curForm = nbtOpt.isPresent() ? ((NBTTagCompound)nbtOpt.get()).getInteger("Variant") : ((EntityPixelmon)pokemonOpt.get()).getForm()) != (newForm = EnumSilvally.getForm((drive = heldItem != null && !heldItem.isEmpty() && heldItem.getItem() instanceof MemoryDrive ? (MemoryDrive)heldItem.getItem() : null) == null ? EnumType.Normal : drive.getType()).ordinal())) {
                PixelmonMethods.changeForm(newForm, owner, nbtOpt, pokemonOpt);
                ChatHandler.sendChat(owner, "pixelmon.abilities.changeform", "Silvally");
            }
        });
    }

    @SubscribeEvent
    public void onBeatNPC(BeatTrainerEvent event) {
        EntityPlayerMP player = event.getPlayer();
        for (int i = 0; i < player.inventory.getSizeInventory(); ++i) {
            ItemStack itemStack = player.inventory.getStackInSlot(i);
            if (itemStack == ItemStack.EMPTY || itemStack.getItem() != PixelmonItems.secretArmorScroll) continue;
            itemStack.setItemDamage(itemStack.getItemDamage() + 1);
            break;
        }
    }

    @SubscribeEvent
    public void onBaseStatsLoad(BaseStatsLoadEvent event) {
        String name = event.getName();
        int form = event.getForm();
        if (name.equals(EnumSpecies.Arceus.name) && event.getBaseStats().isPresent()) {
            if (event.getBaseStats().isPresent()) {
                if (form == -1) {
                    return;
                }
                BaseStats baseStats = event.getBaseStats().get();
                EnumArceus arceusForm = EnumArceus.values()[Ints.constrainToRange((int)form, (int)0, (int)(EnumArceus.values().length - 1))];
                baseStats.type1 = EnumType.Normal;
                if (arceusForm.type != null) {
                    baseStats.type1 = arceusForm.type;
                }
                event.setBaseStats(baseStats);
            } else {
                Pixelmon.LOGGER.log(Level.ERROR, "Problem handling Arceus base stats");
            }
        } else if (name.equals(EnumSpecies.Rotom.name) && event.getBaseStats().isPresent()) {
            if (form == -1) {
                return;
            }
            if (form > 0) {
                BaseStats baseStats = event.getBaseStats().get();
                baseStats.attack = 65;
                baseStats.defence = 107;
                baseStats.spAtt = 105;
                baseStats.spDef = 107;
                baseStats.speed = 86;
                switch (EnumRotom.values()[Ints.constrainToRange((int)form, (int)0, (int)(EnumRotom.values().length - 1))]) {
                    case HEAT: {
                        baseStats.type2 = EnumType.Fire;
                        break;
                    }
                    case WASH: {
                        baseStats.type2 = EnumType.Water;
                        break;
                    }
                    case FROST: {
                        baseStats.type2 = EnumType.Ice;
                        break;
                    }
                    case FAN: {
                        baseStats.type2 = EnumType.Flying;
                        break;
                    }
                    case MOW: {
                        baseStats.type2 = EnumType.Grass;
                    }
                }
                event.setBaseStats(baseStats);
            }
        } else if (name.equals(EnumSpecies.Silvally.name) && event.getBaseStats().isPresent()) {
            if (event.getBaseStats().isPresent()) {
                if (form == -1) {
                    return;
                }
                BaseStats baseStats = event.getBaseStats().get();
                EnumSilvally silvallyForm = EnumSilvally.values()[Ints.constrainToRange((int)form, (int)0, (int)(EnumSilvally.values().length - 1))];
                baseStats.type1 = EnumType.Normal;
                if (silvallyForm.type != null) {
                    baseStats.type1 = silvallyForm.type;
                }
                event.setBaseStats(baseStats);
            } else {
                Pixelmon.LOGGER.log(Level.ERROR, "Problem handling Silvally base stats");
            }
        }
    }

    @SubscribeEvent
    public void onSentOut(PixelmonSendOutEvent event) {
        EntityPlayerMP player = event.getPlayer();
        NBTTagCompound nbt = event.getTagCompound();
        if (nbt.getString("Name").equals(EnumSpecies.Shaymin.name)) {
            if (nbt.getInteger("Variant") == 1 && !player.getServerWorld().isDaytime()) {
                player.world.getMinecraftServer().addScheduledTask(() -> {
                    EntityPixelmon pokemon = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt, player.getServerWorld());
                    pokemon.setForm(0, true);
                    pokemon.writeToNBT(nbt);
                    ChatHandler.sendChat(player, "pixelmon.abilities.changeform", nbt.getString("Name"));
                });
            }
        } else if (nbt.getString("Name").equals(EnumSpecies.Cherrim.name)) {
            if (nbt.getInteger("Variant") == 1) {
                player.world.getMinecraftServer().addScheduledTask(() -> {
                    EntityPixelmon pokemon = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt, player.getServerWorld());
                    pokemon.setForm(0, true);
                    pokemon.writeToNBT(nbt);
                });
            }
        } else if (nbt.getString("Name").equals(EnumSpecies.Hoopa.name) && nbt.getInteger("Variant") == 1) {
            if (!nbt.getCompoundTag("ForgeData").hasKey("unboundTime") || System.currentTimeMillis() > nbt.getCompoundTag("ForgeData").getLong("unboundTime") + (long)PixelmonConfig.hoopaUnbound) {
                player.world.getMinecraftServer().addScheduledTask(() -> {
                    EntityPixelmon pokemon = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt, player.getServerWorld());
                    pokemon.setForm(0, true);
                    pokemon.getMoveset().replaceMove("Hyperspace Fury", new Attack("Hyperspace Hole"));
                    pokemon.update(EnumUpdateType.Moveset);
                    pokemon.getEntityData().removeTag("unboundTime");
                    pokemon.writeToNBT(nbt);
                    ChatHandler.sendChat(player, "pixelmon.abilities.changeform", nbt.getString("Name"));
                });
            }
        } else if ((nbt.getString("Name").equals(EnumSpecies.Wooloo.name) || nbt.getString("Name").equals(EnumSpecies.Mareep.name)) && nbt.getInteger("Variant") == 16) {
            if (!nbt.getCompoundTag("ForgeData").hasKey("fluffTime") || System.currentTimeMillis() > nbt.getCompoundTag("ForgeData").getLong("fluffTime") + (long)PixelmonConfig.furRegrowth) {
                player.world.getMinecraftServer().addScheduledTask(() -> {
                    EntityPixelmon pokemon = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt, player.getServerWorld());
                    pokemon.setForm(0, true);
                    pokemon.getEntityData().removeTag("fluffTime");
                    pokemon.writeToNBT(nbt);
                    ChatHandler.sendChat(player, "pixelmon.fur_regrown", nbt.getString("Name"));
                });
            }
        } else if (nbt.getCompoundTag("ForgeData").hasKey("specialForm") && nbt.getCompoundTag("ForgeData").getInteger("specialForm") > 0) {
            player.world.getMinecraftServer().addScheduledTask(() -> {
                EntityPixelmon pokemon = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt, player.getServerWorld());
                pokemon.setForm(nbt.getCompoundTag("ForgeData").getInteger("specialForm"), true);
                pokemon.getEntityData().removeTag("specialForm");
                pokemon.writeToNBT(nbt);
            });
        }
    }

    @SubscribeEvent
    public void onBattleEnd(BattleEndEvent event) {
        if (event.getBattleController().isHordeBattle() && event.getEndCause() == EnumBattleEndCause.NORMAL) {
            ArrayList<BattleParticipant> players = new ArrayList<>();
            WildPixelmonParticipant wildPokemon = null;
            int wildPokemonDefeated = 0;
            for (BattleParticipant battleParticipant : event.getBattleController().participants) {
                if (battleParticipant instanceof WildPixelmonParticipant && battleParticipant.isDefeated) {
                    ++wildPokemonDefeated;
                    continue;
                }
                if (!(battleParticipant instanceof PlayerParticipant)) continue;
                players.add((BattleParticipant)((Object)((PlayerParticipant)battleParticipant).player));
            }
            if (wildPokemonDefeated == event.getBattleController().getWildPokemonCount()) {
                ArrayList<DroppedItem> givenDrops = new ArrayList<DroppedItem>();
                for (Map.Entry<Integer, PokemonSpec> entry : event.getBattleController().hordePokemonParticipants.entrySet()) {
                    int n;
                    try {
                        n = entry.getValue().form;
                    }
                    catch (NullPointerException er) {
                        n = 0;
                    }
                    EnumSpecies species = entry.getValue().getSpecies();
                    DropItemRegistry.getDropsForPokemon(species, n).forEach(itemStack -> {
                        itemStack.setCount(itemStack.getCount());
                        givenDrops.add(DroppedItem.of(itemStack));
                    });
                }
                if (givenDrops.size() > 0) {
                    DropQueryFactory.newInstance().addDrops(givenDrops).addTargets(players.toArray(new EntityPlayerMP[0])).send();
                }
            }
        }
        for (BattleParticipant participant : event.getBattleController().participants) {
            for (PixelmonWrapper pixelmonWrapper : participant.controlledPokemon) {
                if (pixelmonWrapper.getHeldItem() == null) continue;
                pixelmonWrapper.getHeldItem().applyAfterBattleEffect();
            }
        }
        for (BattleParticipant bp : event.getBattleController().participants) {
            if (bp.getStorage() == null) continue;
            for (NBTTagCompound nbt : bp.getStorage().partyPokemon) {
                if (nbt == null || !nbt.getString("Name").equals(EnumSpecies.Meloetta.name) && !nbt.getString("Name").equals(EnumSpecies.Xerneas.name)) continue;
                ((EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt, bp.getWorld())).setForm(0);
            }
        }
        int amount = (int)event.getBattleResults().entrySet().stream().filter(a -> this.check((Map.Entry<BattleParticipant, BattleResults>)a, ParticipantType.Trainer, BattleResults.DEFEAT)).count();
        if (amount > 0) {
            event.getBattleResults().entrySet().stream().filter(a -> this.check((Map.Entry<BattleParticipant, BattleResults>)a, ParticipantType.Player, BattleResults.VICTORY)).map(Map.Entry::getKey).map(PlayerParticipant.class::cast).map(a -> a.player).forEach(player -> {
                ArrayList<ItemStack> playerInv = new ArrayList<ItemStack>(player.inventory.mainInventory);
                playerInv.addAll(player.inventory.offHandInventory);
                for (ItemStack item : playerInv) {
                    int dmg = item.getItemDamage();
                    if (item.getItem() != PixelmonItems.sparklingStone) continue;
                    item.setItemDamage(dmg + amount);
                    return;
                }
            });
        }
        int wildAmount = (int) event.getBattleResults().entrySet().stream().filter(a -> a.getKey().getType() == ParticipantType.WildPokemon).count();
        if (wildAmount > 0 && !event.getPlayers().isEmpty()) {
            event.getPlayers().forEach(player -> {
                NBTLink link;
                PokerusEvent.InfectEvent pokerusEvent;
                Random random;
                PlayerStorage playerStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
                NBTTagCompound nbt = RandomHelper.getRandomElementFromList(playerStorage.getTeam());
                if (nbt.getInteger("PokeRus") == 0 && (random = new Random()).nextInt(PixelmonConfig.pokeRusChance) == 1 && !MinecraftForge.EVENT_BUS.post(pokerusEvent = new PokerusEvent.InfectEvent((EntityPlayerMP)player, link = new NBTLink(nbt)))) {
                    nbt = pokerusEvent.getPokemonLink().getNBT();
                    nbt.setInteger("PokeRus", 1);
                    playerStorage.changePokemon(playerStorage.getPosition(PixelmonMethods.getID(nbt)), nbt);
                    playerStorage.sendUpdatedList();
                    TextComponentTranslation msg = new TextComponentTranslation("pokerus.infected", !nbt.getString("Nickname").isEmpty() ? nbt.getString("Nickname") : nbt.getString("Name"));
                    msg.getStyle().setColor(TextFormatting.GREEN).setBold(true);
                    player.sendMessage(msg);
                }
            });
        }
    }

    private boolean check(Map.Entry<BattleParticipant, BattleResults> entry, ParticipantType type, BattleResults results) {
        return entry.getKey().getType() == type && entry.getValue() == results;
    }

    @SubscribeEvent
    public void onAggro(AggressionEvent event) {
        if (event.getAggressor() instanceof EntityPixelmon) {
            EntityPlayerMP player;
            EntityPixelmon pokemon;
            if (event.getTarget() != null && event.getTarget() instanceof EntityPixelmon && (pokemon = (EntityPixelmon)event.getTarget()).isNoble()) {
                event.setCanceled(true);
                return;
            }
            if (event.getPlayer() != null && NobleBattle.nobleBattleMap.containsKey((player = event.getPlayer()).getUniqueID())) {
                event.setCanceled(true);
            }
        }
    }

    @SubscribeEvent
    public void onBattleStart(BattleStartEvent event) {
        PlayerParticipant pp = null;
        WildPixelmonParticipant wpp = null;
        if (event.getBattleController().participants.get(0) instanceof PlayerParticipant && event.getBattleController().participants.get(1) instanceof WildPixelmonParticipant) {
            pp = (PlayerParticipant)event.getBattleController().participants.get(0);
            wpp = (WildPixelmonParticipant)event.getBattleController().participants.get(1);
        } else if (event.getBattleController().participants.get(1) instanceof PlayerParticipant && event.getBattleController().participants.get(0) instanceof WildPixelmonParticipant) {
            pp = (PlayerParticipant)event.getBattleController().participants.get(1);
            wpp = (WildPixelmonParticipant)event.getBattleController().participants.get(0);
        }
        if (pp != null && NobleBattle.nobleBattleMap.containsKey(pp.player.getUniqueID())) {
            event.setCanceled(true);
            return;
        }
        if (wpp != null) {
            EntityPixelmon wildPokemon = ((PixelmonWrapper)wpp.controlledPokemon.get((int)0)).pokemon;
            if (wildPokemon.isNoble()) {
                event.setCanceled(true);
                return;
            }
            for (String tag : wildPokemon.getTags()) {
                if (!tag.equalsIgnoreCase("MountPokemon")) continue;
                event.setCanceled(true);
                break;
            }
        }
        if (!PixelmonConfig.allowMissingNoCapture) {
            event.getBattleController().participants.stream().filter(bp -> bp.getType() == ParticipantType.WildPokemon).forEach(bp -> {
                if (bp.getActiveUnfaintedPokemon().stream().anyMatch(pw -> pw.getSpecies() == EnumSpecies.MissingNo && pw.getForm() < 5)) {
                    event.setCanceled(true);
                }
            });
        }
        event.getBattleController().participants.forEach(bp -> {
            if (bp.getStorage() != null) {
                for (NBTTagCompound nbt : bp.getStorage().partyPokemon) {
                    if (nbt == null || !nbt.getString("Name").equals(EnumSpecies.Xerneas.name)) continue;
                    ((EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt, bp.getWorld())).setForm(1);
                }
            }
            for (PixelmonWrapper pw : bp.controlledPokemon) {
                if (pw != null && pw.getPokemonName().equals(EnumSpecies.Xerneas.name)) {
                    pw.setForm(1);
                    continue;
                }
                if (pw == null || !pw.getPokemonName().equals(EnumSpecies.Keldeo.name)) continue;
                if (pw.getMoveset().hasAttack("Secret Sword") && pw.getForm() != 1) {
                    pw.setForm(1);
                    continue;
                }
                if (pw.getMoveset().hasAttack("Secret Sword") || pw.getForm() == 0) continue;
                pw.setForm(0);
            }
        });
    }

    @SubscribeEvent
    public void onMoveLearned(MovesetEvent.LearntMoveEvent event) {
        PokemonLink pokemonLink = event.getPokemon();
        if (pokemonLink.getSpecies() == EnumSpecies.Keldeo) {
            if (event.getLearntAttack().isAttack("Secret Sword") && pokemonLink.getForm() != EnumKeldeo.RESOLUTE.getForm()) {
                pokemonLink.getEntity().setForm(EnumKeldeo.RESOLUTE.getForm(), true);
            } else if (!event.getMoveset().hasAttack("Secret Sword") && pokemonLink.getForm() == EnumKeldeo.RESOLUTE.getForm()) {
                pokemonLink.getEntity().setForm(EnumKeldeo.ORDINARY.getForm(), true);
            }
        }
    }

    @SubscribeEvent
    public void onMoveForgot(MovesetEvent.ForgotMoveEvent event) {
        PokemonLink pokemonLink = event.getPokemon();
        if (pokemonLink.getSpecies() == EnumSpecies.Keldeo) {
            if (event.getForgottenAttack().isAttack("Secret Sword")) {
                pokemonLink.getEntity().setForm(EnumKeldeo.ORDINARY.getForm(), true);
            }
        }
    }

    @SubscribeEvent
    public void onPostEvo(EvolveEvent.PostEvolve event) {
        EntityPixelmon pokemon = event.getPokemon();
        if (pokemon.getSpecies() == EnumSpecies.Vivillon) {
            Biome biome = pokemon.getEntityWorld().getBiome(pokemon.getPosition());
            EnumVivillon vivillon = EnumVivillon.findFormForBiome(biome);
            pokemon.setForm(vivillon.getForm(), true);
        } else if (pokemon.getSpecies() == EnumSpecies.Meowstic && pokemon.getGender() == Gender.Female) {
            pokemon.setForm(1, true);
            if (pokemon.getAbilitySlot() == 2) {
                pokemon.setAbility(new Competitive());
            }
        }
    }

    private static boolean checkTrade(Entity entity, NBTTagCompound pokemon) {
        if (UNTRADEABLE.matches(pokemon)) {
            entity.sendMessage(new TextComponentTranslation("error.untradeable", pokemon.getString("Name")));
            return true;
        }
        return false;
    }

    @SubscribeEvent
    public void onTrade(PixelmonTradeEvent.PlayerTradeEvent event) {
        if (PixelmonListener.checkTrade(event.getPlayerOne(), event.getPokemonOne()) || PixelmonListener.checkTrade(event.getPlayerTwo(), event.getPokemonTwo())) {
            event.setCanceled(true);
        }
    }

    @SubscribeEvent
    public void onGrassTrigger(PixelmonBlockStartingBattleEvent event) {
        if (PixelmonConfig.hordesEnabled) {
            HordeTriggeredEvent hordeEvent;
            EntityPlayerMP player = event.player;
            if (RandomHelper.getRandomNumberBetween(1, PixelmonConfig.hordeTriggerChance) == 1 && !MinecraftForge.EVENT_BUS.post(hordeEvent = new HordeTriggeredEvent(event.wildPixelmon1, event.player))) {
                event.wildPixelmon1.setDead();
                BattleFactory.createHordeBattle(player, hordeEvent.getHordePokemon());
                event.setCanceled(true);
            }
        }
    }

    @SubscribeEvent
    public void onReceive(PixelmonReceivedEvent event) {
        if (event.getPokemon().isAlpha()) {
            event.getPokemon().removeColorTint();
        }
    }

    @SubscribeEvent
    public void onUpdate(PixelmonUpdateEvent event) {
        EntityPlayer closestPlayer;
        EntityPixelmon pokemon;
        if (event.getTickPhase() == TickEvent.Phase.START && !(pokemon = event.getPokemon()).hasOwner() && (pokemon.isAlpha() && PixelmonServerConfig.alphasSpawnHostile || pokemon.isNoble()) && (closestPlayer = pokemon.world.getClosestPlayer(pokemon.posX, pokemon.posY, pokemon.posZ, 7.0, false)) != null) {
            pokemon.setHostile(true, closestPlayer);
        }
    }

    @SubscribeEvent
    public void onNobleDefeat(DefeatNobleEvent event) {
        if (!PixelmonServerConfig.allowCelestialFlute) {
            return;
        }
        EntityPlayerMP player = event.getPlayer();
        PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage(player).get();
        if (!storage.mountData.hasFlute) {
            storage.mountData.hasFlute = true;
        }
        EntityPixelmon pokemon = event.getPokemon();
        PokemonSpec spec = null;
        if (pokemon.getSpecies() == EnumSpecies.Kleavor) {
            for (Map.Entry<PokemonSpec, Boolean> entry : storage.mountData.mountRegistry.entrySet()) {
                if (!entry.getKey().name.equalsIgnoreCase("Wyrdeer")) continue;
                spec = entry.getKey();
                break;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Lilligant) {
            for (Map.Entry<PokemonSpec, Boolean> entry : storage.mountData.mountRegistry.entrySet()) {
                if (!entry.getKey().name.equalsIgnoreCase("Ursaluna")) continue;
                spec = entry.getKey();
                break;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Arcanine) {
            for (Map.Entry<PokemonSpec, Boolean> entry : storage.mountData.mountRegistry.entrySet()) {
                if (!entry.getKey().name.equalsIgnoreCase("Basculegion")) continue;
                spec = entry.getKey();
                break;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Electrode) {
            for (Map.Entry<PokemonSpec, Boolean> entry : storage.mountData.mountRegistry.entrySet()) {
                if (!entry.getKey().name.equalsIgnoreCase("Sneasler")) continue;
                spec = entry.getKey();
                break;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Avalugg) {
            for (Map.Entry<PokemonSpec, Boolean> entry : storage.mountData.mountRegistry.entrySet()) {
                if (!entry.getKey().name.equalsIgnoreCase("Braviary")) continue;
                spec = entry.getKey();
                break;
            }
        }
        if (spec != null) {
            storage.mountData.mountRegistry.put(spec, true);
            player.sendMessage(new TextComponentString("You've unlocked " + spec.name + "!"));
        }
    }

    @SubscribeEvent
    public void onPokeBallImpact(PokeballImpactEvent event) {
        if (event.getEntityHit() instanceof EntityPixelmon) {
            EntityPixelmon pokemon = (EntityPixelmon)event.getEntityHit();
            for (String tag : pokemon.getTags()) {
                if (!tag.equalsIgnoreCase("MountPokemon")) continue;
                event.setCanceled(true);
                break;
            }
        }
    }

    @SubscribeEvent
    public void onDespawnEvent(DespawnEvent event) {
        if (event.getPokemon().isNoble()) {
            NobleBattle.nobleBattleMap.entrySet().removeIf(entry -> {
                NobleBattle battle = (NobleBattle)entry.getValue();
                if (battle.getPokemon().getEntityPixelmon().getUniqueID().toString().equalsIgnoreCase(event.getPokemon().getUniqueID().toString())) {
                    NobleBattlePlayer battlePlayer = battle.getPlayer();
                    battlePlayer.getEntityPlayer().sendMessage(new TextComponentString(battle.getPokemon().getEntityPixelmon().getPokemonName() + " fled!"));
                    battle.getFrenzyGauge().setVisible(false);
                    battle.getFrenzyGauge().removePlayer(battle.getPlayer().getEntityPlayer());
                    return true;
                }
                return false;
            });
        }
    }

    @SubscribeEvent
    public void onSTDStart(SpaceTimeDistortionEvent.StartEvent event) {
        PixelmonTasks.startSTDSpawns(event);
    }

    @SubscribeEvent
    public void onSTDEnd(SpaceTimeDistortionEvent.EndEvent event) {
        PixelmonTasks.stopSTDSpawns(event);
        PixelmonTasks.spawnedSpaceTimeDistortions = Math.max(0, PixelmonTasks.spawnedSpaceTimeDistortions - 1);
    }
}

