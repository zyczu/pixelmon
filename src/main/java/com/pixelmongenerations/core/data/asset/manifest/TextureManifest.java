/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.asset.manifest;

import com.pixelmongenerations.core.data.asset.IAssetManifest;
import com.pixelmongenerations.core.data.asset.entry.TextureAsset;
import com.pixelmongenerations.core.network.packetHandlers.SyncManifest;
import java.util.ArrayList;
import java.util.List;

public class TextureManifest
implements IAssetManifest<TextureAsset> {
    private List<TextureAsset> resources = new ArrayList<TextureAsset>();

    @Override
    public SyncManifest.AssetManifestType getType() {
        return SyncManifest.AssetManifestType.TEXTURE;
    }

    @Override
    public boolean addAsset(TextureAsset resource) {
        return this.resources.add(resource);
    }

    @Override
    public List<TextureAsset> getAssets() {
        return this.resources;
    }

    @Override
    public void setAssets(List<TextureAsset> resources) {
        this.resources = resources;
    }

    @Override
    public String toJson() {
        return GSON.toJson((Object)this);
    }

    @Override
    public void loadFromJson(String json) {
        this.setAssets(((TextureManifest)GSON.fromJson(json, TextureManifest.class)).getAssets());
    }
}

