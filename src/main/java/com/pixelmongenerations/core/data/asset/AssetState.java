/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.asset;

public enum AssetState {
    Start,
    Downloading,
    Downloaded,
    Loading,
    Loaded,
    Failed;


    public boolean shouldRemove() {
        return this == Failed || this == Downloaded;
    }
}

