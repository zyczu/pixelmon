/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.asset.manifest;

import com.pixelmongenerations.core.data.asset.IAssetManifest;
import com.pixelmongenerations.core.data.asset.entry.MinecraftModelAsset;
import com.pixelmongenerations.core.network.packetHandlers.SyncManifest;
import java.util.ArrayList;
import java.util.List;

public class MinecraftModelManifest
implements IAssetManifest<MinecraftModelAsset> {
    private List<MinecraftModelAsset> resources = new ArrayList<MinecraftModelAsset>();

    @Override
    public SyncManifest.AssetManifestType getType() {
        return SyncManifest.AssetManifestType.MINECRAFT_MODEL;
    }

    @Override
    public boolean addAsset(MinecraftModelAsset resource) {
        return this.resources.add(resource);
    }

    @Override
    public List<MinecraftModelAsset> getAssets() {
        return this.resources;
    }

    @Override
    public void setAssets(List<MinecraftModelAsset> resources) {
        this.resources = resources;
    }

    @Override
    public String toJson() {
        return GSON.toJson((Object)this);
    }

    @Override
    public void loadFromJson(String json) {
        this.setAssets(((MinecraftModelManifest)GSON.fromJson(json, MinecraftModelManifest.class)).getAssets());
    }
}

