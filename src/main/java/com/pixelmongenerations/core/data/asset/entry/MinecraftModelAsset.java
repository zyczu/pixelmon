/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.asset.entry;

import com.pixelmongenerations.common.cosmetic.PositionOperation;
import com.pixelmongenerations.core.data.asset.type.MinecraftModelType;
import java.util.Arrays;

public class MinecraftModelAsset {
    public String modelName;
    public String modelId;
    public MinecraftModelType modelType;
    public String modelURL;
    public String defaultTextureName;
    public PositionOperation[] operations;
    public PositionOperation[] thirdPersonOperations;
    public PositionOperation[] guiOperations;
    public PositionOperation[] armorStandOperations;
    public boolean preLoad;

    public String toString() {
        return "MinecraftModelAsset [modelName=" + this.modelName + ", modelId=" + this.modelId + ", modelType=" + (Object)((Object)this.modelType) + ", modelURL=" + this.modelURL + ", defaultTextureName=" + this.defaultTextureName + ", operations=" + Arrays.toString(this.operations) + "]";
    }
}

