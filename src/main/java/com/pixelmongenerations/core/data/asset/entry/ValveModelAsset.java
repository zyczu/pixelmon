/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.asset.entry;

import com.pixelmongenerations.common.cosmetic.PositionOperation;
import com.pixelmongenerations.core.data.asset.type.ValveModelType;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.ArrayList;
import java.util.Arrays;

public class ValveModelAsset {
    public String modelName;
    public String modelId;
    public ValveModelType modelType;
    public EnumSpecies species;
    public String pqcURL;
    public ArrayList<String> smdURLs;
    public PositionOperation[] operations;
    public boolean preLoad;

    public String toString() {
        return "ValveModelAsset [modelName=" + this.modelName + ", modelId=" + this.modelId + ", modelType=" + (Object)((Object)this.modelType) + ", species=" + (Object)((Object)this.species) + ", pqcURL=" + this.pqcURL + ", smdURLs=" + this.smdURLs + ", operations=" + Arrays.toString(this.operations) + "]";
    }
}

