/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.apache.logging.log4j.Level
 */
package com.pixelmongenerations.core.data.pokemon;

import com.pixelmongenerations.api.def.PokemonDefSerializer;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.data.pokemon.PokemonRegistry;
import com.pixelmongenerations.core.util.helper.RCFileHelper;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import net.minecraft.util.ResourceLocation;
import org.apache.logging.log4j.Level;

public class PokemonLoader {
    public static final String POKEMON_DEF_ROOT = "pixelmon/pokemon/";
    public static final String DEFAULT_POKEMON_SET_FOLDER = "default";
    public static final String FILE_NAME_ENDING = ".def.json";

    public static ArrayList<BaseStats> importSetsFrom(String dir) {
        ArrayList<BaseStats> setList = new ArrayList<BaseStats>();
        File file = new File(dir);
        if (file.exists()) {
            ArrayList<File> jsons = new ArrayList<File>();
            if (file.isDirectory()) {
                PokemonLoader.recursiveSetSearch(dir, jsons);
            } else if (dir.endsWith(FILE_NAME_ENDING)) {
                jsons.add(file);
            }
            for (File json : jsons) {
                try {
                    BaseStats def = PokemonDefSerializer.deserialize(new FileReader(json));
                    setList.add(def);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return setList;
    }

    public static void recursiveSetSearch(String dir, ArrayList<File> jsons) {
        File file = new File(dir);
        for (String name : file.list()) {
            File subFile = new File(dir + "/" + name);
            if (subFile.isFile() && name.endsWith(FILE_NAME_ENDING)) {
                jsons.add(subFile);
                continue;
            }
            if (!subFile.isDirectory()) continue;
            PokemonLoader.recursiveSetSearch(dir + "/" + name, jsons);
        }
    }

    public static void checkForMissingPokemon() {
        File file = new File("pixelmon/pokemon/default/");
        file.mkdirs();
        PokemonLoader.retrievePokemonFromAssets();
    }

    public static List<BaseStats> retrievePokemonFromAssets() {
        ArrayList<BaseStats> spawnSets = new ArrayList<BaseStats>();
        try {
            Path path = RCFileHelper.pathFromResourceLocation(new ResourceLocation("pixelmon", "pokemon/"));
            List<Path> setPaths = RCFileHelper.listFilesRecursively(path, entry -> entry.getFileName().toString().endsWith(FILE_NAME_ENDING), true);
            Iterator<Path> iterator = setPaths.iterator();
            while (iterator.hasNext()) {
                Path setPath;
                Path lowerPath = setPath = iterator.next();
                String root = "/assets/pixelmon/pokemon/";
                String assetPath = "";
                while (!lowerPath.getParent().endsWith("pokemon")) {
                    assetPath = lowerPath.getParent().getFileName().toString() + "/" + assetPath;
                    lowerPath = lowerPath.getParent();
                }
                InputStream iStream = PokemonLoader.class.getResourceAsStream(root + assetPath + setPath.getFileName());
                if (iStream == null) {
                    Pixelmon.LOGGER.log(Level.WARN, "Couldn't find internal pokemon JSON at " + root + assetPath + setPath.getFileName());
                    continue;
                }
                String json = "";
                BaseStats set = null;
                try {
                    Scanner s = new Scanner(iStream);
                    s.useDelimiter("\\A");
                    json = s.hasNext() ? s.next() : "";
                    s.close();
                    set = PokemonDefSerializer.deserialize(json);
                }
                catch (Exception e) {
                    Pixelmon.LOGGER.error("Couldn't load pokemon JSON: " + root + assetPath + setPath.getFileName());
                    e.printStackTrace();
                    continue;
                }
                spawnSets.add(set);
                if (PixelmonConfig.useExternalJSONFilesPokemon) {
                    String primaryPath = "./pixelmon/pokemon/default";
                    String relevantPath = "";
                    while (!setPath.getParent().endsWith("pokemon")) {
                        relevantPath = setPath.getParent().getFileName().toString() + "/" + relevantPath;
                        setPath = setPath.getParent();
                    }
                    String fileName = set.pokemon.name + "_" + set.id;
                    File file = new File(primaryPath + "/" + relevantPath + fileName + FILE_NAME_ENDING);
                    if (!file.exists()) {
                        file.getParentFile().mkdirs();
                        PrintWriter pw = new PrintWriter(file);
                        pw.write(json);
                        pw.flush();
                        pw.close();
                    }
                }
                try {
                    iStream.close();
                }
                catch (IOException var15) {
                    var15.printStackTrace();
                }
            }
            return spawnSets;
        }
        catch (Exception var17) {
            var17.printStackTrace();
            return spawnSets;
        }
    }

    public static void loadPokemon() {
        ArrayList<BaseStats> standard;
        Pixelmon.LOGGER.info("Registering Pokemon.");
        if (PixelmonConfig.useExternalJSONFilesMoves) {
            File spawnSetDir = new File("pixelmon/pokemon/default");
            if (!spawnSetDir.isDirectory()) {
                Pixelmon.LOGGER.info("Creating Pokemon directory");
                spawnSetDir.mkdirs();
            }
            PokemonLoader.checkForMissingPokemon();
            standard = PokemonLoader.importSetsFrom("pixelmon/pokemon/default/");
        } else {
            standard = new ArrayList<BaseStats>(PokemonLoader.retrievePokemonFromAssets());
        }
        PokemonRegistry.registerBases(standard);
    }
}

