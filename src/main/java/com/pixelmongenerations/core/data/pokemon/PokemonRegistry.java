/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.data.pokemon;

import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.ArrayList;
import java.util.Optional;

public class PokemonRegistry {
    public static ArrayList<BaseStats> defs = new ArrayList();

    public static void registerBases(ArrayList<BaseStats> defList) {
        defList.forEach(BaseStats::init);
        defs.addAll(defList);
        defs.forEach(BaseStats::postInit);
    }

    public static BaseStats getBaseFor(EnumSpecies species, int form) {
        Optional<BaseStats> stats = defs.stream().filter(baseStats -> baseStats.pokemon == species && baseStats.form == form).findFirst();
        if (!stats.isPresent() && form == -1) {
            return PokemonRegistry.getBaseFor(species, 0);
        }
        if (stats.isPresent()) {
            return stats.get();
        }
        if (PokemonRegistry.getBaseFor(species, -1) != null) {
            return PokemonRegistry.getBaseFor(species, -1);
        }
        return null;
    }

    public static int getFormForPokemonId(int pokemonId) {
        Optional<BaseStats> stats = defs.stream().filter(baseStats -> baseStats.id == pokemonId).findFirst();
        return stats.isPresent() ? stats.get().form : -1;
    }

    public static BaseStats getBaseFor(int pokemonId) {
        Optional<BaseStats> stats = defs.stream().filter(baseStats -> baseStats.id == pokemonId).findFirst();
        return stats.isPresent() ? stats.get() : null;
    }

    public static BaseStats getBaseFor(String name) {
        Optional<BaseStats> stats = defs.stream().filter(baseStats -> baseStats.pixelmonName.equalsIgnoreCase(name)).findFirst();
        return stats.isPresent() ? stats.get() : null;
    }

    public static ArrayList<BaseStats> getAllPokemon() {
        return defs;
    }
}

