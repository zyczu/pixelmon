/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.apache.logging.log4j.Level
 */
package com.pixelmongenerations.core.data.moves;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.AttackBase;
import com.pixelmongenerations.common.battle.attacks.AttackBaseSerializer;
import com.pixelmongenerations.common.battle.attacks.RegularAttackBase;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.util.helper.RCFileHelper;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import net.minecraft.util.ResourceLocation;
import org.apache.logging.log4j.Level;

public class MoveLoader {
    public static final String MOVES_ROOT = "pixelmon/moves/";
    public static final String DEFAULT_MOVES_FOLDER = "default";

    public static ArrayList<AttackBase> importSetsFrom(String dir) {
        ArrayList<AttackBase> setList = new ArrayList<AttackBase>();
        File file = new File(dir);
        if (file.exists()) {
            ArrayList<File> jsons = new ArrayList<File>();
            if (file.isDirectory()) {
                MoveLoader.recursiveSetSearch(dir, jsons);
            } else if (dir.endsWith(".move.json")) {
                jsons.add(file);
            }
            for (File json : jsons) {
                try {
                    RegularAttackBase move = AttackBaseSerializer.deserialize(new FileReader(json));
                    setList.add(move);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return setList;
    }

    public static void recursiveSetSearch(String dir, ArrayList<File> jsons) {
        File file = new File(dir);
        for (String name : file.list()) {
            File subFile = new File(dir + "/" + name);
            if (subFile.isFile() && name.endsWith(".move.json")) {
                jsons.add(subFile);
                continue;
            }
            if (!subFile.isDirectory()) continue;
            MoveLoader.recursiveSetSearch(dir + "/" + name, jsons);
        }
    }

    public static void checkForMissingMoves() {
        File file = new File("pixelmon/moves/default/");
        file.mkdirs();
        MoveLoader.retrieveMoveFromAssets();
    }

    public static List<RegularAttackBase> retrieveMoveFromAssets() {
        ArrayList<RegularAttackBase> spawnSets = new ArrayList<RegularAttackBase>();
        try {
            Path path = RCFileHelper.pathFromResourceLocation(new ResourceLocation("pixelmon", "moves/"));
            List<Path> setPaths = RCFileHelper.listFilesRecursively(path, entry -> entry.getFileName().toString().endsWith(".json"), true);
            Iterator<Path> iterator = setPaths.iterator();
            while (iterator.hasNext()) {
                Path setPath;
                Path lowerPath = setPath = iterator.next();
                String root = "/assets/pixelmon/moves/";
                String assetPath = "";
                while (!lowerPath.getParent().endsWith("moves")) {
                    assetPath = lowerPath.getParent().getFileName().toString() + "/" + assetPath;
                    lowerPath = lowerPath.getParent();
                }
                InputStream iStream = MoveLoader.class.getResourceAsStream(root + assetPath + setPath.getFileName());
                if (iStream == null) {
                    Pixelmon.LOGGER.log(Level.WARN, "Couldn't find internal move JSON at " + root + assetPath + setPath.getFileName());
                    continue;
                }
                String json = "";
                RegularAttackBase set = null;
                try {
                    Scanner s = new Scanner(iStream);
                    s.useDelimiter("\\A");
                    json = s.hasNext() ? s.next() : "";
                    s.close();
                    set = AttackBaseSerializer.deserialize(json);
                }
                catch (Exception e) {
                    Pixelmon.LOGGER.error("Couldn't load move JSON: " + root + assetPath + setPath.getFileName());
                    e.printStackTrace();
                    continue;
                }
                spawnSets.add(set);
                if (PixelmonConfig.useExternalJSONFilesSpawning) {
                    String primaryPath = "./pixelmon/moves/default";
                    String relevantPath = "";
                    while (!setPath.getParent().endsWith("moves")) {
                        relevantPath = setPath.getParent().getFileName().toString() + "/" + relevantPath;
                        setPath = setPath.getParent();
                    }
                    File file = new File(primaryPath + "/" + relevantPath + set.getUnlocalizedName() + ".move.json");
                    if (!file.exists()) {
                        file.getParentFile().mkdirs();
                        PrintWriter pw = new PrintWriter(file);
                        pw.write(json);
                        pw.flush();
                        pw.close();
                    }
                }
                try {
                    iStream.close();
                }
                catch (IOException var15) {
                    var15.printStackTrace();
                }
            }
            return spawnSets;
        }
        catch (Exception var17) {
            var17.printStackTrace();
            return spawnSets;
        }
    }

    public static void loadMoves() {
        ArrayList<AttackBase> standard = new ArrayList<AttackBase>();
        Pixelmon.LOGGER.info("Registering moves.");
        if (PixelmonConfig.useExternalJSONFilesMoves) {
            File spawnSetDir = new File("pixelmon/moves/default");
            if (!spawnSetDir.isDirectory()) {
                Pixelmon.LOGGER.info("Creating spawning directory");
                spawnSetDir.mkdirs();
            }
            MoveLoader.checkForMissingMoves();
            standard = MoveLoader.importSetsFrom("pixelmon/moves/default/");
        } else {
            List<RegularAttackBase> internalSets = MoveLoader.retrieveMoveFromAssets();
            standard.addAll(internalSets);
        }
        Attack.registerMoves(standard);
    }
}

