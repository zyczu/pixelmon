/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.eventbus.EventBus
 *  com.google.common.eventbus.Subscribe
 */
package com.pixelmongenerations.core.plugin;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import java.net.MalformedURLException;
import net.minecraftforge.fml.common.DummyModContainer;
import net.minecraftforge.fml.common.LoadController;
import net.minecraftforge.fml.common.ModMetadata;
import net.minecraftforge.fml.common.event.FMLConstructionEvent;

public class DummyContainer
extends DummyModContainer {
    public DummyContainer() {
        super(new ModMetadata());
        ModMetadata meta = this.getMetadata();
        meta.modId = "aearlymodloader";
        meta.name = "AEarlyModLoader";
        meta.version = "8.7.1";
    }

    @Override
    public boolean registerBus(EventBus bus, LoadController controller) {
        bus.register((Object)this);
        return true;
    }

    @Subscribe
    public void modConstruction(FMLConstructionEvent event) throws MalformedURLException {
    }
}

