/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.plugin;

import com.pixelmongenerations.common.world.ultraspace.IUltraWorld;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.player.EntityPlayer;

public class TransformerHooks {
    public static double getGravityForEntity(Entity entity) {
        if (entity.world.provider instanceof IUltraWorld) {
            if (entity instanceof EntityChicken) {
                return 0.08;
            }
            IUltraWorld customProvider = (IUltraWorld)((Object)entity.world.provider);
            if (entity instanceof EntityPlayer) {
                EntityPlayer player = (EntityPlayer)entity;
                if (player.inventory != null) {
                    int armorModLowGrav = 100;
                    int armorModHighGrav = 100;
                    if (customProvider.getGravity() > 0.0f) {
                        return 0.08 - (double)(customProvider.getGravity() * (float)armorModLowGrav / 100.0f);
                    }
                    return 0.08 - (double)(customProvider.getGravity() * (float)armorModHighGrav / 100.0f);
                }
            }
            return 0.08 - (double)customProvider.getGravity();
        }
        return 0.08;
    }

    public static double getItemGravity(EntityItem e) {
        if (e.world.provider instanceof IUltraWorld) {
            IUltraWorld customProvider = (IUltraWorld)((Object)e.world.provider);
            return Math.max(0.002, (double)0.04f - (double)customProvider.getGravity() / 1.75);
        }
        return 0.04f;
    }
}

