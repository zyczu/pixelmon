/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  net.minecraft.launchwrapper.IClassTransformer
 *  net.minecraft.launchwrapper.Launch
 *  org.objectweb.asm.ClassReader
 *  org.objectweb.asm.ClassVisitor
 *  org.objectweb.asm.ClassWriter
 *  org.objectweb.asm.tree.AbstractInsnNode
 *  org.objectweb.asm.tree.ClassNode
 *  org.objectweb.asm.tree.LdcInsnNode
 *  org.objectweb.asm.tree.MethodInsnNode
 *  org.objectweb.asm.tree.MethodNode
 *  org.objectweb.asm.tree.VarInsnNode
 */
package com.pixelmongenerations.core.plugin;

import java.util.HashMap;
import net.minecraft.launchwrapper.IClassTransformer;
import net.minecraft.launchwrapper.Launch;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.common.asm.transformers.deobf.FMLDeobfuscatingRemapper;
import net.minecraftforge.fml.relauncher.FMLInjectionData;
import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.VarInsnNode;

@IFMLLoadingPlugin.TransformerExclusions(value={"com.pixelmongenerations.core.plugin"})
public class PixelmonTransformer
implements IClassTransformer {
    HashMap<String, ObfuscationEntry> nodemap = new HashMap();
    private boolean deobfuscated = true;
    private String mcVersion = (String)FMLInjectionData.data()[4];
    private String nameForgeHooksClient;
    private String nameEntityLiving;
    private String nameEntityItem;
    private String nameEntity;
    private static final String KEY_CLASS_FORGE_HOOKS_CLIENT = "forgeHooks";
    private static final String KEY_CLASS_ENTITY = "entityClass";
    private static final String KEY_CLASS_ENTITY_LIVING = "entityLivingClass";
    private static final String KEY_CLASS_ENTITY_ITEM = "entityItemClass";
    private static final String KEY_METHOD_MOVE_ENTITY = "moveEntityMethod";
    private static final String KEY_METHOD_ON_UPDATE = "onUpdateMethod";
    private static final String CLASS_TRANSFORMER_HOOKS = "com/pixelmongenerations/core/plugin/TransformerHooks";
    private static int operationCount = 0;
    private static int injectionCount = 0;

    public PixelmonTransformer() {
        try {
            this.deobfuscated = Launch.classLoader.getClassBytes("net.minecraft.world.World") != null;
        }
        catch (Exception exception) {
            // empty catch block
        }
        if (this.mcVersionMatches("[1.12.2]")) {
            this.nodemap.put(KEY_CLASS_ENTITY_LIVING, new ObfuscationEntry("net/minecraft/entity/EntityLivingBase", "vp"));
            this.nodemap.put(KEY_CLASS_ENTITY_ITEM, new ObfuscationEntry("net/minecraft/entity/item/EntityItem", "acl"));
            this.nodemap.put(KEY_CLASS_ENTITY, new ObfuscationEntry("net/minecraft/entity/Entity", "vg"));
            this.nodemap.put(KEY_CLASS_FORGE_HOOKS_CLIENT, new ObfuscationEntry("net/minecraftforge/client/ForgeHooksClient"));
            this.nodemap.put(KEY_METHOD_MOVE_ENTITY, new MethodObfuscationEntry("travel", "a", "(FFF)V"));
            this.nodemap.put(KEY_METHOD_ON_UPDATE, new MethodObfuscationEntry("onUpdate", "B_", "()V"));
        }
    }

    public byte[] transform(String name, String transformedName, byte[] bytes) {
        String testName;
        if (bytes == null) {
            return null;
        }
        if (this.nameForgeHooksClient == null) {
            this.nameForgeHooksClient = this.getName(KEY_CLASS_FORGE_HOOKS_CLIENT);
            if (this.deobfuscated) {
                this.populateNamesDeObf();
            } else {
                this.populateNamesObf();
            }
        }
        if ((testName = name.replace('.', '/')).length() <= 3 || this.deobfuscated) {
            return this.transformVanilla(testName, bytes);
        }
        return bytes;
    }

    private void populateNamesDeObf() {
        this.nameEntityLiving = this.getName(KEY_CLASS_ENTITY_LIVING);
        this.nameEntityItem = this.getName(KEY_CLASS_ENTITY_ITEM);
    }

    private void populateNamesObf() {
        this.nameEntityLiving = this.nodemap.get((Object)"entityLivingClass").obfuscatedName;
        this.nameEntityItem = this.nodemap.get((Object)"entityItemClass").obfuscatedName;
    }

    private byte[] transformVanilla(String testName, byte[] bytes) {
        if (testName.equals(this.nameEntityLiving)) {
            return this.transformEntityLiving(bytes);
        }
        if (testName.equals(this.nameEntityItem)) {
            return this.transformEntityItem(bytes);
        }
        return bytes;
    }

    public byte[] transformEntityLiving(byte[] bytes) {
        ClassNode node = this.startInjection(bytes);
        operationCount = 1;
        MethodNode method = this.getMethod(node, KEY_METHOD_MOVE_ENTITY);
        if (method != null) {
            for (int count = 0; count < method.instructions.size(); ++count) {
                AbstractInsnNode list = method.instructions.get(count);
                if (!(list instanceof LdcInsnNode)) continue;
                LdcInsnNode nodeAt = (LdcInsnNode)list;
                if (!nodeAt.cst.equals(0.08)) continue;
                VarInsnNode beforeNode = new VarInsnNode(25, 0);
                MethodInsnNode overwriteNode = new MethodInsnNode(184, CLASS_TRANSFORMER_HOOKS, "getGravityForEntity", "(L" + this.getNameDynamic(KEY_CLASS_ENTITY) + ";)D");
                method.instructions.insertBefore((AbstractInsnNode)nodeAt, (AbstractInsnNode)beforeNode);
                method.instructions.set((AbstractInsnNode)nodeAt, (AbstractInsnNode)overwriteNode);
                ++injectionCount;
            }
        }
        return this.finishInjection(node);
    }

    public byte[] transformEntityItem(byte[] bytes) {
        ClassNode node = this.startInjection(bytes);
        operationCount = 1;
        MethodNode method = this.getMethod(node, KEY_METHOD_ON_UPDATE);
        if (method != null) {
            for (int count = 0; count < method.instructions.size(); ++count) {
                AbstractInsnNode list = method.instructions.get(count);
                if (!(list instanceof LdcInsnNode)) continue;
                LdcInsnNode nodeAt = (LdcInsnNode)list;
                if (!nodeAt.cst.equals(0.04f)) continue;
                VarInsnNode beforeNode = new VarInsnNode(25, 0);
                MethodInsnNode overwriteNode = new MethodInsnNode(184, CLASS_TRANSFORMER_HOOKS, "getItemGravity", "(L" + this.getNameDynamic(KEY_CLASS_ENTITY_ITEM) + ";)D");
                method.instructions.insertBefore((AbstractInsnNode)nodeAt, (AbstractInsnNode)beforeNode);
                method.instructions.set((AbstractInsnNode)nodeAt, (AbstractInsnNode)overwriteNode);
                ++injectionCount;
            }
        }
        return this.finishInjection(node);
    }

    private void printResultsAndReset(String nodeName) {
        if (operationCount > 0) {
            if (injectionCount >= operationCount) {
                this.printLog("Pixelmon successfully injected bytecode into: " + nodeName + " (" + injectionCount + " / " + operationCount + ")");
            } else {
                System.err.println("Potential problem: Pixelmon did not complete injection of bytecode into: " + nodeName + " (" + injectionCount + " / " + operationCount + ")");
            }
        }
    }

    private MethodNode getMethod(ClassNode node, String keyName) {
        for (MethodNode methodNode : node.methods) {
            if (!this.methodMatches(keyName, methodNode)) continue;
            return methodNode;
        }
        return null;
    }

    private MethodNode getMethodNoDesc(ClassNode node, String methodName) {
        for (MethodNode methodNode : node.methods) {
            if (!methodNode.name.equals(methodName)) continue;
            return methodNode;
        }
        return null;
    }

    private boolean methodMatches(String keyName, MethodInsnNode node) {
        return node.name.equals(this.getNameDynamic(keyName)) && node.desc.equals(this.getDescDynamic(keyName));
    }

    private boolean methodMatches(String keyName, MethodNode node) {
        return node.name.equals(this.getNameDynamic(keyName)) && node.desc.equals(this.getDescDynamic(keyName));
    }

    public String getName(String keyName) {
        return this.nodemap.get((Object)keyName).name;
    }

    public String getObfName(String keyName) {
        return this.nodemap.get((Object)keyName).obfuscatedName;
    }

    public String getNameDynamic(String keyName) {
        try {
            if (this.deobfuscated) {
                return this.getName(keyName);
            }
            return this.getObfName(keyName);
        }
        catch (NullPointerException e) {
            System.err.println("Could not find key: " + keyName);
            throw e;
        }
    }

    public String getDescDynamic(String keyName) {
        return ((MethodObfuscationEntry)this.nodemap.get((Object)keyName)).methodDesc;
    }

    private void printLog(String message) {
        FMLLog.info(message, new Object[0]);
    }

    private ClassNode startInjection(byte[] bytes) {
        ClassNode node = new ClassNode();
        ClassReader reader = new ClassReader(bytes);
        reader.accept((ClassVisitor)node, 0);
        injectionCount = 0;
        operationCount = 0;
        return node;
    }

    private byte[] finishInjection(ClassNode node) {
        return this.finishInjection(node, true);
    }

    private byte[] finishInjection(ClassNode node, boolean printToLog) {
        ClassWriter writer = new ClassWriter(1);
        node.accept((ClassVisitor)writer);
        if (printToLog) {
            this.printResultsAndReset(node.name);
        }
        return writer.toByteArray();
    }

    private byte[] finishInjectionWithFrames(ClassNode node, boolean printToLog) {
        ClassWriter writer = new ClassWriter(2);
        node.accept((ClassVisitor)writer);
        if (printToLog) {
            this.printResultsAndReset(node.name);
        }
        return writer.toByteArray();
    }

    private boolean mcVersionMatches(String testVersion) {
        return testVersion.contains(this.mcVersion);
    }

    public static class FieldObfuscationEntry
    extends ObfuscationEntry {
        public FieldObfuscationEntry(String name, String obfuscatedName) {
            super(name, obfuscatedName);
        }
    }

    public static class MethodObfuscationEntry
    extends ObfuscationEntry {
        public String methodDesc;

        public MethodObfuscationEntry(String name, String obfuscatedName, String methodDesc) {
            super(name, obfuscatedName);
            this.methodDesc = methodDesc;
        }

        public MethodObfuscationEntry(String commonName, String methodDesc) {
            this(commonName, commonName, methodDesc);
        }
    }

    public static class ObfuscationEntry {
        public String name;
        public String obfuscatedName;

        public ObfuscationEntry(String name, String obfuscatedName) {
            this.name = name;
            this.obfuscatedName = obfuscatedName;
        }

        public ObfuscationEntry(String name) {
            this(name, FMLDeobfuscatingRemapper.INSTANCE.unmap(name));
        }
    }
}

