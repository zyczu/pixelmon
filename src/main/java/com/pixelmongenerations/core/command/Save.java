/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.core.command;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.core.storage.AsyncStorageWrapper;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;

public class Save
extends PixelmonCommand {
    public Save() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "pokesave";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/pokesave <all|flush|player..>";
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 1 && args[0].equalsIgnoreCase("flush")) {
            PixelmonStorage.saveAll(sender.getEntityWorld());
            if (PixelmonStorage.storageAdapter instanceof AsyncStorageWrapper) {
                ((AsyncStorageWrapper)PixelmonStorage.storageAdapter).flush();
            }
        } else if (args.length == 1 && args[0].equalsIgnoreCase("all")) {
            PixelmonStorage.saveAll(sender.getEntityWorld());
        } else if (args.length >= 1) {
            for (String arg : args) {
                EntityPlayerMP player = this.getPlayer(arg);
                if (player == null) {
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
                    continue;
                }
                PixelmonStorage.save(player);
            }
        } else {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        if (args.length == 1) {
            ArrayList list = Lists.newArrayList((Object[])new String[]{"all", "flush"});
            list.addAll(Lists.newArrayList((Object[])server.getOnlinePlayerNames()));
            return list;
        }
        return this.tabCompleteUsernames(args);
    }
}

