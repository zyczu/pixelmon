/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.common.spawning.PixelmonSpawner;
import com.pixelmongenerations.common.spawning.PixelmonSpawning;
import com.pixelmongenerations.common.spawning.PlayerTrackingSpawner;
import com.pixelmongenerations.common.spawning.spawners.SpawnerBoss;
import com.pixelmongenerations.common.spawning.spawners.SpawnerNPC;
import com.pixelmongenerations.core.network.ChatHandler;
import java.util.ArrayList;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

public class Spawning
extends PixelmonCommand {
    public static final String COMMAND_NAME = "spawning";

    public Spawning() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return COMMAND_NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/spawning [off | base | diagnose]";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 4;
    }

    @Override
    protected void execute(ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            if (PixelmonSpawning.coordinator != null && PixelmonSpawning.coordinator.getActive()) {
                this.sendMessage(sender, "Spawner: Base.", new Object[0]);
            } else {
                this.sendMessage(sender, "Spawner: Off", new Object[0]);
            }
        } else {
            String option;
            switch (option = args[0].toLowerCase()) {
                case "off": {
                    if (PixelmonSpawning.coordinator != null) {
                        PixelmonSpawning.coordinator.deactivate();
                    }
                    PixelmonSpawner.spawners.clear();
                    this.sendMessage(sender, "All spawning has been turned off.", new Object[0]);
                    break;
                }
                case "base": {
                    PixelmonSpawner.spawners.removeIf(spawner -> !(spawner instanceof SpawnerNPC) && !(spawner instanceof SpawnerBoss));
                    if (PixelmonSpawning.coordinator != null) {
                        PixelmonSpawning.coordinator.deactivate();
                    }
                    PixelmonSpawning.startTrackingSpawner();
                    this.sendMessage(sender, "Beta spawning system has been engaged.", new Object[0]);
                    break;
                }
                case "diagnose": {
                    boolean spawnerActive = PixelmonSpawning.coordinator != null && PixelmonSpawning.coordinator.getActive();
                    ChatHandler.sendChat(sender, new TextComponentTranslation((Object)((Object)TextFormatting.GRAY) + "Better Spawner: " + (spawnerActive ? (Object)((Object)TextFormatting.GREEN) + "Active." : (Object)((Object)TextFormatting.RED) + "Inactive."), new Object[0]));
                    if (!spawnerActive) break;
                    int spawnerCount = PixelmonSpawning.coordinator.spawners.size();
                    this.sendMessage(sender, (Object)((Object)TextFormatting.GRAY) + "Spawner count: " + (Object)((Object)TextFormatting.YELLOW) + spawnerCount, new Object[0]);
                    if (spawnerCount <= 0) break;
                    ArrayList<String> summaryMessages = new ArrayList<String>();
                    for (AbstractSpawner spawner2 : PixelmonSpawning.coordinator.spawners) {
                        double d;
                        if (!(spawner2 instanceof PlayerTrackingSpawner)) continue;
                        String message = "";
                        PlayerTrackingSpawner pts = (PlayerTrackingSpawner)spawner2;
                        EntityPlayerMP player = pts.getTrackedPlayer();
                        message = player == null ? (Object)((Object)TextFormatting.RED) + "OFFLINE PLAYER " + (Object)((Object)TextFormatting.GRAY) + "(" : (Object)((Object)TextFormatting.YELLOW) + player.getName() + " (";
                        double percentage = (double)pts.spawned.size() / (double)pts.capacity * 100.0;
                        String strPercentage = String.format("%.1f", percentage) + "%";
                        strPercentage = percentage == 100.0 || percentage == 0.0 ? (Object)((Object)TextFormatting.RED) + strPercentage : (Object)((Object)TextFormatting.YELLOW) + strPercentage;
                        message = message + strPercentage + (Object)((Object)TextFormatting.GRAY) + ") - Last Cycle: ";
                        long timeSinceLastCycle = System.currentTimeMillis() - pts.lastCycleTime;
                        double seconds = (double)timeSinceLastCycle / 1000.0;
                        double expectedSeconds = pts.spawnFrequency / 60.0f;
                        message = seconds > expectedSeconds * 60.0 ? message + (Object)((Object)TextFormatting.RED) + String.format("%.1f", seconds) + "s" : (seconds > expectedSeconds * 10.0 ? message + (Object)((Object)TextFormatting.YELLOW) + String.format("%.1f", seconds) + "s" : message + (Object)((Object)TextFormatting.GREEN) + String.format("%.1f", seconds) + "s");
                        message = message + (Object)((Object)TextFormatting.GRAY) + " - Last Spawn: ";
                        long timeSinceLastSpawn = System.currentTimeMillis() - pts.lastSpawnTime;
                        seconds = (double)timeSinceLastSpawn / 1000.0;
                        String time = String.format("%.1f", seconds) + "s";
                        message = seconds > expectedSeconds * 60.0 ? message + (Object)((Object)TextFormatting.RED) + time : (seconds > expectedSeconds * 10.0 ? message + (Object)((Object)TextFormatting.YELLOW) + time : message + (Object)((Object)TextFormatting.GREEN) + time);
                        summaryMessages.add(message);
                    }
                    for (String message : summaryMessages) {
                        sender.sendMessage(new TextComponentString(message));
                    }
                    break;
                }
            }
        }
    }
}

