/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.common.item.ItemZoneWand;
import com.pixelmongenerations.common.world.Schematic;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.CommandChatHandler;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Objects;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

public class CommandSchematic
extends CommandBase {
    private ArrayList<BlockPos> corners = new ArrayList();
    public static Schematic testSchematic;

    @Override
    public String getName() {
        return "schematics";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/" + this.getName() + " <argument>";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (!(sender instanceof EntityPlayer) || args.length == 0) {
            CommandChatHandler.sendChat(sender, this.getUsage(sender), new Object[0]);
            return;
        }
        EntityPlayer player = (EntityPlayer)sender;
        World world = player.world;
        switch (args[0]) {
            case "save": {
                ItemStack wand = player.getHeldItem(EnumHand.MAIN_HAND);
                if (!ItemZoneWand.isValidNBTWand(wand)) {
                    CommandChatHandler.sendChat(sender, "pixelmon.command.schematic.novalidwanddetected", new Object[0]);
                    return;
                }
                Vec3d corner0 = ItemZoneWand.getPosition(wand, ItemZoneWand.SelectPoint.One);
                Vec3d corner1 = ItemZoneWand.getPosition(wand, ItemZoneWand.SelectPoint.Two);
                testSchematic = Schematic.createFromWorld(world, new BlockPos(corner0), new BlockPos(corner1));
                CommandSchematic.testSchematic.name = "test";
                if (args.length > 1) {
                    CommandSchematic.testSchematic.name = args[1];
                    String baseDirectory = Pixelmon.modDirectory + "/schematics/";
                    String filename = baseDirectory + args[1] + ".schem";
                    File saveFile = new File(filename);
                    File saveDirPath = new File(baseDirectory);
                    if (!saveDirPath.exists()) {
                        saveDirPath.mkdirs();
                    }
                    try {
                        FileOutputStream f = new FileOutputStream(saveFile);
                        DataOutputStream s = new DataOutputStream(f);
                        CompressedStreamTools.writeCompressed(testSchematic.saveToNBT(), s);
                        s.close();
                        f.close();
                        CommandChatHandler.sendChat(sender, "pixelmon.command.snapshot.savefile", args[1]);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
                CommandChatHandler.sendChat(sender, "pixelmon.command.schematic.save", new Object[0]);
                break;
            }
            case "read": {
                if (args.length <= 1) break;
                String filename = Pixelmon.modDirectory + "/schematics/" + args[1] + ".schem";
                File saveFile = new File(filename);
                try {
                    DataInputStream s = new DataInputStream(new FileInputStream(saveFile));
                    testSchematic = Schematic.loadFromNBT(Objects.requireNonNull(CompressedStreamTools.readCompressed(s)));
                    CommandChatHandler.sendChat(sender, "pixelmon.command.snapshot.load", args[1]);
                    break;
                }
                catch (Exception e) {
                    e.printStackTrace();
                    CommandChatHandler.sendFormattedChat(sender, TextFormatting.RED, "pixelmon.command.snapshot.errorread", args[1]);
                    return;
                }
            }
            case "place": {
                this.place(sender, player, world);
                break;
            }
            default: {
                CommandChatHandler.sendChat(sender, this.getUsage(sender), new Object[0]);
            }
        }
    }

    private void place(ICommandSender sender, EntityPlayer player, World world) {
        if (testSchematic == null) {
            CommandChatHandler.sendChat(sender, "pixelmon.command.snapshot.nosave", new Object[0]);
            return;
        }
        BlockPos basePos = player.getPosition();
        testSchematic.place(world, basePos.getX(), basePos.getY(), basePos.getZ(), true);
        CommandChatHandler.sendChat(sender, "pixelmon.command.snapshot.place", new Object[0]);
    }
}

