/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.block.spawning.TileEntityPixelmonSpawner;
import com.pixelmongenerations.common.entity.EntityDynamaxEnergy;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.command.CommandException;
import net.minecraft.command.EntitySelector;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;

public class MaxEnergyBeam
extends PixelmonCommand {
    private Random random = new Random();

    public MaxEnergyBeam() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "maxenergybeam";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/maxenergybeam <player>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            this.execute(sender, new String[]{sender.getName()});
        } else if (args.length > 1) {
            for (String player : args) {
                this.execute(sender, new String[]{player});
            }
        } else if (args[0].startsWith("@")) {
            List<EntityPlayerMP> list = EntitySelector.matchEntities(sender, args[0], EntityPlayerMP.class);
            for (EntityPlayerMP p : list) {
                this.execute(sender, new String[]{p.getName()});
            }
        } else {
            EntityPlayerMP player = this.getPlayer(args[0]);
            int spawnRadius = 30;
            int x = player.getPosition().getX() + player.world.rand.nextInt(spawnRadius * 2 + 1) - spawnRadius;
            Integer y = player.getPosition().getY();
            int z = player.getPosition().getZ() + player.world.rand.nextInt(spawnRadius * 2 + 1) - spawnRadius;
            if (!MaxEnergyBeam.attemptEnergySpawn(player, x, y, z)) {
                y = player.world.getHeight(x, z);
                Block block = player.world.getBlockState(new BlockPos(x, y - 1, z)).getBlock();
                if (block.getRegistryName().toString().contains("leaves")) {
                    y = y - 8;
                }
                if ((block = player.world.getBlockState(new BlockPos(x, y, z)).getBlock()) == Blocks.AIR) {
                    y = y - 5;
                }
                MaxEnergyBeam.attemptEnergySpawn(player, x, y, z);
            }
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        return args.length != 1 && args.length != 2 ? Collections.emptyList() : MaxEnergyBeam.getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames());
    }

    public static boolean attemptEnergySpawn(EntityPlayerMP player, int x, int y, int z) {
        boolean valid = false;
        Integer yLevel = TileEntityPixelmonSpawner.spawnerLand.getSpawnConditionY(player.world, new BlockPos(x, y, z));
        boolean bl = valid = yLevel != null && TileEntityPixelmonSpawner.spawnerLand.canPokemonSpawnHere(player.world, new BlockPos(x, yLevel, z));
        if (valid) {
            EntityDynamaxEnergy dynamaxEnergy = new EntityDynamaxEnergy(player.world);
            dynamaxEnergy.setPosition(x, yLevel.intValue(), z);
            player.world.spawnEntity(dynamaxEnergy);
            return true;
        }
        return false;
    }
}

