/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.cosmetic.CosmeticEntry;
import com.pixelmongenerations.common.cosmetic.EnumCosmetic;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import net.minecraft.command.CommandException;
import net.minecraft.command.EntitySelector;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;

public class GiveCosmetic
extends PixelmonCommand {
    public GiveCosmetic() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "givecosmetic";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/givecosmetic <player> <cosmetic>";
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        block12: {
            if (args.length >= 2) {
                if (args[0].startsWith("@")) {
                    List<EntityPlayerMP> list = EntitySelector.matchEntities(sender, args[0], EntityPlayerMP.class);
                    for (EntityPlayerMP p : list) {
                        args[0] = p.getName();
                        this.execute(sender, args);
                    }
                    return;
                }
                try {
                    EntityPlayerMP player = this.getPlayer(args[0]);
                    if (player == null) {
                        this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
                        return;
                    }
                    PlayerStorage storage = this.getPlayerStorage(player);
                    Optional<EnumCosmetic> cosmeticTypeOpt = EnumCosmetic.getCosmetic(args[1]);
                    if (cosmeticTypeOpt.isPresent()) {
                        if (!cosmeticTypeOpt.get().isExclusive()) {
                            CosmeticEntry cosmetic = CosmeticEntry.of(cosmeticTypeOpt.get());
                            if (storage.cosmeticData.addCosmetic(cosmetic)) {
                                this.sendMessage(sender, TextFormatting.GREEN, "pixelmon.command.cosmetic.added", player.getName(), cosmeticTypeOpt.get().getName());
                            } else {
                                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.cosmetic.alreadyowned", player.getName(), cosmeticTypeOpt.get().getName());
                            }
                        } else {
                            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.cosmetic.exclusive", cosmeticTypeOpt.get().getName());
                        }
                        break block12;
                    }
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.cosmetic.unknowntype", args[1]);
                }
                catch (NullPointerException npe) {
                    npe.printStackTrace();
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.notingame", args[1]);
                }
            } else {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalid", new Object[0]);
                this.sendMessage(sender, TextFormatting.RED, this.getUsage(sender), new Object[0]);
            }
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        if (args.length == 1) {
            return this.tabCompleteUsernames(args);
        }
        if (args.length == 2) {
            return PixelmonCommand.getListOfStringsMatchingLastWord(args, EnumCosmetic.getNameList());
        }
        return Collections.emptyList();
    }
}

