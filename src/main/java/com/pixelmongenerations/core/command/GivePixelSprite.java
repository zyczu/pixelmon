/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.SpriteHelper;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.annotation.Nullable;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.translation.I18n;

public class GivePixelSprite
extends PixelmonCommand {
    public GivePixelSprite() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "givepixelsprite";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/givepixelsprite <pokemon name> [form] OR /givepixelsprite <pixelmon:sprites/pokemon/001>";
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (!(sender instanceof EntityPlayerMP)) {
            return;
        }
        if (args.length == 0 || args.length > 2) {
            sender.sendMessage(new TextComponentString(this.getUsage(sender)));
            return;
        }
        EntityPlayerMP player = (EntityPlayerMP)sender;
        ItemStack itemStack = new ItemStack(PixelmonItems.itemPixelmonSprite);
        NBTTagCompound tagCompound = new NBTTagCompound();
        itemStack.setTagCompound(tagCompound);
        if (args[0].contains(":")) {
            tagCompound.setString("SpriteName", args[0]);
        } else {
            EnumSpecies pokemon = EnumSpecies.getFromNameAnyCase(args[0]);
            if (pokemon == null) {
                player.sendMessage(new TextComponentString(this.getUsage(sender)));
                return;
            }
            int form = 0;
            form = args.length == 2 ? GivePixelSprite.parseInt(args[1]) : (int)pokemon.getFormEnum(form).getForm();
            Optional<BaseStats> stats = Entity3HasStats.getBaseStats(pokemon.name);
            if (!stats.isPresent()) {
                player.sendMessage(new TextComponentString(this.getUsage(sender)));
                return;
            }
            String tag = "pixelmon:sprites/pokemon/" + String.format("%03d", stats.get().nationalPokedexNumber);
            if (form != -1) {
                tag = tag + SpriteHelper.getSpriteExtra(stats.get().pixelmonName, form);
            }
            tagCompound.setString("SpriteName", tag);
            NBTTagCompound display = new NBTTagCompound();
            display.setString("Name", Entity1Base.getLocalizedName(pokemon.name) + " " + I18n.translateToLocal("item.pixelmon_sprite.name"));
            tagCompound.setTag("display", display);
        }
        player.inventory.addItemStackToInventory(itemStack);
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos pos) {
        return args.length == 1 ? this.tabCompletePokemon(args) : Collections.emptyList();
    }
}

