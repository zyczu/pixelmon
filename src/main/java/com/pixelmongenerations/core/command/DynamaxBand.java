/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.core.command.MegaRing;
import com.pixelmongenerations.core.enums.EnumDynamaxItem;
import com.pixelmongenerations.core.enums.EnumMegaItem;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Collections;
import java.util.List;
import net.minecraft.command.CommandException;
import net.minecraft.command.EntitySelector;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;

public class DynamaxBand
extends PixelmonCommand {
    public DynamaxBand() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "dynamaxband";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/dynamaxband <player>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            this.execute(sender, new String[]{sender.getName()});
        } else if (args.length > 1) {
            for (String player : args) {
                this.execute(sender, new String[]{player});
            }
        } else if (args[0].startsWith("@")) {
            List<EntityPlayerMP> list = EntitySelector.matchEntities(sender, args[0], EntityPlayerMP.class);
            for (EntityPlayerMP p : list) {
                this.execute(sender, new String[]{p.getName()});
            }
        } else {
            EntityPlayerMP player = this.getPlayer(args[0]);
            PlayerStorage storage = this.getPlayerStorage(player);
            if (storage.megaData.getMegaItem() == EnumMegaItem.BraceletORAS) {
                storage.megaData.setMegaItem(EnumMegaItem.None, false);
            }
            storage.dynamaxData.setDynamaxItem(EnumDynamaxItem.DynamaxBand, false);
            storage.dynamaxData.setCanEquipDynamaxBand(true);
            DynamaxBand.notifyCommandListener(sender, (ICommand)this, 0, "pixelmon.command.dynamaxband.notifygave", player.getDisplayNameString());
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        return args.length != 1 && args.length != 2 ? Collections.emptyList() : MegaRing.getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames());
    }
}

