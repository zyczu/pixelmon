/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.block.tileEntities.TileEntityWarpPlate;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import net.minecraft.block.Block;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextFormatting;

public class WarpPlate
extends PixelmonCommand {
    public WarpPlate() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "warpplate";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/warpplate set x y z";
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (sender instanceof EntityPlayerMP) {
            EntityPlayerMP player = (EntityPlayerMP)sender;
            if (args.length == 4 && args[0].equalsIgnoreCase("set")) {
                Block blockStandingOn = player.world.getBlockState(player.getPosition()).getBlock();
                if (blockStandingOn == PixelmonBlocks.warpPlate || blockStandingOn == PixelmonBlocks.dynamicWarpPlate) {
                    TileEntityWarpPlate warpPlate = (TileEntityWarpPlate)player.world.getTileEntity(player.getPosition());
                    if (warpPlate != null && warpPlate.calculatePosition(args[1], args[2], args[3]) != null) {
                        warpPlate.setWarpPosition(args[1], args[2], args[3]);
                        this.sendMessage(sender, "pixelmon.command.warpplate.set", warpPlate.getWarpPosition());
                    } else {
                        this.sendMessage(sender, TextFormatting.RED, this.getUsage(player), new Object[0]);
                    }
                } else {
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.warpplate.notstanding", new Object[0]);
                }
            } else {
                this.sendMessage(sender, TextFormatting.RED, this.getUsage(player), new Object[0]);
            }
        }
    }
}

