/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.api.pc.BackgroundRegistry;
import com.pixelmongenerations.api.pc.IBackground;
import com.pixelmongenerations.core.storage.PCServer;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import net.minecraft.command.CommandException;
import net.minecraft.command.EntitySelector;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;

public class GiveBackground
extends PixelmonCommand {
    public GiveBackground() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "givepcbackground";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/givepcbackground <player> <background>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        block9: {
            if (args.length >= 2) {
                if (args[0].startsWith("@")) {
                    List<EntityPlayerMP> list = EntitySelector.matchEntities(sender, args[0], EntityPlayerMP.class);
                    for (EntityPlayerMP p : list) {
                        args[0] = p.getName();
                        this.execute(sender, args);
                    }
                    return;
                }
                try {
                    EntityPlayerMP player = this.getPlayer(args[0]);
                    if (player == null) {
                        this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
                        return;
                    }
                    if (Objects.equals(args[1], "@a")) {
                        for (IBackground background : BackgroundRegistry.getBackgrounds()) {
                            PCServer.giveBackground(player, background.getId());
                        }
                        GiveBackground.notifyCommandListener(sender, (ICommand)this, 0, "pixelmon.command.givebackground.notifygaveall", player.getDisplayNameString());
                        break block9;
                    }
                    PCServer.giveBackground(player, args[1]);
                    GiveBackground.notifyCommandListener(sender, (ICommand)this, 0, "pixelmon.command.givebackground.notifygave", player.getDisplayNameString(), args[1]);
                }
                catch (NullPointerException npe) {
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.notingame", args[1]);
                }
            } else {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalid", new Object[0]);
                this.sendMessage(sender, TextFormatting.RED, this.getUsage(sender), new Object[0]);
            }
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        if (args.length == 1) {
            return this.tabCompleteUsernames(args);
        }
        if (args.length == 2) {
            return PixelmonCommand.getListOfStringsMatchingLastWord(args, BackgroundRegistry.getBackgrounds().stream().map(IBackground::getId).collect(Collectors.toList()));
        }
        return Collections.emptyList();
    }
}

