/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.battle.BattleFactory;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.TrainerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.EnumAggression;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import com.pixelmongenerations.core.network.CommandChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.RegexPatterns;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import net.minecraft.block.Block;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class Battle2
extends PixelmonCommand {
    public Battle2() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "pokebattle2";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/pokebattle2 <player> <player|pokemon> <player|pokemon> <player|pokemon>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (args.length < 2) {
            this.sendMessage(sender, this.getUsage(sender), new Object[0]);
            return;
        }
        try {
            BattleParticipant[] team2;
            BattleParticipant[] team1;
            ArrayList<BattleParticipant> participants = new ArrayList<BattleParticipant>();
            ArrayList<EntityPixelmon> wildPixelmon = new ArrayList<EntityPixelmon>();
            ArrayList<String> playerNames = new ArrayList<String>();
            int numParticipants = 0;
            for (String arg : args) {
                if (arg.startsWith("lvl")) continue;
                ++numParticipants;
            }
            if (numParticipants < 2 || numParticipants > 4) {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.battle2.cantstart", new Object[0]);
            }
            for (int i = 0; i < args.length; ++i) {
                if (args[i].startsWith("lvl")) continue;
                Object part = this.getPart(this.getServer(), sender, args[i]);
                if (part == null) {
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.battle2.invalid", i + 1);
                    return;
                }
                if (part instanceof EntityPlayerMP) {
                    EntityPlayerMP playerPart = (EntityPlayerMP)part;
                    if (BattleRegistry.getBattle(playerPart) != null) {
                        this.sendMessage(sender, TextFormatting.RED, "sendpixelmon.inbattle", playerPart.getDisplayName());
                        return;
                    }
                    if (playerNames.contains(playerPart.getDisplayNameString())) {
                        this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.battle2.duplicate", new Object[0]);
                        return;
                    }
                    playerNames.add(playerPart.getDisplayNameString());
                    Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(playerPart);
                    if (optstorage.isPresent()) {
                        EntityPixelmon[] arrentityPixelmon;
                        PlayerStorage storage = optstorage.get();
                        if (numParticipants == 2 || numParticipants == 3 && i == 0) {
                            arrentityPixelmon = storage.getAmountAblePokemon(playerPart.world, 2);
                        } else {
                            EntityPixelmon[] arrentityPixelmon2 = new EntityPixelmon[1];
                            arrentityPixelmon = arrentityPixelmon2;
                            arrentityPixelmon2[0] = storage.getFirstAblePokemon(playerPart.world);
                        }
                        EntityPixelmon[] leadPokemon = arrentityPixelmon;
                        if (leadPokemon.length > 0 && leadPokemon[0] != null) {
                            participants.add(new PlayerParticipant(playerPart, leadPokemon));
                            continue;
                        }
                        this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.battle.nopokemon", playerPart.getDisplayName());
                        return;
                    }
                    Pixelmon.LOGGER.debug("Error loading player for command /pokebattle2 " + args[0] + "  " + args[1]);
                    continue;
                }
                if (part instanceof EntityPixelmon) {
                    EntityPixelmon wildPart = (EntityPixelmon)part;
                    if (i == 0) {
                        this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.battle2.first", new Object[0]);
                        return;
                    }
                    if (i + 1 < args.length && args[i + 1].startsWith("lvl")) {
                        int level = Integer.parseInt(RegexPatterns.LETTERS.matcher(args[i + 1]).replaceAll(""));
                        if (level > 0 && level <= PixelmonServerConfig.maxLevel) {
                            wildPart.getLvl().setLevel(level);
                        } else {
                            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.cheater", new Object[0]);
                        }
                    } else {
                        wildPart.getLvl().setLevel(((PlayerParticipant)participants.get(0)).getHighestLevel());
                    }
                    wildPixelmon.add(wildPart);
                    participants.add(new WildPixelmonParticipant(wildPart));
                    continue;
                }
                if (!(part instanceof NPCTrainer)) continue;
                NPCTrainer trainer = (NPCTrainer)part;
                if (trainer.battleController != null) {
                    this.sendMessage(sender, TextFormatting.RED, "sendpixelmon.inbattle", trainer.getName());
                    return;
                }
                participants.add(new TrainerParticipant(trainer, 2));
            }
            EntityPlayerMP player = ((PlayerParticipant)participants.get((int)0)).player;
            for (int i = 0; i < wildPixelmon.size(); ++i) {
                EntityPixelmon p = (EntityPixelmon)wildPixelmon.get(i);
                p.aggression = EnumAggression.passive;
                if (i == 0) {
                    p.setPosition(player.posX + 2.0, this.getTopEarthBlock(player.world, (int)player.posX, (int)player.posZ).intValue(), player.posZ);
                } else if (i == 1) {
                    p.setPosition(player.posX, this.getTopEarthBlock(player.world, (int)player.posX, (int)player.posZ).intValue(), player.posZ + 2.0);
                } else if (i == 2) {
                    p.setPosition(player.posX + 2.0, this.getTopEarthBlock(player.world, (int)player.posX, (int)player.posZ).intValue(), player.posZ + 2.0);
                }
                player.world.spawnEntity(p);
            }
            ArrayList battleParticipants = new ArrayList();
            if (numParticipants == 2) {
                team1 = new BattleParticipant[]{(BattleParticipant)participants.get(0)};
                team2 = new BattleParticipant[]{(BattleParticipant)participants.get(1)};
            } else if (numParticipants == 3) {
                team1 = new BattleParticipant[]{(BattleParticipant)participants.get(0)};
                for (BattleParticipant participant : team2 = new BattleParticipant[]{(BattleParticipant)participants.get(1), (BattleParticipant)participants.get(2)}) {
                    if (!(participant instanceof TrainerParticipant)) continue;
                    participant.setNumControlledPokemon(1);
                }
            } else if (numParticipants == 4) {
                team1 = new BattleParticipant[]{(BattleParticipant)participants.get(0), (BattleParticipant)participants.get(1)};
                team2 = new BattleParticipant[]{(BattleParticipant)participants.get(2), (BattleParticipant)participants.get(3)};
                for (BattleParticipant participant : participants) {
                    if (!(participant instanceof TrainerParticipant)) continue;
                    participant.setNumControlledPokemon(1);
                }
            } else {
                CommandChatHandler.sendChat(sender, "pixelmon.command.battle2.cantstart", new Object[0]);
                return;
            }
            BattleFactory.createBattle().team1(team1).team2(team2).type(EnumBattleType.Double).startBattle();
        }
        catch (NumberFormatException e) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.battle2.level", new Object[0]);
        }
        catch (ClassCastException e) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.battle2.cantstart", new Object[0]);
        }
        catch (Exception e) {
            System.out.println("Error loading player for command /pokebattle2 " + args[0] + "  " + args[1]);
            e.printStackTrace();
        }
    }

    public Integer getTopEarthBlock(World world, int cpX, int cpZ) {
        Chunk chunk = world.getChunk(new BlockPos(cpX, 0, cpZ));
        Integer k = null;
        k = Math.max(0, Math.min(chunk.getTopFilledSegment() + 15, 255));
        while (k > 0) {
            Block block = world.getBlockState(new BlockPos(cpX, k, cpZ)).getBlock();
            if (block != Blocks.AIR && !block.isFoliage(world, new BlockPos(cpX, k, cpZ))) {
                return k + 1;
            }
            k = k - 1;
        }
        return null;
    }

    private Object getPart(MinecraftServer server, ICommandSender sender, String value) {
        EntityPlayerMP player = this.getPlayer(sender, value);
        if (player != null) {
            return player;
        }
        if (value.equalsIgnoreCase("random")) {
            value = EnumSpecies.randomPoke().name;
        }
        if (EnumSpecies.hasPokemon(value)) {
            return (EntityPixelmon)PixelmonEntityList.createEntityByName(value, sender.getEntityWorld());
        }
        String langCode = sender instanceof EntityPlayerMP ? ((EntityPlayerMP)sender).language : "en_US";
        Optional<NPCTrainer> trainer = EntityNPC.locateNPCServer(sender.getEntityWorld(), value, NPCTrainer.class, langCode);
        if (trainer.isPresent()) {
            return trainer.get();
        }
        return null;
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        if (args.length >= 1 && args.length <= 4) {
            return this.tabCompleteUsernames(args);
        }
        return Collections.emptyList();
    }
}

