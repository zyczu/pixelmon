/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.api.events.BreedEvent;
import com.pixelmongenerations.common.entity.pixelmon.Entity10CanBreed;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.network.CommandChatHandler;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Collections;
import java.util.List;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.common.MinecraftForge;

public class Breed
extends PixelmonCommand {
    public Breed() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "breed";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/breed <username> <party slot #1> <party slot #2>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        EntityPlayerMP commandSender;
        String playerName;
        String nameArg;
        if (args.length != 1 && args.length != 3) {
            CommandChatHandler.sendChat(sender, "pixelmon.command.general.invalid", new Object[0]);
            CommandChatHandler.sendChat(sender, this.getUsage(sender), new Object[0]);
            return;
        }
        if (sender instanceof EntityPlayerMP && !(nameArg = args[0]).equalsIgnoreCase(playerName = (commandSender = (EntityPlayerMP)sender).getName()) && !commandSender.canUseCommand(2, "pixelmon.command.breed.others")) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
            return;
        }
        EntityPlayerMP player = this.getPlayer(sender, args[0]);
        if (player == null) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
            return;
        }
        PlayerStorage storage = this.getPlayerStorage(player);
        if (storage != null) {
            NBTTagCompound[] all_nbt = storage.getList();
            int intPartyPosition1 = -1;
            int intPartyPosition2 = -1;
            if (args.length == 1) {
                boolean pairFound = false;
                for (int x = 0; x < all_nbt.length; ++x) {
                    NBTTagCompound nbt1 = all_nbt[x];
                    if (nbt1 != null) {
                        for (int y = 0; y < all_nbt.length; ++y) {
                            NBTTagCompound nbt2 = all_nbt[y];
                            if (nbt1 != nbt2 && nbt2 != null && Entity10CanBreed.canBreed((EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt1, player.world), (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt2, player.world))) {
                                pairFound = true;
                                intPartyPosition1 = x;
                                intPartyPosition2 = y;
                            }
                            if (pairFound) break;
                        }
                    }
                    if (!pairFound) {
                        continue;
                    }
                    break;
                }
            } else if (args.length == 3) {
                intPartyPosition1 = Breed.parseInt(args[1], 1) - 1;
                intPartyPosition2 = Breed.parseInt(args[2], 1) - 1;
                if (intPartyPosition1 > 5 || intPartyPosition1 < 0) {
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.breed.invalidslot", "1");
                    this.sendMessage(sender, TextFormatting.RED, this.getUsage(sender), new Object[0]);
                    return;
                }
                if (intPartyPosition2 > 5 || intPartyPosition2 < 0) {
                    this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.breed.invalidslot", "2");
                    this.sendMessage(sender, TextFormatting.RED, this.getUsage(sender), new Object[0]);
                    return;
                }
            }
            if (intPartyPosition1 == -1 || intPartyPosition2 == -1) {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.breed.novalidpairs", player.getDisplayName().getUnformattedText());
                return;
            }
            int nullPartySlot = -1;
            if (all_nbt[intPartyPosition1] == null) {
                nullPartySlot = intPartyPosition1 + 1;
            } else if (all_nbt[intPartyPosition2] == null) {
                nullPartySlot = intPartyPosition2 + 1;
            }
            if (nullPartySlot > -1) {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.breed.nullslot", nullPartySlot);
                return;
            }
            EntityPixelmon pixelmon1 = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(all_nbt[intPartyPosition1], player.world);
            EntityPixelmon pixelmon2 = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(all_nbt[intPartyPosition2], player.world);
            if (!pixelmon1.isEgg && !pixelmon2.isEgg && Entity10CanBreed.canBreed(pixelmon1, pixelmon2)) {
                EntityPixelmon egg = (EntityPixelmon)PixelmonEntityList.createEntityByName("Magikarp", player.world);
                egg.makeEntityIntoEgg(pixelmon1, pixelmon2);
                BreedEvent.MakeEggEvent makeEvent = new BreedEvent.MakeEggEvent(player.getUniqueID(), null, egg, pixelmon2, pixelmon2);
                if (MinecraftForge.EVENT_BUS.post(makeEvent)) {
                    return;
                }
                BreedEvent.CollectEggEvent eggEvent = new BreedEvent.CollectEggEvent(player.getUniqueID(), null, makeEvent.getEgg());
                MinecraftForge.EVENT_BUS.post(eggEvent);
                this.sendMessage(sender, "pixelmon.command.breed.giveegg", pixelmon1.getNickname(), pixelmon2.getNickname());
                storage.addToParty(eggEvent.getEgg());
            } else {
                Object[] arrobject = new Object[]{pixelmon1.isEgg ? I18n.translateToLocal("pixelmon.egg.name") : pixelmon1.getNickname(), pixelmon2.isEgg ? I18n.translateToLocal("pixelmon.egg.name") : pixelmon2.getNickname()};
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.breed.notcompatible", arrobject);
            }
        } else {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.invalidplayer", new Object[0]);
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        if (args.length == 1) {
            return this.tabCompleteUsernames(args);
        }
        if (args.length == 2 || args.length == 3) {
            return Breed.getListOfStringsMatchingLastWord(args, "1", "2", "3", "4", "5", "6");
        }
        return Collections.emptyList();
    }
}

