/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;

public class PixelTP
extends PixelmonCommand {
    public PixelTP() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "pixeltp";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/pixeltp - teleports to a random Pixelmon";
    }

    @Override
    protected void execute(ICommandSender sender, String[] args) throws CommandException {
        if (sender instanceof EntityPlayerMP) {
            EntityPlayerMP player = PixelTP.getCommandSenderAsPlayer(sender);
            List<EntityPixelmon> pixelmonList = player.world.getEntities(EntityPixelmon.class, entity -> !entity.isDead);
            if (pixelmonList.isEmpty()) {
                return;
            }
            int selector = RandomHelper.getRandomNumberBetween(0, pixelmonList.size() - 1);
            BlockPos pos = pixelmonList.get(selector).getPosition();
            player.setPositionAndUpdate(pos.getX(), pos.getY(), pos.getZ());
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        return super.getTabCompletions(server, sender, args, targetPos);
    }
}

