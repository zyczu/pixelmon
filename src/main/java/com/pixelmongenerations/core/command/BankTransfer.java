/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.core.network.CommandChatHandler;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Collections;
import java.util.List;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;

public class BankTransfer
extends PixelmonCommand {
    public BankTransfer() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "transfer";
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/transfer <player> <money>";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (args.length < 2 || args.length > 3) {
            CommandChatHandler.sendChat(sender, "pixelmon.command.general.invalid", new Object[0]);
            CommandChatHandler.sendChat(sender, this.getUsage(sender), new Object[0]);
            return;
        }
        EntityPlayerMP user = this.getPlayer(sender, sender.getName());
        if (user == null) {
            throw new PlayerNotFoundException(sender.getName());
        }
        int amount = 0;
        try {
            amount = Integer.parseInt(args[1]);
        }
        catch (NumberFormatException numberFormatException) {
            // empty catch block
        }
        if (amount <= 0) {
            this.sendMessage(sender, "pixelmon.command.general.invalid", new Object[0]);
            this.sendMessage(sender, this.getUsage(sender), new Object[0]);
            return;
        }
        EntityPlayerMP target = this.getPlayer(sender, args[0]);
        if (target == null) {
            throw new PlayerNotFoundException(args[0]);
        }
        if (user == target) {
            this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.transfer.sameplayer", new Object[0]);
            return;
        }
        PlayerStorage userStorage = this.getPlayerStorage(user);
        PlayerStorage targetStorage = this.getPlayerStorage(target);
        if (userStorage != null && targetStorage != null) {
            if (userStorage.getCurrency() < amount) {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.transfer.notenoughmoney", new Object[0]);
                return;
            }
            int beforeCurrency = targetStorage.getCurrency();
            targetStorage.addCurrency(amount);
            int currencyDifference = targetStorage.getCurrency() - beforeCurrency;
            userStorage.addCurrency(-1 * currencyDifference);
            ITextComponent targetName = target.getDisplayName();
            if (currencyDifference == 0) {
                if (amount > 0) {
                    this.sendMessage(sender, "pixelmon.command.givemoney.moneylimit", targetName);
                } else {
                    this.sendMessage(sender, "pixelmon.command.givemoney.nomoney", targetName);
                }
            } else {
                String currencyString = Integer.toString(currencyDifference);
                BankTransfer.notifyCommandListener(sender, (ICommand)this, 0, "pixelmon.command.transfer.notifytransfer", sender.getName(), currencyString, targetName);
                this.sendMessage(sender, "pixelmon.command.transfer.transferred", currencyString, targetName);
                this.sendMessage((ICommandSender)target, "pixelmon.command.transfer.received", user.getDisplayName(), currencyString);
            }
        } else {
            this.sendMessage(sender, "pixelmon.command.general.invalidplayer", new Object[0]);
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        return args.length != 1 ? Collections.emptyList() : this.tabCompleteUsernames(args);
    }
}

