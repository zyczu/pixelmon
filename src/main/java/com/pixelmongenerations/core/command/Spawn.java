/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.Collections;
import java.util.List;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.NumberInvalidException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.WorldServer;

public class Spawn
extends PixelmonCommand {
    public Spawn() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "pokespawn";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/pokespawn <pokemon>";
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        BlockPos pos;
        WorldServer world;
        WorldServer worldServer = world = sender instanceof EntityPlayerMP ? ((EntityPlayerMP)sender).getServerWorld() : this.getServer().getWorld(0);
        if (args.length >= 4) {
            try {
                pos = Spawn.parseBlockPos(sender, args, 1, false);
            }
            catch (NumberInvalidException e) {
                pos = sender.getPosition();
            }
        } else {
            pos = sender.getPosition();
        }
        if (args.length >= 1) {
            String name = args[0];
            if (name.equalsIgnoreCase("random")) {
                args[0] = name = EnumSpecies.randomPoke().name;
            }
            PokemonSpec spec = PokemonSpec.from(args);
            if (spec.name != null) {
                EntityPixelmon pokemon = spec.create(world);
                pokemon.setPosition(pos.getX(), pos.getY() + 1, pos.getZ());
                pokemon.canDespawn = false;
                pokemon.setSpawnLocation(pokemon.getDefaultSpawnLocation());
                world.spawnEntity(pokemon);
                this.sendMessage(sender, "pixelmon.command.spawn.spawned", new TextComponentTranslation("pixelmon." + pokemon.getPokemonName().toLowerCase() + ".name", new Object[0]));
                Spawn.notifyCommandListener(sender, (ICommand)this, 0, "pixelmon.command.spawn.spawnednotify", sender.getName(), I18n.translateToLocal("pixelmon." + pokemon.getPokemonName().toLowerCase() + ".name"));
            } else {
                this.sendMessage(sender, TextFormatting.RED, "pixelmon.command.general.nopokemontospawn", name);
            }
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        return args.length == 1 ? this.tabCompletePokemon(args) : Collections.emptyList();
    }
}

