/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.api.dialogue.Choice;
import com.pixelmongenerations.api.dialogue.Dialogue;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnSet;
import com.pixelmongenerations.api.spawning.archetypes.entities.pokemon.SpawnInfoPokemon;
import com.pixelmongenerations.api.spawning.conditions.LocationType;
import com.pixelmongenerations.api.spawning.conditions.WorldTime;
import com.pixelmongenerations.api.spawning.util.SetLoader;
import com.pixelmongenerations.api.world.BlockCollection;
import com.pixelmongenerations.api.world.WeatherType;
import com.pixelmongenerations.client.models.PixelmonModelBase;
import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.client.models.PixelmonModelSmd;
import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.spawning.PixelmonBiomeDictionary;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.helper.DebugHelper;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.Function;
import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.biome.Biome;

public class Debug
extends PixelmonCommand {
    public Debug() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "pixeldebug";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/pixeldebug [lang/effect/camera/fov/reloadmodels/itemsprite/render/collection/spawns/spawnwiki/testspec]";
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            throw new CommandException(this.getUsage(sender), new Object[0]);
        }
        switch (args[0].toLowerCase()) {
            case "lang": {
                DebugHelper.langCheck((EntityPlayer)sender);
                break;
            }
            case "fov": {
                Minecraft.getMinecraft().gameSettings.fovSetting = Integer.parseInt(args[1]);
                break;
            }
            case "reloadmodels": {
                try {
                    Method m = PixelmonModelRegistry.class.getDeclaredMethod("init", new Class[0]);
                    m.setAccessible(true);
                    m.invoke(null, new Object[0]);
                }
                catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e2) {
                    e2.printStackTrace();
                }
                GenericSmdModel.loadedModels.forEach(GenericSmdModel::reloadModel);
                break;
            }
            case "itemsprite": {
                ItemStack stack = new ItemStack(PixelmonItems.itemPixelmonSprite);
                NBTTagCompound tagCompound = new NBTTagCompound();
                tagCompound.setString("SpriteName", args[1]);
                stack.setTagCompound(tagCompound);
                ((EntityPlayerMP)sender).inventory.addItemStackToInventory(stack);
                break;
            }
            case "render": {
                Minecraft.getMinecraft().gameSettings.renderDistanceChunks = 1000;
                break;
            }
            case "collection": {
                EntityPlayerMP player = (EntityPlayerMP)sender;
                int n = Integer.parseInt(args[1]);
                int radius = Integer.parseInt(args[2]);
                long tic = System.currentTimeMillis();
                for (int i = 0; i < n; ++i) {
                    new BlockCollection(player.world, (int)player.posX - radius, (int)player.posX + radius, (int)player.posY - radius, (int)player.posY + radius, (int)player.posZ - radius, (int)player.posZ + radius);
                }
                long toc = System.currentTimeMillis();
                this.sendMessage(sender, "Time to process " + n + " collections of radius " + radius + " (" + n * 8 * radius * radius * radius + " blocks): " + (toc - tic) + " milliseconds.", new Object[0]);
                break;
            }
            case "model-scale": {
                if (args.length != 3) {
                    sender.sendMessage(new TextComponentString("Usage: /pixeldebug model-scale <pokemon:form> <scale>"));
                    break;
                }
                try {
                    String name = args[1];
                    int form = -1;
                    if (args[1].contains(":")) {
                        name = args[1].split(":")[0];
                        form = Debug.parseInt(args[1].split(":")[1]);
                    }
                    EnumSpecies pokemon = EnumSpecies.valueOf(name);
                    double scale = Debug.parseDouble(args[2]);
                    PixelmonModelBase model = (PixelmonModelBase)PixelmonModelRegistry.getModel(pokemon, pokemon.getFormEnum(form));
                    if (model instanceof PixelmonModelSmd) {
                        ValveStudioModel valve = ((PixelmonModelSmd)model).theModel;
                        Field field = ValveStudioModel.class.getDeclaredField("scale");
                        field.setAccessible(true);
                        field.setFloat(valve, (float)scale);
                        sender.sendMessage(new TextComponentString("scale changed. This does not effect pokemon already spawned in the world."));
                        break;
                    }
                    sender.sendMessage(new TextComponentString("That model is not smd!"));
                }
                catch (IllegalArgumentException e3) {
                    sender.sendMessage(new TextComponentString("No model by that name found!"));
                }
                catch (IllegalAccessException | NoSuchFieldException e4) {
                    e4.printStackTrace();
                }
                break;
            }
            case "dialogue": {
                ArrayList<Dialogue> dialogues = new ArrayList<Dialogue>();
                dialogues.add(Dialogue.builder().setName("Gus").setText("Hello, friend").addChoice(Choice.builder().setText("Hello, Gus").setHandle(e -> e.reply(Dialogue.builder().setName("Gus").setText("Enjoy your day!").build())).build()).addChoice(Choice.builder().setText("You are everything wrong with the world.").setHandle(e -> e.reply(Dialogue.builder().setName("Gus").setText("Wow, ok then.").build())).build()).build());
                Dialogue.setPlayerDialogueData((EntityPlayerMP)sender, dialogues, true);
                break;
            }
            case "spawns": {
                for (EnumSpecies pokemon : EnumSpecies.values()) {
                    SpawnSet set = SetLoader.getDefaultSpawnSetFor(pokemon);
                    if (set == null) continue;
                    set.export("exportDir");
                }
                break;
            }
            case "updatespawns": {
                for (EnumSpecies pokemon : EnumSpecies.values()) {
                    SpawnSet set = (SpawnSet)SetLoader.getSet(pokemon.name());
                    if (set == null) continue;
                    if (set.spawnInfos.size() == 1) {
                        SpawnInfoPokemon pokemonInfo = (SpawnInfoPokemon)set.spawnInfos.get(0);
                        PokemonSpec spec = pokemonInfo.getPokemonSpec();
                        EnumSpecies species = EnumSpecies.getFromName(spec.name).get();
                        if (!EnumSpecies.formList.containsKey((Object)species)) {
                            spec.form = -1;
                        } else if (((IEnumForm)EnumSpecies.formList.get(species).get(0)).getClass() == EnumForms.class) {
                            // empty if block
                        }
                        pokemonInfo.setPokemon(spec);
                        pokemonInfo.tag = null;
                        set.spawnInfos.set(0, pokemonInfo);
                    }
                    set.export("exportDir");
                }
                break;
            }
            case "spawnwiki": {
                Function<String, String> raiseFirstCap = s -> s.toUpperCase().charAt(0) + s.substring(1).toLowerCase();
                BiFunction<ArrayList, Boolean, String> listToString = (list, cap) -> {
                    if (list.isEmpty()) {
                        return "";
                    }
                    StringBuilder str = new StringBuilder(cap != false ? (String)raiseFirstCap.apply(list.get(0).toString()) : list.get(0).toString());
                    for (int i = 1; i < list.size(); ++i) {
                        str.append(", ").append(cap != false ? (String)raiseFirstCap.apply(list.get(i).toString()) : list.get(i).toString());
                    }
                    return str + "\n";
                };
                File file = new File("wikidata.txt");
                try {
                    file.createNewFile();
                    PrintWriter pw = new PrintWriter(file);
                    for (EnumSpecies pokemon : EnumSpecies.values()) {
                        SpawnSet set = (SpawnSet)SetLoader.getSet(pokemon.name);
                        if (set == null) {
                            System.out.println("Empty: " + pokemon.name);
                            continue;
                        }
                        String wikiBit = "|'''#" + pokemon.getNationalPokedexNumber() + "'''\n";
                        wikiBit = wikiBit + "|" + pokemon.name + "\n";
                        ArrayList<LocationType> locations = new ArrayList<LocationType>();
                        ArrayList<String> biomes = new ArrayList<String>();
                        ArrayList<WorldTime> times = new ArrayList<WorldTime>();
                        ArrayList<WeatherType> weathers = new ArrayList<WeatherType>();
                        for (SpawnInfo info : set.spawnInfos) {
                            for (LocationType location : info.locationTypes) {
                                if (locations.contains(location)) continue;
                                locations.add(location);
                            }
                            for (Biome biome : info.condition.biomes) {
                                PixelmonBiomeDictionary.PixelmonBiomeInfo biomeInfo = PixelmonBiomeDictionary.getBiomeInfo(biome);
                                if (biomes.contains(biomeInfo.biomeEnglishName)) continue;
                                biomes.add(biomeInfo.biomeEnglishName);
                            }
                            for (WorldTime time : info.condition.times) {
                                if (times.contains((Object)time)) continue;
                                times.add(time);
                            }
                            for (WeatherType weather : info.condition.weathers) {
                                if (weathers.contains((Object)weather)) continue;
                                weathers.add(weather);
                            }
                        }
                        String locString = listToString.apply(locations, true);
                        if (locString.equals("")) continue;
                        wikiBit = wikiBit + "|" + locString;
                        String biomeString = listToString.apply(biomes, false);
                        if (biomeString.equals("")) continue;
                        wikiBit = wikiBit + "|" + biomeString;
                        String timeString = listToString.apply(times, true);
                        if (timeString.equals("")) continue;
                        wikiBit = wikiBit + "|" + timeString;
                        String weatherString = listToString.apply(weathers, true);
                        if (weatherString.equals("")) continue;
                        wikiBit = wikiBit + "|" + weatherString;
                        wikiBit = wikiBit + "|-\n";
                        pw.write(wikiBit);
                    }
                    pw.flush();
                    pw.close();
                    System.out.println("Done");
                }
                catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                break;
            }
            case "testspec": {
                String action = args[1].toLowerCase();
                if (!action.equals("matches") && !action.equals("apply")) {
                    throw new CommandException("Use with /pixeldebug testspec <matches/apply> <slot> <spec>", new Object[0]);
                }
                PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)sender).get();
                EntityPixelmon pixelmon = storage.getPokemon(storage.getIDFromPosition(Debug.parseInt(args[2], 0, 5)), sender.getEntityWorld());
                CharSequence[] specs = Arrays.asList(args).subList(3, args.length).toArray(new String[0]);
                sender.sendMessage(new TextComponentString(String.format("Testing '%s' on %s.", String.join((CharSequence)" ", specs), pixelmon.getName())));
                if (action.equals("matches")) {
                    boolean result = new PokemonSpec((String[])specs).matches(pixelmon);
                    sender.sendMessage(new TextComponentString("Matches: " + result));
                    break;
                }
                new PokemonSpec((String[])specs).apply(pixelmon);
                sender.sendMessage(new TextComponentString("Applied!"));
            }
        }
    }
}

