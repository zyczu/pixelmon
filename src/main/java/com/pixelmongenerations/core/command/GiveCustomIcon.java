/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.item.ItemCustomIcon;
import com.pixelmongenerations.core.enums.items.EnumCustomIcon;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;

public class GiveCustomIcon
extends PixelmonCommand {
    public GiveCustomIcon() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "givecustomicon";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/givecustomicon <icon name>";
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("gci");
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        if (!(sender instanceof EntityPlayerMP)) {
            return;
        }
        if (args.length == 0 || args.length > 1) {
            sender.sendMessage(new TextComponentString(this.getUsage(sender)));
            return;
        }
        EntityPlayerMP player = (EntityPlayerMP)sender;
        ItemStack itemStack = ItemCustomIcon.getIcon(args[0]);
        player.inventory.addItemStackToInventory(itemStack);
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos pos) {
        return args.length == 1 ? this.tabCompleteCustomItems(args) : Collections.emptyList();
    }

    public List<String> tabCompleteCustomItems(String[] args) {
        return PixelmonCommand.getListOfStringsMatchingLastWord(args, EnumCustomIcon.getNameList());
    }
}

