/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.command;

import com.pixelmongenerations.api.command.PixelmonCommand;
import com.pixelmongenerations.common.world.gen.structure.StructureInfo;
import com.pixelmongenerations.common.world.gen.structure.StructureRegistry;
import com.pixelmongenerations.common.world.gen.structure.gyms.GymInfo;
import com.pixelmongenerations.common.world.gen.structure.towns.ComponentTownPart;
import com.pixelmongenerations.common.world.gen.structure.util.StructureScattered;
import com.pixelmongenerations.common.world.gen.structure.worldGen.WorldGenGym;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.List;
import java.util.Random;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;

public class Struc
extends PixelmonCommand {
    public Struc() {
        super(new PixelmonCommand[0]);
    }

    @Override
    public String getName() {
        return "struc";
    }

    @Override
    public void execute(ICommandSender sender, String[] args) throws CommandException {
        StructureInfo structure;
        World world = Struc.getCommandSenderAsPlayer((ICommandSender)sender).world;
        BlockPos cc = sender.getPosition();
        Random random = RandomHelper.staticRandomWithXZSeed(world, cc.getX() >> 4, cc.getZ() >> 4);
        if (args.length == 0) {
            structure = StructureRegistry.getScatteredStructureFromBiome(random, world.getBiome(cc));
            if (structure == null) {
                sender.sendMessage(new TextComponentTranslation("pixelmon.command.struc.nostruc", world.getBiome(cc).getRegistryName().getPath()));
                return;
            }
        } else {
            if (args[0].equals("list")) {
                StringBuilder stringBuilder = new StringBuilder();
                for (String name : StructureRegistry.structureIds) {
                    stringBuilder.append(name).append(" ");
                }
                sender.sendMessage(new TextComponentString(stringBuilder.toString()));
                return;
            }
            structure = StructureRegistry.getByID(args[0]);
        }
        if (structure == null) {
            sender.sendMessage(new TextComponentTranslation("pixelmon.command.struc.notfound", args[0]));
            return;
        }
        StructureScattered ss = structure.createStructure(random, cc, true, true, Struc.getCommandSenderAsPlayer(sender).getHorizontalFacing());
        if (!ss.generate(world, random)) {
            sender.sendMessage(new TextComponentTranslation("pixelmon.command.struc.cantfit", cc.toString()));
        } else if (structure instanceof GymInfo) {
            GymInfo gymInfo = (GymInfo)structure;
            int level = -1;
            if (args.length > 1 && args[1].startsWith("lvl")) {
                try {
                    int parseLevel = Integer.parseInt(args[1].replaceFirst("lvl", ""));
                    if (parseLevel > 0) {
                        level = Math.min(parseLevel, PixelmonServerConfig.maxLevel);
                    }
                }
                catch (NumberFormatException numberFormatException) {
                    // empty catch block
                }
            }
            gymInfo.level = level;
            WorldGenGym.spawnNPCs(world, ss, gymInfo);
        } else {
            ComponentTownPart.spawnVillagers(ss, structure, world, ss.getBoundingBox());
        }
    }

    @Override
    public String getUsage(ICommandSender icommandsender) {
        return "/struc [name]";
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        return StructureRegistry.structureIds;
    }
}

