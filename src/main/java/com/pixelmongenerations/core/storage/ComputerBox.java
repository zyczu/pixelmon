/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage;

import com.pixelmongenerations.api.pc.BackgroundRegistry;
import com.pixelmongenerations.api.pc.IBackground;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.pcClientStorage.PCAdd;
import com.pixelmongenerations.core.network.packetHandlers.pcClientStorage.PCClear;
import com.pixelmongenerations.core.network.packetHandlers.pcClientStorage.PCRemove;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.storage.deepstorage.DeepStorageManager;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.helper.NbtHelper;
import java.util.ArrayList;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class ComputerBox {
    public boolean hasChanged = false;
    public static final int boxLimit = 30;
    public int position;
    private NBTTagCompound[] storedPokemon = new NBTTagCompound[30];
    private PlayerComputerStorage parentStorage;
    private String name;
    public String background;

    public ComputerBox(PlayerComputerStorage parentStorage, int position) {
        this.position = position;
        this.hasChanged = true;
        this.parentStorage = parentStorage;
        this.name = "Box: " + Integer.toString(this.position + 1);
        this.background = "box_forest";
    }

    public boolean hasSpace() {
        int count = 0;
        for (int i = 0; i < 30; ++i) {
            if (this.storedPokemon[i] == null) continue;
            ++count;
        }
        return count < 30;
    }

    public int count() {
        int count = 0;
        for (int i = 0; i < 30; ++i) {
            if (this.storedPokemon[i] == null) continue;
            ++count;
        }
        return count;
    }

    public void add(EntityPixelmon p) {
        NBTTagCompound n = new NBTTagCompound();
        if (this.parentStorage.playerStorage != null) {
            p.setPokemonId(this.parentStorage.playerStorage.getNewPokemonID());
        }
        p.writeEntityToNBT(n);
        p.writeToNBT(n);
        int pos = this.getNextSpace();
        n.setString("id", p.getPokemonName());
        n.setBoolean("IsInBall", true);
        n.setBoolean("IsShiny", p.isShiny());
        if (p.getOwner() != null && n.getString("originalTrainer").isEmpty()) {
            n.setString("originalTrainer", p.getOwner().getDisplayName().getUnformattedText());
            n.setString("originalTrainerUUID", p.getOwner().getUniqueID().toString());
        }
        n.setInteger("PixelmonOrder", pos);
        n.setInteger("BoxNumber", this.position);
        if (n.getShort("Health") > 0) {
            n.setBoolean("IsFainted", false);
        }
        if (p.type.get(0) != p.baseStats.type1) {
            n.setShort("primaryType", (short)((EnumType)((Object)p.type.get(0))).getIndex());
        }
        if (p.type.size() > 1) {
            if (p.type.get(1) != p.baseStats.type2) {
                n.setShort("secondaryType", (short)((EnumType)((Object)p.type.get(1))).getIndex());
            }
        } else {
            n.setShort("secondaryType", (short)-1);
        }
        this.storedPokemon[pos] = n;
        this.hasChanged = true;
    }

    public int getNextSpace() {
        for (int i = 0; i < 30; ++i) {
            if (this.storedPokemon[i] != null) continue;
            return i;
        }
        return 0;
    }

    public NBTTagCompound get(int[] id) {
        for (NBTTagCompound n : this.storedPokemon) {
            if (n == null || !PixelmonMethods.isIDSame(n, id)) continue;
            return n;
        }
        return null;
    }

    public NBTTagCompound[] getStoredPokemon() {
        for (int i = 0; i < this.storedPokemon.length; ++i) {
            NBTTagCompound n = this.storedPokemon[i];
            if (n == null || !n.getString("Name").equals("")) continue;
            this.storedPokemon[i] = null;
        }
        return this.storedPokemon;
    }

    public NBTTagCompound getNBTByPosition(int pos) {
        return this.storedPokemon[pos];
    }

    public void load(NBTTagCompound boxTag) {
        for (int i = 0; i < 30; ++i) {
            this.storedPokemon[i] = null;
        }
        ArrayList<NBTTagCompound> unaddedPokemon = new ArrayList<NBTTagCompound>();
        for (int i = 0; i < 30; ++i) {
            if (!boxTag.hasKey("pc" + i)) continue;
            NBTTagCompound tag = boxTag.getCompoundTag("pc" + i);
            if (!tag.hasKey("pixelmonID1")) {
                int[] id = this.parentStorage.playerStorage.getNewPokemonID();
                tag.setInteger("pixelmonID1", id[0]);
                tag.setInteger("pixelmonID2", id[1]);
            }
            if (!EnumSpecies.hasPokemonAnyCase(tag.getString("Name"))) {
                unaddedPokemon.add(tag);
                boxTag.removeTag("pc" + i);
                continue;
            }
            this.storedPokemon[i] = NbtHelper.fixPokemonData(tag);
            this.hasChanged = true;
        }
        this.name = boxTag.getString("name");
        String string = this.background = boxTag.hasKey("background") ? boxTag.getString("background") : "box_forest";
        if (this.name.isEmpty()) {
            this.name = "Box: " + Integer.toString(this.position + 1);
            this.hasChanged = true;
        }
        if (!unaddedPokemon.isEmpty()) {
            DeepStorageManager.bury(this.parentStorage.getPlayerID(), unaddedPokemon, false);
            this.hasChanged = true;
        } else {
            this.hasChanged = false;
        }
    }

    public void save(NBTTagCompound nbt) {
        for (int i = 0; i < 30; ++i) {
            if (this.storedPokemon[i] == null) continue;
            this.storedPokemon[i].setInteger("PixelmonOrder", i);
            nbt.setTag("pc" + i, this.storedPokemon[i]);
        }
        nbt.setString("name", this.name);
        nbt.setString("background", this.background);
        this.hasChanged = true;
    }

    public void addToFirstSpace(NBTTagCompound n) {
        for (int i = 0; i < 30; ++i) {
            if (this.storedPokemon[i] != null) continue;
            n.setInteger("PixelmonOrder", i);
            this.storedPokemon[i] = n;
            this.updatePCData(i);
            this.hasChanged = true;
            return;
        }
    }

    public void changePokemon(int boxPos, NBTTagCompound n) {
        if (n != null) {
            n.setInteger("PixelmonOrder", boxPos);
            n.setInteger("BoxNumber", this.position);
        }
        this.storedPokemon[boxPos] = n;
        this.hasChanged = true;
    }

    private void updatePCData(int pos) {
        if (this.parentStorage.getPlayer() != null) {
            Pixelmon.NETWORK.sendTo(new PCClear(), this.parentStorage.getPlayer());
            this.hasChanged = true;
            if (this.storedPokemon[pos] != null) {
                Pixelmon.NETWORK.sendTo(new PCAdd(new PixelmonData(this.storedPokemon[pos])), this.parentStorage.getPlayer());
            } else {
                Pixelmon.NETWORK.sendTo(new PCRemove(this.position, pos), this.parentStorage.getPlayer());
            }
            this.hasChanged = true;
        }
    }

    public boolean contains(int[] id) {
        for (NBTTagCompound n : this.storedPokemon) {
            if (n == null || !PixelmonMethods.isIDSame(n, id)) continue;
            return true;
        }
        return false;
    }

    public EntityPixelmon getPokemonEntity(int[] pokemonID, World worldObj) {
        for (NBTTagCompound n : this.storedPokemon) {
            if (n == null || !PixelmonMethods.isIDSame(n, pokemonID)) continue;
            n.setFloat("FallDistance", 0.0f);
            n.setBoolean("IsInBall", false);
            EntityPixelmon e = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(n, worldObj);
            e.setOwnerId(this.parentStorage.getPlayerID());
            e.playerOwned = true;
            e.motionZ = 0.0;
            e.motionY = 0.0;
            e.motionX = 0.0;
            e.isDead = false;
            return e;
        }
        return null;
    }

    public void updatePokemonEntry(EntityPixelmon p) {
        for (int i = 0; i < this.storedPokemon.length; ++i) {
            NBTTagCompound nbt = this.storedPokemon[i];
            if (nbt == null || !PixelmonMethods.isIDSame(nbt, p)) continue;
            NBTTagCompound n = new NBTTagCompound();
            p.writeEntityToNBT(n);
            p.writeToNBT(n);
            n.setString("id", p.getPokemonName());
            n.setBoolean("IsInBall", true);
            n.setBoolean("IsShiny", p.isShiny());
            n.setInteger("PixelmonOrder", nbt.getInteger("PixelmonOrder"));
            n.setBoolean("isInRanch", nbt.getBoolean("isInRanch"));
            n.setInteger("BoxNumber", this.position);
            if (n.getShort("Health") > 0) {
                n.setBoolean("IsFainted", false);
            }
            this.storedPokemon[i] = n;
            this.hasChanged = true;
        }
    }

    public NBTTagCompound getPokemonNBT(int[] id) {
        for (NBTTagCompound n : this.storedPokemon) {
            if (n == null || !PixelmonMethods.isIDSame(n, id)) continue;
            return n;
        }
        return null;
    }

    public void updatePokemonNBT(int[] id, NBTTagCompound nbt) {
        for (int i = 0; i < this.storedPokemon.length; ++i) {
            NBTTagCompound n = this.storedPokemon[i];
            if (n == null || !PixelmonMethods.isIDSame(n, id)) continue;
            this.storedPokemon[i] = nbt;
            this.hasChanged = true;
        }
    }

    public void unlockAllPokemon() {
        for (int i = 0; i < 30; ++i) {
            if (this.storedPokemon[i] == null) continue;
            this.storedPokemon[i].setBoolean("isInRanch", false);
            this.hasChanged = true;
        }
    }

    public int getPlayerIdFromPokemon() {
        for (int i = 0; i < 30; ++i) {
            if (this.storedPokemon[i] == null) continue;
            return PixelmonMethods.getID(this.storedPokemon[i])[0];
        }
        return -1;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.hasChanged = true;
        this.name = name;
    }

    public String getBackground() {
        return this.background;
    }

    public boolean setBackground(String background) {
        IBackground bg = BackgroundRegistry.getBackground(background);
        if (bg == null) {
            return false;
        }
        if (!bg.hasUnlocked(this.parentStorage.getPlayer())) {
            return false;
        }
        this.hasChanged = true;
        this.background = background;
        return true;
    }
}

