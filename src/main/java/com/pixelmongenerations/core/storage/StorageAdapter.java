/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage;

import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.io.File;
import java.util.UUID;
import net.minecraft.nbt.NBTTagCompound;

public interface StorageAdapter {
    public NBTTagCompound readPlayerData(UUID var1);

    public NBTTagCompound readPlayerData(UUID var1, File var2);

    public void writePlayerData(UUID var1, NBTTagCompound var2);

    public void writePlayerData(UUID var1, NBTTagCompound var2, File var3, String var4);

    public NBTTagCompound readComputerData(UUID var1);

    public NBTTagCompound readComputerData(UUID var1, File var2);

    public void writeComputerData(UUID var1, NBTTagCompound var2);

    public void writeComputerData(UUID var1, NBTTagCompound var2, File var3, String var4);

    default public void savePlayerStorage(PlayerStorage storage) {
        NBTTagCompound nbt = new NBTTagCompound();
        storage.writeToNBT(nbt);
        PixelmonStorage.storageAdapter.writePlayerData(storage.getPlayerID(), nbt);
    }

    default public void savePlayerStorage(PlayerStorage storage, File file, String extension) {
        NBTTagCompound nbt = new NBTTagCompound();
        storage.writeToNBT(nbt);
        PixelmonStorage.storageAdapter.writePlayerData(storage.getPlayerID(), nbt, file, extension);
    }

    default public void saveComputerStorage(PlayerComputerStorage storage) {
        if (storage.hasChanges()) {
            NBTTagCompound nbt = new NBTTagCompound();
            storage.writeToNBT(nbt);
            this.writeComputerData(storage.getPlayerID(), nbt);
        }
    }

    default public void saveComputerStorage(PlayerComputerStorage storage, File file, String extension) {
        if (storage.hasChanges()) {
            NBTTagCompound nbt = new NBTTagCompound();
            storage.writeToNBT(nbt);
            this.writeComputerData(storage.getPlayerID(), nbt, file, extension);
        }
    }
}

