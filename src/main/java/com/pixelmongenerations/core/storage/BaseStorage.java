/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage;

import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class BaseStorage {
    protected final UUID uuid;

    public BaseStorage(UUID uuid) {
        this.uuid = uuid;
    }

    public boolean isOffline() {
        return this.getPlayer() == null;
    }

    public UUID getPlayerID() {
        return this.uuid;
    }

    public EntityPlayerMP getPlayer() {
        return FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUUID(this.uuid);
    }
}

