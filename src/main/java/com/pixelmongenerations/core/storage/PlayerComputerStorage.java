/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage;

import com.pixelmongenerations.api.pc.BackgroundRegistry;
import com.pixelmongenerations.api.pc.IBackground;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.BaseStorage;
import com.pixelmongenerations.core.storage.ComputerBox;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;

public class PlayerComputerStorage
extends BaseStorage {
    public static int boxCount = PixelmonConfig.computerBoxes;
    private ComputerBox[] storageBoxes = new ComputerBox[boxCount];
    public PlayerStorage playerStorage;
    public int lastBoxOpen = 0;
    private HashSet<String> unlockedBackgrounds = new HashSet();

    public PlayerComputerStorage(MinecraftServer server, UUID uuid) {
        super(uuid);
        for (int i = 0; i < boxCount; ++i) {
            this.storageBoxes[i] = new ComputerBox(this, i);
        }
    }

    public PlayerComputerStorage(EntityPlayerMP player) {
        this(player.world.getMinecraftServer(), player.getUniqueID());
        Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (optstorage.isPresent()) {
            this.playerStorage = optstorage.get();
        }
    }

    public HashSet<String> getUnlockedBackgrounds() {
        return this.unlockedBackgrounds;
    }

    public boolean addUnlockedBackground(String background) {
        IBackground bg = BackgroundRegistry.getBackground(background);
        if (bg == null) {
            return false;
        }
        if (this.unlockedBackgrounds.contains(background)) {
            return false;
        }
        this.unlockedBackgrounds.add(background);
        return true;
    }

    public boolean addToComputer(NBTTagCompound nbtTagCompound) {
        for (ComputerBox c : this.storageBoxes) {
            if (!c.hasSpace()) continue;
            c.addToFirstSpace(nbtTagCompound);
            return true;
        }
        return false;
    }

    public void addToComputer(EntityPixelmon p) {
        for (ComputerBox c : this.storageBoxes) {
            if (!c.hasSpace()) continue;
            c.add(p);
            return;
        }
        ChatHandler.sendChat(p.getOwner(), "pixelmon.storage.compfull", new Object[0]);
    }

    public int count() {
        int count = 0;
        for (ComputerBox c : this.storageBoxes) {
            count += c.count();
        }
        return count;
    }

    public void changePokemon(int box, int boxPos, NBTTagCompound n) {
        if (n != null) {
            n.setInteger("BoxNumber", box);
            n.setInteger("PixelmonOrder", boxPos);
        }
        ComputerBox c = this.storageBoxes[box];
        c.changePokemon(boxPos, n);
    }

    public void addToBox(int originalBox, NBTTagCompound n) {
        ComputerBox c = this.storageBoxes[originalBox];
        if (n != null) {
            n.setInteger("BoxNumber", originalBox);
        }
        c.addToFirstSpace(n);
    }

    public ComputerBox getBox(int boxNumber) {
        return this.storageBoxes[boxNumber];
    }

    public ComputerBox getBoxFromPosition(int pos) {
        for (int i = 0; i < boxCount; ++i) {
            if (this.storageBoxes[i].position != pos) continue;
            return this.storageBoxes[i];
        }
        return null;
    }

    public ComputerBox[] getBoxList() {
        return this.storageBoxes;
    }

    public void readFromNBT(NBTTagCompound var1) {
        if (var1.hasKey("lastBoxOpen")) {
            this.lastBoxOpen = var1.getInteger("lastBoxOpen");
            if (this.lastBoxOpen >= boxCount) {
                this.lastBoxOpen = 0;
            }
        }
        for (int i = 0; i < this.storageBoxes.length; ++i) {
            NBTBase nbtbase;
            if (!var1.hasKey("" + i) || !((nbtbase = var1.getTag("" + i)) instanceof NBTTagCompound)) continue;
            NBTTagCompound tag = (NBTTagCompound)nbtbase;
            ComputerBox c = new ComputerBox(this, i);
            c.load(tag);
            this.storageBoxes[i] = c;
        }
        int backgrounds = var1.getInteger("BackgroundCount");
        if (backgrounds > 0) {
            for (int i = 0; i < backgrounds; ++i) {
                String background = var1.getString("Background" + i);
                if (background.isEmpty() || BackgroundRegistry.getBackground(background) == null) continue;
                this.unlockedBackgrounds.add(background);
            }
        }
    }

    public void writeToNBT(NBTTagCompound n) {
        n.setInteger("lastBoxOpen", this.lastBoxOpen);
        for (ComputerBox c : this.storageBoxes) {
            NBTTagCompound cTag = new NBTTagCompound();
            c.save(cTag);
            n.setTag("" + c.position, cTag);
        }
        if (this.unlockedBackgrounds.size() > 0) {
            int i = 0;
            for (String background : this.unlockedBackgrounds) {
                n.setString("Background" + i, background);
                ++i;
            }
            n.setInteger("BackgroundCount", i);
        }
    }

    public boolean hasChanges() {
        for (ComputerBox c : this.storageBoxes) {
            if (!c.hasChanged) continue;
            return true;
        }
        return false;
    }

    public boolean contains(int[] pokemonID) {
        for (ComputerBox b : this.storageBoxes) {
            if (!b.contains(pokemonID)) continue;
            return true;
        }
        return false;
    }

    public EntityPixelmon getPokemonEntity(int[] pokemonID, World world) {
        for (ComputerBox b : this.storageBoxes) {
            if (!b.contains(pokemonID)) continue;
            return b.getPokemonEntity(pokemonID, world);
        }
        return null;
    }

    public void updatePokemonEntry(EntityPixelmon p) {
        for (ComputerBox b : this.storageBoxes) {
            if (!b.contains(p.getPokemonId())) continue;
            b.updatePokemonEntry(p);
        }
    }

    public NBTTagCompound getPokemonNBT(int[] id) {
        for (ComputerBox b : this.storageBoxes) {
            if (!b.contains(id)) continue;
            return b.getPokemonNBT(id);
        }
        return null;
    }

    public void updatePokemonNBT(int[] id, NBTTagCompound nbt) {
        for (ComputerBox b : this.storageBoxes) {
            if (!b.contains(id)) continue;
            b.updatePokemonNBT(id, nbt);
        }
    }

    public void setChanged(int[] pokemonID) {
        for (ComputerBox b : this.storageBoxes) {
            if (!b.contains(pokemonID)) continue;
            b.hasChanged = true;
        }
    }
}

