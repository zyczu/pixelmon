/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage;

public class PlayerNotLoadedException
extends Exception {
    public PlayerNotLoadedException() {
    }

    public PlayerNotLoadedException(String message) {
        super(message);
    }
}

