/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage.deepstorage;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.storage.deepstorage.DeepStorage;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class DeepStorageManager {
    private static final HashMap<UUID, DeepStorage> deepStorages = new HashMap();

    public static File getFile(UUID uuid) {
        return new File(DimensionManager.getCurrentSaveRootDirectory(), "pokemon/deepStorage/" + uuid.toString() + ".deep");
    }

    public static void bury(UUID uuid, ArrayList<NBTTagCompound> unaddedPokemon, boolean party) {
        DeepStorage deepStorage = DeepStorageManager.getOrCreateDeepStorage(uuid);
        if (deepStorage != null) {
            for (NBTTagCompound nbt : unaddedPokemon) {
                deepStorage.put(nbt);
            }
            DeepStorageManager.save(uuid);
            EntityPlayerMP player = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUUID(uuid);
            if (player != null) {
                TextComponentString msg = new TextComponentString((Object)((Object)TextFormatting.GRAY) + "" + unaddedPokemon.size() + (party ? " party" : " PC") + " un-added Pok\u00e9mon have been safely stowed in deep storage");
                player.sendMessage(msg);
            }
        }
    }

    public static boolean hasPokemonInDeepStorage(UUID uuid) {
        File file = DeepStorageManager.getFile(uuid);
        return file.exists();
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    public static DeepStorage getOrCreateDeepStorage(UUID uuid) {
        if (deepStorages.containsKey(uuid)) {
            return deepStorages.get(uuid);
        }
        if (!DeepStorageManager.hasPokemonInDeepStorage(uuid)) {
            NBTTagCompound nbt = new NBTTagCompound();
            DeepStorage deepStorage2 = new DeepStorage(nbt);
            deepStorages.put(uuid, deepStorage2);
            return deepStorage2;
        }
        File file = DeepStorageManager.getFile(uuid);
        file.getParentFile().mkdirs();
        try (DataInputStream dataStream = new DataInputStream(new FileInputStream(file));){
            NBTTagCompound nbt = CompressedStreamTools.read(dataStream);
            DeepStorage deepStorage2 = new DeepStorage(nbt);
            deepStorages.put(uuid, deepStorage2);
            DeepStorage deepStorage = deepStorage2;
            return deepStorage;
        }
        catch (IOException e) {
            if (!PixelmonConfig.printErrors) {
                return null;
            }
            Pixelmon.LOGGER.error("Couldn't read deep store data file for " + uuid, (Throwable)e);
            return null;
        }
    }

    public static void save(UUID uuid) {
        DeepStorage deepStorage = deepStorages.get(uuid);
        if (deepStorage != null) {
            File file = DeepStorageManager.getFile(uuid);
            file.getParentFile().mkdirs();
            file.delete();
            if (!deepStorage.isEmpty()) {
                deepStorage.write(file);
            }
        }
    }
}

