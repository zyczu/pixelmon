/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableList
 */
package com.pixelmongenerations.core.storage.deepstorage;

import com.google.common.collect.ImmutableList;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;

public class DeepStorage {
    private ArrayList<NBTTagCompound> archivedPokemon = new ArrayList();

    public DeepStorage(NBTTagCompound nbt) {
        int i = 0;
        while (nbt.hasKey("pokemon" + i)) {
            this.archivedPokemon.add(nbt.getCompoundTag("pokemon" + i++));
        }
        this.clearDuplicates();
    }

    public boolean isEmpty() {
        return this.archivedPokemon.isEmpty();
    }

    public void write(File file) {
        block15: {
            try {
                file.createNewFile();
                NBTTagCompound deepStorageNBT = new NBTTagCompound();
                this.clearDuplicates();
                for (int i = 0; i < this.archivedPokemon.size(); ++i) {
                    deepStorageNBT.setTag("pokemon" + i, this.archivedPokemon.get(i));
                }
                try (DataOutputStream dataStream = new DataOutputStream(new FileOutputStream(file));){
                    CompressedStreamTools.write(deepStorageNBT, dataStream);
                }
            }
            catch (IOException e) {
                if (!PixelmonConfig.printErrors) break block15;
                Pixelmon.LOGGER.error("Couldn't write deep store data file for " + file.getName(), (Throwable)e);
            }
        }
    }

    public void clearDuplicates() {
        ArrayList<Integer[]> ids = new ArrayList<Integer[]>();
        Iterator<NBTTagCompound> it = this.archivedPokemon.iterator();
        while (it.hasNext()) {
            NBTTagCompound nbt = it.next();
            int[] idraw = PixelmonMethods.getID(nbt);
            Integer[] id = new Integer[]{idraw[0], idraw[1]};
            if (ids.contains(id)) {
                it.remove();
                continue;
            }
            ids.add(id);
        }
    }

    public void put(NBTTagCompound nbt) {
        nbt.setBoolean("isInRanch", false);
        this.archivedPokemon.add(nbt);
    }

    public ArrayList<NBTTagCompound> tryRetrieve() {
        ArrayList<NBTTagCompound> freedPokemon = new ArrayList<NBTTagCompound>();
        for (NBTTagCompound nbt : this.archivedPokemon) {
            if (!EnumSpecies.hasPokemonAnyCase(nbt.getString("Name"))) continue;
            freedPokemon.add(nbt);
        }
        this.archivedPokemon.removeAll(freedPokemon);
        return freedPokemon;
    }

    public ImmutableList<NBTTagCompound> getArchivedPokemon() {
        return ImmutableList.copyOf(this.archivedPokemon);
    }
}

