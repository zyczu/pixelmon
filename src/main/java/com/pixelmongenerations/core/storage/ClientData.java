/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage;

public class ClientData {
    public static int playerMoney;
    public static boolean openMegaItemGui;
    public static boolean openShinyItemGui;
    public static boolean openDynamaxItemGui;

    static {
        openMegaItemGui = false;
        openShinyItemGui = false;
        openDynamaxItemGui = false;
    }
}

