/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.storage.BaseStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.storage.StorageAdapter;
import java.io.File;
import java.util.Deque;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class AsyncStorageWrapper
implements StorageAdapter {
    private ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
    private Deque<PlayerStorage> playerStorages = new ConcurrentLinkedDeque<PlayerStorage>();
    private Deque<PlayerComputerStorage> playerComputerStorages = new ConcurrentLinkedDeque<PlayerComputerStorage>();
    private final StorageAdapter storageAdapter;
    private final int interval;

    public AsyncStorageWrapper(StorageAdapter storageAdapter) {
        this.storageAdapter = storageAdapter;
        this.interval = PixelmonConfig.asyncInterval;
        this.executor.scheduleAtFixedRate(() -> {
            try {
                if (FMLCommonHandler.instance().getMinecraftServerInstance() != null && FMLCommonHandler.instance().getMinecraftServerInstance().isServerRunning()) {
                    Pixelmon.LOGGER.info("Saving Player & Party Data...");
                    this.saveAllQueued();
                    Pixelmon.LOGGER.info("Saved Player & Party data successfully. Next save in: " + this.interval + " seconds.");
                }
            }
            catch (Throwable t) {
                Pixelmon.LOGGER.error("Error while saving pixelmon data async! This could be bad!", t);
            }
        }, this.interval, this.interval, TimeUnit.SECONDS);
        MinecraftForge.EVENT_BUS.register(this);
        Pixelmon.LOGGER.info("Async world saving ACTIVE!!!");
    }

    public PlayerStorage getQueuedStorage(UUID uuid) {
        for (PlayerStorage storage : this.playerStorages) {
            if (!storage.getPlayerID().equals(uuid)) continue;
            return storage;
        }
        return null;
    }

    public PlayerComputerStorage getQueuedComputerStorage(UUID uuid) {
        for (PlayerComputerStorage storage : this.playerComputerStorages) {
            if (!storage.getPlayerID().equals(uuid)) continue;
            return storage;
        }
        return null;
    }

    public void flush() {
        try {
            this.executor.submit(this::saveAllQueued).get();
        }
        catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    @SubscribeEvent
    public void onWorldUnload(WorldEvent.Unload event) {
        if (event.getWorld().provider.getDimension() == 0) {
            Pixelmon.LOGGER.info("Main world being unloaded. Saving all queued saves!");
            this.saveAllQueued();
        }
    }

    @Override
    public void savePlayerStorage(PlayerStorage storage) {
        if (!this.playerStorages.contains(storage)) {
            this.playerStorages.add(storage);
        }
    }

    @Override
    public void saveComputerStorage(PlayerComputerStorage storage) {
        if (storage.hasChanges() && !this.playerComputerStorages.contains(storage)) {
            this.playerComputerStorages.add(storage);
        }
    }

    @Override
    public NBTTagCompound readPlayerData(UUID uuid) {
        return this.storageAdapter.readPlayerData(uuid);
    }

    @Override
    public NBTTagCompound readPlayerData(UUID uuid, File file) {
        return this.storageAdapter.readPlayerData(uuid, file);
    }

    @Override
    public void writePlayerData(UUID uuid, NBTTagCompound storage) {
        this.storageAdapter.writePlayerData(uuid, storage);
    }

    @Override
    public void writePlayerData(UUID uuid, NBTTagCompound storage, File file, String extension) {
        this.storageAdapter.writePlayerData(uuid, storage, file, extension);
    }

    @Override
    public NBTTagCompound readComputerData(UUID uuid) {
        return this.storageAdapter.readComputerData(uuid);
    }

    @Override
    public NBTTagCompound readComputerData(UUID uuid, File file) {
        return this.storageAdapter.readComputerData(uuid, file);
    }

    @Override
    public void writeComputerData(UUID uuid, NBTTagCompound storage) {
        this.storageAdapter.writeComputerData(uuid, storage);
    }

    @Override
    public void writeComputerData(UUID uuid, NBTTagCompound storage, File file, String extension) {
        this.storageAdapter.writeComputerData(uuid, storage, file, extension);
    }

    private void saveAllQueued() {
        NBTTagCompound nbt;
        BaseStorage storage;
        this.info("Starting async player storage saving.");
        long starttime = System.currentTimeMillis();
        int count = 0;
        while (this.playerStorages.peek() != null) {
            storage = this.playerStorages.poll();
            ++count;
            nbt = new NBTTagCompound();
            ((PlayerStorage)storage).writeToNBT(nbt);
            this.storageAdapter.writePlayerData(storage.getPlayerID(), nbt);
        }
        this.info("Player storage: saved " + count + ", took " + (System.currentTimeMillis() - starttime) + "ms");
        starttime = System.currentTimeMillis();
        count = 0;
        this.info("Starting async computer storage saving.");
        while (this.playerComputerStorages.peek() != null) {
            storage = this.playerComputerStorages.poll();
            if (!((PlayerComputerStorage)storage).hasChanges()) continue;
            ++count;
            nbt = new NBTTagCompound();
            ((PlayerComputerStorage)storage).writeToNBT(nbt);
            this.storageAdapter.writeComputerData(storage.getPlayerID(), nbt);
        }
        this.info("Computer storage: saved " + count + ", took " + (System.currentTimeMillis() - starttime) + "ms");
    }

    private void info(String msg) {
        if (this.interval >= 60) {
            Pixelmon.LOGGER.info(msg);
        }
    }
}

