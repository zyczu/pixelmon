/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Maps
 */
package com.pixelmongenerations.core.storage;

import com.google.common.collect.Maps;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.event.TickHandler;
import com.pixelmongenerations.core.storage.AsyncStorageWrapper;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerNotLoadedException;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class PokeballManager {
    public static boolean disableStarterMenu = false;
    private static final Map<UUID, PlayerStorage> playerMap = Maps.newConcurrentMap();
    private int lastSaveTick = 0;

    public Optional<PlayerStorage> getPlayerStorage(EntityPlayerMP player) {
        if (player == null) {
            return Optional.empty();
        }
        PlayerStorage store = playerMap.get(player.getUniqueID());
        if (store == null) {
            try {
                store = this.loadPlayer(player);
                if (store == null) {
                    return Optional.empty();
                }
                playerMap.put(player.getUniqueID(), store);
            }
            catch (PlayerNotLoadedException var4) {
                return Optional.empty();
            }
        }
        return Optional.of(store);
    }

    private PlayerStorage getFromStorage(EntityPlayerMP player) {
        return playerMap.get(player.getUniqueID());
    }

    public Optional<PlayerStorage> getPlayerStorageFromName(MinecraftServer server, String name) throws PlayerNotLoadedException {
        return this.getPlayerStorage(this.getPlayerFromName(server, name));
    }

    public EntityPlayerMP getPlayerFromName(MinecraftServer server, String name) {
        return server.getPlayerList().getPlayerByUsername(name);
    }

    public EntityPlayerMP getPlayerFromUUID(MinecraftServer server, UUID uuid) {
        return server.getPlayerList().getPlayerByUUID(uuid);
    }

    public Optional<PlayerStorage> getPlayerStorageFromUUID(MinecraftServer server, UUID uuid) {
        if (playerMap.containsKey(uuid)) {
            return Optional.of(playerMap.get(uuid));
        }
        EntityPlayerMP player = this.getPlayerFromUUID(server, uuid);
        if (player != null) {
            return this.getPlayerStorage(player);
        }
        NBTTagCompound compound = PixelmonStorage.storageAdapter.readPlayerData(uuid);
        if (compound == null) {
            return Optional.empty();
        }
        PlayerStorage p = new PlayerStorage(server, uuid);
        p.readFromNBT(compound);
        playerMap.put(uuid, p);
        return Optional.of(p);
    }

    public Optional<PlayerStorage> getPlayerStorageFromUUID(UUID uuid) {
        return this.getPlayerStorageFromUUID(FMLCommonHandler.instance().getMinecraftServerInstance(), uuid);
    }

    public void refreshPlayerStorage(EntityPlayerMP player) throws PlayerNotLoadedException {
        PlayerStorage store = playerMap.get(player.getUniqueID());
        if (store != null) {
            if (!player.server.isSinglePlayer()) {
                if (PixelmonStorage.storageAdapter instanceof AsyncStorageWrapper) {
                    playerMap.remove(player.getUniqueID());
                    NBTTagCompound compound = new NBTTagCompound();
                    store.writeToNBT(compound);
                    store = new PlayerStorage(player);
                    store.readFromNBT(compound);
                    playerMap.put(player.getUniqueID(), store);
                    return;
                }
                this.savePlayer(player.server, store);
                playerMap.remove(player.getUniqueID());
            } else {
                playerMap.remove(player.getUniqueID());
            }
        } else if (PixelmonStorage.storageAdapter instanceof AsyncStorageWrapper && (store = ((AsyncStorageWrapper)PixelmonStorage.storageAdapter).getQueuedStorage(player.getUniqueID())) != null) {
            NBTTagCompound compound = new NBTTagCompound();
            store.writeToNBT(compound);
            store = new PlayerStorage(player);
            store.readFromNBT(compound);
            playerMap.put(player.getUniqueID(), store);
            return;
        }
        store = this.loadPlayer(player);
        playerMap.put(player.getUniqueID(), store);
    }

    public PlayerStorage loadPlayer(EntityPlayerMP player) throws PlayerNotLoadedException {
        PlayerStorage old;
        if (player == null) {
            throw new PlayerNotLoadedException();
        }
        PlayerStorage p = new PlayerStorage(player);
        NBTTagCompound compound = null;
        if (PixelmonStorage.storageAdapter instanceof AsyncStorageWrapper && (old = ((AsyncStorageWrapper)PixelmonStorage.storageAdapter).getQueuedStorage(player.getUniqueID())) != null) {
            compound = new NBTTagCompound();
            old.writeToNBT(compound);
        }
        if (compound == null) {
            compound = PixelmonStorage.storageAdapter.readPlayerData(player.getUniqueID());
        }
        if (compound == null) {
            if (!disableStarterMenu && PixelmonConfig.giveStarter) {
                TickHandler.registerStarterList(player);
            }
        } else {
            p.readFromNBT(compound);
        }
        if (p.count() == 0 && !disableStarterMenu && PixelmonConfig.giveStarter) {
            TickHandler.registerStarterList(player);
        }
        return p;
    }

    public void savePlayer(MinecraftServer server, PlayerStorage p) {
        if (!server.isCallingFromMinecraftThread()) {
            server.addScheduledTask(() -> PixelmonStorage.storageAdapter.savePlayerStorage(p));
        } else {
            PixelmonStorage.storageAdapter.savePlayerStorage(p);
        }
    }

    @SubscribeEvent
    public void onWorldSave(WorldEvent.Save event) {
        int currentTick = event.getWorld().getMinecraftServer().getTickCounter();
        if (PixelmonConfig.dataSaveOnWorldSave && this.lastSaveTick != currentTick) {
            this.lastSaveTick = currentTick;
            this.saveAll(event.getWorld());
        }
    }

    public void serverStopped() {
        Iterator<PlayerStorage> i = playerMap.values().iterator();
        while (i.hasNext()) {
            PlayerStorage storage = i.next();
            this.savePlayer(FMLCommonHandler.instance().getMinecraftServerInstance(), storage);
            i.remove();
        }
    }

    public void saveAll(MinecraftServer server) {
        Iterator<PlayerStorage> i = playerMap.values().iterator();
        while (i.hasNext()) {
            PlayerStorage storage = i.next();
            this.savePlayer(server, storage);
            if (!storage.isOffline()) continue;
            i.remove();
        }
    }

    public void printStorage() {
        for (Map.Entry<UUID, PlayerStorage> entry : playerMap.entrySet()) {
            Pixelmon.LOGGER.info("Storage: " + entry.getValue().userName + "  guiopen=" + entry.getValue().guiOpened + "  isOnline=" + !entry.getValue().isOffline());
        }
    }

    @Deprecated
    public void saveAll(World world) {
        this.saveAll(world.getMinecraftServer());
    }

    public PlayerStorage getIsLoaded(EntityPlayerMP entity) {
        PlayerStorage storage;
        Iterator<PlayerStorage> var2 = playerMap.values().iterator();
        do {
            if (var2.hasNext()) continue;
            return null;
        } while ((storage = var2.next()).getPlayerID() != entity.getUniqueID());
        return storage;
    }

    public static Collection<PlayerStorage> getLoadedPlayers() {
        return playerMap.values();
    }

    public void clearStorage() {
        playerMap.clear();
    }
}

