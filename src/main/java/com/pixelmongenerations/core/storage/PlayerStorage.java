/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.util.concurrent.ListenableFuture
 */
package com.pixelmongenerations.core.storage;

import com.google.common.util.concurrent.ListenableFuture;
import com.pixelmongenerations.api.economy.AccountHolder;
import com.pixelmongenerations.api.economy.Transaction;
import com.pixelmongenerations.api.economy.TransactionType;
import com.pixelmongenerations.api.events.EconomyEvent;
import com.pixelmongenerations.api.events.PokedexEvent;
import com.pixelmongenerations.api.events.PokerusEvent;
import com.pixelmongenerations.api.events.player.PlayerEggHatchEvent;
import com.pixelmongenerations.api.events.player.PlayerEggStepsEvent;
import com.pixelmongenerations.api.events.player.PlayerMovementEvent;
import com.pixelmongenerations.api.events.player.PlayerRetrievePokemonEvent;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.cosmetic.CosmeticData;
import com.pixelmongenerations.common.currydex.CurryDex;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.NBTLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pokeballs.EntityOccupiedPokeball;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.pokedex.EnumPokedexRegisterStatus;
import com.pixelmongenerations.common.pokedex.Pokedex;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.economy.AccountHolderImpl;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumCatchCombo;
import com.pixelmongenerations.core.enums.EnumEggType;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.event.EntityPlayerExtension;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonUpdateData;
import com.pixelmongenerations.core.network.packetHandlers.chooseMoveset.ChooseMoveset;
import com.pixelmongenerations.core.network.packetHandlers.chooseMoveset.ChoosingMovesetData;
import com.pixelmongenerations.core.network.packetHandlers.clientStorage.Add;
import com.pixelmongenerations.core.network.packetHandlers.clientStorage.Remove;
import com.pixelmongenerations.core.network.packetHandlers.clientStorage.Update;
import com.pixelmongenerations.core.network.packetHandlers.clientStorage.UpdateClientPlayerData;
import com.pixelmongenerations.core.storage.BaseStorage;
import com.pixelmongenerations.core.storage.EnumPokeballManagerMode;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStats;
import com.pixelmongenerations.core.storage.deepstorage.DeepStorageManager;
import com.pixelmongenerations.core.storage.playerData.DynamaxData;
import com.pixelmongenerations.core.storage.playerData.ExternalMoves;
import com.pixelmongenerations.core.storage.playerData.MegaData;
import com.pixelmongenerations.core.storage.playerData.MountData;
import com.pixelmongenerations.core.storage.playerData.PlayerData;
import com.pixelmongenerations.core.storage.playerData.ShinyData;
import com.pixelmongenerations.core.storage.playerData.TeleportPosition;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.PixelmonPlayerUtils;
import com.pixelmongenerations.core.util.helper.NbtHelper;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import org.jetbrains.annotations.Nullable;

public class PlayerStorage
extends BaseStorage {
    public NBTTagCompound[] partyPokemon = new NBTTagCompound[6];
    public ExternalMoves moves;
    public TeleportPosition teleportPos = new TeleportPosition();
    private AtomicInteger idCounter;
    private static final int carryLimit = 6;
    public NPCTrainer trainer;
    public String userName;
    public EnumPokeballManagerMode mode;
    public boolean guiOpened = false;
    public boolean battleEnabled = true;
    public boolean starterPicked = false;
    private int ticksTillEncounter;
    public EntityOccupiedPokeball thrownPokeball = null;
    public int id = -1;
    private boolean[] isInWorld = new boolean[6];
    public int lastXPos = 0;
    public int lastYPos = 0;
    public int lastZPos = 0;
    public int eggTick = 0;
    public Pokedex pokedex;
    public PlayerStats stats;
    private AccountHolder accountHolder;
    public CurryDex curryDex;
    public MegaData megaData = new MegaData(this);
    public ShinyData shinyData = new ShinyData(this);
    public PlayerData playerData = new PlayerData();
    public CosmeticData cosmeticData;
    public DynamaxData dynamaxData = new DynamaxData(this);
    public MountData mountData = new MountData();
    public String cape = "";
    private int catchCombo = 0;
    private EnumSpecies catchComboSpecies;

    public PlayerStorage(EntityPlayerMP player) {
        super(player.getUniqueID());
        this.idCounter = new AtomicInteger(0);
        this.moves = new ExternalMoves(this);
        this.mode = EnumPokeballManagerMode.Player;
        this.userName = player.getGameProfile().getName();
        this.pokedex = new Pokedex(player);
        this.curryDex = new CurryDex();
        this.stats = new PlayerStats();
        this.ticksTillEncounter = player.getRNG().nextInt(900) + 100;
        try {
            this.accountHolder = (AccountHolder)Pixelmon.defaultAccountHolder.newInstance();
            this.accountHolder.setupAccount(player.getUniqueID());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        this.cosmeticData = Pixelmon.cosmeticDataFactory.apply(this.getPlayerID());
    }

    public PlayerStorage(MinecraftServer server, UUID uuid) {
        super(uuid);
        this.moves = new ExternalMoves(this);
        this.mode = EnumPokeballManagerMode.Offline;
        this.pokedex = new Pokedex();
        this.curryDex = new CurryDex();
        this.stats = new PlayerStats();
        this.idCounter = new AtomicInteger(0);
        try {
            this.accountHolder = (AccountHolder)Pixelmon.defaultAccountHolder.newInstance();
            this.accountHolder.setupAccount(uuid);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        this.cosmeticData = Pixelmon.cosmeticDataFactory.apply(uuid);
    }

    public PlayerStorage(NPCTrainer trainer) {
        super(null);
        this.mode = EnumPokeballManagerMode.Trainer;
        this.trainer = trainer;
        this.idCounter = new AtomicInteger(0);
        this.accountHolder = new AccountHolderImpl();
    }

    public void handleCapture(EnumSpecies species, boolean failed) {
        if (PixelmonConfig.enableCatchCombos) {
            if (failed) {
                this.endCatchCombo();
                this.catchCombo = 0;
                this.catchComboSpecies = null;
            } else if (species == this.catchComboSpecies) {
                EntityPlayerMP player;
                ++this.catchCombo;
                if (this.catchCombo >= 2 && (player = this.getPlayer()) != null && this.catchCombo >= 2) {
                    ChatHandler.sendFormattedChat(player, TextFormatting.GOLD, "pixelmon.catchcombo.update", this.catchComboSpecies.name, this.catchCombo);
                }
            } else {
                if (this.catchComboSpecies != null) {
                    this.endCatchCombo();
                }
                this.catchCombo = 1;
                this.catchComboSpecies = species;
            }
        }
    }

    public void endCatchCombo() {
        EntityPlayerMP player = this.getPlayer();
        if (player != null && this.catchCombo >= 2) {
            TextComponentTranslation text = new TextComponentTranslation("pixelmon.catchcombo.end", this.catchComboSpecies.name, this.catchCombo);
            text.getStyle().setColor(TextFormatting.GOLD);
            player.sendMessage(text);
        }
        this.catchComboSpecies = null;
        this.catchCombo = 0;
    }

    public EnumCatchCombo getCatchCombo() {
        return EnumCatchCombo.getCombo(this.catchCombo);
    }

    public int getCatchStreak() {
        return this.catchCombo;
    }

    public Optional<EnumSpecies> getCatchComboSpecies() {
        return this.catchComboSpecies != null ? Optional.of(this.catchComboSpecies) : Optional.empty();
    }

    public AccountHolder getAccountHolder() {
        return this.accountHolder;
    }

    public void setCape(String cape) {
        this.cape = cape;
        EntityPlayerExtension.updatePlayerCape(this.getPlayer(), cape);
    }

    public void removeCape() {
        this.cape = "";
        EntityPlayerExtension.updatePlayerCape(this.getPlayer(), this.cape);
    }

    public int getCurrency() {
        return this.accountHolder.getBalance();
    }

    public void addCurrency(int amount) {
        int oldBalance = this.accountHolder.getBalance();
        EconomyEvent.PreTransactionEvent preEvent = new EconomyEvent.PreTransactionEvent(this.getPlayer(), TransactionType.DEPOSIT, oldBalance, amount);
        if (!MinecraftForge.EVENT_BUS.post(preEvent)) {
            this.accountHolder.set(preEvent.getBalance());
            Transaction transaction = this.accountHolder.deposit(preEvent.getAmount());
            if (!transaction.transactionSuccess()) {
                return;
            }
            MinecraftForge.EVENT_BUS.post(new EconomyEvent.PostTransactionEvent(this.getPlayer(), TransactionType.DEPOSIT, preEvent.getBalance(), transaction.balance));
            if (this.getPlayer() != null) {
                Pixelmon.NETWORK.sendTo(new UpdateClientPlayerData(Math.min(transaction.balance, 999999)), this.getPlayer());
            }
        }
    }

    public void removeCurrency(int amount) {
        int oldBalance = this.accountHolder.getBalance();
        EconomyEvent.PreTransactionEvent preEvent = new EconomyEvent.PreTransactionEvent(this.getPlayer(), TransactionType.WITHDRAW, oldBalance, amount);
        if (!MinecraftForge.EVENT_BUS.post(preEvent)) {
            this.accountHolder.set(preEvent.getBalance());
            Transaction transaction = this.accountHolder.withdraw(preEvent.getAmount());
            if (!transaction.transactionSuccess()) {
                return;
            }
            MinecraftForge.EVENT_BUS.post(new EconomyEvent.PostTransactionEvent(this.getPlayer(), TransactionType.WITHDRAW, preEvent.getBalance(), transaction.balance));
            if (this.getPlayer() != null) {
                Pixelmon.NETWORK.sendTo(new UpdateClientPlayerData(Math.min(transaction.balance, 999999)), this.getPlayer());
            }
        }
    }

    public void setCurrency(int amount) {
        Transaction transaction = this.accountHolder.set(amount);
        if (!transaction.transactionSuccess()) {
            return;
        }
        if (this.getPlayer() != null) {
            Pixelmon.NETWORK.sendTo(new UpdateClientPlayerData(transaction.balance > 999999 ? 999999 : transaction.balance), this.getPlayer());
        }
    }

    public boolean hasSpace() {
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt != null) continue;
            return true;
        }
        return false;
    }

    public int getNextOpen() {
        for (int i = 0; i < this.partyPokemon.length; ++i) {
            if (this.partyPokemon[i] != null) continue;
            return i;
        }
        return 0;
    }

    public void setPokemon(NBTTagCompound[] pokemon) {
        this.partyPokemon = pokemon;
    }

    public void addToParty(EntityPixelmon p) {
        this.addToParty(p, this.getNextOpen(), true);
    }

    public void addToParty(EntityPixelmon p, int slot) {
        this.addToParty(p, slot, false);
    }

    private void addToParty(EntityPixelmon p, int slot, boolean checkFull) {
        this.addToStorage(p, slot, checkFull, false);
    }

    public void addToPC(EntityPixelmon p) {
        this.addToStorage(p, 0, false, true);
    }

    private void addToStorage(EntityPixelmon p, int slot, boolean checkFull, boolean forcePC) {
        if (this.mode == EnumPokeballManagerMode.Player && this.pokedex != null && !p.isEgg && !MinecraftForge.EVENT_BUS.post(new PokedexEvent((EntityPlayerMP)this.pokedex.owner, this.pokedex, p.getSpecies(), EnumPokedexRegisterStatus.caught))) {
            this.pokedex.set(p.getSpecies(), EnumPokedexRegisterStatus.caught);
            this.pokedex.sendToPlayer((EntityPlayerMP)this.pokedex.owner);
        }
        if (p.getMoveset().isEmpty()) {
            p.loadMoveset();
        }
        p.setBoss(EnumBossMode.NotBoss);
        if (p.caughtBall == null) {
            p.caughtBall = EnumPokeball.PokeBall;
        }
        if (this.mode == EnumPokeballManagerMode.Player) {
            p.setOwnerId(this.getPlayerID());
        } else if (this.mode == EnumPokeballManagerMode.Trainer) {
            p.setTrainer(this.trainer);
        }
        if (this.getPlayer() == null || p.getPokemonId()[0] != this.getPlayerMost()) {
            int[] id = this.getNewPokemonID();
            p.setPokemonId(id);
        }
        if (!(forcePC || checkFull && !this.hasSpace())) {
            NBTTagCompound n = new NBTTagCompound();
            p.writeToNBT(n);
            n.setString("id", "Pixelmon");
            n.setBoolean("IsInBall", true);
            n.setBoolean("IsShiny", p.isShiny());
            if (this.mode == EnumPokeballManagerMode.Player && "".equals(p.originalTrainer)) {
                n.setString("originalTrainer", this.getPlayer().getDisplayName().getUnformattedText());
            }
            if (this.mode == EnumPokeballManagerMode.Player && "".equals(p.originalTrainerUUID)) {
                n.setString("originalTrainerUUID", this.getPlayer().getUniqueID().toString());
            }
            if (this.mode == EnumPokeballManagerMode.Trainer && "".equals(p.originalTrainer)) {
                n.setString("originalTrainer", this.trainer.getName());
            }
            if (this.mode == EnumPokeballManagerMode.Trainer && "".equals(p.originalTrainerUUID)) {
                n.setString("originalTrainerUUID", this.trainer.getUniqueID().toString());
            }
            if (p.getHeldItemMainhand() != null) {
                n.setTag("HeldItemStack", p.getHeldItemMainhand().writeToNBT(new NBTTagCompound()));
            }
            n.setInteger("PixelmonOrder", slot);
            this.partyPokemon[slot] = n;
            if (p.getHealth() > 0.0f) {
                n.setBoolean("IsFainted", false);
            }
            if (p.type.get(0) != p.baseStats.type1) {
                n.setShort("primaryType", (short)((EnumType)((Object)p.type.get(0))).getIndex());
            }
            EnumType type2 = null;
            if (p.type.size() > 1) {
                type2 = (EnumType)((Object)p.type.get(1));
            }
            if (type2 != null && type2 != p.baseStats.type2) {
                n.setShort("secondaryType", (short)type2.getIndex());
            } else {
                n.setShort("secondaryType", (short)-1);
            }
            if (this.mode == EnumPokeballManagerMode.Player) {
                Pixelmon.NETWORK.sendTo(new Add(new PixelmonData(n)), this.getPlayer());
                EntityPlayerExtension.updatePlayerPokeballs(this.getPlayer(), this.getPokeballList());
                EntityPlayerExtension.updatePlayerEggs(this.getPlayer(), this.getEggList());
            }
        } else {
            if (!forcePC) {
                Object[] arrobject = new Object[]{p.isEgg ? I18n.translateToLocal("pixelmon.egg.name") : p.getNickname()};
                ChatHandler.sendChat(p.getOwner(), "pixelmon.storage.partyfull", arrobject);
            }
            PixelmonStorage.computerManager.getPlayerStorage(this.getPlayer()).addToComputer(p);
        }
    }

    public int[] getNewPokemonID() {
        int[] arrn;
        if (this.mode == EnumPokeballManagerMode.Player) {
            return new int[]{this.getPlayerMost(), this.getNextID()};
        }
        if (this.mode == EnumPokeballManagerMode.Trainer) {
            int[] arrn2 = new int[2];
            arrn2[0] = (int)this.trainer.getUniqueID().getLeastSignificantBits();
            arrn = arrn2;
            arrn2[1] = this.getNextID();
        } else {
            int[] arrn3 = new int[2];
            arrn3[0] = -1;
            arrn = arrn3;
            arrn3[1] = -1;
        }
        return arrn;
    }

    public int getPlayerMost() {
        return (int)this.getPlayerID().getMostSignificantBits();
    }

    public void retrieve(EntityPixelmon currentPixelmon) {
        for (NBTTagCompound n : this.partyPokemon) {
            if (n == null || !PixelmonMethods.isIDSame(n, currentPixelmon)) continue;
            if (this.getPlayer() != null) {
                PlayerRetrievePokemonEvent event = new PlayerRetrievePokemonEvent(this.getPlayer(), currentPixelmon);
                MinecraftForge.EVENT_BUS.post(event);
                currentPixelmon = event.getPokemon();
            }
            currentPixelmon.writeToNBT(n);
            n.setBoolean("IsInBall", true);
            this.setInWorld(currentPixelmon, false);
        }
    }

    public boolean contains(int[] id) {
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || nbt.getInteger("pixelmonID1") != id[0] || nbt.getInteger("pixelmonID2") != id[1]) continue;
            return true;
        }
        return false;
    }

    public EntityPixelmon sendOut(int[] id, World world) {
        for (NBTTagCompound n : this.partyPokemon) {
            if (n == null || !PixelmonMethods.isIDSame(n, id)) continue;
            return this.sendOut(n, world);
        }
        return null;
    }

    private EntityPixelmon sendOut(NBTTagCompound n, World world) {
        n.setFloat("FallDistance", 0.0f);
        n.setBoolean("IsInBall", false);
        EntityPixelmon e = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(n, world);
        if (this.mode == EnumPokeballManagerMode.Player && !this.isOffline()) {
            EntityPlayerMP playerOwner = this.getPlayer();
            e.setOwnerId(playerOwner.getUniqueID());
            e.playerOwned = true;
            e.dimension = playerOwner.dimension;
        } else if (this.mode == EnumPokeballManagerMode.Trainer) {
            e.setTrainer(this.trainer);
            e.dimension = this.trainer.dimension;
        }
        e.motionZ = 0.0;
        e.motionY = 0.0;
        e.motionX = 0.0;
        e.isDead = false;
        e.setNumBreedingLevels(-1);
        return e;
    }

    public NBTTagCompound getNBT(int[] id) {
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || !PixelmonMethods.isIDSame(nbt, id)) continue;
            return nbt;
        }
        return null;
    }

    public NBTTagCompound[] getList() {
        return this.partyPokemon;
    }

    public void replace(EntityPixelmon entityPixelmon, EntityPixelmon entityCapturedPixelmon) {
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || !PixelmonMethods.isIDSame(nbt, entityPixelmon)) continue;
            entityCapturedPixelmon.setPokemonId(entityPixelmon.getPokemonId());
            entityCapturedPixelmon.writeToNBT(nbt);
            nbt.setString("id", entityCapturedPixelmon.getPokemonName());
            if (this.mode != EnumPokeballManagerMode.Player || this.isOffline()) continue;
            Pixelmon.NETWORK.sendTo(new Add(new PixelmonData(nbt)), this.getPlayer());
            EntityPlayerExtension.updatePlayerPokeballs(this.getPlayer(), this.getPokeballList());
            EntityPlayerExtension.updatePlayerEggs(this.getPlayer(), this.getEggList());
        }
    }

    public void changePokemon(int pos, NBTTagCompound n) {
        if (pos >= this.partyPokemon.length) {
            pos = this.partyPokemon.length - 1;
        }
        if (n != null) {
            n.setInteger("PixelmonOrder", pos);
        }
        this.partyPokemon[pos] = n;
        if (this.mode == EnumPokeballManagerMode.Player && !this.isOffline()) {
            EntityPlayerExtension.updatePlayerPokeballs(this.getPlayer(), this.getPokeballList());
            EntityPlayerExtension.updatePlayerEggs(this.getPlayer(), this.getEggList());
        }
    }

    public void changePokemonAndAssignID(int pos, NBTTagCompound n) {
        int[] id = this.getNewPokemonID();
        if (this.partyPokemon[pos] != null) {
            int[] idpixel;
            Optional<EntityPixelmon> p;
            if (n == null && (p = this.getAlreadyExists(idpixel = PixelmonMethods.getID(this.partyPokemon[pos]), this.getPlayer().world)).isPresent()) {
                EntityPixelmon pixelmon = p.get();
                this.retrieve(pixelmon);
                this.updateNBT(pixelmon);
                pixelmon.isInBall = true;
                pixelmon.unloadEntity();
            }
            if (this.mode == EnumPokeballManagerMode.Player && !this.isOffline()) {
                Pixelmon.NETWORK.sendTo(new Remove(PixelmonMethods.getID(this.partyPokemon[pos])), this.getPlayer());
                EntityPlayerExtension.updatePlayerPokeballs(this.getPlayer(), this.getPokeballList());
                EntityPlayerExtension.updatePlayerEggs(this.getPlayer(), this.getEggList());
            }
        }
        if (n != null) {
            n.setInteger("pixelmonID1", id[0]);
            n.setInteger("pixelmonID2", id[1]);
        }
        this.changePokemon(pos, n);
        if (this.mode == EnumPokeballManagerMode.Player && n != null && !this.isOffline()) {
            Pixelmon.NETWORK.sendTo(new Add(new PixelmonData(n)), this.getPlayer());
            EntityPlayerExtension.updatePlayerPokeballs(this.getPlayer(), this.getPokeballList());
        }
    }

    public void addToFirstEmptySpace(NBTTagCompound n) {
        for (int i = 0; i < this.partyPokemon.length; ++i) {
            if (this.partyPokemon[i] != null) continue;
            if (n != null) {
                n.setInteger("PixelmonOrder", i);
                if (this.mode == EnumPokeballManagerMode.Player && !this.isOffline()) {
                    Pixelmon.NETWORK.sendTo(new Add(new PixelmonData(n)), this.getPlayer());
                }
            }
            this.partyPokemon[i] = n;
            if (this.mode == EnumPokeballManagerMode.Player && !this.isOffline()) {
                EntityPlayerExtension.updatePlayerPokeballs(this.getPlayer(), this.getPokeballList());
                EntityPlayerExtension.updatePlayerEggs(this.getPlayer(), this.getEggList());
            }
            return;
        }
    }

    public int[] getPokeballList() {
        int[] balls = new int[6];
        for (int i = 0; i < this.partyPokemon.length; ++i) {
            if (this.partyPokemon[i] != null) {
                if (this.partyPokemon[i].getBoolean("isEgg")) {
                    balls[i] = -1;
                    continue;
                }
                balls[i] = this.partyPokemon[i].getInteger("CaughtBall");
                continue;
            }
            balls[i] = -1;
        }
        return balls;
    }

    public int[] getEggList() {
        int[] eggs = new int[6];
        for (int i = 0; i < this.partyPokemon.length; ++i) {
            if (this.partyPokemon[i] != null) {
                if (this.partyPokemon[i].getBoolean("isEgg")) {
                    eggs[i] = EnumEggType.getTypeFromPokemon(this.partyPokemon[i].getString("Name")).ordinal();
                    continue;
                }
                eggs[i] = -1;
                continue;
            }
            eggs[i] = -1;
        }
        return eggs;
    }

    public int count() {
        int count = 0;
        for (NBTTagCompound aPartyPokemon : this.partyPokemon) {
            if (aPartyPokemon == null) continue;
            ++count;
        }
        return count;
    }

    public int countTeam() {
        int c = 0;
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || nbt.getBoolean("isEgg")) continue;
            ++c;
        }
        return c;
    }

    public List<NBTTagCompound> getTeam() {
        ArrayList<NBTTagCompound> team = new ArrayList<NBTTagCompound>();
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || nbt.getBoolean("isEgg")) continue;
            team.add(nbt);
        }
        return team;
    }

    public List<NBTTagCompound> getTeamIncludeEgg() {
        ArrayList<NBTTagCompound> team = new ArrayList<NBTTagCompound>();
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null) continue;
            team.add(nbt);
        }
        return team;
    }

    public int countAblePokemon() {
        int c = 0;
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || nbt.getBoolean("IsFainted") || nbt.getBoolean("isEgg") || nbt.getFloat("Health") <= 0.0f) continue;
            ++c;
        }
        return c;
    }

    public boolean isIn(EntityPixelmon entityPixelmon) {
        return this.contains(entityPixelmon.getPokemonId());
    }

    public int getHighestLevel() {
        int lvl = -1;
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null) continue;
            lvl = Math.max(lvl, nbt.getInteger("Level"));
        }
        return lvl;
    }

    public boolean hasSentOut(int[] pixelmonID) {
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || !PixelmonMethods.isIDSame(nbt, pixelmonID) || nbt.getBoolean("IsInBall")) continue;
            return true;
        }
        return false;
    }

    public boolean isFainted(int[] id) {
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || !PixelmonMethods.isIDSame(nbt, id)) continue;
            return PlayerStorage.isFainted(nbt);
        }
        return false;
    }

    public static boolean isFainted(NBTTagCompound nbt) {
        if (nbt == null) {
            return false;
        }
        return nbt.getBoolean("IsFainted") || nbt.getFloat("Health") <= 0.0f;
    }

    public static boolean canBattle(NBTTagCompound nbt) {
        return nbt != null && !PlayerStorage.isFainted(nbt) && !nbt.getBoolean("isEgg");
    }

    public boolean hasPrimaryStatus(NBTTagCompound nbt) {
        return nbt.hasKey("Status");
    }

    public boolean isEgg(int[] id) {
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || !PixelmonMethods.isIDSame(nbt, id) || !nbt.getBoolean("isEgg")) continue;
            return true;
        }
        return false;
    }

    public void updateNBT(EntityPixelmon pixelmon) {
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || !PixelmonMethods.isIDSame(nbt, pixelmon)) continue;
            pixelmon.writeToNBT(nbt);
            nbt.setString("id", pixelmon.getPokemonName());
            if (this.moves != null) {
                this.moves.refresh(nbt);
            }
            if (pixelmon.getHealth() <= 0.0f) {
                nbt.setBoolean("IsFainted", true);
            }
            if (this.mode != EnumPokeballManagerMode.Player || this.isOffline()) continue;
            EntityPlayerExtension.updatePlayerPokeballs(this.getPlayer(), this.getPokeballList());
            EntityPlayerExtension.updatePlayerEggs(this.getPlayer(), this.getEggList());
        }
    }

    public void updateClient(NBTTagCompound nbt, EnumUpdateType ... types) {
        this.updateClient(nbt, false, types);
    }

    public void updateClient(NBTTagCompound nbt, boolean inBattle, EnumUpdateType ... types) {
        if (this.mode == EnumPokeballManagerMode.Player && !this.isOffline()) {
            Pixelmon.NETWORK.sendTo(new Update(new PixelmonUpdateData(nbt, types)), this.getPlayer());
        }
    }

    public void update(EntityPixelmon pixelmon, EnumUpdateType type) {
        this.updateAndSendToClient(pixelmon, type);
    }

    public void updateAndSendToClient(EntityPixelmon pixelmon, EnumUpdateType ... types) {
        this.updateNBT(pixelmon);
        if (this.mode == EnumPokeballManagerMode.Player && !this.isOffline()) {
            Pixelmon.NETWORK.sendTo(new Update(new PixelmonUpdateData(pixelmon, types)), this.getPlayer());
        }
    }

    public void updateClient(PokemonLink pixelmon, boolean inBattle, EnumUpdateType ... types) {
        if (this.mode == EnumPokeballManagerMode.Player && !this.isOffline()) {
            Pixelmon.NETWORK.sendTo(new Update(new PixelmonUpdateData(pixelmon, types, inBattle)), this.getPlayer());
        }
    }

    public void sendUpdatedList() {
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || this.mode != EnumPokeballManagerMode.Player || this.isOffline()) continue;
            Pixelmon.NETWORK.sendTo(new Add(new PixelmonData(nbt)), this.getPlayer());
        }
    }

    public int[] getIDFromPosition(int pos) {
        for (NBTTagCompound n : this.partyPokemon) {
            if (n == null || n.getInteger("PixelmonOrder") != pos) continue;
            return PixelmonMethods.getID(n);
        }
        return new int[]{-1, -1};
    }

    public boolean entityAlreadyExists(int[] id, World world) {
        for (int i = 0; i < world.loadedEntityList.size(); ++i) {
            Entity entity = world.loadedEntityList.get(i);
            if (!(entity instanceof EntityPixelmon) || !PixelmonMethods.isIDSame((EntityPixelmon)entity, id)) continue;
            return true;
        }
        return false;
    }

    public boolean entityAlreadyExists(EntityPixelmon pixelmon) {
        for (int i = 0; i < pixelmon.world.loadedEntityList.size(); ++i) {
            Entity entity = pixelmon.world.loadedEntityList.get(i);
            if (!(entity instanceof EntityPixelmon) || !PixelmonMethods.isIDSame((EntityPixelmon)entity, pixelmon)) continue;
            return true;
        }
        return false;
    }

    public Optional<EntityPixelmon> getAlreadyExists(int[] id, World world) {
        Callable<Optional> callable = () -> {
            if (id != null && id[0] != -1) {
                for (int i = 0; i < world.loadedEntityList.size(); ++i) {
                    EntityPixelmon entityPixelmon;
                    Entity entity = world.loadedEntityList.get(i);
                    if (!(entity instanceof EntityPixelmon) || !PixelmonMethods.isIDSame((entityPixelmon = (EntityPixelmon)entity).getPokemonId(), id)) continue;
                    return Optional.of(entityPixelmon);
                }
                return Optional.empty();
            }
            return Optional.empty();
        };
        ListenableFuture<Optional> listenableFuture = world.getMinecraftServer().callFromMainThread(callable);
        try {
            return (Optional)listenableFuture.get();
        }
        catch (Exception var6) {
            var6.printStackTrace();
            Pixelmon.LOGGER.info("Something went very bad.");
            return Optional.empty();
        }
    }

    public EntityPixelmon getPokemon(int[] id, World world) {
        Optional<EntityPixelmon> pixelmonOptional = this.getAlreadyExists(id, world);
        return pixelmonOptional.orElseGet(() -> this.sendOut(id, world));
    }

    public void writeToNBT(NBTTagCompound nbt) {
        nbt.setString("cape", this.cape);
        this.playerData.writeToNBT(nbt);
        this.teleportPos.writeToNBT(nbt);
        nbt.setInteger("pixelDollars", this.accountHolder.getBalance());
        nbt.setInteger("idcounter", this.idCounter.get());
        nbt.setBoolean("starterPicked", this.starterPicked);
        for (int i = 0; i < this.partyPokemon.length; ++i) {
            NBTTagCompound e = this.partyPokemon[i];
            if (e == null) continue;
            if (this.trainer != null || this.getPlayer() != null) {
                Optional<EntityPixelmon> pixelmonOpt;
                int[] id = new int[]{e.getInteger("pixelmonID1"), e.getInteger("pixelmonID2")};
                if (this.isInWorld(id) && (pixelmonOpt = this.getAlreadyExists(id, this.getPlayer() != null ? this.getPlayer().world : this.trainer.world)).isPresent()) {
                    EntityPixelmon pixelmon = pixelmonOpt.get();
                    this.updateNBT(pixelmon);
                }
            }
            e.removeTag("HeldItemName");
            e.setInteger("PixelmonOrder", i);
            nbt.setTag("party" + i, e);
        }
        if (this.pokedex != null) {
            this.pokedex.writeToNBT(nbt);
            this.shinyData.writeToNBT(nbt);
            this.cosmeticData.writeToNBT(nbt);
            this.curryDex.writeToNBT(nbt);
            this.dynamaxData.writeToNBT(nbt);
        }
        if (this.stats != null) {
            this.stats.writeToNBT(nbt);
        }
        this.megaData.writeToNBT(nbt);
        nbt.setInteger("TicksTillNextEncounter", this.ticksTillEncounter);
        if (this.catchComboSpecies != null) {
            nbt.setInteger("CatchComboDex", this.catchComboSpecies.getNationalPokedexInteger());
            nbt.setInteger("CatchComboValue", this.catchCombo);
            nbt.setLong("CatchComboLeaveTime", Instant.now().getEpochSecond());
        }
    }

    public void readFromNBT(NBTTagCompound nbt) {
        this.setCape(nbt.getString("cape"));
        this.playerData.readFromNBT(nbt);
        this.teleportPos.readFromNBT(nbt);
        if (this.accountHolder instanceof AccountHolderImpl) {
            this.accountHolder.set(nbt.getInteger("pixelDollars"));
        }
        if (nbt.hasKey("idcounter")) {
            this.idCounter.set(nbt.getInteger("idcounter"));
        }
        if (nbt.hasKey("starterPicked")) {
            this.starterPicked = nbt.getBoolean("starterPicked");
        }
        ArrayList<NBTTagCompound> unaddedPokemon = new ArrayList<NBTTagCompound>();
        for (int i = 0; i < 6; ++i) {
            NBTBase nbtbase;
            if (!nbt.hasKey("party" + i) || !((nbtbase = nbt.getTag("party" + i)) instanceof NBTTagCompound)) continue;
            NBTTagCompound pokemonData = (NBTTagCompound)nbtbase;
            if (!pokemonData.hasKey("pixelmonID1")) {
                int[] id = this.getNewPokemonID();
                pokemonData.setInteger("pixelmonID1", id[0]);
                pokemonData.setInteger("pixelmonID2", id[1]);
                pokemonData.removeTag("pixelmonID");
            }
            if (pokemonData.hasKey("HeldItemName")) {
                pokemonData.setTag("HeldItemStack", new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(pokemonData.getString("HeldItemName"))), 1).writeToNBT(new NBTTagCompound()));
            }
            if (!EnumSpecies.hasPokemonAnyCase(pokemonData.getString("Name"))) {
                unaddedPokemon.add(pokemonData);
                continue;
            }
            this.partyPokemon[pokemonData.getInteger((String)"PixelmonOrder")] = NbtHelper.fixPokemonData(pokemonData);
            if (this.mode != EnumPokeballManagerMode.Player || this.isOffline()) continue;
            Pixelmon.NETWORK.sendTo(new Add(new PixelmonData(pokemonData)), this.getPlayer());
            EntityPlayerExtension.updatePlayerPokeballs(this.getPlayer(), this.getPokeballList());
            EntityPlayerExtension.updatePlayerEggs(this.getPlayer(), this.getEggList());
        }
        if (!unaddedPokemon.isEmpty()) {
            DeepStorageManager.bury(this.getPlayerID(), unaddedPokemon, true);
        }
        if (this.pokedex != null) {
            this.pokedex.readFromNBT(nbt);
            this.shinyData.readFromNBT(nbt);
            this.cosmeticData.readFromNBT(nbt);
            this.curryDex = CurryDex.fromNbt(nbt);
            this.dynamaxData.readFromNBT(nbt);
        }
        if (this.stats != null) {
            this.stats.readFromNBT(nbt);
        }
        this.megaData.readFromNBT(nbt);
        if (this.getPlayer() != null) {
            Pixelmon.NETWORK.sendTo(new UpdateClientPlayerData(this.accountHolder.getBalance()), this.getPlayer());
        }
        if (nbt.hasKey("CatchComboLeaveTime") && nbt.hasKey("CatchComboDex") && nbt.hasKey("CatchComboValue")) {
            Instant time = Instant.ofEpochSecond(nbt.getLong("CatchComboLeaveTime"));
            EnumSpecies species = EnumSpecies.getFromDex(nbt.getInteger("CatchComboDex")).get();
            int value = nbt.getInteger("CatchComboValue");
            if (Instant.now().isBefore(time.plusSeconds(PixelmonConfig.catchComboRestoreSeconds))) {
                this.catchComboSpecies = species;
                this.catchCombo = value;
            }
        }
    }

    private int getNextID() {
        return this.idCounter.getAndIncrement();
    }

    public EntityPixelmon getFirstAblePokemon(World world) {
        for (int i = 0; i < 6; ++i) {
            int[] id = this.getIDFromPosition(i);
            if (id[0] == -1 || this.isFainted(id) || this.isEgg(id) || this.trainer != null && this.isInWorld(id)) continue;
            return this.sendOut(id, world);
        }
        return null;
    }

    public EntityPixelmon getSecondAblePokemon(World world) {
        boolean foundOne = false;
        for (int i = 0; i < 6; ++i) {
            int[] id = this.getIDFromPosition(i);
            if (id[0] == -1 || this.isFainted(id) || this.isEgg(id) || this.trainer != null && this.isInWorld(id)) continue;
            if (foundOne) {
                return this.sendOut(id, world);
            }
            foundOne = true;
        }
        return null;
    }

    public EntityPixelmon getFirstAblePokemonParty(World world) {
        for (int i = 0; i < 6; ++i) {
            int[] id = this.getIDFromPosition(i);
            if (id[0] == -1 || this.isFainted(id) || this.isEgg(id) || this.isInWorld(id)) continue;
            return this.sendOut(id, world);
        }
        return null;
    }

    public int[] getFirstAblePokemonID(World world) {
        for (int i = 0; i < 6; ++i) {
            int[] id = this.getIDFromPosition(i);
            if (id[0] == -1 || this.isFainted(id) || this.isEgg(id)) continue;
            return id;
        }
        return new int[]{-1, -1};
    }

    public ArrayList<int[]> getAllAblePokemonIDs() {
        ArrayList<int[]> ids = new ArrayList<int[]>(6);
        for (int i = 0; i < 6; ++i) {
            int[] id = this.getIDFromPosition(i);
            if (id[0] == -1 || this.isFainted(id) || this.isEgg(id)) continue;
            ids.add(id);
        }
        return ids;
    }

    public EntityPixelmon[] getAmountAblePokemon(World world, int amount) {
        EntityPixelmon[] sentOut = new EntityPixelmon[amount];
        int arrCounter = 0;
        for (int i = 0; i < 6; ++i) {
            int[] id = this.getIDFromPosition(i);
            if (id[0] == -1 || this.isFainted(id) || this.isEgg(id)) continue;
            sentOut[arrCounter] = this.sendOut(id, world);
            if (++arrCounter >= amount) break;
        }
        return sentOut;
    }

    public void healAllPokemon(World world) {
        for (NBTTagCompound nbt : this.partyPokemon) {
            Optional<EntityPixelmon> pix;
            if (nbt == null) continue;
            this.heal(nbt);
            int[] id = PixelmonMethods.getID(nbt);
            if (!this.isInWorld(id) || !(pix = this.getAlreadyExists(id, world)).isPresent()) continue;
            EntityPixelmon pokemon = pix.get();
            pokemon.setHealth(pokemon.stats.HP);
            pokemon.getMoveset().healAllPP();
            pokemon.clearStatus();
        }
    }

    public EntityPixelmon sendOutFromPosition(int pos, World world) {
        return this.sendOut(this.partyPokemon[pos], world);
    }

    public void heal(int[] id) {
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || !PixelmonMethods.isIDSame(nbt, id)) continue;
            this.heal(nbt);
        }
    }

    private void heal(NBTTagCompound nbt) {
        nbt.setFloat("Health", nbt.getInteger("StatsHP"));
        nbt.setBoolean("IsFainted", false);
        int numMoves = nbt.getInteger("PixelmonNumberMoves");
        for (int i = 0; i < numMoves; ++i) {
            nbt.setInteger("PixelmonMovePP" + i, nbt.getInteger("PixelmonMovePPBase" + i));
        }
        nbt.removeTag("Status");
        if (this.mode == EnumPokeballManagerMode.Player) {
            this.updateClient(nbt, EnumUpdateType.Status, EnumUpdateType.HP, EnumUpdateType.Moveset);
        }
    }

    public void recallAllPokemon() {
        for (NBTTagCompound aPartyPokemon : this.partyPokemon) {
            int[] id;
            Optional<EntityPixelmon> p;
            if (aPartyPokemon == null || !(p = this.getAlreadyExists(id = PixelmonMethods.getID(aPartyPokemon), this.getPlayer().world)).isPresent()) continue;
            EntityPixelmon pixelmon = p.get();
            this.retrieve(pixelmon);
            this.updateNBT(pixelmon);
            pixelmon.isInBall = true;
            pixelmon.unloadEntity();
        }
    }

    public int getPosition(int[] pokemonID) {
        for (NBTTagCompound n : this.partyPokemon) {
            if (n == null || !PixelmonMethods.isIDSame(n, pokemonID)) continue;
            return n.getInteger("PixelmonOrder");
        }
        return -1;
    }

    public int[] getNextPokemonId(int[] pokemonID) {
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || !PixelmonMethods.isIDSame(nbt, pokemonID) || nbt.getBoolean("IsFainted") || nbt.getFloat("Health") < 0.0f) continue;
            return PixelmonMethods.getID(nbt);
        }
        return new int[]{-1, -1};
    }

    public void setAllToLevel(int level) {
        this.recallAllPokemon();
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null) continue;
            EntityPixelmon p = this.sendOut(PixelmonMethods.getID(nbt), this.getPlayer().world);
            p.getLvl().setLevel(level);
            p.getLvl().setExp(0);
            p.update(EnumUpdateType.Stats);
            p.unloadEntity();
        }
    }

    public int[] setAllToTempLevel(int level) {
        int[] oldLevels = new int[this.partyPokemon.length];
        for (int i = 0; i < this.partyPokemon.length; ++i) {
            NBTTagCompound nbt = this.partyPokemon[i];
            if (nbt == null) continue;
            EntityPixelmon p = this.sendOut(PixelmonMethods.getID(nbt), this.getPlayer().world);
            oldLevels[i] = p.getLvl().getLevel();
            if (p.getLvl().getLevel() > level) {
                p.getLvl().setLevel(level);
            }
            p.update(EnumUpdateType.Stats);
            p.unloadEntity();
        }
        return oldLevels;
    }

    public void setAllToLevel(int[] levels) {
        for (int i = 0; i < this.partyPokemon.length; ++i) {
            NBTTagCompound nbt = this.partyPokemon[i];
            if (nbt == null) continue;
            EntityPixelmon p = this.sendOut(PixelmonMethods.getID(nbt), this.getPlayer().world);
            p.getLvl().setLevel(levels[i]);
            p.update(EnumUpdateType.Stats);
            p.unloadEntity();
        }
    }

    public void setAllToLevelAndChooseMoveset(int level) {
        this.setAllToLevel(level);
        ArrayList<NBTTagCompound> pokemonList = new ArrayList<NBTTagCompound>(6);
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null) continue;
            pokemonList.add(nbt);
        }
        ChoosingMovesetData data = new ChoosingMovesetData(this.getPlayer(), pokemonList);
        data.next();
        if (!data.pokemonList.isEmpty()) {
            ChooseMoveset.choosingMoveset.add(data);
        }
    }

    public void setAllToLevelTrainer(int level) {
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null) continue;
            EntityPixelmon p = this.sendOut(PixelmonMethods.getID(nbt), this.trainer.world);
            p.getLvl().setLevel(level);
            this.updateNBT(p);
            p.unloadEntity();
        }
    }

    public void updateStatsTrainer() {
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null) continue;
            EntityPixelmon p = this.sendOut(PixelmonMethods.getID(nbt), this.trainer.world);
            this.updateNBT(p);
            p.unloadEntity();
        }
    }

    public Integer calculateHatchingBonus() {
        if (PixelmonPlayerUtils.hasItem(this.getPlayer(), itemStack -> itemStack.getItem() == PixelmonItems.ovalCharm)) {
            return 2;
        }
        String[] hatchingBonusAbilities = new String[]{"MagmaArmor", "MagmaArmour", "FlameBody"};
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || nbt.getBoolean("isEgg")) continue;
            String ability = nbt.getString("Ability");
            int var8 = hatchingBonusAbilities.length;
            for (String bonusAbility : hatchingBonusAbilities) {
                if (!ability.equalsIgnoreCase(bonusAbility)) continue;
                return 2;
            }
        }
        return 1;
    }

    public int calculateWalkedSteps(EntityPlayerMP playerMP) {
        int posX = playerMP.getPosition().getX();
        int posY = playerMP.getPosition().getY();
        int posZ = playerMP.getPosition().getZ();
        BlockPos lastPos = new BlockPos(this.lastXPos, this.lastYPos, this.lastZPos);
        BlockPos pos = playerMP.getPosition();
        int changeX = this.lastXPos - posX;
        int changeZ = this.lastZPos - posZ;
        this.lastXPos = posX;
        this.lastYPos = posY;
        this.lastZPos = posZ;
        if (changeX == -posX && changeZ == -posZ) {
            return 0;
        }
        int stepsTaken = Math.abs(changeX) + Math.abs(changeZ);
        if (stepsTaken > 20 || stepsTaken == 0) {
            return 0;
        }
        PlayerMovementEvent movementEvent = new PlayerMovementEvent(playerMP, stepsTaken, lastPos, pos);
        MinecraftForge.EVENT_BUS.post(movementEvent);
        int hatchBonus = this.calculateHatchingBonus();
        for (NBTTagCompound pokemonNBT : this.partyPokemon) {
            if (pokemonNBT == null || !pokemonNBT.getBoolean("isEgg")) continue;
            int currentSteps = pokemonNBT.getInteger("steps");
            pokemonNBT.setInteger("steps", currentSteps += stepsTaken);
            int stepsNeeded = PixelmonConfig.stepsPerEggCycle;
            PlayerEggStepsEvent event = new PlayerEggStepsEvent(playerMP, stepsNeeded);
            MinecraftForge.EVENT_BUS.post(event);
            if (event.isCanceled()) {
                return 0;
            }
            stepsNeeded = event.getStepsRequired();
            if (currentSteps > stepsNeeded) {
                pokemonNBT.setInteger("steps", 0);
                int eggCycles = pokemonNBT.getInteger("eggCycles");
                pokemonNBT.setInteger("eggCycles", eggCycles -= Math.max(1, hatchBonus));
                if (eggCycles < 0) {
                    this.hatchEgg(playerMP, pokemonNBT);
                }
            }
            this.updateClient(pokemonNBT, EnumUpdateType.Egg);
        }
        if (this.mode == EnumPokeballManagerMode.Player) {
            EntityPlayerExtension.updatePlayerPokeballs(playerMP, this.getPokeballList());
            EntityPlayerExtension.updatePlayerEggs(this.getPlayer(), this.getEggList());
        }
        return stepsTaken;
    }

    public void hatchEgg(EntityPlayerMP playerMP, NBTTagCompound eggNBT) {
        String name;
        String nameLower;
        eggNBT.setString("originalTrainer", playerMP.getDisplayName().getUnformattedText());
        eggNBT.setString("originalTrainerUUID", playerMP.getUniqueID().toString());
        eggNBT.setBoolean("isEgg", false);
        PlayerEggHatchEvent event = new PlayerEggHatchEvent(playerMP, eggNBT);
        MinecraftForge.EVENT_BUS.post(event);
        eggNBT = event.getEggNBT();
        EnumSpecies species = EnumSpecies.getFromNameAnyCase(eggNBT.getString("Name"));
        if (!MinecraftForge.EVENT_BUS.post(new PokedexEvent(playerMP, this.pokedex, species, EnumPokedexRegisterStatus.caught))) {
            this.pokedex.set(species, EnumPokedexRegisterStatus.caught);
            this.pokedex.sendToPlayer(playerMP);
        }
        String langString = !(nameLower = (name = Entity1Base.getLocalizedName(eggNBT.getString("Name"))).toLowerCase()).startsWith("a") && !nameLower.startsWith("e") && !nameLower.startsWith("i") && !nameLower.startsWith("o") && !nameLower.startsWith("u") ? "pixelmon.egg.hatching" : "pixelmon.egg.hatchingan";
        ChatHandler.sendFormattedChat(playerMP, TextFormatting.GREEN, langString, name);
        this.sendUpdatedList();
    }

    public void checkEggStep(EntityPlayerMP player) {
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || !nbt.getBoolean("isEgg")) continue;
            BlockPos position = player.getPosition();
            int posX = position.getX();
            int posZ = position.getZ();
            int changeX = Math.abs(nbt.getInteger("lastEggXCoord") - posX);
            int changeZ = Math.abs(nbt.getInteger("lastEggZCoord") - posZ);
            if (changeX < 2 && changeZ < 2) continue;
            nbt.setInteger("lastEggXCoord", posX);
            nbt.setInteger("lastEggZCoord", posZ);
            int numSteps = changeX + changeZ;
            if (numSteps > 20) {
                numSteps = 0;
            }
            nbt.setInteger("steps", nbt.getInteger("steps") + numSteps);
            if (nbt.getInteger("steps") > PixelmonConfig.stepsPerEggCycle) {
                nbt.setInteger("eggCycles", nbt.getInteger("eggCycles") - this.calculateHatchingBonus());
                if (nbt.getInteger("eggCycles") < 0) {
                    String name;
                    String nameLower;
                    nbt.setString("originalTrainer", player.getDisplayName().getUnformattedText());
                    nbt.setString("originalTrainerUUID", player.getUniqueID().toString());
                    nbt.setBoolean("isEgg", false);
                    PlayerEggHatchEvent event = new PlayerEggHatchEvent(player, nbt);
                    MinecraftForge.EVENT_BUS.post(event);
                    nbt = event.getEggNBT();
                    EnumSpecies species = EnumSpecies.getFromNameAnyCase(nbt.getString("Name"));
                    if (!MinecraftForge.EVENT_BUS.post(new PokedexEvent(player, this.pokedex, species, EnumPokedexRegisterStatus.caught))) {
                        this.pokedex.set(species, EnumPokedexRegisterStatus.caught);
                        this.pokedex.sendToPlayer(player);
                    }
                    String langString = !(nameLower = (name = Entity1Base.getLocalizedName(nbt.getString("Name"))).toLowerCase()).startsWith("a") && !nameLower.startsWith("e") && !nameLower.startsWith("i") && !nameLower.startsWith("o") && !nameLower.startsWith("u") ? "pixelmon.egg.hatching" : "pixelmon.egg.hatchingan";
                    ChatHandler.sendFormattedChat(player, TextFormatting.GREEN, langString, name);
                    this.sendUpdatedList();
                } else {
                    nbt.setInteger("steps", 0);
                }
            }
            if (this.mode != EnumPokeballManagerMode.Player) continue;
            this.updateClient(nbt, EnumUpdateType.Egg);
            EntityPlayerExtension.updatePlayerPokeballs(player, this.getPokeballList());
            EntityPlayerExtension.updatePlayerEggs(this.getPlayer(), this.getEggList());
        }
    }

    public static LinkedHashMap<String, Class> getNBTTags() {
        LinkedHashMap<String, Class> tags = new LinkedHashMap<String, Class>(35);
        PlayerData.getNBTTags(tags);
        TeleportPosition.getNBTTags(tags);
        tags.put("pixelDollars", Integer.class);
        tags.put("idcounter", Integer.class);
        tags.put("starterPicked", Boolean.class);
        Pokedex.getNBTTags(tags);
        PlayerStats.getNBTTags(tags);
        tags.put("TicksTillNextEncounter", Integer.class);
        MegaData.getNBTTags(tags);
        return tags;
    }

    public String getDisplayName() {
        return this.getPlayer().getDisplayName().getUnformattedText();
    }

    public int getAveragePartyLevel() {
        int c = 0;
        int levelSum = 0;
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || nbt.getBoolean("isEgg")) continue;
            ++c;
            levelSum += nbt.getInteger("Level");
        }
        return (int)((float)levelSum / (float)c);
    }

    public int[] getRandomPartyPokemon(ArrayList<PixelmonWrapper> inBattle) {
        NBTTagCompound[] nbtList;
        ArrayList<NBTTagCompound> array = new ArrayList<NBTTagCompound>();
        for (NBTTagCompound nbt : nbtList = this.getList()) {
            if (nbt == null) continue;
            boolean isInBattle = false;
            for (PixelmonWrapper pw : inBattle) {
                if (!PixelmonMethods.isIDSame(nbt, pw.getPokemonID())) continue;
                isInBattle = true;
                break;
            }
            if (isInBattle || nbt.getBoolean("IsFainted") || nbt.getFloat("Health") < 0.0f || nbt.getBoolean("isEgg")) continue;
            array.add(nbt);
        }
        if (array.isEmpty()) {
            return null;
        }
        NBTTagCompound nbt = (NBTTagCompound)RandomHelper.getRandomElementFromList(array);
        return PixelmonMethods.getID(nbt);
    }

    public int getTicksTillEncounter() {
        return this.ticksTillEncounter;
    }

    public void updateTicksTillEncounter() {
        if (this.ticksTillEncounter <= 1) {
            ItemStack heldStack;
            NBTTagCompound compound = this.partyPokemon[0];
            if (compound != null && !compound.getBoolean("isEgg") && (heldStack = ItemHeld.readHeldItemFromNBT(compound)) != null && ((ItemHeld)heldStack.getItem()).getHeldItemType() == EnumHeldItems.cleanseTag) {
                this.ticksTillEncounter = this.getPlayer().getRNG().nextInt(1200) + 600;
                return;
            }
            this.ticksTillEncounter = this.getPlayer().getRNG().nextInt(900) + 100;
        } else {
            --this.ticksTillEncounter;
        }
    }

    public void randomizePokemon() {
        int highestLevel = this.getHighestLevel();
        for (int i = 0; i < this.partyPokemon.length; ++i) {
            EntityPixelmon newPokemon = (EntityPixelmon)PixelmonEntityList.createEntityByName(EnumSpecies.randomPoke().name, this.getPlayer().world);
            newPokemon.getLvl().setLevel(highestLevel);
            newPokemon.friendship.initFromCapture();
            this.addToParty(newPokemon, i);
        }
    }

    public void removeFromPartyPlayer(int slot) {
        if (this.partyPokemon[slot] != null) {
            Pixelmon.NETWORK.sendTo(new Remove(PixelmonMethods.getID(this.partyPokemon[slot])), this.getPlayer());
            this.partyPokemon[slot] = null;
        }
    }

    public void removeFromPartyTrainer(int slot) {
        int numPokemon = this.count();
        if (this.partyPokemon[slot] != null) {
            if (numPokemon - (slot + 1) >= 0) {
                System.arraycopy(this.partyPokemon, slot + 1, this.partyPokemon, slot + 1 - 1, numPokemon - (slot + 1));
            }
            this.partyPokemon[numPokemon - 1] = null;
        }
    }

    public PixelmonData[] convertToData() {
        PixelmonData[] data = new PixelmonData[6];
        for (int i = 0; i < this.partyPokemon.length; ++i) {
            if (this.partyPokemon[i] == null) continue;
            data[i] = new PixelmonData(this.partyPokemon[i]);
        }
        return data;
    }

    public void setInWorld(EntityPixelmon t, boolean b) {
        for (int i = 0; i < this.partyPokemon.length; ++i) {
            if (this.partyPokemon[i] == null || !PixelmonMethods.isIDSame(this.partyPokemon[i], t)) continue;
            this.isInWorld[i] = b;
        }
    }

    public void setInWorld(int[] pokemonId, boolean b) {
        for (int i = 0; i < this.partyPokemon.length; ++i) {
            if (this.partyPokemon[i] == null || !PixelmonMethods.isIDSame(this.partyPokemon[i], pokemonId)) continue;
            this.isInWorld[i] = b;
        }
    }

    public boolean isInWorld(int[] pokemonId) {
        for (int i = 0; i < this.partyPokemon.length; ++i) {
            if (this.partyPokemon[i] == null || !PixelmonMethods.isIDSame(this.partyPokemon[i], pokemonId)) continue;
            return this.isInWorld[i];
        }
        return false;
    }

    public boolean isEvolving(World world) {
        for (int i = 0; i < this.partyPokemon.length; ++i) {
            Optional<EntityPixelmon> pokemon;
            if (!this.isInWorld[i] || this.partyPokemon[i] == null || !(pokemon = this.getAlreadyExists(PixelmonMethods.getID(this.partyPokemon[i]), world)).isPresent() || !pokemon.get().isEvolving()) continue;
            return true;
        }
        return false;
    }

    public boolean canPartyBattle() {
        for (NBTTagCompound nbt : this.getList()) {
            if (!PlayerStorage.canBattle(nbt)) continue;
            return true;
        }
        return false;
    }

    public boolean hasPokeRusPokemon() {
        for (NBTTagCompound nbt : this.partyPokemon) {
            if (nbt == null || nbt.getInteger("PokeRus") != 1) continue;
            return true;
        }
        return false;
    }

    public void pokeRusTick() {
        if (this.hasPokeRusPokemon()) {
            for (NBTTagCompound nbtTagCompound : this.partyPokemon) {
                NBTLink link;
                PokerusEvent pokerusEvent;
                NBTTagCompound nbt = nbtTagCompound;
                if (nbt == null) continue;
                EntityPlayerMP player = this.getPlayer();
                if (RandomHelper.getRandomChance(1.0 / (double)PixelmonConfig.pokeRusSpreadRate) && nbt.getInteger("PokeRus") == 0 && player != null && !MinecraftForge.EVENT_BUS.post(pokerusEvent = new PokerusEvent.SpreadEvent(player, link = new NBTLink(nbt)))) {
                    nbt = pokerusEvent.getPokemonLink().getNBT();
                    nbt.setInteger("PokeRus", 1);
                    this.changePokemon(this.getPosition(PixelmonMethods.getID(nbt)), nbt);
                    this.updateClient(nbt, EnumUpdateType.values());
                    TextComponentTranslation msg = new TextComponentTranslation("pokerus.infected", !nbt.getString("Nickname").isEmpty() ? nbt.getString("Nickname") : nbt.getString("Name"));
                    msg.getStyle().setColor(TextFormatting.GREEN).setBold(true);
                    player.sendMessage(msg);
                }
                if (nbt.getInteger("PokeRus") != 1) continue;
                if (nbt.getInteger("PokeRusTimer") >= PixelmonConfig.pokeRusTimerMax) {
                    link = new NBTLink(nbt);
                    pokerusEvent = new PokerusEvent.CuredEvent(player, link);
                    if (MinecraftForge.EVENT_BUS.post(pokerusEvent)) continue;
                    nbt = pokerusEvent.getPokemonLink().getNBT();
                    nbt.setInteger("PokeRus", 2);
                    this.changePokemon(this.getPosition(PixelmonMethods.getID(nbt)), nbt);
                    this.updateClient(nbt, EnumUpdateType.values());
                    continue;
                }
                if (nbt.getInteger("PokeRusTimer") >= PixelmonConfig.pokeRusTimerMin) {
                    if (!RandomHelper.getRandomChance(20000) || MinecraftForge.EVENT_BUS.post(pokerusEvent = new PokerusEvent.CuredEvent(player, link = new NBTLink(nbt)))) continue;
                    nbt = pokerusEvent.getPokemonLink().getNBT();
                    nbt.setInteger("PokeRus", 2);
                    this.changePokemon(this.getPosition(PixelmonMethods.getID(nbt)), nbt);
                    this.updateClient(nbt, EnumUpdateType.values());
                    continue;
                }
                nbt.setInteger("PokeRusTimer", nbt.getInteger("PokeRusTimer") + 1);
            }
        }
    }

    @Nullable
    public NBTTagCompound getPokemonNbtFromSlot(int slot) {
        if (slot == -1) {
            return null;
        }
        return this.partyPokemon[slot];
    }

    public int getFirstEggSlot() {
        for (int i = 0; i < this.partyPokemon.length; ++i) {
            NBTTagCompound tag = this.partyPokemon[i];
            if (tag == null || !tag.getBoolean("isEgg")) continue;
            return i;
        }
        return -1;
    }
}

