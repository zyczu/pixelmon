/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Maps
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.storage.playerData;

import com.google.common.collect.Maps;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.core.enums.EnumSpecies;
import io.netty.buffer.ByteBuf;
import java.util.LinkedHashMap;
import java.util.Map;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class MountData {
    public Map<PokemonSpec, Boolean> mountRegistry = this.generateDefaultData();
    public boolean hasFlute = false;

    private Map<PokemonSpec, Boolean> generateDefaultData() {
        LinkedHashMap map = Maps.newLinkedHashMap();
        map.put(EnumSpecies.Wyrdeer.toSpec(), false);
        map.put(EnumSpecies.Ursaluna.toSpec(), false);
        map.put(EnumSpecies.Basculegion.toSpec(), false);
        map.put(EnumSpecies.Sneasler.toSpec(), false);
        map.put(PokemonSpec.from("Braviary", "f:11"), false);
        return map;
    }

    public void writeToByteBuf(ByteBuf buf) {
        buf.writeInt(this.mountRegistry.size());
        this.mountRegistry.forEach((species, bool) -> {
            ByteBufUtils.writeUTF8String(buf, species.toString());
            buf.writeBoolean(bool.booleanValue());
        });
    }

    public void readToByteBuf(ByteBuf buf) {
        this.mountRegistry.clear();
        int size = buf.readInt();
        for (int i = 0; i < size; ++i) {
            this.mountRegistry.put(PokemonSpec.from(ByteBufUtils.readUTF8String(buf).split(" ")), buf.readBoolean());
        }
    }

    public void writeToNBT(NBTTagCompound nbt) {
        NBTTagList list = new NBTTagList();
        for (PokemonSpec pokemon : this.mountRegistry.keySet()) {
            NBTTagCompound status = new NBTTagCompound();
            status.setString("Name", pokemon.toString());
            status.setBoolean("Status", this.mountRegistry.get(pokemon));
            list.appendTag(list);
        }
        nbt.setTag("MountStatus", list);
        nbt.setBoolean("HasFlute", this.hasFlute);
    }

    public void readFromNBT(NBTTagCompound nbt) {
        if (nbt.hasKey("MountStatus")) {
            NBTTagList list = nbt.getTagList("MountStatus", 10);
            for (int i = 0; i < list.tagCount(); ++i) {
                NBTTagCompound statusInfo = (NBTTagCompound)list.get(i);
                PokemonSpec pokemon = new PokemonSpec(statusInfo.getString("Name"));
                boolean status = statusInfo.getBoolean("Status");
                this.mountRegistry.put(pokemon, status);
            }
        }
        if (nbt.hasKey("HasFlute")) {
            this.hasFlute = nbt.getBoolean("HasFlute");
        }
    }
}

