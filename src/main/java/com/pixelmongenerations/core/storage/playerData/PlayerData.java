/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage.playerData;

import com.pixelmongenerations.core.storage.playerData.ISaveData;
import java.time.LocalDate;
import java.util.HashMap;
import net.minecraft.nbt.NBTTagCompound;

public class PlayerData
implements ISaveData {
    private boolean gifted = false;
    public boolean giftOpened = false;
    public boolean candyRecieved = false;
    public boolean candyOpened = false;
    public int candyLastDay = -1;
    public boolean obtainedPoipole = false;
    public boolean obtainedArceus = false;
    public String unownCaught = "";

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        nbt.setBoolean("gifted", this.gifted);
        nbt.setBoolean("giftOpened", this.giftOpened);
        nbt.setBoolean("candyRecieved", this.candyRecieved);
        nbt.setBoolean("candyOpened", this.candyOpened);
        nbt.setInteger("candyLastDay", this.candyLastDay);
        nbt.setBoolean("obtainedPoipole", this.obtainedPoipole);
        nbt.setBoolean("obtainedArceus", this.obtainedArceus);
        nbt.setString("unownCaught", this.unownCaught);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        this.gifted = nbt.getBoolean("gifted");
        this.giftOpened = nbt.getBoolean("giftOpened");
        this.candyRecieved = nbt.getBoolean("candyRecieved");
        this.candyOpened = nbt.getBoolean("candyOpened");
        this.candyLastDay = nbt.getInteger("candyLastDay");
        this.obtainedPoipole = nbt.getBoolean("obtainedPoipole");
        this.obtainedArceus = nbt.getBoolean("obtainedArceus");
        this.unownCaught = nbt.getString("unownCaught");
    }

    public String[] getDate() {
        return LocalDate.now().toString().split("-");
    }

    public boolean hasRecievedGift() {
        return this.gifted;
    }

    public void setReceivedGift(boolean gifted) {
        this.gifted = gifted;
    }

    public boolean hasRecievedCandy(int currentDay) {
        return this.candyLastDay == currentDay;
    }

    public void setRecievedCandy(int currentDay) {
        this.candyLastDay = currentDay;
    }

    public void setObtainedPoipole(boolean obtainedPoipole) {
        this.obtainedPoipole = obtainedPoipole;
    }

    public void setObtainedArceus(boolean obtainedArceus) {
        this.obtainedArceus = obtainedArceus;
    }

    public boolean hasObtainedPoipole() {
        return this.obtainedPoipole;
    }

    public boolean hasObtainedArceus() {
        return this.obtainedArceus;
    }

    public String getUnownCaught() {
        return this.unownCaught;
    }

    public static void getNBTTags(HashMap<String, Class> tags) {
        tags.put("gifted", Boolean.class);
        tags.put("giftOpened", Boolean.class);
        tags.put("candyRecieved", Boolean.class);
        tags.put("candyOpen", Boolean.class);
        tags.put("candyLastDay", Integer.class);
        tags.put("obtainedPoipole", Boolean.class);
        tags.put("obtainedArceus", Boolean.class);
        tags.put("unownCaught", String.class);
        tags.put("cape", String.class);
    }
}

