/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage.playerData;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumShinyItem;
import com.pixelmongenerations.core.event.EntityPlayerExtension;
import com.pixelmongenerations.core.network.packetHandlers.clientStorage.UpdateClientPlayerData;
import com.pixelmongenerations.core.storage.PlayerStorage;
import net.minecraft.nbt.NBTTagCompound;

public class ShinyData {
    private EnumShinyItem shinyItem = EnumShinyItem.Disabled;
    private final PlayerStorage parentStorage;
    private boolean canEquip = false;

    public ShinyData(PlayerStorage playerStorage) {
        this.parentStorage = playerStorage;
    }

    public void writeToNBT(NBTTagCompound nbt) {
        nbt.setString("ShinyItemString", this.shinyItem.toString());
        nbt.setBoolean("CanEquip", this.canEquip);
    }

    public void readFromNBT(NBTTagCompound nbt) {
        if (nbt.hasKey("ShinyItemString") || this.canEquipShinyCharm()) {
            if (!nbt.hasKey("ShinyItemString") && this.canEquipShinyCharm()) {
                this.setShinyItem(EnumShinyItem.None, false);
            } else if (this.canEquipShinyCharm()) {
                this.setShinyItem(EnumShinyItem.getFromString(nbt.getString("ShinyItemString")), false);
            } else {
                this.setShinyItem(EnumShinyItem.Disabled, false);
            }
        } else {
            this.setShinyItem(EnumShinyItem.Disabled, false);
        }
        this.canEquip = nbt.getBoolean("CanEquip");
    }

    public void setShinyItem(EnumShinyItem shinyItem, boolean giveChoice) {
        this.shinyItem = shinyItem;
        if (this.parentStorage.getPlayer() != null) {
            if (giveChoice) {
                Pixelmon.NETWORK.sendTo(new UpdateClientPlayerData(this.shinyItem, true), this.parentStorage.getPlayer());
            }
            EntityPlayerExtension.updatePlayerShinyItem(this.parentStorage.getPlayer(), this.shinyItem);
        }
    }

    public boolean canEquipShinyCharm() {
        return this.canEquip || this.parentStorage.pokedex.isCompleted();
    }

    public void setCanEquipShinyCharm(boolean canEquip) {
        this.canEquip = canEquip;
    }

    public EnumShinyItem getShinyItem() {
        return this.shinyItem;
    }
}

