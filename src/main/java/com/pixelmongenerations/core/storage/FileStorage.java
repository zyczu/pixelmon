/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.storage;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.storage.StorageAdapter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.DimensionManager;

public class FileStorage
implements StorageAdapter {
    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    @Override
    public NBTTagCompound readPlayerData(UUID uuid) {
        File pokemonFolder = new File(DimensionManager.getCurrentSaveRootDirectory(), "pokemon");
        File playerSaveFile = new File(pokemonFolder, uuid.toString() + ".pk");
        if (!playerSaveFile.exists()) {
            return null;
        }
        try (DataInputStream dataStream = new DataInputStream(new FileInputStream(playerSaveFile));){
            NBTTagCompound nBTTagCompound = CompressedStreamTools.read(dataStream);
            return nBTTagCompound;
        }
        catch (IOException e) {
            if (!PixelmonConfig.printErrors) {
                return null;
            }
            Pixelmon.LOGGER.error("Couldn't read player data file for " + uuid, (Throwable)e);
            return null;
        }
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    @Override
    public NBTTagCompound readPlayerData(UUID uuid, File file) {
        if (!file.exists()) {
            return null;
        }
        try (DataInputStream dataStream = new DataInputStream(new FileInputStream(file));){
            NBTTagCompound nBTTagCompound = CompressedStreamTools.read(dataStream);
            return nBTTagCompound;
        }
        catch (IOException e) {
            if (!PixelmonConfig.printErrors) {
                return null;
            }
            Pixelmon.LOGGER.error("Couldn't read player data file for " + uuid.toString(), (Throwable)e);
            return null;
        }
    }

    @Override
    public void writePlayerData(UUID uuid, NBTTagCompound storage) {
        File pokemonFolder = new File(DimensionManager.getCurrentSaveRootDirectory(), "pokemon");
        if (!pokemonFolder.exists()) {
            pokemonFolder.mkdirs();
        }
        File playerSaveFile = new File(pokemonFolder, uuid.toString() + ".pktemp");
        try {
            playerSaveFile.createNewFile();
            try (DataOutputStream dataStream = new DataOutputStream(new FileOutputStream(playerSaveFile));){
                CompressedStreamTools.write(storage, dataStream);
            }
            this.replaceSaveFile(uuid, ".pk");
        }
        catch (IOException e) {
            if (!PixelmonConfig.printErrors) {
                return;
            }
            Pixelmon.LOGGER.error("Couldn't write player data file for " + uuid, (Throwable)e);
        }
    }

    @Override
    public void writePlayerData(UUID uuid, NBTTagCompound storage, File file, String extension) {
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            try (DataOutputStream dataStream = new DataOutputStream(new FileOutputStream(file));){
                CompressedStreamTools.write(storage, dataStream);
            }
            this.replaceSaveFile(uuid, extension, file);
        }
        catch (IOException e) {
            if (!PixelmonConfig.printErrors) {
                return;
            }
            Pixelmon.LOGGER.error("Couldn't write player data file for " + uuid.toString(), (Throwable)e);
        }
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    @Override
    public NBTTagCompound readComputerData(UUID uuid) {
        File pokemonFolder = new File(DimensionManager.getCurrentSaveRootDirectory(), "pokemon");
        File playerSaveFile = new File(pokemonFolder, uuid.toString() + ".comp");
        if (!playerSaveFile.exists()) {
            return null;
        }
        try (DataInputStream dataStream = new DataInputStream(new FileInputStream(playerSaveFile));){
            NBTTagCompound nBTTagCompound = CompressedStreamTools.read(dataStream);
            return nBTTagCompound;
        }
        catch (IOException e) {
            if (!PixelmonConfig.printErrors) {
                return null;
            }
            Pixelmon.LOGGER.error("Couldn't read player computer data file for " + uuid, (Throwable)e);
            return null;
        }
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    @Override
    public NBTTagCompound readComputerData(UUID uuid, File file) {
        if (!file.exists()) {
            return null;
        }
        try (DataInputStream dataStream = new DataInputStream(new FileInputStream(file));){
            NBTTagCompound nBTTagCompound = CompressedStreamTools.read(dataStream);
            return nBTTagCompound;
        }
        catch (IOException e) {
            if (!PixelmonConfig.printErrors) {
                return null;
            }
            Pixelmon.LOGGER.error("Couldn't read player computer data file for " + uuid.toString(), (Throwable)e);
            return null;
        }
    }

    @Override
    public void writeComputerData(UUID uuid, NBTTagCompound storage) {
        CompletableFuture.runAsync(() -> {
            File pokemonFolder = new File(DimensionManager.getCurrentSaveRootDirectory(), "pokemon");
            File playerSaveFile = new File(pokemonFolder, uuid.toString() + ".comptemp");
            try {
                playerSaveFile.createNewFile();
                try (DataOutputStream dataStream = new DataOutputStream(new FileOutputStream(playerSaveFile));){
                    CompressedStreamTools.write(storage, dataStream);
                }
            }
            catch (IOException e) {
                if (!PixelmonConfig.printErrors) {
                    return;
                }
                Pixelmon.LOGGER.error("Couldn't write player computer data file for " + uuid, (Throwable)e);
            }
        }).thenAccept(v -> this.replaceSaveFile(uuid, ".comp"));
    }

    @Override
    public void writeComputerData(UUID uuid, NBTTagCompound storage, File file, String extension) {
        CompletableFuture.runAsync(() -> {
            try {
                if (!file.exists()) {
                    file.createNewFile();
                }
                try (DataOutputStream dataStream = new DataOutputStream(new FileOutputStream(file));){
                    CompressedStreamTools.write(storage, dataStream);
                }
            }
            catch (IOException e) {
                if (!PixelmonConfig.printErrors) {
                    return;
                }
                Pixelmon.LOGGER.error("Couldn't write player computer data file for " + uuid.toString(), (Throwable)e);
            }
        }).thenAccept(v -> this.replaceSaveFile(uuid, extension, file));
    }

    private void replaceSaveFile(UUID uuid, String fileExtension) {
        File pokemonFolder = new File(DimensionManager.getCurrentSaveRootDirectory(), "pokemon");
        File tempFile = new File(pokemonFolder, uuid.toString() + fileExtension + "temp");
        File saveFile = new File(pokemonFolder, uuid + fileExtension);
        if (tempFile.exists()) {
            if (saveFile.exists()) {
                saveFile.delete();
            }
            tempFile.renameTo(saveFile);
        }
    }

    private void replaceSaveFile(UUID uuid, String fileExtension, File file) {
        File tempFile = new File(file, uuid.toString() + fileExtension + "temp");
        File saveFile = new File(file, uuid + fileExtension);
        if (tempFile.exists()) {
            if (saveFile.exists()) {
                saveFile.delete();
            }
            tempFile.renameTo(saveFile);
        }
    }
}

