/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.common.battle.attacks.Effectiveness;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import net.minecraft.util.text.translation.I18n;

public enum EnumType {
    Normal(0, 1, "Normal", 0xDDDDDD, 0.0f, 297.0f),
    Fire(1, 10, "Fire", 0xFF8800, 198.0f, 99.0f),
    Water(2, 11, "Water", 5727743, 396.0f, 99.0f),
    Electric(3, 13, "Electric", 16118348, 297.0f, 0.0f),
    Grass(4, 12, "Grass", 54304, 99.0f, 198.0f),
    Ice(5, 15, "Ice", 0xB0FFFF, 297.0f, 198.0f),
    Fighting(6, 2, "Fighting", 12189752, 99.0f, 99.0f),
    Poison(7, 4, "Poison", 13174256, 99.0f, 297.0f),
    Ground(8, 5, "Ground", 10053184, 198.0f, 198.0f),
    Flying(9, 3, "Flying", 13424127, 297.0f, 99.0f),
    Psychic(10, 14, "Psychic", 16733419, 198.0f, 297.0f),
    Bug(11, 7, "Bug", 11067475, 0.0f, 0.0f),
    Rock(12, 6, "Rock", 10712917, 297.0f, 297.0f),
    Ghost(13, 8, "Ghost", 7089034, 0.0f, 198.0f),
    Dragon(14, 16, "Dragon", 2830540, 198.0f, 0.0f),
    Dark(15, 17, "Dark", 0x404040, 99.0f, 0.0f),
    Steel(16, 9, "Steel", 0xBCBCC2, 396.0f, 0.0f),
    Mystery(17, 17, "???", 3184256, 0.0f, 297.0f),
    Fairy(18, 18, "Fairy", 16614535, 0.0f, 99.0f);

    private int index;
    private int dbIndex;
    private String name;
    private String lowerName;
    private int color;
    public float textureX;
    public float textureY;

    private EnumType(int i, int dbi, String s, int c, float texX, float texY) {
        this.index = i;
        this.dbIndex = dbi;
        this.name = s;
        this.color = c;
        this.textureX = texX;
        this.textureY = texY;
    }

    public int getColor() {
        return this.color;
    }

    public String getName() {
        return this.name;
    }

    public String getLocalizedName() {
        if (this == Mystery) {
            return this.name;
        }
        return I18n.translateToLocal("type." + this.name.toLowerCase());
    }

    public static EnumType parseTypeFromDBID(int id) {
        for (EnumType type : EnumType.values()) {
            if (type.dbIndex != id) continue;
            return type;
        }
        return Normal;
    }

    public static EnumType parseType(int index) {
        try {
            return EnumType.values()[index];
        }
        catch (IndexOutOfBoundsException e) {
            return Normal;
        }
    }

    public static EnumType parseType(String name) {
        try {
            return EnumType.valueOf(name);
        }
        catch (Exception e) {
            return Normal;
        }
    }

    public static EnumType parseOrNull(int index) {
        try {
            return EnumType.values()[index];
        }
        catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public static ArrayList<EnumType> getAllTypes() {
        ArrayList<EnumType> list = new ArrayList<EnumType>();
        EnumType[] t = EnumType.values();
        Collections.addAll(list, t);
        return list;
    }

    public static float getTotalEffectiveness(List<EnumType> enumTypes, EnumType attackType) {
        return EnumType.getTotalEffectiveness(enumTypes, attackType, false);
    }

    public static float getTotalEffectiveness(List<EnumType> enumTypes, EnumType attackType, boolean inverse) {
        float f = 1.0f;
        for (EnumType type : enumTypes) {
            if (type == null) continue;
            f *= EnumType.getEffectiveness(attackType, type);
        }
        if (inverse) {
            f = EnumType.inverseEffectiveness(f);
        }
        return f;
    }

    public static float inverseEffectiveness(float effectiveness) {
        return effectiveness == 0.0f ? 2.0f : 1.0f / effectiveness;
    }

    public static float getEffectiveness(EnumType t, EnumType t1) {
        if (t == Mystery || t1 == Mystery) {
            return Effectiveness.Normal.value;
        }
        float e = 1.0f;
        if (t == Normal) {
            if (t1 == Rock || t1 == Steel) {
                e = Effectiveness.Not.value;
            } else if (t1 == Ghost) {
                e = Effectiveness.None.value;
            }
        } else if (t == Fire) {
            if (t1 == Grass || t1 == Ice || t1 == Bug || t1 == Steel) {
                e = Effectiveness.Super.value;
            } else if (t1 == Fire || t1 == Water || t1 == Rock || t1 == Dragon) {
                e = Effectiveness.Not.value;
            }
        } else if (t == Water) {
            if (t1 == Fire || t1 == Ground || t1 == Rock) {
                e = Effectiveness.Super.value;
            } else if (t1 == Water || t1 == Grass || t1 == Dragon) {
                e = Effectiveness.Not.value;
            }
        } else if (t == Electric) {
            if (t1 == Water || t1 == Flying) {
                e = Effectiveness.Super.value;
            } else if (t1 == Electric || t1 == Grass || t1 == Dragon) {
                e = Effectiveness.Not.value;
            } else if (t1 == Ground) {
                e = Effectiveness.None.value;
            }
        } else if (t == Grass) {
            if (t1 == Water || t1 == Ground || t1 == Rock) {
                e = Effectiveness.Super.value;
            } else if (t1 == Fire || t1 == Grass || t1 == Poison || t1 == Flying || t1 == Bug || t1 == Dragon || t1 == Steel) {
                e = Effectiveness.Not.value;
            }
        } else if (t == Ice) {
            if (t1 == Grass || t1 == Ground || t1 == Flying || t1 == Dragon) {
                e = Effectiveness.Super.value;
            } else if (t1 == Fire || t1 == Water || t1 == Ice || t1 == Steel) {
                e = Effectiveness.Not.value;
            }
        } else if (t == Fighting) {
            if (t1 == Normal || t1 == Ice || t1 == Rock || t1 == Dark || t1 == Steel) {
                e = Effectiveness.Super.value;
            } else if (t1 == Poison || t1 == Flying || t1 == Psychic | t1 == Bug || t1 == Fairy) {
                e = Effectiveness.Not.value;
            } else if (t1 == Ghost) {
                e = Effectiveness.None.value;
            }
        } else if (t == Poison) {
            if (t1 == Grass || t1 == Fairy) {
                e = Effectiveness.Super.value;
            } else if (t1 == Poison || t1 == Ground || t1 == Rock || t1 == Ghost) {
                e = Effectiveness.Not.value;
            } else if (t1 == Steel) {
                e = Effectiveness.None.value;
            }
        } else if (t == Ground) {
            if (t1 == Fire || t1 == Electric || t1 == Poison || t1 == Rock || t1 == Steel) {
                e = Effectiveness.Super.value;
            } else if (t1 == Grass || t1 == Bug) {
                e = Effectiveness.Not.value;
            } else if (t1 == Flying) {
                e = Effectiveness.None.value;
            }
        } else if (t == Flying) {
            if (t1 == Grass || t1 == Fighting || t1 == Bug) {
                e = Effectiveness.Super.value;
            } else if (t1 == Electric || t1 == Rock || t1 == Steel) {
                e = Effectiveness.Not.value;
            }
        } else if (t == Psychic) {
            if (t1 == Fighting || t1 == Poison) {
                e = Effectiveness.Super.value;
            } else if (t1 == Psychic || t1 == Steel) {
                e = Effectiveness.Not.value;
            } else if (t1 == Dark) {
                e = Effectiveness.None.value;
            }
        } else if (t == Bug) {
            if (t1 == Grass || t1 == Psychic || t1 == Dark) {
                e = Effectiveness.Super.value;
            } else if (t1 == Fire || t1 == Fighting || t1 == Poison || t1 == Flying || t1 == Ghost || t1 == Steel || t1 == Fairy) {
                e = Effectiveness.Not.value;
            }
        } else if (t == Rock) {
            if (t1 == Fire || t1 == Ice || t1 == Flying || t1 == Bug) {
                e = Effectiveness.Super.value;
            } else if (t1 == Fighting || t1 == Ground || t1 == Steel) {
                e = Effectiveness.Not.value;
            }
        } else if (t == Ghost) {
            if (t1 == Psychic || t1 == Ghost) {
                e = Effectiveness.Super.value;
            } else if (t1 == Dark) {
                e = Effectiveness.Not.value;
            } else if (t1 == Normal) {
                e = Effectiveness.None.value;
            }
        } else if (t == Dragon) {
            if (t1 == Dragon) {
                e = Effectiveness.Super.value;
            } else if (t1 == Steel) {
                e = Effectiveness.Not.value;
            } else if (t1 == Fairy) {
                e = Effectiveness.None.value;
            }
        } else if (t == Dark) {
            if (t1 == Psychic || t1 == Ghost) {
                e = Effectiveness.Super.value;
            } else if (t1 == Fighting || t1 == Dark || t1 == Fairy) {
                e = Effectiveness.Not.value;
            }
        } else if (t == Steel) {
            if (t1 == Ice || t1 == Rock || t1 == Fairy) {
                e = Effectiveness.Super.value;
            } else if (t1 == Fire || t1 == Water || t1 == Electric || t1 == Steel) {
                e = Effectiveness.Not.value;
            }
        } else if (t == Fairy) {
            if (t1 == Dark || t1 == Dragon || t1 == Fighting) {
                e = Effectiveness.Super.value;
            } else if (t1 == Fire || t1 == Poison || t1 == Steel) {
                e = Effectiveness.Not.value;
            }
        }
        return e;
    }

    public int getIndex() {
        return this.index;
    }

    public ArrayList<EnumType> makeTypeList() {
        ArrayList<EnumType> typeList = new ArrayList<EnumType>(1);
        typeList.add(this);
        return typeList;
    }

    public static boolean hasType(String name) {
        try {
            return EnumType.valueOf(name) != null;
        }
        catch (Exception e) {
            return false;
        }
    }

    public static EnumType getTypeFromStartingString(String name) {
        for (EnumType type : EnumType.values()) {
            if (type.lowerName == null) {
                type.lowerName = type.name().toLowerCase();
            }
            if (!type.lowerName.startsWith(name)) continue;
            return type;
        }
        return null;
    }

    public static List<EnumType> ignoreType(List<EnumType> origTypes, EnumType ignoreType) {
        List<EnumType> newTypes = origTypes.stream().filter(type -> type != ignoreType).collect(Collectors.toList());
        if (newTypes.isEmpty()) {
            newTypes.add(Mystery);
        }
        return newTypes;
    }
}

