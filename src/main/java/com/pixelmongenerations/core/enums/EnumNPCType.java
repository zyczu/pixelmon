/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

public enum EnumNPCType {
    Trainer("Trainer"),
    ChattingNPC("Chat"),
    Relearner("Relearner"),
    Tutor("Tutor"),
    Trader("Trader"),
    Shopkeeper("Shopkeeper"),
    NurseJoy("Nurse"),
    Groomer("Groomer"),
    Ultra("Ultra"),
    Damos("Damos"),
    Sticker("Sticker");

    private String defaultName;

    private EnumNPCType(String defaultName) {
        this.defaultName = defaultName;
    }

    public String getDefaultName() {
        return this.defaultName;
    }

    public static EnumNPCType getFromOrdinal(short ordinal) {
        return EnumNPCType.values()[ordinal];
    }

    public static EnumNPCType getFromOrdinal(int ordinal) {
        return EnumNPCType.values()[ordinal];
    }

    public static EnumNPCType getFromString(String npcType) {
        for (EnumNPCType enumNPCType : EnumNPCType.values()) {
            if (!enumNPCType.toString().equalsIgnoreCase(npcType)) continue;
            return enumNPCType;
        }
        if (npcType.equalsIgnoreCase("chat")) {
            return ChattingNPC;
        }
        return null;
    }
}

