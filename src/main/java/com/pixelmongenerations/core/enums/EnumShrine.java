/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

public enum EnumShrine {
    Articuno,
    Zapdos,
    Moltres;


    public static boolean hasShrine(String name) {
        try {
            return EnumShrine.valueOf(name) != null;
        }
        catch (Exception e) {
            return false;
        }
    }
}

