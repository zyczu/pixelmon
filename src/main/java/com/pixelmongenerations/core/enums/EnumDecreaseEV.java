/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public enum EnumDecreaseEV {
    PomegBerry(StatsType.HP, "pomegberry"),
    KelpsyBerry(StatsType.Attack, "kelpsyberry"),
    QualotBerry(StatsType.Defence, "qualotberry"),
    HondewBerry(StatsType.SpecialAttack, "hondewberry"),
    GrepaBerry(StatsType.SpecialDefence, "grepaberry"),
    TamatoBerry(StatsType.Speed, "tamatoberry");

    public String textureName;
    public StatsType statAffected;

    private EnumDecreaseEV(StatsType statAffected, String textureName) {
        this.statAffected = statAffected;
        this.textureName = textureName;
    }

    public static boolean hasDecreaseEV(String name) {
        try {
            return EnumDecreaseEV.valueOf(name) != null;
        }
        catch (Exception e) {
            return false;
        }
    }
}

