/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.common.item.ICurryRarity;
import com.pixelmongenerations.core.enums.EnumBerryModel;
import com.pixelmongenerations.core.enums.EnumFlavor;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Arrays;
import net.minecraft.block.Block;
import net.minecraft.item.Item;

public enum EnumBerry implements ICurryRarity
{
    Cheri(new int[]{10, 0, 0, 0, 0}, 4, 15, 24, 1.0, 1.0, 3.0, 1, EnumBerryColor.Red, EnumBerryModel.cheriTaller, EnumBerryModel.cheriBloom, EnumBerryModel.cheriBerry, 0.8f, 2, true),
    Chesto(new int[]{0, 10, 0, 0, 0}, 4, 15, 24, 1.0, 1.0, 3.0, 1, EnumBerryColor.Purple, EnumBerryModel.chestoTaller, EnumBerryModel.chestoBloom, EnumBerryModel.chestoBerry),
    Pecha(new int[]{0, 0, 10, 0, 0}, 4, 15, 24, 1.0, 2.0, 3.0, 1, EnumBerryColor.Pink, EnumBerryModel.pechaTaller, EnumBerryModel.pechaBloom, EnumBerryModel.pechaBerry),
    Rawst(new int[]{0, 0, 0, 10, 0}, 4, 15, 24, 1.0, 1.0, 3.0, 1, EnumBerryColor.Green, EnumBerryModel.rawstTaller, EnumBerryModel.rawstBloom, EnumBerryModel.rawstBerry),
    Aspear(new int[]{0, 0, 0, 0, 10}, 4, 15, 24, 1.0, 1.0, 3.0, 1, EnumBerryColor.Yellow, EnumBerryModel.aspearTaller, EnumBerryModel.aspearBloom, EnumBerryModel.aspearBerry),
    Leppa(new int[]{10, 0, 10, 10, 10}, 4, 15, 24, 1.5, 1.5, 3.0, 1, EnumBerryColor.Red, EnumBerryModel.leppaTaller, EnumBerryModel.leppaBloom, EnumBerryModel.leppaBerry),
    Oran(new int[]{10, 10, 0, 10, 10}, 4, 15, 24, 1.0, 2.0, 3.0, 1, EnumBerryColor.Blue, EnumBerryModel.oranTaller, EnumBerryModel.oranBloom, EnumBerryModel.oranBerry),
    Persim(new int[]{10, 10, 10, 0, 10}, 4, 15, 24, 1.0, 1.0, 3.0, 1, EnumBerryColor.Pink, EnumBerryModel.persimTaller, EnumBerryModel.persimBloom, EnumBerryModel.persimBerry),
    Lum(new int[]{10, 10, 10, 10, 0}, 3, 20, 48, 1.2, 0.5, 3.0, 2, EnumBerryColor.Green, EnumBerryModel.lumTaller, EnumBerryModel.lumBloom, EnumBerryModel.lumBerry),
    Sitrus(new int[]{0, 10, 10, 10, 10}, 3, 20, 48, 1.2, 0.5, 3.0, 2, EnumBerryColor.Yellow, EnumBerryModel.sitrusTaller, EnumBerryModel.sitrusBloom, EnumBerryModel.sitrusBerry),
    Figy(new int[]{15, 0, 0, 0, 0}, 3, 15, 24, 1.5, 1.0, 3.0, 2, EnumBerryColor.Red, EnumBerryModel.figyTaller, EnumBerryModel.figyBloom, EnumBerryModel.figyBerry),
    Wiki(new int[]{0, 15, 0, 0, 0}, 3, 15, 24, 1.5, 1.0, 3.0, 2, EnumBerryColor.Purple, EnumBerryModel.wikiTaller, EnumBerryModel.wikiBloom, EnumBerryModel.wikiBerry),
    Mago(new int[]{0, 0, 15, 0, 0}, 3, 15, 24, 1.5, 1.0, 3.0, 2, EnumBerryColor.Pink, EnumBerryModel.magoTaller, EnumBerryModel.magoBloom, EnumBerryModel.magoBerry),
    Aguav(new int[]{0, 0, 0, 15, 0}, 3, 15, 24, 1.5, 1.0, 3.0, 2, EnumBerryColor.Green, EnumBerryModel.aguavTaller, EnumBerryModel.aguavBloom, EnumBerryModel.aguavBerry),
    Iapapa(new int[]{0, 0, 0, 0, 15}, 3, 15, 24, 1.5, 1.0, 3.0, 2, EnumBerryColor.Yellow, EnumBerryModel.iapapaTaller, EnumBerryModel.iapapaBloom, EnumBerryModel.iapapaBerry),
    Razz(new int[]{10, 10, 0, 0, 0}, 3, 15, 24, 1.0, 1.0, 3.0, 4, EnumBerryColor.Red, EnumBerryModel.razzTaller, EnumBerryModel.razzBloom, EnumBerryModel.razzBerry),
    Bluk(new int[]{0, 10, 10, 0, 0}, 3, 15, 24, 1.0, 1.0, 3.0, 4, EnumBerryColor.Purple, EnumBerryModel.blukTaller, EnumBerryModel.blukBloom, EnumBerryModel.blukBerry),
    Nanab(new int[]{0, 0, 10, 10, 0}, 3, 15, 24, 1.0, 1.0, 3.0, 0, EnumBerryColor.Pink, EnumBerryModel.nanabTaller, EnumBerryModel.nanabBloom, EnumBerryModel.nanabBerry),
    Wepear(new int[]{0, 0, 0, 10, 10}, 3, 15, 24, 1.0, 1.0, 3.0, 0, EnumBerryColor.Green, EnumBerryModel.wepearTaller, EnumBerryModel.wepearBloom, EnumBerryModel.wepearBerry),
    Pinap(new int[]{10, 0, 0, 0, 10}, 3, 15, 24, 1.0, 1.0, 3.0, 0, EnumBerryColor.Yellow, EnumBerryModel.pinapTaller, EnumBerryModel.pinapBloom, EnumBerryModel.pinapBerry),
    Pomeg(new int[]{10, 0, 10, 10, 0}, 1, 20, 48, 0.5, 1.5, 3.0, 0, EnumBerryColor.Red, EnumBerryModel.pomegTaller, EnumBerryModel.pomegBloom, EnumBerryModel.pomegBerry, 0.23f, 1, true),
    Kelpsy(new int[]{0, 10, 0, 10, 10}, 1, 20, 48, 0.5, 1.5, 3.0, 0, EnumBerryColor.Blue, EnumBerryModel.kelpsyTaller, EnumBerryModel.kelpsyBloom, EnumBerryModel.kelpsyBerry, 0.3f, 1, true),
    Qualot(new int[]{10, 0, 10, 0, 10}, 1, 20, 48, 0.5, 1.5, 3.0, 0, EnumBerryColor.Yellow, EnumBerryModel.qualotTaller, EnumBerryModel.qualotBloom, EnumBerryModel.qualotBerry, 0.27f, 1, true),
    Hondew(new int[]{10, 10, 0, 10, 0}, 1, 20, 48, 0.5, 1.5, 3.0, 0, EnumBerryColor.Green, EnumBerryModel.hondewTaller, EnumBerryModel.hondewBloom, EnumBerryModel.hondewBerry, 0.3f, 2, true),
    Grepa(new int[]{0, 10, 10, 0, 10}, 1, 20, 48, 0.5, 1.5, 3.0, 0, EnumBerryColor.Yellow, EnumBerryModel.grepaTaller, EnumBerryModel.grepaBloom, EnumBerryModel.grepaBerry, 0.25f, 2, true),
    Tamato(new int[]{20, 10, 0, 0, 0}, 1, 20, 48, 0.5, 1.5, 3.0, 0, EnumBerryColor.Red, EnumBerryModel.tamatoTaller, EnumBerryModel.tamatoBloom, EnumBerryModel.tamatoBerry, 0.27f, 2, true),
    Cornn(new int[]{0, 20, 10, 0, 0}, 3, 15, 24, 1.0, 1.0, 3.0, 0, EnumBerryColor.Purple, EnumBerryModel.cornnTaller, EnumBerryModel.cornnBloom, EnumBerryModel.cornnBerry),
    Magost(new int[]{0, 0, 20, 10, 0}, 3, 15, 24, 1.0, 1.0, 3.0, 0, EnumBerryColor.Pink, EnumBerryModel.magostTaller, EnumBerryModel.magostBloom, EnumBerryModel.magostBerry),
    Rabuta(new int[]{0, 0, 0, 20, 10}, 3, 15, 24, 1.0, 1.0, 3.0, 0, EnumBerryColor.Green, EnumBerryModel.rabutaTaller, EnumBerryModel.rabutaBloom, EnumBerryModel.rabutaBerry),
    Nomel(new int[]{10, 0, 0, 0, 20}, 3, 15, 24, 1.0, 1.0, 3.0, 0, EnumBerryColor.Yellow, EnumBerryModel.nomelTaller, EnumBerryModel.nomelBloom, EnumBerryModel.nomelBerry),
    Spelon(new int[]{30, 10, 0, 0, 0}, 3, 15, 24, 1.0, 1.0, 3.0, 0, EnumBerryColor.Red, EnumBerryModel.spelonTaller, EnumBerryModel.spelonBloom, EnumBerryModel.spelonBerry),
    Pamtre(new int[]{0, 30, 10, 0, 0}, 3, 15, 24, 1.0, 1.0, 3.0, 0, EnumBerryColor.Purple, EnumBerryModel.pamtreTaller, EnumBerryModel.pamtreBloom, EnumBerryModel.pamtreBerry),
    Watmel(new int[]{0, 0, 30, 10, 0}, 3, 15, 24, 1.0, 1.0, 3.0, 0, EnumBerryColor.Pink, EnumBerryModel.watmelTaller, EnumBerryModel.watmelBloom, EnumBerryModel.watmelBerry),
    Durin(new int[]{0, 0, 0, 30, 10}, 3, 15, 24, 1.0, 1.0, 3.0, 0, EnumBerryColor.Green, EnumBerryModel.durinTaller, EnumBerryModel.durinBloom, EnumBerryModel.durinBerry),
    Belue(new int[]{10, 0, 0, 0, 30}, 3, 15, 24, 1.0, 1.0, 3.0, 0, EnumBerryColor.Purple, EnumBerryModel.belueTaller, EnumBerryModel.belueBloom, EnumBerryModel.belueBerry),
    Occa(new int[]{15, 0, 10, 0, 0}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Red, EnumBerryModel.occaTaller, EnumBerryModel.occaBloom, EnumBerryModel.occaBerry, 0.3f, 1, true),
    Passho(new int[]{0, 15, 0, 10, 0}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Blue, EnumBerryModel.passhoTaller, EnumBerryModel.passhoBloom, EnumBerryModel.passhoBerry, 0.4f, 1, true),
    Wacan(new int[]{0, 0, 15, 0, 10}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Yellow, EnumBerryModel.wacanTaller, EnumBerryModel.wacanBloom, EnumBerryModel.wacanBerry, 0.24f, 1, true),
    Rindo(new int[]{10, 0, 0, 15, 0}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Yellow, EnumBerryModel.rindoTaller, EnumBerryModel.rindoBloom, EnumBerryModel.rindoBerry, 0.3f, 2, true),
    Yache(new int[]{0, 10, 0, 0, 15}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Blue, EnumBerryModel.yacheTaller, EnumBerryModel.yacheBloom, EnumBerryModel.yacheBerry, 0.2f, 2, true),
    Chople(new int[]{15, 0, 0, 10, 0}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Red, EnumBerryModel.chopleTaller, EnumBerryModel.chopleBloom, EnumBerryModel.chopleBerry, 0.25f, 2, true),
    Kebia(new int[]{0, 15, 0, 0, 10}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Green, EnumBerryModel.kebiaTaller, EnumBerryModel.kebiaBloom, EnumBerryModel.kebiaBerry, 0.3f, 2, true),
    Shuca(new int[]{10, 0, 15, 0, 0}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Yellow, EnumBerryModel.shucaTaller, EnumBerryModel.shucaBloom, EnumBerryModel.shucaBerry, 0.15f, 1, true),
    Coba(new int[]{0, 10, 0, 15, 0}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Blue, EnumBerryModel.cobaTaller, EnumBerryModel.cobaBloom, EnumBerryModel.cobaBerry, 0.1f, 1, true),
    Payapa(new int[]{0, 0, 10, 0, 15}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Purple, EnumBerryModel.payapaTaller, EnumBerryModel.payapaBloom, EnumBerryModel.payapaBerry),
    Tanga(new int[]{20, 0, 0, 0, 10}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Green, EnumBerryModel.tangaTaller, EnumBerryModel.tangaBloom, EnumBerryModel.tangaBerry, 0.27f, 1, true),
    Charti(new int[]{10, 20, 0, 0, 0}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Yellow, EnumBerryModel.chartiTaller, EnumBerryModel.chartiBloom, EnumBerryModel.chartiBerry, 0.3f, 2, true),
    Kasib(new int[]{0, 10, 20, 0, 0}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Purple, EnumBerryModel.kasibTaller, EnumBerryModel.kasibBloom, EnumBerryModel.kasibBerry, 0.3f, 2, true),
    Haban(new int[]{0, 0, 10, 20, 0}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Red, EnumBerryModel.habanTaller, EnumBerryModel.habanBloom, EnumBerryModel.habanBerry, 0.13f, 1, true),
    Colbur(new int[]{0, 0, 0, 10, 20}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Purple, EnumBerryModel.colburTaller, EnumBerryModel.colburBloom, EnumBerryModel.colburBerry, 0.4f, 2, true),
    Babiri(new int[]{25, 10, 0, 0, 0}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Green, EnumBerryModel.babiriTaller, EnumBerryModel.babiriBloom, EnumBerryModel.babiriBerry, 0.25f, 2, true),
    Chilan(new int[]{0, 25, 10, 0, 0}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Yellow, EnumBerryModel.chilanTaller, EnumBerryModel.chilanBloom, EnumBerryModel.chilanBerry),
    Roseli(new int[]{0, 0, 25, 10, 0}, 3, 20, 48, 1.0, 0.5, 2.0, 5, EnumBerryColor.Pink, EnumBerryModel.roseliTaller, EnumBerryModel.roseliBloom, EnumBerryModel.roseliBerry),
    Liechi(new int[]{30, 10, 30, 0, 0}, 1, 10, 96, 0.2, 0.1, 1.0, 10, EnumBerryColor.Red, EnumBerryModel.liechiTaller, EnumBerryModel.liechiBloom, EnumBerryModel.liechiBerry, 0.3f, 2, true),
    Ganlon(new int[]{0, 30, 10, 30, 0}, 1, 10, 96, 0.2, 0.1, 1.0, 10, EnumBerryColor.Purple, EnumBerryModel.ganlonTaller, EnumBerryModel.ganlonBloom, EnumBerryModel.ganlonBerry, 0.27f, 2, true),
    Salac(new int[]{0, 0, 30, 10, 30}, 1, 10, 96, 0.2, 0.1, 1.0, 10, EnumBerryColor.Green, EnumBerryModel.salacTaller, EnumBerryModel.salacBloom, EnumBerryModel.salacBerry, 0.25f, 1, true),
    Petaya(new int[]{30, 0, 0, 30, 10}, 1, 10, 96, 0.2, 0.1, 1.0, 10, EnumBerryColor.Pink, EnumBerryModel.petayaTaller, EnumBerryModel.petayaBloom, EnumBerryModel.petayaBerry, 0.23f, 1, true),
    Apicot(new int[]{10, 30, 0, 0, 30}, 1, 10, 96, 0.2, 0.1, 1.0, 10, EnumBerryColor.Blue, EnumBerryModel.apicotTaller, EnumBerryModel.apicotBloom, EnumBerryModel.apicotBerry, 0.3f, 2, true),
    Lansat(new int[]{30, 10, 30, 10, 30}, 1, 5, 120, 0.1, 0.1, 0.5, 15, EnumBerryColor.Red, EnumBerryModel.lansatTaller, EnumBerryModel.lansatBloom, EnumBerryModel.lansatBerry, 0.3f, 2, true),
    Starf(new int[]{30, 10, 30, 10, 30}, 1, 5, 120, 0.1, 0.1, 0.5, 30, EnumBerryColor.Green, EnumBerryModel.starfTaller, EnumBerryModel.starfBloom, EnumBerryModel.starfBerry, 0.3f, 1, true),
    Pumkin(new int[]{0, 0, 0, 0, 50}, 1, 5, 72, 0.2, 0.1, 0.2, 0, EnumBerryColor.Yellow, EnumBerryModel.pumkinTaller, EnumBerryModel.pumkinBloom, EnumBerryModel.pumkinBerry),
    Drash(new int[]{0, 0, 50, 0, 0}, 1, 5, 72, 0.2, 0.1, 0.2, 0, EnumBerryColor.Pink, EnumBerryModel.drashTaller, EnumBerryModel.drashBloom, EnumBerryModel.drashBerry),
    Eggant(new int[]{0, 50, 0, 0, 0}, 1, 5, 72, 0.2, 0.1, 0.2, 0, EnumBerryColor.Blue, EnumBerryModel.eggantTaller, EnumBerryModel.eggantBloom, EnumBerryModel.eggantBerry),
    Strib(new int[]{20, 0, 0, 20, 0}, 1, 5, 72, 0.2, 0.1, 0.2, 0, EnumBerryColor.Green, EnumBerryModel.stribTaller, EnumBerryModel.stribBloom, EnumBerryModel.stribBerry),
    Nutpea(new int[]{0, 0, 0, 0, 10}, 1, 5, 72, 0.2, 0.1, 0.2, 0, EnumBerryColor.Yellow, EnumBerryModel.nutpeaTaller, EnumBerryModel.nutpeaBloom, EnumBerryModel.nutpeaBerry),
    Ginema(new int[]{0, 20, 0, 0, 20}, 1, 5, 72, 0.2, 0.1, 0.2, 0, EnumBerryColor.Yellow, EnumBerryModel.ginemaTaller, EnumBerryModel.ginemaBloom, EnumBerryModel.ginemaBerry),
    Kuo(new int[]{0, 0, 0, 10, 0}, 1, 5, 72, 0.2, 0.1, 0.2, 0, EnumBerryColor.Green, EnumBerryModel.kuoTaller, EnumBerryModel.kuoBloom, EnumBerryModel.kuoBerry),
    Yago(new int[]{0, 0, 0, 0, 50}, 1, 5, 72, 0.2, 0.1, 0.2, 0, EnumBerryColor.Green, EnumBerryModel.yagoTaller, EnumBerryModel.yagoBloom, EnumBerryModel.yagoBerry),
    Touga(new int[]{50, 0, 0, 0, 0}, 1, 5, 72, 0.2, 0.1, 0.2, 0, EnumBerryColor.Red, EnumBerryModel.tougaTaller, EnumBerryModel.tougaBloom, EnumBerryModel.tougaBerry),
    Niniku(new int[]{10, 0, 0, 0, 0}, 1, 5, 72, 0.2, 0.1, 0.2, 0, EnumBerryColor.Blue, EnumBerryModel.ninikuTaller, EnumBerryModel.ninikuBloom, EnumBerryModel.ninikuBerry),
    Topo(new int[]{0, 0, 20, 0, 20}, 1, 5, 72, 0.2, 0.1, 0.2, 0, EnumBerryColor.Pink, EnumBerryModel.topoTaller, EnumBerryModel.topoBloom, EnumBerryModel.topoBerry),
    Enigma(new int[]{0, 20, 0, 20, 0}, 1, 5, 72, 0.2, 0.1, 0.2, 20, EnumBerryColor.Purple, EnumBerryModel.enigmaTaller, EnumBerryModel.enigmaBloom, EnumBerryModel.enigmaBerry, 0.3f, 2, true),
    Micle(new int[]{0, 40, 10, 0, 0}, 1, 5, 72, 0.2, 0.1, 0.2, 20, EnumBerryColor.Green, EnumBerryModel.micleTaller, EnumBerryModel.micleBloom, EnumBerryModel.micleBerry, 0.25f, 1, true),
    Custap(new int[]{0, 0, 40, 10, 0}, 1, 5, 72, 0.2, 0.1, 0.2, 20, EnumBerryColor.Red, EnumBerryModel.custapTaller, EnumBerryModel.custapBloom, EnumBerryModel.custapBerry, 0.25f, 2, true),
    Jaboca(new int[]{0, 0, 0, 40, 10}, 1, 5, 72, 0.2, 0.1, 0.2, 20, EnumBerryColor.Yellow, EnumBerryModel.jabocaTaller, EnumBerryModel.jabocaBloom, EnumBerryModel.jabocaBerry, 0.35f, 2, true),
    Rowap(new int[]{10, 0, 0, 0, 40}, 1, 5, 72, 0.2, 0.1, 0.2, 20, EnumBerryColor.Blue, EnumBerryModel.rowapTaller, EnumBerryModel.rowapBloom, EnumBerryModel.rowapBerry, 0.17f, 1, true),
    Kee(new int[]{30, 30, 10, 10, 10}, 1, 10, 96, 0.2, 0.1, 1.0, 10, EnumBerryColor.Pink, EnumBerryModel.keeTaller, EnumBerryModel.keeBloom, EnumBerryModel.keeBerry),
    Maranga(new int[]{10, 10, 30, 30, 10}, 1, 10, 96, 0.2, 0.1, 1.0, 10, EnumBerryColor.Blue, EnumBerryModel.marangaTaller, EnumBerryModel.marangaBloom, EnumBerryModel.marangaBerry);

    private final int[] flavors;
    public final byte minYield;
    public final byte maxYield;
    public final byte growthTime;
    public final boolean isImplemented;
    public final float scale;
    public final EnumBerryModel[] models;
    public final int height;
    private static ArrayList<EnumBerry> implemented;
    private final double waterAmount;
    private final double weedingAmount;
    private final double pestRemovalAmount;
    private final int rarity;
    private final EnumBerryColor color;

    public Item getBerry() {
        return Item.getByNameOrId("pixelmon:" + this.name().toLowerCase() + "_berry");
    }

    private EnumBerry(int[] flavors, int minYield, int maxYield, int growthTime, double waterAmount, double weedingAmount, double pestRemovalAmount, int rarity, EnumBerryColor color, EnumBerryModel tallerModel, EnumBerryModel bloomModel, EnumBerryModel berryModel, float scale, int height, boolean isImplemented) {
        this.flavors = flavors;
        this.minYield = (byte)minYield;
        this.maxYield = (byte)maxYield;
        this.growthTime = (byte)growthTime;
        this.waterAmount = waterAmount;
        this.weedingAmount = weedingAmount;
        this.pestRemovalAmount = pestRemovalAmount;
        this.rarity = rarity;
        this.color = color;
        this.models = new EnumBerryModel[]{EnumBerryModel.seeded, EnumBerryModel.sprouted, tallerModel, bloomModel, bloomModel, berryModel};
        this.isImplemented = isImplemented;
        this.scale = scale;
        this.height = height;
    }

    private EnumBerry(int[] flavors, int minYield, int maxYield, int growthTime, double waterAmount, double weedingAmount, double pestRemovalAmount, int rarity, EnumBerryColor color, EnumBerryModel tallerModel, EnumBerryModel bloomModel, EnumBerryModel berryModel) {
        this(flavors, minYield, maxYield, growthTime, waterAmount, weedingAmount, pestRemovalAmount, rarity, color, tallerModel, bloomModel, berryModel, 0.3f, 2, false);
    }

    public Block getTreeBlock() {
        return Block.getBlockFromName("pixelmon:berrytree_" + this.name().toLowerCase());
    }

    public static EnumBerry fromId(int id) {
        try {
            return EnumBerry.values()[id];
        }
        catch (Exception e) {
            return null;
        }
    }

    public static EnumFlavor getDominantFlavor(EnumBerry ... berries) {
        int[] output = new int[5];
        for (EnumBerry berry : berries) {
            for (int j = 0; j < 5; ++j) {
                int n = j;
                output[n] = output[n] + berry.flavors[j];
            }
        }
        if (Arrays.stream(output).distinct().count() <= 1L) {
            return EnumFlavor.None;
        }
        int max = Integer.MIN_VALUE;
        int maxIndex = -1;
        for (int i = 0; i < output.length; ++i) {
            if (max >= output[i]) continue;
            max = output[i];
            maxIndex = i;
        }
        return EnumBerry.getFlavorFromBerryFlavorIndex(maxIndex);
    }

    public static EnumFlavor getFlavorFromBerryFlavorIndex(int value) {
        switch (value) {
            case 0: {
                return EnumFlavor.Spicy;
            }
            case 1: {
                return EnumFlavor.Dry;
            }
            case 2: {
                return EnumFlavor.Sweet;
            }
            case 3: {
                return EnumFlavor.Bitter;
            }
            case 4: {
                return EnumFlavor.Sour;
            }
        }
        return EnumFlavor.None;
    }

    public int getFlavorValue(EnumFlavor flavor) {
        switch (flavor) {
            case Spicy: {
                return this.flavors[0];
            }
            case Dry: {
                return this.flavors[1];
            }
            case Sweet: {
                return this.flavors[2];
            }
            case Bitter: {
                return this.flavors[3];
            }
            case Sour: {
                return this.flavors[4];
            }
        }
        return 0;
    }

    public static EnumBerry getImplementedBerry() {
        if (implemented == null) {
            implemented = new ArrayList();
            for (EnumBerry berry : EnumBerry.values()) {
                if (!berry.isImplemented) continue;
                implemented.add(berry);
            }
        }
        if (implemented.isEmpty()) {
            return null;
        }
        return RandomHelper.getRandomElementFromList(implemented);
    }

    public double getWaterAmount() {
        return this.waterAmount;
    }

    public double getWeedingAmount() {
        return this.weedingAmount;
    }

    public double getPestRemovalAmount() {
        return this.pestRemovalAmount;
    }

    public EnumBerryColor getColor() {
        return this.color;
    }

    @Override
    public int getRarity() {
        return this.rarity;
    }

    public static enum EnumBerryColor {
        Red,
        Blue,
        Purple,
        Green,
        Yellow,
        Pink;

    }
}

