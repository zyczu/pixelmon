/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.heldItems;

public enum EnumIncenses {
    oddIncense,
    rockIncense,
    seaIncense,
    waveIncense,
    roseIncense,
    pureIncense,
    luckIncense,
    laxIncense,
    fullIncense;

}

