/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.common.capabilities.curry.CurryData;
import java.util.function.Consumer;
import net.minecraft.util.text.TextFormatting;

public enum EnumCurryTasteRating {
    Koffing(25, 0.25, false, false, TextFormatting.DARK_RED),
    Wobbuffet(200, 0.5, false, false, TextFormatting.RED),
    Milcery(1000, 1.0, true, false, TextFormatting.WHITE),
    Copperajah(5000, 1.0, true, true, TextFormatting.DARK_GREEN),
    Charizard(10000, 1.0, true, true, TextFormatting.GREEN);

    private final Consumer<CurryData> consumer;
    private final TextFormatting textFormatting;

    private EnumCurryTasteRating(int exp, double healthPercent, boolean healStatus, boolean restorePP, TextFormatting textFormatting) {
        this.textFormatting = textFormatting;
        this.consumer = curryData -> curryData.setExperience(exp).setHealthPercentage(healthPercent).setCanHealStatus(healStatus).setCanRestorePP(restorePP);
    }

    public static EnumCurryTasteRating fromId(int id) {
        try {
            return EnumCurryTasteRating.values()[id];
        }
        catch (Exception e) {
            return Koffing;
        }
    }

    public void configureData(CurryData data) {
        this.consumer.accept(data);
    }

    public String getName() {
        return (Object)((Object)this.textFormatting) + this.name();
    }
}

