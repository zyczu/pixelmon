/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

public enum EnumShinyItem {
    Disabled,
    None,
    ShinyCharm;


    public static EnumShinyItem getFromString(String shinyItemString) {
        for (EnumShinyItem item : EnumShinyItem.values()) {
            if (!item.toString().equalsIgnoreCase(shinyItemString)) continue;
            return item;
        }
        return Disabled;
    }
}

