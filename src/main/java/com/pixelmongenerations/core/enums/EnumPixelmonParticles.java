/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.common.battle.animations.particles.ParticleGastly;
import com.pixelmongenerations.common.battle.animations.particles.ParticleKoffing;
import com.pixelmongenerations.common.battle.animations.particles.ParticlePixelmonFlame;
import com.pixelmongenerations.common.battle.animations.particles.ParticleSmoke;
import net.minecraft.client.particle.Particle;

public enum EnumPixelmonParticles {
    Gastly(ParticleGastly.class),
    Koffing(ParticleKoffing.class),
    Flame(ParticlePixelmonFlame.class),
    Smoke(ParticleSmoke.class);

    public Class<? extends Particle> particleClass;

    private EnumPixelmonParticles(Class<? extends Particle> particleClass) {
        this.particleClass = particleClass;
    }

    public static boolean hasPixelmonParticle(String name) {
        try {
            return EnumPixelmonParticles.valueOf(name) != null;
        }
        catch (Exception e) {
            return false;
        }
    }
}

