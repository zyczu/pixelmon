/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.function.Supplier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public enum EnumMegaPokemon {
    Abomasnow(EnumSpecies.Abomasnow, 1, () -> new Item[]{PixelmonItemsHeld.abomasite}),
    Absol(EnumSpecies.Absol, 1, () -> new Item[]{PixelmonItemsHeld.absolite}),
    Aerodactyl(EnumSpecies.Aerodactyl, 1, () -> new Item[]{PixelmonItemsHeld.aerodactylite}),
    Aggron(EnumSpecies.Aggron, 1, () -> new Item[]{PixelmonItemsHeld.aggronite}),
    Alakazam(EnumSpecies.Alakazam, 1, () -> new Item[]{PixelmonItemsHeld.alakazite}),
    Altaria(EnumSpecies.Altaria, 1, () -> new Item[]{PixelmonItemsHeld.altarianite}),
    Ampharos(EnumSpecies.Ampharos, 1, () -> new Item[]{PixelmonItemsHeld.ampharosite}),
    Audino(EnumSpecies.Audino, 1, () -> new Item[]{PixelmonItemsHeld.audinite}),
    Banette(EnumSpecies.Banette, 1, () -> new Item[]{PixelmonItemsHeld.banettite}),
    Beedrill(EnumSpecies.Beedrill, 1, () -> new Item[]{PixelmonItemsHeld.beedrillite}),
    Blastoise(EnumSpecies.Blastoise, 1, () -> new Item[]{PixelmonItemsHeld.blastoisinite}),
    Blaziken(EnumSpecies.Blaziken, 1, () -> new Item[]{PixelmonItemsHeld.blazikenite}),
    Camerupt(EnumSpecies.Camerupt, 1, () -> new Item[]{PixelmonItemsHeld.cameruptite}),
    Charizard(EnumSpecies.Charizard, 2, () -> new Item[]{PixelmonItemsHeld.charizardite_x, PixelmonItemsHeld.charizardite_y}),
    Diancie(EnumSpecies.Diancie, 1, () -> new Item[]{PixelmonItemsHeld.diancite}),
    Gallade(EnumSpecies.Gallade, 1, () -> new Item[]{PixelmonItemsHeld.galladite}),
    Garchomp(EnumSpecies.Garchomp, 1, () -> new Item[]{PixelmonItemsHeld.garchompite}),
    Gardevoir(EnumSpecies.Gardevoir, 1, () -> new Item[]{PixelmonItemsHeld.gardevoirite}),
    Gengar(EnumSpecies.Gengar, 1, () -> new Item[]{PixelmonItemsHeld.gengarite}),
    Glalie(EnumSpecies.Glalie, 1, () -> new Item[]{PixelmonItemsHeld.glalitite}),
    Gyarados(EnumSpecies.Gyarados, 1, () -> new Item[]{PixelmonItemsHeld.gyaradosite}),
    Heracross(EnumSpecies.Heracross, 1, () -> new Item[]{PixelmonItemsHeld.heracronite}),
    Houndoom(EnumSpecies.Houndoom, 1, () -> new Item[]{PixelmonItemsHeld.houndoomite}),
    Kangaskhan(EnumSpecies.Kangaskhan, 1, () -> new Item[]{PixelmonItemsHeld.kangaskhanite}),
    Latias(EnumSpecies.Latias, 1, () -> new Item[]{PixelmonItemsHeld.latiasite}),
    Latios(EnumSpecies.Latios, 1, () -> new Item[]{PixelmonItemsHeld.latiosite}),
    Lopunny(EnumSpecies.Lopunny, 1, () -> new Item[]{PixelmonItemsHeld.lopunnite}),
    Lucario(EnumSpecies.Lucario, 1, () -> new Item[]{PixelmonItemsHeld.lucarionite}),
    Manectric(EnumSpecies.Manectric, 1, () -> new Item[]{PixelmonItemsHeld.manectite}),
    Mawile(EnumSpecies.Mawile, 1, () -> new Item[]{PixelmonItemsHeld.mawilite}),
    Medicham(EnumSpecies.Medicham, 1, () -> new Item[]{PixelmonItemsHeld.medichamite}),
    Metagross(EnumSpecies.Metagross, 1, () -> new Item[]{PixelmonItemsHeld.metagrossite}),
    Mewtwo(EnumSpecies.Mewtwo, 2, () -> new Item[]{PixelmonItemsHeld.mewtwonite_x, PixelmonItemsHeld.mewtwonite_y}),
    Pidgeot(EnumSpecies.Pidgeot, 1, () -> new Item[]{PixelmonItemsHeld.pidgeotite}),
    Pinsir(EnumSpecies.Pinsir, 1, () -> new Item[]{PixelmonItemsHeld.pinsirite}),
    Rayquaza(EnumSpecies.Rayquaza, 1, () -> new Item[0]),
    Sableye(EnumSpecies.Sableye, 1, () -> new Item[]{PixelmonItemsHeld.sablenite}),
    Salamence(EnumSpecies.Salamence, 1, () -> new Item[]{PixelmonItemsHeld.salamencite}),
    Sceptile(EnumSpecies.Sceptile, 1, () -> new Item[]{PixelmonItemsHeld.sceptilite}),
    Scizor(EnumSpecies.Scizor, 1, () -> new Item[]{PixelmonItemsHeld.scizorite}),
    Sharpedo(EnumSpecies.Sharpedo, 1, () -> new Item[]{PixelmonItemsHeld.sharpedonite}),
    Slowbro(EnumSpecies.Slowbro, 1, () -> new Item[]{PixelmonItemsHeld.slowbronite}),
    Steelix(EnumSpecies.Steelix, 1, () -> new Item[]{PixelmonItemsHeld.steelixite}),
    Swampert(EnumSpecies.Swampert, 1, () -> new Item[]{PixelmonItemsHeld.swampertite}),
    Tyranitar(EnumSpecies.Tyranitar, 1, () -> new Item[]{PixelmonItemsHeld.tyranitarite}),
    Venusaur(EnumSpecies.Venusaur, 1, () -> new Item[]{PixelmonItemsHeld.venusaurite});

    public EnumSpecies pokemon;
    public int numMegaForms;
    private Supplier<Item[]> megaEvoItems;
    private Item[] megaDropItems;

    private EnumMegaPokemon(EnumSpecies pokemon, int numMegaForms, Supplier<Item[]> megaEvoItems) {
        this.pokemon = pokemon;
        this.numMegaForms = numMegaForms;
        this.megaEvoItems = megaEvoItems;
    }

    public static EnumMegaPokemon getMega(EnumSpecies pokemon) {
        for (EnumMegaPokemon enumMegaPokemon : EnumMegaPokemon.values()) {
            if (enumMegaPokemon.pokemon != pokemon) continue;
            return enumMegaPokemon;
        }
        return null;
    }

    public Item[] getMegaEvoItems() {
        if (this.megaDropItems == null) {
            this.megaDropItems = this.megaEvoItems.get();
        }
        if (this.megaDropItems.length > 0) {
            return this.megaDropItems;
        }
        return new Item[]{ItemStack.EMPTY.getItem()};
    }
}

