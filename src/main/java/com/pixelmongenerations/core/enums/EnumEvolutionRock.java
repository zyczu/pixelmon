/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import net.minecraft.init.Biomes;
import net.minecraft.world.biome.Biome;

public enum EnumEvolutionRock {
    MossyRock(Biomes.FOREST, Biomes.FOREST_HILLS),
    IcyRock(Biomes.ICE_PLAINS, Biomes.ICE_MOUNTAINS);

    public Biome[] biomes;

    private EnumEvolutionRock(Biome ... biomes) {
        this.biomes = biomes;
    }

    public static EnumEvolutionRock getEvolutionRock(String name) {
        try {
            return EnumEvolutionRock.valueOf(name);
        }
        catch (Exception e) {
            return null;
        }
    }

    public static boolean hasEvolutionRock(String name) {
        try {
            return EnumEvolutionRock.valueOf(name) != null;
        }
        catch (Exception e) {
            return false;
        }
    }
}

