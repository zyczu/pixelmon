/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.awt.Color;
import net.minecraft.util.text.translation.I18n;

public enum EnumBossMode {
    NotBoss(0, -1, Color.WHITE, 1.0f, 0),
    Uncommon(1, -1, Color.GREEN, 1.2f, 5),
    Rare(2, 70, Color.YELLOW, 1.4f, 10),
    Legendary(3, 30, Color.RED, 1.6f, 20),
    Ultimate(4, -1, Color.ORANGE, 1.8f, 40),
    Equal(5, -1, Color.WHITE, 1.0f, 0);

    public int index;
    public int rarity;
    public Color colour;
    public float scaleFactor;
    public int extraLevels;
    public float r;
    public float g;
    public float b;

    private EnumBossMode(int index, int rarity, Color colour, float scaleFactor, int extraLevels) {
        this.index = index;
        this.rarity = rarity;
        this.colour = colour;
        this.scaleFactor = scaleFactor;
        this.extraLevels = extraLevels;
        if (colour == Color.CYAN) {
            this.r = 0.5f;
            this.g = 1.0f;
            this.b = 1.0f;
        } else if (colour == Color.GREEN) {
            this.r = 0.5f;
            this.g = 1.0f;
            this.b = 0.5f;
        } else if (colour == Color.RED) {
            this.r = 1.0f;
            this.g = 0.5f;
            this.b = 0.5f;
        } else if (colour == Color.YELLOW) {
            this.r = 1.0f;
            this.g = 1.0f;
            this.b = 0.5f;
        } else if (colour == Color.ORANGE) {
            this.r = 1.0f;
            this.g = 0.75f;
            this.b = 0.5f;
        }
    }

    public static EnumBossMode getMode(int index) {
        if (index < 0) {
            index = 0;
        }
        if (index >= EnumBossMode.values().length) {
            index = Legendary.ordinal();
        }
        try {
            return EnumBossMode.values()[index];
        }
        catch (IndexOutOfBoundsException e) {
            return NotBoss;
        }
    }

    public static EnumBossMode getRandomMode() {
        int val = RandomHelper.rand.nextInt(100);
        int total = 0;
        for (EnumBossMode b : EnumBossMode.values()) {
            if (b.rarity == -1 || val > (total += b.rarity)) continue;
            return b;
        }
        return Rare;
    }

    public int getColourInt() {
        if (this.colour == Color.WHITE) {
            return -1;
        }
        if (this.colour == Color.GREEN) {
            return 500000;
        }
        if (this.colour == Color.CYAN) {
            return 100000;
        }
        if (this.colour == Color.RED) {
            return -65280;
        }
        if (this.colour == Color.YELLOW) {
            return -255;
        }
        if (this.colour == Color.ORANGE) {
            return -32768;
        }
        return -1;
    }

    public static EnumBossMode getNextMode(EnumBossMode bossMode) {
        int index = bossMode.ordinal();
        index = index == EnumBossMode.NotBoss.index ? EnumBossMode.Equal.index : (index == EnumBossMode.Equal.index ? EnumBossMode.Uncommon.index : (index == EnumBossMode.Ultimate.index ? EnumBossMode.NotBoss.index : ++index));
        return EnumBossMode.getMode(index);
    }

    public static boolean hasBossMode(String name) {
        for (EnumBossMode mode : EnumBossMode.values()) {
            if (!mode.name().equalsIgnoreCase(name)) continue;
            return true;
        }
        return false;
    }

    public static EnumBossMode getBossMode(String name) {
        for (EnumBossMode mode : EnumBossMode.values()) {
            if (!mode.name().equalsIgnoreCase(name)) continue;
            return mode;
        }
        return null;
    }

    public String getBossText() {
        return I18n.translateToLocal("enum.trainerBoss." + this.toString().toLowerCase());
    }

    public boolean isBossPokemon() {
        return this.extraLevels > 0;
    }

    public String getLocalizedName() {
        return I18n.translateToLocal("enum.trainerBoss." + this.name().toLowerCase());
    }
}

