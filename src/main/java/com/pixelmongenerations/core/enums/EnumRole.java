/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.common.cosmetic.EnumCosmetic;

public enum EnumRole {
    Owner("Owner", EnumCosmetic.values()),
    CoOwner("Co-Owner", EnumCosmetic.values()),
    LeadDeveloper("Lead Developer", EnumCosmetic.values()),
    Developer("Developer", new EnumCosmetic[]{EnumCosmetic.BetaBackpack, EnumCosmetic.BetaCap, EnumCosmetic.BetaSash, EnumCosmetic.DeveloperSash, EnumCosmetic.NitroBackpack, EnumCosmetic.NitroCap, EnumCosmetic.NitroSash}),
    Manager("Manager", EnumCosmetic.values()),
    Staff("Team Member", new EnumCosmetic[]{EnumCosmetic.BetaBackpack, EnumCosmetic.BetaCap, EnumCosmetic.BetaSash}),
    GenerationsWinner("Champion", new EnumCosmetic[0]),
    NitroBooster("Nitro Booster", new EnumCosmetic[]{EnumCosmetic.NitroBackpack, EnumCosmetic.NitroCap, EnumCosmetic.NitroSash}),
    Master("Supporter", new EnumCosmetic[0]),
    None("", new EnumCosmetic[0]);

    public String name;
    public EnumCosmetic[] cosmetics;

    private EnumRole(String name, EnumCosmetic[] cosmetics) {
        this.name = name;
        this.cosmetics = cosmetics;
    }

    public boolean hasCosmetics() {
        return this.cosmetics.length > 0;
    }

    public EnumCosmetic[] getCosmetics() {
        return this.cosmetics;
    }

    public String getName() {
        return this.name;
    }
}

