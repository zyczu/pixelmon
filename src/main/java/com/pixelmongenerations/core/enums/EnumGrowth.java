/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.core.enums;

import com.google.common.collect.Lists;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.List;
import net.minecraft.util.text.translation.I18n;

public enum EnumGrowth {
    Pygmy(0, 0.5f, 5, 1),
    Runt(1, 0.75f, 10, 2),
    Small(2, 0.9f, 20, 3),
    Ordinary(3, 1.0f, 30, 4),
    Huge(4, 1.1f, 20, 5),
    Giant(5, 1.25f, 10, 6),
    Enormous(6, 1.5f, 5, 7),
    Ginormous(7, 1.66f, 0, 8),
    Microscopic(8, 0.33f, 0, 0),
    Alpha(9, 1.82f, 0, 9),
    Noble(10, 2.18f, 0, 10);

    public float scaleValue;
    public int rarity;
    public int index;
    public int scaleOrdinal;
    public String name;
    private static final List<EnumGrowth> properOrder;

    private EnumGrowth(int index, float scaleValue, int rarity, int scaleOrdinal) {
        this.index = index;
        this.scaleValue = scaleValue;
        this.rarity = rarity;
        this.scaleOrdinal = scaleOrdinal;
    }

    public static EnumGrowth getRandomGrowth() {
        int tot = 0;
        int rndm = RandomHelper.rand.nextInt(100);
        for (EnumGrowth g : EnumGrowth.values()) {
            if (rndm >= (tot += g.rarity)) continue;
            return g;
        }
        return Ordinary;
    }

    public static EnumGrowth getGrowthFromIndex(int index) {
        try {
            return EnumGrowth.values()[index];
        }
        catch (Exception npe) {
            return null;
        }
    }

    public static EnumGrowth getNextGrowthRestricted(EnumGrowth g) {
        int index = g.ordinal();
        index = index == 6 ? 0 : ++index;
        for (EnumGrowth v : EnumGrowth.values()) {
            if (v.ordinal() != index) continue;
            return v;
        }
        return null;
    }

    public static EnumGrowth getNextGrowth(EnumGrowth g) {
        int index = g.scaleOrdinal;
        index = index == 8 ? 0 : ++index;
        for (EnumGrowth v : EnumGrowth.values()) {
            if (v.scaleOrdinal != index) continue;
            return v;
        }
        return null;
    }

    public static boolean hasGrowth(String name) {
        for (EnumGrowth growth : EnumGrowth.values()) {
            if (!growth.name().equalsIgnoreCase(name) && !growth.getLocalizedName().equalsIgnoreCase(name)) continue;
            return true;
        }
        return false;
    }

    public static EnumGrowth growthFromString(String name) {
        for (EnumGrowth growth : EnumGrowth.values()) {
            if (!growth.name().equalsIgnoreCase(name) && !growth.getLocalizedName().equalsIgnoreCase(name)) continue;
            return growth;
        }
        return null;
    }

    public static EnumGrowth getGrowthFromScaleOrdinal(Integer scaleOrdinal) {
        for (EnumGrowth v : EnumGrowth.values()) {
            if (v.scaleOrdinal != scaleOrdinal) continue;
            return v;
        }
        return null;
    }

    public static List<EnumGrowth> getProperOrder() {
        return properOrder;
    }

    public String getLocalizedName() {
        return I18n.translateToLocal("enum.growth." + this.toString().toLowerCase());
    }

    public static EnumGrowth getGrowthFromStartingString(String name) {
        for (EnumGrowth growth : EnumGrowth.values()) {
            if (growth.name == null) {
                growth.name = growth.name().toLowerCase();
            }
            if (!growth.name.startsWith(name)) continue;
            return growth;
        }
        return null;
    }

    static {
        properOrder = Lists.newArrayList(Microscopic, Pygmy, Runt, Small, Ordinary, Huge, Giant, Enormous, Ginormous);
    }
}

