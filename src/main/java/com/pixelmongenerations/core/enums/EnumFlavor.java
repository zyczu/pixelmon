/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.awt.Color;
import net.minecraft.util.text.translation.I18n;

public enum EnumFlavor {
    None(StatsType.None, Color.BLACK),
    Spicy(StatsType.Attack, Color.decode("#f08030")),
    Dry(StatsType.SpecialAttack, Color.decode("#6890f0")),
    Sweet(StatsType.Speed, Color.decode("#f85888")),
    Bitter(StatsType.SpecialDefence, Color.decode("#78c850")),
    Sour(StatsType.Defence, Color.decode("#f8d030"));

    private final StatsType type;
    private final Color color;

    private EnumFlavor(StatsType type, Color color) {
        this.type = type;
        this.color = color;
    }

    public StatsType getType() {
        return this.type;
    }

    public String getLocalizedName() {
        return this != None ? I18n.translateToLocal("enum.flavor." + this.toString().toLowerCase()) : "";
    }

    public int getIndex() {
        return this.ordinal();
    }

    public static EnumFlavor geFlavorFromIndex(int index) {
        try {
            return EnumFlavor.values()[index];
        }
        catch (Exception npe) {
            return null;
        }
    }

    public Color getColor() {
        return this.color;
    }
}

