/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.common.item.IEnumItem;
import com.pixelmongenerations.common.item.ItemEvolutionStone;
import com.pixelmongenerations.core.config.PixelmonItems;
import net.minecraft.item.Item;

public enum EnumEvolutionStone implements IEnumItem
{
    Firestone,
    Thunderstone,
    Waterstone,
    Sunstone,
    Leafstone,
    Dawnstone,
    Duskstone,
    Moonstone,
    Shinystone,
    Icestone,
    CrackedPot,
    ChippedPot,
    StarSweet,
    StrawberrySweet,
    BerrySweet,
    CloverSweet,
    FlowerSweet,
    LoveSweet,
    RibbonSweet,
    SweetApple,
    TartApple,
    GalaricaCuff,
    GalaricaWreath,
    BlackAugurite,
    LinkingCord,
    PeatBlock;


    public static EnumEvolutionStone getEvolutionStone(String name) {
        try {
            return EnumEvolutionStone.valueOf(name);
        }
        catch (Exception var2) {
            return null;
        }
    }

    public static boolean hasEvolutionStone(String name) {
        try {
            return EnumEvolutionStone.valueOf(name) != null;
        }
        catch (Exception var2) {
            return false;
        }
    }

    @Override
    public ItemEvolutionStone getItem(int useless) {
        Item result = null;
        switch (this) {
            case Dawnstone: {
                result = PixelmonItems.dawnStone;
                break;
            }
            case Duskstone: {
                result = PixelmonItems.duskStone;
                break;
            }
            case Firestone: {
                result = PixelmonItems.fireStone;
                break;
            }
            case Leafstone: {
                result = PixelmonItems.leafStone;
                break;
            }
            case Moonstone: {
                result = PixelmonItems.moonStone;
                break;
            }
            case Shinystone: {
                result = PixelmonItems.shinyStone;
                break;
            }
            case Sunstone: {
                result = PixelmonItems.sunStone;
                break;
            }
            case Thunderstone: {
                result = PixelmonItems.thunderStone;
                break;
            }
            case Waterstone: {
                result = PixelmonItems.waterStone;
                break;
            }
            case Icestone: {
                result = PixelmonItems.iceStone;
                break;
            }
            case CrackedPot: {
                result = PixelmonItems.crackedPot;
                break;
            }
            case ChippedPot: {
                result = PixelmonItems.chippedPot;
                break;
            }
            case StarSweet: {
                result = PixelmonItems.starSweet;
                break;
            }
            case StrawberrySweet: {
                result = PixelmonItems.strawberrySweet;
                break;
            }
            case BerrySweet: {
                result = PixelmonItems.berrySweet;
                break;
            }
            case CloverSweet: {
                result = PixelmonItems.cloverSweet;
                break;
            }
            case FlowerSweet: {
                result = PixelmonItems.flowerSweet;
                break;
            }
            case LoveSweet: {
                result = PixelmonItems.loveSweet;
                break;
            }
            case RibbonSweet: {
                result = PixelmonItems.ribbonSweet;
                break;
            }
            case SweetApple: {
                result = PixelmonItems.sweetApple;
                break;
            }
            case TartApple: {
                result = PixelmonItems.tartApple;
                break;
            }
            case GalaricaCuff: {
                result = PixelmonItems.galaricaCuff;
                break;
            }
            case GalaricaWreath: {
                result = PixelmonItems.galaricaWreath;
                break;
            }
            case BlackAugurite: {
                result = PixelmonItems.blackAugurite;
                break;
            }
            case LinkingCord: {
                result = PixelmonItems.linkingCord;
                break;
            }
            case PeatBlock: {
                result = PixelmonItems.peatBlock;
            }
        }
        return (ItemEvolutionStone)result;
    }

    @Override
    public int numTypes() {
        return 1;
    }
}

