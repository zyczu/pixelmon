/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumSeason implements IEnumForm
{
    Spring,
    Summer,
    Autumn,
    Winter;


    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }

    @Override
    public String getProperName() {
        return this.name();
    }

    public static EnumSeason getFromIndex(int index) {
        if (index == -1) {
            return Winter;
        }
        try {
            return EnumSeason.values()[index];
        }
        catch (IndexOutOfBoundsException var2) {
            return Winter;
        }
    }
}

