/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumKyogre implements IEnumForm
{
    Normal(0, "normal"),
    Primal(1, "primal");

    private final String suffix;
    private final int form;

    private EnumKyogre(int form, String suffix) {
        this.suffix = suffix;
        this.form = form;
    }

    @Override
    public String getSpriteSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public byte getForm() {
        return (byte)this.form;
    }

    @Override
    public boolean isDefaultForm() {
        return this == Normal;
    }

    @Override
    public boolean isTemporary() {
        return this == Primal;
    }

    @Override
    public IEnumForm getDefaultFromTemporary() {
        return Normal;
    }

    @Override
    public boolean hasNoForm() {
        return true;
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

