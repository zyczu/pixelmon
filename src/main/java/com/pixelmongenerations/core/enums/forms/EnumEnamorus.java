/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumEnamorus implements IEnumForm
{
    Normal(0, ""),
    Therian(1, "-therian");

    private final String suffix;
    private final int form;

    private EnumEnamorus(int form, String suffix) {
        this.suffix = suffix;
        this.form = form;
    }

    @Override
    public String getSpriteSuffix() {
        return this.suffix;
    }

    @Override
    public String getFormSuffix() {
        return this.suffix;
    }

    @Override
    public byte getForm() {
        return (byte)this.form;
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

