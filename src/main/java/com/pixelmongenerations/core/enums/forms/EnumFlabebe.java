/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumFlabebe implements IEnumForm
{
    RED("Red"),
    YELLOW("Yellow"),
    ORANGE("Orange"),
    BLUE("Blue"),
    WHITE("White");

    private String name;

    private EnumFlabebe(String name) {
        this.name = name;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public String getProperName() {
        return this.name;
    }
}

