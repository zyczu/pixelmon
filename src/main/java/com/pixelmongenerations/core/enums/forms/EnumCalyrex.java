/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumCalyrex implements IEnumForm
{
    Normal(0, ""),
    Icerider(1, "icerider"),
    Shadowrider(2, "shadowrider");

    private final String suffix;
    private final int form;

    private EnumCalyrex(int form, String suffix) {
        this.suffix = suffix;
        this.form = form;
    }

    @Override
    public String getSpriteSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public byte getForm() {
        return (byte)this.form;
    }

    @Override
    public boolean isTemporary() {
        return false;
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }

    @Override
    public boolean isDefaultForm() {
        return false;
    }

    public static EnumCalyrex getFromPokemon(String name) {
        if (name.equalsIgnoreCase("Glastrier")) {
            return Icerider;
        }
        return Shadowrider;
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

