/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.EnumForms;

public interface IEnumForm {
    default public String getSpriteSuffix() {
        return this.getFormSuffix();
    }

    public String getFormSuffix();

    public byte getForm();

    default public boolean isTemporary() {
        return false;
    }

    default public IEnumForm getDefaultFromTemporary() {
        return EnumForms.NoForm;
    }

    default public boolean hasNoForm() {
        return false;
    }

    default public boolean isDefaultForm() {
        return false;
    }

    default public String format(String contents) {
        String properName = this.getProperName();
        return !properName.isEmpty() ? properName + " " + contents : contents;
    }

    public String getProperName();
}

