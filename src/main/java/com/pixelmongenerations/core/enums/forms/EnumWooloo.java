/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.core.enums.forms;

import com.google.common.collect.Lists;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;

public enum EnumWooloo implements IEnumForm
{
    Regular(0, "", EnumDyeColor.WHITE),
    Black(1, "Black Dyed", EnumDyeColor.BLACK),
    Blue(2, "Blue Dyed", EnumDyeColor.BLUE),
    Brown(3, "Brown Dyed", EnumDyeColor.BROWN),
    Cyan(4, "Cyan Dyed", EnumDyeColor.CYAN),
    Grey(5, "Grey Dyed", EnumDyeColor.GRAY),
    Green(6, "Green Dyed", EnumDyeColor.GREEN),
    LightBlue(7, "Light Blue Dyed", EnumDyeColor.LIGHT_BLUE),
    LightGrey(8, "Light Grey Dyed", EnumDyeColor.SILVER),
    Lime(9, "Lime Dyed", EnumDyeColor.LIME),
    Magenta(10, "Magenta Dyed", EnumDyeColor.MAGENTA),
    Orange(11, "Orange Dyed", EnumDyeColor.ORANGE),
    Pink(12, "Pink Dyed", EnumDyeColor.PINK),
    Purple(13, "Purple Dyed", EnumDyeColor.PURPLE),
    Red(14, "Red Dyed", EnumDyeColor.RED),
    Yellow(15, "Yellow Dyed", EnumDyeColor.YELLOW),
    Shaved(16, "Shaved", null);

    private final String proper;
    private final int form;
    private final EnumDyeColor color;

    private EnumWooloo(int form, String proper, EnumDyeColor color) {
        this.proper = proper;
        this.form = form;
        this.color = color;
    }

    public static IEnumForm getFormFromDye(EnumDyeColor co) {
        if (co != null) {
            EnumWooloo[] values;
            for (EnumWooloo form : values = EnumWooloo.values()) {
                if (form.color != co) continue;
                return form;
            }
        }
        return Shaved;
    }

    @Override
    public String getSpriteSuffix() {
        return this == Regular ? "" : "-" + this.name().toLowerCase();
    }

    @Override
    public String getFormSuffix() {
        return this == Regular ? "" : "-" + this.name().toLowerCase();
    }

    @Override
    public boolean isDefaultForm() {
        return this == Regular;
    }

    @Override
    public byte getForm() {
        return (byte)this.form;
    }

    @Override
    public boolean isTemporary() {
        return false;
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }

    @Override
    public String getProperName() {
        return this.proper;
    }

    public List<ItemStack> getShearDrop(EnumGrowth growth, boolean shiny) {
        System.out.println(this.color + " -> " + (Object)((Object)growth));
        if (this.color == null) {
            return Lists.newArrayList(ItemStack.EMPTY);
        }
        int amount = 0;
        switch (growth) {
            case Microscopic: 
            case Pygmy: 
            case Runt: {
                amount = 2;
                break;
            }
            case Small: 
            case Ordinary: 
            case Huge: {
                amount = 4;
                break;
            }
            case Giant: 
            case Enormous: 
            case Ginormous: {
                amount = 6;
            }
        }
        return IntStream.range(0, amount).mapToObj(i -> new ItemStack(Blocks.WOOL, 1, this.getColor(shiny).getMetadata())).collect(Collectors.toList());
    }

    public static EnumWooloo getFromName(String suffix) {
        for (EnumWooloo form : EnumWooloo.values()) {
            if (!form.name().equalsIgnoreCase(suffix)) continue;
            return form;
        }
        return null;
    }

    public EnumDyeColor getColor(boolean shiny) {
        if (shiny) {
            if (this == Regular) {
                return EnumDyeColor.BLACK;
            }
            if (this == Black) {
                return EnumDyeColor.WHITE;
            }
        }
        return this.color;
    }
}

