/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;
import net.minecraft.world.biome.Biome;

public enum EnumVivillon implements IEnumForm
{
    ARCHIPELAGO("Archipelago", "minecraft:mushroom_island", "minecraft:mushroom_island_shore", "biomesoplenty:bayou", "biomesoplenty:xeric_shrubland"),
    CONTINENTAL("Continental", "minecraft:mesa", "minecraft:mutated_mesa", "minecraft:mesa_clear_rock", "minecraft:mesa_rock", "minecraft:mutated_mesa_clear_rock", "minecraft:mutated_mesa_rock", "biomesoplenty:boreal_forest", "biomesoplenty:crag", "biomesoplenty:highland", "biomesoplenty:mountain"),
    ELEGANT("Elegant", "biomesoplenty:roofed_forest", "biomesoplenty:mutated_roofed_forest", "biomesoplenty:dead_forest", "biomesoplenty:dead_swamp", "biomesoplenty:pasture"),
    GARDEN("Garden", "minecraft:mutated_birch_forest", "biomesoplenty:birch_forest", "biomesoplenty:birch_forest_hills", "biomesoplenty:mutated_birch_forest_hills", "biomesoplenty:flower_field", "biomesoplenty:lavender_fields", "biomesoplenty:flower_island"),
    HIGHPLAINS("High Plains", "minecraft:redwood_taiga", "minecraft:redwood_taiga_hills", "minecraft:mutated_redwood_taiga", "minecraft:mutated_redwood_taiga_hills", "biomesoplenty:brushland", "biomesoplenty:overgrown_cliffs", "biomesoplenty:mountain_foothills"),
    ICYSNOW("Icy Snow", "minecraft:cold_taiga", "biomesoplenty:cold_taiga_hills", "biomesoplenty:mutated_taiga_cold", "biomesoplenty:alps", "biomesoplenty:alps_foothills", "biomesoplenty:snowy_forest", "biomesoplenty:glacier"),
    JUNGLE("Jungle", "minecraft:jungle_edge", "minecraft:mutated_jungle", "minecraft:mutated_jungle_edge", "minecraft:jungle", "minecraft:jungle_hills", "biomesoplenty:bamboo_forest", "biomesoplenty:eucalyptus_forest", "biomesoplenty:rainforest", "biomesoplenty:redwood_forest", "biomesoplenty:temperate_rainforest"),
    MARINE("Marine", "minecraft:beach", "minecraft:cold_beach", "minecraft:stone_beach", "biomesoplenty:coral_reef", "biomesoplenty:origin_beach", "biomesoplenty:origin_island"),
    MEADOW("Meadow", "minecraft:mutated_forest", "biomesoplenty:cherry_blossom_grove", "biomesoplenty:meadow", "biomesoplenty:mystic_grove"),
    MODERN("Modern", "minecraft:extreme_hills", "minecraft:extreme_hills_edge", "minecraft:mutated_extreme_hills", "minecraft:mutated_extreme_hills_with_trees", "minecraft:extreme_hills_with_trees", "biomesoplenty:fen", "biomesoplenty:prairie", "biomesoplenty:seasonal_forest", "biomesoplenty:shrubland"),
    MONSOON("Monsoon", "minecraft:swampland", "minecraft:mutated_swampland", "biomesoplenty:marsh", "biomesoplenty:moor", "biomesoplenty:tropical_rainforest", "biomesoplenty:wetland"),
    OCEAN("Ocean", "minecraft:ocean", "minecraft:deep_ocean", "biomesoplenty:kelp_forest", "biomesoplenty:gravel_beach", "biomesoplenty:tropical_island"),
    POLAR("Polar", "minecraft:frozen_ocean", "minecraft:frozen_river", "minecraft:ice_mountains", "biomesoplenty:coniferous_forest", "biomesoplenty:shield", "biomesoplenty:snowy_coniferous_forest"),
    RIVER("River", "minecraft:river"),
    SANDSTORM("Sandstorm", "minecraft:desert", "minecraft:desert_hills", "minecraft:mutated_desert", "biomesoplenty:bog", "biomesoplenty:steppe", "biomesoplenty:tundra", "biomesoplenty:wasteland"),
    SAVANNA("Savanna", "minecraft:savanna", "minecraft:savanna_plateau", "minecraft:mutated_savanna", "minecraft:mutated_savanna_rock", "biomesoplenty:grassland", "biomesoplenty:outback", "biomesoplenty:volcanic_island"),
    SUN("Sun", "minecraft:mutated_plains", "biomesoplenty:maple_woods", "biomesoplenty:orchard", "biomesoplenty:oasis"),
    TUNDRA("Tundra", "minecraft:ice_plains", "minecraft:mutated_ice_flats", "biomesoplenty:snowy_tundra", "biomesoplenty:cold_desert"),
    POKEBALL("Pokeball", "biomesoplenty:chaparral", "biomesoplenty:grove"),
    FANCY("Fancy", "minecraft:plains", "biomesoplenty:ominous_woods", "biomesoplenty:sacred_springs", "biomesoplenty:woodland");

    private String name;
    public final String[] biomes;

    private EnumVivillon(String name, String ... biomes) {
        this.name = name;
        this.biomes = biomes;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public String getSpriteSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    public static EnumVivillon findFormForBiome(Biome biome) {
        String biomeId = biome.getRegistryName().toString();
        for (EnumVivillon vivillon : EnumVivillon.values()) {
            for (String evoBiome : vivillon.biomes) {
                if (!evoBiome.equals(biomeId)) continue;
                return vivillon;
            }
        }
        return POKEBALL;
    }

    private static String convert(Biome biome) {
        return biome.getRegistryName().toString();
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }

    @Override
    public String getProperName() {
        return this.name;
    }
}

