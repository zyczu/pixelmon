/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import net.minecraft.item.Item;

public enum EnumPikachu implements IEnumForm
{
    Cosplay(0),
    Rockstar(1),
    Belle(2),
    Popstar(3),
    PhD(4),
    Libre(6);

    int formIndex = -1;

    private EnumPikachu(int formIndex) {
        this.formIndex = formIndex;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.toString().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.formIndex;
    }

    public boolean canCosplay() {
        switch (this) {
            case Cosplay: 
            case Rockstar: 
            case Belle: 
            case Popstar: 
            case PhD: 
            case Libre: {
                return true;
            }
        }
        return false;
    }

    public static EnumPikachu getCosplayForm(Item item) {
        if (item == PixelmonItems.rockStarCostume) {
            return Rockstar;
        }
        if (item == PixelmonItems.belleCostume) {
            return Belle;
        }
        if (item == PixelmonItems.popStarCostume) {
            return Popstar;
        }
        if (item == PixelmonItems.phdCostume) {
            return PhD;
        }
        if (item == PixelmonItems.libreCostume) {
            return Libre;
        }
        return null;
    }

    public Item getCosplayItem() {
        if (this == Rockstar) {
            return PixelmonItems.rockStarCostume;
        }
        if (this == Belle) {
            return PixelmonItems.belleCostume;
        }
        if (this == Popstar) {
            return PixelmonItems.popStarCostume;
        }
        if (this == PhD) {
            return PixelmonItems.phdCostume;
        }
        if (this == Libre) {
            return PixelmonItems.libreCostume;
        }
        return null;
    }

    public Attack getCosplayAttack() {
        if (!this.canCosplay()) {
            return null;
        }
        switch (this) {
            case Cosplay: {
                return DatabaseMoves.getAttack("Thunder Shock");
            }
            case Rockstar: {
                return DatabaseMoves.getAttack("Meteor Mash");
            }
            case Belle: {
                return DatabaseMoves.getAttack("Icicle Crash");
            }
            case Popstar: {
                return DatabaseMoves.getAttack("Draining Kiss");
            }
            case PhD: {
                return DatabaseMoves.getAttack("Electric Terrain");
            }
            case Libre: {
                return DatabaseMoves.getAttack("Flying Press");
            }
        }
        return null;
    }

    @Override
    public boolean hasNoForm() {
        return true;
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

