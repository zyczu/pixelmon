/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumDarmanitan implements IEnumForm
{
    Normal(0, "normal"),
    Zen(1, "zen"),
    GalarZen(11, "galarian-zen");

    private final String suffix;
    private final int form;

    private EnumDarmanitan(int form, String suffix) {
        this.suffix = suffix;
        this.form = form;
    }

    @Override
    public String getSpriteSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public byte getForm() {
        return (byte)this.form;
    }

    @Override
    public boolean isTemporary() {
        return this == Zen || this == GalarZen;
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }

    @Override
    public String getProperName() {
        return this.name();
    }

    @Override
    public IEnumForm getDefaultFromTemporary() {
        if (this == Zen) {
            return Normal;
        }
        if (this == GalarZen) {
            return EnumForms.Galarian;
        }
        return this;
    }
}

