/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumCastform implements IEnumForm
{
    Normal,
    Ice("Snowy"),
    Rain("Rainy"),
    Sun("Sunny");

    private String altName;

    private EnumCastform() {
        this.altName = this.name();
    }

    private EnumCastform(String altName) {
        this.altName = altName;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    public static EnumCastform getFromIndex(int index) {
        try {
            return EnumCastform.values()[index];
        }
        catch (IndexOutOfBoundsException e) {
            return Normal;
        }
    }

    public static EnumCastform getFromName(String name) {
        for (EnumCastform form : EnumCastform.values()) {
            if (!form.name().equals(name) && !form.altName.equals(name)) continue;
            return form;
        }
        return null;
    }

    @Override
    public boolean isDefaultForm() {
        return this == Normal;
    }

    @Override
    public String getProperName() {
        return this.isDefaultForm() ? "" : this.altName;
    }
}

