/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumMissingNo implements IEnumForm
{
    RedBlue(0, "redblue"),
    Yellow(1, "yellow"),
    Kabutops(2, "kabutops"),
    Aerodactyl(3, "aerodactyl"),
    Ghost(4, "ghost"),
    Fakemon5(5, "fakemon5"),
    Fakemon6(6, "fakemon6"),
    Fakemon7(7, "fakemon7"),
    Fakemon8(8, "fakemon8"),
    Fakemon9(9, "fakemon9"),
    Fakemon10(10, "fakemon10"),
    Fakemon11(11, "fakemon11"),
    Fakemon12(12, "fakemon12"),
    Fakemon13(13, "fakemon13"),
    Fakemon14(14, "fakemon14"),
    Fakemon15(15, "fakemon15"),
    Fakemon16(16, "fakemon16"),
    Fakemon17(17, "fakemon17"),
    Fakemon18(18, "fakemon18"),
    Fakemon19(19, "fakemon19"),
    Fakemon20(20, "fakemon20"),
    Fakemon21(21, "fakemon21"),
    Fakemon22(22, "fakemon22"),
    Fakemon23(23, "fakemon23"),
    Fakemon24(24, "fakemon24"),
    Fakemon25(25, "fakemon25"),
    Fakemon26(26, "fakemon26"),
    Fakemon27(27, "fakemon27"),
    Fakemon28(28, "fakemon28"),
    Fakemon29(29, "fakemon29"),
    Fakemon30(30, "fakemon30"),
    Fakemon31(31, "fakemon31"),
    Fakemon32(32, "fakemon32"),
    Fakemon33(33, "fakemon33"),
    Fakemon34(34, "fakemon34"),
    Fakemon35(35, "fakemon35"),
    Fakemon36(36, "fakemon36"),
    Fakemon37(37, "fakemon37"),
    Fakemon38(38, "fakemon38"),
    Fakemon39(39, "fakemon39"),
    Fakemon40(40, "fakemon40"),
    Fakemon41(41, "fakemon41"),
    Fakemon42(42, "fakemon42"),
    Fakemon43(43, "fakemon43"),
    Fakemon44(44, "fakemon44"),
    Fakemon45(45, "fakemon45"),
    Fakemon46(46, "fakemon46"),
    Fakemon47(47, "fakemon47"),
    Fakemon48(48, "fakemon48"),
    Fakemon49(49, "fakemon49"),
    Fakemon50(50, "fakemon50"),
    Fakemon51(51, "fakemon51"),
    Fakemon52(52, "fakemon52"),
    Fakemon53(53, "fakemon53"),
    Fakemon54(54, "fakemon54");

    private final String suffix;
    private final int form;

    private EnumMissingNo(int form, String suffix) {
        this.suffix = suffix;
        this.form = form;
    }

    @Override
    public String getSpriteSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public byte getForm() {
        return (byte)this.form;
    }

    @Override
    public boolean isTemporary() {
        return false;
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

