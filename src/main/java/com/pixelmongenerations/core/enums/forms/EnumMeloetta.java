/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumMeloetta implements IEnumForm
{
    ARIA("Aria"),
    PIROUETTE("Pirouette");

    private String name;

    private EnumMeloetta(String name) {
        this.name = name;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public boolean isDefaultForm() {
        return this == ARIA;
    }

    @Override
    public boolean isTemporary() {
        return this == PIROUETTE;
    }

    @Override
    public IEnumForm getDefaultFromTemporary() {
        return ARIA;
    }

    @Override
    public String getProperName() {
        return this.name;
    }
}

