/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumOricorio implements IEnumForm
{
    Baile("Baile"),
    PomPom("Pom Pom"),
    Pau("Pau"),
    Sensu("Sensu");

    public String spritename = this.toString().toLowerCase();
    private String name;

    private EnumOricorio(String name) {
        this.name = name;
    }

    public static EnumOricorio getFromIndex(int index) {
        try {
            return EnumOricorio.values()[index];
        }
        catch (IndexOutOfBoundsException var2) {
            return Baile;
        }
    }

    public static int getNextIndex(int index) {
        return index >= 0 && index < EnumOricorio.values().length - 1 ? index + 1 : 0;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.spritename;
    }

    @Override
    public boolean isDefaultForm() {
        return this == Baile;
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public String getProperName() {
        return this.name;
    }
}

