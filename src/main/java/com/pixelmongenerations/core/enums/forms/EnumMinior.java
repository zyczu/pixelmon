/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumMinior implements IEnumForm
{
    MeteorRed(0, "meteorred", true),
    MeteorOrange(1, "meteororange", true),
    MeteorYellow(2, "meteoryellow", true),
    MeteorGreen(3, "meteorgreen", true),
    MeteorBlue(4, "meteorblue", true),
    MeteorIndigo(5, "meteorindigo", true),
    MeteorViolet(6, "meteorviolet", true),
    Red(7, "red", false),
    Orange(8, "orange", false),
    Yellow(9, "yellow", false),
    Green(10, "green", false),
    Blue(11, "blue", false),
    Indigo(12, "indigo", false),
    Violet(13, "violet", false);

    private final String suffix;
    private final int form;
    private boolean isMeteor;

    private EnumMinior(int form, String suffix, boolean isMeteor) {
        this.suffix = suffix;
        this.form = form;
        this.isMeteor = isMeteor;
    }

    @Override
    public String getSpriteSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.suffix;
    }

    @Override
    public boolean isDefaultForm() {
        return this == MeteorRed;
    }

    @Override
    public byte getForm() {
        return (byte)this.form;
    }

    @Override
    public boolean isTemporary() {
        return false;
    }

    @Override
    public boolean hasNoForm() {
        return false;
    }

    @Override
    public String getProperName() {
        return this.isMeteor() ? "Meteor" : this.name();
    }

    public boolean isMeteor() {
        return this.isMeteor;
    }
}

