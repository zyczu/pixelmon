/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumPolteageist implements IEnumForm
{
    Phony,
    Antique;


    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    public static EnumPolteageist getFromIndex(int index) {
        try {
            return EnumPolteageist.values()[index];
        }
        catch (IndexOutOfBoundsException e) {
            return Phony;
        }
    }

    public static int getNextIndex(int index) {
        if (index < 0 || index >= EnumPolteageist.values().length - 1) {
            return 0;
        }
        return index + 1;
    }

    @Override
    public boolean isDefaultForm() {
        return this == Phony;
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

