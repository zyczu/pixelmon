/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumShellos implements IEnumForm
{
    East,
    West;


    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    public static EnumShellos getFromIndex(int index) {
        try {
            return EnumShellos.values()[index];
        }
        catch (IndexOutOfBoundsException e) {
            return East;
        }
    }

    @Override
    public String getProperName() {
        return this.name();
    }
}

