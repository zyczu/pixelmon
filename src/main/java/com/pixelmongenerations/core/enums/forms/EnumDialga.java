/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.forms;

import com.pixelmongenerations.core.enums.forms.IEnumForm;

public enum EnumDialga implements IEnumForm
{
    ALTERED("Altered"),
    ORIGIN("Origin");

    private String name;

    private EnumDialga(String name) {
        this.name = name;
    }

    @Override
    public String getFormSuffix() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public byte getForm() {
        return (byte)this.ordinal();
    }

    @Override
    public boolean isDefaultForm() {
        return this == ALTERED;
    }

    @Override
    public String getProperName() {
        return this.name;
    }
}

