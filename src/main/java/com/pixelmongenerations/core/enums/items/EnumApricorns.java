/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.items;

import com.pixelmongenerations.common.item.IEnumItem;
import com.pixelmongenerations.common.item.ItemApricorn;
import com.pixelmongenerations.common.item.ItemApricornCooked;
import com.pixelmongenerations.core.config.PixelmonItemsApricorns;
import net.minecraft.item.Item;

public enum EnumApricorns implements IEnumItem
{
    Black(PixelmonItemsApricorns.apricornBlack, PixelmonItemsApricorns.apricornBlackCooked),
    White(PixelmonItemsApricorns.apricornWhite, PixelmonItemsApricorns.apricornWhiteCooked),
    Pink(PixelmonItemsApricorns.apricornPink, PixelmonItemsApricorns.apricornPinkCooked),
    Green(PixelmonItemsApricorns.apricornGreen, PixelmonItemsApricorns.apricornGreenCooked),
    Blue(PixelmonItemsApricorns.apricornBlue, PixelmonItemsApricorns.apricornBlueCooked),
    Yellow(PixelmonItemsApricorns.apricornYellow, PixelmonItemsApricorns.apricornYellowCooked),
    Red(PixelmonItemsApricorns.apricornRed, PixelmonItemsApricorns.apricornRedCooked);

    public Item apricorn;
    public Item cookedApricorn;

    private EnumApricorns(Item apricorn, Item apricornCooked) {
        this.apricorn = apricorn;
        this.cookedApricorn = apricornCooked;
    }

    public ItemApricorn getApricorn() {
        return (ItemApricorn)this.apricorn;
    }

    public ItemApricornCooked getCookedApricorn() {
        return (ItemApricornCooked)this.cookedApricorn;
    }

    @Override
    public Item getItem(int type) {
        switch (type) {
            case 0: {
                return this.getApricorn();
            }
            case 1: {
                return this.getCookedApricorn();
            }
        }
        return null;
    }

    @Override
    public int numTypes() {
        return 2;
    }

    public static boolean hasApricorn(String apricorn) {
        try {
            return EnumApricorns.valueOf(apricorn) != null;
        }
        catch (Exception e) {
            return false;
        }
    }
}

