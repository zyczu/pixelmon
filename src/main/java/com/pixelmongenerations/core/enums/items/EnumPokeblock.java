/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.items;

import com.pixelmongenerations.common.item.IEnumItem;
import net.minecraft.item.Item;

public enum EnumPokeblock implements IEnumItem
{
    Black_Pokeblock(0, "black_pokeblock"),
    Blue_Pokeblock(1, "blue_pokeblock"),
    Brown_Pokeblock(2, "brown_pokeblock"),
    Green_Pokeblock(3, "green_pokeblock"),
    Pink_Pokeblock(4, "pink_pokeblock"),
    Red_Pokeblock(5, "red_pokeblock"),
    White_Pokeblock(6, "white_pokeblock"),
    Yellow_Pokeblock(7, "yellow_pokeblock");

    private int index;
    private String id;

    private EnumPokeblock(int index, String id) {
        this.index = index;
        this.id = id;
    }

    @Override
    public Item getItem(int var1) {
        return null;
    }

    @Override
    public int numTypes() {
        return 0;
    }
}

