/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.items;

import com.pixelmongenerations.core.config.PixelmonItems;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

public enum EnumBike {
    Red,
    Orange,
    Yellow,
    Green,
    Blue,
    Purple;

    private final ResourceLocation texture = new ResourceLocation("pixelmon:textures/bikes/" + this.name().toLowerCase() + ".png");

    public ResourceLocation getTexture() {
        return this.texture;
    }

    public static EnumBike get(String enumStr) {
        for (EnumBike enumBike : EnumBike.values()) {
            if (!enumBike.name().equalsIgnoreCase(enumStr)) continue;
            return enumBike;
        }
        return Red;
    }

    public Item getItem() {
        switch (this) {
            case Orange: {
                return PixelmonItems.orangeBike;
            }
            case Yellow: {
                return PixelmonItems.yellowBike;
            }
            case Green: {
                return PixelmonItems.greenBike;
            }
            case Blue: {
                return PixelmonItems.blueBike;
            }
            case Purple: {
                return PixelmonItems.purpleBike;
            }
        }
        return PixelmonItems.redBike;
    }
}

