/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.items;

import com.pixelmongenerations.common.item.IEnumItem;
import net.minecraft.item.Item;

public enum EnumBalm implements IEnumItem
{
    Forest_Balm(0, "forest_balm"),
    Marsh_Balm(1, "marsh_balm"),
    Mountain_Balm(2, "mountain_balm"),
    Snow_Balm(3, "snow_balm"),
    Volcano_Balm(4, "volcano_balm");

    private int index;
    private String id;

    private EnumBalm(int index, String id) {
        this.index = index;
        this.id = id;
    }

    @Override
    public Item getItem(int var1) {
        return null;
    }

    @Override
    public int numTypes() {
        return 0;
    }
}

