/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.battle;

public enum EnumBattleEndCause {
    NORMAL,
    FORCE,
    FLEE,
    FORFEIT;

}

