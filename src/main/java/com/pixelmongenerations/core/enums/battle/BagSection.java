/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.battle;

import net.minecraft.util.text.translation.I18n;

public enum BagSection {
    Pokeballs(0, I18n.translateToLocal("gui.choosebag.pokeballs")),
    HP(1, I18n.translateToLocal("gui.choosebag.hppp")),
    BattleItems(2, I18n.translateToLocal("gui.choosebag.battleitems")),
    StatusRestore(3, I18n.translateToLocal("gui.choosebag.statusrestore"));

    public String displayString;
    public int index;

    private BagSection(int index, String displayString) {
        this.index = index;
        this.displayString = displayString;
    }

    public static BagSection getSection(int index) {
        try {
            return BagSection.values()[index];
        }
        catch (Exception npe) {
            return null;
        }
    }

    public static boolean hasBag(String name) {
        try {
            BagSection.valueOf(name);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
}

