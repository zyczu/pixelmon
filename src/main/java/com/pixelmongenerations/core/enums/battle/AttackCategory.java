/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.battle;

import net.minecraft.client.resources.I18n;

public enum AttackCategory {
    Physical("attack.category.physical"),
    Special("attack.category.special"),
    Status("attack.category.status");

    private final String langKey;

    private AttackCategory(String langKey) {
        this.langKey = langKey;
    }

    public String getLocalizedName() {
        return I18n.format(this.langKey, new Object[0]);
    }

    public int getIndex() {
        return this.ordinal();
    }
}

