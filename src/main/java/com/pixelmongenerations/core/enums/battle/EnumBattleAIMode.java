/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.battle;

import com.pixelmongenerations.common.battle.controller.ai.AdvancedAI;
import com.pixelmongenerations.common.battle.controller.ai.AggressiveAI;
import com.pixelmongenerations.common.battle.controller.ai.BattleAIBase;
import com.pixelmongenerations.common.battle.controller.ai.RandomAI;
import com.pixelmongenerations.common.battle.controller.ai.TacticalAI;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import net.minecraft.util.text.translation.I18n;

public enum EnumBattleAIMode {
    Default,
    Random,
    Aggressive,
    Tactical,
    Advanced;


    public static EnumBattleAIMode getFromIndex(int index) {
        try {
            return EnumBattleAIMode.values()[index];
        }
        catch (IndexOutOfBoundsException e) {
            return Random;
        }
    }

    public EnumBattleAIMode getNextMode() {
        return EnumBattleAIMode.values()[(this.ordinal() + 1) % EnumBattleAIMode.values().length];
    }

    public BattleAIBase createAI(BattleParticipant participant) {
        switch (this) {
            case Random: {
                return new RandomAI(participant);
            }
            case Aggressive: {
                return new AggressiveAI(participant);
            }
            case Tactical: {
                return new TacticalAI(participant);
            }
            case Advanced: {
                return new AdvancedAI(participant);
            }
        }
        return new RandomAI(participant);
    }

    public String getLocalizedName() {
        return I18n.translateToLocal("enum.battleAI." + this.toString().toLowerCase());
    }
}

