/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.battle;

public enum EnumBattleStartTypes {
    PUGRASSSINGLE,
    PUGRASSDOUBLE,
    SEAWEED,
    ROCKSMASH,
    DIVE,
    HEADBUTT,
    SWEETSCENT,
    CUSTOMGRASS;


    public static String[] getTypeNames() {
        String[] typeNames = new String[EnumBattleStartTypes.values().length];
        for (int i = 0; i < EnumBattleStartTypes.values().length; ++i) {
            typeNames[i] = EnumBattleStartTypes.values()[i].name();
        }
        return typeNames;
    }
}

