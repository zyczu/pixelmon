/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.battle;

import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public enum EnumBattleItems {
    xAttack(StatsType.Attack, "xattack"),
    xDefence(StatsType.Defence, "xdefence"),
    xSpecialAttack(StatsType.SpecialAttack, "xspecialattack"),
    xSpecialDefence(StatsType.SpecialDefence, "xspecialdefence"),
    xSpeed(StatsType.Speed, "xspeed"),
    xAccuracy(StatsType.Accuracy, "xaccuracy"),
    direHit(StatsType.None, "direhit"),
    guardSpec(StatsType.None, "guardspec"),
    maxMushrooms(StatsType.None, "max_mushrooms"),
    adrenalineOrb(StatsType.None, "adrenaline_orb");

    public StatsType effectType;
    public String textureName;
    private boolean affectsBattle;
    private boolean usableInBattle;

    private EnumBattleItems(StatsType type, String textureName) {
        this.effectType = type;
        this.textureName = textureName;
    }

    private EnumBattleItems() {
        this(true, true);
    }

    private EnumBattleItems(boolean usableInBattle, boolean affectsBattle) {
        this.usableInBattle = usableInBattle;
        this.affectsBattle = affectsBattle;
    }

    public boolean getUsableInBattle() {
        return this.usableInBattle;
    }

    public boolean getAffectsBattle() {
        return this.affectsBattle;
    }

    public static boolean hasBattleItem(String name) {
        try {
            EnumBattleItems.valueOf(name);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
}

