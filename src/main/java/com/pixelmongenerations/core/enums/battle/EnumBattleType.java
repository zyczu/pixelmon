/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums.battle;

import net.minecraft.util.text.translation.I18n;

public enum EnumBattleType {
    Single(1),
    Double(2),
    Triple(3),
    Rotation(3),
    SOS(2);

    public int numPokemon;

    private EnumBattleType(int numPokemon) {
        this.numPokemon = numPokemon;
    }

    public EnumBattleType next() {
        return this == Single ? Double : Single;
    }

    public String getLocalizedName() {
        return I18n.translateToLocal("gui.acceptdeny." + this.toString().toLowerCase());
    }

    public static EnumBattleType getFromOrdinal(int ordinal) {
        EnumBattleType[] values = EnumBattleType.values();
        if (ordinal >= 0 && ordinal < values.length) {
            return values[ordinal];
        }
        return Single;
    }
}

