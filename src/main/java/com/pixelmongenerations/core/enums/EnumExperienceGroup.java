/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import java.util.function.Function;

public enum EnumExperienceGroup {
    Erratic(l -> {
        if (l <= 50) {
            return (100 - l) * (l * l * l) / 50;
        }
        if (l <= 68) {
            return (150 - l) * (l * l * l) / 100;
        }
        if (l <= 98) {
            return (191 - l) * (l * l * l) / 150;
        }
        if (l <= 100) {
            return (160 - l) * (l * l * l) / 100;
        }
        return 60 * (l * l * l) / 100;
    }),
    Fast(l -> (int)(0.8 * (double)l.intValue() * (double)l.intValue() * (double)l.intValue())),
    MediumFast(l -> l * l * l),
    MediumSlow(l -> l == 2 ? 9 : (int)(1.2 * (double)l.intValue() * (double)l.intValue() * (double)l.intValue() - (double)(15 * l * l) + (double)(100 * l) - 140.0)),
    Slow(l -> (int)(1.25 * (double)l.intValue() * (double)l.intValue() * (double)l.intValue())),
    Fluctuating(l -> {
        if (l <= 15) {
            return (l + 73) * (l * l * l) / 150;
        }
        if (l <= 36) {
            return (l + 14) * (l * l * l) / 50;
        }
        return (l + 64) * (l * l * l) / 100;
    });

    private Function<Integer, Integer> expForLevelFunc;

    private EnumExperienceGroup(Function<Integer, Integer> expForLevelFunc) {
        this.expForLevelFunc = expForLevelFunc;
    }

    public static EnumExperienceGroup getExperienceGroup(String s) {
        if (s.equalsIgnoreCase("Erratic")) {
            return Erratic;
        }
        if (s.equalsIgnoreCase("Fast")) {
            return Fast;
        }
        if (s.equalsIgnoreCase("MediumFast")) {
            return MediumFast;
        }
        if (s.equalsIgnoreCase("MediumSlow")) {
            return MediumSlow;
        }
        if (s.equalsIgnoreCase("Slow")) {
            return Slow;
        }
        if (s.equalsIgnoreCase("Fluctuating")) {
            return Fluctuating;
        }
        return null;
    }

    public int getExpForLevel(int level) {
        return this.expForLevelFunc.apply(level);
    }
}

