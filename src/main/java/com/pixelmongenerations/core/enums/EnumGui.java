/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

public enum EnumGui {
    ChoosePokemon,
    ChooseAttack,
    ChooseStarter,
    Dialogue,
    FaintedChoice,
    Healer,
    Pokedex,
    PokeChecker,
    RenamePokemon,
    LearnMove,
    PC,
    Battle,
    LevelUp,
    PokeCheckerStats,
    PokeCheckerMoves,
    Trading,
    Doctor,
    PokemonTrade,
    AcceptDeny,
    Evolution,
    ItemDrops,
    PixelmonSpawner,
    TrainerEditor,
    TradeYesNo,
    ChooseMoveset,
    RanchBlock,
    NPCTrade,
    NPCTraderGui,
    PCNoParty,
    StatueEditor,
    ExtendRanch,
    MechaAnvil,
    InputScreen,
    SelectNPCType,
    NPCChatEditor,
    NPCChat,
    ChooseRelearnMove,
    Relearner,
    TutorEditor,
    ChooseTutor,
    Tutor,
    HealerNurseJoy,
    Shopkeeper,
    ShopkeeperEditor,
    VendingMachine,
    PokemonEditor,
    EditedPlayer,
    MegaItem,
    BattleRulesPlayer,
    BattleRulesFixed,
    TeamSelect,
    Cosmetics,
    PixelmonCustomGrass,
    ShinyItem,
    PokedexInfo,
    RotomCatalog,
    TrashCan,
    CookingPot,
    CurryDex,
    Mail,
    AdventCalendar,
    Feeder,
    MountSelection;


    public Integer getIndex() {
        return this.ordinal();
    }

    public static boolean hasGUI(String name) {
        try {
            return EnumGui.valueOf(name) != null;
        }
        catch (Exception var2) {
            return false;
        }
    }

    public static EnumGui getFromOrdinal(int id) {
        return EnumGui.values()[id];
    }
}

