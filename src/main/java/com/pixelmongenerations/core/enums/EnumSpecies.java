/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ArrayListMultimap
 *  com.google.common.collect.ImmutableList
 *  com.google.common.collect.ListMultimap
 *  com.google.common.collect.Lists
 *  com.google.common.collect.Multimap
 *  com.google.common.collect.MultimapBuilder
 */
package com.pixelmongenerations.core.enums;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumAegislashTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumAerodactylTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumAggronTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumAlolanRaichuTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumArmorTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumAvaluggLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumBeldumLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumBellsproutTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumBidoofTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumBonslyTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumBuizelTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumBulbasaurTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumButterfreeTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumCNYTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumCarracostaTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumCaterpieTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumChanseyTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumCharjabugTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumCharmanderTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumCheongsamTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumChimechoTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumChinglingTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumClefableTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumCramorantTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumCryogonalTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumCufantLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumDiglettTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumDittoTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumDragoniteTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumDreepyLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumEeveeTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumEspeonTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumEternatusTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumFalinksTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumFarfetchdTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumFlabebeTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumFloatzelTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumGibleLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumGolduckTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumGoodraLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumGrowlitheTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumGyradosTexture;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumHoopaTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumHydregonTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumJirachiTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumJumpluffTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumKrabbyTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumLanternTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumLeafeonTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumLillipupTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumLotadLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumLuvdiscTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumLycanrocTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumMagickarpTexture;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumMantykeLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumMeltanTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumMeowthTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumMetapodTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumMewTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumMimeJrTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumMimikyuTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumMunnaTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumMusharnaTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumNoctowlTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumOnixTexture;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumPidgeyLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumPikachuTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumPoliwagLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumPorygonTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumPsyduckTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumPurrloinTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumRookideeLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumSableyeTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumSaddleTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumScalesTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumSharpedoLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumShuckleTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumSimiPanLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumSpringTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumSquirtleTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumSudowoodoTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumSummerTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumSurskitTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumSwabluTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumSwordsOfJusticeTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumTentacruelLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumTorkoalTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumTrapinchLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumTrapinchTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumTrubbishTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumUmbreonTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumValentinesTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumVaporeonTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumVolcaronaLineTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumVulpixTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumWoolooTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumZacianTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumZigzagoonTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumZubatLine;
import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.data.pokemon.PokemonRegistry;
import com.pixelmongenerations.core.enums.EnumMegaPokemon;
import com.pixelmongenerations.core.enums.forms.EnumAegislash;
import com.pixelmongenerations.core.enums.forms.EnumAlcremie;
import com.pixelmongenerations.core.enums.forms.EnumArceus;
import com.pixelmongenerations.core.enums.forms.EnumBasculegion;
import com.pixelmongenerations.core.enums.forms.EnumBasculin;
import com.pixelmongenerations.core.enums.forms.EnumBurmy;
import com.pixelmongenerations.core.enums.forms.EnumCalyrex;
import com.pixelmongenerations.core.enums.forms.EnumCastform;
import com.pixelmongenerations.core.enums.forms.EnumCherrim;
import com.pixelmongenerations.core.enums.forms.EnumDarmanitan;
import com.pixelmongenerations.core.enums.forms.EnumDeoxys;
import com.pixelmongenerations.core.enums.forms.EnumDialga;
import com.pixelmongenerations.core.enums.forms.EnumEiscue;
import com.pixelmongenerations.core.enums.forms.EnumEnamorus;
import com.pixelmongenerations.core.enums.forms.EnumEternatus;
import com.pixelmongenerations.core.enums.forms.EnumFlabebe;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.forms.EnumFurfrou;
import com.pixelmongenerations.core.enums.forms.EnumGastrodon;
import com.pixelmongenerations.core.enums.forms.EnumGenesect;
import com.pixelmongenerations.core.enums.forms.EnumGiratina;
import com.pixelmongenerations.core.enums.forms.EnumGreninja;
import com.pixelmongenerations.core.enums.forms.EnumGroudon;
import com.pixelmongenerations.core.enums.forms.EnumHoopa;
import com.pixelmongenerations.core.enums.forms.EnumIndeedee;
import com.pixelmongenerations.core.enums.forms.EnumKeldeo;
import com.pixelmongenerations.core.enums.forms.EnumKyogre;
import com.pixelmongenerations.core.enums.forms.EnumKyurem;
import com.pixelmongenerations.core.enums.forms.EnumLandorus;
import com.pixelmongenerations.core.enums.forms.EnumLycanroc;
import com.pixelmongenerations.core.enums.forms.EnumMareep;
import com.pixelmongenerations.core.enums.forms.EnumMeloetta;
import com.pixelmongenerations.core.enums.forms.EnumMeowstic;
import com.pixelmongenerations.core.enums.forms.EnumMinior;
import com.pixelmongenerations.core.enums.forms.EnumMissingNo;
import com.pixelmongenerations.core.enums.forms.EnumMorpeko;
import com.pixelmongenerations.core.enums.forms.EnumNecrozma;
import com.pixelmongenerations.core.enums.forms.EnumOricorio;
import com.pixelmongenerations.core.enums.forms.EnumPalkia;
import com.pixelmongenerations.core.enums.forms.EnumPikachu;
import com.pixelmongenerations.core.enums.forms.EnumPolteageist;
import com.pixelmongenerations.core.enums.forms.EnumRotom;
import com.pixelmongenerations.core.enums.forms.EnumSeason;
import com.pixelmongenerations.core.enums.forms.EnumShaymin;
import com.pixelmongenerations.core.enums.forms.EnumShellos;
import com.pixelmongenerations.core.enums.forms.EnumSilvally;
import com.pixelmongenerations.core.enums.forms.EnumSinistea;
import com.pixelmongenerations.core.enums.forms.EnumThundurus;
import com.pixelmongenerations.core.enums.forms.EnumTornadus;
import com.pixelmongenerations.core.enums.forms.EnumToxtricity;
import com.pixelmongenerations.core.enums.forms.EnumUnown;
import com.pixelmongenerations.core.enums.forms.EnumUrshifu;
import com.pixelmongenerations.core.enums.forms.EnumVivillon;
import com.pixelmongenerations.core.enums.forms.EnumWishiwashi;
import com.pixelmongenerations.core.enums.forms.EnumWooloo;
import com.pixelmongenerations.core.enums.forms.EnumWormadam;
import com.pixelmongenerations.core.enums.forms.EnumXerneas;
import com.pixelmongenerations.core.enums.forms.EnumZacian;
import com.pixelmongenerations.core.enums.forms.EnumZamazenta;
import com.pixelmongenerations.core.enums.forms.EnumZygarde;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.translation.I18n;

public enum EnumSpecies {
    MissingNo(0, "MissingNo"),
    Bulbasaur(1, "Bulbasaur"),
    Ivysaur(2, "Ivysaur"),
    Venusaur(3, "Venusaur", true),
    Charmander(4, "Charmander"),
    Charmeleon(5, "Charmeleon"),
    Charizard(6, "Charizard", true),
    Squirtle(7, "Squirtle"),
    Wartortle(8, "Wartortle"),
    Blastoise(9, "Blastoise", true),
    Caterpie(10, "Caterpie"),
    Metapod(11, "Metapod"),
    Butterfree(12, "Butterfree", true),
    Weedle(13, "Weedle"),
    Kakuna(14, "Kakuna"),
    Beedrill(15, "Beedrill"),
    Pidgey(16, "Pidgey"),
    Pidgeotto(17, "Pidgeotto"),
    Pidgeot(18, "Pidgeot"),
    Rattata(19, "Rattata"),
    Raticate(20, "Raticate"),
    Spearow(21, "Spearow"),
    Fearow(22, "Fearow"),
    Ekans(23, "Ekans"),
    Arbok(24, "Arbok"),
    Pikachu(25, "Pikachu", true),
    Raichu(26, "Raichu"),
    Sandshrew(27, "Sandshrew"),
    Sandslash(28, "Sandslash"),
    Nidoranfemale(29, "Nidoranfemale"),
    Nidorina(30, "Nidorina"),
    Nidoqueen(31, "Nidoqueen"),
    Nidoranmale(32, "Nidoranmale"),
    Nidorino(33, "Nidorino"),
    Nidoking(34, "Nidoking"),
    Clefairy(35, "Clefairy"),
    Clefable(36, "Clefable"),
    Vulpix(37, "Vulpix"),
    Ninetales(38, "Ninetales"),
    Jigglypuff(39, "Jigglypuff"),
    Wigglytuff(40, "Wigglytuff"),
    Zubat(41, "Zubat"),
    Golbat(42, "Golbat"),
    Oddish(43, "Oddish"),
    Gloom(44, "Gloom"),
    Vileplume(45, "Vileplume"),
    Paras(46, "Paras"),
    Parasect(47, "Parasect"),
    Venonat(48, "Venonat"),
    Venomoth(49, "Venomoth"),
    Diglett(50, "Diglett"),
    Dugtrio(51, "Dugtrio"),
    Meowth(52, "Meowth", true),
    Persian(53, "Persian"),
    Psyduck(54, "Psyduck"),
    Golduck(55, "Golduck"),
    Mankey(56, "Mankey"),
    Primeape(57, "Primeape"),
    Growlithe(58, "Growlithe"),
    Arcanine(59, "Arcanine"),
    Poliwag(60, "Poliwag"),
    Poliwhirl(61, "Poliwhirl"),
    Poliwrath(62, "Poliwrath"),
    Abra(63, "Abra"),
    Kadabra(64, "Kadabra"),
    Alakazam(65, "Alakazam"),
    Machop(66, "Machop"),
    Machoke(67, "Machoke"),
    Machamp(68, "Machamp", true),
    Bellsprout(69, "Bellsprout"),
    Weepinbell(70, "Weepinbell"),
    Victreebel(71, "Victreebel"),
    Tentacool(72, "Tentacool"),
    Tentacruel(73, "Tentacruel"),
    Geodude(74, "Geodude"),
    Graveler(75, "Graveler"),
    Golem(76, "Golem"),
    Ponyta(77, "Ponyta"),
    Rapidash(78, "Rapidash"),
    Slowpoke(79, "Slowpoke"),
    Slowbro(80, "Slowbro"),
    Magnemite(81, "Magnemite"),
    Magneton(82, "Magneton"),
    Farfetchd(83, "Farfetchd"),
    Doduo(84, "Doduo"),
    Dodrio(85, "Dodrio"),
    Seel(86, "Seel"),
    Dewgong(87, "Dewgong"),
    Grimer(88, "Grimer"),
    Muk(89, "Muk"),
    Shellder(90, "Shellder"),
    Cloyster(91, "Cloyster"),
    Gastly(92, "Gastly"),
    Haunter(93, "Haunter"),
    Gengar(94, "Gengar", true),
    Onix(95, "Onix"),
    Drowzee(96, "Drowzee"),
    Hypno(97, "Hypno"),
    Krabby(98, "Krabby"),
    Kingler(99, "Kingler", true),
    Voltorb(100, "Voltorb"),
    Electrode(101, "Electrode"),
    Exeggcute(102, "Exeggcute"),
    Exeggutor(103, "Exeggutor"),
    Cubone(104, "Cubone"),
    Marowak(105, "Marowak"),
    Hitmonlee(106, "Hitmonlee"),
    Hitmonchan(107, "Hitmonchan"),
    Lickitung(108, "Lickitung"),
    Koffing(109, "Koffing"),
    Weezing(110, "Weezing"),
    Rhyhorn(111, "Rhyhorn"),
    Rhydon(112, "Rhydon"),
    Chansey(113, "Chansey"),
    Tangela(114, "Tangela"),
    Kangaskhan(115, "Kangaskhan"),
    Horsea(116, "Horsea"),
    Seadra(117, "Seadra"),
    Goldeen(118, "Goldeen"),
    Seaking(119, "Seaking"),
    Staryu(120, "Staryu"),
    Starmie(121, "Starmie"),
    MrMime(122, "MrMime"),
    Scyther(123, "Scyther"),
    Jynx(124, "Jynx"),
    Electabuzz(125, "Electabuzz"),
    Magmar(126, "Magmar"),
    Pinsir(127, "Pinsir"),
    Tauros(128, "Tauros"),
    Magikarp(129, "Magikarp"),
    Gyarados(130, "Gyarados"),
    Lapras(131, "Lapras", true),
    Ditto(132, "Ditto"),
    Eevee(133, "Eevee", true),
    Vaporeon(134, "Vaporeon"),
    Jolteon(135, "Jolteon"),
    Flareon(136, "Flareon"),
    Porygon(137, "Porygon"),
    Omanyte(138, "Omanyte"),
    Omastar(139, "Omastar"),
    Kabuto(140, "Kabuto"),
    Kabutops(141, "Kabutops"),
    Aerodactyl(142, "Aerodactyl"),
    Snorlax(143, "Snorlax", true),
    Articuno(144, "Articuno"),
    Zapdos(145, "Zapdos"),
    Moltres(146, "Moltres"),
    Dratini(147, "Dratini"),
    Dragonair(148, "Dragonair"),
    Dragonite(149, "Dragonite"),
    Mewtwo(150, "Mewtwo"),
    Mew(151, "Mew"),
    Chikorita(152, "Chikorita"),
    Bayleef(153, "Bayleef"),
    Meganium(154, "Meganium"),
    Cyndaquil(155, "Cyndaquil"),
    Quilava(156, "Quilava"),
    Typhlosion(157, "Typhlosion"),
    Totodile(158, "Totodile"),
    Croconaw(159, "Croconaw"),
    Feraligatr(160, "Feraligatr"),
    Sentret(161, "Sentret"),
    Furret(162, "Furret"),
    Hoothoot(163, "Hoothoot"),
    Noctowl(164, "Noctowl"),
    Ledyba(165, "Ledyba"),
    Ledian(166, "Ledian"),
    Spinarak(167, "Spinarak"),
    Ariados(168, "Ariados"),
    Crobat(169, "Crobat"),
    Chinchou(170, "Chinchou"),
    Lanturn(171, "Lanturn"),
    Pichu(172, "Pichu"),
    Cleffa(173, "Cleffa"),
    Igglybuff(174, "Igglybuff"),
    Togepi(175, "Togepi"),
    Togetic(176, "Togetic"),
    Natu(177, "Natu"),
    Xatu(178, "Xatu"),
    Mareep(179, "Mareep"),
    Flaaffy(180, "Flaaffy"),
    Ampharos(181, "Ampharos"),
    Bellossom(182, "Bellossom"),
    Marill(183, "Marill"),
    Azumarill(184, "Azumarill"),
    Sudowoodo(185, "Sudowoodo"),
    Politoed(186, "Politoed"),
    Hoppip(187, "Hoppip"),
    Skiploom(188, "Skiploom"),
    Jumpluff(189, "Jumpluff"),
    Aipom(190, "Aipom"),
    Sunkern(191, "Sunkern"),
    Sunflora(192, "Sunflora"),
    Yanma(193, "Yanma"),
    Wooper(194, "Wooper"),
    Quagsire(195, "Quagsire"),
    Espeon(196, "Espeon"),
    Umbreon(197, "Umbreon"),
    Murkrow(198, "Murkrow"),
    Slowking(199, "Slowking"),
    Misdreavus(200, "Misdreavus"),
    Unown(201, "Unown"),
    Wobbuffet(202, "Wobbuffet"),
    Girafarig(203, "Girafarig"),
    Pineco(204, "Pineco"),
    Forretress(205, "Forretress"),
    Dunsparce(206, "Dunsparce"),
    Gligar(207, "Gligar"),
    Steelix(208, "Steelix"),
    Snubbull(209, "Snubbull"),
    Granbull(210, "Granbull"),
    Qwilfish(211, "Qwilfish"),
    Scizor(212, "Scizor"),
    Shuckle(213, "Shuckle"),
    Heracross(214, "Heracross"),
    Sneasel(215, "Sneasel"),
    Teddiursa(216, "Teddiursa"),
    Ursaring(217, "Ursaring"),
    Slugma(218, "Slugma"),
    Magcargo(219, "Magcargo"),
    Swinub(220, "Swinub"),
    Piloswine(221, "Piloswine"),
    Corsola(222, "Corsola"),
    Remoraid(223, "Remoraid"),
    Octillery(224, "Octillery"),
    Delibird(225, "Delibird"),
    Mantine(226, "Mantine"),
    Skarmory(227, "Skarmory"),
    Houndour(228, "Houndour"),
    Houndoom(229, "Houndoom"),
    Kingdra(230, "Kingdra"),
    Phanpy(231, "Phanpy"),
    Donphan(232, "Donphan"),
    Porygon2(233, "Porygon2"),
    Stantler(234, "Stantler"),
    Smeargle(235, "Smeargle"),
    Tyrogue(236, "Tyrogue"),
    Hitmontop(237, "Hitmontop"),
    Smoochum(238, "Smoochum"),
    Elekid(239, "Elekid"),
    Magby(240, "Magby"),
    Miltank(241, "Miltank"),
    Blissey(242, "Blissey"),
    Raikou(243, "Raikou"),
    Entei(244, "Entei"),
    Suicune(245, "Suicune"),
    Larvitar(246, "Larvitar"),
    Pupitar(247, "Pupitar"),
    Tyranitar(248, "Tyranitar"),
    Lugia(249, "Lugia"),
    Hooh(250, "Ho-Oh"),
    Celebi(251, "Celebi"),
    Treecko(252, "Treecko"),
    Grovyle(253, "Grovyle"),
    Sceptile(254, "Sceptile"),
    Torchic(255, "Torchic"),
    Combusken(256, "Combusken"),
    Blaziken(257, "Blaziken"),
    Mudkip(258, "Mudkip"),
    Marshtomp(259, "Marshtomp"),
    Swampert(260, "Swampert"),
    Poochyena(261, "Poochyena"),
    Mightyena(262, "Mightyena"),
    Zigzagoon(263, "Zigzagoon"),
    Linoone(264, "Linoone"),
    Wurmple(265, "Wurmple"),
    Silcoon(266, "Silcoon"),
    Beautifly(267, "Beautifly"),
    Cascoon(268, "Cascoon"),
    Dustox(269, "Dustox"),
    Lotad(270, "Lotad"),
    Lombre(271, "Lombre"),
    Ludicolo(272, "Ludicolo"),
    Seedot(273, "Seedot"),
    Nuzleaf(274, "Nuzleaf"),
    Shiftry(275, "Shiftry"),
    Taillow(276, "Taillow"),
    Swellow(277, "Swellow"),
    Wingull(278, "Wingull"),
    Pelipper(279, "Pelipper"),
    Ralts(280, "Ralts"),
    Kirlia(281, "Kirlia"),
    Gardevoir(282, "Gardevoir"),
    Surskit(283, "Surskit"),
    Masquerain(284, "Masquerain"),
    Shroomish(285, "Shroomish"),
    Breloom(286, "Breloom"),
    Slakoth(287, "Slakoth"),
    Vigoroth(288, "Vigoroth"),
    Slaking(289, "Slaking"),
    Nincada(290, "Nincada"),
    Ninjask(291, "Ninjask"),
    Shedinja(292, "Shedinja"),
    Whismur(293, "Whismur"),
    Loudred(294, "Loudred"),
    Exploud(295, "Exploud"),
    Makuhita(296, "Makuhita"),
    Hariyama(297, "Hariyama"),
    Azurill(298, "Azurill"),
    Nosepass(299, "Nosepass"),
    Skitty(300, "Skitty"),
    Delcatty(301, "Delcatty"),
    Sableye(302, "Sableye"),
    Mawile(303, "Mawile"),
    Aron(304, "Aron"),
    Lairon(305, "Lairon"),
    Aggron(306, "Aggron"),
    Meditite(307, "Meditite"),
    Medicham(308, "Medicham"),
    Electrike(309, "Electrike"),
    Manectric(310, "Manectric"),
    Plusle(311, "Plusle"),
    Minun(312, "Minun"),
    Volbeat(313, "Volbeat"),
    Illumise(314, "Illumise"),
    Roselia(315, "Roselia"),
    Gulpin(316, "Gulpin"),
    Swalot(317, "Swalot"),
    Carvanha(318, "Carvanha"),
    Sharpedo(319, "Sharpedo"),
    Wailmer(320, "Wailmer"),
    Wailord(321, "Wailord"),
    Numel(322, "Numel"),
    Camerupt(323, "Camerupt"),
    Torkoal(324, "Torkoal"),
    Spoink(325, "Spoink"),
    Grumpig(326, "Grumpig"),
    Spinda(327, "Spinda"),
    Trapinch(328, "Trapinch"),
    Vibrava(329, "Vibrava"),
    Flygon(330, "Flygon"),
    Cacnea(331, "Cacnea"),
    Cacturne(332, "Cacturne"),
    Swablu(333, "Swablu"),
    Altaria(334, "Altaria"),
    Zangoose(335, "Zangoose"),
    Seviper(336, "Seviper"),
    Lunatone(337, "Lunatone"),
    Solrock(338, "Solrock"),
    Barboach(339, "Barboach"),
    Whiscash(340, "Whiscash"),
    Corphish(341, "Corphish"),
    Crawdaunt(342, "Crawdaunt"),
    Baltoy(343, "Baltoy"),
    Claydol(344, "Claydol"),
    Lileep(345, "Lileep"),
    Cradily(346, "Cradily"),
    Anorith(347, "Anorith"),
    Armaldo(348, "Armaldo"),
    Feebas(349, "Feebas"),
    Milotic(350, "Milotic"),
    Castform(351, "Castform"),
    Kecleon(352, "Kecleon"),
    Shuppet(353, "Shuppet"),
    Banette(354, "Banette"),
    Duskull(355, "Duskull"),
    Dusclops(356, "Dusclops"),
    Tropius(357, "Tropius"),
    Chimecho(358, "Chimecho"),
    Absol(359, "Absol"),
    Wynaut(360, "Wynaut"),
    Snorunt(361, "Snorunt"),
    Glalie(362, "Glalie"),
    Spheal(363, "Spheal"),
    Sealeo(364, "Sealeo"),
    Walrein(365, "Walrein"),
    Clamperl(366, "Clamperl"),
    Huntail(367, "Huntail"),
    Gorebyss(368, "Gorebyss"),
    Relicanth(369, "Relicanth"),
    Luvdisc(370, "Luvdisc"),
    Bagon(371, "Bagon"),
    Shelgon(372, "Shelgon"),
    Salamence(373, "Salamence"),
    Beldum(374, "Beldum"),
    Metang(375, "Metang"),
    Metagross(376, "Metagross"),
    Regirock(377, "Regirock"),
    Regice(378, "Regice"),
    Registeel(379, "Registeel"),
    Latias(380, "Latias"),
    Latios(381, "Latios"),
    Kyogre(382, "Kyogre"),
    Groudon(383, "Groudon"),
    Rayquaza(384, "Rayquaza"),
    Jirachi(385, "Jirachi"),
    Deoxys(386, "Deoxys"),
    Turtwig(387, "Turtwig"),
    Grotle(388, "Grotle"),
    Torterra(389, "Torterra"),
    Chimchar(390, "Chimchar"),
    Monferno(391, "Monferno"),
    Infernape(392, "Infernape"),
    Piplup(393, "Piplup"),
    Prinplup(394, "Prinplup"),
    Empoleon(395, "Empoleon"),
    Starly(396, "Starly"),
    Staravia(397, "Staravia"),
    Staraptor(398, "Staraptor"),
    Bidoof(399, "Bidoof"),
    Bibarel(400, "Bibarel"),
    Kricketot(401, "Kricketot"),
    Kricketune(402, "Kricketune"),
    Shinx(403, "Shinx"),
    Luxio(404, "Luxio"),
    Luxray(405, "Luxray"),
    Budew(406, "Budew"),
    Roserade(407, "Roserade"),
    Cranidos(408, "Cranidos"),
    Rampardos(409, "Rampardos"),
    Shieldon(410, "Shieldon"),
    Bastiodon(411, "Bastiodon"),
    Burmy(412, "Burmy"),
    Wormadam(413, "Wormadam"),
    Mothim(414, "Mothim"),
    Combee(415, "Combee"),
    Vespiquen(416, "Vespiquen"),
    Pachirisu(417, "Pachirisu"),
    Buizel(418, "Buizel"),
    Floatzel(419, "Floatzel"),
    Cherubi(420, "Cherubi"),
    Cherrim(421, "Cherrim"),
    Shellos(422, "Shellos"),
    Gastrodon(423, "Gastrodon"),
    Ambipom(424, "Ambipom"),
    Drifloon(425, "Drifloon"),
    Drifblim(426, "Drifblim"),
    Buneary(427, "Buneary"),
    Lopunny(428, "Lopunny"),
    Mismagius(429, "Mismagius"),
    Honchkrow(430, "Honchkrow"),
    Glameow(431, "Glameow"),
    Purugly(432, "Purugly"),
    Chingling(433, "Chingling"),
    Stunky(434, "Stunky"),
    Skuntank(435, "Skuntank"),
    Bronzor(436, "Bronzor"),
    Bronzong(437, "Bronzong"),
    Bonsly(438, "Bonsly"),
    MimeJr(439, "MimeJr"),
    Happiny(440, "Happiny"),
    Chatot(441, "Chatot"),
    Spiritomb(442, "Spiritomb"),
    Gible(443, "Gible"),
    Gabite(444, "Gabite"),
    Garchomp(445, "Garchomp"),
    Munchlax(446, "Munchlax"),
    Riolu(447, "Riolu"),
    Lucario(448, "Lucario"),
    Hippopotas(449, "Hippopotas"),
    Hippowdon(450, "Hippowdon"),
    Skorupi(451, "Skorupi"),
    Drapion(452, "Drapion"),
    Croagunk(453, "Croagunk"),
    Toxicroak(454, "Toxicroak"),
    Carnivine(455, "Carnivine"),
    Finneon(456, "Finneon"),
    Lumineon(457, "Lumineon"),
    Mantyke(458, "Mantyke"),
    Snover(459, "Snover"),
    Abomasnow(460, "Abomasnow"),
    Weavile(461, "Weavile"),
    Magnezone(462, "Magnezone"),
    Lickilicky(463, "Lickilicky"),
    Rhyperior(464, "Rhyperior"),
    Tangrowth(465, "Tangrowth"),
    Electivire(466, "Electivire"),
    Magmortar(467, "Magmortar"),
    Togekiss(468, "Togekiss"),
    Yanmega(469, "Yanmega"),
    Leafeon(470, "Leafeon"),
    Glaceon(471, "Glaceon"),
    Gliscor(472, "Gliscor"),
    Mamoswine(473, "Mamoswine"),
    PorygonZ(474, "Porygon-Z"),
    Gallade(475, "Gallade"),
    Probopass(476, "Probopass"),
    Dusknoir(477, "Dusknoir"),
    Froslass(478, "Froslass"),
    Rotom(479, "Rotom"),
    Uxie(480, "Uxie"),
    Mesprit(481, "Mesprit"),
    Azelf(482, "Azelf"),
    Dialga(483, "Dialga"),
    Palkia(484, "Palkia"),
    Heatran(485, "Heatran"),
    Regigigas(486, "Regigigas"),
    Giratina(487, "Giratina"),
    Cresselia(488, "Cresselia"),
    Phione(489, "Phione"),
    Manaphy(490, "Manaphy"),
    Darkrai(491, "Darkrai"),
    Shaymin(492, "Shaymin"),
    Arceus(493, "Arceus"),
    Victini(494, "Victini"),
    Snivy(495, "Snivy"),
    Servine(496, "Servine"),
    Serperior(497, "Serperior"),
    Tepig(498, "Tepig"),
    Pignite(499, "Pignite"),
    Emboar(500, "Emboar"),
    Oshawott(501, "Oshawott"),
    Dewott(502, "Dewott"),
    Samurott(503, "Samurott"),
    Patrat(504, "Patrat"),
    Watchog(505, "Watchog"),
    Lillipup(506, "Lillipup"),
    Herdier(507, "Herdier"),
    Stoutland(508, "Stoutland"),
    Purrloin(509, "Purrloin"),
    Liepard(510, "Liepard"),
    Pansage(511, "Pansage"),
    Simisage(512, "Simisage"),
    Pansear(513, "Pansear"),
    Simisear(514, "Simisear"),
    Panpour(515, "Panpour"),
    Simipour(516, "Simipour"),
    Munna(517, "Munna"),
    Musharna(518, "Musharna"),
    Pidove(519, "Pidove"),
    Tranquill(520, "Tranquill"),
    Unfezant(521, "Unfezant"),
    Blitzle(522, "Blitzle"),
    Zebstrika(523, "Zebstrika"),
    Roggenrola(524, "Roggenrola"),
    Boldore(525, "Boldore"),
    Gigalith(526, "Gigalith"),
    Woobat(527, "Woobat"),
    Swoobat(528, "Swoobat"),
    Drilbur(529, "Drilbur"),
    Excadrill(530, "Excadrill"),
    Audino(531, "Audino"),
    Timburr(532, "Timburr"),
    Gurdurr(533, "Gurdurr"),
    Conkeldurr(534, "Conkeldurr"),
    Tympole(535, "Tympole"),
    Palpitoad(536, "Palpitoad"),
    Seismitoad(537, "Seismitoad"),
    Throh(538, "Throh"),
    Sawk(539, "Sawk"),
    Sewaddle(540, "Sewaddle"),
    Swadloon(541, "Swadloon"),
    Leavanny(542, "Leavanny"),
    Venipede(543, "Venipede"),
    Whirlipede(544, "Whirlipede"),
    Scolipede(545, "Scolipede"),
    Cottonee(546, "Cottonee"),
    Whimsicott(547, "Whimsicott"),
    Petilil(548, "Petilil"),
    Lilligant(549, "Lilligant"),
    Basculin(550, "Basculin"),
    Sandile(551, "Sandile"),
    Krokorok(552, "Krokorok"),
    Krookodile(553, "Krookodile"),
    Darumaka(554, "Darumaka"),
    Darmanitan(555, "Darmanitan"),
    Maractus(556, "Maractus"),
    Dwebble(557, "Dwebble"),
    Crustle(558, "Crustle"),
    Scraggy(559, "Scraggy"),
    Scrafty(560, "Scrafty"),
    Sigilyph(561, "Sigilyph"),
    Yamask(562, "Yamask"),
    Cofagrigus(563, "Cofagrigus"),
    Tirtouga(564, "Tirtouga"),
    Carracosta(565, "Carracosta"),
    Archen(566, "Archen"),
    Archeops(567, "Archeops"),
    Trubbish(568, "Trubbish"),
    Garbodor(569, "Garbodor", true),
    Zorua(570, "Zorua"),
    Zoroark(571, "Zoroark"),
    Minccino(572, "Minccino"),
    Cinccino(573, "Cinccino"),
    Gothita(574, "Gothita"),
    Gothorita(575, "Gothorita"),
    Gothitelle(576, "Gothitelle"),
    Solosis(577, "Solosis"),
    Duosion(578, "Duosion"),
    Reuniclus(579, "Reuniclus"),
    Ducklett(580, "Ducklett"),
    Swanna(581, "Swanna"),
    Vanillite(582, "Vanillite"),
    Vanillish(583, "Vanillish"),
    Vanilluxe(584, "Vanilluxe"),
    Deerling(585, "Deerling"),
    Sawsbuck(586, "Sawsbuck"),
    Emolga(587, "Emolga"),
    Karrablast(588, "Karrablast"),
    Escavalier(589, "Escavalier"),
    Foongus(590, "Foongus"),
    Amoonguss(591, "Amoonguss"),
    Frillish(592, "Frillish"),
    Jellicent(593, "Jellicent"),
    Alomomola(594, "Alomomola"),
    Joltik(595, "Joltik"),
    Galvantula(596, "Galvantula"),
    Ferroseed(597, "Ferroseed"),
    Ferrothorn(598, "Ferrothorn"),
    Klink(599, "Klink"),
    Klang(600, "Klang"),
    Klinklang(601, "Klinklang"),
    Tynamo(602, "Tynamo"),
    Eelektrik(603, "Eelektrik"),
    Eelektross(604, "Eelektross"),
    Elgyem(605, "Elgyem"),
    Beheeyem(606, "Beheeyem"),
    Litwick(607, "Litwick"),
    Lampent(608, "Lampent"),
    Chandelure(609, "Chandelure"),
    Axew(610, "Axew"),
    Fraxure(611, "Fraxure"),
    Haxorus(612, "Haxorus"),
    Cubchoo(613, "Cubchoo"),
    Beartic(614, "Beartic"),
    Cryogonal(615, "Cryogonal"),
    Shelmet(616, "Shelmet"),
    Accelgor(617, "Accelgor"),
    Stunfisk(618, "Stunfisk"),
    Mienfoo(619, "Mienfoo"),
    Mienshao(620, "Mienshao"),
    Druddigon(621, "Druddigon"),
    Golett(622, "Golett"),
    Golurk(623, "Golurk"),
    Pawniard(624, "Pawniard"),
    Bisharp(625, "Bisharp"),
    Bouffalant(626, "Bouffalant"),
    Rufflet(627, "Rufflet"),
    Braviary(628, "Braviary"),
    Vullaby(629, "Vullaby"),
    Mandibuzz(630, "Mandibuzz"),
    Heatmor(631, "Heatmor"),
    Durant(632, "Durant"),
    Deino(633, "Deino"),
    Zweilous(634, "Zweilous"),
    Hydreigon(635, "Hydreigon"),
    Larvesta(636, "Larvesta"),
    Volcarona(637, "Volcarona"),
    Cobalion(638, "Cobalion"),
    Terrakion(639, "Terrakion"),
    Virizion(640, "Virizion"),
    Tornadus(641, "Tornadus"),
    Thundurus(642, "Thundurus"),
    Reshiram(643, "Reshiram"),
    Zekrom(644, "Zekrom"),
    Landorus(645, "Landorus"),
    Kyurem(646, "Kyurem"),
    Keldeo(647, "Keldeo"),
    Meloetta(648, "Meloetta"),
    Genesect(649, "Genesect"),
    Chespin(650, "Chespin"),
    Quilladin(651, "Quilladin"),
    Chesnaught(652, "Chesnaught"),
    Fennekin(653, "Fennekin"),
    Braixen(654, "Braixen"),
    Delphox(655, "Delphox"),
    Froakie(656, "Froakie"),
    Frogadier(657, "Frogadier"),
    Greninja(658, "Greninja"),
    Bunnelby(659, "Bunnelby"),
    Diggersby(660, "Diggersby"),
    Fletchling(661, "Fletchling"),
    Fletchinder(662, "Fletchinder"),
    Talonflame(663, "Talonflame"),
    Scatterbug(664, "Scatterbug"),
    Spewpa(665, "Spewpa"),
    Vivillon(666, "Vivillon"),
    Litleo(667, "Litleo"),
    Pyroar(668, "Pyroar"),
    Flabebe(669, "Flabebe"),
    Floette(670, "Floette"),
    Florges(671, "Florges"),
    Skiddo(672, "Skiddo"),
    Gogoat(673, "Gogoat"),
    Pancham(674, "Pancham"),
    Pangoro(675, "Pangoro"),
    Furfrou(676, "Furfrou"),
    Espurr(677, "Espurr"),
    Meowstic(678, "Meowstic"),
    Honedge(679, "Honedge"),
    Doublade(680, "Doublade"),
    Aegislash(681, "Aegislash"),
    Spritzee(682, "Spritzee"),
    Aromatisse(683, "Aromatisse"),
    Swirlix(684, "Swirlix"),
    Slurpuff(685, "Slurpuff"),
    Inkay(686, "Inkay"),
    Malamar(687, "Malamar"),
    Binacle(688, "Binacle"),
    Barbaracle(689, "Barbaracle"),
    Skrelp(690, "Skrelp"),
    Dragalge(691, "Dragalge"),
    Clauncher(692, "Clauncher"),
    Clawitzer(693, "Clawitzer"),
    Helioptile(694, "Helioptile"),
    Heliolisk(695, "Heliolisk"),
    Tyrunt(696, "Tyrunt"),
    Tyrantrum(697, "Tyrantrum"),
    Amaura(698, "Amaura"),
    Aurorus(699, "Aurorus"),
    Sylveon(700, "Sylveon"),
    Hawlucha(701, "Hawlucha"),
    Dedenne(702, "Dedenne"),
    Carbink(703, "Carbink"),
    Goomy(704, "Goomy"),
    Sliggoo(705, "Sliggoo"),
    Goodra(706, "Goodra"),
    Klefki(707, "Klefki"),
    Phantump(708, "Phantump"),
    Trevenant(709, "Trevenant"),
    Pumpkaboo(710, "Pumpkaboo"),
    Gourgeist(711, "Gourgeist"),
    Bergmite(712, "Bergmite"),
    Avalugg(713, "Avalugg"),
    Noibat(714, "Noibat"),
    Noivern(715, "Noivern"),
    Xerneas(716, "Xerneas"),
    Yveltal(717, "Yveltal"),
    Zygarde(718, "Zygarde"),
    Diancie(719, "Diancie"),
    Hoopa(720, "Hoopa"),
    Volcanion(721, "Volcanion"),
    Rowlet(722, "Rowlet"),
    Dartrix(723, "Dartrix"),
    Decidueye(724, "Decidueye"),
    Litten(725, "Litten"),
    Torracat(726, "Torracat"),
    Incineroar(727, "Incineroar"),
    Popplio(728, "Popplio"),
    Brionne(729, "Brionne"),
    Primarina(730, "Primarina"),
    Pikipek(731, "Pikipek"),
    Trumbeak(732, "Trumbeak"),
    Toucannon(733, "Toucannon"),
    Yungoos(734, "Yungoos"),
    Gumshoos(735, "Gumshoos"),
    Grubbin(736, "Grubbin"),
    Charjabug(737, "Charjabug"),
    Vikavolt(738, "Vikavolt"),
    Crabrawler(739, "Crabrawler"),
    Crabominable(740, "Crabominable"),
    Oricorio(741, "Oricorio"),
    Cutiefly(742, "Cutiefly"),
    Ribombee(743, "Ribombee"),
    Rockruff(744, "Rockruff"),
    Lycanroc(745, "Lycanroc"),
    Wishiwashi(746, "Wishiwashi"),
    Mareanie(747, "Mareanie"),
    Toxapex(748, "Toxapex"),
    Mudbray(749, "Mudbray"),
    Mudsdale(750, "Mudsdale"),
    Dewpider(751, "Dewpider"),
    Araquanid(752, "Araquanid"),
    Fomantis(753, "Fomantis"),
    Lurantis(754, "Lurantis"),
    Morelull(755, "Morelull"),
    Shiinotic(756, "Shiinotic"),
    Salandit(757, "Salandit"),
    Salazzle(758, "Salazzle"),
    Stufful(759, "Stufful"),
    Bewear(760, "Bewear"),
    Bounsweet(761, "Bounsweet"),
    Steenee(762, "Steenee"),
    Tsareena(763, "Tsareena"),
    Comfey(764, "Comfey"),
    Oranguru(765, "Oranguru"),
    Passimian(766, "Passimian"),
    Wimpod(767, "Wimpod"),
    Golisopod(768, "Golisopod"),
    Sandygast(769, "Sandygast"),
    Palossand(770, "Palossand"),
    Pyukumuku(771, "Pyukumuku"),
    TypeNull(772, "TypeNull"),
    Silvally(773, "Silvally"),
    Minior(774, "Minior"),
    Komala(775, "Komala"),
    Turtonator(776, "Turtonator"),
    Togedemaru(777, "Togedemaru"),
    Mimikyu(778, "Mimikyu"),
    Bruxish(779, "Bruxish"),
    Drampa(780, "Drampa"),
    Dhelmise(781, "Dhelmise"),
    Jangmoo(782, "Jangmo-o"),
    Hakamoo(783, "Hakamo-o"),
    Kommoo(784, "Kommo-o"),
    TapuKoko(785, "TapuKoko"),
    TapuLele(786, "TapuLele"),
    TapuBulu(787, "TapuBulu"),
    TapuFini(788, "TapuFini"),
    Cosmog(789, "Cosmog"),
    Cosmoem(790, "Cosmoem"),
    Solgaleo(791, "Solgaleo"),
    Lunala(792, "Lunala"),
    Nihilego(793, "Nihilego"),
    Buzzwole(794, "Buzzwole"),
    Pheromosa(795, "Pheromosa"),
    Xurkitree(796, "Xurkitree"),
    Celesteela(797, "Celesteela"),
    Kartana(798, "Kartana"),
    Guzzlord(799, "Guzzlord"),
    Necrozma(800, "Necrozma"),
    Magearna(801, "Magearna"),
    Marshadow(802, "Marshadow"),
    Poipole(803, "Poipole"),
    Naganadel(804, "Naganadel"),
    Stakataka(805, "Stakataka"),
    Blacephalon(806, "Blacephalon"),
    Zeraora(807, "Zeraora"),
    Meltan(808, "Meltan"),
    Melmetal(809, "Melmetal", true),
    Grookey(810, "Grookey"),
    Thwackey(811, "Thwackey"),
    Rillaboom(812, "Rillaboom", true),
    Scorbunny(813, "Scorbunny"),
    Raboot(814, "Raboot"),
    Cinderace(815, "Cinderace", true),
    Sobble(816, "Sobble"),
    Drizzile(817, "Drizzile"),
    Inteleon(818, "Inteleon", true),
    Skwovet(819, "Skwovet"),
    Greedent(820, "Greedent"),
    Rookidee(821, "Rookidee"),
    Corvisquire(822, "Corvisquire"),
    Corviknight(823, "Corviknight", true),
    Blipbug(824, "Blipbug"),
    Dottler(825, "Dottler"),
    Orbeetle(826, "Orbeetle", true),
    Nickit(827, "Nickit"),
    Thievul(828, "Thievul"),
    Gossifleur(829, "Gossifleur"),
    Eldegoss(830, "Eldegoss"),
    Wooloo(831, "Wooloo"),
    Dubwool(832, "Dubwool"),
    Chewtle(833, "Chewtle"),
    Drednaw(834, "Drednaw", true),
    Yamper(835, "Yamper"),
    Boltund(836, "Boltund"),
    Rolycoly(837, "Rolycoly"),
    Carkol(838, "Carkol"),
    Coalossal(839, "Coalossal", true),
    Applin(840, "Applin"),
    Flapple(841, "Flapple", true),
    Appletun(842, "Appletun", true),
    Silicobra(843, "Silicobra"),
    Sandaconda(844, "Sandaconda", true),
    Cramorant(845, "Cramorant"),
    Arrokuda(846, "Arrokuda"),
    Barraskewda(847, "Barraskewda"),
    Toxel(848, "Toxel"),
    Toxtricity(849, "Toxtricity", true),
    Sizzlipede(850, "Sizzlipede"),
    Centiskorch(851, "Centiskorch", true),
    Clobbopus(852, "Clobbopus"),
    Grapploct(853, "Grapploct"),
    Sinistea(854, "Sinistea"),
    Polteageist(855, "Polteageist"),
    Hatenna(856, "Hatenna"),
    Hattrem(857, "Hattrem"),
    Hatterene(858, "Hatterene", true),
    Impidimp(859, "Impidimp"),
    Morgrem(860, "Morgrem"),
    Grimmsnarl(861, "Grimmsnarl", true),
    Obstagoon(862, "Obstagoon"),
    Perrserker(863, "Perrserker"),
    Cursola(864, "Cursola"),
    Sirfetchd(865, "Sirfetchd"),
    MrRime(866, "MrRime"),
    Runerigus(867, "Runerigus"),
    Milcery(868, "Milcery"),
    Alcremie(869, "Alcremie", true),
    Falinks(870, "Falinks"),
    Pincurchin(871, "Pincurchin"),
    Snom(872, "Snom"),
    Frosmoth(873, "Frosmoth"),
    Stonjourner(874, "Stonjourner"),
    Eiscue(875, "Eiscue"),
    Indeedee(876, "Indeedee"),
    Morpeko(877, "Morpeko"),
    Cufant(878, "Cufant"),
    Copperajah(879, "Copperajah", true),
    Dracozolt(880, "Dracozolt"),
    Arctozolt(881, "Arctozolt"),
    Dracovish(882, "Dracovish"),
    Arctovish(883, "Arctovish"),
    Duraludon(884, "Duraludon", true),
    Dreepy(885, "Dreepy"),
    Drakloak(886, "Drakloak"),
    Dragapult(887, "Dragapult"),
    Zacian(888, "Zacian"),
    Zamazenta(889, "Zamazenta"),
    Eternatus(890, "Eternatus"),
    Kubfu(891, "Kubfu"),
    Urshifu(892, "Urshifu", true),
    Zarude(893, "Zarude"),
    Regieleki(894, "Regieleki"),
    Regidrago(895, "Regidrago"),
    Glastrier(896, "Glastrier"),
    Spectrier(897, "Spectrier"),
    Calyrex(898, "Calyrex"),
    Wyrdeer(899, "Wyrdeer"),
    Kleavor(900, "Kleavor"),
    Ursaluna(901, "Ursaluna"),
    Basculegion(902, "Basculegion"),
    Sneasler(903, "Sneasler"),
    Overqwil(904, "Overqwil"),
    Enamorus(905, "Enamorus");
    
    private static final EnumSpecies[] VALUES;
    public static List<String> pokemonNameList;
    public static final EnumSpecies[] LEGENDARY_ENUMS;
    public static final EnumSpecies[] ULTRA_BEASTS_ENUMS;
    public static ArrayList<String> legendaries;
    public static ArrayList<String> ultrabeasts;
    public static ArrayList<String> rockSmashEncounters;
    public static ArrayList<String> headbuttEncounters;
    public static ArrayList<EnumSpecies> zombieTextured;
    public static ArrayList<EnumSpecies> mfTextured;
    public static ArrayList<EnumSpecies> mfSprite;
    public static Map<EnumSpecies, Multimap<IEnumForm, IEnumSpecialTexture>> specialTextureMap;
    public static ListMultimap<EnumSpecies, IEnumForm> formList;
    public static Map<Integer, List<EnumSpecies>> generationMap;
    public String name;
    private int nationalDex;
    private boolean gmax;

    private EnumSpecies(int dex, String name) {
        this.nationalDex = dex;
        this.name = name;
    }

    private EnumSpecies(int dex, String name, boolean gmax) {
        this.nationalDex = dex;
        this.name = name;
        this.gmax = gmax;
    }

    public boolean isLegendary() {
        return legendaries.contains(this.name);
    }

    public boolean isUltraBeast() {
        return ultrabeasts.contains(this.name);
    }

    public static boolean hasPokemon(String name) {
        return EnumSpecies.getFromName(name).isPresent();
    }

    @Deprecated
    public static EnumSpecies get(String name) {
        return EnumSpecies.getFromName(name).get();
    }

    public String getPokemonName() {
        return this.name;
    }

    public static boolean hasPokemonAnyCase(String name) {
        return EnumSpecies.getFromNameAnyCase(name) != null;
    }

    public static Optional<EnumSpecies> contains(String containsString) {
        for (EnumSpecies e : VALUES) {
            if (!containsString.replace("-", "").contains(e.name.replace("-", "").toLowerCase())) continue;
            return Optional.of(e);
        }
        return Optional.empty();
    }

    public static EnumSpecies getFromPixelmonId(int id) {
        BaseStats stats = PokemonRegistry.getBaseFor(id);
        return EnumSpecies.getFromName(stats.pixelmonName).orElse(null);
    }

    public static EnumSpecies getFromOrdinal(int ordinal) {
        return ordinal >= 0 && ordinal < VALUES.length ? VALUES[ordinal] : null;
    }

    public static EnumSpecies getFromNameAnyCase(String name) {
        for (EnumSpecies pokemon : VALUES) {
            if (name == null || pokemon.name == null || !name.equalsIgnoreCase(pokemon.name) && !name.equalsIgnoreCase(I18n.translateToLocal("pixelmon." + pokemon.name.toLowerCase() + ".name"))) continue;
            return pokemon;
        }
        return null;
    }

    public static Optional<EnumSpecies> getFromName(String name) {
        try {
            return Optional.of(EnumSpecies.valueOf(name));
        }
        catch (Exception var2) {
            EnumSpecies pokemonEnum = EnumSpecies.getFromNameAnyCase(name);
            if (pokemonEnum == null) {
                return Optional.of(Pikachu);
            }
            return Optional.of(pokemonEnum);
        }
    }

    public static Optional<EnumSpecies> getFromDex(int nationalDex) {
        for (EnumSpecies pokemon : EnumSpecies.values()) {
            if (pokemon.getNationalPokedexInteger() != nationalDex) continue;
            return Optional.of(pokemon);
        }
        System.out.println("ERROR FINDING POKEMON WITH POKEDEX ID: " + nationalDex);
        return Optional.of(Pikachu);
    }

    public static Optional<EnumSpecies> getFromDexUnsafe(int nationalDex) {
        for (EnumSpecies pokemon : EnumSpecies.values()) {
            if (pokemon.getNationalPokedexInteger() != nationalDex) continue;
            return Optional.of(pokemon);
        }
        return Optional.empty();
    }

    public static ImmutableList<String> getNameList() {
        return ImmutableList.copyOf(pokemonNameList);
    }

    public static boolean hasNoFormForm(EnumSpecies pokemonEnum) {
        List<IEnumForm> forms = formList.get(pokemonEnum);
        for (IEnumForm form : forms) {
            if (!form.hasNoForm()) continue;
            return true;
        }
        return PixelmonModelRegistry.getModel(pokemonEnum, EnumForms.NoForm) != null;
    }

    public String getNationalPokedexNumber() {
        return String.format("%03d", this.nationalDex);
    }

    public int getNationalPokedexInteger() {
        return this.nationalDex;
    }

    public boolean hasSpecialTexture() {
        return this.hasSpecialTexture(EnumForms.NoForm);
    }

    public boolean hasSpecialTexture(IEnumForm form) {
        return specialTextureMap.containsKey(this) && specialTextureMap.get(this).containsKey(form);
    }

    public boolean hasSpecialTexture(int specialTexutre) {
        return this.hasSpecialTexture(EnumForms.NoForm, specialTexutre);
    }

    public boolean hasSpecialTexture(IEnumForm form, int specialTexutre) {
        return this.hasSpecialTexture(form) && specialTextureMap.get(this).get(form).stream().map(IEnumSpecialTexture::getId).anyMatch(a -> a == specialTexutre);
    }

    public IEnumForm getFormEnum(int form) {
        if (form != -1 && formList.containsKey(this)) {
            List<IEnumForm> list = formList.get(this);
            for (IEnumForm iEnumForm : list) {
                if (iEnumForm.getForm() != form) continue;
                return iEnumForm;
            }
            return EnumForms.NoForm;
        }
        return EnumForms.NoForm;
    }

    public IEnumForm getFormFromIndex(int index) {
        List forms = formList.get(this);
        return (IEnumForm)forms.get(MathHelper.clamp(index, 0, forms.size()));
    }

    public ArrayList<Integer> getAllForms() {
        ArrayList<Integer> forms = new ArrayList<Integer>();
        List<IEnumForm> list = formList.get(this);
        if (!list.isEmpty()) {
            list.forEach(form -> forms.add(Integer.valueOf(form.getForm())));
        }
        return forms;
    }

    public int getNumForms(boolean temporary) {
        int forms = 0;
        if (formList.containsKey(this)) {
            List<IEnumForm> list = formList.get(this);
            for (IEnumForm iEnumForm : list) {
                if (iEnumForm.isTemporary() && !temporary) continue;
                ++forms;
            }
        }
        return forms;
    }

    public IEnumSpecialTexture getSpecialTexture() {
        return this.getSpecialTexture(EnumForms.NoForm, 1);
    }

    public IEnumSpecialTexture getSpecialTexture(IEnumForm form, int index) {
        return this.hasSpecialTexture(form, index) ? specialTextureMap.get(this).get(form).stream().filter(a -> a.getId() == index).findAny().orElse(EnumTextures.None) : EnumTextures.None;
    }

    public boolean hasMega() {
        return EnumMegaPokemon.getMega(this) != null;
    }

    public static void registerSpecialTexture(EnumSpecies pokemon, IEnumSpecialTexture texture) {
        EnumSpecies.registerSpecialTexture(pokemon, EnumForms.NoForm, texture);
    }

    public static void registerSpecialTexture(EnumSpecies pokemon, IEnumForm form, IEnumSpecialTexture texture) {
        specialTextureMap.computeIfAbsent(pokemon, p -> ArrayListMultimap.create()).put(form, texture);
    }

    public static void registerSpecialTextureForPokemonSet(IEnumSpecialTexture texture, EnumSpecies ... pokemon) {
        for (EnumSpecies poke : pokemon) {
            EnumSpecies.registerSpecialTexture(poke, texture);
        }
    }

    public static int getFormFromName(EnumSpecies pokemon, String form) {
        for (IEnumForm iEnumForm : formList.get(pokemon)) {
            if (!iEnumForm.getFormSuffix().toLowerCase().contains(form.toLowerCase())) continue;
            return iEnumForm.getForm();
        }
        return -1;
    }

    public static IEnumForm getFormFromID(EnumSpecies pokemon, int form) {
        for (IEnumForm iEnumForm : formList.get(pokemon)) {
            if (iEnumForm.getForm() != form) continue;
            return iEnumForm;
        }
        return EnumForms.NoForm;
    }

    public List<IEnumSpecialTexture> getSpecialTextures(int form) {
        return this.getSpecialTextures(this.getFormEnum(form));
    }

    public List<IEnumSpecialTexture> getSpecialTextures(IEnumForm form) {
        ArrayList<IEnumSpecialTexture> list = new ArrayList<IEnumSpecialTexture>(Collections.singletonList(EnumTextures.None));
        if (this.hasSpecialTexture(form)) {
            list.addAll(specialTextureMap.get(this).get(form));
        }
        return list;
    }

    public String toString() {
        return this.name;
    }

    public boolean isPokemon(EnumSpecies ... species) {
        return Arrays.stream(species).anyMatch(p -> p == this);
    }

    public boolean hasGmaxForm() {
        return this.gmax;
    }

    public static EnumSpecies randomPoke() {
        return EnumSpecies.randomPoke(true);
    }

    public static EnumSpecies randomPokeFromGeneration(int generation) {
        return RandomHelper.getRandomElementFromList(generationMap.get(generation));
    }

    public static EnumSpecies randomLegendary() {
        return RandomHelper.getRandomElementFromArray(LEGENDARY_ENUMS);
    }

    public static EnumSpecies randomPoke(boolean canBeLegendary) {
        boolean isValid = false;
        EnumSpecies randomPokemon = Bulbasaur;
        while (!isValid) {
            randomPokemon = RandomHelper.getRandomElementFromArray(VALUES);
            isValid = true;
            if (!canBeLegendary && randomPokemon.isLegendary() || randomPokemon.isUltraBeast()) {
                isValid = false;
                continue;
            }
            if (PixelmonConfig.allGenerationsDisabled() || Entity3HasStats.isAvailableGeneration(randomPokemon.name)) continue;
            isValid = false;
        }
        return randomPokemon;
    }

    public PokemonSpec toSpec() {
        if (this.name.equalsIgnoreCase("arcanine") || this.name.equalsIgnoreCase("electrode") || this.name.equalsIgnoreCase("lilligant")) {
            PokemonSpec pokemon = PokemonSpec.from(this.name);
            pokemon.form = 11;
            return pokemon;
        }
        return new PokemonSpec(this.name);
    }

    static {
        int i;
        VALUES = EnumSpecies.values();
        pokemonNameList = new ArrayList<String>();
        specialTextureMap = new HashMap<EnumSpecies, Multimap<IEnumForm, IEnumSpecialTexture>>();
        generationMap = new HashMap<Integer, List<EnumSpecies>>();
        for (EnumSpecies enumPokemon : EnumSpecies.values()) {
            pokemonNameList.add(enumPokemon.name);
            generationMap.computeIfAbsent(Entity3HasStats.getGenerationFrom(enumPokemon), k -> new ArrayList()).add(enumPokemon);
        }
        LEGENDARY_ENUMS = new EnumSpecies[]{Articuno, Zapdos, Moltres, Mewtwo, Mew, Raikou, Entei, Suicune, Lugia, Hooh, Celebi, Regirock, Regice, Registeel, Latias, Latios, Kyogre, Groudon, Rayquaza, Jirachi, Deoxys, Uxie, Mesprit, Azelf, Dialga, Palkia, Heatran, Regigigas, Giratina, Cresselia, Phione, Manaphy, Darkrai, Shaymin, Arceus, Cobalion, Terrakion, Virizion, Tornadus, Thundurus, Landorus, Reshiram, Zekrom, Kyurem, Keldeo, Meloetta, Genesect, Victini, Xerneas, Yveltal, Zygarde, Diancie, Hoopa, Volcanion, TapuFini, TapuKoko, TapuBulu, TapuLele, Solgaleo, Lunala, Necrozma, Marshadow, Cosmoem, Cosmog, Magearna, Zeraora, TypeNull, Silvally, Melmetal, Eternatus, Kubfu, Urshifu, Zacian, Zamazenta, Zarude, Regieleki, Regidrago, Spectrier, Glastrier, Calyrex, Enamorus};
        ULTRA_BEASTS_ENUMS = new EnumSpecies[]{Nihilego, Buzzwole, Pheromosa, Xurkitree, Celesteela, Kartana, Guzzlord, Stakataka, Blacephalon, Poipole, Naganadel};
        legendaries = Lists.newArrayList("Articuno", "Zapdos", "Moltres", "Mewtwo", "Mew", "Raikou", "Entei", "Suicune", "Lugia", "Ho-Oh", "Celebi", "Regirock", "Regice", "Registeel", "Latias", "Latios", "Kyogre", "Groudon", "Rayquaza", "Jirachi", "Deoxys", "Uxie", "Mesprit", "Azelf", "Dialga", "Palkia", "Heatran", "Regigigas", "Giratina", "Cresselia", "Manaphy", "Darkrai", "Shaymin", "Arceus", "Cobalion", "Terrakion", "Virizion", "Tornadus", "Thundurus", "Landorus", "Reshiram", "Zekrom", "Kyurem", "Keldeo", "Meloetta", "Genesect", "Victini", "Xerneas", "Yveltal", "Zygarde", "Diancie", "Hoopa", "Volcanion", "TapuFini", "TapuKoko", "TapuBulu", "TapuLele", "Solgaleo", "Lunala", "Necrozma", "Marshadow", "Cosmoem", "Cosmog", "Magearna", "Zeraora", "TypeNull", "Silvally", "Melmetal", "Eternatus", "Kubfu", "Urshifu", "Zacian", "Zamazenta", "Zarude", "Regieleki", "Regidrago", "Spectrier", "Glastrier", "Calyrex", "Enamorus");
        ultrabeasts = Lists.newArrayList("Nihilego", "Buzzwole", "Pheromosa", "Xurkitree", "Celesteela", "Kartana", "Guzzlord", "Stakataka", "Blacephalon", "Poipole", "Naganadel");
        rockSmashEncounters = Lists.newArrayList("Diglett", "Geodude", "Graveler", "Krabby", "Kingler", "Onix", "Dunsparce", "Shuckle", "Slugma", "Nosepass", "Boldore", "Dwebble", "Crustle", "Binacle");
        headbuttEncounters = Lists.newArrayList("Caterpie", "Metapod", "Butterfree", "Weedle", "Kakuna", "Beedrill", "Exeggcute", "Pineco", "Spearow", "Aipom", "Heracross", "Ekans", "Hoothoot", "Noctowl", "Venonat", "Ledyba", "Spinarak", "Natu", "Ledian", "Ariados", "Seedot", "Taillow", "Shroomish", "Slakoth", "Starly", "Burmy", "Cherubi", "Wurmple", "Combee");
        zombieTextured = Lists.newArrayList(Gyarados);
        mfTextured = Lists.newArrayList(Magikarp, Buizel, Floatzel, Hippopotas, Hippowdon, Snover, Unfezant, Frillish, Jellicent, Pyroar);
        mfSprite = Lists.newArrayList(Hippopotas, Hippowdon, Unfezant, Frillish, Jellicent, Pyroar);
        EnumSpecies.registerSpecialTexture(Gyarados, EnumGyradosTexture.Zombie);
        EnumSpecies.registerSpecialTexture(Magikarp, EnumMagickarpTexture.Roasted);
        EnumSpecies.registerSpecialTexture(Butterfree, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Chesnaught, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Decidueye, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Delphox, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Ditto, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Espeon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Flareon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gardevoir, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gengar, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Glaceon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Greninja, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Haunter, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Hooh, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Incineroar, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Kecleon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Kirlia, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Leafeon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Mewtwo, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Mimikyu, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(MrMime, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumOnixTexture.Crystal, Onix, Steelix);
        EnumSpecies.registerSpecialTexture(Pikachu, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Primarina, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Ralts, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Sceptile, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Shuckle, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Sudowoodo, EnumSudowoodoTextures.Gold);
        EnumSpecies.registerSpecialTexture(Tangrowth, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Togedemaru, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Turtonator, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Blastoise, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Blaziken, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Charizard, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Emboar, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Empoleon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Feraligatr, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Infernape, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Meganium, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Samurott, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Serperior, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Swampert, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Torterra, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Typhlosion, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Venusaur, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Buneary, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Lopunny, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Ninetales, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Absol, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Masquerain, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Arcanine, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Bidoof, EnumBidoofTextures.God);
        EnumSpecies.registerSpecialTexture(Bibarel, EnumBidoofTextures.God);
        EnumSpecies.registerSpecialTexture(Dialga, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Mew, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Rapidash, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Vaporeon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Aegislash, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Tyranitar, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gothita, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gothitelle, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gothorita, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Pyukumuku, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Scizor, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Staraptor, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Zoroark, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Braviary, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gallade, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Bounsweet, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Vanillish, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Vanillite, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Vanilluxe, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Bulbasaur, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Ivysaur, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Charmander, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Charmeleon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Squirtle, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Wartortle, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Lucario, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Jirachi, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Beautifly, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Phantump, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Trevenant, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Articuno, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Zapdos, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Moltres, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Bellossom, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Boldore, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gigalith, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Hoppip, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Roggenrola, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Skiploom, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Wobbuffet, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Wynaut, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Umbreon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Electrode, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Shaymin, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Snorlax, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Growlithe, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Riolu, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Darkrai, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Altaria, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Chatot, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Crobat, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Crustle, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Delibird, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Dwebble, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(MimeJr, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Sableye, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Sylveon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Amaura, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Aurorus, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Heliolisk, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Helioptile, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Klefki, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Milotic, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(TapuLele, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Skitty, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Delcatty, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Vikavolt, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Zeraora, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Porygon2, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Voltorb, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Drifloon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Rampardos, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Spinarak, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Solgaleo, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Lunala, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Jolteon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Roserade, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Skiddo, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gogoat, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Escavalier, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Accelgor, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Zorua, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Cosmoem, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Cosmog, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Talonflame, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Breloom, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Bruxish, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Celebi, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Donphan, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Exeggcute, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Exeggutor, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Herdier, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Igglybuff, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Mamoswine, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Phanpy, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Piloswine, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Ponyta, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Rapidash, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Remoraid, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Shroomish, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Smeargle, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Starmie, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Staryu, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Stoutland, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Swinub, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Toucannon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Teddiursa, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Ursaring, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Spinda, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Beedrill, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Kyogre, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Manaphy, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Marshadow, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Swellow, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Slowking, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Nidoking, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Ambipom, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Cleffa, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Clefable, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Clefairy, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Squirtle, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Feebas, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Roserade, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Banette, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Bronzor, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Caterpie, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Chespin, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gastly, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Grotle, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Metapod, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Quilladin, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Turtwig, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Tepig, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Pignite, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Snivy, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Servine, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Oshawott, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Dewott, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Banette, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Sneasel, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Weavile, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Magearna, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Fletchinder, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Fletchling, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Lilligant, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Munchlax, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Rayquaza, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Slowbro, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Slowpoke, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Sunkern, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Tsareena, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Victreebel, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Ariados, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Budew, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Cascoon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Cranidos, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Dustox, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gloom, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Jigglypuff, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Kakuna, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Karrablast, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Larvitar, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Nidoranmale, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Nidorino, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Octillery, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Oddish, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Pupitar, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Roselia, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Shelmet, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Shuppet, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Silcoon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Staravia, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Starly, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Steenee, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Taillow, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Tangela, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Vileplume, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Weedle, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Wigglytuff, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Wurmple, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Weepinbell, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Arbok, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Aromatisse, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Yveltal, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Cacnea, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Ekans, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gligar, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gliscor, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Swalot, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Groudon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gulpin, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Horsea, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Mareep, EnumMareep.Regular, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Toxapex, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Minun, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Noibat, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Noivern, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Plusle, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Ampharos, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Flaaffy, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Skarmory, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Sentret, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Spritzee, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Seadra, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Kingdra, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Stakataka, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Litten, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Furret, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Mareanie, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Cacturne, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Heatran, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Liepard, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Mareanie, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Snover, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Abomasnow, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Cresselia, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Aipom, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Azurill, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Baltoy, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Brionne, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Cherubi, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Cinccino, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Drampa, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Marill, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Azumarill, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Minccino, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Popplio, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Seviper, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Shedinja, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Togepi, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Togekiss, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Togetic, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Dubwool, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Appletun, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Applin, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Flapple, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Chinchou, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Lanturn, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Cottonee, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Whimsicott, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Skwovet, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Greedent, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Hattrem, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Hatenna, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Hatterene, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Komala, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Sewaddle, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Swadloon, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Leavanny, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Nidoranfemale, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Nidorina, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Nidoqueen, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Sawsbuck, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Pachirisu, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Chewtle, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Drednaw, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Sandshrew, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Sandslash, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Palkia, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gossifleur, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Eldegoss, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Heracross, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Spoink, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Grumpig, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Froslass, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Glalie, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Diancie, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Tympole, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Palpitoad, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Seismitoad, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Lunatone, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Solrock, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Maractus, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Venonat, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Venomoth, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Pumpkaboo, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gourgeist, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Misdreavus, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Mismagius, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Boltund, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Yamper, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Cradily, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Mawile, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Woobat, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Swoobat, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Stunky, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Skuntank, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Comfey, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Illumise, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Volbeat, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Sizzlipede, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Centiskorch, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Omanyte, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Omastar, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Flabebe, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Florges, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Floette, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Blipbug, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Dottler, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Orbeetle, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Necrozma, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Necrozma, EnumNecrozma.Normal, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Necrozma, EnumNecrozma.Dawn, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Necrozma, EnumNecrozma.Dusk, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Floette, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Floette, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Alomomola, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Audino, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Audino, EnumForms.Mega, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Camerupt, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Camerupt, EnumForms.Mega, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Dodrio, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Doduo, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Koffing, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Mothim, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Burmy, EnumBurmy.Plant, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Burmy, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Wormadam, EnumWormadam.Plant, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Wormadam, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Nosepass, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Numel, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Probopass, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Weezing, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Weezing, EnumForms.Galarian, EnumSpringTextures.Spring);
        EnumSpecies.registerSpecialTexture(Sinistea, EnumSinistea.Antique, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Sinistea, EnumSinistea.Phony, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Polteageist, EnumPolteageist.Antique, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Polteageist, EnumPolteageist.Phony, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Ponyta, EnumForms.Galarian, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Rapidash, EnumForms.Galarian, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Corsola, EnumForms.Galarian, EnumSummerTextures.Summer);
        EnumSpecies.registerSpecialTexture(Cherrim, EnumCherrim.OVERCAST, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Cherrim, EnumCherrim.SUNSHINE, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Morpeko, EnumMorpeko.FullBellyMode, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Morpeko, EnumMorpeko.HangryMode, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Deoxys, EnumDeoxys.Attack, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Deoxys, EnumDeoxys.Defense, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Deoxys, EnumDeoxys.Speed, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Deoxys, EnumDeoxys.Normal, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Pikachu, EnumForms.Hat, EnumPikachuTextures.Rocket);
        EnumSpecies.registerSpecialTexture(Duraludon, EnumTextures.Halloween);
        Arrays.stream(EnumSeason.values()).forEach(enumSeason -> EnumSpecies.registerSpecialTexture(Sawsbuck, enumSeason, EnumTextures.Halloween));
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumTextures.Halloween, Squirtle, Wartortle, Cyndaquil, Quilava, Totodile, Croconaw, Chikorita, Bayleef, Torchic, Combusken, Mudkip, Marshtomp, Treecko, Grovyle, Chimchar, Monferno, Piplup, Prinplup, Turtwig, Grotle, Fennekin, Braixen, Froakie, Frogadier, Chespin, Quilladin, Tepig, Pignite, Snivy, Servine, Oshawott, Dewott, Sobble, Drizzile, Inteleon, Grookey, Thwackey, Rillaboom, Scorbunny, Raboot, Cinderace);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumDragoniteTextures.TropicalSea, Dratini, Dragonair, Dragonite);
        EnumSpecies.registerSpecialTexture(Aegislash, EnumAegislash.Blade, EnumAegislashTextures.CaptainAmerica);
        EnumSpecies.registerSpecialTexture(Aegislash, EnumAegislash.Shield, EnumAegislashTextures.CaptainAmerica);
        EnumSpecies.registerSpecialTexture(Aegislash, EnumAegislash.Shield, EnumAegislashTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Aegislash, EnumAegislash.Blade, EnumAegislashTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Flabebe, EnumFlabebe.BLUE, EnumFlabebeTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Flabebe, EnumFlabebe.ORANGE, EnumFlabebeTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Flabebe, EnumFlabebe.RED, EnumFlabebeTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Flabebe, EnumFlabebe.WHITE, EnumFlabebeTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Flabebe, EnumFlabebe.YELLOW, EnumFlabebeTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Floette, EnumFlabebe.BLUE, EnumFlabebeTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Floette, EnumFlabebe.ORANGE, EnumFlabebeTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Floette, EnumFlabebe.RED, EnumFlabebeTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Floette, EnumFlabebe.WHITE, EnumFlabebeTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Floette, EnumFlabebe.YELLOW, EnumFlabebeTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Florges, EnumFlabebe.BLUE, EnumFlabebeTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Florges, EnumFlabebe.ORANGE, EnumFlabebeTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Florges, EnumFlabebe.RED, EnumFlabebeTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Florges, EnumFlabebe.WHITE, EnumFlabebeTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Florges, EnumFlabebe.YELLOW, EnumFlabebeTextures.Valentines);
        EnumSpecies.registerSpecialTexture(Zacian, EnumZacian.Normal, EnumZacianTextures.Minecraftfox);
        EnumSpecies.registerSpecialTexture(Zacian, EnumZacian.Crowned, EnumZacianTextures.Minecraftfox);
        EnumSpecies.registerSpecialTexture(Aron, EnumAggronTextures.AgedCopper);
        EnumSpecies.registerSpecialTexture(Lairon, EnumAggronTextures.BrassForge);
        EnumSpecies.registerSpecialTexture(Aggron, EnumAggronTextures.CarbonSteel);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumCarracostaTextures.SeaTurtle, Tirtouga, Carracosta);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumSummerTextures.Summer, Sunflora, Machamp, Lapras, Snorunt, Miltank, Cursola, Silicobra, Sandaconda, Sandile, Krokorok, Krookodile, Fearow, Spearow);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumCNYTextures.CNY, Bagon, Bunnelby, Deerling, Diggersby, Dunsparce, Luxio, Luxray, Mudbray, Mudsdale, Salamence, Shelgon, Shinx, Tauros);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumValentinesTextures.Valentines, Amoonguss, Bewear, Blastoise, Foongus, Grotle, Kartana, Luvdisc, Squirtle, Stufful, Torterra, Turtwig, Wartortle);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumCheongsamTextures.Cheongsam, Gallade, Gardevoir, Kirlia, Ralts);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumLanternTextures.Lantern, Drifloon, Drifblim);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumScalesTextures.Scales, Larvitar, Pupitar, Tyranitar);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumPoliwagLineTextures.Candy, Poliwag, Poliwhirl, Poliwrath, Politoed);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumSimiPanLineTextures.Candy, Pansage, Simipour, Panpour, Simisage, Simisear, Pansear);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumAvaluggLineTextures.Candy, Avalugg, Bergmite);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumCufantLineTextures.Elephant, Cufant, Copperajah);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumDreepyLineTextures.Nerf, Dreepy, Drakloak, Dragapult);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumPidgeyLineTextures.Zelda, Pidgey, Pidgeotto, Pidgeot);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumTrapinchLineTextures.Cosmic, Trapinch, Vibrava, Flygon);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumVolcaronaLineTextures.Tropical, Volcarona, Larvesta);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumMantykeLineTextures.Gyorg, Mantyke, Mantine);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumSharpedoLineTextures.Scrapforge, Carvanha, Sharpedo);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumMeltanTextures.Magma, Meltan, Melmetal);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumTrubbishTextures.Patch, Trubbish, Garbodor);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumSwordsOfJusticeTextures.Space, Cobalion, Terrakion, Virizion);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumAerodactylTextures.Retro, Aerodactyl);
        EnumSpecies.registerSpecialTexture(Keldeo, EnumKeldeo.ORDINARY, EnumSwordsOfJusticeTextures.Space);
        EnumSpecies.registerSpecialTexture(Keldeo, EnumKeldeo.RESOLUTE, EnumSwordsOfJusticeTextures.Space);
        EnumSpecies.registerSpecialTexture(Cramorant, EnumCramorantTextures.Flamingo);
        EnumSpecies.registerSpecialTexture(Falinks, EnumFalinksTextures.Kirby);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumLotadLineTextures.Robodisco, Lotad, Lombre, Ludicolo);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumBeldumLineTextures.Gohma, Beldum, Metang, Metagross);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumGibleLineTextures.Scrapforge, Gible, Gabite, Garchomp);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumRookideeLineTextures.Eagle, Rookidee, Corvisquire, Corviknight);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumDiglettTextures.Snow, Diglett, Dugtrio);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumPsyduckTextures.Cartoon, Psyduck, Golduck);
        EnumSpecies.registerSpecialTexture(Deerling, EnumSeason.Autumn, EnumCNYTextures.CNY);
        EnumSpecies.registerSpecialTexture(Deerling, EnumSeason.Spring, EnumCNYTextures.CNY);
        EnumSpecies.registerSpecialTexture(Deerling, EnumSeason.Winter, EnumCNYTextures.CNY);
        EnumSpecies.registerSpecialTexture(Deerling, EnumSeason.Summer, EnumCNYTextures.CNY);
        EnumSpecies.registerSpecialTexture(Vaporeon, EnumVaporeonTextures.Summer);
        EnumSpecies.registerSpecialTexture(Jirachi, EnumJirachiTextures.Hana);
        EnumSpecies.registerSpecialTexture(Chimecho, EnumChimechoTextures.DecoraKei);
        EnumSpecies.registerSpecialTexture(Chingling, EnumChinglingTextures.DecoraKei);
        EnumSpecies.registerSpecialTexture(Chingling, EnumChinglingTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Bellsprout, EnumBellsproutTextures.PirahnaPlant);
        EnumSpecies.registerSpecialTexture(Purrloin, EnumPurrloinTextures.Morgana);
        EnumSpecies.registerSpecialTexture(Zigzagoon, EnumZigzagoonTextures.Rocket);
        EnumSpecies.registerSpecialTexture(Farfetchd, EnumFarfetchdTextures.DonaldDuck);
        EnumSpecies.registerSpecialTexture(Munna, EnumMunnaTextures.LucidDream);
        EnumSpecies.registerSpecialTexture(Musharna, EnumMusharnaTextures.LucidDream);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumHydregonTextures.FlowerBud, Deino, Zweilous, Hydreigon);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumTentacruelLineTextures.GooperBlooper, Tentacool, Tentacruel);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumGoodraLineTextures.ChocolateCherry, Goomy, Sliggoo, Goodra);
        EnumSpecies.registerSpecialTexture(Buizel, EnumBuizelTextures.Tails);
        EnumSpecies.registerSpecialTexture(Floatzel, EnumFloatzelTextures.Tails);
        EnumSpecies.registerSpecialTexture(Torkoal, EnumTorkoalTextures.KoopaTroopa);
        EnumSpecies.registerSpecialTexture(Butterfree, EnumButterfreeTextures.BugCatcher);
        EnumSpecies.registerSpecialTexture(Caterpie, EnumCaterpieTextures.BugCatcher);
        EnumSpecies.registerSpecialTexture(Metapod, EnumMetapodTextures.BugCatcher);
        EnumSpecies.registerSpecialTexture(Shuckle, EnumShuckleTextures.GreenShell);
        EnumSpecies.registerSpecialTexture(Shuckle, EnumShuckleTextures.RedShell);
        EnumSpecies.registerSpecialTexture(Wooloo, EnumWooloo.Regular, EnumWoolooTextures.Santa);
        EnumSpecies.registerSpecialTexture(Wooloo, EnumWooloo.Regular, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Squirtle, EnumSquirtleTextures.Classic);
        EnumSpecies.registerSpecialTexture(Machamp, EnumSaddleTextures.Saddle);
        EnumSpecies.registerSpecialTexture(Mudsdale, EnumSaddleTextures.Saddle);
        EnumSpecies.registerSpecialTexture(Stoutland, EnumSaddleTextures.Saddle);
        EnumSpecies.registerSpecialTexture(Tauros, EnumSaddleTextures.Saddle);
        EnumSpecies.registerSpecialTexture(Charizard, EnumSaddleTextures.Saddle);
        EnumSpecies.registerSpecialTexture(Lapras, EnumSaddleTextures.Saddle);
        EnumSpecies.registerSpecialTexture(Mewtwo, EnumArmorTextures.Armor);
        EnumSpecies.registerSpecialTexture(Sableye, EnumSableyeTextures.Shadow);
        EnumSpecies.registerSpecialTexture(Cryogonal, EnumCryogonalTextures.Radioactive);
        EnumSpecies.registerSpecialTexture(Eternatus, EnumEternatus.Normal, EnumEternatusTextures.Radioactive);
        EnumSpecies.registerSpecialTexture(Eternatus, EnumEternatus.Eternamax, EnumEternatusTextures.Dragonbone);
        EnumSpecies.registerSpecialTexture(Eternatus, EnumEternatus.Normal, EnumEternatusTextures.Dragonbone);
        EnumSpecies.registerSpecialTexture(Wooloo, EnumWoolooTextures.Santa);
        EnumSpecies.registerSpecialTexture(Pikachu, EnumPikachuTextures.Detective);
        Arrays.stream(EnumPikachu.values()).forEach(enumPikachu -> EnumSpecies.registerSpecialTexture(Pikachu, enumPikachu, EnumPikachuTextures.Detective));
        EnumSpecies.registerSpecialTexture(Ditto, EnumDittoTextures.Tophat);
        EnumSpecies.registerSpecialTexture(Ditto, EnumDittoTextures.Classic);
        EnumSpecies.registerSpecialTexture(Chansey, EnumChanseyTextures.Nursejoy);
        EnumSpecies.registerSpecialTexture(Golduck, EnumGolduckTextures.Captain);
        EnumSpecies.registerSpecialTexture(Growlithe, EnumGrowlitheTextures.Officer);
        EnumSpecies.registerSpecialTexture(Luvdisc, EnumLuvdiscTextures.Heart);
        EnumSpecies.registerSpecialTexture(Krabby, EnumKrabbyTextures.Cowboy);
        EnumSpecies.registerSpecialTexture(Krabby, EnumKrabbyTextures.Police);
        EnumSpecies.registerSpecialTexture(Krabby, EnumKrabbyTextures.Pink);
        EnumSpecies.registerSpecialTexture(Krabby, EnumKrabbyTextures.Classic);
        EnumSpecies.registerSpecialTexture(Eevee, EnumEeveeTextures.Galaxy);
        EnumSpecies.registerSpecialTexture(Eevee, EnumEeveeTextures.PaleHat);
        EnumSpecies.registerSpecialTexture(Eevee, EnumEeveeTextures.LightHat);
        EnumSpecies.registerSpecialTexture(Eevee, EnumEeveeTextures.DarkHat);
        EnumSpecies.registerSpecialTexture(Eevee, EnumEeveeTextures.Bowtie);
        EnumSpecies.registerSpecialTexture(Eevee, EnumEeveeTextures.Classic);
        EnumSpecies.registerSpecialTexture(Hoopa, EnumHoopa.CONFINED, EnumHoopaTextures.Shadow);
        EnumSpecies.registerSpecialTexture(Hoopa, EnumHoopa.UNBOUND, EnumHoopaTextures.Shadow);
        EnumSpecies.registerSpecialTexture(Raichu, EnumForms.Alolan, EnumAlolanRaichuTextures.Thunderclap);
        EnumSpecies.registerSpecialTexture(Raichu, EnumForms.Alolan, EnumAlolanRaichuTextures.PoolParty);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumClefableTextures.Lunar, Cleffa, Clefairy, Clefable);
        EnumSpecies.registerSpecialTexture(Mimikyu, EnumMimikyuTextures.Drawn);
        EnumSpecies.registerSpecialTexture(Leafeon, EnumLeafeonTextures.Blooming);
        EnumSpecies.registerSpecialTexture(Leafeon, EnumLeafeonTextures.Flower);
        EnumSpecies.registerSpecialTexture(Espeon, EnumEspeonTextures.Dawn);
        EnumSpecies.registerSpecialTexture(Umbreon, EnumUmbreonTextures.Supernova);
        EnumSpecies.registerSpecialTexture(Umbreon, EnumUmbreonTextures.Classic);
        EnumSpecies.registerSpecialTexture(Charjabug, EnumCharjabugTextures.SchoolBus);
        EnumSpecies.registerSpecialTexture(Honedge, EnumAegislashTextures.FourSword);
        EnumSpecies.registerSpecialTexture(Doublade, EnumAegislashTextures.GanonBlade);
        EnumSpecies.registerSpecialTexture(Grubbin, EnumCharjabugTextures.YellowPurple);
        EnumSpecies.registerSpecialTexture(Charjabug, EnumCharjabugTextures.YellowPurple);
        EnumSpecies.registerSpecialTexture(Lillipup, EnumLillipupTextures.Silver);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumZubatLine.Batman, Zubat, Golbat);
        EnumSpecies.registerSpecialTexture(Vulpix, EnumVulpixTextures.Kurama);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumPorygonTextures.RubberDuck, Porygon, PorygonZ);
        EnumSpecies.registerSpecialTexture(Bonsly, EnumBonslyTextures.Golden);
        EnumSpecies.registerSpecialTexture(Jumpluff, EnumJumpluffTextures.Snoppy);
        EnumSpecies.registerSpecialTexture(Surskit, EnumSurskitTextures.Octorock);
        EnumSpecies.registerSpecialTexture(Swablu, EnumSwabluTextures.Cocktiel);
        EnumSpecies.registerSpecialTexture(MimeJr, EnumMimeJrTextures.Nutcracker);
        EnumSpecies.registerSpecialTexture(Aegislash, EnumAegislash.Blade, EnumAegislashTextures.Zelda);
        EnumSpecies.registerSpecialTexture(Aegislash, EnumAegislash.Shield, EnumAegislashTextures.Zelda);
        EnumSpecies.registerSpecialTexture(Shaymin, EnumShaymin.LAND, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Shaymin, EnumShaymin.SKY, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Vulpix, EnumForms.Alolan, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Ninetales, EnumForms.Alolan, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumTextures.Shadow, Scyther, Lugia, Hoothoot, Noctowl);
        EnumSpecies.registerSpecialTexture(Noctowl, EnumNoctowlTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Hoothoot, EnumNoctowlTextures.Halloween);
        EnumSpecies.registerSpecialTextureForPokemonSet(EnumMeowthTextures.Ninja, Meowth, Persian);
        EnumSpecies.registerSpecialTexture(Greninja, EnumGreninja.BATTLE_BOND, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Greninja, EnumGreninja.BASE, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Aggron, EnumForms.Mega, EnumAggronTextures.CrystalForge);
        EnumSpecies.registerSpecialTexture(Ampharos, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Altaria, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Beedrill, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Blaziken, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gallade, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Gengar, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Lopunny, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Lucario, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Mewtwo, EnumForms.MegaX, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Mewtwo, EnumForms.MegaY, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Scizor, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Slowbro, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Steelix, EnumForms.Mega, EnumOnixTexture.Crystal);
        EnumSpecies.registerSpecialTexture(Venusaur, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Charizard, EnumForms.MegaX, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Charizard, EnumForms.MegaY, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Swampert, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Tyranitar, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Rayquaza, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Heracross, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Glalie, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Mawile, EnumForms.Mega, EnumTextures.Halloween);
        EnumSpecies.registerSpecialTexture(Salamence, EnumForms.Mega, EnumCNYTextures.CNY);
        EnumSpecies.registerSpecialTexture(Gallade, EnumForms.Mega, EnumCheongsamTextures.Cheongsam);
        EnumSpecies.registerSpecialTexture(Gardevoir, EnumForms.Mega, EnumCheongsamTextures.Cheongsam);
        EnumSpecies.registerSpecialTexture(Pidgeot, EnumForms.Mega, EnumPidgeyLineTextures.Zelda);
        EnumSpecies.registerSpecialTexture(Sharpedo, EnumForms.Mega, EnumSharpedoLineTextures.Scrapforge);
        EnumSpecies.registerSpecialTexture(Aerodactyl, EnumForms.Mega, EnumAerodactylTextures.Retro);
        EnumSpecies.registerSpecialTexture(Metagross, EnumForms.Mega, EnumBeldumLineTextures.Gohma);
        EnumSpecies.registerSpecialTexture(Garchomp, EnumForms.Mega, EnumGibleLineTextures.Scrapforge);
        EnumSpecies.registerSpecialTexture(Rockruff, EnumLycanrocTextures.Minty);
        EnumSpecies.registerSpecialTexture(Rockruff, EnumLycanrocTextures.Husky);
        EnumSpecies.registerSpecialTexture(Lycanroc, EnumLycanroc.Day, EnumLycanrocTextures.Minty);
        EnumSpecies.registerSpecialTexture(Lycanroc, EnumLycanroc.Day, EnumLycanrocTextures.Husky);
        EnumSpecies.registerSpecialTexture(Lycanroc, EnumLycanroc.Dusk, EnumLycanrocTextures.Minty);
        EnumSpecies.registerSpecialTexture(Lycanroc, EnumLycanroc.Dusk, EnumLycanrocTextures.Husky);
        EnumSpecies.registerSpecialTexture(Lycanroc, EnumLycanroc.Night, EnumLycanrocTextures.Minty);
        EnumSpecies.registerSpecialTexture(Lycanroc, EnumLycanroc.Night, EnumLycanrocTextures.Husky);
        EnumSpecies.registerSpecialTexture(Mew, EnumMewTextures.Classic);
        EnumSpecies.registerSpecialTexture(Mew, EnumMewTextures.ClassicShadow);
        EnumSpecies.registerSpecialTexture(Arcanine, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Growlithe, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Trapinch, EnumTrapinchTextures.Classic);
        EnumSpecies.registerSpecialTexture(Bulbasaur, EnumBulbasaurTextures.Classic);
        EnumSpecies.registerSpecialTexture(Charmander, EnumCharmanderTextures.Classic);
        EnumSpecies.registerSpecialTexture(Magikarp, EnumMagickarpTexture.Classic);
        EnumSpecies.registerSpecialTexture(Vulpix, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Vaporeon, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Leafeon, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Espeon, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Zapdos, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Ninetales, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Bellsprout, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Bastiodon, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Articuno, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Armaldo, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Archeops, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Archen, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Arbok, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Anorith, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Alakazam, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Aerodactyl, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Abra, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Jolteon, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Flareon, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Glaceon, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Rattata, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Raticate, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Persian, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Meowth, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Moltres, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Dewgong, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Cubone, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Croconaw, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Cranidos, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Cloyster, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Clefairy, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Clefable, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Chansey, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Carracosta, EnumTextures.Classic);
        EnumSpecies.registerSpecialTexture(Camerupt, EnumTextures.Classic);
        formList = MultimapBuilder.treeKeys().arrayListValues(1).build();
        EnumSpecies[] megas = new EnumSpecies[]{Venusaur, Blastoise, Alakazam, Gengar, Kangaskhan, Pinsir, Gyarados, Aerodactyl, Ampharos, Scizor, Heracross, Houndoom, Tyranitar, Blaziken, Gardevoir, Mawile, Aggron, Medicham, Manectric, Banette, Absol, Garchomp, Lucario, Abomasnow, Beedrill, Pidgeot, Slowbro, Steelix, Sceptile, Swampert, Sableye, Sharpedo, Camerupt, Altaria, Glalie, Salamence, Metagross, Latias, Latios, Rayquaza, Lopunny, Gallade, Audino, Diancie};
        for (EnumSpecies enumSpecies : megas) {
            if (!enumSpecies.hasMega()) continue;
            formList.put(enumSpecies, EnumForms.Mega);
        }
        formList.put(Charizard, EnumForms.MegaX);
        formList.put(Charizard, EnumForms.MegaY);
        formList.put(Mewtwo, EnumForms.MegaX);
        formList.put(Mewtwo, EnumForms.MegaY);
        formList.put(Rattata, EnumForms.Alolan);
        formList.put(Raticate, EnumForms.Alolan);
        formList.put(Diglett, EnumForms.Alolan);
        formList.put(Dugtrio, EnumForms.Alolan);
        formList.put(Meowth, EnumForms.Alolan);
        formList.put(Persian, EnumForms.Alolan);
        formList.put(Raichu, EnumForms.Alolan);
        formList.put(Golem, EnumForms.Alolan);
        formList.put(Graveler, EnumForms.Alolan);
        formList.put(Geodude, EnumForms.Alolan);
        formList.put(Sandslash, EnumForms.Alolan);
        formList.put(Sandshrew, EnumForms.Alolan);
        formList.put(Ninetales, EnumForms.Alolan);
        formList.put(Marowak, EnumForms.Alolan);
        formList.put(Vulpix, EnumForms.Alolan);
        formList.put(Exeggutor, EnumForms.Alolan);
        formList.put(Grimer, EnumForms.Alolan);
        formList.put(Muk, EnumForms.Alolan);
        formList.put(Meowth, EnumForms.Galarian);
        formList.put(Ponyta, EnumForms.Galarian);
        formList.put(Rapidash, EnumForms.Galarian);
        formList.put(Slowpoke, EnumForms.Galarian);
        formList.put(Slowbro, EnumForms.Galarian);
        formList.put(Slowking, EnumForms.Galarian);
        formList.put(Articuno, EnumForms.Galarian);
        formList.put(Zapdos, EnumForms.Galarian);
        formList.put(Moltres, EnumForms.Galarian);
        formList.put(Farfetchd, EnumForms.Galarian);
        formList.put(Weezing, EnumForms.Galarian);
        formList.put(MrMime, EnumForms.Galarian);
        formList.put(Corsola, EnumForms.Galarian);
        formList.put(Zigzagoon, EnumForms.Galarian);
        formList.put(Linoone, EnumForms.Galarian);
        formList.put(Darumaka, EnumForms.Galarian);
        formList.put(Darmanitan, EnumForms.Galarian);
        formList.put(Yamask, EnumForms.Galarian);
        formList.put(Stunfisk, EnumForms.Galarian);
        formList.put(Growlithe, EnumForms.Hisuian);
        formList.put(Arcanine, EnumForms.Hisuian);
        formList.put(Voltorb, EnumForms.Hisuian);
        formList.put(Electrode, EnumForms.Hisuian);
        formList.put(Lilligant, EnumForms.Hisuian);
        formList.put(Zorua, EnumForms.Hisuian);
        formList.put(Zoroark, EnumForms.Hisuian);
        formList.put(Sliggoo, EnumForms.Hisuian);
        formList.put(Goodra, EnumForms.Hisuian);
        formList.put(Braviary, EnumForms.Hisuian);
        formList.put(Decidueye, EnumForms.Hisuian);
        formList.put(Avalugg, EnumForms.Hisuian);
        formList.put(Qwilfish, EnumForms.Hisuian);
        formList.put(Samurott, EnumForms.Hisuian);
        formList.put(Sneasel, EnumForms.Hisuian);
        formList.put(Typhlosion, EnumForms.Hisuian);
        for (EnumPikachu enum_ : EnumPikachu.values()) {
            formList.put(Pikachu, enum_);
        }
        formList.put(Pikachu, EnumForms.Hat);
        formList.put(Pikachu, EnumForms.NoForm);
        formList.put(Meowth, EnumForms.Ninja);
        formList.put(Persian, EnumForms.Ninja);
        formList.put(Meowstic, EnumMeowstic.Male);
        formList.put(Meowstic, EnumMeowstic.Female);
        formList.put(Basculegion, EnumBasculegion.Male);
        formList.put(Basculegion, EnumBasculegion.Female);
        formList.put(Indeedee, EnumIndeedee.Male);
        formList.put(Indeedee, EnumIndeedee.Female);
        formList.put(Lycanroc, EnumLycanroc.Day);
        formList.put(Lycanroc, EnumLycanroc.Night);
        formList.put(Lycanroc, EnumLycanroc.Dusk);
        formList.put(MissingNo, EnumMissingNo.RedBlue);
        formList.put(MissingNo, EnumMissingNo.Yellow);
        formList.put(MissingNo, EnumMissingNo.Kabutops);
        formList.put(MissingNo, EnumMissingNo.Aerodactyl);
        formList.put(MissingNo, EnumMissingNo.Ghost);
        formList.put(MissingNo, EnumMissingNo.Fakemon5);
        formList.put(MissingNo, EnumMissingNo.Fakemon6);
        formList.put(MissingNo, EnumMissingNo.Fakemon7);
        formList.put(MissingNo, EnumMissingNo.Fakemon8);
        formList.put(MissingNo, EnumMissingNo.Fakemon9);
        formList.put(MissingNo, EnumMissingNo.Fakemon10);
        formList.put(MissingNo, EnumMissingNo.Fakemon11);
        formList.put(MissingNo, EnumMissingNo.Fakemon12);
        formList.put(MissingNo, EnumMissingNo.Fakemon13);
        formList.put(MissingNo, EnumMissingNo.Fakemon14);
        formList.put(MissingNo, EnumMissingNo.Fakemon15);
        formList.put(MissingNo, EnumMissingNo.Fakemon16);
        formList.put(MissingNo, EnumMissingNo.Fakemon17);
        formList.put(MissingNo, EnumMissingNo.Fakemon18);
        formList.put(MissingNo, EnumMissingNo.Fakemon19);
        formList.put(MissingNo, EnumMissingNo.Fakemon20);
        formList.put(MissingNo, EnumMissingNo.Fakemon21);
        formList.put(MissingNo, EnumMissingNo.Fakemon22);
        formList.put(MissingNo, EnumMissingNo.Fakemon23);
        formList.put(MissingNo, EnumMissingNo.Fakemon24);
        formList.put(MissingNo, EnumMissingNo.Fakemon25);
        formList.put(MissingNo, EnumMissingNo.Fakemon26);
        formList.put(MissingNo, EnumMissingNo.Fakemon27);
        formList.put(MissingNo, EnumMissingNo.Fakemon28);
        formList.put(MissingNo, EnumMissingNo.Fakemon29);
        formList.put(MissingNo, EnumMissingNo.Fakemon30);
        formList.put(MissingNo, EnumMissingNo.Fakemon31);
        formList.put(MissingNo, EnumMissingNo.Fakemon32);
        formList.put(MissingNo, EnumMissingNo.Fakemon33);
        formList.put(MissingNo, EnumMissingNo.Fakemon34);
        formList.put(MissingNo, EnumMissingNo.Fakemon35);
        formList.put(MissingNo, EnumMissingNo.Fakemon36);
        formList.put(MissingNo, EnumMissingNo.Fakemon37);
        formList.put(MissingNo, EnumMissingNo.Fakemon38);
        formList.put(MissingNo, EnumMissingNo.Fakemon39);
        formList.put(MissingNo, EnumMissingNo.Fakemon40);
        formList.put(MissingNo, EnumMissingNo.Fakemon41);
        formList.put(MissingNo, EnumMissingNo.Fakemon42);
        formList.put(MissingNo, EnumMissingNo.Fakemon43);
        formList.put(MissingNo, EnumMissingNo.Fakemon44);
        formList.put(MissingNo, EnumMissingNo.Fakemon45);
        formList.put(MissingNo, EnumMissingNo.Fakemon46);
        formList.put(MissingNo, EnumMissingNo.Fakemon47);
        formList.put(MissingNo, EnumMissingNo.Fakemon48);
        formList.put(MissingNo, EnumMissingNo.Fakemon49);
        formList.put(MissingNo, EnumMissingNo.Fakemon50);
        formList.put(MissingNo, EnumMissingNo.Fakemon51);
        formList.put(MissingNo, EnumMissingNo.Fakemon52);
        formList.put(MissingNo, EnumMissingNo.Fakemon53);
        formList.put(MissingNo, EnumMissingNo.Fakemon54);
        formList.put(Zygarde, EnumZygarde.Fifty);
        formList.put(Zygarde, EnumZygarde.Ten);
        formList.put(Zygarde, EnumZygarde.Complete10);
        formList.put(Zygarde, EnumZygarde.Complete50);
        formList.put(Minior, EnumMinior.Red);
        formList.put(Minior, EnumMinior.Orange);
        formList.put(Minior, EnumMinior.Yellow);
        formList.put(Minior, EnumMinior.Green);
        formList.put(Minior, EnumMinior.Blue);
        formList.put(Minior, EnumMinior.Indigo);
        formList.put(Minior, EnumMinior.Violet);
        formList.put(Minior, EnumMinior.MeteorRed);
        formList.put(Minior, EnumMinior.MeteorOrange);
        formList.put(Minior, EnumMinior.MeteorYellow);
        formList.put(Minior, EnumMinior.MeteorGreen);
        formList.put(Minior, EnumMinior.MeteorBlue);
        formList.put(Minior, EnumMinior.MeteorIndigo);
        formList.put(Minior, EnumMinior.MeteorViolet);
        formList.put(Alcremie, EnumAlcremie.BerryCaramelSwirl);
        formList.put(Alcremie, EnumAlcremie.BerryLemonCream);
        formList.put(Alcremie, EnumAlcremie.BerryMatchaCream);
        formList.put(Alcremie, EnumAlcremie.BerryMintCream);
        formList.put(Alcremie, EnumAlcremie.BerryRainbowSwirl);
        formList.put(Alcremie, EnumAlcremie.BerryRubyCream);
        formList.put(Alcremie, EnumAlcremie.BerryRubySwirl);
        formList.put(Alcremie, EnumAlcremie.BerrySaltedCream);
        formList.put(Alcremie, EnumAlcremie.BerryVanillaCream);
        formList.put(Alcremie, EnumAlcremie.CloverCaramelSwirl);
        formList.put(Alcremie, EnumAlcremie.CloverLemonCream);
        formList.put(Alcremie, EnumAlcremie.CloverMatchaCream);
        formList.put(Alcremie, EnumAlcremie.CloverMintCream);
        formList.put(Alcremie, EnumAlcremie.CloverRainbowSwirl);
        formList.put(Alcremie, EnumAlcremie.CloverRubyCream);
        formList.put(Alcremie, EnumAlcremie.CloverRubySwirl);
        formList.put(Alcremie, EnumAlcremie.CloverSaltedCream);
        formList.put(Alcremie, EnumAlcremie.CloverVanillaCream);
        formList.put(Alcremie, EnumAlcremie.FlowerCaramelSwirl);
        formList.put(Alcremie, EnumAlcremie.FlowerLemonCream);
        formList.put(Alcremie, EnumAlcremie.FlowerMatchaCream);
        formList.put(Alcremie, EnumAlcremie.FlowerMintCream);
        formList.put(Alcremie, EnumAlcremie.FlowerRainbowSwirl);
        formList.put(Alcremie, EnumAlcremie.FlowerRubyCream);
        formList.put(Alcremie, EnumAlcremie.FlowerRubySwirl);
        formList.put(Alcremie, EnumAlcremie.FlowerSaltedCream);
        formList.put(Alcremie, EnumAlcremie.FlowerVanillaCream);
        formList.put(Alcremie, EnumAlcremie.LoveCaramelSwirl);
        formList.put(Alcremie, EnumAlcremie.LoveLemonCream);
        formList.put(Alcremie, EnumAlcremie.LoveMatchaCream);
        formList.put(Alcremie, EnumAlcremie.LoveMintCream);
        formList.put(Alcremie, EnumAlcremie.LoveRainbowSwirl);
        formList.put(Alcremie, EnumAlcremie.LoveRubyCream);
        formList.put(Alcremie, EnumAlcremie.LoveRubySwirl);
        formList.put(Alcremie, EnumAlcremie.LoveSaltedCream);
        formList.put(Alcremie, EnumAlcremie.LoveVanillaCream);
        formList.put(Alcremie, EnumAlcremie.RibbonCaramelSwirl);
        formList.put(Alcremie, EnumAlcremie.RibbonLemonCream);
        formList.put(Alcremie, EnumAlcremie.RibbonMatchaCream);
        formList.put(Alcremie, EnumAlcremie.RibbonMintCream);
        formList.put(Alcremie, EnumAlcremie.RibbonRainbowSwirl);
        formList.put(Alcremie, EnumAlcremie.RibbonRubyCream);
        formList.put(Alcremie, EnumAlcremie.RibbonRubySwirl);
        formList.put(Alcremie, EnumAlcremie.RibbonSaltedCream);
        formList.put(Alcremie, EnumAlcremie.RibbonVanillaCream);
        formList.put(Alcremie, EnumAlcremie.StarCaramelSwirl);
        formList.put(Alcremie, EnumAlcremie.StarLemonCream);
        formList.put(Alcremie, EnumAlcremie.StarMatchaCream);
        formList.put(Alcremie, EnumAlcremie.StarMintCream);
        formList.put(Alcremie, EnumAlcremie.StarRainbowSwirl);
        formList.put(Alcremie, EnumAlcremie.StarRubyCream);
        formList.put(Alcremie, EnumAlcremie.StarRubySwirl);
        formList.put(Alcremie, EnumAlcremie.StarSaltedCream);
        formList.put(Alcremie, EnumAlcremie.StarVanillaCream);
        formList.put(Alcremie, EnumAlcremie.StrawberryCaramelSwirl);
        formList.put(Alcremie, EnumAlcremie.StrawberryLemonCream);
        formList.put(Alcremie, EnumAlcremie.StrawberryMatchaCream);
        formList.put(Alcremie, EnumAlcremie.StrawberryMintCream);
        formList.put(Alcremie, EnumAlcremie.StrawberryRainbowSwirl);
        formList.put(Alcremie, EnumAlcremie.StrawberryRubyCream);
        formList.put(Alcremie, EnumAlcremie.StrawberryRubySwirl);
        formList.put(Alcremie, EnumAlcremie.StrawberrySaltedCream);
        formList.put(Alcremie, EnumAlcremie.StrawberryVanillaCream);
        formList.put(Wishiwashi, EnumWishiwashi.Solo);
        formList.put(Wishiwashi, EnumWishiwashi.School);
        formList.put(Kyurem, EnumKyurem.Black);
        formList.put(Kyurem, EnumKyurem.White);
        formList.put(Calyrex, EnumCalyrex.Icerider);
        formList.put(Calyrex, EnumCalyrex.Shadowrider);
        formList.put(Necrozma, EnumNecrozma.Dusk);
        formList.put(Necrozma, EnumNecrozma.Dawn);
        formList.put(Necrozma, EnumNecrozma.UltraDawn);
        formList.put(Necrozma, EnumNecrozma.UltraDusk);
        formList.put(Darmanitan, EnumDarmanitan.Normal);
        formList.put(Darmanitan, EnumDarmanitan.Zen);
        formList.put(Darmanitan, EnumDarmanitan.GalarZen);
        formList.put(Landorus, EnumLandorus.Therian);
        formList.put(Thundurus, EnumThundurus.Therian);
        formList.put(Tornadus, EnumTornadus.Therian);
        formList.put(Enamorus, EnumEnamorus.Therian);
        formList.put(Zamazenta, EnumZamazenta.Normal);
        formList.put(Zamazenta, EnumZamazenta.Crowned);
        formList.put(Zacian, EnumZacian.Normal);
        formList.put(Zacian, EnumZacian.Crowned);
        formList.put(Eternatus, EnumEternatus.Normal);
        formList.put(Eternatus, EnumEternatus.Eternamax);
        formList.put(Urshifu, EnumUrshifu.Rapid);
        formList.put(Urshifu, EnumUrshifu.Single);
        formList.put(Aegislash, EnumAegislash.Shield);
        formList.put(Aegislash, EnumAegislash.Blade);
        for (EnumGreninja enum_ : EnumGreninja.values()) {
            formList.put(Greninja, enum_);
        }
        formList.put(Groudon, EnumForms.NoForm);
        formList.put(Kyogre, EnumForms.NoForm);
        formList.put(Groudon, EnumGroudon.Primal);
        formList.put(Kyogre, EnumKyogre.Primal);
        for (i = 0; i < EnumSeason.values().length; ++i) {
            EnumSeason form = EnumSeason.values()[i];
            formList.put(Deerling, form);
            formList.put(Sawsbuck, form);
        }
        for (i = 0; i < EnumUnown.values().length; ++i) {
            EnumUnown form = EnumUnown.values()[i];
            formList.put(Unown, form);
        }
        for (i = 0; i < EnumBurmy.values().length; ++i) {
            EnumBurmy form = EnumBurmy.values()[i];
            formList.put(Burmy, form);
        }
        for (i = 0; i < EnumWormadam.values().length; ++i) {
            EnumWormadam form = EnumWormadam.values()[i];
            formList.put(Wormadam, form);
        }
        for (i = 0; i < EnumCastform.values().length; ++i) {
            EnumCastform form = EnumCastform.values()[i];
            formList.put(Castform, form);
        }
        for (i = 0; i < EnumDeoxys.values().length; ++i) {
            EnumDeoxys form = EnumDeoxys.values()[i];
            formList.put(Deoxys, form);
        }
        for (i = 0; i < EnumToxtricity.values().length; ++i) {
            EnumToxtricity form = EnumToxtricity.values()[i];
            formList.put(Toxtricity, form);
        }
        for (i = 0; i < EnumEiscue.values().length; ++i) {
            EnumEiscue form = EnumEiscue.values()[i];
            formList.put(Eiscue, form);
        }
        for (i = 0; i < EnumSinistea.values().length; ++i) {
            EnumSinistea form = EnumSinistea.values()[i];
            formList.put(Sinistea, form);
        }
        for (i = 0; i < EnumPolteageist.values().length; ++i) {
            EnumPolteageist form = EnumPolteageist.values()[i];
            formList.put(Polteageist, form);
        }
        for (i = 0; i < EnumMorpeko.values().length; ++i) {
            EnumMorpeko form = EnumMorpeko.values()[i];
            formList.put(Morpeko, form);
        }
        for (i = 0; i < EnumShellos.values().length; ++i) {
            EnumShellos form = EnumShellos.values()[i];
            formList.put(Shellos, form);
        }
        for (i = 0; i < EnumGastrodon.values().length; ++i) {
            EnumGastrodon form = EnumGastrodon.values()[i];
            formList.put(Gastrodon, form);
        }
        for (i = 0; i < EnumCherrim.values().length; ++i) {
            EnumCherrim form = EnumCherrim.values()[i];
            formList.put(Cherrim, form);
        }
        for (i = 0; i < EnumRotom.values().length; ++i) {
            EnumRotom form = EnumRotom.values()[i];
            formList.put(Rotom, form);
        }
        for (i = 0; i < EnumGiratina.values().length; ++i) {
            EnumGiratina form = EnumGiratina.values()[i];
            formList.put(Giratina, form);
        }
        for (i = 0; i < EnumDialga.values().length; ++i) {
            EnumDialga form = EnumDialga.values()[i];
            formList.put(Dialga, form);
        }
        for (i = 0; i < EnumPalkia.values().length; ++i) {
            EnumPalkia form = EnumPalkia.values()[i];
            formList.put(Palkia, form);
        }
        for (i = 0; i < EnumShaymin.values().length; ++i) {
            EnumShaymin form = EnumShaymin.values()[i];
            formList.put(Shaymin, form);
        }
        for (i = 0; i < EnumArceus.values().length; ++i) {
            EnumArceus form = EnumArceus.values()[i];
            formList.put(Arceus, form);
        }
        for (i = 0; i < EnumBasculin.values().length; ++i) {
            EnumBasculin form = EnumBasculin.values()[i];
            formList.put(Basculin, form);
        }
        for (i = 0; i < EnumMeloetta.values().length; ++i) {
            EnumMeloetta form = EnumMeloetta.values()[i];
            formList.put(Meloetta, form);
        }
        for (i = 0; i < EnumKeldeo.values().length; ++i) {
            EnumKeldeo form = EnumKeldeo.values()[i];
            formList.put(Keldeo, form);
        }
        for (i = 0; i < EnumGenesect.values().length; ++i) {
            EnumGenesect form = EnumGenesect.values()[i];
            formList.put(Genesect, form);
        }
        for (i = 0; i < EnumVivillon.values().length; ++i) {
            EnumVivillon form = EnumVivillon.values()[i];
            formList.put(Vivillon, form);
        }
        for (i = 0; i < EnumXerneas.values().length; ++i) {
            EnumXerneas form = EnumXerneas.values()[i];
            formList.put(Xerneas, form);
        }
        for (i = 0; i < EnumHoopa.values().length; ++i) {
            EnumHoopa form = EnumHoopa.values()[i];
            formList.put(Hoopa, form);
        }
        for (i = 0; i < EnumFlabebe.values().length; ++i) {
            EnumFlabebe form = EnumFlabebe.values()[i];
            formList.put(Flabebe, form);
            formList.put(Floette, form);
            formList.put(Florges, form);
        }
        for (i = 0; i < EnumSilvally.values().length; ++i) {
            EnumSilvally form = EnumSilvally.values()[i];
            formList.put(Silvally, form);
        }
        for (i = 0; i < EnumOricorio.values().length; ++i) {
            EnumOricorio form = EnumOricorio.values()[i];
            formList.put(Oricorio, form);
        }
        for (i = 0; i < EnumFurfrou.values().length; ++i) {
            EnumFurfrou form = EnumFurfrou.values()[i];
            formList.put(Furfrou, form);
        }
        for (EnumWooloo enumWooloo : EnumWooloo.values()) {
            formList.put(Wooloo, enumWooloo);
        }
        for (EnumMareep enum_ : EnumMareep.values()) {
            formList.put(Mareep, enum_);
        }
    }
}

