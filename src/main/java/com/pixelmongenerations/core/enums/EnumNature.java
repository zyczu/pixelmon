/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumFlavor;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import net.minecraft.util.text.translation.I18n;

public enum EnumNature {
    Hardy(0, EnumFlavor.None, EnumFlavor.None),
    Serious(1, EnumFlavor.None, EnumFlavor.None),
    Docile(2, EnumFlavor.None, EnumFlavor.None),
    Bashful(3, EnumFlavor.None, EnumFlavor.None),
    Quirky(4, EnumFlavor.None, EnumFlavor.None),
    Lonely(5, EnumFlavor.Spicy, EnumFlavor.Sour),
    Brave(6, EnumFlavor.Spicy, EnumFlavor.Sweet),
    Adamant(7, EnumFlavor.Spicy, EnumFlavor.Dry),
    Naughty(8, EnumFlavor.Spicy, EnumFlavor.Bitter),
    Bold(9, EnumFlavor.Sour, EnumFlavor.Spicy),
    Relaxed(10, EnumFlavor.Sour, EnumFlavor.Sweet),
    Impish(11, EnumFlavor.Sour, EnumFlavor.Dry),
    Lax(12, EnumFlavor.Sour, EnumFlavor.Bitter),
    Timid(13, EnumFlavor.Sweet, EnumFlavor.Spicy),
    Hasty(14, EnumFlavor.Sweet, EnumFlavor.Sour),
    Jolly(15, EnumFlavor.Sweet, EnumFlavor.Dry),
    Naive(16, EnumFlavor.Sweet, EnumFlavor.Bitter),
    Modest(17, EnumFlavor.Dry, EnumFlavor.Spicy),
    Mild(18, EnumFlavor.Dry, EnumFlavor.Sour),
    Quiet(19, EnumFlavor.Dry, EnumFlavor.Sweet),
    Rash(20, EnumFlavor.Dry, EnumFlavor.Bitter),
    Calm(21, EnumFlavor.Bitter, EnumFlavor.Spicy),
    Gentle(22, EnumFlavor.Bitter, EnumFlavor.Sour),
    Sassy(23, EnumFlavor.Bitter, EnumFlavor.Sweet),
    Careful(24, EnumFlavor.Bitter, EnumFlavor.Dry);

    public StatsType increasedStat;
    public StatsType decreasedStat;
    public int index;
    public String name;

    private EnumNature(int index, EnumFlavor liked, EnumFlavor disliked) {
        this.index = index;
        this.increasedStat = liked.getType();
        this.decreasedStat = disliked.getType();
    }

    public static EnumNature getNatureFromIndex(int index) {
        try {
            return EnumNature.values()[index];
        }
        catch (Exception npe) {
            return null;
        }
    }

    public static EnumNature getRandomNature() {
        int rndm = RandomHelper.rand.nextInt(25);
        return EnumNature.getNatureFromIndex(rndm);
    }

    public static boolean hasNature(String name) {
        try {
            return EnumNature.natureFromString(name) != null;
        }
        catch (Exception e) {
            return false;
        }
    }

    public static EnumNature natureFromString(String name) {
        try {
            return EnumNature.valueOf(name);
        }
        catch (Exception e) {
            for (EnumNature nature : EnumNature.values()) {
                if (!name.equalsIgnoreCase(nature.toString()) && !name.equalsIgnoreCase(nature.getLocalizedName())) continue;
                return nature;
            }
            return null;
        }
    }

    public static EnumNature getNatureFromStartingString(String name) {
        for (EnumNature type : EnumNature.values()) {
            if (type.name == null) {
                type.name = type.name().toLowerCase();
            }
            if (!type.name.startsWith(name)) continue;
            return type;
        }
        return null;
    }

    public String getLocalizedName() {
        return I18n.translateToLocal("enum.nature." + this.toString().toLowerCase());
    }
}

