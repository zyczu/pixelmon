/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.enums;

import com.pixelmongenerations.client.models.obj.ObjLoader;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.client.models.smd.ValveStudioModelLoader;
import com.pixelmongenerations.common.cosmetic.PositionOperation;
import java.util.HashMap;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModel;

public enum EnumCustomModel {
    PillarPlatform("blocks/pillar/pillar_platform.obj", new PositionOperation[0]),
    PillarColumn("blocks/pillar/pillar_column.obj", new PositionOperation[0]),
    PillarColumnFracturedBottom("blocks/pillar/pillar_column_fractured_bottom.obj", new PositionOperation[0]),
    PillarColumnFracturedTop("blocks/pillar/pillar_column_fractured_top.obj", new PositionOperation[0]),
    Pokeball("pokeballs/base.pqc", new PositionOperation[0]),
    Cherishball("pokeballs/cherishball.pqc", new PositionOperation[0]),
    Greatball("pokeballs/greatball.pqc", new PositionOperation[0]),
    Heavyball("pokeballs/heavyball.pqc", new PositionOperation[0]),
    Masterball("pokeballs/masterball.pqc", new PositionOperation[0]),
    Netball("pokeballs/netball.pqc", new PositionOperation[0]),
    Timerball("pokeballs/timerball.pqc", new PositionOperation[0]),
    Beastball("pokeballs/beastball.pqc", new PositionOperation[0]),
    Sash("playeritems/sash/sash.pqc", new PositionOperation[0]),
    Fez("playeritems/hats/fez.pqc", new PositionOperation[0]),
    Fedora("playeritems/hats/fedora.pqc", new PositionOperation[0]),
    TopHat("playeritems/hats/tophat.pqc", new PositionOperation[0]),
    Monocle("playeritems/hats/monocle.pqc", new PositionOperation[0]),
    TrainerHat("playeritems/hats/trainerhat.pqc", new PositionOperation[0]),
    PikaHood("playeritems/hats/pikahood.pqc", new PositionOperation[0]),
    MegaBraceletORAS("playeritems/megaitems/megabraceletoras.pqc", new PositionOperation[0]),
    MegaBraceletORASStone("playeritems/megaitems/megabraceletorasstone.pqc", new PositionOperation[0]),
    DynamaxBand("playeritems/bands/dynamaxband.pqc", new PositionOperation[0]),
    Backpack("cosmetics/back/backpack/basic.pqc", new PositionOperation[0]),
    CharizardBackpack("cosmetics/back/backpack/charizard.pqc", new PositionOperation[0]),
    EeveeBackpack("cosmetics/back/backpack/eevee.pqc", new PositionOperation[0]),
    GengarBackpack("cosmetics/back/backpack/gengar.pqc", new PositionOperation[0]),
    HoopaBackpack("cosmetics/back/backpack/hoopa.pqc", new PositionOperation[0]),
    MegaGengarBackpack("cosmetics/back/backpack/megagengar.pqc", new PositionOperation[0]),
    PikachuBackpack("cosmetics/back/backpack/pikachu.pqc", new PositionOperation[0]),
    SportyBackpack("cosmetics/back/backpack/sportybackpack.pqc", PositionOperation.translate(0.0f, 2.2f, -0.05f), PositionOperation.scale(0.02f, 0.02f, 0.02f)),
    LeatherBackpack("cosmetics/back/backpack/leatherbackpack.pqc", PositionOperation.translate(0.0f, 2.2f, -0.05f), PositionOperation.scale(0.02f, 0.02f, 0.02f)),
    ScoutBackpack("cosmetics/back/backpack/scoutbackpack.pqc", PositionOperation.translate(0.0f, 2.2f, -0.05f), PositionOperation.scale(0.02f, 0.02f, 0.02f)),
    Cap("cosmetics/head/cap.pqc", new PositionOperation[0]),
    Pokepack("cosmetics/back/backpack/pokepack.pqc", new PositionOperation[0]),
    StrawHat("cosmetics/head/strawhat.pqc", new PositionOperation[0]),
    Crown("cosmetics/head/crown.pqc", new PositionOperation[0]),
    DetectiveHat("cosmetics/head/detectivehat.pqc", new PositionOperation[0]),
    CowboyHat("cosmetics/head/cowboyhat.pqc", PositionOperation.translate(0.0f, 1.49f, 0.0f)),
    LetsGoCap("cosmetics/head/letsgocap.pqc", PositionOperation.scale(0.038f, 0.04f, 0.045f), PositionOperation.translate(0.0f, -12.0f, 0.35f), PositionOperation.rotate(90.0f, -1.0f, 0.0f, 0.0f)),
    ThickGlasses("cosmetics/face/thickglasses.pqc", PositionOperation.scale(0.015f, 0.015f, 0.015f), PositionOperation.translate(0.0f, -12.0f, -15.3f)),
    Sunglasses("cosmetics/face/sunglasses.pqc", PositionOperation.scale(0.015f, 0.015f, 0.015f), PositionOperation.translate(0.0f, -12.0f, -15.3f)),
    Flowers("cosmetics/head/flower.pqc", PositionOperation.translate(0.2f, -0.45f, -0.27f), PositionOperation.scale(0.01f, 0.01f, 0.01f)),
    AviatorShades("cosmetics/face/aviatorshades.pqc", PositionOperation.scale(0.02f, 0.02f, 0.02f), PositionOperation.translate(0.0f, -8.25f, 16.3f)),
    EggHat("cosmetics/head/egghat.pqc", new PositionOperation[0]),
    DragonairHat("cosmetics/head/dragonairhat.pqc", PositionOperation.scale(0.5f, 0.5f, 0.5f), PositionOperation.translate(0.0f, -3.0f, 0.0f), PositionOperation.rotate(90.0f, -1.0f, 0.0f, 0.0f)),
    VenusaurFlower("cosmetics/back/venusaurflower.pqc", PositionOperation.scale(0.4f, 0.4f, 0.4f), PositionOperation.translate(0.0f, -2.7f, -0.2f), PositionOperation.rotate(90.0f, -1.0f, 0.0f, 0.0f)),
    VileplumeHat("cosmetics/head/vileplumehat.pqc", PositionOperation.scale(0.2f, 0.2f, 0.2f), PositionOperation.translate(0.0f, -8.0f, 0.0f), PositionOperation.rotate(90.0f, -1.0f, 0.0f, 0.0f)),
    NinetalesTail("cosmetics/back/ninetalestail.pqc", PositionOperation.scale(0.3f, 0.3f, 0.3f), PositionOperation.translate(0.0f, -2.5f, 0.0f), PositionOperation.rotate(90.0f, -1.0f, 0.0f, 0.0f)),
    ButterfreeWings("cosmetics/back/butterfreewings.pqc", PositionOperation.scale(0.15f, 0.15f, 0.15f), PositionOperation.translate(0.0f, -7.5f, 0.18f), PositionOperation.rotate(90.0f, -1.0f, 0.0f, 0.0f)),
    BellossomFlowers("cosmetics/head/bellossomflowers.pqc", PositionOperation.scale(0.3f, 0.3f, 0.3f), PositionOperation.translate(0.0f, -4.8f, -0.3f), PositionOperation.rotate(90.0f, -1.0f, 0.0f, 0.0f)),
    KlangGears("cosmetics/head/klanggears.pqc", PositionOperation.scale(0.25f, 0.25f, 0.25f), PositionOperation.translate(0.0f, -6.0f, 0.0f), PositionOperation.rotate(90.0f, -1.0f, 0.0f, 0.0f)),
    RaltsHead("cosmetics/head/raltshead.pqc", PositionOperation.scale(0.3f, 0.3f, 0.3f), PositionOperation.translate(0.0f, -4.5f, 0.0f), PositionOperation.rotate(90.0f, -1.0f, 0.0f, 0.0f)),
    AegislashShield("cosmetics/back/aegislashshield.pqc", PositionOperation.scale(0.4f, 0.4f, 0.4f), PositionOperation.translate(0.0f, -1.75f, -0.25f), PositionOperation.rotate(90.0f, -1.0f, 0.0f, 0.0f)),
    ArceusHead("cosmetics/head/arceushead.pqc", PositionOperation.scale(0.3f, 0.3f, 0.3f), PositionOperation.translate(0.0f, -4.8f, -0.6f), PositionOperation.rotate(90.0f, -1.0f, 0.0f, 0.0f));

    String fileName;
    public IModel theModel;
    public PositionOperation[] operations;
    private boolean initialised = false;
    public static HashMap<String, ResourceLocation> skins;
    public static HashMap<String, IModel> cachedModels;

    private EnumCustomModel(String fileName, PositionOperation ... operations) {
        this.fileName = fileName;
        this.operations = operations;
    }

    public IModel getModel() {
        if (!this.initialised) {
            try {
                ResourceLocation rl = new ResourceLocation("pixelmon:models/" + this.fileName);
                if (ValveStudioModelLoader.instance.accepts(rl)) {
                    this.theModel = ValveStudioModelLoader.instance.loadModel(rl);
                } else if (ObjLoader.accepts(rl)) {
                    this.theModel = ObjLoader.loadModel(rl);
                }
            }
            catch (Exception var2) {
                System.out.println("Could not load the model: " + this.fileName);
                var2.printStackTrace();
            }
            this.initialised = true;
        }
        if (!cachedModels.containsKey(this.name())) {
            cachedModels.put(this.name(), this.theModel instanceof ValveStudioModel ? new ValveStudioModel((ValveStudioModel)this.theModel) : this.theModel);
        }
        return cachedModels.get(this.name());
    }

    public PositionOperation[] getOperaitons() {
        return this.operations;
    }

    public static EnumCustomModel getFromString(String customModel) {
        for (EnumCustomModel enumCustomModel : EnumCustomModel.values()) {
            if (!enumCustomModel.name().equalsIgnoreCase(customModel)) continue;
            return enumCustomModel;
        }
        return null;
    }

    static {
        skins = new HashMap();
        cachedModels = new HashMap();
    }
}

