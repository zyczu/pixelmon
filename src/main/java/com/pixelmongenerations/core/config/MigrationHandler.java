/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import java.util.HashMap;
import java.util.function.Consumer;

public class MigrationHandler {
    public static HashMap<Object, Consumer<CommentedConfigurationNode>> consumers = new HashMap();

    public static boolean handleMigration(CommentedConfigurationNode configNode) {
        if (consumers.containsKey(configNode.getKey())) {
            Pixelmon.LOGGER.info("MigrationHandler: Migrating " + configNode.getKey());
            consumers.get(configNode.getKey()).accept(configNode);
            Pixelmon.LOGGER.info("MigrationHandler: Migrated " + configNode.getKey());
            return true;
        }
        return false;
    }

    static {
        Consumer<CommentedConfigurationNode> useExternalJSONFilesConsumer = configNode -> {
            CommentedConfigurationNode parent = configNode.getParent();
            CommentedConfigurationNode mainNode = PixelmonConfig.getConfig();
            boolean value = configNode.getBoolean();
            Pixelmon.LOGGER.info("MigrationHandler: Applying old value " + value + " of " + configNode.getKey() + " to new nodes.");
            PixelmonConfig.useExternalJSONFilesDrops = mainNode.getNode("General").getNode("useExternalJSONFilesDrops").setValue(value).getBoolean();
            PixelmonConfig.useExternalJSONFilesNPCs = mainNode.getNode("General").getNode("useExternalJSONFilesNPCs").setValue(value).getBoolean();
            PixelmonConfig.useExternalJSONFilesRules = mainNode.getNode("General").getNode("useExternalJSONFilesRules").setValue(value).getBoolean();
            PixelmonConfig.useExternalJSONFilesSpawning = mainNode.getNode("General").getNode("useExternalJSONFilesSpawning").setValue(value).getBoolean();
            PixelmonConfig.useExternalJSONFilesStructures = mainNode.getNode("General").getNode("useExternalJSONFilesStructures").setValue(value).getBoolean();
            parent.removeChild(configNode.getKey());
            PixelmonConfig.saveConfig();
        };
        consumers.put("useExternalJSONFiles", useExternalJSONFilesConsumer);
    }
}

