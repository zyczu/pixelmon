/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.common.block.decorative.BlockUnown;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonBlocksApricornTrees;
import com.pixelmongenerations.core.config.PixelmonBlocksBerryTrees;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsApricorns;
import com.pixelmongenerations.core.config.PixelmonItemsBadges;
import com.pixelmongenerations.core.config.PixelmonItemsBalms;
import com.pixelmongenerations.core.config.PixelmonItemsCurryIngredients;
import com.pixelmongenerations.core.config.PixelmonItemsFossils;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.config.PixelmonItemsMail;
import com.pixelmongenerations.core.config.PixelmonItemsPokeballs;
import com.pixelmongenerations.core.config.PixelmonItemsPokeblocks;
import com.pixelmongenerations.core.config.PixelmonItemsTMs;
import com.pixelmongenerations.core.config.PixelmonItemsTools;
import com.pixelmongenerations.core.config.PixelmonOres;
import com.pixelmongenerations.core.config.TileEntityRegistry;
import com.pixelmongenerations.core.config.UnownBlockRecipe;
import com.pixelmongenerations.core.util.PixelSounds;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.EntityEntry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

@Mod.EventBusSubscriber
public class RegistryListener {
    @SubscribeEvent
    public static void onRegisterBlocks(RegistryEvent.Register<Block> event) {
        PixelmonBlocks.load();
        PixelmonBlocksBerryTrees.load();
        PixelmonBlocksApricornTrees.load();
        PixelmonBlocks.registerBlocks(event);
        PixelmonBlocksBerryTrees.registerBlocks(event);
        RegistryListener.registerAllFields(PixelmonBlocksApricornTrees.class, Block.class, event.getRegistry());
        TileEntityRegistry.registerTileEntities();
    }

    @SubscribeEvent
    public static void onRegisterItems(RegistryEvent.Register<Item> event) {
        PixelmonItems.load();
        PixelmonItemsHeld.load();
        PixelmonItemsTools.load();
        PixelmonItemsBadges.load();
        PixelmonItemsFossils.load();
        PixelmonItemsPokeballs.load();
        PixelmonItemsBalms.load();
        PixelmonItemsPokeblocks.load();
        PixelmonItemsApricorns.load();
        PixelmonItemsCurryIngredients.load();
        PixelmonBlocks.registerSlabItems(event);
        RegistryListener.registerAllFields(PixelmonItems.class, Item.class, event.getRegistry());
        RegistryListener.registerAllFields(PixelmonItemsHeld.class, Item.class, event.getRegistry());
        RegistryListener.registerAllFields(PixelmonItemsTools.class, Item.class, event.getRegistry());
        RegistryListener.registerAllFields(PixelmonItemsBadges.class, Item.class, event.getRegistry());
        RegistryListener.registerAllFields(PixelmonItemsFossils.class, Item.class, event.getRegistry());
        RegistryListener.registerAllFields(PixelmonItemsPokeballs.class, Item.class, event.getRegistry());
        RegistryListener.registerAllFields(PixelmonItemsBalms.class, Item.class, event.getRegistry());
        RegistryListener.registerAllFields(PixelmonItemsPokeblocks.class, Item.class, event.getRegistry());
        RegistryListener.registerAllFields(PixelmonItemsApricorns.class, Item.class, event.getRegistry());
        RegistryListener.registerAllFields(PixelmonItemsCurryIngredients.class, Item.class, event.getRegistry());
        PixelmonItemsTMs.registerItems(event);
        PixelmonItemsMail.registerMailItems(event);
        Pixelmon.PROXY.fixModelDefs();
        Pixelmon.PROXY.registerBlockModels();
        PixelmonOres.populateOres();
        PixelmonOres.registerOres();
        OreDictionary.registerOre("blockBraille", new ItemStack(PixelmonBlocks.blockBraille, 1, 32767));
        OreDictionary.registerOre("blockBraille", new ItemStack(PixelmonBlocks.blockBraille2, 1, 32767));
    }

    @SubscribeEvent
    public static void onRegisterRecipes(RegistryEvent.Register<IRecipe> event) {
        for (int i = 0; i < BlockUnown.getNumUnownBlocks(); ++i) {
            event.getRegistry().register(new UnownBlockRecipe(i));
        }
        FurnaceRecipes furnaceRecipes = FurnaceRecipes.instance();
        PixelmonOres.registerFurnaceRecipes(furnaceRecipes);
        PixelmonItemsApricorns.registerFurnaceRecipes(furnaceRecipes);
    }

    @SubscribeEvent
    public static void onRegisterEntities(RegistryEvent.Register<EntityEntry> event) {
        PixelmonEntityList.registerEntities();
    }

    @SubscribeEvent
    public static void onRegisterSounds(RegistryEvent.Register<SoundEvent> event) {
        PixelSounds.registerSounds(event.getRegistry());
    }

    @SubscribeEvent
    public static void onRegisterModels(ModelRegistryEvent event) {
        PixelmonItems.registerRenderers();
        RegistryListener.registerItemRenderersByFields(PixelmonItemsHeld.class);
        RegistryListener.registerItemRenderersByFields(PixelmonItemsTools.class);
        RegistryListener.registerItemRenderersByFields(PixelmonItemsBadges.class);
        RegistryListener.registerItemRenderersByFields(PixelmonItemsBalms.class);
        RegistryListener.registerItemRenderersByFields(PixelmonItemsPokeblocks.class);
        RegistryListener.registerItemRenderersByFields(PixelmonItemsFossils.class);
        RegistryListener.registerItemRenderersByFields(PixelmonItemsPokeballs.class);
        RegistryListener.registerItemRenderersByFields(PixelmonItemsApricorns.class);
        RegistryListener.registerItemRenderersByFields(PixelmonItemsCurryIngredients.class);
        PixelmonItemsTMs.registerRenderers();
        PixelmonItemsMail.registerRenderers();
    }

    private static <T extends IForgeRegistryEntry<T>> void registerAllFields(Class clazzWithFields, Class<T> type, IForgeRegistry<T> registry) {
        try {
            for (Field field : clazzWithFields.getFields()) {
                if (!Modifier.isPublic(field.getModifiers()) || !Modifier.isStatic(field.getModifiers()) || !type.isInstance(field.get(null))) continue;
                registry.register((T) field.get(null));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SideOnly(value=Side.CLIENT)
    private static void registerItemRenderersByFields(Class clazzWithFields) {
        try {
            for (Field field : clazzWithFields.getFields()) {
                if (!Modifier.isPublic(field.getModifiers()) || !Modifier.isStatic(field.getModifiers()) || !(field.get(null) instanceof Item)) continue;
                Item item = (Item)field.get(null);
                ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(item.getRegistryName(), "inventory"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}

