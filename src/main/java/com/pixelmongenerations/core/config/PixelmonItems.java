/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.api.pokemon.SpawnPokemon;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.block.tileEntities.TileEntityGenerationsShrine;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.DecreaseEV;
import com.pixelmongenerations.common.item.EnumIsisHourglassType;
import com.pixelmongenerations.common.item.HealFixed;
import com.pixelmongenerations.common.item.HealFraction;
import com.pixelmongenerations.common.item.IncreaseEV;
import com.pixelmongenerations.common.item.ItemAbilityCapsule;
import com.pixelmongenerations.common.item.ItemAbilityPatch;
import com.pixelmongenerations.common.item.ItemBattleItem;
import com.pixelmongenerations.common.item.ItemBike;
import com.pixelmongenerations.common.item.ItemCamera;
import com.pixelmongenerations.common.item.ItemCandyBag;
import com.pixelmongenerations.common.item.ItemCurry;
import com.pixelmongenerations.common.item.ItemCustomIcon;
import com.pixelmongenerations.common.item.ItemDimension;
import com.pixelmongenerations.common.item.ItemElixir;
import com.pixelmongenerations.common.item.ItemEther;
import com.pixelmongenerations.common.item.ItemEvolutionStone;
import com.pixelmongenerations.common.item.ItemExpAll;
import com.pixelmongenerations.common.item.ItemFadedOrb;
import com.pixelmongenerations.common.item.ItemFeather;
import com.pixelmongenerations.common.item.ItemFishingRod;
import com.pixelmongenerations.common.item.ItemFlute;
import com.pixelmongenerations.common.item.ItemGift;
import com.pixelmongenerations.common.item.ItemGracidea;
import com.pixelmongenerations.common.item.ItemHiddenDoor;
import com.pixelmongenerations.common.item.ItemIsisHourglass;
import com.pixelmongenerations.common.item.ItemItemFinder;
import com.pixelmongenerations.common.item.ItemJuice;
import com.pixelmongenerations.common.item.ItemLegendFinder;
import com.pixelmongenerations.common.item.ItemLevelCandy;
import com.pixelmongenerations.common.item.ItemMagmaCrystal;
import com.pixelmongenerations.common.item.ItemMeltanBox;
import com.pixelmongenerations.common.item.ItemMulch;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import com.pixelmongenerations.common.item.ItemPPUp;
import com.pixelmongenerations.common.item.ItemPesticide;
import com.pixelmongenerations.common.item.ItemPicketFence;
import com.pixelmongenerations.common.item.ItemPixelmonPainting;
import com.pixelmongenerations.common.item.ItemPixelmonRecord;
import com.pixelmongenerations.common.item.ItemPixelmonSprite;
import com.pixelmongenerations.common.item.ItemPokeMint;
import com.pixelmongenerations.common.item.ItemPokePuff;
import com.pixelmongenerations.common.item.ItemPokedex;
import com.pixelmongenerations.common.item.ItemPokemonEditor;
import com.pixelmongenerations.common.item.ItemPotion;
import com.pixelmongenerations.common.item.ItemRanchUpgrade;
import com.pixelmongenerations.common.item.ItemRepel;
import com.pixelmongenerations.common.item.ItemRevive;
import com.pixelmongenerations.common.item.ItemRotomCatalog;
import com.pixelmongenerations.common.item.ItemRuby;
import com.pixelmongenerations.common.item.ItemSecretArmorScroll;
import com.pixelmongenerations.common.item.ItemShrineOrbWithInfo;
import com.pixelmongenerations.common.item.ItemSpawnGrotto;
import com.pixelmongenerations.common.item.ItemStatueMaker;
import com.pixelmongenerations.common.item.ItemStatusAilmentHealer;
import com.pixelmongenerations.common.item.ItemTimeGlass;
import com.pixelmongenerations.common.item.ItemWailmerPail;
import com.pixelmongenerations.common.item.ItemZoneWand;
import com.pixelmongenerations.common.item.ItemZygardeCube;
import com.pixelmongenerations.common.item.MedicinePotion;
import com.pixelmongenerations.common.item.MedicineRevive;
import com.pixelmongenerations.common.item.MedicineStatus;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.common.item.PixelmonItemBlock;
import com.pixelmongenerations.common.item.PixelmonItemFood;
import com.pixelmongenerations.common.item.PorygonPiece;
import com.pixelmongenerations.common.item.ShrineItem;
import com.pixelmongenerations.common.item.TaoTrioStone;
import com.pixelmongenerations.common.item.heldItems.ItemRoomService;
import com.pixelmongenerations.common.item.server.ServerItem;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonBlocksApricornTrees;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.config.PixelmonItemsApricorns;
import com.pixelmongenerations.core.config.PixelmonItemsBadges;
import com.pixelmongenerations.core.config.PixelmonItemsBalms;
import com.pixelmongenerations.core.config.PixelmonItemsFossils;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.config.PixelmonItemsMail;
import com.pixelmongenerations.core.config.PixelmonItemsPokeballs;
import com.pixelmongenerations.core.config.PixelmonItemsTMs;
import com.pixelmongenerations.core.config.PixelmonItemsTools;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.EnumDecreaseEV;
import com.pixelmongenerations.core.enums.EnumEvolutionStone;
import com.pixelmongenerations.core.enums.EnumIncreaseEV;
import com.pixelmongenerations.core.enums.EnumMulch;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.EnumBattleItems;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.items.EnumBike;
import com.pixelmongenerations.core.enums.items.EnumEXPAmount;
import com.pixelmongenerations.core.enums.items.EnumFlutes;
import com.pixelmongenerations.core.enums.items.EnumJuice;
import com.pixelmongenerations.core.enums.items.EnumRodType;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.util.PixelSounds;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PixelmonItems {
    public static Map<String, Item> allItemMap = null;
    public static ArrayList<Item> allItemList = null;
    public static Item itemFinder;
    public static Item rareCandy;
    public static Item dynamaxCandy;
    public static Item temporaryDimensionItem;
    public static Item potion;
    public static Item superPotion;
    public static Item hyperPotion;
    public static Item maxPotion;
    public static Item freshWater;
    public static Item sodaPop;
    public static Item lemonade;
    public static Item moomooMilk;
    public static Item ether;
    public static Item maxEther;
    public static Item elixir;
    public static Item maxElixir;
    public static Item fullRestore;
    public static Item revive;
    public static Item maxRevive;
    public static Item antidote;
    public static Item parlyzHeal;
    public static Item awakening;
    public static Item burnHeal;
    public static Item iceHeal;
    public static Item fullHeal;
    public static Item HpUp;
    public static Item Protein;
    public static Item Iron;
    public static Item Calcium;
    public static Item Zinc;
    public static Item Carbos;
    public static Item pomegBerry;
    public static Item kelpsyBerry;
    public static Item qualotBerry;
    public static Item hondewBerry;
    public static Item grepaBerry;
    public static Item tamatoBerry;
    public static Item healPowder;
    public static Item energyPowder;
    public static Item energyRoot;
    public static Item revivalHerb;
    public static Item xAttack;
    public static Item xDefence;
    public static Item xSpecialAttack;
    public static Item xSpecialDefence;
    public static Item xSpeed;
    public static Item xAccuracy;
    public static Item direHit;
    public static Item guardSpec;
    public static Item fireStone;
    public static Item waterStone;
    public static Item moonStone;
    public static Item thunderStone;
    public static Item leafStone;
    public static Item sunStone;
    public static Item dawnStone;
    public static Item duskStone;
    public static Item shinyStone;
    public static Item blackAugurite;
    public static Item linkingCord;
    public static Item peatBlock;
    public static Item thunderStoneShard;
    public static Item leafStoneShard;
    public static Item waterStoneShard;
    public static Item fireStoneShard;
    public static Item sunStoneShard;
    public static Item moonStoneShard;
    public static Item dawnStoneShard;
    public static Item duskStoneShard;
    public static Item shinyStoneShard;
    public static Item sweetApple;
    public static Item tartApple;
    public static Item galaricaCuff;
    public static Item galaricaWreath;
    public static Item crackedPot;
    public static Item chippedPot;
    public static Item starSweet;
    public static Item strawberrySweet;
    public static Item berrySweet;
    public static Item cloverSweet;
    public static Item flowerSweet;
    public static Item loveSweet;
    public static Item ribbonSweet;
    public static Item wailmerPail;
    public static Item oldRod;
    public static Item goodRod;
    public static Item superRod;
    public static Item chisel;
    public static Item ranchUpgrade;
    public static Item tradeMonitor;
    public static Item tradeHolderRight;
    public static Item LtradeHolderLeft;
    public static Item tradePanel;
    public static Item orb;
    public static Item unoOrb;
    public static Item dosOrb;
    public static Item tresOrb;
    public static Item mysticalOrb;
    public static Item martialOrb;
    public static Item malevolentOrb;
    public static Item trainerEditor;
    public static Item pokemonEditor;
    public static Item grottoSpawner;
    public static Item cloningMachineGreenItem;
    public static Item cloningMachineOrangeItem;
    public static Item cloningMachineCordItem;
    public static Item bikeFrame;
    public static Item bikeHandlebars;
    public static Item bikeSeat;
    public static Item bikeWheel;
    public static Item hourglassGold;
    public static Item hourglassSilver;
    public static Item hiddenIronDoorItem;
    public static Item hiddenWoodenDoorItem;
    public static Item gift;
    public static Item candy;
    public static Item itemPixelmonSprite;
    public static Item itemCustomIcon;
    public static Item pixelmonPaintingItem;
    public static Item cameraItem;
    public static Item filmItem;
    public static ArrayList<Item> potionElixirList;
    public static ArrayList<Item> evostoneList;
    public static Item treeItem;
    public static Item amethyst;
    public static Item ruby;
    public static Item crystal;
    public static Item sapphire;
    public static Item siliconItem;
    public static Item picketFenceItem;
    public static Item aluminiumIngot;
    public static Item aluminiumPlate;
    public static Item abilityCapsule;
    public static Item abilityPatch;
    public static Item expAll;
    public static Item meteorite;
    public static Item gracidea;
    public static Item reveal_glass;
    public static Item mirror;
    public static Item redchain;
    public static Item repel;
    public static Item superRepel;
    public static Item maxRepel;
    public static Item dnaSplicers;
    public static Item reinsOfUnity;
    public static Item corruptedGem;
    public static Item nSolarizer;
    public static Item nLunarizer;
    public static Item porygonPieces;
    public static Item legendFinder;
    public static Item fadedRedOrb;
    public static Item fadedBlueOrb;
    public static Item rockStarCostume;
    public static Item belleCostume;
    public static Item popStarCostume;
    public static Item phdCostume;
    public static Item libreCostume;
    public static Item mewtwoArmor;
    public static Item balmMushroom;
    public static Item bigMushroom;
    public static Item bigNugget;
    public static Item bigPearl;
    public static Item goldLeaf;
    public static Item smallBouquet;
    public static Item blueShard;
    public static Item cometShard;
    public static Item greenShard;
    public static Item nugget;
    public static Item pearl;
    public static Item pearlString;
    public static Item redShard;
    public static Item starPiece;
    public static Item tinyMushroom;
    public static Item yellowShard;
    public static Item iceStone;
    public static Item iceStoneShard;
    public static Item pinkNectar;
    public static Item purpleNectar;
    public static Item redNectar;
    public static Item yellowNectar;
    public static Item ppUp;
    public static Item maxPp;
    public static Item redBike;
    public static Item orangeBike;
    public static Item yellowBike;
    public static Item greenBike;
    public static Item blueBike;
    public static Item purpleBike;
    public static Item sunFlute;
    public static Item moonFlute;
    public static Item rustyFragment;
    public static Item rustySword;
    public static Item rustyShield;
    public static Item mintSerious;
    public static Item mintLonely;
    public static Item mintBrave;
    public static Item mintAdamant;
    public static Item mintNaughty;
    public static Item mintBold;
    public static Item mintRelaxed;
    public static Item mintImpish;
    public static Item mintGentle;
    public static Item mintLax;
    public static Item mintTimid;
    public static Item mintHasty;
    public static Item mintJolly;
    public static Item mintNaive;
    public static Item mintModest;
    public static Item mintMild;
    public static Item mintQuiet;
    public static Item mintRash;
    public static Item mintCalm;
    public static Item mintSassy;
    public static Item mintCareful;
    public static Item expCandyXS;
    public static Item expCandyS;
    public static Item expCandyM;
    public static Item expCandyL;
    public static Item expCandyXL;
    public static Item zygardeCube;
    public static Item meltanBox;
    public static Item lureModule;
    public static Item timeGlass;
    public static Item rotomCatalog;
    public static Item jewelOfLife;
    public static Item basicSweetPokePuff;
    public static Item basicMintPokePuff;
    public static Item basicCitrusPokePuff;
    public static Item basicMochaPokePuff;
    public static Item basicSpicePokePuff;
    public static Item roomService;
    public static Item pokedex;
    public static Item hoopaRing;
    public static Item zoneWand;
    public static Item lavaCrystal;
    public static Item magmaCrystal;
    public static Item relicSongRecord;
    public static Item shatteredRelicSong1;
    public static Item shatteredRelicSong2;
    public static Item shatteredRelicSong3;
    public static Item shatteredRelicSong4;
    public static Item rockPeakKey;
    public static Item icebergKey;
    public static Item ironKey;
    public static Item dragoKey;
    public static Item elekiKey;
    public static Item shatteredIceKey1;
    public static Item shatteredIceKey2;
    public static Item shatteredIceKey3;
    public static Item shatteredIceKey4;
    public static Item crumbledRockKey1;
    public static Item crumbledRockKey2;
    public static Item crumbledRockKey3;
    public static Item crumbledRockKey4;
    public static Item rustyIronKey1;
    public static Item rustyIronKey2;
    public static Item rustyIronKey3;
    public static Item rustyIronKey4;
    public static Item fragmentedDragoKey1;
    public static Item fragmentedDragoKey2;
    public static Item fragmentedDragoKey3;
    public static Item fragmentedDragoKey4;
    public static Item dischargedElekiKey1;
    public static Item dischargedElekiKey2;
    public static Item dischargedElekiKey3;
    public static Item dischargedElekiKey4;
    public static Item scrollPage;
    public static Item secretArmorScroll;
    public static Item lightStone;
    public static Item dragonStone;
    public static Item darkStone;
    public static Item generationsOrb;
    public static Item rainbowWing;
    public static Item darkSoul;
    public static Item dragonSoul;
    public static Item sparklingStone;
    public static Item sparklingShard;
    public static Item melodyFlute;
    public static Item curry;
    public static Item lavaCookie;
    public static Item rageCandyBar;
    public static Item bigMalasada;
    public static Item oldGateau;
    public static Item casteliacone;
    public static Item lumioseGalette;
    public static Item shalourSable;
    public static Item bottleCap;
    public static Item goldBottleCap;
    public static Item relicGold;
    public static Item relicCopper;
    public static Item relicBand;
    public static Item relicSilver;
    public static Item relicStatue;
    public static Item relicVase;
    public static Item relicCrown;
    public static Item heartScale;
    public static Item record1;
    public static Item record2;
    public static Item record3;
    public static Item record4;
    public static Item record5;
    public static Item record6;
    public static Item record7;
    public static Item record8;
    public static Item record9;
    public static Item record10;
    public static Item record11;
    public static Item record12;
    public static Item record13;
    public static Item record14;
    public static Item record15;
    public static Item record16;
    public static Item record17;
    public static Item record18;
    public static Item record19;
    public static Item record20;
    public static Item record21;
    public static Item zIngot;
    public static Item expCharm;
    public static Item blueFlute;
    public static Item yellowFlute;
    public static Item redFlute;
    public static Item shoalShell;
    public static Item shoalSalt;
    public static Item healthFeather;
    public static Item muscleFeather;
    public static Item resistFeather;
    public static Item geniusFeather;
    public static Item cleverFeather;
    public static Item swiftFeather;
    public static Item prettyFeather;
    public static Item growthMulch;
    public static Item dampMulch;
    public static Item stableMulch;
    public static Item gooeyMulch;
    public static Item amazeMulch;
    public static Item boostMulch;
    public static Item richMulch;
    public static Item surpriseMulch;
    public static Item pesticde;
    public static Item beachGlass;
    public static Item chalkyStone;
    public static Item loneEarring;
    public static Item marble;
    public static Item polishedMudBall;
    public static Item tropicalShell;
    public static Item stretchySpring;
    public static Item catchingCharm;
    public static Item stardust;
    public static Item rareBone;
    public static Item sweetHeart;
    public static Item galaricaTwig;
    public static Item silverLeaf;
    public static Item strangeSouvenir;
    public static Item maxHoney;
    public static Item maxSoup;
    public static Item slowpokeTail;
    public static Item honey;
    public static Item maxMushrooms;
    public static Item maxPowder;
    public static Item wishingStar;
    public static Item unchargedDynamaxBand;
    public static Item markCharm;
    public static Item ovalCharm;
    public static Item oddKeystone;
    public static Item escapeRope;
    public static Item omelette;
    public static Item serverItem;
    public static Item purpleJuice;
    public static Item redJuice;
    public static Item yellowJuice;
    public static Item blueJuice;
    public static Item greenJuice;
    public static Item pinkJuice;
    public static Item feederBase;
    public static Item totemSticker;
    public static Item kleavorAxe;
    public static Item lilligantFlower;
    public static Item arcanineRock;
    public static Item electrodeLightning;
    public static Item avaluggIce;
    public static Item adamant_crystal;
    public static Item lustrous_globe;
    public static Item griseous_core;

    public static void load() {
        serverItem = new ServerItem();
        curry = new ItemCurry("curry");
        purpleJuice = new ItemJuice(EnumJuice.Purple);
        redJuice = new ItemJuice(EnumJuice.Red);
        yellowJuice = new ItemJuice(EnumJuice.Yellow);
        blueJuice = new ItemJuice(EnumJuice.Blue);
        greenJuice = new ItemJuice(EnumJuice.Green);
        pinkJuice = new ItemJuice(EnumJuice.Pink);
        zIngot = new PixelmonItem("z_ingot").setCreativeTab(CreativeTabs.MATERIALS);
        ovalCharm = new PixelmonItem("oval_charm").setCreativeTab(PixelmonCreativeTabs.keyItems);
        markCharm = new PixelmonItem("mark_charm").setCreativeTab(PixelmonCreativeTabs.keyItems);
        catchingCharm = new PixelmonItem("catching_charm").setCreativeTab(PixelmonCreativeTabs.keyItems);
        bottleCap = new PixelmonItem("bottle_cap").setCreativeTab(CreativeTabs.MISC);
        goldBottleCap = new PixelmonItem("gold_bottle_cap").setCreativeTab(CreativeTabs.MISC);
        relicGold = new PixelmonItem("relic_gold").setCreativeTab(PixelmonCreativeTabs.valueables);
        relicCopper = new PixelmonItem("relic_copper").setCreativeTab(PixelmonCreativeTabs.valueables);
        relicBand = new PixelmonItem("relic_band").setCreativeTab(PixelmonCreativeTabs.valueables);
        relicSilver = new PixelmonItem("relic_silver").setCreativeTab(PixelmonCreativeTabs.valueables);
        relicStatue = new PixelmonItem("relic_statue").setCreativeTab(PixelmonCreativeTabs.valueables);
        relicVase = new PixelmonItem("relic_vase").setCreativeTab(PixelmonCreativeTabs.valueables);
        relicCrown = new PixelmonItem("relic_crown").setCreativeTab(PixelmonCreativeTabs.valueables);
        heartScale = new PixelmonItem("heart_scale").setCreativeTab(PixelmonCreativeTabs.valueables);
        expCharm = new PixelmonItem("exp_charm").setCreativeTab(PixelmonCreativeTabs.keyItems);
        shoalShell = new PixelmonItem("shoal_shell").setCreativeTab(PixelmonCreativeTabs.valueables);
        shoalSalt = new PixelmonItem("shoal_salt").setCreativeTab(PixelmonCreativeTabs.valueables);
        stardust = new PixelmonItem("stardust").setCreativeTab(PixelmonCreativeTabs.valueables);
        rareBone = new PixelmonItem("rare_bone").setCreativeTab(PixelmonCreativeTabs.valueables);
        galaricaTwig = new PixelmonItem("galarica_twig").setCreativeTab(PixelmonCreativeTabs.natural);
        silverLeaf = new PixelmonItem("silver_leaf").setCreativeTab(PixelmonCreativeTabs.valueables);
        strangeSouvenir = new PixelmonItem("strange_souvenir").setCreativeTab(PixelmonCreativeTabs.valueables);
        slowpokeTail = new PixelmonItem("slowpoke_tail").setCreativeTab(PixelmonCreativeTabs.valueables);
        wishingStar = new PixelmonItem("wishing_star").setCreativeTab(PixelmonCreativeTabs.valueables);
        unchargedDynamaxBand = new PixelmonItem("uncharged_dynamax_band").setMaxStackSize(1).setCreativeTab(PixelmonCreativeTabs.valueables);
        oddKeystone = new PixelmonItem("odd_keystone").setCreativeTab(PixelmonCreativeTabs.valueables);
        escapeRope = new PixelmonItem("escape_rope").setCreativeTab(PixelmonCreativeTabs.keyItems);
        omelette = new PixelmonItemFood(8, 0.8f, false, "omelette").setCreativeTab(CreativeTabs.FOOD);
        dynamaxCandy = new PixelmonItem("dynamax_candy").setCreativeTab(PixelmonCreativeTabs.restoration);
        honey = new PixelmonItemFood(4, 1.0f, false, "honey").setCreativeTab(CreativeTabs.MISC);
        maxHoney = new ItemRevive("max_honey", new MedicineRevive(new HealFraction(1.0f)));
        maxMushrooms = new ItemBattleItem(EnumBattleItems.maxMushrooms, "max_mushrooms");
        maxPowder = new PixelmonItem("max_powder").setCreativeTab(CreativeTabs.MISC);
        maxSoup = new PixelmonItem("max_soup").setCreativeTab(CreativeTabs.MISC);
        aluminiumIngot = new PixelmonItem("aluminium_ingot").setCreativeTab(CreativeTabs.MATERIALS);
        aluminiumPlate = new PixelmonItem("aluminium_plate").setCreativeTab(CreativeTabs.MATERIALS);
        rareCandy = new PixelmonItem("rare_candy").setCreativeTab(PixelmonCreativeTabs.restoration);
        temporaryDimensionItem = new ItemDimension("temporary_dimension_item").setCreativeTab(PixelmonCreativeTabs.restoration);
        potion = new ItemPotion("potion", new MedicinePotion(new HealFixed(20)));
        sweetHeart = new ItemPotion("sweet_heart", new MedicinePotion(new HealFixed(20)));
        superPotion = new ItemPotion("super_potion", new MedicinePotion(new HealFixed(60)));
        hyperPotion = new ItemPotion("hyper_potion", new MedicinePotion(new HealFixed(120)));
        maxPotion = new ItemPotion("max_potion", new MedicinePotion(new HealFraction(1.0f)));
        freshWater = new ItemPotion("fresh_water", new MedicinePotion(new HealFixed(30)));
        sodaPop = new ItemPotion("soda_pop", new MedicinePotion(new HealFixed(50)));
        lemonade = new ItemPotion("lemonade", new MedicinePotion(new HealFixed(70)));
        moomooMilk = new ItemPotion("moomoo_milk", new MedicinePotion(new HealFixed(100)));
        revive = new ItemRevive("revive", new MedicineRevive(new HealFraction(0.5f)));
        maxRevive = new ItemRevive("max_revive", new MedicineRevive(new HealFraction(1.0f)));
        ether = new ItemEther("ether", false);
        maxEther = new ItemEther("max_ether", true);
        elixir = new ItemElixir("elixir", false);
        maxElixir = new ItemElixir("max_elixir", true);
        lumioseGalette = new ItemStatusAilmentHealer("lumiose_galette", new MedicineStatus(StatusType.Poison, StatusType.Burn, StatusType.Paralysis, StatusType.Freeze, StatusType.Sleep));
        shalourSable = new ItemStatusAilmentHealer("shalour_sable", new MedicineStatus(StatusType.Poison, StatusType.Burn, StatusType.Paralysis, StatusType.Freeze, StatusType.Sleep));
        casteliacone = new ItemStatusAilmentHealer("casteliacone", new MedicineStatus(StatusType.Poison, StatusType.Burn, StatusType.Paralysis, StatusType.Freeze, StatusType.Sleep));
        oldGateau = new ItemStatusAilmentHealer("old_gateau", new MedicineStatus(StatusType.Poison, StatusType.Burn, StatusType.Paralysis, StatusType.Freeze, StatusType.Sleep));
        bigMalasada = new ItemStatusAilmentHealer("big_malasada", new MedicineStatus(StatusType.Poison, StatusType.Burn, StatusType.Paralysis, StatusType.Freeze, StatusType.Sleep));
        lavaCookie = new ItemStatusAilmentHealer("lava_cookie", new MedicineStatus(StatusType.Burn, StatusType.Freeze, StatusType.Paralysis, StatusType.Poison, StatusType.PoisonBadly, StatusType.Sleep));
        rageCandyBar = new ItemStatusAilmentHealer("rage_candy_bar", new MedicineStatus(StatusType.Burn, StatusType.Freeze, StatusType.Paralysis, StatusType.Poison, StatusType.PoisonBadly, StatusType.Sleep, StatusType.Confusion));
        antidote = new ItemStatusAilmentHealer("antidote", new MedicineStatus(StatusType.Poison, StatusType.PoisonBadly));
        parlyzHeal = new ItemStatusAilmentHealer("paralyze_heal", new MedicineStatus(StatusType.Paralysis));
        awakening = new ItemStatusAilmentHealer("awakening", new MedicineStatus(StatusType.Sleep));
        burnHeal = new ItemStatusAilmentHealer("burn_heal", new MedicineStatus(StatusType.Burn));
        iceHeal = new ItemStatusAilmentHealer("ice_heal", new MedicineStatus(StatusType.Freeze));
        blueFlute = new ItemStatusAilmentHealer("blue_flute", new MedicineStatus(StatusType.Sleep));
        yellowFlute = new ItemStatusAilmentHealer("yellow_flute", new MedicineStatus(StatusType.Confusion));
        redFlute = new ItemStatusAilmentHealer("red_flute", new MedicineStatus(StatusType.Infatuated));
        MedicineStatus allStatuses = new MedicineStatus(StatusType.Burn, StatusType.Confusion, StatusType.Freeze, StatusType.Paralysis, StatusType.Poison, StatusType.PoisonBadly, StatusType.Sleep, StatusType.Infatuated);
        fullHeal = new ItemStatusAilmentHealer("full_heal", allStatuses);
        fullRestore = new ItemPotion("full_restore", new MedicinePotion(new HealFraction(1.0f)), allStatuses);
        healthFeather = new ItemFeather(StatsType.HP, "health_feather");
        muscleFeather = new ItemFeather(StatsType.Attack, "muscle_feather");
        resistFeather = new ItemFeather(StatsType.Defence, "resist_feather");
        geniusFeather = new ItemFeather(StatsType.SpecialAttack, "genius_feather");
        cleverFeather = new ItemFeather(StatsType.SpecialDefence, "clever_feather");
        swiftFeather = new ItemFeather(StatsType.Speed, "swift_feather");
        prettyFeather = new PixelmonItem("pretty_feather").setCreativeTab(PixelmonCreativeTabs.valueables);
        HpUp = new IncreaseEV(EnumIncreaseEV.HpUp, "hp_up");
        Protein = new IncreaseEV(EnumIncreaseEV.Protein, "protein");
        Iron = new IncreaseEV(EnumIncreaseEV.Iron, "iron");
        Calcium = new IncreaseEV(EnumIncreaseEV.Calcium, "calcium");
        Zinc = new IncreaseEV(EnumIncreaseEV.Zinc, "zinc");
        Carbos = new IncreaseEV(EnumIncreaseEV.Carbos, "carbos");
        xAttack = new ItemBattleItem(EnumBattleItems.xAttack, "x_attack");
        xDefence = new ItemBattleItem(EnumBattleItems.xDefence, "x_defence");
        xSpecialAttack = new ItemBattleItem(EnumBattleItems.xSpecialAttack, "x_special_attack");
        xSpecialDefence = new ItemBattleItem(EnumBattleItems.xSpecialDefence, "x_special_defence");
        xSpeed = new ItemBattleItem(EnumBattleItems.xSpeed, "x_speed");
        xAccuracy = new ItemBattleItem(EnumBattleItems.xAccuracy, "x_accuracy");
        direHit = new ItemBattleItem(EnumBattleItems.direHit, "dire_hit");
        guardSpec = new ItemBattleItem(EnumBattleItems.guardSpec, "guard_spec");
        pomegBerry = new DecreaseEV(EnumDecreaseEV.PomegBerry, EnumBerry.Pomeg, "pomeg_berry");
        kelpsyBerry = new DecreaseEV(EnumDecreaseEV.KelpsyBerry, EnumBerry.Kelpsy, "kelpsy_berry");
        qualotBerry = new DecreaseEV(EnumDecreaseEV.QualotBerry, EnumBerry.Qualot, "qualot_berry");
        hondewBerry = new DecreaseEV(EnumDecreaseEV.HondewBerry, EnumBerry.Hondew, "hondew_berry");
        grepaBerry = new DecreaseEV(EnumDecreaseEV.GrepaBerry, EnumBerry.Grepa, "grepa_berry");
        tamatoBerry = new DecreaseEV(EnumDecreaseEV.TamatoBerry, EnumBerry.Tamato, "tamato_berry");
        healPowder = new ItemStatusAilmentHealer("heal_powder", allStatuses);
        energyPowder = new ItemPotion("energy_powder", new MedicinePotion(new HealFixed(50))).setFriendshipDecrease(5, 10);
        energyRoot = new ItemPotion("energy_root", new MedicinePotion(new HealFixed(200))).setFriendshipDecrease(10, 15);
        revivalHerb = new ItemRevive("revival_herb", new MedicineRevive(new HealFraction(1.0f))).setFriendshipDecrease(15, 20);
        fireStone = new ItemEvolutionStone(EnumEvolutionStone.Firestone, "fire_stone").setCreativeTab(PixelmonCreativeTabs.natural);
        waterStone = new ItemEvolutionStone(EnumEvolutionStone.Waterstone, "water_stone").setCreativeTab(PixelmonCreativeTabs.natural);
        moonStone = new ItemEvolutionStone(EnumEvolutionStone.Moonstone, "moon_stone").setCreativeTab(PixelmonCreativeTabs.natural);
        thunderStone = new ItemEvolutionStone(EnumEvolutionStone.Thunderstone, "thunder_stone").setCreativeTab(PixelmonCreativeTabs.natural);
        sunStone = new ItemEvolutionStone(EnumEvolutionStone.Sunstone, "sun_stone").setCreativeTab(PixelmonCreativeTabs.natural);
        leafStone = new ItemEvolutionStone(EnumEvolutionStone.Leafstone, "leaf_stone").setCreativeTab(PixelmonCreativeTabs.natural);
        dawnStone = new ItemEvolutionStone(EnumEvolutionStone.Dawnstone, "dawn_stone").setCreativeTab(PixelmonCreativeTabs.natural);
        duskStone = new ItemEvolutionStone(EnumEvolutionStone.Duskstone, "dusk_stone").setCreativeTab(PixelmonCreativeTabs.natural);
        shinyStone = new ItemEvolutionStone(EnumEvolutionStone.Shinystone, "shiny_stone").setCreativeTab(PixelmonCreativeTabs.natural);
        blackAugurite = new ItemEvolutionStone(EnumEvolutionStone.BlackAugurite, "black_augurite").setCreativeTab(PixelmonCreativeTabs.natural);
        linkingCord = new ItemEvolutionStone(EnumEvolutionStone.LinkingCord, "linking_cord").setCreativeTab(PixelmonCreativeTabs.natural);
        peatBlock = new ItemEvolutionStone(EnumEvolutionStone.PeatBlock, "peat_block").setCreativeTab(PixelmonCreativeTabs.natural);
        sweetApple = new ItemEvolutionStone(EnumEvolutionStone.SweetApple, "sweet_apple").setCreativeTab(PixelmonCreativeTabs.natural);
        tartApple = new ItemEvolutionStone(EnumEvolutionStone.TartApple, "tart_apple").setCreativeTab(PixelmonCreativeTabs.natural);
        galaricaCuff = new ItemEvolutionStone(EnumEvolutionStone.GalaricaCuff, "galarica_cuff").setCreativeTab(PixelmonCreativeTabs.natural);
        galaricaWreath = new ItemEvolutionStone(EnumEvolutionStone.GalaricaWreath, "galarica_wreath").setCreativeTab(PixelmonCreativeTabs.natural);
        chippedPot = new ItemEvolutionStone(EnumEvolutionStone.ChippedPot, "chipped_pot").setCreativeTab(PixelmonCreativeTabs.natural);
        crackedPot = new ItemEvolutionStone(EnumEvolutionStone.CrackedPot, "cracked_pot").setCreativeTab(PixelmonCreativeTabs.natural);
        starSweet = new ItemEvolutionStone(EnumEvolutionStone.StarSweet, "star_sweet").setCreativeTab(PixelmonCreativeTabs.natural);
        strawberrySweet = new ItemEvolutionStone(EnumEvolutionStone.StrawberrySweet, "strawberry_sweet").setCreativeTab(PixelmonCreativeTabs.natural);
        berrySweet = new ItemEvolutionStone(EnumEvolutionStone.BerrySweet, "berry_sweet").setCreativeTab(PixelmonCreativeTabs.natural);
        cloverSweet = new ItemEvolutionStone(EnumEvolutionStone.CloverSweet, "clover_sweet").setCreativeTab(PixelmonCreativeTabs.natural);
        flowerSweet = new ItemEvolutionStone(EnumEvolutionStone.FlowerSweet, "flower_sweet").setCreativeTab(PixelmonCreativeTabs.natural);
        loveSweet = new ItemEvolutionStone(EnumEvolutionStone.LoveSweet, "love_sweet").setCreativeTab(PixelmonCreativeTabs.natural);
        ribbonSweet = new ItemEvolutionStone(EnumEvolutionStone.RibbonSweet, "ribbon_sweet").setCreativeTab(PixelmonCreativeTabs.natural);
        thunderStoneShard = new PixelmonItem("thunder_stone_shard").setCreativeTab(PixelmonCreativeTabs.natural);
        leafStoneShard = new PixelmonItem("leaf_stone_shard").setCreativeTab(PixelmonCreativeTabs.natural);
        waterStoneShard = new PixelmonItem("water_stone_shard").setCreativeTab(PixelmonCreativeTabs.natural);
        fireStoneShard = new PixelmonItem("fire_stone_shard").setCreativeTab(PixelmonCreativeTabs.natural);
        sunStoneShard = new PixelmonItem("sun_stone_shard").setCreativeTab(PixelmonCreativeTabs.natural);
        moonStoneShard = new PixelmonItem("moon_stone_shard").setCreativeTab(PixelmonCreativeTabs.natural);
        dawnStoneShard = new PixelmonItem("dawn_stone_shard").setCreativeTab(PixelmonCreativeTabs.natural);
        duskStoneShard = new PixelmonItem("dusk_stone_shard").setCreativeTab(PixelmonCreativeTabs.natural);
        shinyStoneShard = new PixelmonItem("shiny_stone_shard").setCreativeTab(PixelmonCreativeTabs.natural);
        wailmerPail = new ItemWailmerPail("wailmer_pail").setCreativeTab(PixelmonCreativeTabs.keyItems);
        oldRod = new ItemFishingRod(EnumRodType.OldRod, "old_rod").setCreativeTab(PixelmonCreativeTabs.keyItems);
        goodRod = new ItemFishingRod(EnumRodType.GoodRod, "good_rod").setCreativeTab(PixelmonCreativeTabs.keyItems);
        superRod = new ItemFishingRod(EnumRodType.SuperRod, "super_rod").setCreativeTab(PixelmonCreativeTabs.keyItems);
        tradeMonitor = new PixelmonItem("trade_monitor");
        tradeHolderRight = new PixelmonItem("trade_holder_right");
        LtradeHolderLeft = new PixelmonItem("trade_holder_left");
        tradePanel = new PixelmonItem("trade_panel");
        unoOrb = new ItemShrineOrbWithInfo("uno_orb", EnumSpecies.Articuno, 375).setCreativeTab(PixelmonCreativeTabs.legendItems);
        dosOrb = new ItemShrineOrbWithInfo("dos_orb", EnumSpecies.Zapdos, 375).setCreativeTab(PixelmonCreativeTabs.legendItems);
        tresOrb = new ItemShrineOrbWithInfo("tres_orb", EnumSpecies.Moltres, 375).setCreativeTab(PixelmonCreativeTabs.legendItems);
        mysticalOrb = new ItemShrineOrbWithInfo("mystical_orb", EnumSpecies.Articuno, EnumForms.Galarian, 375).setCreativeTab(PixelmonCreativeTabs.legendItems);
        martialOrb = new ItemShrineOrbWithInfo("martial_orb", EnumSpecies.Zapdos, EnumForms.Galarian, 375).setCreativeTab(PixelmonCreativeTabs.legendItems);
        malevolentOrb = new ItemShrineOrbWithInfo("malevolent_orb", EnumSpecies.Moltres, EnumForms.Galarian, 375).setCreativeTab(PixelmonCreativeTabs.legendItems);
        orb = new PixelmonItem("orb").setCreativeTab(PixelmonCreativeTabs.legendItems);
        trainerEditor = new ItemNPCEditor();
        pokemonEditor = new ItemPokemonEditor();
        itemFinder = new ItemItemFinder();
        grottoSpawner = new ItemSpawnGrotto();
        cloningMachineGreenItem = new PixelmonItem("green_tank").setCreativeTab(CreativeTabs.MISC);
        cloningMachineOrangeItem = new PixelmonItem("orange_tank").setCreativeTab(CreativeTabs.MISC);
        cloningMachineCordItem = new PixelmonItem("cloner_cord").setCreativeTab(CreativeTabs.MISC);
        hourglassGold = new ItemIsisHourglass(EnumIsisHourglassType.Gold);
        hourglassSilver = new ItemIsisHourglass(EnumIsisHourglassType.Silver);
        hiddenIronDoorItem = new ItemHiddenDoor(PixelmonBlocks.hiddenIronDoor, "hidden_iron_door");
        hiddenWoodenDoorItem = new ItemHiddenDoor(PixelmonBlocks.hiddenWoodenDoor, "hidden_wooden_door");
        chisel = new ItemStatueMaker();
        gift = new ItemGift("gift_box");
        candy = new ItemCandyBag("candy_bag");
        if (PixelmonConfig.allowRanchExpansion) {
            ranchUpgrade = new ItemRanchUpgrade();
        }
        itemPixelmonSprite = new ItemPixelmonSprite();
        itemCustomIcon = new ItemCustomIcon();
        pixelmonPaintingItem = new ItemPixelmonPainting("painting");
        cameraItem = new ItemCamera().setCreativeTab(PixelmonCreativeTabs.keyItems);
        filmItem = new PixelmonItem("film").setCreativeTab(PixelmonCreativeTabs.keyItems);
        bikeFrame = new PixelmonItem("bike_frame").setCreativeTab(CreativeTabs.MISC);
        bikeHandlebars = new PixelmonItem("bike_handlebars").setCreativeTab(CreativeTabs.MISC);
        bikeSeat = new PixelmonItem("bike_seat").setCreativeTab(CreativeTabs.MISC);
        bikeWheel = new PixelmonItem("bike_wheel").setCreativeTab(CreativeTabs.MISC);
        amethyst = new PixelmonItem("amethyst").setCreativeTab(PixelmonCreativeTabs.natural);
        ruby = new ItemRuby().setCreativeTab(PixelmonCreativeTabs.natural);
        crystal = new PixelmonItem("crystal").setCreativeTab(PixelmonCreativeTabs.natural);
        sapphire = new PixelmonItem("sapphire").setCreativeTab(PixelmonCreativeTabs.natural);
        siliconItem = new PixelmonItem("silicon").setCreativeTab(PixelmonCreativeTabs.natural);
        picketFenceItem = new ItemPicketFence().setCreativeTab(PixelmonCreativeTabs.decoration);
        treeItem = new PixelmonItemBlock(PixelmonBlocks.treeBlock, "tree").setCreativeTab(PixelmonCreativeTabs.decoration);
        abilityCapsule = new ItemAbilityCapsule();
        abilityPatch = new ItemAbilityPatch();
        expAll = new ItemExpAll("exp_all");
        meteorite = new PixelmonItem("meteorite");
        corruptedGem = new PixelmonItem("corrupted_gem");
        gracidea = new ItemGracidea();
        reveal_glass = new ShrineItem("reveal_glass", EnumSpecies.Tornadus).setCreativeTab(PixelmonCreativeTabs.legendItems);
        mirror = new PixelmonItem("mirror");
        repel = new ItemRepel(ItemRepel.EnumRepel.REPEL).setCreativeTab(PixelmonCreativeTabs.keyItems);
        superRepel = new ItemRepel(ItemRepel.EnumRepel.SUPER_REPEL).setCreativeTab(PixelmonCreativeTabs.keyItems);
        maxRepel = new ItemRepel(ItemRepel.EnumRepel.MAX_REPEL).setCreativeTab(PixelmonCreativeTabs.keyItems);
        redchain = new PixelmonItem("red_chain", true).setMaxStackSize(1).setCreativeTab(PixelmonCreativeTabs.legendItems);
        dnaSplicers = new PixelmonItem("dna_splicers").setCreativeTab(CreativeTabs.MISC);
        reinsOfUnity = new PixelmonItem("reins_of_unity").setCreativeTab(CreativeTabs.MISC).setCreativeTab(PixelmonCreativeTabs.legendItems);
        nSolarizer = new PixelmonItem("n_solarizer").setCreativeTab(CreativeTabs.MISC);
        nLunarizer = new PixelmonItem("n_lunarizer").setCreativeTab(CreativeTabs.MISC);
        porygonPieces = new PorygonPiece("porygon_piece");
        legendFinder = new ItemLegendFinder();
        hoopaRing = new PixelmonItem("hoopa_ring").setCreativeTab(PixelmonCreativeTabs.legendItems);
        fadedRedOrb = new ItemFadedOrb("faded_red_orb", EnumSpecies.Groudon, EnumType.Fire).setCreativeTab(PixelmonCreativeTabs.legendItems);
        fadedBlueOrb = new ItemFadedOrb("faded_blue_orb", EnumSpecies.Kyogre, EnumType.Water).setCreativeTab(PixelmonCreativeTabs.legendItems);
        rockStarCostume = new PixelmonItem("rockstar_costume");
        belleCostume = new PixelmonItem("belle_costume");
        popStarCostume = new PixelmonItem("popstar_costume");
        phdCostume = new PixelmonItem("phd_costume");
        libreCostume = new PixelmonItem("libre_costume");
        mewtwoArmor = new PixelmonItem("mewtwo_armor");
        balmMushroom = new PixelmonItem("balm_mushroom").setCreativeTab(PixelmonCreativeTabs.valueables);
        bigMushroom = new PixelmonItem("big_mushroom").setCreativeTab(PixelmonCreativeTabs.valueables);
        bigNugget = new PixelmonItem("big_nugget").setCreativeTab(PixelmonCreativeTabs.valueables);
        bigPearl = new PixelmonItem("big_pearl").setCreativeTab(PixelmonCreativeTabs.valueables);
        goldLeaf = new PixelmonItem("gold_leaf").setCreativeTab(PixelmonCreativeTabs.valueables);
        smallBouquet = new PixelmonItem("small_bouquet");
        blueShard = new PixelmonItem("blue_shard").setCreativeTab(PixelmonCreativeTabs.valueables);
        cometShard = new PixelmonItem("comet_shard").setCreativeTab(PixelmonCreativeTabs.valueables);
        greenShard = new PixelmonItem("green_shard").setCreativeTab(PixelmonCreativeTabs.valueables);
        nugget = new PixelmonItem("nugget").setCreativeTab(PixelmonCreativeTabs.valueables);
        pearl = new PixelmonItem("pearl").setCreativeTab(PixelmonCreativeTabs.valueables);
        pearlString = new PixelmonItem("pearl_string").setCreativeTab(PixelmonCreativeTabs.valueables);
        redShard = new PixelmonItem("red_shard").setCreativeTab(PixelmonCreativeTabs.valueables);
        starPiece = new PixelmonItem("star_piece").setCreativeTab(PixelmonCreativeTabs.valueables);
        tinyMushroom = new PixelmonItem("tiny_mushroom").setCreativeTab(PixelmonCreativeTabs.valueables);
        yellowShard = new PixelmonItem("yellow_shard").setCreativeTab(PixelmonCreativeTabs.valueables);
        iceStone = new ItemEvolutionStone(EnumEvolutionStone.Icestone, "ice_stone").setCreativeTab(PixelmonCreativeTabs.natural);
        iceStoneShard = new PixelmonItem("ice_stone_shard").setCreativeTab(PixelmonCreativeTabs.natural);
        pinkNectar = new PixelmonItem("pink_nectar").setCreativeTab(CreativeTabs.MISC);
        purpleNectar = new PixelmonItem("purple_nectar").setCreativeTab(CreativeTabs.MISC);
        redNectar = new PixelmonItem("red_nectar").setCreativeTab(CreativeTabs.MISC);
        yellowNectar = new PixelmonItem("yellow_nectar").setCreativeTab(CreativeTabs.MISC);
        ppUp = new ItemPPUp("pp_up", false);
        maxPp = new ItemPPUp("pp_max", true);
        redBike = new ItemBike(EnumBike.Red).setCreativeTab(PixelmonCreativeTabs.keyItems);
        orangeBike = new ItemBike(EnumBike.Orange).setCreativeTab(PixelmonCreativeTabs.keyItems);
        yellowBike = new ItemBike(EnumBike.Yellow).setCreativeTab(PixelmonCreativeTabs.keyItems);
        greenBike = new ItemBike(EnumBike.Green).setCreativeTab(PixelmonCreativeTabs.keyItems);
        blueBike = new ItemBike(EnumBike.Blue).setCreativeTab(PixelmonCreativeTabs.keyItems);
        purpleBike = new ItemBike(EnumBike.Purple).setCreativeTab(CreativeTabs.MISC);
        zygardeCube = new ItemZygardeCube().setCreativeTab(PixelmonCreativeTabs.legendItems);
        meltanBox = new ItemMeltanBox().setCreativeTab(PixelmonCreativeTabs.legendItems);
        lureModule = new PixelmonItem("lure_module").setCreativeTab(PixelmonCreativeTabs.keyItems);
        timeGlass = new ItemTimeGlass().setCreativeTab(PixelmonCreativeTabs.legendItems);
        moonFlute = new ItemFlute(EnumFlutes.Moon).setCreativeTab(CreativeTabs.MISC).setCreativeTab(PixelmonCreativeTabs.legendItems);
        sunFlute = new ItemFlute(EnumFlutes.Sun).setCreativeTab(CreativeTabs.MISC).setCreativeTab(PixelmonCreativeTabs.legendItems);
        rotomCatalog = new ItemRotomCatalog().setCreativeTab(PixelmonCreativeTabs.keyItems);
        jewelOfLife = new PixelmonItem("jewel_of_life").setCreativeTab(PixelmonCreativeTabs.valueables).setCreativeTab(PixelmonCreativeTabs.legendItems);
        mintSerious = new ItemPokeMint(EnumNature.Serious);
        mintLonely = new ItemPokeMint(EnumNature.Lonely);
        mintBrave = new ItemPokeMint(EnumNature.Brave);
        mintAdamant = new ItemPokeMint(EnumNature.Adamant);
        mintNaughty = new ItemPokeMint(EnumNature.Naughty);
        mintBold = new ItemPokeMint(EnumNature.Bold);
        mintRelaxed = new ItemPokeMint(EnumNature.Relaxed);
        mintImpish = new ItemPokeMint(EnumNature.Impish);
        mintGentle = new ItemPokeMint(EnumNature.Gentle);
        mintLax = new ItemPokeMint(EnumNature.Lax);
        mintTimid = new ItemPokeMint(EnumNature.Timid);
        mintHasty = new ItemPokeMint(EnumNature.Hasty);
        mintJolly = new ItemPokeMint(EnumNature.Jolly);
        mintNaive = new ItemPokeMint(EnumNature.Naive);
        mintModest = new ItemPokeMint(EnumNature.Modest);
        mintMild = new ItemPokeMint(EnumNature.Mild);
        mintQuiet = new ItemPokeMint(EnumNature.Quiet);
        mintRash = new ItemPokeMint(EnumNature.Rash);
        mintCalm = new ItemPokeMint(EnumNature.Calm);
        mintSassy = new ItemPokeMint(EnumNature.Sassy);
        mintCareful = new ItemPokeMint(EnumNature.Careful);
        expCandyXS = new ItemLevelCandy(EnumEXPAmount.XS);
        expCandyS = new ItemLevelCandy(EnumEXPAmount.S);
        expCandyM = new ItemLevelCandy(EnumEXPAmount.M);
        expCandyL = new ItemLevelCandy(EnumEXPAmount.L);
        expCandyXL = new ItemLevelCandy(EnumEXPAmount.XL);
        roomService = new ItemRoomService();
        basicSweetPokePuff = new ItemPokePuff("basic_sweet_poke_puff", 10).setCreativeTab(CreativeTabs.MISC);
        basicCitrusPokePuff = new ItemPokePuff("basic_citrus_poke_puff", 10).setCreativeTab(CreativeTabs.MISC);
        basicMintPokePuff = new ItemPokePuff("basic_mint_poke_puff", 10).setCreativeTab(CreativeTabs.MISC);
        basicMochaPokePuff = new ItemPokePuff("basic_mocha_poke_puff", 10).setCreativeTab(CreativeTabs.MISC);
        basicSpicePokePuff = new ItemPokePuff("basic_spice_poke_puff", 10).setCreativeTab(CreativeTabs.MISC);
        pokedex = new ItemPokedex().setCreativeTab(PixelmonCreativeTabs.keyItems);
        zoneWand = new ItemZoneWand();
        lavaCrystal = new PixelmonItem("lava_crystal").setCreativeTab(PixelmonCreativeTabs.legendItems);
        magmaCrystal = new ItemMagmaCrystal("magma_crystal").setCreativeTab(PixelmonCreativeTabs.legendItems);
        relicSongRecord = new ItemPixelmonRecord("relic_song", PixelSounds.meloettasRelicSong, 30).setCreativeTab(PixelmonCreativeTabs.legendItems);
        shatteredRelicSong1 = new PixelmonItem("shattered_relic_song_1").setCreativeTab(PixelmonCreativeTabs.legendItems);
        shatteredRelicSong2 = new PixelmonItem("shattered_relic_song_2").setCreativeTab(PixelmonCreativeTabs.legendItems);
        shatteredRelicSong3 = new PixelmonItem("shattered_relic_song_3").setCreativeTab(PixelmonCreativeTabs.legendItems);
        shatteredRelicSong4 = new PixelmonItem("shattered_relic_song_4").setCreativeTab(PixelmonCreativeTabs.legendItems);
        record1 = new ItemPixelmonRecord("record_1", PixelSounds.track1);
        record2 = new ItemPixelmonRecord("record_2", PixelSounds.track2);
        record3 = new ItemPixelmonRecord("record_3", PixelSounds.track3);
        record4 = new ItemPixelmonRecord("record_4", PixelSounds.track4);
        record5 = new ItemPixelmonRecord("record_5", PixelSounds.track5);
        record6 = new ItemPixelmonRecord("record_6", PixelSounds.track6);
        record7 = new ItemPixelmonRecord("record_7", PixelSounds.track7);
        record8 = new ItemPixelmonRecord("record_8", PixelSounds.track8);
        record9 = new ItemPixelmonRecord("record_9", PixelSounds.track9);
        record10 = new ItemPixelmonRecord("record_10", PixelSounds.track10);
        record11 = new ItemPixelmonRecord("record_11", PixelSounds.battle1);
        record12 = new ItemPixelmonRecord("record_12", PixelSounds.battle2);
        record13 = new ItemPixelmonRecord("record_13", PixelSounds.battle3);
        record14 = new ItemPixelmonRecord("record_14", PixelSounds.battle4);
        record15 = new ItemPixelmonRecord("record_15", PixelSounds.battle5);
        record16 = new ItemPixelmonRecord("record_16", PixelSounds.battle6);
        record17 = new ItemPixelmonRecord("record_17", PixelSounds.battle7);
        record18 = new ItemPixelmonRecord("record_18", PixelSounds.battle8);
        record19 = new ItemPixelmonRecord("record_19", PixelSounds.battle9);
        record20 = new ItemPixelmonRecord("record_20", PixelSounds.battle10);
        record21 = new ItemPixelmonRecord("record_21", PixelSounds.track11);
        rockPeakKey = new ShrineItem("rock_peak_key", EnumSpecies.Regirock).setCreativeTab(PixelmonCreativeTabs.legendItems);
        icebergKey = new ShrineItem("iceberg_key", EnumSpecies.Regice).setCreativeTab(PixelmonCreativeTabs.legendItems);
        ironKey = new ShrineItem("iron_key", EnumSpecies.Registeel).setCreativeTab(PixelmonCreativeTabs.legendItems);
        dragoKey = new ShrineItem("drago_key", EnumSpecies.Regidrago).setCreativeTab(PixelmonCreativeTabs.legendItems);
        elekiKey = new ShrineItem("eleki_key", EnumSpecies.Regieleki).setCreativeTab(PixelmonCreativeTabs.legendItems);
        shatteredIceKey1 = new PixelmonItem("shattered_ice_key_1").setCreativeTab(PixelmonCreativeTabs.legendItems);
        shatteredIceKey2 = new PixelmonItem("shattered_ice_key_2").setCreativeTab(PixelmonCreativeTabs.legendItems);
        shatteredIceKey3 = new PixelmonItem("shattered_ice_key_3").setCreativeTab(PixelmonCreativeTabs.legendItems);
        shatteredIceKey4 = new PixelmonItem("shattered_ice_key_4").setCreativeTab(PixelmonCreativeTabs.legendItems);
        crumbledRockKey1 = new PixelmonItem("crumbled_rock_key_1").setCreativeTab(PixelmonCreativeTabs.legendItems);
        crumbledRockKey2 = new PixelmonItem("crumbled_rock_key_2").setCreativeTab(PixelmonCreativeTabs.legendItems);
        crumbledRockKey3 = new PixelmonItem("crumbled_rock_key_3").setCreativeTab(PixelmonCreativeTabs.legendItems);
        crumbledRockKey4 = new PixelmonItem("crumbled_rock_key_4").setCreativeTab(PixelmonCreativeTabs.legendItems);
        rustyIronKey1 = new PixelmonItem("rusty_iron_key_1").setCreativeTab(PixelmonCreativeTabs.legendItems);
        rustyIronKey2 = new PixelmonItem("rusty_iron_key_2").setCreativeTab(PixelmonCreativeTabs.legendItems);
        rustyIronKey3 = new PixelmonItem("rusty_iron_key_3").setCreativeTab(PixelmonCreativeTabs.legendItems);
        rustyIronKey4 = new PixelmonItem("rusty_iron_key_4").setCreativeTab(PixelmonCreativeTabs.legendItems);
        fragmentedDragoKey1 = new PixelmonItem("fragmented_drago_key_1").setCreativeTab(PixelmonCreativeTabs.legendItems);
        fragmentedDragoKey2 = new PixelmonItem("fragmented_drago_key_2").setCreativeTab(PixelmonCreativeTabs.legendItems);
        fragmentedDragoKey3 = new PixelmonItem("fragmented_drago_key_3").setCreativeTab(PixelmonCreativeTabs.legendItems);
        fragmentedDragoKey4 = new PixelmonItem("fragmented_drago_key_4").setCreativeTab(PixelmonCreativeTabs.legendItems);
        dischargedElekiKey1 = new PixelmonItem("discharged_eleki_key_1").setCreativeTab(PixelmonCreativeTabs.legendItems);
        dischargedElekiKey2 = new PixelmonItem("discharged_eleki_key_2").setCreativeTab(PixelmonCreativeTabs.legendItems);
        dischargedElekiKey3 = new PixelmonItem("discharged_eleki_key_3").setCreativeTab(PixelmonCreativeTabs.legendItems);
        dischargedElekiKey4 = new PixelmonItem("discharged_eleki_key_4").setCreativeTab(PixelmonCreativeTabs.legendItems);
        lightStone = new TaoTrioStone("light_stone", EnumSpecies.Reshiram).setCreativeTab(PixelmonCreativeTabs.legendItems);
        darkStone = new TaoTrioStone("dark_stone", EnumSpecies.Zekrom).setCreativeTab(PixelmonCreativeTabs.legendItems);
        dragonStone = new TaoTrioStone("dragon_stone", EnumSpecies.Kyurem).setCreativeTab(PixelmonCreativeTabs.legendItems);
        generationsOrb = new ItemShrineOrbWithInfo("generations_orb", null, 250){

            @Override
            public PokemonGroup getGroup() {
                return TileEntityGenerationsShrine.getPokemonGroup();
            }
        }.setCreativeTab(PixelmonCreativeTabs.legendItems);
        rainbowWing = new PixelmonItem("rainbow_wing").setMaxStackSize(1).setMaxDamage(10).setCreativeTab(PixelmonCreativeTabs.legendItems);
        darkSoul = new PixelmonItem("dark_soul").setCreativeTab(PixelmonCreativeTabs.legendItems);
        dragonSoul = new PixelmonItem("dragon_soul").setCreativeTab(PixelmonCreativeTabs.legendItems);
        melodyFlute = new ShrineItem("melody_flute", EnumSpecies.Lugia).setCreativeTab(PixelmonCreativeTabs.legendItems);
        sparklingShard = new PixelmonItem("sparkling_shard").setCreativeTab(PixelmonCreativeTabs.legendItems);
        sparklingStone = new ItemShrineOrbWithInfo("sparkling_stone", EnumSpecies.TapuBulu, 100){

            @Override
            public boolean acceptsType(List<EnumType> type) {
                return false;
            }
        }.setCreativeTab(PixelmonCreativeTabs.legendItems);
        rustyFragment = new PixelmonItem("rusty_fragment").setCreativeTab(PixelmonCreativeTabs.legendItems);
        rustySword = new ItemShrineOrbWithInfo("rusty_sword", EnumSpecies.Zacian, 250){

            @Override
            public boolean acceptsType(List<EnumType> type) {
                return type.contains((Object)EnumType.Fairy);
            }

            @Override
            public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand hand) {
                ItemStack stack = playerIn.getHeldItem(hand);
                if (!worldIn.isRemote) {
                    int damage = stack.getItemDamage();
                    if (damage >= this.getMaxDamage(stack)) {
                        PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID(playerIn.getUniqueID()).ifPresent(storage -> {
                            if (storage.getTeam().stream().anyMatch(data -> data.getString("Name").equals(EnumSpecies.Zacian.name))) {
                                ChatHandler.sendChat(playerIn, "pixelmon.rusty_sword.checkparty", new Object[0]);
                            } else {
                                stack.shrink(1);
                                SpawnPokemon.of(EnumSpecies.Zacian, playerIn.getPosition(), playerIn.getRotationYawHead()).spawn((EntityPlayerMP)playerIn, playerIn.world);
                            }
                        });
                    } else {
                        ChatHandler.sendChat(playerIn, "pixelmon.rusty_sword.amountfull", this.getMaxDamage(stack) - damage);
                        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
                    }
                }
                return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
            }
        }.setCreativeTab(PixelmonCreativeTabs.legendItems);
        rustyShield = new ItemShrineOrbWithInfo("rusty_shield", EnumSpecies.Zamazenta, 250){

            @Override
            public boolean acceptsType(List<EnumType> type) {
                return type.contains((Object)EnumType.Fighting);
            }

            @Override
            public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand hand) {
                ItemStack stack = playerIn.getHeldItem(hand);
                if (!worldIn.isRemote) {
                    int damage = stack.getItemDamage();
                    if (damage >= this.getMaxDamage(stack)) {
                        PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID(playerIn.getUniqueID()).ifPresent(storage -> {
                            if (storage.getTeam().stream().anyMatch(data -> data.getString("Name").equals(EnumSpecies.Zamazenta.name))) {
                                ChatHandler.sendChat(playerIn, "pixelmon.rusty_shield.checkparty", new Object[0]);
                            } else {
                                stack.shrink(1);
                                SpawnPokemon.of(EnumSpecies.Zamazenta, playerIn.getPosition(), playerIn.getRotationYawHead()).spawn((EntityPlayerMP)playerIn, playerIn.world);
                            }
                        });
                    } else {
                        ChatHandler.sendChat(playerIn, "pixelmon.rusty_shield.amountfull", this.getMaxDamage(stack) - damage);
                        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
                    }
                }
                return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
            }
        }.setCreativeTab(PixelmonCreativeTabs.legendItems);
        scrollPage = new PixelmonItem("scroll_page").setCreativeTab(PixelmonCreativeTabs.legendItems);
        secretArmorScroll = new ItemSecretArmorScroll().setCreativeTab(PixelmonCreativeTabs.legendItems);
        growthMulch = new ItemMulch(EnumMulch.Growth);
        dampMulch = new ItemMulch(EnumMulch.Damp);
        stableMulch = new ItemMulch(EnumMulch.Stable);
        gooeyMulch = new ItemMulch(EnumMulch.Gooey);
        amazeMulch = new ItemMulch(EnumMulch.Amaze);
        boostMulch = new ItemMulch(EnumMulch.Boost);
        richMulch = new ItemMulch(EnumMulch.Rich);
        surpriseMulch = new ItemMulch(EnumMulch.Surprise);
        pesticde = new ItemPesticide("pesticide");
        beachGlass = new PixelmonItem("beach_glass").setCreativeTab(PixelmonCreativeTabs.valueables);
        chalkyStone = new PixelmonItem("chalky_stone").setCreativeTab(PixelmonCreativeTabs.valueables);
        loneEarring = new PixelmonItem("lone_earring").setCreativeTab(PixelmonCreativeTabs.valueables);
        marble = new PixelmonItem("marble").setCreativeTab(PixelmonCreativeTabs.valueables);
        polishedMudBall = new PixelmonItem("polished_mud_ball").setCreativeTab(PixelmonCreativeTabs.valueables);
        stretchySpring = new PixelmonItem("stretchy_spring").setCreativeTab(PixelmonCreativeTabs.valueables);
        tropicalShell = new PixelmonItem("tropical_shell").setCreativeTab(PixelmonCreativeTabs.valueables);
        feederBase = new PixelmonItem("feeder_base").setCreativeTab(CreativeTabs.MATERIALS);
        totemSticker = new PixelmonItem("totem_sticker").setCreativeTab(PixelmonCreativeTabs.valueables);
        kleavorAxe = new PixelmonItem("kleavor_axe");
        lilligantFlower = new PixelmonItem("lilligant_flower");
        arcanineRock = new PixelmonItem("arcanine_rock");
        electrodeLightning = new PixelmonItem("electrode_lightning");
        avaluggIce = new PixelmonItem("avalugg_ice");
        adamant_crystal = new PixelmonItem("adamant_crystal");
        lustrous_globe = new PixelmonItem("lustrous_globe");
        griseous_core = new PixelmonItem("griseous_core");
    }

    public static ArrayList<Item> initializePotionElixirList() {
        ArrayList<Item> list = new ArrayList<Item>();
        list.add(potion);
        list.add(superPotion);
        list.add(hyperPotion);
        list.add(maxPotion);
        list.add(freshWater);
        list.add(sodaPop);
        list.add(lemonade);
        list.add(moomooMilk);
        list.add(ether);
        list.add(maxEther);
        list.add(elixir);
        list.add(maxElixir);
        list.add(fullRestore);
        list.add(revive);
        list.add(maxRevive);
        list.add(antidote);
        list.add(parlyzHeal);
        list.add(awakening);
        list.add(burnHeal);
        list.add(iceHeal);
        list.add(fullHeal);
        list.add(healPowder);
        list.add(energyPowder);
        list.add(energyRoot);
        list.add(revivalHerb);
        list.add(xAttack);
        list.add(xDefence);
        list.add(xSpecialAttack);
        list.add(xSpecialDefence);
        list.add(xSpeed);
        list.add(xAccuracy);
        list.add(direHit);
        list.add(guardSpec);
        list.add(HpUp);
        list.add(Protein);
        list.add(Iron);
        list.add(Calcium);
        list.add(Zinc);
        list.add(Carbos);
        list.add(pomegBerry);
        list.add(kelpsyBerry);
        list.add(qualotBerry);
        list.add(hondewBerry);
        list.add(grepaBerry);
        list.add(tamatoBerry);
        list.add(ppUp);
        list.add(maxPp);
        return list;
    }

    public static ArrayList<Item> getPotionElixirList() {
        if (potionElixirList.isEmpty()) {
            potionElixirList = PixelmonItems.initializePotionElixirList();
        }
        return potionElixirList;
    }

    public static ArrayList<Item> initializeEvostoneList() {
        ArrayList<Item> list = new ArrayList<Item>();
        list.add(fireStone);
        list.add(waterStone);
        list.add(moonStone);
        list.add(thunderStone);
        list.add(leafStone);
        list.add(sunStone);
        list.add(duskStone);
        list.add(dawnStone);
        list.add(shinyStone);
        return list;
    }

    public static Item getItemFromName(String itemName) {
        if (allItemMap == null) {
            PixelmonItems.initializeItemMap();
        }
        return allItemMap.get(itemName.toLowerCase());
    }

    @Deprecated
    public static Item getItemFromUnlocalizedName(String itemName) {
        return PixelmonItems.getItemFromName(itemName);
    }

    public static void initializeItemMap() {
        allItemMap = new HashMap<String, Item>();
        allItemList = new ArrayList();
        PixelmonItems.populateItemMap(PixelmonItems.class.getFields());
        PixelmonItems.populateItemMap(PixelmonItemsApricorns.class.getFields());
        PixelmonItems.populateItemMap(PixelmonItemsBadges.class.getFields());
        PixelmonItems.populateItemMap(PixelmonItemsFossils.class.getFields());
        PixelmonItems.populateItemMap(PixelmonItemsHeld.class.getFields());
        PixelmonItems.populateItemMap(PixelmonItemsMail.items);
        PixelmonItems.populateItemMap(PixelmonItemsPokeballs.class.getFields());
        PixelmonItems.populateItemMap(PixelmonItemsBalms.class.getFields());
        PixelmonItems.populateItemMap(PixelmonItemsTMs.TRs);
        PixelmonItems.populateItemMap(PixelmonItemsTMs.TMs);
        PixelmonItems.populateItemMap(PixelmonItemsTools.class.getFields());
        PixelmonItems.populateItemMap(PixelmonBlocks.class.getFields());
        PixelmonItems.populateItemMap(PixelmonBlocksApricornTrees.class.getFields());
    }

    private static void populateItemMap(ArrayList<?> items) {
        for (Object object : items) {
            PixelmonItems.populateItemMap(object);
        }
    }

    private static void populateItemMap(Field[] items) {
        for (Field item : items) {
            try {
                Object object = item.get(null);
                PixelmonItems.populateItemMap(object);
            }
            catch (Exception exception) {
                // empty catch block
            }
        }
    }

    private static void populateItemMap(Object object) {
        Item item = null;
        if (object instanceof Item) {
            item = (Item)object;
        } else if (object instanceof Block) {
            item = Item.getItemFromBlock((Block)object);
        }
        if (item != null) {
            ItemStack itemStack = new ItemStack(item);
            allItemMap.put(itemStack.getTranslationKey().toLowerCase(), item);
            allItemMap.put(itemStack.getDisplayName().toLowerCase(), item);
            allItemList.add(item);
        }
    }

    public static ArrayList<Item> getEvostoneList() {
        if (evostoneList.isEmpty()) {
            evostoneList = PixelmonItems.initializeEvostoneList();
        }
        return evostoneList;
    }

    @SideOnly(value=Side.CLIENT)
    public static void registerRenderers() {
        try {
            for (Field field : PixelmonItems.class.getFields()) {
                if (!(field.get(null) instanceof Item)) continue;
                Item item = (Item)field.get(null);
                if (item.getRegistryName() == null) {
                    System.out.println(item.getTranslationKey() + " missing registery name!");
                }
                ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(item.getRegistryName(), "inventory"));
                if (item == itemFinder) {
                    ModelLoader.setCustomModelResourceLocation(item, 1, new ModelResourceLocation("pixelmon:item_finder_front", "inventory"));
                    ModelLoader.setCustomModelResourceLocation(item, 2, new ModelResourceLocation("pixelmon:item_finder_back", "inventory"));
                    ModelLoader.setCustomModelResourceLocation(item, 3, new ModelResourceLocation("pixelmon:item_finder_right", "inventory"));
                    ModelLoader.setCustomModelResourceLocation(item, 4, new ModelResourceLocation("pixelmon:item_finder_left", "inventory"));
                    ModelBakery.registerItemVariants(itemFinder, itemFinder.getRegistryName(), new ResourceLocation(itemFinder.getRegistryName() + "_front"), new ResourceLocation(itemFinder.getRegistryName() + "_back"), new ResourceLocation(itemFinder.getRegistryName() + "_right"), new ResourceLocation(itemFinder.getRegistryName() + "_left"));
                    continue;
                }
                if (item == ruby) {
                    ModelLoader.setCustomModelResourceLocation(item, 1, new ModelResourceLocation("pixelmon:ruby"));
                    ModelLoader.setCustomModelResourceLocation(item, 2, new ModelResourceLocation("pixelmon:ruby"));
                    ModelLoader.setCustomModelResourceLocation(item, 3, new ModelResourceLocation("pixelmon:ruby"));
                    continue;
                }
                if (item != porygonPieces) continue;
                ModelLoader.setCustomModelResourceLocation(item, 1, new ModelResourceLocation("pixelmon:porygon_piece_head"));
                ModelLoader.setCustomModelResourceLocation(item, 2, new ModelResourceLocation("pixelmon:porygon_piece_body"));
                ModelLoader.setCustomModelResourceLocation(item, 3, new ModelResourceLocation("pixelmon:porygon_piece_leg"));
                ModelLoader.setCustomModelResourceLocation(item, 4, new ModelResourceLocation("pixelmon:porygon_piece_tail"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    static {
        expCandyXS = new PixelmonItem("xs_candy").setCreativeTab(PixelmonCreativeTabs.restoration);
        expCandyS = new PixelmonItem("s_candy").setCreativeTab(PixelmonCreativeTabs.restoration);
        expCandyM = new PixelmonItem("m_candy").setCreativeTab(PixelmonCreativeTabs.restoration);
        expCandyL = new PixelmonItem("l_candy").setCreativeTab(PixelmonCreativeTabs.restoration);
        expCandyXL = new PixelmonItem("xl_candy").setCreativeTab(PixelmonCreativeTabs.restoration);
        potionElixirList = new ArrayList();
        evostoneList = new ArrayList();
    }
}

