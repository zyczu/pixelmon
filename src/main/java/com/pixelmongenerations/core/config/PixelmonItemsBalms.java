/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.common.item.ItemForestBalm;
import com.pixelmongenerations.common.item.ItemMarshBalm;
import com.pixelmongenerations.common.item.ItemMountainBalm;
import com.pixelmongenerations.common.item.ItemSnowBalm;
import com.pixelmongenerations.common.item.ItemVolcanoBalm;
import com.pixelmongenerations.core.enums.items.EnumBalm;
import net.minecraft.item.Item;

public class PixelmonItemsBalms {
    public static Item forestBalm;
    public static Item marshBalm;
    public static Item mountainBalm;
    public static Item snowBalm;
    public static Item volcanoBalm;

    public static void load() {
        forestBalm = new ItemForestBalm("forest_balm");
        marshBalm = new ItemMarshBalm("marsh_balm");
        mountainBalm = new ItemMountainBalm("mountain_balm");
        snowBalm = new ItemSnowBalm("snow_balm");
        volcanoBalm = new ItemVolcanoBalm("volcano_balm");
    }

    public static ItemForestBalm getItemFromEnum(EnumBalm balm) {
        Item result = null;
        switch (balm) {
            case Forest_Balm: {
                result = forestBalm;
                break;
            }
            case Marsh_Balm: {
                result = marshBalm;
                break;
            }
            case Mountain_Balm: {
                result = mountainBalm;
                break;
            }
            case Snow_Balm: {
                result = snowBalm;
                break;
            }
            case Volcano_Balm: {
                result = volcanoBalm;
            }
        }
        return (ItemForestBalm)result;
    }
}

