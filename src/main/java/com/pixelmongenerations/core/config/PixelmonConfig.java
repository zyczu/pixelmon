/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableList
 *  com.google.common.reflect.TypeToken
 */
package com.pixelmongenerations.core.config;

import com.google.common.collect.ImmutableList;
import com.google.common.reflect.TypeToken;
import com.pixelmongenerations.common.starter.CustomStarters;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.EnumForceBattleResult;
import com.pixelmongenerations.core.config.EnumPokelootModes;
import com.pixelmongenerations.core.config.EnumPokelootRate;
import com.pixelmongenerations.core.config.MigrationHandler;
import com.pixelmongenerations.core.config.Node;
import com.pixelmongenerations.core.enums.battle.EnumBattleAIMode;
import ninja.leaping.configurate.ConfigurationOptions;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.List;
import net.minecraft.util.text.translation.I18n;

public class PixelmonConfig {
    public static final int MILLI = 1000;
    public static final int SECONDS = 1;
    public static final int MINUTES = 60;
    public static final int HOURS = 3600;
    public static boolean isInMetric = true;
    @Node(category="General", nameOverride="awardPhotos")
    public static boolean awardTokens = false;
    @Node(category="General")
    public static float ballSlackRadius = 1.0f;
    @Node(category="General")
    public static boolean spawnStructures = true;
    @Node(category="General", nameOverride="allowPokemonNicknames")
    public static boolean allowNicknames = true;
    @Node(category="General", nameOverride="allowAnvilAutoreloading")
    public static boolean allowAnvilAutoloading = false;
    @Node(category="General", nameOverride="allowVanillaMobs")
    public static boolean allowNonPixelmonMobs = false;
    @Node(category="General", nameOverride="allowCaptureOutsideBattle")
    public static boolean allowCapturingOutsideBattle = true;
    @Node(category="General", nameOverride="namePlateRange", minValue=0.0)
    public static int nameplateRangeModifier = 1;
    @Node(category="General")
    public static boolean showWildNames = true;
    @Node(category="General", nameOverride="scalePokemonModels", minValue=0.0)
    public static boolean scaleModelsUp = true;
    @Node(category="General", nameOverride="growthScaleModifier", minValue=0.0, maxValue=2.0)
    public static double growthModifier = 1.0;
    @Node(category="General")
    public static boolean pokemonDropsEnabled = true;
    @Node(category="General")
    public static boolean printErrors = true;
    @Node(category="General")
    public static boolean allowRiding = true;
    @Node(category="General")
    public static boolean allowPlanting = true;
    @Node(category="General", minValue=0.0)
    public static int maximumPlants = 32;
    @Node(category="General", nameOverride="allowPvPExperience")
    public static boolean allowPVPExperience = true;
    @Node(category="General")
    public static boolean allowTrainerExperience = true;
    @Node(category="General")
    public static boolean returnHeldItems = true;
    @Node(category="General")
    public static EnumForceBattleResult forceEndBattleResult = EnumForceBattleResult.WINNER;
    @Node(category="General")
    public static boolean cloningMachineEnabled = true;
    @Node(category="General")
    public static int lakeTrioMaxEnchants = 6;
    @Node(category="General")
    public static int lightTrioMaxWormholes = 3;
    @Node(category="General", nameOverride="engagePlayerByPokeBall")
    public static boolean pokeBallPlayerEngage = true;
    @Node(category="General", minValue=0.0, maxValue=256.0)
    public static int computerBoxes = 30;
    @Node(category="General", nameOverride="enableWildAggression")
    public static boolean isAggressionAllowed = true;
    @Node(category="General")
    public static boolean allowTMReuse = true;
    @Node(category="General")
    public static boolean writeEntitiesToWorld = false;
    @Node(category="General")
    public static int scarecrowEffectPeriod = 30;
    @Node(category="General")
    public static int scarecrowRadius = 32;
    @Node(category="General")
    public static boolean spawnBirdShrines = true;
    @Node(category="General")
    public static boolean reusableBirdShrines = false;
    @Node(category="General", nameOverride="spawnersOpOnly")
    public static boolean opToUseSpawners = true;
    @Node(category="General", nameOverride="needHMToRide")
    public static boolean haveHM = false;
    @Node(category="General")
    public static boolean ranchBlocksDropUpgrades = true;
    @Node(category="General", nameOverride="tradersReusable")
    public static boolean reuseTraders = false;
    @Node(category="General")
    public static boolean allowEventMoveTutors = true;
    @Node(category="General", nameOverride="starterOnJoin")
    public static boolean giveStarter = true;
    @Node(category="General")
    public static boolean enablePointToSteer = true;
    @Node(category="General", nameOverride="useSystemTimeForWorldTime")
    public static boolean useSystemWorldTime = false;
    @Node(category="General", nameOverride="systemTimeSyncInterval", minValue=2.0)
    public static int timeUpdateInterval = 30;
    @Node(category="General")
    public static boolean useExternalJSONFilesDrops = false;
    @Node(category="General")
    public static boolean useExternalJSONFilesNPCs = false;
    @Node(category="General")
    public static boolean useExternalJSONFilesRules = false;
    @Node(category="General")
    public static boolean useExternalJSONFilesSpawning = false;
    @Node(category="General")
    public static boolean useExternalJSONFilesMoves = false;
    @Node(category="General")
    public static boolean useExternalJSONFilesPokemon = false;
    @Node(category="General")
    public static boolean useExternalJSONFilesStructures = false;
    @Node(category="General")
    public static boolean useExternalJSONFilesBreedingConditions = false;
    @Node(category="General")
    public static boolean useExternalJSONFilesFeederSpawns = false;
    @Node(category="General")
    public static boolean useExternalJSONFilesTrades = false;
    @Node(category="General")
    public static boolean useExternalJSONFilesSpawnColors = false;
    @Node(category="General")
    public static boolean useExternalJSONFilesGenerationsGroup = false;
    @Node(category="General")
    public static int chunkSpawnRadius = 6;
    @Node(category="General")
    public static EnumBattleAIMode battleAIWild = EnumBattleAIMode.Random;
    @Node(category="General")
    public static EnumBattleAIMode battleAIBoss = EnumBattleAIMode.Aggressive;
    @Node(category="General")
    public static EnumBattleAIMode battleAITrainer = EnumBattleAIMode.Tactical;
    @Node(category="General", minValue=0.0)
    public static float expModifier = 1.0f;
    @Node(category="General", nameOverride="multiplePhotosOfSamePokemon")
    public static boolean allowMultiplePhotosOfSamePokemon = true;
    @Node(category="General")
    public static boolean allowPayDayMoney = true;
    @Node(category="General")
    public static int pickupRate = 10;
    @Node(category="General")
    public static int catchComboRestoreSeconds = 1800;
    @Node(category="General")
    public static boolean bedsHealPokemon = true;
    @Node(category="General")
    public static boolean allowPokemonEditors = true;
    @Node(category="General")
    public static boolean dataSaveOnWorldSave = true;
    @Node(category="General")
    public static boolean useDropGUI = true;
    @Node(category="General", minValue=0.0, maxValue=1.0)
    public static float ridingSpeedMultiplier = 1.0f;
    @Node(category="General", minValue=0.10000000149011612, maxValue=1000.0)
    public static float berryTreeGrowthMultiplier = 1.0f;
    @Node(category="General", minValue=1.0)
    public static int maxLevel = 100;
    @Node(category="General", minValue=1.0)
    public static int despawnRadius = 80;
    @Node(category="General")
    public static boolean canPokemonBeHit = false;
    @Node(category="General")
    public static boolean alwaysHaveMegaRing = false;
    @Node(category="General")
    public static boolean allowDynamax = true;
    @Node(category="General")
    public static boolean enableRichPresence = true;
    @Node(category="General")
    public static boolean rareCandyTriggersLevelEvent = true;
    @Node(category="General")
    public static boolean levelCandyTriggersLevelEvent = true;
    @Node(category="General")
    public static boolean curryTriggersLevelEvent = true;
    @Node(category="General", minValue=1.0)
    public static int lureRadius = 5;
    @Node(category="General", minValue=1.0)
    public static int lureTimer = 1200;
    @Node(category="General", minValue=1.0)
    public static int lureSpawns = 10;
    @Node(category="General", minValue=1.0)
    public static int lureShinyChance = 2048;
    @Node(category="General", minValue=1.0)
    public static float dynamaxScale = 3.0f;
    @Node(category="General", minValue=1.0)
    public static int dynamaxEnergyBeam = 5000;
    @Node(category="General", minValue=1.0)
    public static int wishingStar = 5000;
    @Node(category="General", minValue=1.0)
    public static float wishingStarDexCompletion = 0.6f;
    @Node(category="Spawning")
    public static int spawnTickRate = 60;
    @Node(category="Spawning", type=Integer.class)
    public static List<Integer> spawnDimensions = Collections.singletonList(0);
    @Node(category="Spawning", nameOverride="shinySpawnRate", minValue=0.0)
    public static float shinyRate = 4096.0f;
    @Node(category="Spawning", nameOverride="ultraShinySpawnRate", minValue=1.0)
    public static int ultraShinyRate = 16;
    @Node(category="Spawning", minValue=0.0)
    public static int bossSpawnTicks = 1200;
    @Node(category="Spawning")
    public static boolean spawnLevelsByDistance = false;
    @Node(category="Spawning")
    public static int maxLevelByDistance = 60;
    @Node(category="Spawning", minValue=0.0)
    public static int distancePerLevel = 30;
    @Node(category="Spawning", nameOverride="hiddenAbilitySpawnRate", minValue=0.0)
    public static int hiddenAbilityRate = 150;
    @Node(category="Spawning", nameOverride="displayLegendaryGlobalMessage")
    public static boolean doLegendaryEvent = true;
    @Node(category="Spawning")
    public static boolean replaceMCVillagers = true;
    @Node(category="Spawning", nameOverride="spawnPokeMarts")
    public static boolean spawnPokemarts = true;
    @Node(category="Spawning")
    public static boolean useRecentLevelMoves = false;
    @Node(category="Spawning")
    public static boolean spawnGyms = true;
    @Node(category="Spawning", minValue=0.0, maxValue=1.0)
    public static float gymChance = 0.125f;
    @Node(category="Spawning", minValue=0.0)
    public static int maxSpawnsPerTick = 100;
    @Node(category="Spawning", nameOverride="maxLandPokemon", minValue=0.0)
    public static int maxNumLandPokemon = 40;
    @Node(category="Spawning", nameOverride="maxUndergroundPokemon", minValue=0.0)
    public static int maxNumUndergroundPokemon = 20;
    @Node(category="Spawning", nameOverride="maxWaterPokemon", minValue=0.0)
    public static int maxNumWaterPokemon = 20;
    @Node(category="Spawning", nameOverride="maxFlyingPokemon", minValue=0.0)
    public static int maxNumAirPokemon = 2;
    @Node(category="Spawning", minValue=0.0)
    public static int maxNumNPCs = 4;
    @Node(category="Spawning", minValue=0.0)
    public static int maxNumBosses = 1;
    @Node(category="Spawning")
    public static boolean despawnOnFleeOrLoss = false;
    @Node(category="Spawning")
    public static float gmaxFactorSpawnRate = 512.0f;
    @Node(category="Server Info")
    public static String serverDisplayName;
    @Node(category="Server Info")
    public static String serverIconName;
    @Node(category="Server Info")
    public static String serverKey;
    @Node(category="Better Spawning")
    public static String spawnSetFolder;
    @Node(category="Better Spawning")
    public static int entitiesPerPlayer;
    @Node(category="Better Spawning")
    public static int spawnsPerPass;
    @Node(category="Better Spawning")
    public static float spawnFrequency;
    @Node(category="Better Spawning")
    public static float minimumDistanceBetweenSpawns;
    @Node(category="Better Spawning")
    public static int minimumDistanceFromCentre;
    @Node(category="Better Spawning")
    public static int maximumDistanceFromCentre;
    @Node(category="Better Spawning")
    public static float horizontalTrackFactor;
    @Node(category="Better Spawning")
    public static float verticalTrackFactor;
    @Node(category="Better Spawning")
    public static int horizontalSliceRadius;
    @Node(category="Better Spawning")
    public static int verticalSliceRadius;
    @Node(category="General")
    public static int pokeRusChance;
    @Node(category="General")
    public static int pokeRusTimerMin;
    @Node(category="General")
    public static int pokeRusTimerMax;
    @Node(category="General")
    public static int pokeRusSpreadRate;
    @Node(category="General", minValue=0.0)
    public static int catchingCharmMaxChance;
    @Node(category="General", minValue=0.0)
    public static int expCharmMaxChance;
    @Node(category="General", minValue=0.0)
    public static int ovalCharmMaxChance;
    @Node(category="General", minValue=0.0)
    public static int markCharmMaxChance;
    @Node(category="General", minValue=0.0)
    public static double bikeSpeed;
    @Node(category="Hordes")
    public static int hordeTriggerChance;
    @Node(category="Hordes")
    public static int hordeMaxSpawnAmount;
    @Node(category="Hordes", minValue=3.0)
    public static int hordeMinSpawnAmount;
    @Node(category="Hordes")
    public static int hordeMaxSpawnLevel;
    @Node(category="Hordes", minValue=1.0)
    public static int hordeMinSpawnLevel;
    @Node(category="Hordes")
    public static boolean hordesEnabled;
    @Node(category="General", nameOverride="showIVsEVs")
    public static boolean showIVsEVs;
    @Node(category="General")
    public static int healerSpeed;
    @Node(category="General")
    public static boolean pcsHealPokemon;
    @Node(category="General")
    public static boolean doWitherEffect;
    @Node(category="General")
    public static boolean doPokemonAttackPlayers;
    @Node(category="General")
    public static boolean stopTeleportingPokemon;
    @Node(category="General")
    public static boolean allowMasterBallEternatus;
    @Node(category="General")
    public static boolean allowMissingNoCapture;
    @Node(category="General")
    public static boolean hiddenName;
    @Node(category="Spawning", minValue=0.0)
    public static int maxNumAlphas;
    @Node(category="Spawning", minValue=0.0)
    public static int alphaSpawnTicks;
    @Node(category="General")
    public static boolean alphasSpawnHostile;
    @Node(category="General")
    public static int pokeBlockTickTime;
    @Node(category="Spawning", minValue=0.0)
    public static double totemConversionChance;
    @Node(category="General")
    public static boolean sneakingRareCandyLevelsDown;
    @Node(category="General")
    public static boolean catchAlphasOutsideOfBattle;
    @Node(category="Spawning")
    public static boolean playShinySoundOnShinySpawn;
    @Node(category="General")
    public static int defaultNPCTrainerAggroRange;
    @Node(category="General")
    public static double feederAttractChance;
    @Node(category="General")
    public static double feederNobleAttractChance;
    @Node(category="General")
    public static boolean allowCelestialFlute;
    @Node(category="General")
    public static boolean forceCloseBattleGuiOnLoss;
    @Node(category="Storage")
    public static boolean disableDefaultSaving;
    @Node(category="Spawning")
    public static boolean allowPokemonSpawns;
    @Node(category="Spawning")
    public static boolean allowNPCSpawns;
    @Node(category="Spawning")
    public static boolean allowAlphaSpawns;
    @Node(category="Spawning")
    public static boolean allowBossSpawns;
    @Node(category="Spawning")
    public static boolean allowTotemSpawns;
    @Node(category="Spawning")
    public static boolean allowNaturalSpawns;
    @Node(category="Performance")
    public static boolean useOptimizedEggStepCalculator;
    @Node(category="Performance")
    public static boolean useOptimizedRepelHandler;
    @Node(category="Performance")
    public static boolean useOptimizedWishingStarSpawner;
    @Node(category="Performance")
    public static boolean useOptimizedDynamaxEnergyBeamSpawner;
    @Node(category="Performance")
    public static boolean useOptimizedPokerusTimer;
    @Node(category="General")
    public static boolean allowSpaceTimeDistortions;
    @Node(category="General")
    public static int spaceTimeDistortionLifeTimer;
    @Node(category="General")
    public static int spaceTimeDistortionItemTimer;
    @Node(category="General")
    public static int spaceTimeDistortionPokemonTimer;
    @Node(category="General")
    public static double spaceTimeDistortionAlphaPokemonChance;
    @Node(category="General")
    public static int spaceTimeDistortionSpawnerTimer;
    @Node(category="General")
    public static double spaceTimeDistortionSpawnChance;
    @Node(category="General")
    public static int maxSpaceTimeDistortions;
    @Node(category="General")
    public static boolean allowMysteriousRings;
    @Node(category="General")
    public static int mysteriousRingSpawnInterval;
    @Node(category="General")
    public static double mysteriousRingSpawnChance;
    @Node(category="General")
    public static int mysteriousRingLifetime;
    @Node(category="General")
    public static int basculinRecoilAmount;
    @Node(category="General")
    public static boolean basculinResetRecoilOnFainting;
    @Node(category="Shiny Rates")
    public static boolean enableCatchCombos;
    @Node(category="Shiny Rates")
    public static boolean enableCatchComboShinyLock;
    @Node(category="Shiny Rates", nameOverride="shinyRateWithCharm", minValue=0.0)
    public static float shinyCharmRate;
    @Node(category="Shiny Rates", nameOverride="catchComboTier1", minValue=0.0)
    public static float catchComboTier1;
    @Node(category="Shiny Rates", nameOverride="catchComboTier1Charm", minValue=0.0)
    public static float catchComboTier1Charm;
    @Node(category="Shiny Rates", nameOverride="catchComboTier2", minValue=0.0)
    public static float catchComboTier2;
    @Node(category="Shiny Rates", nameOverride="catchComboTier2Charm", minValue=0.0)
    public static float catchComboTier2Charm;
    @Node(category="Shiny Rates", nameOverride="catchComboTier3", minValue=0.0)
    public static float catchComboTier3;
    @Node(category="Shiny Rates", nameOverride="catchComboTier3Charm", minValue=0.0)
    public static float catchComboTier3Charm;
    @Node(category="Economy")
    public static float happyHourMultiplier;
    @Node(category="Economy")
    public static float amuletCoinMultiplier;
    @Node(category="Economy")
    public static int payDayMultiplier;
    @Node(category="Economy")
    public static boolean goldRushEnabled;
    @Node(category="Economy")
    public static int goldRushSuccessionIncrement;
    @Node(category="Timing")
    public static int hoopaUnbound;
    @Node(category="Timing")
    public static int furRegrowth;
    @Node(category="Spawning.Gens")
    public static boolean Gen1;
    @Node(category="Spawning.Gens")
    public static boolean Gen2;
    @Node(category="Spawning.Gens")
    public static boolean Gen3;
    @Node(category="Spawning.Gens")
    public static boolean Gen4;
    @Node(category="Spawning.Gens")
    public static boolean Gen5;
    @Node(category="Spawning.Gens")
    public static boolean Gen6;
    @Node(category="Spawning.Gens")
    public static boolean Gen7;
    @Node(category="Spawning.Gens")
    public static boolean Gen8;
    @Node(category="PokeLoot")
    public static boolean spawnNormal;
    @Node(category="PokeLoot")
    public static boolean spawnHidden;
    @Node(category="PokeLoot")
    public static boolean spawnGrotto;
    @Node(category="PokeLoot")
    public static boolean wildPokemonHidingInLoot;
    @Node(category="PokeLoot", minValue=1.0)
    public static int wildPokemonLootSpawnChance;
    @Node(category="PokeLoot")
    public static EnumPokelootRate spawnRate;
    @Node(category="PokeLoot")
    public static EnumPokelootModes spawnMode;
    @Node(category="PokeLoot", nameOverride="timedLootReuseHours")
    public static int lootTime;
    @Node(category="PokeLoot", nameOverride="timedPokeStopReuseHours")
    public static int pokeStopLootTime;
    @Node(category="AFKHandler", nameOverride="enableAFKHandler")
    public static boolean afkHandlerOn;
    @Node(category="AFKHandler", nameOverride="afkActivateSeconds", minValue=0.0)
    public static int afkTimerActivateSeconds;
    @Node(category="AFKHandler", nameOverride="afkHandlerTurnSeconds", minValue=0.0)
    public static int afkTimerTurnSeconds;
    @Node(category="Graphics", minValue=1.0)
    public static double pokemonRenderDistance;
    @Node(category="Graphics", minValue=0.0)
    public static double renderDistanceWeight;
    @Node(category="Graphics", nameOverride="lowResTextures")
    public static boolean useLowResTextures;
    @Node(category="Graphics", nameOverride="useSmoothShadingOnPokeBalls")
    public static boolean enableSmoothPokeballShading;
    @Node(category="Graphics", nameOverride="useSmoothShadingOnPokemon")
    public static boolean enableSmoothPokemonShading;
    @Node(category="Graphics", nameOverride="useOriginalPokemonTexturesForStatues")
    public static boolean statuesUseOriginalPokemonTextures;
    @Node(category="Graphics", nameOverride="showCurrentAttackTarget")
    public static boolean showTarget;
    @Node(category="Graphics")
    public static boolean drawHealthBars;
    @Node(category="Graphics")
    public static boolean useBattleCamera;
    @Node(category="Graphics")
    public static boolean playerControlCamera;
    @Node(category="Graphics")
    public static boolean enableWindowIcon;
    @Node(category="Breeding")
    public static boolean allowBreeding;
    @Node(category="Breeding")
    public static boolean allowDittoDittoBreeding;
    @Node(category="Breeding")
    public static boolean allowRandomBreedingEggsToBeLegendary;
    @Node(category="Breeding")
    public static boolean allowRandomSpawnedEggsToBeLegendary;
    @Node(category="Breeding")
    public static boolean useBreedingEnvironment;
    @Node(category="Breeding", nameOverride="breedingEnvironmentCheckSeconds")
    public static int breedingEnviromentCheckSeconds;
    @Node(category="Breeding", minValue=0.0)
    public static int stepsPerEggCycle;
    @Node(category="Breeding", nameOverride="numBreedingStages", minValue=0.0)
    public static int numBreedingLevels;
    @Node(category="Breeding", minValue=20.0)
    public static int breedingTicks;
    @Node(category="Breeding")
    public static boolean allowRanchExpansion;
    @Node(category="Incubator")
    public static int ticksPerStep;
    @Node(category="PixelUtilities")
    public static boolean scaleGrassBattles;
    @Node(category="PixelUtilities")
    public static boolean blocksHaveLegendaries;
    @Node(category="PixelUtilities", nameOverride="pokeGiftReusable")
    public static boolean pokegiftMany;
    @Node(category="PixelUtilities", nameOverride="pokeGiftHaveEvents")
    public static boolean doPokegiftEvents;
    @Node(category="PixelUtilities", nameOverride="eventPokeGiftLoad")
    public static boolean isPokegiftEvent;
    @Node(category="PixelUtilities", nameOverride="eventHasLegendaries")
    public static boolean pokegiftEventLegendaries;
    @Node(category="PixelUtilities", nameOverride="eventHasShinies")
    public static boolean pokegiftEventShinies;
    @Node(category="PixelUtilities", nameOverride="eventMaxPokemon", minValue=0.0)
    public static int pokegiftEventMaxPokes;
    @Node(category="PixelUtilities", nameOverride="eventShinyRate", minValue=0.0)
    public static int pokegiftEventShinyRate;
    @Node(category="PixelUtilities", nameOverride="eventTime")
    public static String customPokegiftEventTime;
    @Node(category="PixelUtilities", nameOverride="eventCoords", type=String.class)
    public static List<String> pokegiftEventCoords;
    @Node(category="ExternalMoves")
    public static boolean allowExternalMoves;
    @Node(category="ExternalMoves")
    public static boolean allowDestructiveExternalMoves;
    @Node(category="Storage")
    public static boolean useAsyncSaving;
    @Node(category="Storage")
    public static int asyncInterval;
    @Node(category="Elevator")
    public static int elevatorSearchRange;
    @Node(category="Shrine")
    public static boolean limitRegigigas;
    @Node(category="Shrine")
    public static boolean limitRayquaza;
    @Node(category="Shrine")
    public static boolean limitHoOh;
    @Node(category="Shrine")
    public static int limitMew;
    @Node(category="Shrine")
    public static boolean disableArceusMethod;
    @Node(category="Performance")
    public static int tickRate24Radius;
    @Node(category="Performance")
    public static int tickRate48Radius;
    @Node(category="Performance")
    public static int tickRateHigherRadius;
    @Node(category="World Gen", type=String.class)
    public static List<String> pokeChestBiomeWhiteList;
    @Node(category="World Gen", type=String.class)
    public static List<String> pokeChestBiomeBlackList;
    @Node(category="World Gen")
    public static boolean generatePokeGass;
    @Node(category="General")
    public static boolean allowVanillaMusic;
    public static boolean useBattleDimension;
    public static boolean allowWildPokemonFleeing;
    public static boolean useExternalBlockSpawnFiles;
    private static HoconConfigurationLoader configurationLoader;
    private static CommentedConfigurationNode mainNode;

    public static void init(File configFile) {
        configurationLoader = ((HoconConfigurationLoader.Builder)HoconConfigurationLoader.builder().setFile(configFile)).build();
        try {
            PixelmonConfig.reload(true);
        }
        catch (IOException var2) {
            var2.printStackTrace();
        }
    }

    public static void reload(boolean loadFromDisk) throws IOException {
        if (loadFromDisk) {
            mainNode = (CommentedConfigurationNode)configurationLoader.load(ConfigurationOptions.defaults().setShouldCopyDefaults(true));
        }
        try {
            CustomStarters.loadConfig(mainNode);
        }
        catch (ObjectMappingException var15) {
            var15.printStackTrace();
        }
        String[] categories = new String[]{"General", "Spawning", "Spawning.Gens", "Better Spawning", "PokeLoot", "AFKHandler", "Graphics", "Breeding", "ExternalMoves", "PixelUtilities", "Elevator", "Performance"};
        for (String category : categories) {
            CommentedConfigurationNode categoryNode = mainNode;
            for (String cat : category.split("\\.")) {
                categoryNode = categoryNode.getNode(cat);
            }
            categoryNode.setComment(I18n.translateToLocal("pixelmon.config." + category.toLowerCase() + ".comment"));
        }
        for (Field field : PixelmonConfig.class.getDeclaredFields()) {
            if (!Modifier.isPublic(field.getModifiers()) || !Modifier.isStatic(field.getModifiers()) || field.getAnnotation(Node.class) == null) continue;
            Node node = field.getAnnotation(Node.class);
            String category = node.category();
            String name = node.nameOverride().isEmpty() ? field.getName() : node.nameOverride();
            String comment = "pixelmon.config." + name + ".comment";
            CommentedConfigurationNode categoryNode = mainNode;
            for (String cat : category.split("\\.")) {
                categoryNode = categoryNode.getNode(cat);
            }
            CommentedConfigurationNode configNode = categoryNode.getNode(name);
            configNode.setComment(I18n.translateToLocal(comment));
            PixelmonConfig.set(field, configNode, node);
        }
        configurationLoader.save(mainNode);
        Pixelmon.LOGGER.info("Checking for migrations..");
        PixelmonConfig.checkMigrations(PixelmonConfig.getConfig());
    }

    private static void set(Field field, CommentedConfigurationNode configNode, Node node) {
        try {
            Class<?> type = field.getType();
            if (type == Boolean.TYPE) {
                boolean value = configNode.getBoolean(field.getBoolean(null));
                field.setBoolean(null, value);
            } else if (type == Integer.TYPE) {
                int value = configNode.getInt(field.getInt(null));
                value = (double)value < node.minValue() ? (int)node.minValue() : ((double)value > node.maxValue() ? (int)node.maxValue() : value);
                field.setInt(null, value);
            } else if (type == Float.TYPE) {
                float value = configNode.getFloat(field.getFloat(null));
                value = (double)value < node.minValue() ? (float)node.minValue() : ((double)value > node.maxValue() ? (float)node.maxValue() : value);
                field.setFloat(null, value);
            } else if (type == Double.TYPE) {
                double value = configNode.getDouble(field.getDouble(null));
                value = value < node.minValue() ? node.minValue() : (value > node.maxValue() ? node.maxValue() : value);
                field.setDouble(null, value);
            } else if (type == String.class) {
                String value = configNode.getString((String)field.get(null));
                field.set(null, value);
            } else if (type == List.class) {
                List value = configNode.getList(TypeToken.of(node.type()), (List)field.get(null));
                field.set(null, value);
            } else if (Enum.class.isAssignableFrom(type)) {
                field.set(null, type.getEnumConstants()[configNode.getInt(((Enum)field.get(null)).ordinal())]);
            } else {
                Pixelmon.LOGGER.error("Cannot read config value " + configNode.getKey());
            }
        }
        catch (ObjectMappingException | IllegalAccessException var7) {
            var7.printStackTrace();
        }
    }

    public static void saveConfig() {
        try {
            configurationLoader.save(mainNode);
        }
        catch (IOException var1) {
            var1.printStackTrace();
        }
    }

    public static CommentedConfigurationNode getConfig() {
        return mainNode;
    }

    public static void disableEventLoading() {
        isPokegiftEvent = mainNode.getNode("PixelUtilities").getNode("eventPokeGiftLoad").setValue(false).getBoolean();
        PixelmonConfig.saveConfig();
    }

    public static double getGrowthModifier() {
        return growthModifier;
    }

    public static boolean isGenerationEnabled(int generation) {
        switch (generation) {
            case 1: {
                return Gen1;
            }
            case 2: {
                return Gen2;
            }
            case 3: {
                return Gen3;
            }
            case 4: {
                return Gen4;
            }
            case 5: {
                return Gen5;
            }
            case 6: {
                return Gen6;
            }
            case 7: {
                return Gen7;
            }
            case 8: {
                return Gen8;
            }
        }
        return false;
    }

    public static boolean allGenerationsDisabled() {
        return !Gen1 && !Gen2 && !Gen3 && !Gen4 && !Gen5 && !Gen6 && !Gen7 && !Gen8;
    }

    public static boolean allGenerationsEnabled() {
        return Gen1 && Gen2 && Gen3 && Gen4 && Gen5 && Gen6 && Gen7 && Gen8;
    }

    public static void checkMigrations(CommentedConfigurationNode configNode) {
        if (configNode.hasMapChildren()) {
            for (CommentedConfigurationNode commentedConfigurationNode : configNode.getChildrenMap().values()) {
                PixelmonConfig.checkMigrations(commentedConfigurationNode);
            }
        }
        MigrationHandler.handleMigration(configNode);
    }

    static {
        spawnSetFolder = "default";
        entitiesPerPlayer = 40;
        spawnsPerPass = 2;
        spawnFrequency = 60.0f;
        minimumDistanceBetweenSpawns = 8.0f;
        minimumDistanceFromCentre = 8;
        maximumDistanceFromCentre = 64;
        horizontalTrackFactor = 100.0f;
        verticalTrackFactor = 0.0f;
        horizontalSliceRadius = 16;
        verticalSliceRadius = 16;
        pokeRusChance = 21845;
        pokeRusTimerMin = 86400;
        pokeRusTimerMax = 345600;
        pokeRusSpreadRate = 20000;
        catchingCharmMaxChance = 1500;
        expCharmMaxChance = 3000;
        ovalCharmMaxChance = 500;
        markCharmMaxChance = 500;
        bikeSpeed = 0.7;
        hordeTriggerChance = 20;
        hordeMaxSpawnAmount = 5;
        hordeMinSpawnAmount = 3;
        hordeMaxSpawnLevel = 8;
        hordeMinSpawnLevel = 4;
        hordesEnabled = true;
        showIVsEVs = false;
        healerSpeed = 100;
        pcsHealPokemon = false;
        doWitherEffect = true;
        doPokemonAttackPlayers = false;
        stopTeleportingPokemon = false;
        allowMasterBallEternatus = false;
        allowMissingNoCapture = false;
        hiddenName = false;
        maxNumAlphas = 1;
        alphaSpawnTicks = 1200;
        alphasSpawnHostile = true;
        pokeBlockTickTime = 400;
        totemConversionChance = 0.009;
        sneakingRareCandyLevelsDown = false;
        catchAlphasOutsideOfBattle = false;
        playShinySoundOnShinySpawn = false;
        defaultNPCTrainerAggroRange = 2;
        feederAttractChance = 0.0025;
        feederNobleAttractChance = 0.15;
        allowCelestialFlute = true;
        forceCloseBattleGuiOnLoss = false;
        disableDefaultSaving = false;
        allowPokemonSpawns = true;
        allowNPCSpawns = true;
        allowAlphaSpawns = true;
        allowBossSpawns = true;
        allowTotemSpawns = true;
        allowNaturalSpawns = true;
        useOptimizedEggStepCalculator = false;
        useOptimizedRepelHandler = false;
        useOptimizedWishingStarSpawner = false;
        useOptimizedDynamaxEnergyBeamSpawner = false;
        useOptimizedPokerusTimer = false;
        allowSpaceTimeDistortions = true;
        spaceTimeDistortionLifeTimer = 300;
        spaceTimeDistortionItemTimer = 45;
        spaceTimeDistortionPokemonTimer = 35;
        spaceTimeDistortionAlphaPokemonChance = 0.09;
        spaceTimeDistortionSpawnerTimer = 3600;
        spaceTimeDistortionSpawnChance = 0.3;
        maxSpaceTimeDistortions = 1;
        allowMysteriousRings = false;
        mysteriousRingSpawnInterval = 2700;
        mysteriousRingSpawnChance = 0.35;
        mysteriousRingLifetime = 60;
        basculinRecoilAmount = 294;
        basculinResetRecoilOnFainting = true;
        enableCatchCombos = true;
        enableCatchComboShinyLock = true;
        shinyCharmRate = 3072.0f;
        catchComboTier1 = 1728.0f;
        catchComboTier1Charm = 1296.0f;
        catchComboTier2 = 1296.0f;
        catchComboTier2Charm = 972.0f;
        catchComboTier3 = 972.0f;
        catchComboTier3Charm = 729.0f;
        happyHourMultiplier = 2.0f;
        amuletCoinMultiplier = 2.0f;
        payDayMultiplier = 5;
        goldRushEnabled = true;
        goldRushSuccessionIncrement = 100;
        hoopaUnbound = 600000;
        furRegrowth = 600000;
        Gen1 = true;
        Gen2 = true;
        Gen3 = true;
        Gen4 = true;
        Gen5 = true;
        Gen6 = true;
        Gen7 = true;
        Gen8 = true;
        spawnNormal = true;
        spawnHidden = true;
        spawnGrotto = true;
        wildPokemonHidingInLoot = true;
        wildPokemonLootSpawnChance = 20;
        spawnRate = EnumPokelootRate.NORMAL;
        spawnMode = EnumPokelootModes.FCFS;
        lootTime = 24;
        pokeStopLootTime = 24;
        afkHandlerOn = false;
        afkTimerActivateSeconds = 90;
        afkTimerTurnSeconds = 15;
        pokemonRenderDistance = 64.0;
        renderDistanceWeight = 2.0;
        useLowResTextures = false;
        enableSmoothPokeballShading = true;
        enableSmoothPokemonShading = true;
        statuesUseOriginalPokemonTextures = true;
        showTarget = true;
        drawHealthBars = false;
        useBattleCamera = true;
        playerControlCamera = true;
        enableWindowIcon = true;
        allowBreeding = true;
        allowDittoDittoBreeding = true;
        allowRandomBreedingEggsToBeLegendary = false;
        allowRandomSpawnedEggsToBeLegendary = false;
        useBreedingEnvironment = true;
        breedingEnviromentCheckSeconds = 500;
        stepsPerEggCycle = 4;
        numBreedingLevels = 5;
        breedingTicks = 18000;
        allowRanchExpansion = true;
        ticksPerStep = 20;
        scaleGrassBattles = false;
        blocksHaveLegendaries = false;
        pokegiftMany = false;
        doPokegiftEvents = true;
        isPokegiftEvent = false;
        pokegiftEventLegendaries = false;
        pokegiftEventShinies = false;
        pokegiftEventMaxPokes = 1;
        pokegiftEventShinyRate = 10;
        customPokegiftEventTime = "D/M";
        pokegiftEventCoords = Collections.singletonList("notConfigured");
        allowExternalMoves = true;
        allowDestructiveExternalMoves = true;
        useAsyncSaving = false;
        asyncInterval = 60;
        elevatorSearchRange = 16;
        limitRegigigas = false;
        limitRayquaza = false;
        limitHoOh = false;
        limitMew = 3;
        disableArceusMethod = false;
        tickRate24Radius = 1;
        tickRate48Radius = 2;
        tickRateHigherRadius = 3;
        pokeChestBiomeWhiteList = ImmutableList.of("all");
        pokeChestBiomeBlackList = ImmutableList.of("oceans");
        generatePokeGass = false;
        allowVanillaMusic = false;
        useBattleDimension = false;
        allowWildPokemonFleeing = false;
        useExternalBlockSpawnFiles = false;
    }
}

