/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.network.packetHandlers.ServerConfigList;

public class PixelmonServerConfig {
    public static int maxLevel = PixelmonConfig.maxLevel;
    public static boolean afkHandlerOn = PixelmonConfig.afkHandlerOn;
    public static int afkTimerActivateSeconds = PixelmonConfig.afkTimerActivateSeconds;
    public static float ridingSpeedMultiplier = PixelmonConfig.ridingSpeedMultiplier;
    public static boolean hiddenName = PixelmonConfig.hiddenName;
    public static boolean alphasSpawnHostile = PixelmonConfig.alphasSpawnHostile;
    public static boolean showIVsEVs = PixelmonConfig.showIVsEVs;
    public static float pokeblockTickTime = PixelmonConfig.pokeBlockTickTime;
    public static boolean sneakingRareCandyLevelsDown = PixelmonConfig.sneakingRareCandyLevelsDown;
    public static boolean catchAlphasOutsideOfBattle = PixelmonConfig.catchAlphasOutsideOfBattle;
    public static boolean playShinySoundOnShinySpawn = PixelmonConfig.playShinySoundOnShinySpawn;
    public static int defaultNPCTrainerAggroRange = PixelmonConfig.defaultNPCTrainerAggroRange;
    public static boolean allowCelestialFlute = PixelmonConfig.allowCelestialFlute;
    public static boolean forceCloseBattleGuiOnLoss = PixelmonConfig.forceCloseBattleGuiOnLoss;
    public static boolean canPokemonBeHit = PixelmonConfig.canPokemonBeHit;
    public static boolean doPokemonAttackPlayers = PixelmonConfig.doPokemonAttackPlayers;
    public static int spaceTimeDistortionLifeTimer = PixelmonConfig.spaceTimeDistortionLifeTimer;
    public static int spaceTimeDistortionItemTimer = PixelmonConfig.spaceTimeDistortionItemTimer;
    public static int spaceTimeDistortionPokemonTimer = PixelmonConfig.spaceTimeDistortionPokemonTimer;
    public static double spaceTimeDistortionAlphaPokemonChance = PixelmonConfig.spaceTimeDistortionAlphaPokemonChance;
    public static int basculinRecoilAmount = PixelmonConfig.basculinRecoilAmount;
    public static boolean basculinResetRecoilOnFainting = PixelmonConfig.basculinResetRecoilOnFainting;

    public static void updateFromServer(ServerConfigList message) {
        maxLevel = message.maxLevel;
        ridingSpeedMultiplier = message.ridingSpeedMultiplier;
        afkHandlerOn = message.afkHandlerOn;
        afkTimerActivateSeconds = message.afkTimerActivateSeconds;
        hiddenName = message.hiddenNames;
        alphasSpawnHostile = message.alphasSpawnHostile;
        showIVsEVs = message.showIVsEVs;
        pokeblockTickTime = message.pokeBlockTickTime;
        sneakingRareCandyLevelsDown = message.sneakingRareCandyLevelsDown;
        catchAlphasOutsideOfBattle = message.catchAlphasOutsideOfBattle;
        playShinySoundOnShinySpawn = message.playShinySoundOnShinySpawn;
        defaultNPCTrainerAggroRange = message.defaultNPCTrainerAggroRange;
        allowCelestialFlute = message.allowCelestialFlute;
        forceCloseBattleGuiOnLoss = message.forceCloseBattleGuiOnLoss;
        canPokemonBeHit = message.canPokemonBeHit;
        doPokemonAttackPlayers = message.doPokemonAttackPlayers;
        spaceTimeDistortionLifeTimer = message.spaceTimeDistortionLifeTimer;
        spaceTimeDistortionItemTimer = message.spaceTimeDistortionItemTimer;
        spaceTimeDistortionPokemonTimer = message.spaceTimeDistortionPokemonTimer;
        spaceTimeDistortionAlphaPokemonChance = message.spaceTimeDistortionAlphaPokemonChance;
        basculinRecoilAmount = message.basculinRecoilAmount;
        basculinResetRecoilOnFainting = message.basculinResetRecoilOnFainting;
    }
}

