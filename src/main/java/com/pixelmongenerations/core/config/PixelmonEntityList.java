/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.common.entity.EntityChairMount;
import com.pixelmongenerations.common.entity.EntityDynamaxEnergy;
import com.pixelmongenerations.common.entity.EntityLegendFinder;
import com.pixelmongenerations.common.entity.EntityMagmaCrystal;
import com.pixelmongenerations.common.entity.EntityMysteriousRing;
import com.pixelmongenerations.common.entity.EntitySpaceTimeDistortion;
import com.pixelmongenerations.common.entity.EntityWishingStar;
import com.pixelmongenerations.common.entity.EntityZygardeCell;
import com.pixelmongenerations.common.entity.SpawningEntity;
import com.pixelmongenerations.common.entity.bikes.EntityBike;
import com.pixelmongenerations.common.entity.custom.EntityPixelmonPainting;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCChatting;
import com.pixelmongenerations.common.entity.npcs.NPCDamos;
import com.pixelmongenerations.common.entity.npcs.NPCGroomer;
import com.pixelmongenerations.common.entity.npcs.NPCNurseJoy;
import com.pixelmongenerations.common.entity.npcs.NPCRelearner;
import com.pixelmongenerations.common.entity.npcs.NPCShopkeeper;
import com.pixelmongenerations.common.entity.npcs.NPCSticker;
import com.pixelmongenerations.common.entity.npcs.NPCTrader;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.npcs.NPCTutor;
import com.pixelmongenerations.common.entity.npcs.NPCUltra;
import com.pixelmongenerations.common.entity.npcs.registry.BaseTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.EntityStatue;
import com.pixelmongenerations.common.entity.pokeballs.EntityEmptyPokeball;
import com.pixelmongenerations.common.entity.pokeballs.EntityOccupiedPokeball;
import com.pixelmongenerations.common.entity.projectiles.EntityArcanineRock;
import com.pixelmongenerations.common.entity.projectiles.EntityAvaluggIce;
import com.pixelmongenerations.common.entity.projectiles.EntityElectrodeLightning;
import com.pixelmongenerations.common.entity.projectiles.EntityForestBalm;
import com.pixelmongenerations.common.entity.projectiles.EntityHook;
import com.pixelmongenerations.common.entity.projectiles.EntityKleavorAxe;
import com.pixelmongenerations.common.entity.projectiles.EntityLilligantFlower;
import com.pixelmongenerations.common.entity.projectiles.EntityMarshBalm;
import com.pixelmongenerations.common.entity.projectiles.EntityMountainBalm;
import com.pixelmongenerations.common.entity.projectiles.EntitySnowBalm;
import com.pixelmongenerations.common.entity.projectiles.EntityVolcanoBalm;
import com.pixelmongenerations.common.spawning.SpawnRegistry;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.EnumEntityListClassType;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.HashMap;
import java.util.Map;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.datafix.fixes.EntityId;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.common.registry.EntityEntry;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

@GameRegistry.ObjectHolder(value="pixelmon")
public class PixelmonEntityList {
    public static final EntityEntry NPC_TRAINER = null;
    public static final EntityEntry HALLOWEEN = null;
    public static final EntityEntry NPC_TRADER = null;
    public static final EntityEntry PIXELMON = null;
    public static final EntityEntry STATUE = null;
    public static final EntityEntry NPC_CHATTING = null;
    public static final EntityEntry NPC_RELEARNER = null;
    public static final EntityEntry NPC_TUTOR = null;
    public static final EntityEntry EMPTY_POKEBALL = null;
    public static final EntityEntry HOOK = null;
    public static final EntityEntry CHAIR_MOUNT = null;
    public static final EntityEntry NPC_NURSEJOY = null;
    public static final EntityEntry NPC_SHOPKEEPER = null;
    public static final EntityEntry PAINTING = null;
    public static final EntityEntry OCCUPIED_POKEBALL = null;
    public static final EntityEntry LEGENDARY_FINDER = null;

    static void registerEntities() {
        int entityID = 0;
        ResourceLocation var10000 = new ResourceLocation("pixelmon", "npc_trainer");
        int var3 = entityID + 1;
        EntityRegistry.registerModEntity(var10000, NPCTrainer.class, "Trainer", entityID, Pixelmon.INSTANCE, 100, 1, true);
        int n = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "npc_trader"), NPCTrader.class, "Trader", n, Pixelmon.INSTANCE, 100, 1, true);
        int n2 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "pixelmon"), EntityPixelmon.class, "Pixelmon", n2, Pixelmon.INSTANCE, 100, 1, true);
        int n3 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "statue"), EntityStatue.class, "Statue", n3, Pixelmon.INSTANCE, 100, 1, true);
        int n4 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "npc_chatting"), NPCChatting.class, "ChattingNPC", n4, Pixelmon.INSTANCE, 100, 1, true);
        int n5 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "npc_relearner"), NPCRelearner.class, "Relearner", n5, Pixelmon.INSTANCE, 100, 1, true);
        int n6 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "npc_tutor"), NPCTutor.class, "Tutor", n6, Pixelmon.INSTANCE, 100, 1, true);
        int n7 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "empty_pokeball"), EntityEmptyPokeball.class, "Pokeball", n7, Pixelmon.INSTANCE, 80, 1, true);
        int n8 = ++var3;
        int hookId = n8;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "hook"), EntityHook.class, "Hook", hookId, Pixelmon.INSTANCE, 75, 1, true);
        int n9 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "chair_mount"), EntityChairMount.class, "EntityChairMount", n9, Pixelmon.INSTANCE, 80, 1, true);
        int n10 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "npc_nursejoy"), NPCNurseJoy.class, "NurseJoy", n10, Pixelmon.INSTANCE, 100, 1, true);
        int n11 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "npc_shopkeeper"), NPCShopkeeper.class, "Shopkeeper", n11, Pixelmon.INSTANCE, 100, 1, true);
        int n12 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "painting"), EntityPixelmonPainting.class, "PixelmonPainting", n12, Pixelmon.INSTANCE, 100, 1, true);
        int n13 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "occupied_pokeball"), EntityOccupiedPokeball.class, "OccupiedPokeball", n13, Pixelmon.INSTANCE, 80, 1, true);
        int n14 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "legend_finder"), EntityLegendFinder.class, "LegendFinder", n14, Pixelmon.INSTANCE, 80, 1, true);
        int n15 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "npc_groomer"), NPCGroomer.class, "NurseGroomer", n15, Pixelmon.INSTANCE, 100, 1, true);
        int n16 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "bike"), EntityBike.class, "EntityBike", n16, Pixelmon.INSTANCE, 100, 1, true);
        int n17 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "zygardecell"), EntityZygardeCell.class, "EntityZygardeCell", n17, Pixelmon.INSTANCE, 100, 1, true);
        int n18 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "spawning_entity"), SpawningEntity.class, "SpawningEntity", n18, Pixelmon.INSTANCE, 128, 1, false);
        int n19 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "npc_ultra"), NPCUltra.class, "Ultra", n19, Pixelmon.INSTANCE, 100, 1, true);
        int n20 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "npc_damos"), NPCDamos.class, "Damos", n20, Pixelmon.INSTANCE, 100, 1, true);
        int n21 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "magma_crystal"), EntityMagmaCrystal.class, "MagmaCrystal", n21, Pixelmon.INSTANCE, 100, 1, true);
        int n22 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "balm"), EntityForestBalm.class, "Balm", n22, Pixelmon.INSTANCE, 100, 1, true);
        int n23 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "balm"), EntityMarshBalm.class, "Balm", n23, Pixelmon.INSTANCE, 100, 1, true);
        int n24 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "balm"), EntityMountainBalm.class, "Balm", n24, Pixelmon.INSTANCE, 100, 1, true);
        int n25 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "balm"), EntitySnowBalm.class, "Balm", n25, Pixelmon.INSTANCE, 100, 1, true);
        int n26 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "balm"), EntityVolcanoBalm.class, "Balm", n26, Pixelmon.INSTANCE, 100, 1, true);
        int n27 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "dynamax_energy"), EntityDynamaxEnergy.class, "EntityDynamaxEnergy", n27, Pixelmon.INSTANCE, 100, 1, true);
        int n28 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "wishing_star"), EntityWishingStar.class, "EntityWishingStar", n28, Pixelmon.INSTANCE, 100, 1, true);
        int n29 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "npc_sticker"), NPCSticker.class, "Sticker", n29, Pixelmon.INSTANCE, 100, 1, true);
        int n30 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "kleavor_axe"), EntityKleavorAxe.class, "Kleavor Axe", n30, Pixelmon.INSTANCE, 100, 1, true);
        int n31 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "lilligant_flower"), EntityLilligantFlower.class, "Lilligant Flower", n31, Pixelmon.INSTANCE, 100, 1, true);
        int n32 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "arcanine_rock"), EntityArcanineRock.class, "Arcanine Rock", n32, Pixelmon.INSTANCE, 100, 1, true);
        int n33 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "electrode_lighting"), EntityElectrodeLightning.class, "Electrode Lightning", n33, Pixelmon.INSTANCE, 100, 1, true);
        int n34 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "avalugg_ice"), EntityAvaluggIce.class, "Avalugg Ice", n34, Pixelmon.INSTANCE, 100, 1, true);
        int n35 = ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "space_time_distortion"), EntitySpaceTimeDistortion.class, "EntitySpaceTimeDistortion", n35, Pixelmon.INSTANCE, 100, 1, true);
        int n36 = ++var3;
        ++var3;
        EntityRegistry.registerModEntity(new ResourceLocation("pixelmon", "mysterious_ring"), EntityMysteriousRing.class, "EntityMysteriousRing", n36, Pixelmon.INSTANCE, 100, 1, true);
        PixelmonEntityList.remap();
        ModContainer pixelmon = Loader.instance().getIndexedModList().get("pixelmon");
        EntityRegistry.instance().lookupModSpawn(pixelmon, hookId).setCustomSpawning(null, false);
    }

    public static EntityLiving createEntityByName(String entityName, World world) {
        return PixelmonEntityList.createEntityByName(entityName, world, null);
    }

    public static EntityLiving createEntityByName(String entityName, World world, String biomeID) {
        EntityLiving entity = null;
        try {
            EnumEntityListClassType type = null;
            if (EnumSpecies.hasPokemonAnyCase(entityName)) {
                type = EnumEntityListClassType.Pixelmon;
                entityName = EnumSpecies.getFromNameAnyCase((String)entityName).name;
            }
            if (type == EnumEntityListClassType.Pixelmon) {
                entity = new EntityPixelmon(world);
                ((EntityPixelmon)entity).init(entityName);
            } else if (ServerNPCRegistry.trainers.has(entityName)) {
                BaseTrainer trainer = ServerNPCRegistry.trainers.get(entityName);
                entity = new NPCTrainer(world);
                ((NPCTrainer)entity).init(trainer);
            } else if (entityName.toLowerCase().equals("zygardecell")) {
                entity = new EntityZygardeCell(world);
            } else if (entityName.toLowerCase().equalsIgnoreCase("space_time_distortion")) {
                entity = new EntitySpaceTimeDistortion(world);
            } else if (entityName.toLowerCase().equalsIgnoreCase("mysterious_ring")) {
                entity = new EntityMysteriousRing(world);
            } else if (entityName.toLowerCase().equals("dynamax_energy")) {
                entity = new EntityDynamaxEnergy(world);
            } else if (entityName.toLowerCase().equals("wishing_star")) {
                entity = new EntityWishingStar(world);
            } else {
                try {
                    EnumNPCType npcType = EnumNPCType.valueOf(entityName);
                    if (npcType == EnumNPCType.Trader) {
                        entity = new NPCTrader(world);
                    } else if (npcType == EnumNPCType.Tutor) {
                        entity = new NPCTutor(world);
                    } else if (npcType == EnumNPCType.Shopkeeper) {
                        entity = new NPCShopkeeper(world);
                        ((NPCShopkeeper)entity).initWanderingAI();
                        ((NPCShopkeeper)entity).initRandom(biomeID);
                    } else {
                        entity = npcType == EnumNPCType.Relearner ? new NPCRelearner(world) : new NPCTrainer(world);
                    }
                    ((EntityNPC)entity).init(entityName);
                }
                catch (IllegalArgumentException var6) {
                    entity = new NPCTrainer(world);
                }
            }
        }
        catch (Exception var7) {
            var7.printStackTrace();
        }
        return entity;
    }

    public static Entity createEntityFromNBT(NBTTagCompound par0NBTTagCompound, World par1World) {
        Entity entity = null;
        try {
            Class<EntityPixelmon> var3 = EntityPixelmon.class;
            entity = (EntityPixelmon)var3.getConstructor(World.class).newInstance(par1World);
            ((EntityPixelmon)entity).init(par0NBTTagCompound.getString("Name"));
        }
        catch (Exception var4) {
            var4.printStackTrace();
        }
        if (entity != null) {
            entity.readFromNBT(par0NBTTagCompound);
        } else if (PixelmonConfig.printErrors) {
            System.out.println("Skipping Entity with id " + par0NBTTagCompound.getString("id"));
        }
        return entity;
    }

    public static void addSpawns() {
        HashMap<String, String> hashmap = new HashMap<String, String>();
        Pixelmon.LOGGER.info("Registering entity spawns.");
        for (EnumSpecies pokemon : EnumSpecies.values()) {
            SpawnRegistry.getGenerationInfo(hashmap, pokemon.name);
        }
        if (ServerNPCRegistry.shopkeepers.hasRoaming()) {
            String[] biomeIDs = ServerNPCRegistry.shopkeepers.getRoamingBiomes();
            SpawnRegistry.addNPCSpawn("Shopkeeper", 1, EnumNPCType.Shopkeeper, biomeIDs);
        }
    }

    private static void remap() {
        Map remap = (Map)ObfuscationReflectionHelper.getPrivateValue(EntityId.class, null, 0);
        remap.put("pixelmon.Trainer", "pixelmon:npc_trainer");
        remap.put("pixelmon.Hallwoeen", "pixelmon:halloween");
        remap.put("pixelmon.Trader", "pixelmon:npc_trader");
        remap.put("pixelmon.Pixelmon", "pixelmon:pixelmon");
        remap.put("pixelmon.Statue", "pixelmon:statue");
        remap.put("pixelmon.ChattingNPC", "pixelmon:npc_chatting");
        remap.put("pixelmon.Relearner", "pixelmon:npc_relearner");
        remap.put("pixelmon.Tutor", "pixelmon:npc_tutor");
        remap.put("pixelmon.Pokeball", "pixelmon:empty_pokeball");
        remap.put("pixelmon.Hook", "pixelmon:hook");
        remap.put("pixelmon.EntityChairMount", "pixelmon:chair_mount");
        remap.put("pixelmon.NurseJoy", "pixelmon:npc_nursejoy");
        remap.put("pixelmon.Shopkeeper", "pixelmon:npc_shopkeeper");
        remap.put("pixelmon.PixelmonPainting", "pixelmon:painting");
        remap.put("pixelmon.OccupiedPokeball", "pixelmon:occupied_pokeball");
        remap.put("pixelmon.LegendFinder", "pixelmon:legend_finder");
        remap.put("pixelmon.Bike", "pixelmon:bike");
        remap.put("pixelmon.ZygardeCell", "pixelmon:zygardecell");
        remap.put("pixelmon.DynamaxEnergy", "pixelmon:dynamax_energy");
        remap.put("pixelmon.WishingStar", "pixelmon:wishing_star");
    }
}

