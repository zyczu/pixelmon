/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.common.item.ItemPokeblock;
import com.pixelmongenerations.core.enums.items.EnumPokeblock;
import net.minecraft.item.Item;

public class PixelmonItemsPokeblocks {
    public static Item blackPokeblock;
    public static Item bluePokeblock;
    public static Item brownPokeblock;
    public static Item greenPokeblock;
    public static Item pinkPokeblock;
    public static Item redPokeblock;
    public static Item whitePokeblock;
    public static Item yellowPokeblock;

    public static void load() {
        blackPokeblock = new ItemPokeblock(EnumPokeblock.Black_Pokeblock);
        bluePokeblock = new ItemPokeblock(EnumPokeblock.Blue_Pokeblock);
        brownPokeblock = new ItemPokeblock(EnumPokeblock.Brown_Pokeblock);
        greenPokeblock = new ItemPokeblock(EnumPokeblock.Green_Pokeblock);
        pinkPokeblock = new ItemPokeblock(EnumPokeblock.Pink_Pokeblock);
        redPokeblock = new ItemPokeblock(EnumPokeblock.Red_Pokeblock);
        whitePokeblock = new ItemPokeblock(EnumPokeblock.White_Pokeblock);
        yellowPokeblock = new ItemPokeblock(EnumPokeblock.Yellow_Pokeblock);
    }
}

