/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.core.config;

import com.pixelmongenerations.common.block.decorative.BlockUnown;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.util.helper.SpriteHelper;
import javax.annotation.Nullable;
import net.minecraft.block.Block;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;

class UnownBlockRecipe
implements IRecipe {
    private ItemStack output;
    private String tagSuffix;
    private ResourceLocation name;

    public UnownBlockRecipe(int damage) {
        int numUnownBlocks = BlockUnown.getNumUnownBlocks();
        int form = damage;
        if (form == numUnownBlocks - 2) {
            form = numUnownBlocks - 3;
        } else if (form == numUnownBlocks - 3) {
            form = numUnownBlocks - 2;
        }
        this.tagSuffix = SpriteHelper.getSpriteExtra("Unown", form);
        Block unownBlock = PixelmonBlocks.blockUnown;
        if (damage >= 16) {
            damage -= 16;
            unownBlock = PixelmonBlocks.blockUnown2;
        }
        this.output = new ItemStack(unownBlock, 1, damage);
        this.setRegistryName(new ResourceLocation("pixelmon", "unown_block" + ((BlockUnown)unownBlock).alphabetInUse[damage]));
    }

    @Override
    public boolean matches(InventoryCrafting inv, World worldIn) {
        boolean hasBlock = false;
        boolean hasPhoto = false;
        boolean hasExtra = false;
        String tag = "pixelmon:sprites/pokemon/201" + this.tagSuffix;
        for (int i = 0; i < inv.getSizeInventory(); ++i) {
            ItemStack currentItem = inv.getStackInSlot(i);
            if (currentItem.isEmpty()) continue;
            if (currentItem.getItem() == Item.getItemFromBlock(PixelmonBlocks.blockUnown2) && currentItem.getItemDamage() == BlockUnown.getBlankUnownBlockIndex() && !hasBlock) {
                hasBlock = true;
                continue;
            }
            if (!hasPhoto && currentItem.getItem() == PixelmonItems.itemPixelmonSprite && currentItem.hasTagCompound() && tag.equals(currentItem.getTagCompound().getString("SpriteName"))) {
                hasPhoto = true;
                continue;
            }
            hasExtra = true;
            break;
        }
        return hasBlock && hasPhoto && !hasExtra;
    }

    @Override
    public ItemStack getCraftingResult(InventoryCrafting inv) {
        return this.output.copy();
    }

    @Override
    public boolean canFit(int width, int height) {
        return width == 2 && height == 2;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return this.output;
    }

    @Override
    public IRecipe setRegistryName(ResourceLocation name) {
        this.name = name;
        return this;
    }

    @Override
    @Nullable
    public ResourceLocation getRegistryName() {
        return this.name;
    }

    @Override
    public Class<IRecipe> getRegistryType() {
        return IRecipe.class;
    }

    @Override
    public NonNullList<ItemStack> getRemainingItems(InventoryCrafting inv) {
        NonNullList<ItemStack> nonnulllist = NonNullList.withSize(inv.getSizeInventory(), ItemStack.EMPTY);
        for (int i = 0; i < nonnulllist.size(); ++i) {
            ItemStack itemStack = inv.getStackInSlot(i);
            nonnulllist.set(i, ForgeHooks.getContainerItem(itemStack));
        }
        return nonnulllist;
    }
}

