/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.network;

import net.minecraft.client.Minecraft;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.fml.common.FMLCommonHandler;
import org.jetbrains.annotations.NotNull;

public class PixelmonPotion
extends Potion {
    public PixelmonPotion(String name, boolean isBadEffect, EnumDyeColor color) {
        super(isBadEffect, FMLCommonHandler.instance().getSide().isClient() ? color.getColorValue() : 0xF9FFFE);
        this.setPotionName(name);
        this.setRegistryName("pixelmon", name);
    }

    public PotionEffect getEffect(boolean timesOut) {
        PotionEffect effect = new PotionEffect(this, 99999);
        effect.setPotionDurationMax(!timesOut);
        return effect;
    }

    public PotionEffect getEffect(int seconds) {
        return new PotionEffect(this, seconds * 20);
    }

    @Override
    public boolean hasStatusIcon() {
        return super.hasStatusIcon();
    }

    @Override
    public void renderHUDEffect(int x, int y, @NotNull PotionEffect effect, Minecraft mc, float alpha) {
        super.renderHUDEffect(x, y, effect, mc, alpha);
    }
}

