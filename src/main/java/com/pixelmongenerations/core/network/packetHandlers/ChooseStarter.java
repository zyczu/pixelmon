/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.api.enums.ReceiveType;
import com.pixelmongenerations.api.events.PixelmonReceivedEvent;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.starter.CustomStarters;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.config.StarterList;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.event.TickHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ChooseStarter
implements IMessage {
    int starterIndex;

    public ChooseStarter(int starterIndex) {
        this.starterIndex = starterIndex;
    }

    public ChooseStarter() {
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.starterIndex = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.starterIndex);
    }

    public static class Handler
    implements IMessageHandler<ChooseStarter, IMessage> {
        @Override
        public IMessage onMessage(ChooseStarter message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            player.getServerWorld().addScheduledTask(() -> {
                Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
                if (optstorage.isPresent()) {
                    PlayerStorage s = optstorage.get();
                    if (!s.starterPicked) {
                        PokemonForm pokemonForm = StarterList.getStarterList()[message.starterIndex];
                        EntityPixelmon p = (EntityPixelmon)PixelmonEntityList.createEntityByName(pokemonForm.pokemon.name, player.world);
                        p.getLvl().setLevel(CustomStarters.starterLevel);
                        if (CustomStarters.shinyStarter) {
                            p.setShiny(CustomStarters.shinyStarter);
                        }
                        if (p.hasForms()) {
                            p.setForm(pokemonForm.form);
                        }
                        p.setHealth(p.stats.HP);
                        p.loadMoveset();
                        p.caughtBall = EnumPokeball.PokeBall;
                        p.friendship.initFromCapture();
                        MinecraftForge.EVENT_BUS.post(new PixelmonReceivedEvent(player, ReceiveType.Starter, p));
                        s.addToParty(p);
                        s.starterPicked = true;
                        TickHandler.deregisterStarterList(player);
                    }
                }
            });
            return null;
        }
    }
}

