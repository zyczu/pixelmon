/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.PixelmonMovesetData;
import com.pixelmongenerations.core.network.PixelmonStatsData;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class LevelUp
implements IMessage {
    public int[] pokemonID;
    public int level;
    public PixelmonStatsData statsLevel1;
    public PixelmonStatsData statsLevel2;
    public PixelmonMovesetData[] moveset = new PixelmonMovesetData[4];

    public LevelUp() {
    }

    public LevelUp(EntityPixelmon p, int level, PixelmonStatsData statsLevel1, PixelmonStatsData statsLevel2) {
        this.pokemonID = p.getPokemonId();
        this.level = level;
        this.statsLevel1 = statsLevel1;
        this.statsLevel2 = statsLevel2;
    }

    public LevelUp(NBTTagCompound p, int level, PixelmonStatsData statsLevel1, PixelmonStatsData statsLevel2) {
        this.pokemonID = PixelmonMethods.getID(p);
        this.level = level;
        this.statsLevel1 = statsLevel1;
        this.statsLevel2 = statsLevel2;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.pokemonID = new int[]{buffer.readInt(), buffer.readInt()};
        this.level = buffer.readInt();
        this.statsLevel1 = new PixelmonStatsData();
        this.statsLevel1.readPacketData(buffer);
        this.statsLevel2 = new PixelmonStatsData();
        this.statsLevel2.readPacketData(buffer);
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pokemonID[0]);
        buffer.writeInt(this.pokemonID[1]);
        buffer.writeInt(this.level);
        this.statsLevel1.writePacketData(buffer);
        this.statsLevel2.writePacketData(buffer);
    }

    public static class Handler
    implements IMessageHandler<LevelUp, IMessage> {
        @Override
        public IMessage onMessage(LevelUp message, MessageContext ctx) {
            ClientProxy.battleManager.levelUpList.add(message);
            Minecraft.getMinecraft().addScheduledTask(() -> {
                if (!(Minecraft.getMinecraft().currentScreen instanceof GuiBattle)) {
                    Minecraft.getMinecraft().player.openGui(Pixelmon.INSTANCE, EnumGui.LevelUp.getIndex(), Minecraft.getMinecraft().player.world, 0, 0, 0);
                }
            });
            return null;
        }
    }
}

