/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.cookingpot;

import com.pixelmongenerations.common.block.tileEntities.TileEntityCookingPot;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class StartCooking
implements IMessage {
    int x;
    int y;
    int z;

    public StartCooking() {
    }

    public StartCooking(TileEntityCookingPot tile) {
        this.x = tile.getPos().getX();
        this.y = tile.getPos().getY();
        this.z = tile.getPos().getZ();
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
    }

    public static class Handler
    implements IMessageHandler<StartCooking, IMessage> {
        @Override
        public IMessage onMessage(StartCooking message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> {
                EntityPlayerMP player = ctx.getServerHandler().player;
                TileEntityCookingPot pot = (TileEntityCookingPot)player.world.getTileEntity(new BlockPos(message.x, message.y, message.z));
                if (pot == null) {
                    return;
                }
                pot.setCooking(!pot.isCooking());
            });
            return null;
        }
    }
}

