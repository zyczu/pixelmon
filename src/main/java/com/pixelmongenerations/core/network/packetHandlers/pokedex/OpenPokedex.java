/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokedex;

import com.pixelmongenerations.common.pokedex.Pokedex;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class OpenPokedex
implements IMessage {
    EnumSpecies pokemon;
    boolean reload;

    public OpenPokedex() {
    }

    public OpenPokedex(EnumSpecies pokemon) {
        this.pokemon = pokemon;
        this.reload = true;
    }

    public OpenPokedex(boolean reload) {
        this.reload = reload;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.pokemon = EnumSpecies.getFromOrdinal(buf.readInt());
        this.reload = buf.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.pokemon == null ? -1 : this.pokemon.ordinal());
        buf.writeBoolean(this.reload);
    }

    public static class Handler
    implements IMessageHandler<OpenPokedex, IMessage> {
        @Override
        public IMessage onMessage(OpenPokedex message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
            if (optstorage.isPresent()) {
                PlayerStorage storage = optstorage.get();
                int id = 1;
                boolean openInfo = false;
                Pokedex pokedex = storage.pokedex;
                if (message.reload) {
                    pokedex.sendToPlayer(player);
                }
                player.getServer().addScheduledTask(() -> player.openGui(Pixelmon.INSTANCE, openInfo ? EnumGui.PokedexInfo.getIndex() : EnumGui.Pokedex.getIndex(), player.world, id, 0, 0));
            }
            return null;
        }
    }
}

