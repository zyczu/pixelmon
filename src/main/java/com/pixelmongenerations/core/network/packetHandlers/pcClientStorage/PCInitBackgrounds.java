/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pcClientStorage;

import com.pixelmongenerations.api.pc.ClientBackground;
import com.pixelmongenerations.core.storage.PCClientStorage;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.List;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PCInitBackgrounds
implements IMessage {
    private List<ClientBackground> backgrounds;

    public PCInitBackgrounds() {
    }

    public PCInitBackgrounds(List<ClientBackground> backgrounds) {
        this.backgrounds = backgrounds;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.backgrounds = new ArrayList<ClientBackground>();
        int size = buf.readInt();
        for (int i = 0; i < size; ++i) {
            this.backgrounds.add(ClientBackground.of(ByteBufUtils.readUTF8String(buf), ByteBufUtils.readUTF8String(buf), buf.readBoolean()));
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.backgrounds.size());
        for (ClientBackground background : this.backgrounds) {
            ByteBufUtils.writeUTF8String(buf, background.getId());
            ByteBufUtils.writeUTF8String(buf, background.getName());
            buf.writeBoolean(background.hasUnlocked(null));
        }
    }

    public static class Handler
    implements IMessageHandler<PCInitBackgrounds, IMessage> {
        @Override
        public IMessage onMessage(PCInitBackgrounds message, MessageContext ctx) {
            PCClientStorage.setBackgroundList(message.backgrounds);
            return null;
        }
    }
}

