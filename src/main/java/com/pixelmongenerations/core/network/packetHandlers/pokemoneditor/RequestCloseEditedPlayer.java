/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.common.item.ItemPokemonEditor;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.CloseEditedPlayer;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class RequestCloseEditedPlayer
implements IMessage {
    UUID editedPlayer;

    public RequestCloseEditedPlayer() {
    }

    public RequestCloseEditedPlayer(UUID editedPlayer) {
        this.editedPlayer = editedPlayer;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        PixelmonMethods.toBytesUUID(buf, this.editedPlayer);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.editedPlayer = PixelmonMethods.fromBytesUUID(buf);
    }

    public static class Handler
    implements IMessageHandler<RequestCloseEditedPlayer, IMessage> {
        @Override
        public IMessage onMessage(RequestCloseEditedPlayer message, MessageContext ctx) {
            if (!ItemPokemonEditor.checkPermission(ctx.getServerHandler().player)) {
                return null;
            }
            EntityPlayerMP editedPlayer = ctx.getServerHandler().player.getServer().getPlayerList().getPlayerByUUID(message.editedPlayer);
            if (editedPlayer != null) {
                Pixelmon.NETWORK.sendTo(new CloseEditedPlayer(), editedPlayer);
            }
            return null;
        }
    }
}

