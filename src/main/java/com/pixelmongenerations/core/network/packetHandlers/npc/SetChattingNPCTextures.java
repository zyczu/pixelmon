/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.client.gui.npcEditor.GuiChattingNPCEditor;
import com.pixelmongenerations.common.entity.npcs.registry.ClientNPCData;
import com.pixelmongenerations.common.entity.npcs.registry.GeneralNPCData;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.core.util.helper.ArrayHelper;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.List;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetChattingNPCTextures
implements IMessage {
    private List<ClientNPCData> data = new ArrayList<ClientNPCData>();

    public SetChattingNPCTextures() {
        for (GeneralNPCData generalData : ServerNPCRegistry.getEnglishNPCs()) {
            for (String texture : generalData.getTextures()) {
                this.data.add(new ClientNPCData(generalData.id, texture));
            }
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ArrayHelper.encodeList(buf, this.data);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.data.clear();
        int listSize = buf.readInt();
        for (int i = 0; i < listSize; ++i) {
            this.data.add(new ClientNPCData(buf));
        }
    }

    public static class Handler
    implements IMessageHandler<SetChattingNPCTextures, IMessage> {
        @Override
        public IMessage onMessage(SetChattingNPCTextures message, MessageContext ctx) {
            GuiChattingNPCEditor.npcData = message.data;
            return null;
        }
    }
}

