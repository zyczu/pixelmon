/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.common.item.ItemPokemonEditor;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import java.util.UUID;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class RandomizePokemon
implements IMessage {
    UUID playerID;

    public RandomizePokemon() {
    }

    public RandomizePokemon(UUID playerUUID) {
        this.playerID = playerUUID;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        PixelmonMethods.toBytesUUID(buf, this.playerID);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.playerID = PixelmonMethods.fromBytesUUID(buf);
    }

    public static class Handler
    implements IMessageHandler<RandomizePokemon, IMessage> {
        @Override
        public IMessage onMessage(RandomizePokemon message, MessageContext ctx) {
            if (!ItemPokemonEditor.checkPermission(ctx.getServerHandler().player)) {
                return null;
            }
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID(ctx.getServerHandler().player.getServer(), message.playerID);
            if (optstorage.isPresent()) {
                PlayerStorage storage = optstorage.get();
                storage.randomizePokemon();
                ItemPokemonEditor.updateEditedPlayer(ctx.getServerHandler().player, message.playerID);
            }
            return null;
        }
    }
}

