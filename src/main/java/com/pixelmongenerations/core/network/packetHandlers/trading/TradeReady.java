/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.trading;

import com.pixelmongenerations.client.gui.ClientTradingManager;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class TradeReady
implements IMessage {
    boolean ready;

    public TradeReady() {
    }

    public TradeReady(boolean ready) {
        this.ready = ready;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeBoolean(this.ready);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.ready = buffer.readBoolean();
    }

    public static class Handler
    implements IMessageHandler<TradeReady, IMessage> {
        @Override
        public IMessage onMessage(TradeReady message, MessageContext ctx) {
            ClientTradingManager.player2Ready = message.ready;
            return null;
        }
    }
}

