/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.common.block.spawning.BlockCustomPixelmonGrass;
import com.pixelmongenerations.core.network.PixelmonGrassData;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UpdateGrass
implements IMessage {
    PixelmonGrassData data;

    public UpdateGrass() {
    }

    public UpdateGrass(PixelmonGrassData data) {
        this.data = data;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.data = new PixelmonGrassData();
        this.data.readPacketData(buffer);
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        this.data.writePacketData(buffer);
    }

    public static class Handler
    implements IMessageHandler<UpdateGrass, IMessage> {
        @Override
        public IMessage onMessage(UpdateGrass message, MessageContext ctx) {
            if (BlockCustomPixelmonGrass.checkPermission(ctx.getServerHandler().player)) {
                message.data.updateTileEntity(ctx.getServerHandler().player.world);
            }
            return null;
        }
    }
}

