/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class InteractNPC
implements IMessage {
    int npcID;
    EnumNPCType npcType;

    public InteractNPC() {
    }

    public InteractNPC(int npcID, EnumNPCType npcType) {
        this.npcID = npcID;
        this.npcType = npcType;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.npcID = buf.readInt();
        this.npcType = EnumNPCType.getFromOrdinal(buf.readInt());
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.npcID);
        buf.writeInt(this.npcType.ordinal());
    }

    public static class Handler
    implements IMessageHandler<InteractNPC, IMessage> {
        @Override
        public IMessage onMessage(InteractNPC message, MessageContext ctx) {
            ServerStorageDisplay.NPCInteractId = message.npcID;
            ClientProxy.battleManager.choosingPokemon = true;
            if (message.npcType == EnumNPCType.Relearner) {
                ClientProxy.battleManager.mode = BattleMode.ChooseRelearnMove;
            } else if (message.npcType == EnumNPCType.Tutor) {
                ClientProxy.battleManager.mode = BattleMode.ChooseTutor;
            }
            return null;
        }
    }
}

