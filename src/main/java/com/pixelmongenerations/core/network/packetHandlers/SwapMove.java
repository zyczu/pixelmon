/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SwapMove
implements IMessage {
    int[] pokemonId;
    int selected;
    int clicked;

    public SwapMove() {
    }

    public SwapMove(int[] pokemonId, int selected, int clicked) {
        this.pokemonId = pokemonId;
        this.selected = selected;
        this.clicked = clicked;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.pokemonId = new int[]{buffer.readInt(), buffer.readInt()};
        this.selected = buffer.readInt();
        this.clicked = buffer.readInt();
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pokemonId[0]);
        buffer.writeInt(this.pokemonId[1]);
        buffer.writeInt(this.selected);
        buffer.writeInt(this.clicked);
    }

    public static class Handler
    implements IMessageHandler<SwapMove, IMessage> {
        @Override
        public IMessage onMessage(SwapMove message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            player.getServer().addScheduledTask(() -> {
                int[] pokemonId = message.pokemonId;
                Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
                if (optstorage.isPresent()) {
                    PlayerStorage storage = optstorage.get();
                    if (storage.contains(pokemonId)) {
                        Optional<EntityPixelmon> pixelmonOptional = storage.getAlreadyExists(pokemonId, player.world);
                        EntityPixelmon p = pixelmonOptional.isPresent() ? pixelmonOptional.get() : storage.sendOut(pokemonId, player.world);
                        p.getMoveset().swap(message.selected, message.clicked);
                        storage.update(p, EnumUpdateType.Moveset);
                    } else if (PixelmonStorage.computerManager.getPlayerStorage(player).contains(pokemonId)) {
                        PlayerComputerStorage compStore = PixelmonStorage.computerManager.getPlayerStorage(player);
                        EntityPixelmon p = compStore.getPokemonEntity(pokemonId, player.world);
                        p.getMoveset().swap(message.selected, message.clicked);
                        compStore.updatePokemonEntry(p);
                    }
                }
            });
            return null;
        }
    }
}

