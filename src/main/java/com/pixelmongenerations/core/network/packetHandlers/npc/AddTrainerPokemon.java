/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.ClearTrainerPokemon;
import com.pixelmongenerations.core.network.packetHandlers.npc.StoreTrainerPokemon;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.ChangePokemonOpenGUI;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class AddTrainerPokemon
implements IMessage {
    int trainerID;

    public AddTrainerPokemon() {
    }

    public AddTrainerPokemon(int trainerId) {
        this.trainerID = trainerId;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.trainerID);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.trainerID = buffer.readInt();
    }

    public static class Handler
    implements IMessageHandler<AddTrainerPokemon, IMessage> {
        @Override
        public IMessage onMessage(AddTrainerPokemon message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            if (!ItemNPCEditor.checkPermission(player)) {
                return null;
            }
            player.getServer().addScheduledTask(() -> {
                NPCTrainer t;
                Optional<NPCTrainer> entityNPCOptional = EntityNPC.locateNPCServer(player.world, message.trainerID, NPCTrainer.class);
                if (entityNPCOptional.isPresent() && (t = entityNPCOptional.get()) != null) {
                    PlayerStorage storage = t.getPokemonStorage();
                    if (storage.hasSpace()) {
                        EntityPixelmon pix = (EntityPixelmon)PixelmonEntityList.createEntityByName("Magikarp", player.world);
                        storage.addToParty(pix);
                    }
                    t.updateLvl();
                    Pixelmon.NETWORK.sendTo(new ClearTrainerPokemon(), player);
                    for (int i = 0; i < storage.count(); ++i) {
                        Pixelmon.NETWORK.sendTo(new StoreTrainerPokemon(new PixelmonData(storage.getList()[i])), player);
                    }
                    Pixelmon.NETWORK.sendTo(new ChangePokemonOpenGUI(storage.count() - 1), player);
                }
            });
            return null;
        }
    }
}

