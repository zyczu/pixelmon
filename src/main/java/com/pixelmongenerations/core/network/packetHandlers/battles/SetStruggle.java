/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetStruggle
implements IMessage {
    public boolean[][] targetting;
    public int battleIndex;
    public int[] pokemonId;

    public SetStruggle() {
    }

    public SetStruggle(int[] pokemonId, boolean[][] targetting, int battleIndex) {
        this.pokemonId = pokemonId;
        this.battleIndex = battleIndex;
        this.targetting = targetting;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        int i;
        this.pokemonId = new int[]{buffer.readInt(), buffer.readInt()};
        this.battleIndex = buffer.readInt();
        this.targetting = new boolean[buffer.readShort()][];
        for (i = 0; i < this.targetting.length; ++i) {
            this.targetting[i] = new boolean[buffer.readShort()];
        }
        for (i = 0; i < this.targetting.length; ++i) {
            for (int j = 0; j < this.targetting[i].length; ++j) {
                this.targetting[i][j] = buffer.readBoolean();
            }
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pokemonId[0]);
        buffer.writeInt(this.pokemonId[1]);
        buffer.writeInt(this.battleIndex);
        buffer.writeShort(this.targetting.length);
        for (boolean[] aTargetting1 : this.targetting) {
            buffer.writeShort(aTargetting1.length);
        }
        boolean[][] arrbl = this.targetting;
        int n = arrbl.length;
        for (int i = 0; i < n; ++i) {
            boolean[] aTargetting;
            for (boolean b : aTargetting = arrbl[i]) {
                buffer.writeBoolean(b);
            }
        }
    }

    public static class Handler
    implements IMessageHandler<SetStruggle, IMessage> {
        @Override
        public IMessage onMessage(SetStruggle message, MessageContext ctx) {
            BattleControllerBase bc = BattleRegistry.getBattle(message.battleIndex);
            for (BattleParticipant p : bc.participants) {
                for (PixelmonWrapper pw : p.controlledPokemon) {
                    if (!PixelmonMethods.isIDSame(pw, message.pokemonId)) continue;
                    ArrayList<PixelmonWrapper> targets = this.findTargets(message, bc, p);
                    pw.setStruggle(targets);
                }
            }
            return null;
        }

        private ArrayList<PixelmonWrapper> findTargets(SetStruggle message, BattleControllerBase bc, BattleParticipant p) {
            ArrayList<PixelmonWrapper> targets = new ArrayList<PixelmonWrapper>();
            ArrayList<PixelmonWrapper> teamPokemon = bc.getTeamPokemon(p);
            for (int i = 0; i < message.targetting[0].length; ++i) {
                if (!message.targetting[0][i]) continue;
                targets.add(teamPokemon.get(i));
            }
            ArrayList<PixelmonWrapper> opponentPokemon = bc.getOpponentPokemon(p);
            for (int i = 0; i < message.targetting[0].length; ++i) {
                if (!message.targetting[1][i]) continue;
                targets.add(opponentPokemon.get(i));
            }
            return targets;
        }
    }
}

