/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.customOverlays;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UnregisterOverlay
implements IMessage {
    public String overlayId;

    public UnregisterOverlay(String overlayId) {
        this.overlayId = overlayId;
    }

    public UnregisterOverlay() {
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        ByteBufUtils.writeUTF8String(buffer, this.overlayId);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.overlayId = ByteBufUtils.readUTF8String(buffer);
    }

    public static class Handler
    implements IMessageHandler<UnregisterOverlay, IMessage> {
        @Override
        public IMessage onMessage(UnregisterOverlay message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> GuiPixelmonOverlay.unregisterOverlay(message.overlayId));
            return null;
        }
    }
}

