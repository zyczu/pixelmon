/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.dialogue;

import com.pixelmongenerations.api.events.dialogue.DialogueEndedEvent;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class DialogueClosure
implements IMessage {
    @Override
    public void fromBytes(ByteBuf buf) {
    }

    @Override
    public void toBytes(ByteBuf buf) {
    }

    public static class Handler
    implements IMessageHandler<DialogueClosure, IMessage> {
        @Override
        public IMessage onMessage(DialogueClosure message, MessageContext ctx) {
            MinecraftForge.EVENT_BUS.post(new DialogueEndedEvent(ctx.getServerHandler().player));
            return null;
        }
    }
}

