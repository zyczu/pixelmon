/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.customOverlays;

import com.pixelmongenerations.client.gui.custom.overlays.CustomScoreboardOverlay;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.Collection;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class CustomScoreboardUpdatePacket
implements IMessage {
    private String title;
    private Collection<String> lines;
    private Collection<String> scores;

    public CustomScoreboardUpdatePacket() {
    }

    public CustomScoreboardUpdatePacket(String title, Collection<String> lines, Collection<String> scores) {
        this.title = title;
        this.lines = lines;
        this.scores = scores;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.title = ByteBufUtils.readUTF8String(buf);
        int lineAmount = buf.readByte();
        this.lines = new ArrayList<String>(lineAmount);
        for (int i = 0; i < lineAmount; ++i) {
            this.lines.add(ByteBufUtils.readUTF8String(buf));
        }
        int scoreAmount = buf.readShort();
        if (scoreAmount != 254) {
            this.scores = new ArrayList<String>(scoreAmount);
            for (int i = 0; i < scoreAmount; ++i) {
                this.scores.add(ByteBufUtils.readUTF8String(buf));
            }
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.title);
        buf.writeByte(this.lines.size());
        for (String line : this.lines) {
            ByteBufUtils.writeUTF8String(buf, line);
        }
        if (this.scores != null) {
            buf.writeShort(this.scores.size());
            for (String score : this.scores) {
                ByteBufUtils.writeUTF8String(buf, score);
            }
        } else {
            buf.writeShort(254);
        }
    }

    public static class Handler
    implements IMessageHandler<CustomScoreboardUpdatePacket, IMessage> {
        @Override
        public IMessage onMessage(CustomScoreboardUpdatePacket message, MessageContext ctx) {
            CustomScoreboardOverlay.resetBoard();
            CustomScoreboardOverlay.populate(null, message.title, message.lines, message.scores);
            return null;
        }
    }
}

