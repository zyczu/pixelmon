/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.Optional;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ChooseAttack
implements IMessage {
    public int buttonId;
    public boolean[][] targetting;
    public int battleIndex;
    public int[] pokemonId;
    public boolean evolving;
    public boolean dynamaxing;

    public ChooseAttack() {
    }

    public ChooseAttack(int[] pokemonId, boolean[][] targetting, int buttonId, int battleIndex, boolean evolving, boolean dynamaxing) {
        this.pokemonId = pokemonId;
        this.buttonId = buttonId;
        this.battleIndex = battleIndex;
        this.targetting = targetting;
        this.evolving = evolving;
        this.dynamaxing = dynamaxing;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        int i;
        this.pokemonId = new int[]{buffer.readInt(), buffer.readInt()};
        this.buttonId = buffer.readInt();
        this.battleIndex = buffer.readInt();
        this.targetting = new boolean[buffer.readShort()][];
        for (i = 0; i < this.targetting.length; ++i) {
            this.targetting[i] = new boolean[buffer.readShort()];
        }
        for (i = 0; i < this.targetting.length; ++i) {
            for (int j = 0; j < this.targetting[i].length; ++j) {
                this.targetting[i][j] = buffer.readBoolean();
            }
        }
        this.evolving = buffer.readBoolean();
        this.dynamaxing = buffer.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pokemonId[0]);
        buffer.writeInt(this.pokemonId[1]);
        buffer.writeInt(this.buttonId);
        buffer.writeInt(this.battleIndex);
        buffer.writeShort(this.targetting.length);
        for (boolean[] aTargetting1 : this.targetting) {
            buffer.writeShort(aTargetting1.length);
        }
        boolean[][] arrbl = this.targetting;
        int n = arrbl.length;
        for (int i = 0; i < n; ++i) {
            boolean[] aTargetting;
            for (boolean b : aTargetting = arrbl[i]) {
                buffer.writeBoolean(b);
            }
        }
        buffer.writeBoolean(this.evolving);
        buffer.writeBoolean(this.dynamaxing);
    }

    public static class Handler
    implements IMessageHandler<ChooseAttack, IMessage> {
        @Override
        public IMessage onMessage(ChooseAttack message, MessageContext ctx) {
            BattleControllerBase bc = BattleRegistry.getBattle(message.battleIndex);
            if (bc == null) {
                return null;
            }
            PixelmonWrapper pw = bc.getPokemonFromID(message.pokemonId);
            if (pw != null) {
                BattleParticipant p = pw.getParticipant();
                if (message.buttonId == -1) {
                    pw.chooseMove(p.getBattleAI().getNextMove(pw));
                } else {
                    if (message.evolving) {
                        Optional<PlayerStorage> storage = PixelmonStorage.pokeBallManager.getPlayerStorage(ctx.getServerHandler().player);
                        if (storage.isPresent()) {
                            if (!storage.get().megaData.getMegaItem().canEvolve() && !PixelmonWrapper.canUltraBurst(pw.getHeldItem(), pw.getSpecies(), (short)pw.getForm())) {
                                message.evolving = false;
                            }
                        } else {
                            message.evolving = false;
                        }
                    }
                    ArrayList<PixelmonWrapper> targets = this.findTargets(message, bc, p);
                    pw.setAttack(message.buttonId, targets, message.evolving, message.dynamaxing);
                }
            }
            return null;
        }

        private ArrayList<PixelmonWrapper> findTargets(ChooseAttack message, BattleControllerBase bc, BattleParticipant p) {
            ArrayList<PixelmonWrapper> targets = new ArrayList<PixelmonWrapper>();
            ArrayList<PixelmonWrapper> teamPokemon = bc.getTeamPokemon(p);
            for (int i = 0; i < message.targetting[0].length; ++i) {
                if (!message.targetting[0][i] || i > teamPokemon.size()) continue;
                targets.add(teamPokemon.get(i));
            }
            ArrayList<PixelmonWrapper> opponentPokemon = bc.getOpponentPokemon(p);
            for (int i = 0; i < message.targetting[1].length; ++i) {
                if (!message.targetting[1][i]) continue;
                if (i < opponentPokemon.size()) {
                    targets.add(opponentPokemon.get(i));
                    continue;
                }
                System.out.println("Printing Opponent Pokemon: " + opponentPokemon);
                PixelmonWrapper fallback = opponentPokemon.get(opponentPokemon.size() - 1);
                if (targets.contains(fallback)) break;
                targets.add(fallback);
                break;
            }
            return targets;
        }
    }
}

