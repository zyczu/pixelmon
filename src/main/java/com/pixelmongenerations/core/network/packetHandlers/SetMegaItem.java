/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.core.enums.EnumMegaItem;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetMegaItem
implements IMessage {
    EnumMegaItem megaItem;

    public SetMegaItem() {
    }

    public SetMegaItem(EnumMegaItem megaItem) {
        this.megaItem = megaItem;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.megaItem = EnumMegaItem.values()[buffer.readInt()];
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.megaItem.ordinal());
    }

    public static class Handler
    implements IMessageHandler<SetMegaItem, IMessage> {
        @Override
        public IMessage onMessage(SetMegaItem message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            Optional<PlayerStorage> storage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
            if (storage.isPresent()) {
                if (storage.get().megaData.canEquipMegaItem()) {
                    storage.get().megaData.setMegaItem(message.megaItem, false);
                } else {
                    storage.get().megaData.setMegaItem(EnumMegaItem.Disabled, false);
                }
            }
            return null;
        }
    }
}

