/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pcClientStorage;

import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.storage.PCClientStorage;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PCAdd
implements IMessage {
    PixelmonData data;

    public PCAdd() {
    }

    public PCAdd(PixelmonData data) {
        this.data = data;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        this.data.encodeInto(buffer);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.data = new PixelmonData();
        this.data.decodeInto(buffer);
    }

    public static class Handler
    implements IMessageHandler<PCAdd, IMessage> {
        @Override
        public IMessage onMessage(PCAdd message, MessageContext ctx) {
            PCClientStorage.addToList(message.data);
            return null;
        }
    }
}

