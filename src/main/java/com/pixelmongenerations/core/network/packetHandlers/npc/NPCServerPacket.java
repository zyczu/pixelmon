/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.common.battle.rules.BattleRules;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClauseRegistry;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCChatting;
import com.pixelmongenerations.common.entity.npcs.NPCDamos;
import com.pixelmongenerations.common.entity.npcs.NPCGroomer;
import com.pixelmongenerations.common.entity.npcs.NPCNurseJoy;
import com.pixelmongenerations.common.entity.npcs.NPCRelearner;
import com.pixelmongenerations.common.entity.npcs.NPCShopkeeper;
import com.pixelmongenerations.common.entity.npcs.NPCSticker;
import com.pixelmongenerations.common.entity.npcs.NPCTrader;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.npcs.NPCTutor;
import com.pixelmongenerations.common.entity.npcs.NPCUltra;
import com.pixelmongenerations.common.entity.npcs.registry.ClientNPCData;
import com.pixelmongenerations.common.entity.npcs.registry.GeneralNPCData;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryTrainers;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.entity.npcs.registry.ShopkeeperData;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumEncounterMode;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumTrainerAI;
import com.pixelmongenerations.core.enums.battle.EnumBattleAIMode;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.SetTrainerData;
import com.pixelmongenerations.core.network.packetHandlers.ClearTrainerPokemon;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.UpdateClientRules;
import com.pixelmongenerations.core.network.packetHandlers.npc.EnumNPCServerPacketType;
import com.pixelmongenerations.core.network.packetHandlers.npc.SetNPCEditData;
import com.pixelmongenerations.core.network.packetHandlers.npc.StoreTrainerPokemon;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.ChangePokemonOpenGUI;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class NPCServerPacket
implements IMessage {
    EnumNPCServerPacketType mode;
    int npcID;
    EnumTrainerAI ai;
    EnumBattleAIMode battleAI;
    EnumBossMode bm;
    EnumEncounterMode em;
    BattleRules battleRules;
    int modelIndex;
    String data;
    EnumSpecies pokemon;
    int pos;
    private int integer;
    EnumNPCType npcType;
    float floatData;
    BlockPos blockPos;
    ArrayList<String> pages;
    String exchange;
    String offer;
    int level;
    boolean shiny;
    int form;
    ClientNPCData npcData;
    String shopkeeperType;

    public NPCServerPacket() {
    }

    public NPCServerPacket(int npcID) {
        this.npcID = npcID;
    }

    public NPCServerPacket(EnumNPCType npcType, BlockPos pos, float rotationDirection) {
        this.mode = EnumNPCServerPacketType.ChooseNPC;
        this.npcType = npcType;
        this.floatData = rotationDirection;
        this.blockPos = pos;
    }

    public NPCServerPacket(int npcID, EnumTrainerAI ai) {
        this(npcID);
        this.mode = EnumNPCServerPacketType.AI;
        this.ai = ai;
    }

    public NPCServerPacket(int npcID, EnumBattleAIMode battleAI) {
        this(npcID);
        this.mode = EnumNPCServerPacketType.BattleAI;
        this.battleAI = battleAI;
    }

    public NPCServerPacket(int npcID, EnumBossMode bm) {
        this(npcID);
        this.mode = EnumNPCServerPacketType.BossMode;
        this.bm = bm;
    }

    public NPCServerPacket(int npcID, EnumEncounterMode bm) {
        this(npcID);
        this.mode = EnumNPCServerPacketType.EncounterMode;
        this.em = bm;
    }

    public NPCServerPacket(int npcID, BattleRules battleRules) {
        this(npcID);
        this.mode = EnumNPCServerPacketType.BattleRules;
        this.battleRules = battleRules;
    }

    public NPCServerPacket(int npcID, int modelIndex) {
        this(npcID);
        this.mode = EnumNPCServerPacketType.Model;
        this.modelIndex = modelIndex;
    }

    public NPCServerPacket(int npcID, EnumNPCServerPacketType mode, String data) {
        this(npcID);
        this.mode = mode;
        this.data = data;
    }

    public NPCServerPacket(int npcID, EnumSpecies pokemon, int pos) {
        this(npcID);
        this.mode = EnumNPCServerPacketType.Pokemon;
        this.pokemon = pokemon;
        this.pos = pos;
    }

    public NPCServerPacket(int npcID, EnumNPCServerPacketType mode, int textureIndex) {
        this(npcID);
        this.mode = mode;
        this.integer = textureIndex;
    }

    public NPCServerPacket(int npcID, EnumNPCServerPacketType mode) {
        this(npcID);
        this.mode = mode;
    }

    public NPCServerPacket(int npcID, ClientNPCData npcData) {
        this(npcID);
        this.mode = EnumNPCServerPacketType.CycleTexture;
        this.npcData = npcData;
    }

    public NPCServerPacket(int npcID, ArrayList<String> pages) {
        this(npcID);
        this.mode = EnumNPCServerPacketType.ChatPages;
        this.pages = pages;
    }

    public NPCServerPacket(int npcID, String exchange, String offer, int level, boolean shiny, int form) {
        this(npcID);
        this.mode = EnumNPCServerPacketType.Trader;
        this.exchange = exchange;
        this.offer = offer;
        this.level = level;
        this.shiny = shiny;
        this.form = form;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeShort(this.mode.ordinal());
        buffer.writeInt(this.npcID);
        switch (this.mode) {
            case AI: {
                buffer.writeInt(this.ai.ordinal());
                break;
            }
            case BattleAI: {
                buffer.writeInt(this.battleAI.ordinal());
                break;
            }
            case BossMode: {
                buffer.writeInt(this.bm.ordinal());
                break;
            }
            case EncounterMode: {
                buffer.writeInt(this.em.ordinal());
                break;
            }
            case BattleRules: {
                this.battleRules.encodeInto(buffer);
                break;
            }
            case Model: {
                buffer.writeInt(this.modelIndex);
                break;
            }
            case CustomSteveTexture: 
            case Name: 
            case CycleJson: {
                ByteBufUtils.writeUTF8String(buffer, this.data);
                break;
            }
            case Pokemon: {
                buffer.writeInt(this.pokemon.ordinal());
                buffer.writeInt(this.pos);
                break;
            }
            case TextureIndex: 
            case CycleName: {
                buffer.writeInt(this.integer);
                break;
            }
            case ChooseNPC: {
                buffer.writeShort(this.npcType.ordinal());
                buffer.writeInt(this.blockPos.getX());
                buffer.writeInt(this.blockPos.getY());
                buffer.writeInt(this.blockPos.getZ());
                buffer.writeFloat(this.floatData);
                break;
            }
            case ChatPages: {
                buffer.writeInt(this.pages.size());
                for (String page : this.pages) {
                    ByteBufUtils.writeUTF8String(buffer, page);
                }
                return;
            }
            case Trader: {
                ByteBufUtils.writeUTF8String(buffer, this.exchange);
                ByteBufUtils.writeUTF8String(buffer, this.offer);
                buffer.writeInt(this.level);
                buffer.writeBoolean(this.shiny);
                buffer.writeInt(this.form);
                break;
            }
            case CycleTexture: {
                if (this.npcData == null) {
                    this.npcData = new ClientNPCData("");
                }
                this.npcData.encodeInto(buffer);
            }
        }
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.mode = EnumNPCServerPacketType.values()[buffer.readShort()];
        this.npcID = buffer.readInt();
        switch (this.mode) {
            case AI: {
                this.ai = EnumTrainerAI.getFromOrdinal(buffer.readInt());
                break;
            }
            case BattleAI: {
                this.battleAI = EnumBattleAIMode.getFromIndex(buffer.readInt());
                break;
            }
            case BossMode: {
                this.bm = EnumBossMode.getMode(buffer.readInt());
                break;
            }
            case EncounterMode: {
                this.em = EnumEncounterMode.getFromIndex(buffer.readInt());
                break;
            }
            case BattleRules: {
                this.battleRules = new BattleRules(buffer);
                break;
            }
            case Model: {
                this.modelIndex = buffer.readInt();
                break;
            }
            case CustomSteveTexture: 
            case Name: 
            case CycleJson: {
                this.data = ByteBufUtils.readUTF8String(buffer);
                break;
            }
            case Pokemon: {
                this.pokemon = EnumSpecies.getFromOrdinal(buffer.readInt());
                this.pos = buffer.readInt();
                break;
            }
            case TextureIndex: 
            case CycleName: {
                this.integer = buffer.readInt();
                break;
            }
            case ChooseNPC: {
                this.npcType = EnumNPCType.getFromOrdinal(buffer.readShort());
                this.blockPos = new BlockPos(buffer.readInt(), buffer.readInt(), buffer.readInt());
                this.floatData = buffer.readFloat();
                break;
            }
            case ChatPages: {
                int numPages = buffer.readInt();
                this.pages = new ArrayList();
                for (int i = 0; i < numPages; ++i) {
                    this.pages.add(ByteBufUtils.readUTF8String(buffer));
                }
                return;
            }
            case Trader: {
                this.exchange = ByteBufUtils.readUTF8String(buffer);
                this.offer = ByteBufUtils.readUTF8String(buffer);
                this.level = buffer.readInt();
                this.shiny = buffer.readBoolean();
                this.form = buffer.readInt();
                break;
            }
            case CycleTexture: {
                this.npcData = new ClientNPCData(buffer);
            }
        }
    }

    public static class Handler
    implements IMessageHandler<NPCServerPacket, IMessage> {
        @Override
        public IMessage onMessage(NPCServerPacket message, MessageContext ctx) {
            ctx.getServerHandler().player.getServerWorld().addScheduledTask(() -> {
                EntityPlayerMP p = ctx.getServerHandler().player;
                if (!ItemNPCEditor.checkPermission(p)) {
                    return;
                }
                NPCTrader trader = null;
                if (message.mode == EnumNPCServerPacketType.ChooseNPC) {
                    this.spawnNPC(p, message.npcType, message.blockPos, message.floatData);
                    return;
                }
                Optional<EntityNPC> npcOptional = EntityNPC.locateNPCServer(p.world, message.npcID, EntityNPC.class);
                if (!npcOptional.isPresent()) {
                    return;
                }
                EntityNPC npc = npcOptional.get();
                NPCTrainer trainer = null;
                if (npc instanceof NPCTrainer) {
                    trainer = (NPCTrainer)npc;
                } else if (npc instanceof NPCTrader) {
                    trader = (NPCTrader)npc;
                }
                switch (message.mode) {
                    case AI: {
                        trainer.setAIMode(message.ai);
                        trainer.initAI();
                        break;
                    }
                    case BattleAI: {
                        trainer.setBattleAIMode(message.battleAI);
                        break;
                    }
                    case BossMode: {
                        trainer.setBossMode(message.bm);
                        break;
                    }
                    case EncounterMode: {
                        trainer.setEncounterMode(message.em);
                        break;
                    }
                    case BattleRules: {
                        message.battleRules.validateRules();
                        trainer.battleRules = message.battleRules;
                        break;
                    }
                    case Model: {
                        if (npc instanceof NPCTrainer) {
                            ((NPCTrainer)npc).setTrainerType(ServerNPCRegistry.trainers.getById(message.modelIndex), p);
                            break;
                        }
                        npc.setBaseTrainer(ServerNPCRegistry.trainers.getById(message.modelIndex));
                        break;
                    }
                    case CustomSteveTexture: {
                        npc.setCustomSteveTexture(message.data);
                        break;
                    }
                    case Name: {
                        if (message.data != null) {
                            npc.setName(message.data);
                        }
                        if (trainer == null) break;
                        trainer.initAI();
                        break;
                    }
                    case CycleJson: {
                        if (!(npc instanceof NPCShopkeeper)) break;
                        ((NPCShopkeeper)npc).cycleJson(p, message.data);
                        break;
                    }
                    case Pokemon: {
                        EntityPixelmon pix = (EntityPixelmon)PixelmonEntityList.createEntityByName(message.pokemon.name, p.world);
                        PlayerStorage storage = trainer.getPokemonStorage();
                        NBTTagCompound[] storageList = storage.getList();
                        storageList[message.pos] = null;
                        storage.addToParty(pix);
                        trainer.updateLvl();
                        Pixelmon.NETWORK.sendTo(new ClearTrainerPokemon(), p);
                        int storageCount = storage.count();
                        for (int i = 0; i < storageCount; ++i) {
                            Pixelmon.NETWORK.sendTo(new StoreTrainerPokemon(new PixelmonData(storageList[i])), p);
                        }
                        Pixelmon.NETWORK.sendTo(new ChangePokemonOpenGUI(message.pos), ctx.getServerHandler().player);
                        break;
                    }
                    case TextureIndex: {
                        npc.setTextureIndex(message.integer);
                        break;
                    }
                    case CycleName: {
                        if (npc instanceof NPCShopkeeper) {
                            ((NPCShopkeeper)npc).cycleName(p, message.integer);
                        }
                    }
                    default: {
                        break;
                    }
                    case ChatPages: {
                        if (!(npc instanceof NPCChatting)) break;
                        ((NPCChatting)npc).setChat(message.pages);
                        break;
                    }
                    case Trader: {
                        if (trader == null) break;
                        trader.updateTrade(message.exchange, message.offer, message.level, message.shiny, message.form);
                        break;
                    }
                    case CycleTexture: {
                        if (!(npc instanceof NPCChatting)) break;
                        ((NPCChatting)npc).cycleTexture(p, message.npcData);
                        break;
                    }
                    case RefreshItems: {
                        if (!(npc instanceof NPCShopkeeper)) break;
                        ((NPCShopkeeper)npc).loadItems();
                    }
                }
            });
            return null;
        }

        private void spawnNPC(EntityPlayerMP player, EnumNPCType npcType, BlockPos blockPos, float rotationYaw) {
            switch (npcType) {
                case Trainer: {
                    NPCTrainer trainer = new NPCTrainer(player.world);
                    trainer.init(NPCRegistryTrainers.Steve);
                    trainer.setPosition((float)blockPos.getX() + 0.5f, blockPos.getY() + 1, (float)blockPos.getZ() + 0.5f);
                    trainer.setAIMode(EnumTrainerAI.StandStill);
                    trainer.ignoreDespawnCounter = true;
                    trainer.initAI();
                    Pixelmon.PROXY.spawnEntitySafely(trainer, player.world);
                    trainer.setPosition((float)blockPos.getX() + 0.5f, blockPos.getY() + 1, (float)blockPos.getZ() + 0.5f);
                    trainer.setStartRotationYaw(rotationYaw + 180.0f);
                    trainer.ignoreDespawnCounter = true;
                    Pixelmon.NETWORK.sendTo(new ClearTrainerPokemon(), player);
                    for (int i = 0; i < trainer.getPokemonStorage().count(); ++i) {
                        Pixelmon.NETWORK.sendTo(new StoreTrainerPokemon(new PixelmonData(trainer.getPokemonStorage().getList()[i])), player);
                    }
                    SetTrainerData trainerData = new SetTrainerData(trainer, player.language);
                    Pixelmon.NETWORK.sendTo(new SetNPCEditData(trainerData), player);
                    player.getServer().addScheduledTask(() -> {
                        if (BattleClauseRegistry.getClauseVersion() > 0) {
                            Pixelmon.NETWORK.sendTo(new UpdateClientRules(), player);
                        }
                        player.openGui(Pixelmon.INSTANCE, EnumGui.TrainerEditor.getIndex(), player.world, trainer.getId(), 0, 0);
                    });
                    break;
                }
                case Trader: {
                    NPCTrader trader = new NPCTrader(player.world);
                    trader.setPosition((float)blockPos.getX() + 0.5f, blockPos.getY() + 1, (float)blockPos.getZ() + 0.5f);
                    Pixelmon.PROXY.spawnEntitySafely(trader, player.world);
                    trader.ignoreDespawnCounter = true;
                    break;
                }
                case ChattingNPC: {
                    NPCChatting npc = new NPCChatting(player.world);
                    GeneralNPCData data = ServerNPCRegistry.villagers.getRandom();
                    npc.init(data);
                    npc.initDefaultAI();
                    npc.setCustomSteveTexture(data.getRandomTexture());
                    npc.setPosition((float)blockPos.getX() + 0.5f, blockPos.getY() + 1, (float)blockPos.getZ() + 0.5f);
                    npc.ignoreDespawnCounter = true;
                    Pixelmon.PROXY.spawnEntitySafely(npc, player.world);
                    break;
                }
                case Relearner: {
                    NPCRelearner relearner = new NPCRelearner(player.world);
                    relearner.setPosition((float)blockPos.getX() + 0.5f, blockPos.getY() + 1, (float)blockPos.getZ() + 0.5f);
                    relearner.setAIMode(EnumTrainerAI.StandStill);
                    relearner.initAI();
                    relearner.ignoreDespawnCounter = true;
                    Pixelmon.PROXY.spawnEntitySafely(relearner, player.world);
                    break;
                }
                case Tutor: {
                    NPCTutor tutor = new NPCTutor(player.world);
                    tutor.init("Tutor");
                    tutor.setPosition((float)blockPos.getX() + 0.5f, blockPos.getY() + 1, (float)blockPos.getZ() + 0.5f);
                    tutor.setAIMode(EnumTrainerAI.StandStill);
                    tutor.initAI();
                    tutor.ignoreDespawnCounter = true;
                    Pixelmon.PROXY.spawnEntitySafely(tutor, player.world);
                    break;
                }
                case Shopkeeper: {
                    ShopkeeperData shopData = ServerNPCRegistry.shopkeepers.getRandom();
                    if (shopData == null) {
                        return;
                    }
                    NPCShopkeeper shopkeeper = new NPCShopkeeper(player.world);
                    shopkeeper.init(shopData);
                    shopkeeper.initDefaultAI();
                    shopkeeper.setPosition((float)blockPos.getX() + 0.5f, blockPos.getY() + 1, (float)blockPos.getZ() + 0.5f);
                    shopkeeper.ignoreDespawnCounter = true;
                    Pixelmon.PROXY.spawnEntitySafely(shopkeeper, player.world);
                    break;
                }
                case NurseJoy: {
                    NPCNurseJoy nursejoy = new NPCNurseJoy(player.world);
                    nursejoy.setPosition((float)blockPos.getX() + 0.5f, blockPos.getY() + 1, (float)blockPos.getZ() + 0.5f);
                    nursejoy.initDefaultAI();
                    nursejoy.ignoreDespawnCounter = true;
                    Pixelmon.PROXY.spawnEntitySafely(nursejoy, player.world);
                    break;
                }
                case Groomer: {
                    NPCGroomer groomer = new NPCGroomer(player.world);
                    groomer.setPosition((float)blockPos.getX() + 0.5f, blockPos.getY() + 1, (float)blockPos.getZ() + 0.5f);
                    groomer.initDefaultAI();
                    Pixelmon.PROXY.spawnEntitySafely(groomer, player.world);
                    groomer.ignoreDespawnCounter = true;
                    break;
                }
                case Ultra: {
                    NPCUltra ultra = new NPCUltra(player.world);
                    ultra.setPosition((float)blockPos.getX() + 0.5f, blockPos.getY() + 1, (float)blockPos.getZ() + 0.5f);
                    ultra.initDefaultAI();
                    Pixelmon.PROXY.spawnEntitySafely(ultra, player.world);
                    ultra.ignoreDespawnCounter = true;
                    break;
                }
                case Damos: {
                    NPCDamos damos = new NPCDamos(player.world);
                    damos.setPosition((float)blockPos.getX() + 0.5f, blockPos.getY() + 1, (float)blockPos.getZ() + 0.5f);
                    damos.initDefaultAI();
                    Pixelmon.PROXY.spawnEntitySafely(damos, player.world);
                    damos.ignoreDespawnCounter = true;
                    break;
                }
                case Sticker: {
                    NPCSticker sticker = new NPCSticker(player.world);
                    sticker.setPosition((float)blockPos.getX() + 0.5f, blockPos.getY() + 1, (float)blockPos.getZ() + 0.5f);
                    sticker.initDefaultAI();
                    Pixelmon.PROXY.spawnEntitySafely(sticker, player.world);
                    sticker.ignoreDespawnCounter = true;
                }
            }
        }
    }
}

