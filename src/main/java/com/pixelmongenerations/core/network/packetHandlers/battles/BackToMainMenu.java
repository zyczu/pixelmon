/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.stream.Collectors;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class BackToMainMenu
implements IMessage {
    boolean canSwitch;
    boolean canFlee;
    ArrayList<int[]> pokemonToChoose;

    public BackToMainMenu() {
    }

    public BackToMainMenu(boolean canSwitch, boolean canFlee, ArrayList<PixelmonWrapper> pokemonToChoose) {
        this.canSwitch = canSwitch;
        this.canFlee = canFlee;
        this.pokemonToChoose = new ArrayList();
        this.pokemonToChoose.addAll(pokemonToChoose.stream().map(PixelmonWrapper::getPokemonID).collect(Collectors.toList()));
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.canSwitch = buffer.readBoolean();
        this.canFlee = buffer.readBoolean();
        int size = buffer.readShort();
        this.pokemonToChoose = new ArrayList();
        for (int i = 0; i < size; ++i) {
            this.pokemonToChoose.add(new int[]{buffer.readInt(), buffer.readInt()});
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeBoolean(this.canSwitch);
        buffer.writeBoolean(this.canFlee);
        buffer.writeShort(this.pokemonToChoose.size());
        for (int[] id : this.pokemonToChoose) {
            buffer.writeInt(id[0]);
            buffer.writeInt(id[1]);
        }
    }

    public static class Handler
    implements IMessageHandler<BackToMainMenu, IMessage> {
        @Override
        public IMessage onMessage(BackToMainMenu message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> ClientProxy.battleManager.startPicking(message.canSwitch, message.canFlee, message.pokemonToChoose));
            return null;
        }
    }
}

