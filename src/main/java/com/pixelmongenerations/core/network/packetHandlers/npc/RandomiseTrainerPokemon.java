/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class RandomiseTrainerPokemon
implements IMessage {
    int trainerId;

    public RandomiseTrainerPokemon() {
    }

    public RandomiseTrainerPokemon(int trainerId) {
        this.trainerId = trainerId;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.trainerId);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.trainerId = buffer.readInt();
    }

    public static class Handler
    implements IMessageHandler<RandomiseTrainerPokemon, IMessage> {
        @Override
        public IMessage onMessage(RandomiseTrainerPokemon message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            if (!ItemNPCEditor.checkPermission(player)) {
                return null;
            }
            player.getServer().addScheduledTask(() -> {
                Optional<NPCTrainer> trainer = EntityNPC.locateNPCServer(player.world, message.trainerId, NPCTrainer.class);
                if (trainer.isPresent()) {
                    trainer.get().randomisePokemon(player);
                }
            });
            return null;
        }
    }
}

