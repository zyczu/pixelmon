/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pcClientStorage;

import com.pixelmongenerations.api.pc.ClientBoxInfo;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.storage.PCClientStorage;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.List;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PCSync
implements IMessage {
    private int lastBox;
    private List<ClientBoxInfo> boxInfo;
    private List<PixelmonData> pokemon;

    public PCSync() {
    }

    public PCSync(int lastBox, List<ClientBoxInfo> boxInfo, List<PixelmonData> pokemon) {
        this.lastBox = lastBox;
        this.boxInfo = boxInfo;
        this.pokemon = pokemon;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.lastBox);
        buffer.writeInt(this.boxInfo.size());
        for (ClientBoxInfo info : this.boxInfo) {
            buffer.writeInt(info.getNumber());
            ByteBufUtils.writeUTF8String(buffer, info.getName());
            ByteBufUtils.writeUTF8String(buffer, info.getBackground());
        }
        buffer.writeInt(this.pokemon.size());
        for (PixelmonData data : this.pokemon) {
            data.encodeInto(buffer);
        }
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.boxInfo = new ArrayList<ClientBoxInfo>();
        this.pokemon = new ArrayList<PixelmonData>();
        this.lastBox = buffer.readInt();
        int boxSize = buffer.readInt();
        for (int i = 0; i < boxSize; ++i) {
            this.boxInfo.add(ClientBoxInfo.of(buffer.readInt(), ByteBufUtils.readUTF8String(buffer), ByteBufUtils.readUTF8String(buffer)));
        }
        int pokemonSize = buffer.readInt();
        for (int i = 0; i < pokemonSize; ++i) {
            PixelmonData data = new PixelmonData();
            data.decodeInto(buffer);
            this.pokemon.add(data);
        }
    }

    public static class Handler
    implements IMessageHandler<PCSync, IMessage> {
        @Override
        public IMessage onMessage(PCSync message, MessageContext ctx) {
            PCClientStorage.clearList();
            PCClientStorage.setLastBoxOpen(message.lastBox);
            for (ClientBoxInfo boxInfo : message.boxInfo) {
                PCClientStorage.setBoxName(boxInfo.getNumber(), boxInfo.getName());
                PCClientStorage.setBoxBackground(boxInfo.getNumber(), boxInfo.getBackground());
            }
            for (PixelmonData pokemon : message.pokemon) {
                PCClientStorage.addToList(pokemon);
            }
            return null;
        }
    }
}

