/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.trading;

import com.pixelmongenerations.client.gui.ClientTradingManager;
import com.pixelmongenerations.core.network.PixelmonStatsData;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetSelectedStats
implements IMessage {
    PixelmonStatsData stats;

    public SetSelectedStats() {
    }

    public SetSelectedStats(PixelmonStatsData stats) {
        this.stats = stats;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        this.stats.writePacketData(buffer);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.stats = new PixelmonStatsData();
        this.stats.readPacketData(buffer);
    }

    public static class Handler
    implements IMessageHandler<SetSelectedStats, IMessage> {
        @Override
        public IMessage onMessage(SetSelectedStats message, MessageContext ctx) {
            ClientTradingManager.selectedStats = message.stats;
            return null;
        }
    }
}

