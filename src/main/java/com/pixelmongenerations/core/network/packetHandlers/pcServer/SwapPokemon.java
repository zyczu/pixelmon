/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pcServer;

import com.pixelmongenerations.core.storage.PCServer;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SwapPokemon
implements IMessage {
    int firstBox;
    int firstPos;
    int secondBox;
    int secondPos;

    public SwapPokemon() {
    }

    public SwapPokemon(int firstBox, int firstPos, int secondBox, int secondPos) {
        this.firstBox = firstBox;
        this.firstPos = firstPos;
        this.secondBox = secondBox;
        this.secondPos = secondPos;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.firstBox);
        buffer.writeInt(this.firstPos);
        buffer.writeInt(this.secondBox);
        buffer.writeInt(this.secondPos);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.firstBox = buffer.readInt();
        this.firstPos = buffer.readInt();
        this.secondBox = buffer.readInt();
        this.secondPos = buffer.readInt();
    }

    public static class Handler
    implements IMessageHandler<SwapPokemon, IMessage> {
        @Override
        public IMessage onMessage(SwapPokemon message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> PCServer.swapPokemon(ctx.getServerHandler().player, message.firstBox, message.firstPos, message.secondBox, message.secondPos));
            return null;
        }
    }
}

