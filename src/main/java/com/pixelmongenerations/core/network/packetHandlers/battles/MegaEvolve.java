/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.client.gui.battles.battleScreens.MegaEvolution;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MegaEvolve
implements IMessage {
    int[] pokemonID;

    public MegaEvolve() {
    }

    public MegaEvolve(int[] pokemonID) {
        this.pokemonID = pokemonID;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        PixelmonMethods.toBytesPokemonID(buf, this.pokemonID);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.pokemonID = PixelmonMethods.fromBytesPokemonID(buf);
    }

    public static class Handler
    implements IMessageHandler<MegaEvolve, IMessage> {
        @Override
        public IMessage onMessage(MegaEvolve message, MessageContext ctx) {
            ClientProxy.battleManager.megaEvolution = message.pokemonID;
            ClientProxy.battleManager.mode = BattleMode.MegaEvolution;
            MegaEvolution.selectEntity();
            return null;
        }
    }
}

