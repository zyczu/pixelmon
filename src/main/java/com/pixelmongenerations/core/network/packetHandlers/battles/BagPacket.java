/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.core.network.ChatHandler;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class BagPacket
implements IMessage {
    public boolean fromPokemon;
    int itemIndex;
    int battleIndex;
    int additionalInfo;
    int[] pokemonId;
    int[] targetPokemonId;

    public BagPacket() {
    }

    public BagPacket(int[] pokemonId, int itemIndex, int battleIndex, int additionalInfo) {
        this.fromPokemon = false;
        this.pokemonId = pokemonId;
        this.itemIndex = itemIndex;
        this.battleIndex = battleIndex;
        this.additionalInfo = additionalInfo;
    }

    public BagPacket(int[] pokemonId, int[] targetPokemonId, int itemIndex, int battleIndex) {
        this.fromPokemon = true;
        this.pokemonId = pokemonId;
        this.targetPokemonId = targetPokemonId;
        this.itemIndex = itemIndex;
        this.battleIndex = battleIndex;
    }

    private void bagPacketFromPokemon(EntityPlayer player) {
        ItemStack usedStack = null;
        for (ItemStack i : player.inventory.mainInventory) {
            if (i == null || Item.getIdFromItem(i.getItem()) != this.itemIndex) continue;
            usedStack = i;
            break;
        }
        if (usedStack == null) {
            ChatHandler.sendChat(player, "bagpacket.itemnotfound", new Object[0]);
            return;
        }
        BattleControllerBase bc = BattleRegistry.getBattle(this.battleIndex);
        if (bc == null) {
            ChatHandler.sendChat(player, "bagpacket.battlenotfound", new Object[0]);
            return;
        }
        bc.setUseItem(this.pokemonId, player, usedStack, this.targetPokemonId);
    }

    private void bagPacket(EntityPlayer player) {
        ItemStack usedStack = null;
        for (ItemStack i : player.inventory.mainInventory) {
            if (i == null || Item.getIdFromItem(i.getItem()) != this.itemIndex) continue;
            usedStack = i;
            break;
        }
        if (usedStack == null) {
            ChatHandler.sendChat(player, "bagpacket.itemnotfound", new Object[0]);
            return;
        }
        BattleControllerBase bc = BattleRegistry.getBattle(this.battleIndex);
        if (bc == null) {
            ChatHandler.sendChat(player, "bagpacket.battlenotfound", new Object[0]);
            return;
        }
        bc.setUseItem(this.pokemonId, player, usedStack, this.additionalInfo);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.fromPokemon = buffer.readBoolean();
        this.pokemonId = new int[]{buffer.readInt(), buffer.readInt()};
        this.itemIndex = buffer.readInt();
        this.battleIndex = buffer.readInt();
        if (!this.fromPokemon) {
            this.additionalInfo = buffer.readInt();
        } else {
            this.targetPokemonId = new int[]{buffer.readInt(), buffer.readInt()};
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeBoolean(this.fromPokemon);
        buffer.writeInt(this.pokemonId[0]);
        buffer.writeInt(this.pokemonId[1]);
        buffer.writeInt(this.itemIndex);
        buffer.writeInt(this.battleIndex);
        if (!this.fromPokemon) {
            buffer.writeInt(this.additionalInfo);
        } else {
            buffer.writeInt(this.targetPokemonId[0]);
            buffer.writeInt(this.targetPokemonId[1]);
        }
    }

    public static class Handler
    implements IMessageHandler<BagPacket, IMessage> {
        @Override
        public IMessage onMessage(BagPacket message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> this.processMessage(message, ctx));
            return null;
        }

        private void processMessage(BagPacket message, MessageContext ctx) {
            if (!message.fromPokemon) {
                message.bagPacket(ctx.getServerHandler().player);
            } else {
                message.bagPacketFromPokemon(ctx.getServerHandler().player);
            }
        }
    }
}

