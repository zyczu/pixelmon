/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCRelearner;
import com.pixelmongenerations.common.entity.npcs.NPCTutor;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.packetHandlers.OpenReplaceMoveScreen;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.MoveCostList;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class LearnMove
implements IMessage {
    int[] pokemonId;
    int attackId;
    boolean checkAble;
    int npcId;

    public LearnMove() {
    }

    public LearnMove(int[] pokemonId, int attackId, boolean checkAble, int npcId) {
        this.pokemonId = pokemonId;
        this.attackId = attackId;
        this.checkAble = checkAble;
        this.npcId = npcId;
    }

    public LearnMove(int[] pokemonId, int attackId, int npcId) {
        this(pokemonId, attackId, false, npcId);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.pokemonId = new int[]{buffer.readInt(), buffer.readInt()};
        this.attackId = buffer.readInt();
        this.checkAble = buffer.readBoolean();
        this.npcId = buffer.readInt();
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pokemonId[0]);
        buffer.writeInt(this.pokemonId[1]);
        buffer.writeInt(this.attackId);
        buffer.writeBoolean(this.checkAble);
        buffer.writeInt(this.npcId);
    }

    public static class Handler
    implements IMessageHandler<LearnMove, IMessage> {
        @Override
        public IMessage onMessage(LearnMove message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            player.getServer().addScheduledTask(() -> {
                Optional<EntityPixelmon> entityPixelmonOptional;
                EntityPixelmon p;
                Attack a = DatabaseMoves.getAttack(message.attackId);
                Optional<EntityNPC> npc = EntityNPC.locateNPCServer(player.world, message.npcId, EntityNPC.class);
                ArrayList<ItemStack> costs = new ArrayList<ItemStack>();
                if (!npc.isPresent()) {
                    return;
                }
                if (npc.get() instanceof NPCRelearner) {
                    ItemStack cost = ((NPCRelearner)npc.get()).getCost();
                    if (!cost.isEmpty()) {
                        costs.add(cost);
                    }
                } else {
                    if (!(npc.get() instanceof NPCTutor)) {
                        return;
                    }
                    NPCTutor tutor = (NPCTutor)npc.get();
                    for (int i = 0; i < tutor.attackList.size(); ++i) {
                        ArrayList<ItemStack> array;
                        if (!a.equals(tutor.attackList.get(i)) || (array = tutor.costs.get(i)).size() < 1) continue;
                        costs = array;
                    }
                }
                Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
                if (!optstorage.isPresent()) {
                    return;
                }
                PlayerStorage storage = optstorage.get();
                if (!costs.isEmpty() && !player.capabilities.isCreativeMode) {
                    MoveCostList.addToList(player, costs);
                }
                EntityPixelmon entityPixelmon = p = (entityPixelmonOptional = storage.getAlreadyExists(message.pokemonId, player.world)).isPresent() ? entityPixelmonOptional.get() : storage.sendOut(message.pokemonId, player.world);
                if (message.checkAble && !DatabaseMoves.CanLearnAttack(DatabaseMoves.getPokemonID(p), a.getAttackBase().getUnlocalizedName())) {
                    return;
                }
                if (p == null || p.getMoveset() == null) {
                    return;
                }
                if (p.getMoveset().size() >= 4) {
                    EntityPlayerMP targetPlayer = (EntityPlayerMP)p.getOwner();
                    Pixelmon.NETWORK.sendTo(new OpenReplaceMoveScreen(targetPlayer.getUniqueID(), p.getPokemonId(), a.getAttackBase().attackIndex, 0, p.getLvl().getLevel()), player);
                    return;
                }
                if (MoveCostList.checkEntry(player)) {
                    p.getMoveset().add(a);
                    p.update(EnumUpdateType.Moveset);
                    ChatHandler.sendChat(p.getOwner(), "pixelmon.stats.learnedmove", p.getNickname(), a.getAttackBase().getLocalizedName());
                    return;
                }
                if (!message.checkAble) {
                    return;
                }
                ChatHandler.sendChat(p.getOwner(), "pixelmon.npc.cantpay", new Object[0]);
            });
            return null;
        }
    }
}

