/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.api.events.NicknameEvent;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class RenamePokemon
implements IMessage {
    int[] id;
    String name;

    public RenamePokemon() {
    }

    public RenamePokemon(int[] pokemonID, String name) {
        this.id = pokemonID;
        this.name = name;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.id = new int[]{buffer.readInt(), buffer.readInt()};
        this.name = ByteBufUtils.readUTF8String(buffer);
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.id[0]);
        buffer.writeInt(this.id[1]);
        ByteBufUtils.writeUTF8String(buffer, this.name);
    }

    public static class Handler
    implements IMessageHandler<RenamePokemon, IMessage> {
        @Override
        public IMessage onMessage(RenamePokemon message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            player.getServer().addScheduledTask(() -> this.processMessage(message, player));
            return null;
        }

        private void processMessage(RenamePokemon message, EntityPlayerMP player) {
            PlayerStorage storage;
            Optional<EntityPixelmon> entityPixelmonOptional;
            NicknameEvent event;
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
            if (optstorage.isPresent() && !(event = new NicknameEvent(player, entityPixelmonOptional = (storage = optstorage.get()).getAlreadyExists(message.id, player.world), message.name)).isCanceled()) {
                PlayerComputerStorage comp;
                NBTTagCompound nbt;
                if (entityPixelmonOptional.isPresent()) {
                    entityPixelmonOptional.get().setNickname(event.getNickname());
                } else if (storage.getNBT(message.id) != null) {
                    NBTTagCompound nbt2 = storage.getNBT(message.id);
                    if (nbt2 != null) {
                        nbt2.setString("Nickname", event.getNickname());
                    }
                } else if (PixelmonStorage.computerManager.getPlayerStorage(player).contains(message.id) && (nbt = (comp = PixelmonStorage.computerManager.getPlayerStorage(player)).getPokemonNBT(message.id)) != null) {
                    nbt.setString("Nickname", event.getNickname());
                    comp.updatePokemonNBT(message.id, nbt);
                }
                PixelmonStorage.save(player);
            }
        }
    }
}

