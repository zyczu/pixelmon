/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.gui;

import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.common.battle.attacks.IBattleMessage;
import com.pixelmongenerations.common.battle.attacks.MessageType;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class StatusPacket
extends IBattleMessage
implements IMessage {
    private int status = -1;

    public StatusPacket() {
    }

    public StatusPacket(int[] id, int status) {
        this.pokemonID = id;
        this.status = status;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.pokemonID = new int[]{buf.readInt(), buf.readInt()};
        this.status = buf.readInt();
        this.messageType = MessageType.STATUS;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        for (int i : this.pokemonID) {
            buf.writeInt(i);
        }
        buf.writeInt(this.status);
    }

    @Override
    public void process() {
        PixelmonInGui pokemon = ClientProxy.battleManager.getPokemon(this.pokemonID);
        if (pokemon != null) {
            pokemon.status = this.status;
        }
        ClientProxy.battleManager.removeBattleMessage(this);
    }

    public static class Handler
    implements IMessageHandler<StatusPacket, IMessage> {
        @Override
        public IMessage onMessage(StatusPacket message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> ClientProxy.battleManager.addBattleMessage(message));
            return null;
        }
    }
}

