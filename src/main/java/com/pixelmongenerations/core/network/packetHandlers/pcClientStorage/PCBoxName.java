/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pcClientStorage;

import com.pixelmongenerations.core.storage.PCClientStorage;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PCBoxName
implements IMessage {
    private int box;
    private String name;

    public PCBoxName() {
    }

    public PCBoxName(int box, String name) {
        this.box = box;
        this.name = name;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.box = buf.readInt();
        this.name = ByteBufUtils.readUTF8String(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.box);
        ByteBufUtils.writeUTF8String(buf, this.name);
    }

    public static class Handler
    implements IMessageHandler<PCBoxName, IMessage> {
        @Override
        public IMessage onMessage(PCBoxName message, MessageContext ctx) {
            PCClientStorage.setBoxName(message.box, message.name);
            return null;
        }
    }
}

