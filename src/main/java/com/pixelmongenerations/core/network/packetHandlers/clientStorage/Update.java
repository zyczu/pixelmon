/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.clientStorage;

import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.core.network.PixelmonUpdateData;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class Update
implements IMessage {
    PixelmonUpdateData data;

    public Update() {
    }

    public Update(PixelmonUpdateData data) {
        this.data = data;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        this.data.encodeInto(buffer);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.data = new PixelmonUpdateData();
        this.data.decodeInto(buffer);
    }

    public static class Handler
    implements IMessageHandler<Update, IMessage> {
        @Override
        public IMessage onMessage(Update message, MessageContext ctx) {
            ServerStorageDisplay.update(message.data);
            return null;
        }
    }
}

