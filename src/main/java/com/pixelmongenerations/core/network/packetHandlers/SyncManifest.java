/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.data.asset.IAssetManifest;
import com.pixelmongenerations.core.util.SyncUtil;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SyncManifest
implements IMessage {
    public String manifestJson;
    public AssetManifestType type;

    public SyncManifest() {
    }

    public SyncManifest(IAssetManifest manifest) {
        this.manifestJson = manifest.toJson();
        this.type = manifest.getType();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.type.ordinal());
        SyncUtil.writeLargeString(this.manifestJson, buf);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.type = AssetManifestType.values()[buf.readInt()];
        this.manifestJson = SyncUtil.readLargeString(buf);
    }

    public static enum AssetManifestType {
        TEXTURE,
        VALVE_MODEL,
        MINECRAFT_MODEL,
        SOUND,
        UNKNOWN;

    }

    public static class Handler
    implements IMessageHandler<SyncManifest, IMessage> {
        @Override
        public IMessage onMessage(SyncManifest message, MessageContext ctx) {
            Pixelmon.LOGGER.debug("Received sync manifest request for " + message.manifestJson);
            Pixelmon.PROXY.startManifestDownload(message.type, message.manifestJson);
            return null;
        }
    }
}

