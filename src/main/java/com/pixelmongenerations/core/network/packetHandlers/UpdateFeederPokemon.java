/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.client.gui.feeder.GuiFeeder;
import com.pixelmongenerations.core.network.PixelmonData;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UpdateFeederPokemon
implements IMessage {
    private PixelmonData data;
    private boolean noPokemon;

    public UpdateFeederPokemon() {
    }

    public UpdateFeederPokemon(PixelmonData data) {
        this.data = data;
        this.noPokemon = data == null;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.noPokemon = buf.readBoolean();
        if (!this.noPokemon) {
            this.data = new PixelmonData();
            this.data.decodeInto(buf);
        } else {
            this.data = null;
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeBoolean(this.noPokemon);
        if (!this.noPokemon) {
            this.data.encodeInto(buf);
        }
    }

    public static class Handler
    implements IMessageHandler<UpdateFeederPokemon, IMessage> {
        @Override
        public IMessage onMessage(UpdateFeederPokemon message, MessageContext ctx) {
            GuiFeeder.pokemon = message.data == null ? null : PokemonSpec.from(message.data.getSpecies()).create(Minecraft.getMinecraft().player.world);
            return null;
        }
    }
}

