/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.gui;

import com.pixelmongenerations.client.gui.battles.timerTasks.DamageTask;
import com.pixelmongenerations.client.gui.battles.timerTasks.HPTask;
import com.pixelmongenerations.client.gui.battles.timerTasks.HealTask;
import com.pixelmongenerations.common.battle.attacks.IBattleMessage;
import com.pixelmongenerations.common.battle.attacks.MessageType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import java.util.Timer;
import java.util.TimerTask;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class HPPacket
extends IBattleMessage
implements IMessage {
    public int healthDifference;

    public HPPacket() {
    }

    public HPPacket(PixelmonWrapper pokemon, int healthDifference) {
        this.pokemonID = pokemon.getPokemonID();
        this.healthDifference = healthDifference;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.pokemonID = new int[]{buf.readInt(), buf.readInt()};
        this.healthDifference = buf.readInt();
        this.messageType = MessageType.HP;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        for (int i : this.pokemonID) {
            buf.writeInt(i);
        }
        buf.writeInt(this.healthDifference);
    }

    @Override
    public void process() {
        HPTask task = this.healthDifference <= 0 ? new DamageTask(this, this.healthDifference, this.pokemonID) : new HealTask(this, this.healthDifference, this.pokemonID);
        new Timer().scheduleAtFixedRate((TimerTask)task, 0L, 10L);
    }

    public static class Handler
    implements IMessageHandler<HPPacket, IMessage> {
        @Override
        public IMessage onMessage(HPPacket message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> ClientProxy.battleManager.addBattleMessage(message));
            return null;
        }
    }
}

