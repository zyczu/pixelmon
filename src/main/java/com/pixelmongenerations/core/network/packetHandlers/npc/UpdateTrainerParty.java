/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.common.item.ItemNPCEditor;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.npc.DeleteTrainerPokemon;
import com.pixelmongenerations.core.network.packetHandlers.npc.UpdateTrainerPokemon;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.UpdateEditedParty;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.UpdateEditedPokemon;
import io.netty.buffer.ByteBuf;
import java.util.List;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UpdateTrainerParty
extends UpdateEditedParty {
    int trainerID;

    public UpdateTrainerParty() {
    }

    public UpdateTrainerParty(List<PixelmonData> party) {
        super(party);
    }

    @Override
    protected UpdateEditedPokemon createPokemonPacket(PixelmonData data) {
        return new UpdateTrainerPokemon(data);
    }

    @Override
    protected UpdateEditedPokemon readPokemonData(ByteBuf buf) {
        UpdateTrainerPokemon data = new UpdateTrainerPokemon();
        data.fromBytes(buf);
        this.trainerID = data.trainerID;
        return data;
    }

    public static class Handler
    implements IMessageHandler<UpdateTrainerParty, IMessage> {
        @Override
        public IMessage onMessage(UpdateTrainerParty message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            if (!ItemNPCEditor.checkPermission(player)) {
                return null;
            }
            UpdateTrainerPokemon.Handler handler = new UpdateTrainerPokemon.Handler();
            int numPokemon = message.party.size();
            for (int i = 0; i < 6; ++i) {
                UpdateTrainerPokemon data = null;
                if (i < numPokemon) {
                    data = (UpdateTrainerPokemon)message.party.get(i);
                }
                if (data == null) {
                    DeleteTrainerPokemon.deletePokemon(message.trainerID, numPokemon, ctx, i == 5);
                    continue;
                }
                handler.onMessage(data, ctx);
            }
            return null;
        }
    }
}

