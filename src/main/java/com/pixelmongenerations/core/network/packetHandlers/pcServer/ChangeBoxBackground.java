/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pcServer;

import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.storage.PCServer;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ChangeBoxBackground
implements IMessage {
    private int box;
    private String background;

    public ChangeBoxBackground() {
    }

    public ChangeBoxBackground(int box, String background) {
        this.box = box;
        this.background = background;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.box);
        ByteBufUtils.writeUTF8String(buffer, this.background);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.box = buffer.readInt();
        this.background = ByteBufUtils.readUTF8String(buffer);
    }

    public static class Handler
    implements IMessageHandler<ChangeBoxBackground, IMessage> {
        @Override
        public IMessage onMessage(ChangeBoxBackground message, MessageContext ctx) {
            if (message.box < -1 || message.box >= PixelmonConfig.computerBoxes) {
                return null;
            }
            if (message.background.length() > 40) {
                return null;
            }
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> PCServer.setBoxBackground(ctx.getServerHandler().player, message.box, message.background));
            return null;
        }
    }
}

