/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.mail;

import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.item.heldItems.ItemMail;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class MailPacket
implements IMessage {
    private Boolean sealed;
    private String author;
    private String contents;

    public MailPacket() {
    }

    public MailPacket(Boolean sealed, String author, String contents, EntityPlayer player) {
        this.sealed = sealed;
        this.author = author;
        this.contents = contents;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.sealed = buf.readBoolean();
        this.author = ByteBufUtils.readUTF8String(buf);
        this.contents = ByteBufUtils.readUTF8String(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeBoolean(this.sealed.booleanValue());
        ByteBufUtils.writeUTF8String(buf, this.author);
        ByteBufUtils.writeUTF8String(buf, this.contents);
    }

    public static class Handler
    implements IMessageHandler<MailPacket, IMessage> {
        @Override
        public IMessage onMessage(MailPacket message, MessageContext ctx) {
            if (ctx.side == Side.SERVER) {
                EntityPlayerMP player = ctx.getServerHandler().player;
                ItemStack letter = player.getHeldItemMainhand();
                if (letter.getItem() instanceof ItemMail) {
                    NBTTagCompound nbtData = new NBTTagCompound();
                    nbtData.setBoolean("editable", message.sealed == false);
                    nbtData.setString("author", message.author);
                    nbtData.setString("contents", message.contents);
                    if (message.sealed.booleanValue()) {
                        letter.setItemDamage(1);
                    }
                    ItemStack newLetter = letter.copy();
                    newLetter.setTagCompound(nbtData);
                    newLetter.setCount(1);
                    letter.shrink(1);
                    if (!player.inventory.addItemStackToInventory(newLetter)) {
                        DropItemHelper.dropItemOnGround(player, newLetter, true, false);
                    }
                }
                player.closeScreen();
            }
            return null;
        }
    }
}

