/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.core.Pixelmon;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class TransferPacket
implements IMessage {
    private String name;
    private String ip;

    public TransferPacket() {
    }

    public TransferPacket(String name, String ip) {
        this.name = name;
        this.ip = ip;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.name = ByteBufUtils.readUTF8String(buf);
        this.ip = ByteBufUtils.readUTF8String(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.name);
        ByteBufUtils.writeUTF8String(buf, this.ip);
    }

    public static class Handler
    implements IMessageHandler<TransferPacket, IMessage> {
        @Override
        public IMessage onMessage(TransferPacket message, MessageContext ctx) {
            Pixelmon.PROXY.sendToServer(message.name, message.ip);
            return null;
        }
    }
}

