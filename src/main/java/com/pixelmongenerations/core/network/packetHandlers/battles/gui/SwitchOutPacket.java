/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.gui;

import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.client.gui.battles.timerTasks.SwitchTask;
import com.pixelmongenerations.common.battle.attacks.IBattleMessage;
import com.pixelmongenerations.common.battle.attacks.MessageType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import java.util.Timer;
import java.util.TimerTask;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SwitchOutPacket
extends IBattleMessage
implements IMessage {
    PixelmonInGui newPokemonGUI;

    public SwitchOutPacket() {
    }

    public SwitchOutPacket(int[] pix1ID, PixelmonWrapper newPokemon) {
        this.pokemonID = pix1ID;
        this.newPokemonGUI = new PixelmonInGui(newPokemon);
    }

    public SwitchOutPacket(int[] pix1ID) {
        this.pokemonID = pix1ID;
        this.newPokemonGUI = new PixelmonInGui();
        this.newPokemonGUI.pokemonID = new int[]{-1, -1};
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.pokemonID = new int[]{buf.readInt(), buf.readInt()};
        this.newPokemonGUI = new PixelmonInGui();
        this.newPokemonGUI.decodeInto(buf);
        this.messageType = MessageType.SWITCHOUT;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        for (int i : this.pokemonID) {
            buf.writeInt(i);
        }
        this.newPokemonGUI.encodeInto(buf);
    }

    @Override
    public void process() {
        new Timer().scheduleAtFixedRate((TimerTask)new SwitchTask(this, this.pokemonID, this.newPokemonGUI), 0L, 5L);
    }

    public static class Handler
    implements IMessageHandler<SwitchOutPacket, IMessage> {
        @Override
        public IMessage onMessage(SwitchOutPacket message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> ClientProxy.battleManager.addBattleMessage(message));
            return null;
        }
    }
}

