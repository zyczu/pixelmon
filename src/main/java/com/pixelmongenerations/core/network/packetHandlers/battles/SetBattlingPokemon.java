/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetBattlingPokemon
implements IMessage {
    int[][] pokemon;

    public SetBattlingPokemon() {
    }

    public SetBattlingPokemon(ArrayList<PixelmonWrapper> arrayList) {
        int numPokemon = arrayList.size();
        this.pokemon = new int[numPokemon][2];
        for (int i = 0; i < numPokemon; ++i) {
            PixelmonWrapper pw = arrayList.get(i);
            if (pw == null) continue;
            this.pokemon[i] = pw.getPokemonID();
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeShort(this.pokemon.length);
        for (int[] id : this.pokemon) {
            buffer.writeInt(id[0]);
            buffer.writeInt(id[1]);
        }
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        int size = buffer.readShort();
        this.pokemon = new int[size][2];
        for (int i = 0; i < size; ++i) {
            this.pokemon[i] = new int[]{buffer.readInt(), buffer.readInt()};
        }
    }

    public static class Handler
    implements IMessageHandler<SetBattlingPokemon, IMessage> {
        @Override
        public IMessage onMessage(SetBattlingPokemon message, MessageContext ctx) {
            ClientProxy.battleManager.setTeamPokemon(message.pokemon);
            return null;
        }
    }
}

