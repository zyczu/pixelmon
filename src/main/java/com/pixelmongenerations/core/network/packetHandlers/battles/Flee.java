/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class Flee
implements IMessage {
    int[] fleeingPokemonID;

    public Flee() {
    }

    public Flee(int[] fleeingPokemonID) {
        this.fleeingPokemonID = fleeingPokemonID;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.fleeingPokemonID = new int[]{buffer.readInt(), buffer.readInt()};
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.fleeingPokemonID[0]);
        buffer.writeInt(this.fleeingPokemonID[1]);
    }

    public static class Handler
    implements IMessageHandler<Flee, IMessage> {
        @Override
        public IMessage onMessage(Flee message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> {
                EntityPlayerMP player = ctx.getServerHandler().player;
                BattleControllerBase bc = BattleRegistry.getBattle(player);
                bc.participants.stream().filter(p -> p instanceof PlayerParticipant).filter(p -> ((PlayerParticipant)p).player == player).forEach(p -> bc.setFlee(message.fleeingPokemonID));
            });
            return null;
        }
    }
}

