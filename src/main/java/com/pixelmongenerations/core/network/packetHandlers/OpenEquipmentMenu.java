/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.dialogue.Choice;
import com.pixelmongenerations.api.dialogue.Dialogue;
import com.pixelmongenerations.api.events.EquipmentMenuOpenEvent;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumDynamaxItem;
import com.pixelmongenerations.core.enums.EnumMegaItem;
import com.pixelmongenerations.core.enums.EnumShinyItem;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class OpenEquipmentMenu
implements IMessage {
    @Override
    public void fromBytes(ByteBuf buf) {
    }

    @Override
    public void toBytes(ByteBuf buf) {
    }

    public static class Handler
    implements IMessageHandler<OpenEquipmentMenu, IMessage> {
        @Override
        public IMessage onMessage(OpenEquipmentMenu message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            try {
                Dialogue.DialogueBuilder dialogue = Dialogue.builder();
                dialogue.setName("Equipment");
                dialogue.column(1);
                dialogue.setText("What item would you like to equip or unequip?");
                PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage(player).orElse(null);
                if (storage != null) {
                    EquipmentMenuOpenEvent event2;
                    if (storage.megaData.canEquipMegaItem()) {
                        dialogue.addChoice(new Choice(storage.megaData.getMegaItem() == EnumMegaItem.BraceletORAS ? "Unequip Mega Bracelet" : "Equip Mega Bracelet", event -> {
                            PlayerStorage playerStorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player).orElse(null);
                            if (playerStorage.dynamaxData.getDynamaxItem() == EnumDynamaxItem.DynamaxBand) {
                                playerStorage.dynamaxData.setDynamaxItem(EnumDynamaxItem.None, false);
                            }
                            boolean equip = playerStorage.megaData.getMegaItem() != EnumMegaItem.BraceletORAS;
                            playerStorage.megaData.setMegaItem(equip ? EnumMegaItem.BraceletORAS : EnumMegaItem.None, false);
                            ChatHandler.sendChat(player, ChatHandler.getMessage("You " + (equip ? "equipped" : "unequipped") + " your Mega Bracelet.", new Object[0]));
                        }));
                    }
                    if (storage.shinyData.canEquipShinyCharm()) {
                        dialogue.addChoice(new Choice(storage.shinyData.getShinyItem() == EnumShinyItem.ShinyCharm ? "Unequip Shiny Charm" : "Equip Shiny Charm", event -> {
                            PlayerStorage playerStorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player).orElse(null);
                            boolean equip = playerStorage.shinyData.getShinyItem() != EnumShinyItem.ShinyCharm;
                            playerStorage.shinyData.setShinyItem(equip ? EnumShinyItem.ShinyCharm : EnumShinyItem.None, false);
                            ChatHandler.sendChat(player, ChatHandler.getMessage("You " + (equip ? "equipped" : "unequipped") + " your Shiny Charm.", new Object[0]));
                        }));
                    }
                    if (storage.dynamaxData.canEquipDynamaxItem()) {
                        dialogue.addChoice(new Choice(storage.dynamaxData.getDynamaxItem() == EnumDynamaxItem.DynamaxBand ? "Unequip Dynamax Band" : "Equip Dynamax Band", event -> {
                            PlayerStorage playerStorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player).orElse(null);
                            if (playerStorage.megaData.getMegaItem() == EnumMegaItem.BraceletORAS) {
                                playerStorage.megaData.setMegaItem(EnumMegaItem.None, false);
                            }
                            boolean equip = storage.dynamaxData.getDynamaxItem() != EnumDynamaxItem.DynamaxBand;
                            storage.dynamaxData.setDynamaxItem(equip ? EnumDynamaxItem.DynamaxBand : EnumDynamaxItem.None, false);
                            ChatHandler.sendChat(player, ChatHandler.getMessage("You " + (equip ? "equipped" : "unequipped") + " your Dynamax Band.", new Object[0]));
                        }));
                    }
                    if (MinecraftForge.EVENT_BUS.post(event2 = new EquipmentMenuOpenEvent(dialogue, player))) {
                        return null;
                    }
                    Dialogue.setPlayerDialogueData(player, Lists.newArrayList(event2.getDialogueBuilder().build()), true);
                } else {
                    Pixelmon.LOGGER.error("Failed to get player storage in OpenEquipmentMenu for " + player.getName());
                }
            }
            catch (Exception e) {
                Pixelmon.LOGGER.error("Failed to open equipment menu for " + player.getName());
                e.printStackTrace();
            }
            return null;
        }
    }
}

