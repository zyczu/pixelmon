/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class EnforcedSwitch
implements IMessage {
    public int switchPosition = -1;

    public EnforcedSwitch() {
    }

    public EnforcedSwitch(int switchPosition) {
        this.switchPosition = switchPosition;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.switchPosition);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.switchPosition = buffer.readInt();
    }

    public void processMessage() {
        ClientProxy.battleManager.addMessage(I18n.translateToLocal("battlecontroller.switch"));
        ClientProxy.battleManager.mode = BattleMode.EnforcedSwitch;
        ClientProxy.battleManager.oldMode = BattleMode.MainMenu;
        ClientProxy.battleManager.currentPokemon = this.switchPosition;
    }

    public static class Handler
    implements IMessageHandler<EnforcedSwitch, IMessage> {
        @Override
        public IMessage onMessage(EnforcedSwitch message, MessageContext ctx) {
            if (ClientProxy.battleManager.mode == BattleMode.EnforcedSwitch) {
                ClientProxy.battleManager.enforcedSwitches.add(message);
            } else {
                message.processMessage();
            }
            return null;
        }
    }
}

