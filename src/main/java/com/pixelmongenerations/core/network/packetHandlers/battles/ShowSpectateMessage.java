/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.client.gui.overlay.SpectateOverlay;
import io.netty.buffer.ByteBuf;
import java.util.UUID;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ShowSpectateMessage
implements IMessage {
    UUID uuid;

    public ShowSpectateMessage() {
    }

    public ShowSpectateMessage(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.uuid = new UUID(buffer.readLong(), buffer.readLong());
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeLong(this.uuid.getMostSignificantBits());
        buffer.writeLong(this.uuid.getLeastSignificantBits());
    }

    public static class Handler
    implements IMessageHandler<ShowSpectateMessage, IMessage> {
        @Override
        public IMessage onMessage(ShowSpectateMessage message, MessageContext ctx) {
            ((SpectateOverlay)GuiPixelmonOverlay.getOverlay(OverlayType.SPECTATE)).showSpectateMessage(message.uuid);
            return null;
        }
    }
}

