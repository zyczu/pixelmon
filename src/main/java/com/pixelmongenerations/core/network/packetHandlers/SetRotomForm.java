/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.google.common.collect.Lists;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.packetHandlers.OpenReplaceMoveScreen;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetRotomForm
implements IMessage {
    int form;

    public SetRotomForm() {
    }

    public SetRotomForm(int form) {
        this.form = form;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.form = buffer.readInt();
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.form);
    }

    public static class Handler
    implements IMessageHandler<SetRotomForm, IMessage> {
        @Override
        public IMessage onMessage(SetRotomForm message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
            if (storageOpt.isPresent()) {
                PlayerStorage storage = storageOpt.get();
                for (int i = 0; i < 6; ++i) {
                    System.out.println("TESTING");
                    NBTTagCompound compound = storage.getTeam().get(i);
                    if (!compound.getString("Name").equalsIgnoreCase("rotom")) continue;
                    compound.setInteger("Variant", message.form);
                    EntityPixelmon pixelmon = storage.sendOutFromPosition(i, storage.getPlayer().world);
                    ArrayList rotomMoves = Lists.newArrayList((Object[])new Attack[]{DatabaseMoves.getAttack("Thunder Shock"), DatabaseMoves.getAttack("Overheat"), DatabaseMoves.getAttack("Hydro Pump"), DatabaseMoves.getAttack("Blizzard"), DatabaseMoves.getAttack("Air Slash"), DatabaseMoves.getAttack("Leaf Storm")});
                    Attack a = null;
                    switch (message.form) {
                        case 0: {
                            a = (Attack)rotomMoves.get(0);
                            break;
                        }
                        case 1: {
                            a = (Attack)rotomMoves.get(1);
                            break;
                        }
                        case 2: {
                            a = (Attack)rotomMoves.get(2);
                            break;
                        }
                        case 3: {
                            a = (Attack)rotomMoves.get(3);
                            break;
                        }
                        case 4: {
                            a = (Attack)rotomMoves.get(4);
                            break;
                        }
                        case 5: {
                            a = (Attack)rotomMoves.get(5);
                        }
                    }
                    pixelmon.getMoveset().removeIf(rotomMoves::contains);
                    if (pixelmon.getMoveset().size() == 4) {
                        Pixelmon.NETWORK.sendTo(new OpenReplaceMoveScreen(player.getUniqueID(), pixelmon.getPokemonId(), a.getAttackBase().attackIndex, 0, pixelmon.getLvl().getLevel()), player);
                    } else {
                        pixelmon.getMoveset().add(a);
                    }
                    pixelmon.update(EnumUpdateType.Moveset);
                    storage.updateClient(compound, EnumUpdateType.Stats);
                    break;
                }
            }
            return null;
        }
    }
}

