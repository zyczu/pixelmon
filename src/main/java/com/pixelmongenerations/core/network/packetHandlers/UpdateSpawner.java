/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.common.block.spawning.BlockPixelmonSpawner;
import com.pixelmongenerations.core.network.PixelmonSpawnerData;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UpdateSpawner
implements IMessage {
    PixelmonSpawnerData data;

    public UpdateSpawner() {
    }

    public UpdateSpawner(PixelmonSpawnerData data) {
        this.data = data;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.data = new PixelmonSpawnerData();
        this.data.readPacketData(buffer);
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        this.data.writePacketData(buffer);
    }

    public static class Handler
    implements IMessageHandler<UpdateSpawner, IMessage> {
        @Override
        public IMessage onMessage(UpdateSpawner message, MessageContext ctx) {
            if (BlockPixelmonSpawner.checkPermission(ctx.getServerHandler().player)) {
                message.data.updateTileEntity(ctx.getServerHandler().player.world);
            }
            return null;
        }
    }
}

