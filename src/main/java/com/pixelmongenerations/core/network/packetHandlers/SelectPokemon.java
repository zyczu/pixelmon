/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.api.enums.ReceiveType;
import com.pixelmongenerations.api.events.PixelmonReceivedEvent;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.starter.CustomStarters;
import com.pixelmongenerations.common.starter.SelectPokemonController;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SelectPokemon
implements IMessage {
    int starterIndex;

    public SelectPokemon() {
    }

    public SelectPokemon(int starterIndex) {
        this.starterIndex = starterIndex;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.starterIndex = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.starterIndex);
    }

    public static class Handler
    implements IMessageHandler<SelectPokemon, IMessage> {
        @Override
        public IMessage onMessage(SelectPokemon message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
            if (optstorage.isPresent()) {
                PlayerStorage storage = optstorage.get();
                if (!SelectPokemonController.isOnList(player)) {
                    return null;
                }
                EntityPixelmon p = (EntityPixelmon)PixelmonEntityList.createEntityByName(SelectPokemonController.getPokemonList((EntityPlayerMP)player)[message.starterIndex].name, player.world);
                SelectPokemonController.removePlayer(player);
                p.getLvl().setLevel(CustomStarters.starterLevel);
                p.setHealth(p.stats.HP);
                p.loadMoveset();
                p.caughtBall = EnumPokeball.PokeBall;
                p.friendship.initFromCapture();
                MinecraftForge.EVENT_BUS.post(new PixelmonReceivedEvent(player, ReceiveType.SelectPokemon, p));
                storage.addToParty(p);
                storage.starterPicked = true;
            }
            return null;
        }
    }
}

