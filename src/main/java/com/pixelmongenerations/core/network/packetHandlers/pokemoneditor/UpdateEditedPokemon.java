/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.pokemoneditor.SetImportPokemonID;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public abstract class UpdateEditedPokemon
implements IMessage {
    PixelmonData data;

    protected UpdateEditedPokemon() {
    }

    protected UpdateEditedPokemon(PixelmonData data) {
        this.data = data;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        if (this.data.pseudoNature == null) {
            this.data.pseudoNature = this.data.nature;
        }
        this.data.encodeInto(buffer);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.data = new PixelmonData();
        this.data.decodeInto(buffer);
    }

    public static NBTTagCompound updatePokemon(UpdateEditedPokemon message, EntityPlayerMP player, PlayerStorage storage) {
        int pos = message.data.order;
        NBTTagCompound tag = storage.getList()[pos];
        EntityPixelmon pixelmon = null;
        if (tag == null) {
            pixelmon = (EntityPixelmon)PixelmonEntityList.createEntityByName(message.data.name, player.getEntityWorld());
            storage.addToParty(pixelmon, message.data.order);
            tag = storage.getList()[pos];
        }
        if (tag != null) {
            message.data.updatePokemon(tag);
            pixelmon = storage.sendOutFromPosition(pos, player.world);
        }
        if (pixelmon != null) {
            if (PixelmonMethods.isIDSame(message.data.pokemonID, new int[]{-1, -1})) {
                Pixelmon.NETWORK.sendTo(new SetImportPokemonID(pos, pixelmon.getPokemonId()), player);
            }
            if (pixelmon.getGrowth() == EnumGrowth.Alpha) {
                if (!pixelmon.isAlpha()) {
                    pixelmon.setAlpha(true, false);
                    if (pixelmon.getLvl().getLevel() > PixelmonConfig.maxLevel) {
                        pixelmon.getLvl().setLevel(PixelmonConfig.maxLevel);
                    }
                }
                if (pixelmon.isAlpha() && pixelmon.hasColorTint()) {
                    pixelmon.removeColorTint();
                }
            }
            pixelmon.updateStats();
            pixelmon.healByPercent(100.0f);
            pixelmon.isFainted = false;
            storage.updateAndSendToClient(pixelmon, EnumUpdateType.Stats, EnumUpdateType.Nickname, EnumUpdateType.Ability, EnumUpdateType.HeldItem, EnumUpdateType.HP, EnumUpdateType.Moveset, EnumUpdateType.Friendship, EnumUpdateType.Texture);
            pixelmon.unloadEntity();
        }
        return tag;
    }
}

