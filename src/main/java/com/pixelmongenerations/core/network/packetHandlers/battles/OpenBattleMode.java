/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class OpenBattleMode
implements IMessage {
    BattleMode mode;
    int position = -1;

    public OpenBattleMode() {
    }

    public OpenBattleMode(BattleMode mode) {
        this.mode = mode;
    }

    public OpenBattleMode(BattleMode mode, int position) {
        this.mode = mode;
        this.position = position;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.mode.ordinal());
        buf.writeInt(this.position);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.mode = BattleMode.values()[buf.readInt()];
        this.position = buf.readInt();
    }

    public static class Handler
    implements IMessageHandler<OpenBattleMode, IMessage> {
        @Override
        public IMessage onMessage(OpenBattleMode message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> {
                ClientProxy.battleManager.mode = message.mode;
                ClientProxy.battleManager.choosingPokemon = true;
                ClientProxy.battleManager.teamPokemon = null;
                if (message.position != -1) {
                    ClientProxy.battleManager.currentPokemon = message.position;
                }
                Minecraft.getMinecraft().player.openGui(Pixelmon.INSTANCE, EnumGui.Battle.getIndex(), Pixelmon.PROXY.getClientWorld(), 0, 0, 0);
            });
            return null;
        }
    }
}

