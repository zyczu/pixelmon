/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class StopStartLevelling
implements IMessage {
    int[] pokemonId;

    public StopStartLevelling() {
    }

    public StopStartLevelling(int[] pokemonId) {
        this.pokemonId = pokemonId;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.pokemonId = new int[]{buffer.readInt(), buffer.readInt()};
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pokemonId[0]);
        buffer.writeInt(this.pokemonId[1]);
    }

    public static class Handler
    implements IMessageHandler<StopStartLevelling, IMessage> {
        @Override
        public IMessage onMessage(StopStartLevelling message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            player.getServer().addScheduledTask(() -> {
                int[] pokemonId = message.pokemonId;
                Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
                if (optstorage.isPresent()) {
                    PlayerStorage storage = optstorage.get();
                    if (storage.isInWorld(pokemonId)) {
                        Optional<EntityPixelmon> pixelmonOptional = storage.getAlreadyExists(pokemonId, player.world);
                        if (pixelmonOptional.isPresent()) {
                            EntityPixelmon pixelmon = pixelmonOptional.get();
                            pixelmon.doesLevel = !pixelmon.doesLevel;
                            storage.update(pixelmon, EnumUpdateType.CanLevel);
                        }
                    } else if (storage.getNBT(pokemonId) != null) {
                        NBTTagCompound nbt = storage.getNBT(pokemonId);
                        if (nbt != null) {
                            this.invertValue(nbt);
                            storage.updateClient(nbt, EnumUpdateType.CanLevel);
                        }
                    } else if (PixelmonStorage.computerManager.getPlayerStorage(player).contains(pokemonId)) {
                        PlayerComputerStorage comp = PixelmonStorage.computerManager.getPlayerStorage(player);
                        if (!comp.contains(pokemonId)) {
                            return;
                        }
                        NBTTagCompound nbt = comp.getPokemonNBT(pokemonId);
                        if (nbt != null) {
                            this.invertValue(nbt);
                            comp.updatePokemonNBT(pokemonId, nbt);
                        }
                    }
                }
            });
            return null;
        }

        public void invertValue(NBTTagCompound nbt) {
            if (nbt.hasKey("DoesLevel")) {
                nbt.setBoolean("DoesLevel", !nbt.getBoolean("DoesLevel"));
            } else {
                nbt.setBoolean("DoesLevel", true);
            }
        }
    }
}

