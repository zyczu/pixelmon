/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.rules;

import com.pixelmongenerations.client.gui.battles.rules.GuiTeamSelect;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class RejectTeamSelect
implements IMessage {
    String clauseID;

    public RejectTeamSelect() {
    }

    public RejectTeamSelect(String clauseID) {
        this.clauseID = clauseID;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.clauseID);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.clauseID = ByteBufUtils.readUTF8String(buf);
    }

    public static class Handler
    implements IMessageHandler<RejectTeamSelect, IMessage> {
        @Override
        public IMessage onMessage(RejectTeamSelect message, MessageContext ctx) {
            GuiScreen screen = Minecraft.getMinecraft().currentScreen;
            if (screen instanceof GuiTeamSelect) {
                ((GuiTeamSelect)screen).rejectClause = message.clauseID;
            }
            return null;
        }
    }
}

