/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import com.pixelmongenerations.client.gui.overlay.SpectateOverlay;
import com.pixelmongenerations.common.item.ItemPokeball;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.storage.PCClientStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ServerConfigList
implements IMessage {
    int computerBoxCount = PlayerComputerStorage.boxCount = PixelmonConfig.computerBoxes;
    boolean allowCapturingOutsideBattle = ItemPokeball.allowCapturingOutsideBattle = PixelmonConfig.allowCapturingOutsideBattle;
    public int maxLevel = PixelmonConfig.maxLevel;
    public float ridingSpeedMultiplier = PixelmonConfig.ridingSpeedMultiplier;
    public boolean afkHandlerOn = PixelmonConfig.afkHandlerOn;
    public int afkTimerActivateSeconds = PixelmonConfig.afkTimerActivateSeconds;
    public boolean hiddenNames = PixelmonConfig.hiddenName;
    public boolean alphasSpawnHostile = PixelmonConfig.alphasSpawnHostile;
    public boolean showIVsEVs = PixelmonConfig.showIVsEVs;
    public float pokeBlockTickTime = PixelmonConfig.pokeBlockTickTime;
    public boolean sneakingRareCandyLevelsDown = PixelmonConfig.sneakingRareCandyLevelsDown;
    public boolean catchAlphasOutsideOfBattle = PixelmonConfig.catchAlphasOutsideOfBattle;
    public boolean playShinySoundOnShinySpawn = PixelmonConfig.playShinySoundOnShinySpawn;
    public int defaultNPCTrainerAggroRange = PixelmonConfig.defaultNPCTrainerAggroRange;
    public boolean allowCelestialFlute = PixelmonConfig.allowCelestialFlute;
    public boolean forceCloseBattleGuiOnLoss = PixelmonConfig.forceCloseBattleGuiOnLoss;
    public boolean canPokemonBeHit = PixelmonConfig.canPokemonBeHit;
    public boolean doPokemonAttackPlayers = PixelmonConfig.doPokemonAttackPlayers;
    public int spaceTimeDistortionLifeTimer = PixelmonConfig.spaceTimeDistortionLifeTimer;
    public int spaceTimeDistortionItemTimer = PixelmonConfig.spaceTimeDistortionItemTimer;
    public int spaceTimeDistortionPokemonTimer = PixelmonConfig.spaceTimeDistortionPokemonTimer;
    public double spaceTimeDistortionAlphaPokemonChance = PixelmonConfig.spaceTimeDistortionAlphaPokemonChance;
    public int basculinRecoilAmount = PixelmonConfig.basculinRecoilAmount;
    public boolean basculinResetRecoilOnFainting = PixelmonConfig.basculinResetRecoilOnFainting;

    @Override
    public void fromBytes(ByteBuf buf) {
        this.computerBoxCount = buf.readInt();
        this.allowCapturingOutsideBattle = buf.readBoolean();
        this.maxLevel = buf.readInt();
        this.ridingSpeedMultiplier = buf.readFloat();
        this.afkHandlerOn = buf.readBoolean();
        this.afkTimerActivateSeconds = buf.readInt();
        this.hiddenNames = buf.readBoolean();
        this.alphasSpawnHostile = buf.readBoolean();
        this.showIVsEVs = buf.readBoolean();
        this.pokeBlockTickTime = buf.readFloat();
        this.sneakingRareCandyLevelsDown = buf.readBoolean();
        this.catchAlphasOutsideOfBattle = buf.readBoolean();
        this.playShinySoundOnShinySpawn = buf.readBoolean();
        this.defaultNPCTrainerAggroRange = buf.readInt();
        this.allowCelestialFlute = buf.readBoolean();
        this.forceCloseBattleGuiOnLoss = buf.readBoolean();
        this.canPokemonBeHit = buf.readBoolean();
        this.doPokemonAttackPlayers = buf.readBoolean();
        this.spaceTimeDistortionLifeTimer = buf.readInt();
        this.spaceTimeDistortionItemTimer = buf.readInt();
        this.spaceTimeDistortionPokemonTimer = buf.readInt();
        this.spaceTimeDistortionAlphaPokemonChance = buf.readDouble();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.computerBoxCount);
        buf.writeBoolean(this.allowCapturingOutsideBattle);
        buf.writeInt(this.maxLevel);
        buf.writeFloat(this.ridingSpeedMultiplier);
        buf.writeBoolean(this.afkHandlerOn);
        buf.writeInt(this.afkTimerActivateSeconds);
        buf.writeBoolean(this.hiddenNames);
        buf.writeBoolean(this.alphasSpawnHostile);
        buf.writeBoolean(this.showIVsEVs);
        buf.writeFloat(this.pokeBlockTickTime);
        buf.writeBoolean(this.sneakingRareCandyLevelsDown);
        buf.writeBoolean(this.catchAlphasOutsideOfBattle);
        buf.writeBoolean(this.playShinySoundOnShinySpawn);
        buf.writeInt(this.defaultNPCTrainerAggroRange);
        buf.writeBoolean(this.allowCelestialFlute);
        buf.writeBoolean(this.forceCloseBattleGuiOnLoss);
        buf.writeBoolean(this.canPokemonBeHit);
        buf.writeBoolean(this.doPokemonAttackPlayers);
        buf.writeInt(this.spaceTimeDistortionLifeTimer);
        buf.writeInt(this.spaceTimeDistortionItemTimer);
        buf.writeInt(this.spaceTimeDistortionPokemonTimer);
        buf.writeDouble(this.spaceTimeDistortionAlphaPokemonChance);
    }

    public static class Handler
    implements IMessageHandler<ServerConfigList, IMessage> {
        @Override
        public IMessage onMessage(ServerConfigList message, MessageContext ctx) {
            PlayerComputerStorage.boxCount = message.computerBoxCount;
            PCClientStorage.refreshStore();
            ItemPokeball.allowCapturingOutsideBattle = message.allowCapturingOutsideBattle;
            ((SpectateOverlay)GuiPixelmonOverlay.getOverlay(OverlayType.SPECTATE)).hideSpectateMessage(null);
            PixelmonServerConfig.updateFromServer(message);
            return null;
        }
    }
}

