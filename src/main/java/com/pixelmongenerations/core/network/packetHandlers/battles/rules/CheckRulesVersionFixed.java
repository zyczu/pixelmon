/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.rules;

import com.pixelmongenerations.core.network.packetHandlers.battles.rules.CheckRulesVersion;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.ShowTeamSelect;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class CheckRulesVersionFixed
extends CheckRulesVersion<ShowTeamSelect> {
    public CheckRulesVersionFixed() {
    }

    public CheckRulesVersionFixed(int clauseVersion, ShowTeamSelect packet) {
        super(clauseVersion, packet);
    }

    @Override
    protected void readPacket(ByteBuf buf) {
        this.packet = new ShowTeamSelect();
        ((ShowTeamSelect)this.packet).fromBytes(buf);
    }

    @Override
    public void processPacket(MessageContext ctx) {
        new ShowTeamSelect.Handler().onMessage((ShowTeamSelect)this.packet, ctx);
    }

    public static class Handler
    implements IMessageHandler<CheckRulesVersionFixed, IMessage> {
        @Override
        public IMessage onMessage(CheckRulesVersionFixed message, MessageContext ctx) {
            message.onMessage(ctx);
            return null;
        }
    }
}

