/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.packetHandlers.PixelmonMountSelectionPacket;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.storage.playerData.MountData;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class OpenMountSelection
implements IMessage {
    private boolean reload;

    public OpenMountSelection() {
    }

    public OpenMountSelection(boolean reload) {
        this.reload = reload;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.reload = buf.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeBoolean(this.reload);
    }

    public static class Handler
    implements IMessageHandler<OpenMountSelection, IMessage> {
        @Override
        public IMessage onMessage(OpenMountSelection message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
            if (optstorage.isPresent()) {
                PlayerStorage storage = optstorage.get();
                MountData mountData = storage.mountData;
                if (mountData.hasFlute) {
                    if (message.reload) {
                        Pixelmon.NETWORK.sendTo(new PixelmonMountSelectionPacket(mountData), player);
                    }
                    player.getServer().addScheduledTask(() -> player.openGui(Pixelmon.INSTANCE, EnumGui.MountSelection.getIndex(), player.world, 0, 0, 0));
                }
            }
            return null;
        }
    }
}

