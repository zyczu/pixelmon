/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.battleScreens.BattleScreen;
import com.pixelmongenerations.client.gui.battles.battleScreens.ChooseAttackScreen;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UsedZMove
implements IMessage {
    @Override
    public void fromBytes(ByteBuf buf) {
    }

    @Override
    public void toBytes(ByteBuf buf) {
    }

    public static class Handler
    implements IMessageHandler<UsedZMove, IMessage> {
        @Override
        public IMessage onMessage(UsedZMove message, MessageContext ctx) {
            if (Minecraft.getMinecraft().currentScreen instanceof GuiBattle) {
                for (BattleScreen battleScreen : ((GuiBattle)Minecraft.getMinecraft().currentScreen).screenList) {
                    if (!(battleScreen instanceof ChooseAttackScreen)) continue;
                    ChooseAttackScreen chooseAttackScreen = (ChooseAttackScreen)battleScreen;
                    chooseAttackScreen.usedZMove = true;
                    break;
                }
            }
            return null;
        }
    }
}

