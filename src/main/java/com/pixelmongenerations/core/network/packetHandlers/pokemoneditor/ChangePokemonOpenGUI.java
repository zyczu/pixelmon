/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.pokemoneditor;

import com.pixelmongenerations.client.gui.pokemoneditor.GuiPartyEditorBase;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ChangePokemonOpenGUI
implements IMessage {
    int slot;

    public ChangePokemonOpenGUI() {
    }

    public ChangePokemonOpenGUI(int slot) {
        this.slot = slot;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.slot);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.slot = buf.readInt();
    }

    public static class Handler
    implements IMessageHandler<ChangePokemonOpenGUI, IMessage> {
        @Override
        public IMessage onMessage(ChangePokemonOpenGUI message, MessageContext ctx) {
            GuiPartyEditorBase.editPokemonPacket(message.slot);
            return null;
        }
    }
}

