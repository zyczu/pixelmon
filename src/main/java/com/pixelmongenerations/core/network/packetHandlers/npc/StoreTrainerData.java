/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import com.pixelmongenerations.core.network.SetTrainerData;
import com.pixelmongenerations.core.network.packetHandlers.npc.EnumStoreTrainerDataType;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class StoreTrainerData
implements IMessage {
    int trainerId;
    SetTrainerData data;
    ItemStack[] drops;
    EnumStoreTrainerDataType dataType;

    public StoreTrainerData() {
    }

    public StoreTrainerData(int trainerId, SetTrainerData data) {
        this.trainerId = trainerId;
        this.data = data;
        this.dataType = EnumStoreTrainerDataType.CHAT;
    }

    public StoreTrainerData(int currentTrainerID, ItemStack[] drops) {
        this.trainerId = currentTrainerID;
        this.drops = drops;
        this.dataType = EnumStoreTrainerDataType.DROPS;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.trainerId = buf.readInt();
        this.dataType = EnumStoreTrainerDataType.values()[buf.readInt()];
        if (this.dataType == EnumStoreTrainerDataType.CHAT) {
            this.data = new SetTrainerData();
            this.data.decodeInto(buf);
        } else {
            this.drops = new ItemStack[buf.readInt()];
            for (int i = 0; i < this.drops.length; ++i) {
                this.drops[i] = ByteBufUtils.readItemStack(buf);
            }
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.trainerId);
        buf.writeInt(this.dataType.ordinal());
        if (this.dataType == EnumStoreTrainerDataType.CHAT) {
            this.data.encodeInto(buf);
        } else {
            buf.writeInt(this.drops.length);
            for (ItemStack stack : this.drops) {
                ByteBufUtils.writeItemStack(buf, stack);
            }
        }
    }

    public static class Handler
    implements IMessageHandler<StoreTrainerData, IMessage> {
        @Override
        public IMessage onMessage(StoreTrainerData message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            if (!ItemNPCEditor.checkPermission(player)) {
                return null;
            }
            Optional<NPCTrainer> entityNPCOptional = EntityNPC.locateNPCServer(player.world, message.trainerId, NPCTrainer.class);
            if (!entityNPCOptional.isPresent()) {
                return null;
            }
            NPCTrainer trainer = entityNPCOptional.get();
            if (message.dataType == EnumStoreTrainerDataType.CHAT) {
                trainer.update(message.data);
            } else {
                trainer.updateDrops(message.drops);
            }
            return null;
        }
    }
}

