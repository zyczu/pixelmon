/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.chooseMoveset;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.chooseMoveset.ChoosingMovesetData;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ChooseMoveset
implements IMessage {
    public static final ArrayList<ChoosingMovesetData> choosingMoveset = new ArrayList(4);
    int[] id;
    ArrayList<Integer> attackIds = new ArrayList(600);

    public ChooseMoveset() {
    }

    public ChooseMoveset(PixelmonData pokemon, ArrayList<Attack> chosenAttackList) {
        this.id = pokemon.pokemonID;
        this.attackIds.addAll(chosenAttackList.stream().map(a -> a.getAttackBase().attackIndex).collect(Collectors.toList()));
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.id = new int[]{buf.readInt(), buf.readInt()};
        int num = buf.readShort();
        for (int i = 0; i < num; ++i) {
            this.attackIds.add(buf.readInt());
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.id[0]);
        buf.writeInt(this.id[1]);
        buf.writeShort(this.attackIds.size());
        this.attackIds.forEach(((ByteBuf)buf)::writeInt);
    }

    public static class Handler
    implements IMessageHandler<ChooseMoveset, IMessage> {
        @Override
        public IMessage onMessage(ChooseMoveset message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> {
                Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(ctx.getServerHandler().player);
                if (optstorage.isPresent()) {
                    ChoosingMovesetData d;
                    EntityPixelmon pixelmon;
                    PlayerStorage storage = optstorage.get();
                    boolean wasout = false;
                    Optional<EntityPixelmon> pixelmonOptional = storage.getAlreadyExists(message.id, ctx.getServerHandler().player.world);
                    if (pixelmonOptional.isPresent()) {
                        pixelmon = pixelmonOptional.get();
                        wasout = true;
                    } else {
                        pixelmon = storage.sendOut(message.id, ctx.getServerHandler().player.world);
                    }
                    pixelmon.getMoveset().replaceWith(message.attackIds);
                    pixelmon.update(EnumUpdateType.Moveset);
                    if (!wasout) {
                        pixelmon.unloadEntity();
                    }
                    if ((d = this.getChoosingMoveset(ctx.getServerHandler().player)) != null) {
                        d.next();
                        if (d.pokemonList.isEmpty()) {
                            choosingMoveset.remove(d);
                        }
                    }
                }
            });
            return null;
        }

        private ChoosingMovesetData getChoosingMoveset(EntityPlayerMP playerEntity) {
            for (ChoosingMovesetData c : choosingMoveset) {
                if (c.player.getPersistentID() != playerEntity.getPersistentID()) continue;
                return c;
            }
            return null;
        }
    }
}

