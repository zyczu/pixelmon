/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.vendingMachine;

import com.pixelmongenerations.client.gui.npc.ClientShopItem;
import com.pixelmongenerations.client.gui.npc.GuiShopContainer;
import com.pixelmongenerations.common.entity.npcs.registry.ShopItemWithVariation;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetVendingMachineData
implements IMessage {
    ArrayList<ShopItemWithVariation> buyList;
    ArrayList<ClientShopItem> clientBuyList;

    public SetVendingMachineData() {
    }

    public SetVendingMachineData(ArrayList<ShopItemWithVariation> buyList) {
        this.buyList = buyList;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeShort(this.buyList.size());
        for (ShopItemWithVariation item : this.buyList) {
            item.writeToBuffer(buffer);
        }
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        int numItems = buffer.readShort();
        this.clientBuyList = new ArrayList();
        for (int i = 0; i < numItems; ++i) {
            this.clientBuyList.add(ClientShopItem.fromBuffer(buffer));
        }
    }

    public static class Handler
    implements IMessageHandler<SetVendingMachineData, IMessage> {
        @Override
        public IMessage onMessage(SetVendingMachineData message, MessageContext ctx) {
            GuiShopContainer.buyItems = message.clientBuyList;
            return null;
        }
    }
}

