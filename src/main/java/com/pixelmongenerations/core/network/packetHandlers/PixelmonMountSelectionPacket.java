/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.client.gui.mount.ClientMountDataManager;
import com.pixelmongenerations.core.storage.playerData.MountData;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PixelmonMountSelectionPacket
implements IMessage {
    private MountData data = new MountData();

    public PixelmonMountSelectionPacket() {
    }

    public PixelmonMountSelectionPacket(MountData data) {
        this.data = data;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        this.data.writeToByteBuf(buffer);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.data.readToByteBuf(buffer);
    }

    public static class Handler
    implements IMessageHandler<PixelmonMountSelectionPacket, IMessage> {
        @Override
        public IMessage onMessage(PixelmonMountSelectionPacket message, MessageContext ctx) {
            ClientMountDataManager.mountData = message.data;
            return null;
        }
    }
}

