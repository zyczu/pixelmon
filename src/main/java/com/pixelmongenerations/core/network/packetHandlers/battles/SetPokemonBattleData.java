/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetPokemonBattleData
implements IMessage {
    PixelmonInGui[] data;
    boolean isOpponent;

    public SetPokemonBattleData() {
    }

    public SetPokemonBattleData(PixelmonInGui[] controlledPokemon) {
        this(controlledPokemon, true);
    }

    public SetPokemonBattleData(PixelmonInGui[] controlledPokemon, boolean isOpponent) {
        this.data = controlledPokemon;
        this.isOpponent = isOpponent;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        int numPokemon = this.data.length;
        for (PixelmonInGui d : this.data) {
            if (d != null) continue;
            --numPokemon;
        }
        buffer.writeShort(numPokemon);
        for (PixelmonInGui d : this.data) {
            if (d == null) continue;
            d.encodeInto(buffer);
        }
        buffer.writeBoolean(this.isOpponent);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.data = new PixelmonInGui[buffer.readShort()];
        for (int i = 0; i < this.data.length; ++i) {
            this.data[i] = new PixelmonInGui();
            this.data[i].decodeInto(buffer);
        }
        this.isOpponent = buffer.readBoolean();
    }

    public static class Handler
    implements IMessageHandler<SetPokemonBattleData, IMessage> {
        @Override
        public IMessage onMessage(SetPokemonBattleData message, MessageContext ctx) {
            if (message.isOpponent) {
                ClientProxy.battleManager.setOpponents(message.data);
            } else {
                ClientProxy.battleManager.setTeamPokemon(message.data);
            }
            return null;
        }
    }
}

