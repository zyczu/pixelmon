/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles.rules;

import com.pixelmongenerations.common.battle.rules.clauses.BattleClauseRegistry;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.RequestCustomRulesUpdate;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public abstract class CheckRulesVersion<T extends IMessage>
implements IMessage {
    int version;
    T packet;
    private static CheckRulesVersion currentPacket;

    protected CheckRulesVersion() {
    }

    public CheckRulesVersion(int version, T packet) {
        this.version = version;
        this.packet = packet;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.version);
        this.packet.toBytes(buf);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.version = buf.readInt();
        this.readPacket(buf);
    }

    protected abstract void readPacket(ByteBuf var1);

    public abstract void processPacket(MessageContext var1);

    protected void onMessage(MessageContext ctx) {
        int currentVersion = BattleClauseRegistry.getClauseVersion();
        if (currentVersion == 0 || currentVersion == this.version) {
            this.processPacket(ctx);
        } else {
            currentPacket = this;
            Pixelmon.NETWORK.sendToServer(new RequestCustomRulesUpdate());
        }
    }

    static void processStoredPacket(MessageContext ctx) {
        if (currentPacket != null) {
            currentPacket.processPacket(ctx);
        }
    }
}

