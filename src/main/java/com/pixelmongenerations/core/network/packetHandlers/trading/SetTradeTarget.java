/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.trading;

import com.pixelmongenerations.client.gui.ClientTradingManager;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonStatsData;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetTradeTarget
implements IMessage {
    PixelmonData data;
    PixelmonStatsData stats;
    boolean clear = false;

    public SetTradeTarget() {
    }

    public SetTradeTarget(PixelmonData data, PixelmonStatsData stats) {
        this.data = data;
        this.stats = stats;
    }

    public SetTradeTarget(boolean clear) {
        this.clear = clear;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeBoolean(this.clear);
        if (!this.clear) {
            this.data.encodeInto(buffer);
            this.stats.writePacketData(buffer);
        }
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.clear = buffer.readBoolean();
        if (!this.clear) {
            this.data = new PixelmonData();
            this.data.decodeInto(buffer);
            this.stats = new PixelmonStatsData();
            this.stats.readPacketData(buffer);
        }
    }

    public static class Handler
    implements IMessageHandler<SetTradeTarget, IMessage> {
        @Override
        public IMessage onMessage(SetTradeTarget message, MessageContext ctx) {
            if (message.clear) {
                ClientTradingManager.tradeTarget = null;
                ClientTradingManager.tradeTargetStats = null;
            } else {
                ClientTradingManager.tradeTarget = message.data;
                ClientTradingManager.tradeTargetStats = message.stats;
            }
            ClientTradingManager.player1Ready = false;
            ClientTradingManager.player2Ready = false;
            return null;
        }
    }
}

