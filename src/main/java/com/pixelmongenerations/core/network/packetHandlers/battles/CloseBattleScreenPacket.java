/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class CloseBattleScreenPacket
implements IMessage {
    @Override
    public void toBytes(ByteBuf buffer) {
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
    }

    public static class Handler
    implements IMessageHandler<CloseBattleScreenPacket, IMessage> {
        @Override
        public IMessage onMessage(CloseBattleScreenPacket message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(ClientProxy.battleManager::resetViewEntityClone);
            return null;
        }
    }
}

