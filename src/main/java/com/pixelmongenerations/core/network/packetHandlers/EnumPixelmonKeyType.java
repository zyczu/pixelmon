/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.network.packetHandlers;

public enum EnumPixelmonKeyType {
    SendPokemon,
    ActionKeyEntity,
    ActionKeyBlock,
    ExternalMoveEntity,
    ExternalMoveBlock,
    ExternalMove;


    public static EnumPixelmonKeyType getFromOrdinal(int ordinal) {
        return EnumPixelmonKeyType.values()[ordinal];
    }

    public boolean isEntity() {
        return this == ActionKeyEntity || this == ExternalMoveEntity;
    }

    public boolean isAction() {
        return this == ActionKeyBlock || this == ActionKeyEntity;
    }
}

