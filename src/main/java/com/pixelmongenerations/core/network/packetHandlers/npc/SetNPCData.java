/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.npc;

import com.pixelmongenerations.client.gui.npc.ClientShopItem;
import com.pixelmongenerations.client.gui.npc.GuiChattingNPC;
import com.pixelmongenerations.client.gui.npc.GuiShopContainer;
import com.pixelmongenerations.client.gui.npc.GuiShopkeeper;
import com.pixelmongenerations.common.entity.npcs.registry.ShopItemWithVariation;
import com.pixelmongenerations.common.entity.npcs.registry.ShopkeeperChat;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.network.SetTrainerData;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SetNPCData
implements IMessage {
    SetTrainerData data;
    EnumNPCType npcType;
    ArrayList<String> chatPages;
    String name;
    ArrayList<ShopItemWithVariation> buyList;
    ArrayList<ShopItemWithVariation> sellList;
    ArrayList<ClientShopItem> clientBuyList;
    ArrayList<ClientShopItem> clientSellList;
    ShopkeeperChat skChat;

    public SetNPCData() {
    }

    public SetNPCData(String name, ArrayList<String> chatPages) {
        this.name = name;
        this.npcType = EnumNPCType.ChattingNPC;
        this.chatPages = chatPages;
    }

    public SetNPCData(String name, ShopkeeperChat chat, ArrayList<ShopItemWithVariation> buyList, ArrayList<ShopItemWithVariation> sellList2) {
        this.name = name;
        this.npcType = EnumNPCType.Shopkeeper;
        this.skChat = chat;
        this.buyList = buyList;
        this.sellList = sellList2;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeShort(this.npcType.ordinal());
        switch (this.npcType) {
            case ChattingNPC: {
                ByteBufUtils.writeUTF8String(buffer, this.name);
                buffer.writeShort(this.chatPages.size());
                for (String page : this.chatPages) {
                    ByteBufUtils.writeUTF8String(buffer, page);
                }
                break;
            }
            case Shopkeeper: {
                ByteBufUtils.writeUTF8String(buffer, this.name);
                this.skChat.writeToBuffer(buffer);
                buffer.writeShort(this.buyList.size());
                for (ShopItemWithVariation item : this.buyList) {
                    item.writeToBuffer(buffer);
                }
                buffer.writeShort(this.sellList.size());
                for (ShopItemWithVariation item : this.sellList) {
                    item.writeToBuffer(buffer);
                }
                break;
            }
        }
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.npcType = EnumNPCType.getFromOrdinal(buffer.readShort());
        switch (this.npcType) {
            case ChattingNPC: {
                this.name = ByteBufUtils.readUTF8String(buffer);
                this.chatPages = new ArrayList();
                int numPages = buffer.readShort();
                for (int i = 0; i < numPages; ++i) {
                    this.chatPages.add(ByteBufUtils.readUTF8String(buffer));
                }
                break;
            }
            case Shopkeeper: {
                int i;
                this.name = ByteBufUtils.readUTF8String(buffer);
                this.skChat = ShopkeeperChat.loadFromBuffer(buffer);
                int numItems = buffer.readShort();
                this.clientBuyList = new ArrayList();
                for (i = 0; i < numItems; ++i) {
                    this.clientBuyList.add(ClientShopItem.fromBuffer(buffer));
                }
                numItems = buffer.readShort();
                this.clientSellList = new ArrayList();
                for (i = 0; i < numItems; ++i) {
                    this.clientSellList.add(ClientShopItem.fromBuffer(buffer));
                }
                break;
            }
        }
    }

    public static class Handler
    implements IMessageHandler<SetNPCData, IMessage> {
        @Override
        public IMessage onMessage(SetNPCData message, MessageContext ctx) {
            switch (message.npcType) {
                case ChattingNPC: {
                    GuiChattingNPC.name = message.name;
                    GuiChattingNPC.chatPages = message.chatPages;
                    break;
                }
                case Shopkeeper: {
                    GuiShopkeeper.name = message.name;
                    GuiShopkeeper.chat = message.skChat;
                    GuiShopContainer.buyItems = message.clientBuyList;
                    GuiShopContainer.sellItems = message.clientSellList;
                    break;
                }
            }
            return null;
        }
    }
}

