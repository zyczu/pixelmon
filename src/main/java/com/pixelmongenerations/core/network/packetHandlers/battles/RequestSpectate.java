/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.battles;

import com.pixelmongenerations.api.events.SpectateEvent;
import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.Spectator;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.battles.SetBattlingPokemon;
import com.pixelmongenerations.core.network.packetHandlers.battles.SetPokemonBattleData;
import com.pixelmongenerations.core.network.packetHandlers.battles.SetPokemonTeamData;
import com.pixelmongenerations.core.network.packetHandlers.battles.StartBattle;
import com.pixelmongenerations.core.network.packetHandlers.battles.StartSpectate;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class RequestSpectate
implements IMessage {
    UUID uuid;

    public RequestSpectate() {
    }

    public RequestSpectate(UUID playerId) {
        this.uuid = playerId;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.uuid = new UUID(buffer.readLong(), buffer.readLong());
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeLong(this.uuid.getMostSignificantBits());
        buffer.writeLong(this.uuid.getLeastSignificantBits());
    }

    public static class Handler
    implements IMessageHandler<RequestSpectate, IMessage> {
        @Override
        public IMessage onMessage(RequestSpectate message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> this.processMessage(message, ctx));
            return null;
        }

        private void processMessage(RequestSpectate message, MessageContext ctx) {
            BattleControllerBase base;
            EntityPlayerMP watcher = ctx.getServerHandler().player;
            EntityPlayerMP watchedPlayer = (EntityPlayerMP)watcher.world.getPlayerEntityByUUID(message.uuid);
            if (watchedPlayer != null && (base = BattleRegistry.getBattle(watchedPlayer)) != null && !MinecraftForge.EVENT_BUS.post(new SpectateEvent.StartSpectate(watcher, base, watchedPlayer))) {
                PlayerParticipant watchedParticipant = base.getPlayer(watchedPlayer.getDisplayNameString());
                if (watchedParticipant == null) {
                    return;
                }
                Pixelmon.NETWORK.sendTo(new StartSpectate(watchedPlayer.getUniqueID(), base.rules.battleType), watcher);
                Pixelmon.NETWORK.sendTo(new StartBattle(base.battleIndex, base.getBattleType(watchedParticipant), base.rules), watcher);
                ArrayList<PixelmonWrapper> teamList = watchedParticipant.getTeamPokemonList();
                Pixelmon.NETWORK.sendTo(new SetBattlingPokemon(teamList), watcher);
                Pixelmon.NETWORK.sendTo(new SetPokemonBattleData(PixelmonInGui.convertToGUI(teamList), false), watcher);
                Pixelmon.NETWORK.sendTo(new SetPokemonBattleData(watchedParticipant.getOpponentData(), true), watcher);
                if (base.getTeam(watchedParticipant).size() > 1) {
                    Pixelmon.NETWORK.sendTo(new SetPokemonTeamData(watchedParticipant.getAllyData()), watcher);
                }
                base.addSpectator(new Spectator(watcher, watchedPlayer.getDisplayNameString()));
            }
        }
    }
}

