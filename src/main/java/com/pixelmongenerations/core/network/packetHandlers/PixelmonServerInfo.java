/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.client.util.PixelmonDRPC;
import com.pixelmongenerations.core.config.PixelmonConfig;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PixelmonServerInfo
implements IMessage {
    public String serverName;
    public String serverIcon;

    public PixelmonServerInfo() {
    }

    public PixelmonServerInfo(String serverKey) {
        if (serverKey != null && serverKey.equalsIgnoreCase("fdefa74c")) {
            this.serverName = PixelmonConfig.serverDisplayName;
            this.serverIcon = PixelmonConfig.serverIconName;
        } else {
            this.serverName = "Multiplayer";
            this.serverIcon = "pglogo";
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.serverName);
        ByteBufUtils.writeUTF8String(buf, this.serverIcon);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.serverName = ByteBufUtils.readUTF8String(buf);
        this.serverIcon = ByteBufUtils.readUTF8String(buf);
    }

    public static class Handler
    implements IMessageHandler<PixelmonServerInfo, IMessage> {
        @Override
        public IMessage onMessage(PixelmonServerInfo message, MessageContext ctx) {
            PixelmonDRPC.setServerInfo(message.serverName, message.serverIcon);
            return null;
        }
    }
}

