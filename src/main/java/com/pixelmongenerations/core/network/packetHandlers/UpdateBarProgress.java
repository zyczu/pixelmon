/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.core.Pixelmon;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UpdateBarProgress
implements IMessage {
    private float progress;

    public UpdateBarProgress() {
    }

    public UpdateBarProgress(float progress) {
        this.progress = progress;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.progress = buf.readFloat();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeFloat(this.progress);
    }

    public static class Handler
    implements IMessageHandler<UpdateBarProgress, IMessage> {
        @Override
        public IMessage onMessage(UpdateBarProgress message, MessageContext ctx) {
            Pixelmon.PROXY.updateBarPercent(message.progress);
            return null;
        }
    }
}

