/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.client.gui.GuiItemDrops;
import com.pixelmongenerations.client.gui.GuiTrading;
import com.pixelmongenerations.client.gui.battles.EvoInfo;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ShowEvolutionScreen
implements IMessage {
    int[] pokemonId;
    String newPokemonName;

    public ShowEvolutionScreen() {
    }

    public ShowEvolutionScreen(int[] pokemonId, String newPokemonName) {
        this.pokemonId = pokemonId;
        this.newPokemonName = newPokemonName;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.pokemonId = new int[]{buffer.readInt(), buffer.readInt()};
        this.newPokemonName = ByteBufUtils.readUTF8String(buffer);
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pokemonId[0]);
        buffer.writeInt(this.pokemonId[1]);
        ByteBufUtils.writeUTF8String(buffer, this.newPokemonName);
    }

    public static class Handler
    implements IMessageHandler<ShowEvolutionScreen, IMessage> {
        @Override
        public IMessage onMessage(ShowEvolutionScreen message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> {
                boolean exists = false;
                for (int i = 0; i < ClientProxy.battleManager.evolveList.size(); ++i) {
                    if (ClientProxy.battleManager.evolveList.get((int)i).pokemonID != message.pokemonId) continue;
                    exists = true;
                }
                if (!exists) {
                    ClientProxy.battleManager.evolveList.add(new EvoInfo(message.pokemonId, message.newPokemonName));
                }
                if (ClientProxy.battleManager.battleEnded && !(Minecraft.getMinecraft().currentScreen instanceof GuiTrading) && !(Minecraft.getMinecraft().currentScreen instanceof GuiItemDrops)) {
                    Minecraft.getMinecraft().player.openGui(Pixelmon.INSTANCE, EnumGui.Evolution.getIndex(), Minecraft.getMinecraft().player.world, 0, 0, 0);
                }
            });
            return null;
        }
    }
}

