/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.api.events.SummonMountEvent;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Map;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SummonMountPacket
implements IMessage {
    EnumSpecies species;
    Integer form;

    public SummonMountPacket() {
    }

    public SummonMountPacket(PokemonSpec pokemon) {
        this.species = pokemon.getSpecies();
        this.form = pokemon.form;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.species = EnumSpecies.getFromOrdinal(buffer.readInt());
        if (buffer.readBoolean()) {
            this.form = buffer.readInt();
        }
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.species.ordinal());
        if (this.form != null) {
            buffer.writeBoolean(true);
            buffer.writeInt(this.form.intValue());
        } else {
            buffer.writeBoolean(false);
        }
    }

    public static class Handler
    implements IMessageHandler<SummonMountPacket, IMessage> {
        @Override
        public IMessage onMessage(SummonMountPacket message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> {
                EntityPlayerMP player = ctx.getServerHandler().player;
                Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
                if (optstorage.isPresent()) {
                    for (Map.Entry<PokemonSpec, Boolean> entry : optstorage.get().mountData.mountRegistry.entrySet()) {
                        if (!entry.getKey().name.equalsIgnoreCase(message.species.toString())) continue;
                        SummonMountEvent event = new SummonMountEvent(player, entry.getKey());
                        if (!event.isCanceled()) {
                            event.summon();
                        }
                        return;
                    }
                }
            });
            return null;
        }
    }
}

