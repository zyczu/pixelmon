/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.common.block.tileEntities.TileEntityFeeder;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class FeederSpawnPokemon
implements IMessage {
    public BlockPos pos;

    public FeederSpawnPokemon() {
    }

    public FeederSpawnPokemon(BlockPos pos) {
        this.pos = pos;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.pos = new BlockPos(buf.readInt(), buf.readInt(), buf.readInt());
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.pos.getX());
        buf.writeInt(this.pos.getY());
        buf.writeInt(this.pos.getZ());
    }

    public static class Handler
    implements IMessageHandler<FeederSpawnPokemon, IMessage> {
        @Override
        public IMessage onMessage(FeederSpawnPokemon message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> {
                EntityPlayerMP player = ctx.getServerHandler().player;
                TileEntity te = player.world.getTileEntity(message.pos);
                if (te instanceof TileEntityFeeder) {
                    TileEntityFeeder feeder = (TileEntityFeeder)te;
                    feeder.spawnPokemon(player);
                }
            });
            return null;
        }
    }
}

