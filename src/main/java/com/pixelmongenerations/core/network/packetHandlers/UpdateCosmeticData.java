/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.google.gson.Gson;
import com.pixelmongenerations.common.cosmetic.CosmeticData;
import com.pixelmongenerations.common.cosmetic.LocalCosmeticCache;
import com.pixelmongenerations.core.util.SyncUtil;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UpdateCosmeticData
implements IMessage {
    public String cosmeticData;

    public UpdateCosmeticData() {
    }

    public UpdateCosmeticData(CosmeticData cosmeticData) {
        this.cosmeticData = new Gson().toJson((Object)cosmeticData);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        SyncUtil.writeLargeString(this.cosmeticData, buf);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.cosmeticData = SyncUtil.readLargeString(buf);
    }

    public static class Handler
    implements IMessageHandler<UpdateCosmeticData, IMessage> {
        @Override
        public IMessage onMessage(UpdateCosmeticData message, MessageContext ctx) {
            LocalCosmeticCache.updateCosmeticData((CosmeticData)new Gson().fromJson(message.cosmeticData, CosmeticData.class));
            return null;
        }
    }
}

