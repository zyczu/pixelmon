/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class DeleteMove
implements IMessage {
    int[] pokemonId;
    int removeIndex;

    public DeleteMove() {
    }

    public DeleteMove(int[] pokemonId, int removeIndex) {
        this.pokemonId = pokemonId;
        this.removeIndex = removeIndex;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.pokemonId = new int[]{buffer.readInt(), buffer.readInt()};
        this.removeIndex = buffer.readInt();
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pokemonId[0]);
        buffer.writeInt(this.pokemonId[1]);
        buffer.writeInt(this.removeIndex);
    }

    public static class Handler
    implements IMessageHandler<DeleteMove, IMessage> {
        @Override
        public IMessage onMessage(DeleteMove message, MessageContext ctx) {
            ctx.getServerHandler().player.getServer().addScheduledTask(() -> {
                EntityPlayerMP player = ctx.getServerHandler().player;
                Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
                if (optstorage.isPresent()) {
                    PlayerStorage storage = optstorage.get();
                    Optional<EntityPixelmon> pixelmonOptional = storage.getAlreadyExists(message.pokemonId, player.world);
                    EntityPixelmon p = pixelmonOptional.isPresent() ? pixelmonOptional.get() : storage.sendOut(message.pokemonId, player.world);
                    ChatHandler.sendChat(player, "deletemove.forgot", new TextComponentTranslation("pixelmon." + p.getLocalizedName().toLowerCase() + ".name", new Object[0]), new TextComponentTranslation("attack." + p.getMoveset().get(message.removeIndex).getAttackBase().getLocalizedName().toLowerCase() + ".name", new Object[0]));
                    p.getMoveset().remove(message.removeIndex);
                    storage.update(p, EnumUpdateType.Moveset);
                }
            });
            return null;
        }
    }
}

