/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.ranch;

import com.pixelmongenerations.client.gui.ranchblock.GuiExtendRanch;
import com.pixelmongenerations.client.gui.ranchblock.GuiRanchBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityRanchBlock;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.ranch.EnumRanchClientPacketMode;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class RanchBlockClientPacket
implements IMessage {
    EnumRanchClientPacketMode mode;
    int x;
    int y;
    int z;
    ArrayList<PixelmonData> pokemon;
    PixelmonData egg;
    boolean hasEgg = false;
    boolean[] extendDirections = new boolean[4];

    public RanchBlockClientPacket() {
    }

    public RanchBlockClientPacket(TileEntityRanchBlock rb, EnumRanchClientPacketMode mode) {
        this.mode = mode;
        this.x = rb.getPos().getX();
        this.y = rb.getPos().getY();
        this.z = rb.getPos().getZ();
        if (mode == EnumRanchClientPacketMode.ViewBlock) {
            this.pokemon = rb.getPokemonData();
            for (PixelmonData d : this.pokemon) {
                d.boxNumber = -1;
            }
            this.egg = rb.getPokemonEggData();
            if (this.egg != null) {
                this.hasEgg = true;
            }
        } else if (mode == EnumRanchClientPacketMode.UpgradeBlock) {
            this.extendDirections[0] = rb.getBounds().canExtend(1, 0, 0, 0);
            this.extendDirections[1] = rb.getBounds().canExtend(0, 1, 0, 0);
            this.extendDirections[2] = rb.getBounds().canExtend(0, 0, 1, 0);
            this.extendDirections[3] = rb.getBounds().canExtend(0, 0, 0, 1);
        }
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.mode = EnumRanchClientPacketMode.getFromOrdinal(buf.readShort());
        if (this.mode == EnumRanchClientPacketMode.ViewBlock) {
            this.pokemon = new ArrayList();
            int count = buf.readShort();
            for (int i = 0; i < count; ++i) {
                PixelmonData p = new PixelmonData();
                p.decodeInto(buf);
                this.pokemon.add(p);
            }
            this.hasEgg = buf.readBoolean();
            if (this.hasEgg) {
                this.egg = new PixelmonData();
                this.egg.decodeInto(buf);
            }
        } else if (this.mode == EnumRanchClientPacketMode.UpgradeBlock) {
            for (int i = 0; i < 4; ++i) {
                this.extendDirections[i] = buf.readBoolean();
            }
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
        buf.writeShort(this.mode.ordinal());
        if (this.mode == EnumRanchClientPacketMode.ViewBlock) {
            buf.writeShort(this.pokemon.size());
            for (PixelmonData d : this.pokemon) {
                d.encodeInto(buf);
            }
            buf.writeBoolean(this.hasEgg);
            if (this.hasEgg) {
                this.egg.encodeInto(buf);
            }
        } else if (this.mode == EnumRanchClientPacketMode.UpgradeBlock) {
            for (int i = 0; i < 4; ++i) {
                buf.writeBoolean(this.extendDirections[i]);
            }
        }
    }

    public static class Handler
    implements IMessageHandler<RanchBlockClientPacket, IMessage> {
        @Override
        public IMessage onMessage(RanchBlockClientPacket message, MessageContext ctx) {
            if (message.mode == EnumRanchClientPacketMode.ViewBlock) {
                GuiRanchBlock.pos = new int[]{message.x, message.y, message.z};
                GuiRanchBlock.pokemon = message.pokemon;
                GuiRanchBlock.egg = message.egg;
                Minecraft.getMinecraft().addScheduledTask(() -> Minecraft.getMinecraft().player.openGui(Pixelmon.INSTANCE, EnumGui.RanchBlock.getIndex(), Minecraft.getMinecraft().player.world, 0, 0, 0));
            } else if (message.mode == EnumRanchClientPacketMode.UpgradeBlock) {
                GuiExtendRanch.pos = new int[]{message.x, message.y, message.z};
                GuiExtendRanch.extendDirections = message.extendDirections;
                Minecraft.getMinecraft().addScheduledTask(() -> Minecraft.getMinecraft().player.openGui(Pixelmon.INSTANCE, EnumGui.ExtendRanch.getIndex(), Minecraft.getMinecraft().player.world, 0, 0, 0));
            }
            return null;
        }
    }
}

