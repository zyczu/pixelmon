/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.evolution;

import com.pixelmongenerations.client.gui.GuiItemDrops;
import com.pixelmongenerations.client.gui.GuiTrading;
import com.pixelmongenerations.client.gui.battles.EvoInfo;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.proxy.ClientProxy;
import io.netty.buffer.ByteBuf;
import java.util.Arrays;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class OpenEvolutionGUI
implements IMessage {
    int[] pokemonId;
    String name;

    public OpenEvolutionGUI() {
    }

    public OpenEvolutionGUI(int[] pokemonId, String name) {
        this.pokemonId = pokemonId;
        this.name = name;
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.pokemonId = new int[]{buffer.readInt(), buffer.readInt()};
        this.name = ByteBufUtils.readUTF8String(buffer);
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.pokemonId[0]);
        buffer.writeInt(this.pokemonId[1]);
        ByteBufUtils.writeUTF8String(buffer, this.name);
    }

    public static class Handler
    implements IMessageHandler<OpenEvolutionGUI, IMessage> {
        @Override
        public IMessage onMessage(OpenEvolutionGUI message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> this.checkEvolution(message));
            return null;
        }

        private void checkEvolution(OpenEvolutionGUI message) {
            boolean exists = false;
            for (int i = 0; i < ClientProxy.battleManager.evolveList.size(); ++i) {
                if (!Arrays.equals(ClientProxy.battleManager.evolveList.get((int)i).pokemonID, message.pokemonId)) continue;
                exists = true;
            }
            if (!exists) {
                ClientProxy.battleManager.evolveList.add(new EvoInfo(message.pokemonId, message.name));
            }
            if (ClientProxy.battleManager.battleEnded && !(Minecraft.getMinecraft().currentScreen instanceof GuiBattle) && !(Minecraft.getMinecraft().currentScreen instanceof GuiTrading) && !(Minecraft.getMinecraft().currentScreen instanceof GuiItemDrops)) {
                Minecraft.getMinecraft().player.openGui(Pixelmon.INSTANCE, EnumGui.Evolution.getIndex(), Minecraft.getMinecraft().player.world, 0, 0, 0);
            }
        }
    }
}

