/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network.packetHandlers.customOverlays;

import com.pixelmongenerations.client.gui.overlay.AboveBarOverlay;
import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.OverlayType;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class UpdateAboveBarOverlayPacket
implements IMessage {
    public String text;
    public int textColor;
    public String icon;
    public boolean clear;

    public UpdateAboveBarOverlayPacket(String text, int textColor, String icon) {
        this.text = text;
        this.textColor = textColor;
        this.icon = icon;
        this.clear = false;
    }

    public UpdateAboveBarOverlayPacket(String text, int textColor) {
        this.text = text;
        this.textColor = textColor;
        this.icon = "";
        this.clear = false;
    }

    public UpdateAboveBarOverlayPacket(boolean clear) {
        this.clear = clear;
    }

    public UpdateAboveBarOverlayPacket() {
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeBoolean(this.clear);
        if (this.clear) {
            return;
        }
        ByteBufUtils.writeUTF8String(buf, this.text);
        buf.writeInt(this.textColor);
        ByteBufUtils.writeUTF8String(buf, this.icon);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        if (buf.readBoolean()) {
            this.clear = true;
            return;
        }
        this.text = ByteBufUtils.readUTF8String(buf);
        this.textColor = buf.readInt();
        this.icon = ByteBufUtils.readUTF8String(buf);
    }

    public static class Handler
    implements IMessageHandler<UpdateAboveBarOverlayPacket, IMessage> {
        @Override
        public IMessage onMessage(UpdateAboveBarOverlayPacket message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> {
                AboveBarOverlay overlay = (AboveBarOverlay)GuiPixelmonOverlay.getOverlay(OverlayType.ABOVE_BAR);
                if (overlay == null) {
                    return;
                }
                overlay.packet = message.clear ? null : message;
            });
            return null;
        }
    }
}

