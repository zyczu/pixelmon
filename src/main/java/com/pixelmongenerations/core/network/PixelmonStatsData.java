/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network;

import com.pixelmongenerations.common.entity.pixelmon.stats.Stats;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;

public class PixelmonStatsData {
    public int HP;
    public int Speed;
    public int Attack;
    public int Defence;
    public int SpecialAttack;
    public int SpecialDefence;

    public PixelmonStatsData() {
    }

    public PixelmonStatsData(NBTTagCompound nbt) {
        this.HP = nbt.getInteger("StatsHP");
        this.Speed = nbt.getInteger("StatsSpeed");
        this.Attack = nbt.getInteger("StatsAttack");
        this.Defence = nbt.getInteger("StatsDefence");
        this.SpecialAttack = nbt.getInteger("StatsSpecialAttack");
        this.SpecialDefence = nbt.getInteger("StatsSpecialDefence");
    }

    public static PixelmonStatsData createPacket(PokemonLink pixelmon) {
        PixelmonStatsData p = new PixelmonStatsData();
        Stats stats = pixelmon.getStats();
        p.HP = stats.HP;
        p.Speed = stats.Speed;
        p.Attack = stats.Attack;
        p.Defence = stats.Defence;
        p.SpecialAttack = stats.SpecialAttack;
        p.SpecialDefence = stats.SpecialDefence;
        return p;
    }

    public static PixelmonStatsData createPacket(NBTTagCompound nbt) {
        return new PixelmonStatsData(nbt);
    }

    public void writePacketData(ByteBuf buffer) {
        buffer.writeShort((int)((short)this.HP));
        buffer.writeShort((int)((short)this.Speed));
        buffer.writeShort((int)((short)this.Attack));
        buffer.writeShort((int)((short)this.Defence));
        buffer.writeShort((int)((short)this.SpecialAttack));
        buffer.writeShort((int)((short)this.SpecialDefence));
    }

    public void readPacketData(ByteBuf data) {
        this.HP = data.readShort();
        this.Speed = data.readShort();
        this.Attack = data.readShort();
        this.Defence = data.readShort();
        this.SpecialAttack = data.readShort();
        this.SpecialDefence = data.readShort();
    }
}

