/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network;

import com.pixelmongenerations.client.gui.pokedex.ClientPokedexManager;
import com.pixelmongenerations.common.pokedex.EnumPokedexRegisterStatus;
import com.pixelmongenerations.common.pokedex.Pokedex;
import io.netty.buffer.ByteBuf;
import java.util.HashMap;
import java.util.Map;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PixelmonPokedexPacket
implements IMessage {
    private HashMap<Integer, EnumPokedexRegisterStatus> data;

    public PixelmonPokedexPacket() {
    }

    public PixelmonPokedexPacket(HashMap<Integer, EnumPokedexRegisterStatus> data) {
        this.data = data;
    }

    public PixelmonPokedexPacket(Pokedex p) {
        this(p.getSeenMap());
    }

    public Pokedex getPokedex() {
        Pokedex p = new Pokedex();
        p.setSeenList(this.data);
        return p;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(this.data.size());
        for (Map.Entry<Integer, EnumPokedexRegisterStatus> pair : this.data.entrySet()) {
            buffer.writeInt(pair.getKey().intValue());
            buffer.writeShort(pair.getValue().ordinal());
        }
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.data = new HashMap();
        int size = buffer.readInt();
        for (int i = 0; i < size; ++i) {
            this.data.put(buffer.readInt(), EnumPokedexRegisterStatus.get(buffer.readShort()));
        }
    }

    public static class Handler
    implements IMessageHandler<PixelmonPokedexPacket, IMessage> {
        @Override
        public IMessage onMessage(PixelmonPokedexPacket message, MessageContext ctx) {
            ClientPokedexManager.pokedex = message.getPokedex();
            return null;
        }
    }
}

