/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network;

import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.core.enums.EnumMark;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonMovesetData;
import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class PixelmonUpdateData
extends PixelmonData {
    EnumUpdateType[] updateTypes;

    public PixelmonUpdateData() {
    }

    public PixelmonUpdateData(NBTTagCompound p, EnumUpdateType[] updateTypes) {
        super(p);
        this.updateTypes = updateTypes;
    }

    public PixelmonUpdateData(EntityPixelmon p, EnumUpdateType[] updateTypes) {
        super(p);
        this.updateTypes = updateTypes;
    }

    public PixelmonUpdateData(PokemonLink p, EnumUpdateType[] updateTypes) {
        super(p);
        this.updateTypes = updateTypes;
    }

    public PixelmonUpdateData(PokemonLink p, EnumUpdateType[] updateTypes, boolean inBattle) {
        this(p, updateTypes);
        this.inBattle = inBattle;
    }

    @Override
    public void encodeInto(ByteBuf data) {
        data.writeInt(this.pokemonID[0]);
        data.writeInt(this.pokemonID[1]);
        data.writeShort(this.updateTypes.length);
        data.writeBoolean(this.inBattle);
        data.writeBoolean(this.alpha);
        for (EnumUpdateType t : this.updateTypes) {
            data.writeShort(t.ordinal());
        }
        block24: for (EnumUpdateType t : this.updateTypes) {
            switch (t) {
                case HP: {
                    data.writeShort(this.hp);
                    data.writeShort(this.health);
                    data.writeBoolean(this.isFainted);
                    continue block24;
                }
                case Stats: {
                    data.writeShort(this.lvl);
                    data.writeInt(this.nextLvlXP);
                    data.writeInt(this.dynamaxLevel);
                    data.writeShort(this.xp);
                    data.writeShort(this.HP);
                    data.writeShort(this.Speed);
                    data.writeShort(this.Attack);
                    data.writeShort(this.Defence);
                    data.writeShort(this.SpecialAttack);
                    data.writeShort(this.SpecialDefence);
                    data.writeShort((int)this.form);
                    data.writeBoolean(this.isShiny);
                    data.writeBoolean(this.hasGmaxFactor);
                    data.writeShort(this.pokeball.ordinal());
                    data.writeShort(this.gender.ordinal());
                    data.writeShort(this.type1 == null ? -1 : this.type1.getIndex());
                    data.writeShort(this.type2 == null ? -1 : this.type2.getIndex());
                    ByteBufUtils.writeUTF8String(data, this.ability);
                    continue block24;
                }
                case Nickname: {
                    ByteBufUtils.writeUTF8String(data, this.nickname);
                    continue block24;
                }
                case Name: {
                    ByteBufUtils.writeUTF8String(data, this.name);
                    continue block24;
                }
                case Friendship: {
                    data.writeShort(this.friendship);
                    continue block24;
                }
                case Moveset: {
                    data.writeShort(this.numMoves);
                    for (int i = 0; i < this.numMoves; ++i) {
                        this.moveset[i].writeData(data);
                    }
                    continue block24;
                }
                case HeldItem: {
                    ByteBufUtils.writeItemStack(data, this.heldItem == null ? ItemStack.EMPTY : this.heldItem);
                    continue block24;
                }
                case Status: {
                    if (this.status == null) {
                        data.writeShort(-1);
                        continue block24;
                    }
                    data.writeShort(this.status.ordinal());
                    continue block24;
                }
                case CanLevel: {
                    data.writeBoolean(this.doesLevel);
                    continue block24;
                }
                case Egg: {
                    data.writeBoolean(this.isEgg);
                    data.writeInt(this.eggCycles);
                    data.writeInt(this.steps);
                    ByteBufUtils.writeUTF8String(data, this.eggDescription);
                    continue block24;
                }
                case Target: {
                    data.writeInt(this.targetId);
                    continue block24;
                }
                case Ability: {
                    ByteBufUtils.writeUTF8String(data, this.ability);
                    continue block24;
                }
                case Texture: {
                    data.writeShort((int)this.specialTexture);
                    ByteBufUtils.writeUTF8String(data, this.customTexture);
                    continue block24;
                }
                case Clones: {
                    data.writeInt(this.numClones);
                    continue block24;
                }
                case Enchants: {
                    data.writeInt(this.numEnchanted);
                    continue block24;
                }
                case Wormholes: {
                    data.writeInt(this.numWormholes);
                    continue block24;
                }
                case AbundantActivations: {
                    data.writeInt(this.numAbundantActivations);
                    continue block24;
                }
                case Nature: {
                    data.writeShort(this.nature.index);
                    continue block24;
                }
                case PseudoNature: {
                    data.writeShort(this.pseudoNature.index);
                    continue block24;
                }
                case PokeRus: {
                    data.writeInt(this.pokerus);
                    continue block24;
                }
                case Mark: {
                    data.writeInt(this.mark.ordinal());
                    continue block24;
                }
            }
        }
    }

    @Override
    public void decodeInto(ByteBuf data) {
        this.pokemonID = new int[]{data.readInt(), data.readInt()};
        int numTypes = data.readShort();
        this.inBattle = data.readBoolean();
        this.alpha = data.readBoolean();
        this.updateTypes = new EnumUpdateType[numTypes];
        for (int i = 0; i < numTypes; ++i) {
            this.updateTypes[i] = EnumUpdateType.getType(data.readShort());
        }
        block24: for (EnumUpdateType t : this.updateTypes) {
            switch (t) {
                case HP: {
                    this.hp = data.readShort();
                    this.health = data.readShort();
                    this.isFainted = data.readBoolean();
                    continue block24;
                }
                case Stats: {
                    this.lvl = data.readShort();
                    this.nextLvlXP = data.readInt();
                    this.dynamaxLevel = data.readInt();
                    this.xp = data.readShort();
                    this.HP = data.readShort();
                    this.Speed = data.readShort();
                    this.Attack = data.readShort();
                    this.Defence = data.readShort();
                    this.SpecialAttack = data.readShort();
                    this.SpecialDefence = data.readShort();
                    this.form = data.readShort();
                    this.isShiny = data.readBoolean();
                    this.hasGmaxFactor = data.readBoolean();
                    this.pokeball = EnumPokeball.getFromIndex(data.readShort());
                    this.gender = Gender.getGender(data.readShort());
                    short type1 = data.readShort();
                    this.type1 = type1 == -1 ? null : EnumType.parseOrNull(type1);
                    short type2 = data.readShort();
                    this.type2 = type2 == -1 ? null : EnumType.parseOrNull(type2);
                    this.ability = ByteBufUtils.readUTF8String(data);
                    continue block24;
                }
                case Nickname: {
                    this.nickname = ByteBufUtils.readUTF8String(data);
                    continue block24;
                }
                case Name: {
                    this.name = ByteBufUtils.readUTF8String(data);
                    this.species = null;
                    continue block24;
                }
                case Friendship: {
                    this.friendship = data.readShort();
                    continue block24;
                }
                case Moveset: {
                    this.numMoves = data.readShort();
                    for (int i = 0; i < this.numMoves; ++i) {
                        this.moveset[i] = new PixelmonMovesetData();
                        this.moveset[i].readData(data);
                    }
                    continue block24;
                }
                case HeldItem: {
                    this.heldItem = ByteBufUtils.readItemStack(data);
                    continue block24;
                }
                case Status: {
                    short statusIndex = data.readShort();
                    if (statusIndex <= -1) continue block24;
                    this.status = StatusType.getEffect(statusIndex);
                    continue block24;
                }
                case CanLevel: {
                    this.doesLevel = data.readBoolean();
                    continue block24;
                }
                case Egg: {
                    this.isEgg = data.readBoolean();
                    this.eggCycles = data.readInt();
                    this.steps = data.readInt();
                    this.eggDescription = ByteBufUtils.readUTF8String(data);
                    continue block24;
                }
                case Target: {
                    this.targetId = data.readInt();
                    continue block24;
                }
                case Ability: {
                    this.ability = ByteBufUtils.readUTF8String(data);
                    continue block24;
                }
                case Texture: {
                    this.specialTexture = data.readShort();
                    this.customTexture = ByteBufUtils.readUTF8String(data);
                    continue block24;
                }
                case Clones: {
                    this.numClones = data.readInt();
                    continue block24;
                }
                case Enchants: {
                    this.numEnchanted = data.readInt();
                    continue block24;
                }
                case Wormholes: {
                    this.numWormholes = data.readInt();
                    continue block24;
                }
                case AbundantActivations: {
                    this.numAbundantActivations = data.readInt();
                    continue block24;
                }
                case Nature: {
                    this.nature = EnumNature.getNatureFromIndex(data.readShort());
                    continue block24;
                }
                case PseudoNature: {
                    this.pseudoNature = EnumNature.getNatureFromIndex(data.readShort());
                    continue block24;
                }
                case PokeRus: {
                    this.pokerus = data.readInt();
                    continue block24;
                }
                case Mark: {
                    this.mark = EnumMark.values()[data.readInt()];
                }
            }
        }
    }
}

