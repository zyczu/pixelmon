/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.network;

import com.pixelmongenerations.common.block.spawning.TileEntityPixelmonGrass;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class PixelmonGrassData {
    int x;
    int y;
    int z;
    NBTTagCompound nbt;

    public PixelmonGrassData(BlockPos pos, NBTTagCompound nbt) {
        this.x = pos.getX();
        this.y = pos.getY();
        this.z = pos.getZ();
        this.nbt = nbt;
    }

    public PixelmonGrassData() {
    }

    public void writePacketData(ByteBuf data) {
        data.writeInt(this.x);
        data.writeInt(this.y);
        data.writeInt(this.z);
        ByteBufUtils.writeTag(data, this.nbt);
    }

    public void readPacketData(ByteBuf data) {
        this.x = data.readInt();
        this.y = data.readInt();
        this.z = data.readInt();
        this.nbt = ByteBufUtils.readTag(data);
    }

    public void updateTileEntity(World world) {
        if (world.getMinecraftServer().isCallingFromMinecraftThread()) {
            BlockPos pos = new BlockPos(this.x, this.y, this.z);
            if (world.getTileEntity(pos) instanceof TileEntityPixelmonGrass) {
                TileEntityPixelmonGrass t = (TileEntityPixelmonGrass)world.getTileEntity(pos);
                if (t == null) {
                    return;
                }
                t.readFromNBT(this.nbt);
                t.finishEdit();
            }
        } else {
            world.getMinecraftServer().addScheduledTask(() -> this.updateTileEntity(world));
        }
    }
}

