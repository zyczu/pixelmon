/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.proxy;

import com.pixelmongenerations.client.comm.ClientPacketProcessing;
import com.pixelmongenerations.client.gui.toast.PixelmonFrameType;
import com.pixelmongenerations.common.battle.BattleTickHandler;
import com.pixelmongenerations.common.battle.rules.clauses.tiers.RulesRegistry;
import com.pixelmongenerations.common.block.machines.FeederSpawns;
import com.pixelmongenerations.common.block.ranch.BreedingConditions;
import com.pixelmongenerations.common.block.tileEntities.TileEntityGenerationsShrine;
import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pokeballs.EntityPokeBall;
import com.pixelmongenerations.common.gui.GuiHandler;
import com.pixelmongenerations.common.item.interactions.InteractionAbilityCapsule;
import com.pixelmongenerations.common.item.interactions.InteractionAbilityPatch;
import com.pixelmongenerations.common.item.interactions.InteractionArmor;
import com.pixelmongenerations.common.item.interactions.InteractionBottleCap;
import com.pixelmongenerations.common.item.interactions.InteractionChangeForm;
import com.pixelmongenerations.common.item.interactions.InteractionCorruptedGem;
import com.pixelmongenerations.common.item.interactions.InteractionCostumes;
import com.pixelmongenerations.common.item.interactions.InteractionCurry;
import com.pixelmongenerations.common.item.interactions.InteractionDnaSplicer;
import com.pixelmongenerations.common.item.interactions.InteractionDynamaxCandy;
import com.pixelmongenerations.common.item.interactions.InteractionElixir;
import com.pixelmongenerations.common.item.interactions.InteractionEther;
import com.pixelmongenerations.common.item.interactions.InteractionEvolutionStone;
import com.pixelmongenerations.common.item.interactions.InteractionFeathers;
import com.pixelmongenerations.common.item.interactions.InteractionHalloweenSkins;
import com.pixelmongenerations.common.item.interactions.InteractionHeldItem;
import com.pixelmongenerations.common.item.interactions.InteractionJuice;
import com.pixelmongenerations.common.item.interactions.InteractionLevelCandy;
import com.pixelmongenerations.common.item.interactions.InteractionMBox;
import com.pixelmongenerations.common.item.interactions.InteractionMareep;
import com.pixelmongenerations.common.item.interactions.InteractionMaxSoup;
import com.pixelmongenerations.common.item.interactions.InteractionMint;
import com.pixelmongenerations.common.item.interactions.InteractionNLunarizer;
import com.pixelmongenerations.common.item.interactions.InteractionNSolarizer;
import com.pixelmongenerations.common.item.interactions.InteractionPPUp;
import com.pixelmongenerations.common.item.interactions.InteractionPokePuff;
import com.pixelmongenerations.common.item.interactions.InteractionPotion;
import com.pixelmongenerations.common.item.interactions.InteractionRareCandy;
import com.pixelmongenerations.common.item.interactions.InteractionReinsOfUnity;
import com.pixelmongenerations.common.item.interactions.InteractionSaddle;
import com.pixelmongenerations.common.item.interactions.InteractionSpecial;
import com.pixelmongenerations.common.item.interactions.InteractionTM;
import com.pixelmongenerations.common.item.interactions.InteractionVitamins;
import com.pixelmongenerations.common.item.interactions.InteractionWooloo;
import com.pixelmongenerations.common.item.interactions.InteractionZygardeCube;
import com.pixelmongenerations.common.spawning.PixelmonSpawner;
import com.pixelmongenerations.common.spawning.PixelmonSpawning;
import com.pixelmongenerations.common.spawning.SpaceTimeDistortionSpawns;
import com.pixelmongenerations.common.world.gen.GenericOreGenerator;
import com.pixelmongenerations.common.world.gen.UltraGenericOreGenerator;
import com.pixelmongenerations.common.world.gen.feature.WorldGenApricornTrees;
import com.pixelmongenerations.common.world.gen.feature.WorldGenBauxiteOre;
import com.pixelmongenerations.common.world.gen.feature.WorldGenBerryTrees;
import com.pixelmongenerations.common.world.gen.feature.WorldGenDawnDuskOre;
import com.pixelmongenerations.common.world.gen.feature.WorldGenEvolutionRock;
import com.pixelmongenerations.common.world.gen.feature.WorldGenFireStoneOre;
import com.pixelmongenerations.common.world.gen.feature.WorldGenFlabebeFlower;
import com.pixelmongenerations.common.world.gen.feature.WorldGenFossils;
import com.pixelmongenerations.common.world.gen.feature.WorldGenGracideaFlowers;
import com.pixelmongenerations.common.world.gen.feature.WorldGenHiddenGrotto;
import com.pixelmongenerations.common.world.gen.feature.WorldGenIceStoneOre;
import com.pixelmongenerations.common.world.gen.feature.WorldGenLeafStoneOre;
import com.pixelmongenerations.common.world.gen.feature.WorldGenPokeChest;
import com.pixelmongenerations.common.world.gen.feature.WorldGenPokemonGrass;
import com.pixelmongenerations.common.world.gen.feature.WorldGenThunderStoneOre;
import com.pixelmongenerations.common.world.gen.feature.WorldGenWaterStoneOre;
import com.pixelmongenerations.common.world.gen.structure.StructureRegistry;
import com.pixelmongenerations.common.world.gen.structure.worldGen.WorldGenGym;
import com.pixelmongenerations.common.world.gen.structure.worldGen.WorldGenScatteredFeature;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.BetterSpawnerConfig;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.data.trades.PokemonTrades;
import com.pixelmongenerations.core.data.trainers.TrainerSpawns;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.event.EntityDeath;
import com.pixelmongenerations.core.event.EntityPlayerExtension;
import com.pixelmongenerations.core.event.EntitySpawning;
import com.pixelmongenerations.core.event.PixelmonPlayerTracker;
import com.pixelmongenerations.core.event.PlayerFallListener;
import com.pixelmongenerations.core.event.ReplaceMCVillagers;
import com.pixelmongenerations.core.event.SleepHandler;
import com.pixelmongenerations.core.event.TickHandler;
import com.pixelmongenerations.core.event.WorldLoaded;
import com.pixelmongenerations.core.network.packetHandlers.SyncManifest;
import com.pixelmongenerations.core.network.packetHandlers.customOverlays.OpenBigImageGui;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.util.SpawnColors;
import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.minecraftforge.common.DungeonHooks;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CommonProxy
implements IGuiHandler {
    public GuiHandler guiHandler = new GuiHandler();

    public void preInit() {
    }

    public void init() {
        GameRegistry.registerWorldGenerator(new WorldGenLeafStoneOre(), 1);
        GameRegistry.registerWorldGenerator(new WorldGenWaterStoneOre(), 1);
        GameRegistry.registerWorldGenerator(new WorldGenThunderStoneOre(), 1);
        GameRegistry.registerWorldGenerator(new WorldGenFireStoneOre(), 1);
        GameRegistry.registerWorldGenerator(new WorldGenDawnDuskOre(), 1);
        GameRegistry.registerWorldGenerator(new WorldGenIceStoneOre(), 1);
        GameRegistry.registerWorldGenerator(new WorldGenGracideaFlowers(), 1);
        GameRegistry.registerWorldGenerator(new WorldGenPokemonGrass(), 1);
        GameRegistry.registerWorldGenerator(new WorldGenFlabebeFlower(), 1);
        GameRegistry.registerWorldGenerator(new WorldGenApricornTrees(), 1);
        GameRegistry.registerWorldGenerator(new WorldGenBauxiteOre(), 1);
        GameRegistry.registerWorldGenerator(new WorldGenFossils(), 1);
        GameRegistry.registerWorldGenerator(new WorldGenEvolutionRock(), 1);
        GameRegistry.registerWorldGenerator(new WorldGenBerryTrees(), 1);
        GameRegistry.registerWorldGenerator(new WorldGenPokeChest(), 1);
        GameRegistry.registerWorldGenerator(new WorldGenHiddenGrotto(), 1);
        GameRegistry.registerWorldGenerator(new GenericOreGenerator(PixelmonBlocks.amethystOre), 1);
        GameRegistry.registerWorldGenerator(new GenericOreGenerator(PixelmonBlocks.siliconOre), 1);
        GameRegistry.registerWorldGenerator(new GenericOreGenerator(PixelmonBlocks.sapphireOre), 1);
        GameRegistry.registerWorldGenerator(new GenericOreGenerator(PixelmonBlocks.rubyOre), 1);
        GameRegistry.registerWorldGenerator(new GenericOreGenerator(PixelmonBlocks.crystalOre), 1);
        GameRegistry.registerWorldGenerator(new UltraGenericOreGenerator(PixelmonBlocks.zCrystalOre), 1);
        if (PixelmonConfig.spawnStructures) {
            GameRegistry.registerWorldGenerator(new WorldGenScatteredFeature(), 0);
        }
        if (PixelmonConfig.spawnGyms) {
            GameRegistry.registerWorldGenerator(new WorldGenGym(), 0);
        }
        try {
            ServerNPCRegistry.shopkeepers.registerShopItems();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ServerNPCRegistry.registerNPCS(ServerNPCRegistry.en_us);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            DropItemRegistry.registerDropItems();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            RulesRegistry.registerRules();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            TrainerSpawns.setup();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        PixelmonEntityList.addSpawns();
        if (PixelmonConfig.replaceMCVillagers) {
            MinecraftForge.EVENT_BUS.register(new ReplaceMCVillagers());
        }
        if (!PixelmonConfig.disableDefaultSaving) {
            MinecraftForge.EVENT_BUS.register(PixelmonStorage.pokeBallManager);
            MinecraftForge.EVENT_BUS.register(PixelmonStorage.computerManager);
        } else {
            Pixelmon.LOGGER.warn("Detected the disabling of Pixelmon's storage save methods. This is not recommended unless you have a custom storage save handler implemented. We are not responsible for any lost Pokemon!");
        }
        MinecraftForge.EVENT_BUS.register(new EntitySpawning());
        MinecraftForge.EVENT_BUS.register(new EntityDeath());
        MinecraftForge.EVENT_BUS.register(new SleepHandler());
        MinecraftForge.EVENT_BUS.register(new PlayerFallListener());
        MinecraftForge.EVENT_BUS.register(new PixelmonPlayerTracker());
        MinecraftForge.EVENT_BUS.register(new TickHandler());
        MinecraftForge.EVENT_BUS.register(new WorldGenGym());
        MinecraftForge.EVENT_BUS.register(new SleepHandler());
        if (PixelmonConfig.allowNaturalSpawns) {
            MinecraftForge.EVENT_BUS.register(new PixelmonSpawner());
        } else {
            Pixelmon.LOGGER.warn("Detected the disabling of the natural spawners. This means no natural spawns will happen! Not recommended to do this unless you are using your own spawning system! We are not responsible for a lack of spawns!");
        }
        MinecraftForge.EVENT_BUS.register(new BattleTickHandler());
        MinecraftForge.EVENT_BUS.register(new WorldLoaded());
        MinecraftForge.TERRAIN_GEN_BUS.register(new WorldLoaded());
        MinecraftForge.EVENT_BUS.register(new EntityPlayerExtension());
        for (EnumSpecies pokemon : EnumSpecies.values()) {
            Entity3HasStats.getBaseStats(pokemon.name);
            if (!EnumSpecies.formList.containsKey(pokemon)) continue;
            EnumSpecies.formList.get(pokemon).forEach(form -> Entity3HasStats.getBaseStats(pokemon, (int)form.getForm()));
        }
    }

    public void postInitClient() {
    }

    public void postInitServer() {
        try {
            BetterSpawnerConfig.load();
            this.initWorldGenFeatures();
            PixelmonSpawning.registerSpawnSets();
            BreedingConditions.registerBreedingConditions();
            FeederSpawns.registerFeederSpawns();
            Pixelmon.spaceTimeDistortionSpawns = new SpaceTimeDistortionSpawns();
            Pixelmon.spaceTimeDistortionSpawns.registerSpaceTimeDistortionSpawns();
            PokemonTrades.setup();
            SpawnColors.setup();
            TileEntityGenerationsShrine.setup();
            StructureRegistry.registerStructures();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initWorldGenFeatures() {
        WorldGenPokeChest.init();
    }

    public void registerRenderers() {
    }

    public void registerBlockModels() {
    }

    public World getClientWorld() {
        return null;
    }

    public EntityPlayer getClientPlayer() {
        return null;
    }

    public void spawnParticle(int particleType, double xCoord, double yCoord, double zCoord, double xSpeed, double ySpeed, double zSpeed, int num1, int num2, int num3) {
    }

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        return this.guiHandler.getServerGuiElement(ID, player, world, x, y, z);
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        return null;
    }

    public void registerKeyBindings() {
    }

    public void registerTickHandlers() {
    }

    public void registerInteractions() {
        EntityPixelmon.interactionList.add(new InteractionMaxSoup());
        EntityPixelmon.interactionList.add(new InteractionCurry());
        EntityPixelmon.interactionList.add(new InteractionTM());
        EntityPixelmon.interactionList.add(new InteractionEther());
        EntityPixelmon.interactionList.add(new InteractionElixir());
        EntityPixelmon.interactionList.add(new InteractionEvolutionStone());
        EntityPixelmon.interactionList.add(new InteractionHeldItem());
        EntityPixelmon.interactionList.add(new InteractionPotion());
        EntityPixelmon.interactionList.add(new InteractionDynamaxCandy());
        EntityPixelmon.interactionList.add(new InteractionRareCandy());
        EntityPixelmon.interactionList.add(new InteractionJuice());
        EntityPixelmon.interactionList.add(new InteractionVitamins());
        EntityPixelmon.interactionList.add(new InteractionSpecial());
        EntityPixelmon.interactionList.add(new InteractionAbilityCapsule());
        EntityPixelmon.interactionList.add(new InteractionAbilityPatch());
        EntityPixelmon.interactionList.add(new InteractionChangeForm());
        EntityPixelmon.interactionList.add(new InteractionDnaSplicer());
        EntityPixelmon.interactionList.add(new InteractionReinsOfUnity());
        EntityPixelmon.interactionList.add(new InteractionNSolarizer());
        EntityPixelmon.interactionList.add(new InteractionNLunarizer());
        EntityPixelmon.interactionList.add(new InteractionCostumes());
        EntityPixelmon.interactionList.add(new InteractionHalloweenSkins());
        EntityPixelmon.interactionList.add(new InteractionPPUp());
        EntityPixelmon.interactionList.add(new InteractionMBox());
        EntityPixelmon.interactionList.add(new InteractionZygardeCube());
        EntityPixelmon.interactionList.add(new InteractionPokePuff());
        EntityPixelmon.interactionList.add(new InteractionCorruptedGem());
        EntityPixelmon.interactionList.add(new InteractionMint());
        EntityPixelmon.interactionList.add(new InteractionLevelCandy());
        EntityPixelmon.interactionList.add(new InteractionBottleCap());
        EntityPixelmon.interactionList.add(new InteractionWooloo());
        EntityPixelmon.interactionList.add(new InteractionMareep());
        EntityPixelmon.interactionList.add(new InteractionArmor());
        EntityPixelmon.interactionList.add(new InteractionSaddle());
        EntityPixelmon.interactionList.add(new InteractionFeathers());
    }

    public ClientPacketProcessing getClientPacketProcessor() {
        return null;
    }

    public void removeDungeonMobs() {
        if (!PixelmonConfig.allowNonPixelmonMobs) {
            DungeonHooks.removeDungeonMob(new ResourceLocation("skeleton"));
            DungeonHooks.removeDungeonMob(new ResourceLocation("spider"));
            DungeonHooks.removeDungeonMob(new ResourceLocation("zombie"));
            DungeonHooks.addDungeonMob(new ResourceLocation("pig"), 100);
        }
    }

    public boolean resourceLocationExists(ResourceLocation resourceLocation) {
        return Pixelmon.class.getResourceAsStream("/assets/" + resourceLocation.getNamespace() + "/" + resourceLocation.getPath()) != null;
    }

    public BufferedInputStream getStreamForResourceLocation(ResourceLocation resourceLocation) {
        return new BufferedInputStream(Pixelmon.class.getResourceAsStream("/assets/" + resourceLocation.getNamespace() + "/" + resourceLocation.getPath()));
    }

    public void fixModelDefs() {
    }

    public void resetMouseOver() {
    }

    public void spawnEntitySafely(Entity entity, World worldObj) {
        worldObj.getMinecraftServer().addScheduledTask(() -> worldObj.spawnEntity(entity));
    }

    public void sendPokeball(EntityPokeBall pokeball, EntityPlayerMP player) {
        player.world.playSound(null, player.posX, player.posY, player.posZ, SoundEvents.ENTITY_ARROW_SHOOT, SoundCategory.NEUTRAL, 0.5f, 0.4f / (player.world.rand.nextFloat() * 0.4f + 0.8f));
        player.world.getMinecraftServer().addScheduledTask(() -> player.world.spawnEntity(pokeball));
    }

    public void startManifestDownload(SyncManifest.AssetManifestType type, String manifest) {
    }

    public void setPokedexSpawns(String spawns, String times) {
    }

    public void openToast(String title, String body, ItemStack itemStack, PixelmonFrameType frameType) {
    }

    public void sendToServer(String name, String ip) {
    }

    public void openBigImage(OpenBigImageGui packet) {
    }

    public void updateBarPercent(float percent) {
    }
}

