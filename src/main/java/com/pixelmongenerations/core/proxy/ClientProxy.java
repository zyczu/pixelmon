/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.mojang.authlib.GameProfile
 *  com.mojang.authlib.minecraft.MinecraftProfileTexture
 *  com.mojang.authlib.minecraft.MinecraftProfileTexture$Type
 */
package com.pixelmongenerations.core.proxy;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import com.pixelmongenerations.client.GenerationsResourcePack;
import com.pixelmongenerations.client.PixelmonStateMapper;
import com.pixelmongenerations.client.RenderInvisibleCamera;
import com.pixelmongenerations.client.ServerStorageDisplay;
import com.pixelmongenerations.client.SoundHelper;
import com.pixelmongenerations.client.assets.resource.MinecraftModelResource;
import com.pixelmongenerations.client.assets.store.MinecraftModelStore;
import com.pixelmongenerations.client.assets.store.TextureStore;
import com.pixelmongenerations.client.assets.store.ValveModelStore;
import com.pixelmongenerations.client.assets.task.MinecraftModelTask;
import com.pixelmongenerations.client.assets.task.ResourceManagementTask;
import com.pixelmongenerations.client.assets.task.SoundTask;
import com.pixelmongenerations.client.assets.task.TextureTask;
import com.pixelmongenerations.client.assets.task.ValveModelTask;
import com.pixelmongenerations.client.camera.EntityCamera;
import com.pixelmongenerations.client.comm.ClientPacketProcessing;
import com.pixelmongenerations.client.culling.CullingThread;
import com.pixelmongenerations.client.event.BiomeFog;
import com.pixelmongenerations.client.event.MouseOverPlayer;
import com.pixelmongenerations.client.event.PlayerInteract;
import com.pixelmongenerations.client.event.RenderWorldPost;
import com.pixelmongenerations.client.gui.GuiBigImage;
import com.pixelmongenerations.client.gui.GuiDoctor;
import com.pixelmongenerations.client.gui.GuiEvolve;
import com.pixelmongenerations.client.gui.GuiHealer;
import com.pixelmongenerations.client.gui.GuiItemDrops;
import com.pixelmongenerations.client.gui.GuiMegaItem;
import com.pixelmongenerations.client.gui.GuiShinyItem;
import com.pixelmongenerations.client.gui.GuiTrading;
import com.pixelmongenerations.client.gui.battles.ClientBattleManager;
import com.pixelmongenerations.client.gui.battles.GuiAcceptDeny;
import com.pixelmongenerations.client.gui.battles.GuiBattle;
import com.pixelmongenerations.client.gui.battles.rules.GuiBattleRulesFixed;
import com.pixelmongenerations.client.gui.battles.rules.GuiBattleRulesPlayer;
import com.pixelmongenerations.client.gui.battles.rules.GuiTeamSelect;
import com.pixelmongenerations.client.gui.chooseMoveset.GuiChooseMoveset;
import com.pixelmongenerations.client.gui.cookingpot.GuiCookingPot;
import com.pixelmongenerations.client.gui.cosmetics.GuiCosmetics;
import com.pixelmongenerations.client.gui.custom.GuiInputScreen;
import com.pixelmongenerations.client.gui.custom.overlays.CustomNoticeOverlay;
import com.pixelmongenerations.client.gui.custom.overlays.CustomOverlay;
import com.pixelmongenerations.client.gui.custom.overlays.CustomScoreboardOverlay;
import com.pixelmongenerations.client.gui.dialogue.GuiDialogue;
import com.pixelmongenerations.client.gui.feeder.GuiFeeder;
import com.pixelmongenerations.client.gui.inventory.InventoryDetectionTickHandler;
import com.pixelmongenerations.client.gui.machines.mechanicalanvil.GuiMechanicalAnvil;
import com.pixelmongenerations.client.gui.mail.GuiMail;
import com.pixelmongenerations.client.gui.mount.GuiMountSelection;
import com.pixelmongenerations.client.gui.npc.GuiChattingNPC;
import com.pixelmongenerations.client.gui.npc.GuiNPCTrader;
import com.pixelmongenerations.client.gui.npc.GuiRelearner;
import com.pixelmongenerations.client.gui.npc.GuiShopkeeper;
import com.pixelmongenerations.client.gui.npc.GuiTradeYesNo;
import com.pixelmongenerations.client.gui.npc.GuiTutor;
import com.pixelmongenerations.client.gui.npcEditor.GuiChattingNPCEditor;
import com.pixelmongenerations.client.gui.npcEditor.GuiChooseNPC;
import com.pixelmongenerations.client.gui.npcEditor.GuiShopkeeperEditor;
import com.pixelmongenerations.client.gui.npcEditor.GuiTradeEditor;
import com.pixelmongenerations.client.gui.npcEditor.GuiTrainerEditor;
import com.pixelmongenerations.client.gui.npcEditor.GuiTutorEditor;
import com.pixelmongenerations.client.gui.overlay.CameraOverlay;
import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import com.pixelmongenerations.client.gui.overlay.ServerBarOverlay;
import com.pixelmongenerations.client.gui.pc.GuiPC;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeChecker;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeCheckerMoves;
import com.pixelmongenerations.client.gui.pokechecker.GuiScreenPokeCheckerStats;
import com.pixelmongenerations.client.gui.pokedex.GuiPokedexHome;
import com.pixelmongenerations.client.gui.pokedex.GuiPokedexInfo;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiEditedPlayer;
import com.pixelmongenerations.client.gui.pokemoneditor.GuiPokemonEditorParty;
import com.pixelmongenerations.client.gui.ranchblock.GuiExtendRanch;
import com.pixelmongenerations.client.gui.ranchblock.GuiPCRanch;
import com.pixelmongenerations.client.gui.ranchblock.GuiRanchBlock;
import com.pixelmongenerations.client.gui.spawner.GuiPixelmonCustomGrass;
import com.pixelmongenerations.client.gui.spawner.GuiPixelmonSpawner;
import com.pixelmongenerations.client.gui.starter.GuiChooseStarter;
import com.pixelmongenerations.client.gui.statueEditor.GuiStatueEditor;
import com.pixelmongenerations.client.gui.toast.PixelmonFrameType;
import com.pixelmongenerations.client.gui.toast.PixelmonToast;
import com.pixelmongenerations.client.gui.trashcan.GuiTrashcan;
import com.pixelmongenerations.client.gui.vendingmachine.GuiVendingMachine;
import com.pixelmongenerations.client.keybindings.ActionKey;
import com.pixelmongenerations.client.keybindings.CelestialFluteKey;
import com.pixelmongenerations.client.keybindings.CosmeticsKey;
import com.pixelmongenerations.client.keybindings.Descend;
import com.pixelmongenerations.client.keybindings.ExternalMoveKey;
import com.pixelmongenerations.client.keybindings.MinimizeMaximizeOverlayKey;
import com.pixelmongenerations.client.keybindings.MovementHandler;
import com.pixelmongenerations.client.keybindings.NextExternalMoveKey;
import com.pixelmongenerations.client.keybindings.NextPokemonKey;
import com.pixelmongenerations.client.keybindings.OptionsKey;
import com.pixelmongenerations.client.keybindings.PokedexKey;
import com.pixelmongenerations.client.keybindings.PreviousPokemonKey;
import com.pixelmongenerations.client.keybindings.SendPokemonKey;
import com.pixelmongenerations.client.keybindings.SpectateKey;
import com.pixelmongenerations.client.keybindings.WikiKey;
import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.client.models.items.ItemCurryModel;
import com.pixelmongenerations.client.models.items.ItemCustomIconModel;
import com.pixelmongenerations.client.models.items.ItemPixelmonSpriteModel;
import com.pixelmongenerations.client.models.items.ServerItemModel;
import com.pixelmongenerations.client.particle.ParticleArcanery;
import com.pixelmongenerations.client.particle.ParticleEvents;
import com.pixelmongenerations.client.render.ParticleBlocks;
import com.pixelmongenerations.client.render.RenderBike;
import com.pixelmongenerations.client.render.RenderChairMount;
import com.pixelmongenerations.client.render.RenderDynamaxEnergy;
import com.pixelmongenerations.client.render.RenderHook;
import com.pixelmongenerations.client.render.RenderMysteriousRing;
import com.pixelmongenerations.client.render.RenderNPC;
import com.pixelmongenerations.client.render.RenderPixelmon;
import com.pixelmongenerations.client.render.RenderPokeball;
import com.pixelmongenerations.client.render.RenderSpaceTimeDistortion;
import com.pixelmongenerations.client.render.RenderSpawningEntity;
import com.pixelmongenerations.client.render.RenderStatue;
import com.pixelmongenerations.client.render.RenderWishingStar;
import com.pixelmongenerations.client.render.RenderZygardeCell;
import com.pixelmongenerations.client.render.custom.PixelmonItemStackRenderer;
import com.pixelmongenerations.client.render.custom.RenderPixelmonPainting;
import com.pixelmongenerations.client.render.layers.LayerBack;
import com.pixelmongenerations.client.render.layers.LayerHead;
import com.pixelmongenerations.client.render.layers.LayerMegaBracelet;
import com.pixelmongenerations.client.render.layers.PixelmonLayerCape;
import com.pixelmongenerations.client.render.player.PixelRenderPlayer;
import com.pixelmongenerations.client.render.tileEntities.RanchBlockHighlightRender;
import com.pixelmongenerations.client.util.PixelmonFontRenderer;
import com.pixelmongenerations.common.battle.animations.particles.ParticleBreeding;
import com.pixelmongenerations.common.battle.animations.particles.ParticleGastly;
import com.pixelmongenerations.common.battle.animations.particles.ParticleShiny;
import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.generic.GenericModelBlock;
import com.pixelmongenerations.common.block.multiBlocks.BlockGenericModelMultiblock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCookingPot;
import com.pixelmongenerations.common.block.tileEntities.TileEntityFeeder;
import com.pixelmongenerations.common.block.tileEntities.TileEntityMechanicalAnvil;
import com.pixelmongenerations.common.block.tileEntities.TileEntityTrashcan;
import com.pixelmongenerations.common.entity.EntityChairMount;
import com.pixelmongenerations.common.entity.EntityDynamaxEnergy;
import com.pixelmongenerations.common.entity.EntityLegendFinder;
import com.pixelmongenerations.common.entity.EntityMagmaCrystal;
import com.pixelmongenerations.common.entity.EntityMysteriousRing;
import com.pixelmongenerations.common.entity.EntitySpaceTimeDistortion;
import com.pixelmongenerations.common.entity.EntityWishingStar;
import com.pixelmongenerations.common.entity.EntityZygardeCell;
import com.pixelmongenerations.common.entity.SpawningEntity;
import com.pixelmongenerations.common.entity.bikes.EntityBike;
import com.pixelmongenerations.common.entity.custom.EntityPixelmonPainting;
import com.pixelmongenerations.common.entity.npcs.NPCChatting;
import com.pixelmongenerations.common.entity.npcs.NPCDamos;
import com.pixelmongenerations.common.entity.npcs.NPCGroomer;
import com.pixelmongenerations.common.entity.npcs.NPCNurseJoy;
import com.pixelmongenerations.common.entity.npcs.NPCRelearner;
import com.pixelmongenerations.common.entity.npcs.NPCShopkeeper;
import com.pixelmongenerations.common.entity.npcs.NPCSticker;
import com.pixelmongenerations.common.entity.npcs.NPCTrader;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.npcs.NPCTutor;
import com.pixelmongenerations.common.entity.npcs.NPCUltra;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.EntityStatue;
import com.pixelmongenerations.common.entity.pokeballs.EntityPokeBall;
import com.pixelmongenerations.common.entity.projectiles.EntityArcanineRock;
import com.pixelmongenerations.common.entity.projectiles.EntityAvaluggIce;
import com.pixelmongenerations.common.entity.projectiles.EntityElectrodeLightning;
import com.pixelmongenerations.common.entity.projectiles.EntityForestBalm;
import com.pixelmongenerations.common.entity.projectiles.EntityHook;
import com.pixelmongenerations.common.entity.projectiles.EntityKleavorAxe;
import com.pixelmongenerations.common.entity.projectiles.EntityLilligantFlower;
import com.pixelmongenerations.common.entity.projectiles.EntityMarshBalm;
import com.pixelmongenerations.common.entity.projectiles.EntityMountainBalm;
import com.pixelmongenerations.common.entity.projectiles.EntitySnowBalm;
import com.pixelmongenerations.common.entity.projectiles.EntityVolcanoBalm;
import com.pixelmongenerations.core.PixelmonMusicTicker;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonBlocksApricornTrees;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsBalms;
import com.pixelmongenerations.core.config.TileEntityRegistry;
import com.pixelmongenerations.core.data.asset.manifest.MinecraftModelManifest;
import com.pixelmongenerations.core.data.asset.manifest.SoundManifest;
import com.pixelmongenerations.core.data.asset.manifest.TextureManifest;
import com.pixelmongenerations.core.data.asset.manifest.ValveModelManifest;
import com.pixelmongenerations.core.enums.EnumBreedingParticles;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.EnumPixelmonParticles;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.packetHandlers.SyncManifest;
import com.pixelmongenerations.core.network.packetHandlers.customOverlays.OpenBigImageGui;
import com.pixelmongenerations.core.proxy.CommonProxy;
import com.pixelmongenerations.core.storage.PCClientStorage;
import com.pixelmongenerations.core.util.PixelMusic;
import com.pixelmongenerations.core.util.PixelSounds;
import com.pixelmongenerations.core.util.Sounds;
import com.pixelmongenerations.core.util.helper.RCFileHelper;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.multiplayer.GuiConnecting;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.entity.RenderSnowball;
import net.minecraft.client.resources.DefaultPlayerSkin;
import net.minecraft.client.resources.IResourceManagerReloadListener;
import net.minecraft.client.resources.SimpleReloadableResourceManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntitySkull;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(value=Side.CLIENT)
public class ClientProxy
extends CommonProxy {
    public static ClientBattleManager battleManager = new ClientBattleManager();
    public static EntityCamera camera;
    public static ActionKey actionKeyBind;
    public static ExternalMoveKey externalKeyBind;
    public static PokedexKey pokedexKeyBind;
    public static WikiKey wikiKeyBind;
    public static SpectateKey spectateKeyBind;
    public static CosmeticsKey cosmeticsKeyBind;
    public static CelestialFluteKey fluteKeyBind;
    ClientPacketProcessing cpp = new ClientPacketProcessing();
    static ConcurrentHashMap<String, ResourceLocation> cachedSkins;
    static List<String> invaildSkins;
    public static TextureStore TEXTURE_STORE;
    public static ValveModelStore VALVE_MODEL_STORE;
    public static MinecraftModelStore MINECRAFT_MODEL_STORE;
    public static ResourceManagementTask RESOURCE_MANAGEMENT_TASK;
    private static final CullingThread CULLING_THREAD;
    public static IBakedModel ibakedmodel;

    @Override
    public void preInit() {
        PixelmonModelRegistry.init();
        this.addPokemonRenderers();
        MinecraftForge.EVENT_BUS.register(new ParticleEvents());
        SimpleReloadableResourceManager manager = (SimpleReloadableResourceManager)Minecraft.getMinecraft().resourceManager;
        manager.reloadResourcePack(new GenerationsResourcePack());
        CULLING_THREAD.start();
    }

    @Override
    public void init() {
        super.init();
        MinecraftForge.EVENT_BUS.register(CameraOverlay.class);
        Timer timer = new Timer("resource-management");
        timer.scheduleAtFixedRate((TimerTask)RESOURCE_MANAGEMENT_TASK, 100L, 100L);
    }

    @Override
    public void postInitClient() {
        PixelmonFontRenderer.setupFontRenderer();
        Minecraft.getMinecraft().resourceManager.registerReloadListener((IResourceManagerReloadListener)((Object)new Sounds()));
        PixelMusic.registerMusicTypes();
        Minecraft.getMinecraft().musicTicker = new PixelmonMusicTicker(Minecraft.getMinecraft());
    }

    @Override
    public void registerBlockModels() {
        PixelmonBlocks.registerModels();
    }

    @Override
    public void registerRenderers() {
        TileEntityRegistry.registerRenderers();
        MinecraftForge.EVENT_BUS.register(new CustomOverlay());
        MinecraftForge.EVENT_BUS.register(new GuiPixelmonOverlay());
        MinecraftForge.EVENT_BUS.register(new BiomeFog());
        MinecraftForge.EVENT_BUS.register(new RanchBlockHighlightRender());
        MinecraftForge.EVENT_BUS.register(new MouseOverPlayer());
        MinecraftForge.EVENT_BUS.register(new PlayerInteract());
        MinecraftForge.EVENT_BUS.register(new RenderWorldPost());
        RenderManager renderManager = Minecraft.getMinecraft().getRenderManager();
        renderManager.skinMap.remove("default");
        renderManager.skinMap.remove("slim");
        renderManager.skinMap.put("default", new PixelRenderPlayer(renderManager));
        renderManager.skinMap.put("slim", new PixelRenderPlayer(renderManager, true));
        RenderPlayer rp = renderManager.skinMap.get("default");
        rp.addLayer(new LayerMegaBracelet(rp));
        rp.addLayer(new LayerBack(rp, rp.getMainModel().bipedBodyWear));
        rp.addLayer(new LayerHead(rp, rp.getMainModel().bipedHead));
        rp.addLayer(new PixelmonLayerCape(rp));
        rp = renderManager.skinMap.get("slim");
        rp.addLayer(new LayerMegaBracelet(rp));
        rp.addLayer(new LayerBack(rp, rp.getMainModel().bipedBodyWear));
        rp.addLayer(new LayerHead(rp, rp.getMainModel().bipedHead));
        rp.addLayer(new PixelmonLayerCape(rp));
        PixelmonItems.serverItem.setTileEntityItemStackRenderer(new PixelmonItemStackRenderer());
    }

    @SubscribeEvent
    public void onModelBakeEvent(ModelBakeEvent event) {
        MinecraftModelResource.modelManager = event.getModelManager();
        MinecraftModelResource.modelLoader = event.getModelLoader();
        this.bakePixelmonSprites(event);
        this.bakeCustomIcons(event);
        this.bakeCurries(event);
        event.getModelRegistry().putObject(ServerItemModel.modelResourceLocation, new ServerItemModel());
    }

    private void bakePixelmonSprites(ModelBakeEvent event) {
        IBakedModel object = event.getModelRegistry().getObject(ItemPixelmonSpriteModel.modelResourceLocation);
        if (object != null) {
            ItemPixelmonSpriteModel customModel = new ItemPixelmonSpriteModel(object);
            event.getModelRegistry().putObject(ItemPixelmonSpriteModel.modelResourceLocation, customModel);
        }
    }

    private void bakeCustomIcons(ModelBakeEvent event) {
        IBakedModel object = event.getModelRegistry().getObject(ItemCustomIconModel.modelResourceLocation);
        if (object != null) {
            ItemCustomIconModel customModel = new ItemCustomIconModel(object);
            event.getModelRegistry().putObject(ItemCustomIconModel.modelResourceLocation, customModel);
        }
    }

    private void bakeCurries(ModelBakeEvent event) {
        IBakedModel object = event.getModelRegistry().getObject(ItemCurryModel.modelResourceLocation);
        if (object != null) {
            ItemCurryModel customModel = new ItemCurryModel(object);
            event.getModelRegistry().putObject(ItemCurryModel.modelResourceLocation, customModel);
        }
    }

    @Override
    public void spawnParticle(int particleType, double xCoord, double yCoord, double zCoord, double xSpeed, double ySpeed, double zSpeed, int num1, int num2, int num3) {
        if (Minecraft.getMinecraft().player == null || Minecraft.getMinecraft().player.world == null) {
            return;
        }
        Minecraft.getMinecraft().player.world.spawnParticle(EnumParticleTypes.getParticleFromId(particleType), false, xCoord, yCoord, zCoord, xSpeed, ySpeed, zSpeed, num1, num2, num3);
    }

    @SubscribeEvent
    public void stitcherEventPre(TextureStitchEvent.Pre event) {
        this.loadImagesToAtlas("sprites/eggs", event);
        this.loadImagesToAtlas("sprites/pokemon", event);
        this.loadImagesToAtlas("sprites/shinypokemon", event);
        this.loadImagesToAtlas("customicon", event);
        this.loadImagesToAtlas("curry/curry", event);
        this.loadImagesToAtlas("curry/haze", event);
        this.loadImagesToAtlas("curry/ingredients", event);
    }

    public void loadImagesToAtlas(String textureSubDir, TextureStitchEvent.Pre event) {
        try {
            Path path = RCFileHelper.pathFromResourceLocation(new ResourceLocation("pixelmon", "textures/" + textureSubDir));
            List<Path> pngPaths = RCFileHelper.listFilesRecursively(path, entry -> entry.getFileName().toString().endsWith(".png"), false);
            for (Path pngPath : pngPaths) {
                String fileName = pngPath.getFileName().toString();
                event.getMap().registerSprite(new ResourceLocation("pixelmon", textureSubDir + "/" + fileName.substring(0, fileName.length() - 4)));
            }
        }
        catch (Exception var8) {
            var8.printStackTrace();
        }
    }

    @Override
    public void fixModelDefs() {
        Block block;
        PixelmonStateMapper pixelmonStateMapper = new PixelmonStateMapper();
        ModelLoader.setCustomStateMapper(PixelmonBlocks.hiddenIronDoor, pixelmonStateMapper);
        ModelLoader.setCustomStateMapper(PixelmonBlocks.hiddenWoodenDoor, pixelmonStateMapper);
        ModelLoader.setCustomStateMapper(PixelmonBlocks.masterChest, pixelmonStateMapper);
        ModelLoader.setCustomStateMapper(PixelmonBlocks.pokeChest, pixelmonStateMapper);
        ModelLoader.setCustomStateMapper(PixelmonBlocks.ultraChest, pixelmonStateMapper);
        ModelLoader.setCustomStateMapper(PixelmonBlocks.mechanicalAnvil, pixelmonStateMapper);
        ModelLoader.setCustomStateMapper(PixelmonBlocks.hiddenPressurePlate, pixelmonStateMapper);
        for (Field field : PixelmonBlocks.class.getFields()) {
            try {
                if (!(field.get(null) instanceof Block) || !((block = (Block)field.get(null)) instanceof MultiBlock) && block.getRenderType(block.getDefaultState()) != EnumBlockRenderType.INVISIBLE && !(block instanceof GenericModelBlock) && !(block instanceof BlockGenericModelMultiblock)) continue;
                ModelLoader.setCustomStateMapper(block, pixelmonStateMapper);
            }
            catch (IllegalAccessException var8) {
                var8.printStackTrace();
            }
        }
        for (Field field : PixelmonBlocksApricornTrees.class.getFields()) {
            try {
                if (!(field.get(null) instanceof Block) || !((block = (Block)field.get(null)) instanceof MultiBlock) && block.getRenderType(block.getDefaultState()) != EnumBlockRenderType.MODEL) continue;
                ModelLoader.setCustomStateMapper(block, pixelmonStateMapper);
            }
            catch (IllegalAccessException var7) {
                var7.printStackTrace();
            }
        }
    }

    @Override
    public World getClientWorld() {
        return Minecraft.getMinecraft().world;
    }

    @Override
    public EntityPlayer getClientPlayer() {
        return Minecraft.getMinecraft().player;
    }

    @Override
    public void registerKeyBindings() {
        MinecraftForge.EVENT_BUS.register(this);
        SendPokemonKey k1 = new SendPokemonKey();
        ClientRegistry.registerKeyBinding(k1);
        MinecraftForge.EVENT_BUS.register(k1);
        NextPokemonKey k2 = new NextPokemonKey();
        ClientRegistry.registerKeyBinding(k2);
        MinecraftForge.EVENT_BUS.register(k2);
        PreviousPokemonKey k3 = new PreviousPokemonKey();
        ClientRegistry.registerKeyBinding(k3);
        MinecraftForge.EVENT_BUS.register(k3);
        MinimizeMaximizeOverlayKey k4 = new MinimizeMaximizeOverlayKey();
        ClientRegistry.registerKeyBinding(k4);
        MinecraftForge.EVENT_BUS.register(k4);
        Descend k5 = new Descend();
        ClientRegistry.registerKeyBinding(k5);
        MinecraftForge.EVENT_BUS.register(k5);
        spectateKeyBind = new SpectateKey();
        ClientRegistry.registerKeyBinding(spectateKeyBind);
        MinecraftForge.EVENT_BUS.register(spectateKeyBind);
        actionKeyBind = new ActionKey();
        ClientRegistry.registerKeyBinding(actionKeyBind);
        MinecraftForge.EVENT_BUS.register(actionKeyBind);
        externalKeyBind = new ExternalMoveKey();
        ClientRegistry.registerKeyBinding(externalKeyBind);
        MinecraftForge.EVENT_BUS.register(externalKeyBind);
        NextExternalMoveKey nextExternalKeyBind = new NextExternalMoveKey();
        ClientRegistry.registerKeyBinding(nextExternalKeyBind);
        MinecraftForge.EVENT_BUS.register(nextExternalKeyBind);
        pokedexKeyBind = new PokedexKey();
        ClientRegistry.registerKeyBinding(pokedexKeyBind);
        MinecraftForge.EVENT_BUS.register(pokedexKeyBind);
        wikiKeyBind = new WikiKey();
        ClientRegistry.registerKeyBinding(wikiKeyBind);
        MinecraftForge.EVENT_BUS.register(wikiKeyBind);
        cosmeticsKeyBind = new CosmeticsKey();
        ClientRegistry.registerKeyBinding(cosmeticsKeyBind);
        MinecraftForge.EVENT_BUS.register(cosmeticsKeyBind);
        fluteKeyBind = new CelestialFluteKey();
        ClientRegistry.registerKeyBinding(fluteKeyBind);
        MinecraftForge.EVENT_BUS.register(fluteKeyBind);
        MinecraftForge.EVENT_BUS.register(new MovementHandler());
        OptionsKey k6 = new OptionsKey();
        ClientRegistry.registerKeyBinding(k6);
        MinecraftForge.EVENT_BUS.register(k6);
    }

    private void addPokemonRenderers() {
        RenderingRegistry.registerEntityRenderingHandler(EntityPokeBall.class, RenderPokeball::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityForestBalm.class, a -> new RenderSnowball(a, PixelmonItemsBalms.forestBalm, Minecraft.getMinecraft().getRenderItem()));
        RenderingRegistry.registerEntityRenderingHandler(EntityMarshBalm.class, a -> new RenderSnowball(a, PixelmonItemsBalms.marshBalm, Minecraft.getMinecraft().getRenderItem()));
        RenderingRegistry.registerEntityRenderingHandler(EntityMountainBalm.class, a -> new RenderSnowball(a, PixelmonItemsBalms.mountainBalm, Minecraft.getMinecraft().getRenderItem()));
        RenderingRegistry.registerEntityRenderingHandler(EntitySnowBalm.class, a -> new RenderSnowball(a, PixelmonItemsBalms.snowBalm, Minecraft.getMinecraft().getRenderItem()));
        RenderingRegistry.registerEntityRenderingHandler(EntityVolcanoBalm.class, a -> new RenderSnowball(a, PixelmonItemsBalms.volcanoBalm, Minecraft.getMinecraft().getRenderItem()));
        RenderingRegistry.registerEntityRenderingHandler(EntityKleavorAxe.class, a -> new RenderSnowball(a, PixelmonItems.kleavorAxe, Minecraft.getMinecraft().getRenderItem()));
        RenderingRegistry.registerEntityRenderingHandler(EntityLilligantFlower.class, a -> new RenderSnowball(a, PixelmonItems.lilligantFlower, Minecraft.getMinecraft().getRenderItem()));
        RenderingRegistry.registerEntityRenderingHandler(EntityArcanineRock.class, a -> new RenderSnowball(a, PixelmonItems.arcanineRock, Minecraft.getMinecraft().getRenderItem()));
        RenderingRegistry.registerEntityRenderingHandler(EntityElectrodeLightning.class, a -> new RenderSnowball(a, PixelmonItems.electrodeLightning, Minecraft.getMinecraft().getRenderItem()));
        RenderingRegistry.registerEntityRenderingHandler(EntityAvaluggIce.class, a -> new RenderSnowball(a, PixelmonItems.avaluggIce, Minecraft.getMinecraft().getRenderItem()));
        RenderingRegistry.registerEntityRenderingHandler(EntityHook.class, RenderHook::new);
        RenderingRegistry.registerEntityRenderingHandler(NPCTrainer.class, RenderNPC::new);
        RenderingRegistry.registerEntityRenderingHandler(NPCChatting.class, RenderNPC::new);
        RenderingRegistry.registerEntityRenderingHandler(NPCTrader.class, RenderNPC::new);
        RenderingRegistry.registerEntityRenderingHandler(NPCRelearner.class, RenderNPC::new);
        RenderingRegistry.registerEntityRenderingHandler(NPCTutor.class, RenderNPC::new);
        RenderingRegistry.registerEntityRenderingHandler(NPCNurseJoy.class, RenderNPC::new);
        RenderingRegistry.registerEntityRenderingHandler(NPCSticker.class, RenderNPC::new);
        RenderingRegistry.registerEntityRenderingHandler(NPCGroomer.class, RenderNPC::new);
        RenderingRegistry.registerEntityRenderingHandler(NPCUltra.class, RenderNPC::new);
        RenderingRegistry.registerEntityRenderingHandler(NPCDamos.class, RenderNPC::new);
        RenderingRegistry.registerEntityRenderingHandler(NPCShopkeeper.class, RenderNPC::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityStatue.class, RenderStatue::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityPixelmon.class, RenderPixelmon::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityCamera.class, RenderInvisibleCamera::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityPixelmonPainting.class, RenderPixelmonPainting::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityLegendFinder.class, new EntityLegendFinder.EntityLegendFactory());
        RenderingRegistry.registerEntityRenderingHandler(EntityBike.class, RenderBike::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityZygardeCell.class, RenderZygardeCell::new);
        RenderingRegistry.registerEntityRenderingHandler(EntitySpaceTimeDistortion.class, RenderSpaceTimeDistortion::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityMysteriousRing.class, RenderMysteriousRing::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityDynamaxEnergy.class, RenderDynamaxEnergy::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityWishingStar.class, RenderWishingStar::new);
        RenderingRegistry.registerEntityRenderingHandler(SpawningEntity.class, RenderSpawningEntity::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityChairMount.class, RenderChairMount::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityMagmaCrystal.class, a -> new RenderSnowball(a, PixelmonItems.magmaCrystal, Minecraft.getMinecraft().getRenderItem()));
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        EnumGui gui = EnumGui.getFromOrdinal(ID);
        if (!Minecraft.getMinecraft().isCallingFromMinecraftThread()) {
            System.out.println("Dodgy gui call from non-main thread for " + gui.toString());
        }
        switch (gui) {
            case ChooseStarter: {
                return new GuiChooseStarter();
            }
            case LearnMove: 
            case LevelUp: 
            case Battle: 
            case ChooseRelearnMove: 
            case ChooseTutor: {
                return new GuiBattle();
            }
            case Pokedex: {
                return new GuiPokedexHome(x);
            }
            case PC: {
                return new GuiPC();
            }
            case Healer: {
                return new GuiHealer();
            }
            case HealerNurseJoy: {
                return new GuiHealer(x, y, z);
            }
            case PokeChecker: {
                if (x == -1) {
                    return new GuiScreenPokeChecker(GuiPC.selected, true, y);
                }
                return new GuiScreenPokeChecker(ServerStorageDisplay.get(new int[]{x, y}), false);
            }
            case PokeCheckerStats: {
                if (x == -1) {
                    return new GuiScreenPokeCheckerStats(GuiPC.selected, true, y);
                }
                return new GuiScreenPokeCheckerStats(ServerStorageDisplay.get(new int[]{x, y}), false);
            }
            case PokeCheckerMoves: {
                if (x == -1) {
                    return new GuiScreenPokeCheckerMoves(GuiPC.selected, true, y);
                }
                return new GuiScreenPokeCheckerMoves(ServerStorageDisplay.get(new int[]{x, y}), false);
            }
            case Trading: {
                return new GuiTrading(x, y, z);
            }
            case Doctor: {
                return new GuiDoctor();
            }
            case AcceptDeny: {
                return new GuiAcceptDeny(x);
            }
            case Evolution: {
                return new GuiEvolve();
            }
            case ItemDrops: {
                return new GuiItemDrops();
            }
            case PixelmonSpawner: {
                return new GuiPixelmonSpawner(x, y, z);
            }
            case PixelmonCustomGrass: {
                return new GuiPixelmonCustomGrass(x, y, z);
            }
            case TrainerEditor: {
                return new GuiTrainerEditor(x);
            }
            case TradeYesNo: {
                return new GuiTradeYesNo(x);
            }
            case NPCTrade: {
                return new GuiTradeEditor(x);
            }
            case NPCTraderGui: {
                return new GuiNPCTrader(x);
            }
            case ChooseMoveset: {
                return new GuiChooseMoveset(ServerStorageDisplay.get(new int[]{x, y}));
            }
            case RanchBlock: {
                return new GuiRanchBlock();
            }
            case ExtendRanch: {
                return new GuiExtendRanch();
            }
            case PCNoParty: {
                return new GuiPCRanch();
            }
            case StatueEditor: {
                return new GuiStatueEditor(x, y == 1);
            }
            case MechaAnvil: {
                return new GuiMechanicalAnvil(player.inventory, (TileEntityMechanicalAnvil)world.getTileEntity(new BlockPos(x, y, z)));
            }
            case InputScreen: {
                return new GuiInputScreen();
            }
            case SelectNPCType: {
                return new GuiChooseNPC(new BlockPos(x, y, z));
            }
            case NPCChatEditor: {
                return new GuiChattingNPCEditor(x);
            }
            case NPCChat: {
                return new GuiChattingNPC(x);
            }
            case Relearner: {
                return new GuiRelearner(ServerStorageDisplay.get(new int[]{x, y}));
            }
            case Tutor: {
                return new GuiTutor(ServerStorageDisplay.get(new int[]{x, y}));
            }
            case TutorEditor: {
                return new GuiTutorEditor(x);
            }
            case Shopkeeper: {
                return new GuiShopkeeper(x);
            }
            case ShopkeeperEditor: {
                return new GuiShopkeeperEditor(x);
            }
            case VendingMachine: {
                return new GuiVendingMachine(new BlockPos(x, y, z));
            }
            case PokemonEditor: {
                return new GuiPokemonEditorParty();
            }
            case EditedPlayer: {
                return new GuiEditedPlayer();
            }
            case MegaItem: {
                if (x == 1) {
                    SoundHelper.playSound(PixelSounds.get_item);
                }
                return new GuiMegaItem(x == 1);
            }
            case BattleRulesPlayer: {
                return new GuiBattleRulesPlayer(x, y == 1);
            }
            case BattleRulesFixed: {
                return new GuiBattleRulesFixed();
            }
            case TeamSelect: {
                return new GuiTeamSelect();
            }
            case Dialogue: {
                return new GuiDialogue();
            }
            case Cosmetics: {
                return new GuiCosmetics();
            }
            case ShinyItem: {
                if (x == 1) {
                    SoundHelper.playSound(PixelSounds.get_item);
                }
                return new GuiShinyItem(x == 1);
            }
            case PokedexInfo: {
                Optional<EnumSpecies> pokemonOpt = EnumSpecies.getFromDex(x);
                if (pokemonOpt.isPresent()) {
                    return new GuiPokedexInfo(pokemonOpt.get());
                }
                return new GuiPokedexHome();
            }
            case TrashCan: {
                return new GuiTrashcan(player.inventory, (TileEntityTrashcan)world.getTileEntity(new BlockPos(x, y, z)));
            }
            case CookingPot: {
                return new GuiCookingPot(player.inventory, (TileEntityCookingPot)world.getTileEntity(new BlockPos(x, y, z)));
            }
            case Mail: {
                return new GuiMail(player, player.getHeldItemMainhand());
            }
            case Feeder: {
                return new GuiFeeder(player.inventory, (TileEntityFeeder)world.getTileEntity(new BlockPos(x, y, z)));
            }
            case MountSelection: {
                return new GuiMountSelection();
            }
        }
        return null;
    }

    public static File getMinecraftDir() {
        return Minecraft.getMinecraft().gameDir;
    }

    @SubscribeEvent
    public void onWorldLoad(WorldEvent.Load event) {
        ServerStorageDisplay.clear();
        PCClientStorage.clearList();
        CustomNoticeOverlay.resetNotice();
        CustomScoreboardOverlay.resetBoard();
    }

    @SubscribeEvent
    public void onWorldUnload(WorldEvent.Unload event) {
        EntityPixelmon.freeze = false;
        ClientProxy.battleManager.battleEnded = true;
        ServerStorageDisplay.clear();
        PCClientStorage.clearList();
        CustomNoticeOverlay.resetNotice();
        CustomScoreboardOverlay.resetBoard();
    }

    public static void spawnParticle(World w, double d1, double d2, double d3, Block stone) {
        ParticleBlocks fx = new ParticleBlocks(w, d1, d2, d3, 0.0, 0.0, 0.0, w.getBlockState(new BlockPos(d1, d2, d3)));
        FMLClientHandler.instance().getClient().effectRenderer.addEffect(fx);
    }

    public static void spawnParticle(EnumPixelmonParticles particle, World worldObj, double posX, double posY, double posZ, boolean isShiny) {
        try {
            Particle fx = particle.particleClass == ParticleGastly.class ? new ParticleGastly(worldObj, posX, posY, posZ, 0.0, 0.0, 0.0, isShiny) : particle.particleClass.getConstructor(World.class, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE).newInstance(worldObj, posX, posY, posZ, 0.0, 0.0, 0.0);
            Minecraft.getMinecraft().effectRenderer.addEffect(fx);
        }
        catch (Exception var10) {
            var10.printStackTrace();
        }
    }

    public static void spawnParticle(EnumBreedingParticles particle, World worldObj, double posX, double posY, double posZ) {
        try {
            ParticleBreeding fx = new ParticleBreeding(worldObj, posX, posY, posZ, 0.0, 0.0, 0.0, particle);
            Minecraft.getMinecraft().effectRenderer.addEffect(fx);
        }
        catch (Exception var10) {
            var10.printStackTrace();
        }
    }

    public static void spawnParticle(String particleId, World worldObj, double posX, double posY, double posZ, int particleTint, int particleTintAlpha) {
        try {
            ParticleShiny fx = new ParticleShiny(particleId, particleTint, particleTintAlpha);
            Minecraft.getMinecraft().effectRenderer.addEffect(new ParticleArcanery(worldObj, posX, posY, posZ, 0.0, 0.0, 0.0, fx));
        }
        catch (Exception var10) {
            var10.printStackTrace();
        }
    }

    @Override
    public void registerTickHandlers() {
        MinecraftForge.EVENT_BUS.register(new InventoryDetectionTickHandler());
        MinecraftForge.EVENT_BUS.register(battleManager);
    }

    @Override
    public ClientPacketProcessing getClientPacketProcessor() {
        return this.cpp;
    }

    public static ResourceLocation bindPlayerTexture(String username) {
        if (!username.isEmpty() && !invaildSkins.contains(username)) {
            if (cachedSkins.containsKey(username)) {
                return cachedSkins.get(username);
            }
            GameProfile profile = new GameProfile(null, username);
            if ((profile = TileEntitySkull.updateGameProfile((GameProfile)profile)).isComplete() && profile.getId().version() == 4 && profile.getProperties().containsKey((Object)"textures")) {
                ResourceLocation resourcelocation;
                Minecraft minecraft = Minecraft.getMinecraft();
                Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> map = minecraft.getSkinManager().loadSkinFromCache(profile);
                if (map.containsKey((Object)MinecraftProfileTexture.Type.SKIN)) {
                    resourcelocation = minecraft.getSkinManager().loadSkin(map.get((Object)MinecraftProfileTexture.Type.SKIN), MinecraftProfileTexture.Type.SKIN);
                    cachedSkins.put(username, resourcelocation);
                } else {
                    resourcelocation = DefaultPlayerSkin.getDefaultSkinLegacy();
                }
                return resourcelocation;
            }
            invaildSkins.add(username);
            return DefaultPlayerSkin.getDefaultSkinLegacy();
        }
        return DefaultPlayerSkin.getDefaultSkinLegacy();
    }

    @Override
    public boolean resourceLocationExists(ResourceLocation resourceLocation) {
        try {
            if (TEXTURE_STORE.getObject(this.getTextureName(resourceLocation)) != null) {
                return true;
            }
            return Minecraft.getMinecraft().getResourceManager().getResource(resourceLocation).getInputStream() != null;
        }
        catch (IOException var3) {
            return false;
        }
    }

    public String getTextureName(ResourceLocation resourceLocation) {
        String[] splits = resourceLocation.getPath().split("/");
        return splits[splits.length - 1].replaceAll(".png", "");
    }

    @Override
    public BufferedInputStream getStreamForResourceLocation(ResourceLocation resourceLocation) {
        try {
            return new BufferedInputStream(Minecraft.getMinecraft().getResourceManager().getResource(resourceLocation).getInputStream());
        }
        catch (IOException var3) {
            throw new RuntimeException(var3);
        }
    }

    @Override
    public void resetMouseOver() {
        Minecraft.getMinecraft().objectMouseOver = null;
    }

    @Override
    public void startManifestDownload(SyncManifest.AssetManifestType type, String manifest) {
        Timer timer = new Timer(type.name().toLowerCase() + "_manifest_download");
        TimerTask timerTask = null;
        switch (type) {
            case TEXTURE: {
                TextureTask task = new TextureTask();
                TextureManifest textureManifest = new TextureManifest();
                textureManifest.loadFromJson(manifest);
                task.formBuffer(textureManifest);
                timerTask = task;
                break;
            }
            case VALVE_MODEL: {
                ValveModelTask task = new ValveModelTask();
                ValveModelManifest modelManifest = new ValveModelManifest();
                modelManifest.loadFromJson(manifest);
                task.formBuffer(modelManifest);
                timerTask = task;
                break;
            }
            case SOUND: {
                SoundTask task = new SoundTask();
                SoundManifest soundManifest = new SoundManifest();
                soundManifest.loadFromJson(manifest);
                task.formBuffer(soundManifest);
                timerTask = task;
                break;
            }
            case MINECRAFT_MODEL: {
                MinecraftModelTask task = new MinecraftModelTask();
                MinecraftModelManifest modelManifest = new MinecraftModelManifest();
                modelManifest.loadFromJson(manifest);
                task.formBuffer(modelManifest);
                timerTask = task;
                break;
            }
        }
        if (timerTask != null) {
            timer.scheduleAtFixedRate(timerTask, 1000L, 1000L);
        }
    }

    @Override
    public void setPokedexSpawns(String spawns, String times) {
        if (Minecraft.getMinecraft().currentScreen instanceof GuiPokedexInfo) {
            GuiPokedexInfo.serverBiomes = spawns;
            GuiPokedexInfo.serverTimes = times;
        }
    }

    @Override
    public void openToast(String title, String body, ItemStack itemStack, PixelmonFrameType frameType) {
        Minecraft.getMinecraft().getToastGui().add(new PixelmonToast(title, body, itemStack, frameType));
    }

    @Override
    public void sendToServer(String name, String ip) {
        Minecraft.getMinecraft().addScheduledTask(() -> {
            try {
                Minecraft mc = Minecraft.getMinecraft();
                mc.world.sendQuittingDisconnectingPacket();
                mc.loadWorld(null);
                mc.displayGuiScreen(new GuiConnecting(new GuiMultiplayer(new GuiMainMenu()), Minecraft.getMinecraft(), new ServerData(name, ip, false)));
            }
            catch (Exception exception) {
                // empty catch block
            }
        });
    }

    @Override
    public void openBigImage(OpenBigImageGui packet) {
        Minecraft.getMinecraft().displayGuiScreen(new GuiBigImage(packet.texture, packet.width, packet.height));
    }

    @Override
    public void updateBarPercent(float percent) {
        ServerBarOverlay.progress = percent;
    }

    static {
        cachedSkins = new ConcurrentHashMap();
        invaildSkins = new ArrayList<String>();
        TEXTURE_STORE = new TextureStore();
        VALVE_MODEL_STORE = new ValveModelStore();
        MINECRAFT_MODEL_STORE = new MinecraftModelStore();
        RESOURCE_MANAGEMENT_TASK = new ResourceManagementTask();
        CULLING_THREAD = new CullingThread();
    }
}

