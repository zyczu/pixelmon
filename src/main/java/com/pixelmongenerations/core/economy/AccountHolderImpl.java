/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.economy;

import com.pixelmongenerations.api.economy.AccountHolder;
import com.pixelmongenerations.api.economy.Transaction;
import java.util.UUID;

public class AccountHolderImpl
implements AccountHolder {
    private long balance = 0L;

    @Override
    public int getBalance() {
        return (int)this.balance;
    }

    @Override
    public Transaction withdraw(int withdrawAmount) {
        if (withdrawAmount < 1) {
            return new Transaction(withdrawAmount, this.getBalance(), Transaction.ResponseType.FAILURE, "Withdraw amount was less than one.");
        }
        if ((long)withdrawAmount > this.balance) {
            return new Transaction(withdrawAmount, this.getBalance(), Transaction.ResponseType.FAILURE, "Withdraw amount was greater than balance.");
        }
        int updatedBalance = this.getBalance() - withdrawAmount;
        this.balance = updatedBalance;
        return new Transaction(withdrawAmount, updatedBalance, Transaction.ResponseType.SUCCESS, null);
    }

    @Override
    public Transaction deposit(int depositAmount) {
        if (depositAmount < 1) {
            return new Transaction(depositAmount, this.getBalance(), Transaction.ResponseType.FAILURE, "Deposit amount was less than one.");
        }
        long updatedBalance = this.balance + (long)depositAmount;
        if (updatedBalance > Integer.MAX_VALUE) {
            updatedBalance = Integer.MAX_VALUE;
        }
        this.balance = updatedBalance;
        return new Transaction(depositAmount, (int)updatedBalance, Transaction.ResponseType.SUCCESS, null);
    }

    @Override
    public Transaction set(int updatedBalance) {
        if (updatedBalance < 1) {
            return new Transaction(updatedBalance, this.getBalance(), Transaction.ResponseType.FAILURE, "Updated balance was less than one.");
        }
        this.balance = updatedBalance;
        return new Transaction(updatedBalance, updatedBalance, Transaction.ResponseType.SUCCESS, null);
    }

    @Override
    public void setupAccount(UUID uuid) {
    }
}

