/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.util;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumShinyItem;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;

public class PixelmonMethods {
    public static ArrayList<EntityPixelmon> getAllActivePokemon(EntityPlayer player) {
        ArrayList<EntityPixelmon> list = new ArrayList<EntityPixelmon>(6);
        Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
        if (storageOpt.isPresent()) {
            PlayerStorage storage = storageOpt.get();
            for (NBTTagCompound nbt : storage.getList()) {
                Optional<EntityPixelmon> pixelmonOptional;
                if (nbt == null || !(pixelmonOptional = storage.getAlreadyExists(PixelmonMethods.getID(nbt), player.world)).isPresent()) continue;
                list.add(pixelmonOptional.get());
            }
        }
        return list;
    }

    public static boolean isWearingShinyCharm(EntityPlayerMP player) {
        Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        return storageOpt.filter(playerStorage -> playerStorage.shinyData.getShinyItem() == EnumShinyItem.ShinyCharm).isPresent();
    }

    public static boolean isCatchComboSpecies(EntityPlayerMP player, EnumSpecies species) {
        Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (storageOpt.isPresent()) {
            if (storageOpt.get().getCatchStreak() < 11) {
                return true;
            }
            Optional<EnumSpecies> speciesOpt = storageOpt.get().getCatchComboSpecies();
            if (speciesOpt.isPresent()) {
                return speciesOpt.get() == species;
            }
        }
        return false;
    }

    public static float getCatchComboChance(EntityPlayerMP player) {
        boolean wearingCharm = PixelmonMethods.isWearingShinyCharm(player);
        Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        return storageOpt.map(playerStorage -> Float.valueOf(playerStorage.getCatchCombo().getShinyRate(wearingCharm))).orElseGet(() -> Float.valueOf(PixelmonConfig.shinyRate)).floatValue();
    }

    public static void retrieveAllPokemon(EntityPlayerMP player) {
        ArrayList<EntityPixelmon> activePokemon = PixelmonMethods.getAllActivePokemon(player);
        Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (storageOpt.isPresent()) {
            storageOpt.get();
            for (EntityPixelmon pokemon : activePokemon) {
                pokemon.catchInPokeball();
            }
        }
    }

    public static int[] getID(NBTTagCompound nbt) {
        return new int[]{nbt.getInteger("pixelmonID1"), nbt.getInteger("pixelmonID2")};
    }

    public static boolean isIDSame(NBTTagCompound nbt, EntityPixelmon entity) {
        return PixelmonMethods.isIDSame(PixelmonMethods.getID(nbt), entity.getPokemonId());
    }

    public static boolean isIDSame(NBTTagCompound nbt, PixelmonWrapper pw) {
        return PixelmonMethods.isIDSame(PixelmonMethods.getID(nbt), pw.getPokemonID());
    }

    public static boolean isIDSame(int[] id1, int[] id2) {
        if (id1 == null || id2 == null) {
            return false;
        }
        return (id1.length == 2 || id1.length == id2.length) && id1[0] == id2[0] && id1[1] == id2[1];
    }

    public static boolean isIDSame(NBTTagCompound nbt, int[] id) {
        return PixelmonMethods.isIDSame(PixelmonMethods.getID(nbt), id);
    }

    public static boolean isIDSame(EntityPixelmon e, int[] id) {
        return PixelmonMethods.isIDSame(e.getPokemonId(), id);
    }

    public static boolean isIDSame(int[] id, EntityPixelmon e) {
        return PixelmonMethods.isIDSame(e.getPokemonId(), id);
    }

    public static boolean isIDSame(int[] id, PokemonLink e) {
        return PixelmonMethods.isIDSame(e.getPokemonID(), id);
    }

    public static boolean isIDSame(EntityPixelmon e1, EntityPixelmon e2) {
        return PixelmonMethods.isIDSame(e1.getPokemonId(), e2.getPokemonId());
    }

    public static boolean isIDSame(PixelmonWrapper e1, PixelmonWrapper e2) {
        return PixelmonMethods.isIDSame(e1.getPokemonID(), e2.getPokemonID());
    }

    public static boolean isIDSame(PixelmonWrapper e1, int[] id2) {
        return PixelmonMethods.isIDSame(e1.getPokemonID(), id2);
    }

    public static boolean isIDSame(int[] id1, PixelmonWrapper e2) {
        return PixelmonMethods.isIDSame(id1, e2.getPokemonID());
    }

    public static void toBytesUUID(ByteBuf buf, UUID uuid) {
        buf.writeLong(uuid.getMostSignificantBits());
        buf.writeLong(uuid.getLeastSignificantBits());
    }

    public static UUID fromBytesUUID(ByteBuf buf) {
        return new UUID(buf.readLong(), buf.readLong());
    }

    public static void toBytesPokemonID(ByteBuf buf, int[] pokemonID) {
        buf.writeInt(pokemonID[0]);
        buf.writeInt(pokemonID[1]);
    }

    public static int[] fromBytesPokemonID(ByteBuf buf) {
        return new int[]{buf.readInt(), buf.readInt()};
    }

    public static void changeForm(int newForm, EntityLivingBase owner, Optional<NBTTagCompound> nbt, Optional<EntityPixelmon> pokemon) {
        if (pokemon.isPresent()) {
            pokemon.get().setForm(newForm, true);
        } else {
            EntityPixelmon entity = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(nbt.get(), owner.world);
            entity.setForm(newForm, true);
        }
        if (owner instanceof EntityPlayerMP) {
            Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)owner);
            storageOpt.ifPresent(PlayerStorage::sendUpdatedList);
        }
    }

    public static void changeForm(int form, EntityPlayer player, EntityPixelmon pokemon) {
        pokemon.setForm(form, true);
        if (player instanceof EntityPlayerMP) {
            Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
            storageOpt.ifPresent(PlayerStorage::sendUpdatedList);
        }
    }
}

