/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class Bounds {
    public int top;
    public int left;
    public int bottom;
    public int right;
    protected World world;

    public Bounds() {
        this.right = 0;
        this.bottom = 0;
        this.left = 0;
        this.top = 0;
    }

    public Bounds(int top, int left, int bottom, int right) {
        this.top = top;
        this.left = left;
        this.bottom = bottom;
        this.right = right;
    }

    public void writeToNBT(NBTTagCompound nbt) {
        nbt.setInteger("boundsTop", this.top);
        nbt.setInteger("boundsLeft", this.left);
        nbt.setInteger("boundsBottom", this.bottom);
        nbt.setInteger("boundsRight", this.right);
    }

    public void readFromNBT(NBTTagCompound nbt) {
        this.top = nbt.getInteger("boundsTop");
        this.left = nbt.getInteger("boundsLeft");
        this.bottom = nbt.getInteger("boundsBottom");
        this.right = nbt.getInteger("boundsRight");
    }

    public boolean isIn(Vec3d vec3) {
        if (vec3 == null) {
            return false;
        }
        return this.isIn(vec3.x, vec3.z);
    }

    public boolean isIn(double x, double z) {
        return x >= (double)this.left && x <= (double)this.right && z >= (double)this.bottom && z <= (double)this.top;
    }

    public void setWorldObj(World world) {
        this.world = world;
    }

    public boolean canExtend() {
        return false;
    }

    public boolean canExtend(int i, int j, int k, int l) {
        return false;
    }

    public void Extend(EntityPlayerMP playerEntity, int i, int j, int k, int l) {
    }
}

