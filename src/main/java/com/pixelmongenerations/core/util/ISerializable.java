/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.core.util;

import io.netty.buffer.ByteBuf;

public interface ISerializable<T> {
    public void serialize(ByteBuf var1);
}

