/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util;

public class PixelmonDebug {
    public static String listObjs(Object ... obs) {
        StringBuilder result = new StringBuilder("(");
        for (int i = 0; i < obs.length; ++i) {
            result.append(obs[i]);
            if (i + 1 == obs.length) continue;
            result.append(", ");
        }
        result.append(")");
        return result.toString();
    }

    public static void printArrayIn2D(Object[] obs, int width, int length) {
        System.out.println();
        System.out.println("2D array start");
        for (int j = 0; j < length; ++j) {
            Object[] row = new Object[width];
            System.arraycopy(obs, j * width, row, 0, width);
            System.out.println(PixelmonDebug.listObjs(row));
        }
        System.out.println("2D array end");
    }

    public static String prevMethod() {
        try {
            StackTraceElement[] elements = Thread.currentThread().getStackTrace();
            int i = Math.min(3, elements.length - 1);
            StackTraceElement stackElement = elements[i];
            return PixelmonDebug.describeStackElement(stackElement);
        }
        catch (Exception elements) {
            return "?";
        }
    }

    public static String describeStackElement(StackTraceElement stackElement) {
        try {
            return Class.forName(stackElement.getClassName()).getSimpleName() + "." + stackElement.getMethodName() + "(line " + stackElement.getLineNumber() + ")";
        }
        catch (ClassNotFoundException shouldntHappen) {
            return null;
        }
    }

    public static void printStackElements(int traceLines) {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        System.out.println("[Begin stack trace]");
        for (int i = 1; i <= traceLines && i < elements.length; ++i) {
            System.out.println(PixelmonDebug.describeStackElement(elements[i]));
        }
        System.out.println("[End stack Trace]");
    }

    public static boolean startsWithVowel(String string) {
        return string.matches("(?i)[aeiou].*");
    }

    public static String upperCaseFirstChar(String string) {
        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }
}

