/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util;

import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.PixelSounds;
import net.minecraft.client.audio.MusicTicker;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.common.util.EnumHelper;

public class PixelMusic {
    public static MusicTicker.MusicType TRACK_1;
    public static MusicTicker.MusicType TRACK_2;
    public static MusicTicker.MusicType TRACK_3;
    public static MusicTicker.MusicType TRACK_4;
    public static MusicTicker.MusicType TRACK_5;
    public static MusicTicker.MusicType TRACK_6;
    public static MusicTicker.MusicType TRACK_7;
    public static MusicTicker.MusicType TRACK_8;
    public static MusicTicker.MusicType TRACK_9;
    public static MusicTicker.MusicType TRACK_10;
    public static MusicTicker.MusicType TRACK_11;
    public static MusicTicker.MusicType BATTLE_1;
    public static MusicTicker.MusicType BATTLE_2;
    public static MusicTicker.MusicType BATTLE_3;
    public static MusicTicker.MusicType BATTLE_4;
    public static MusicTicker.MusicType BATTLE_5;
    public static MusicTicker.MusicType BATTLE_6;
    public static MusicTicker.MusicType BATTLE_7;
    public static MusicTicker.MusicType BATTLE_8;
    public static MusicTicker.MusicType BATTLE_9;
    public static MusicTicker.MusicType BATTLE_10;
    public static MusicTicker.MusicType DAY;
    public static MusicTicker.MusicType NIGHT;

    public static void registerMusicTypes() {
        TRACK_1 = PixelMusic.createMusicType("track1", PixelSounds.track1);
        TRACK_2 = PixelMusic.createMusicType("track2", PixelSounds.track2);
        TRACK_3 = PixelMusic.createMusicType("track3", PixelSounds.track3);
        TRACK_4 = PixelMusic.createMusicType("track4", PixelSounds.track4);
        TRACK_5 = PixelMusic.createMusicType("track5", PixelSounds.track5);
        TRACK_6 = PixelMusic.createMusicType("track6", PixelSounds.track6);
        TRACK_7 = PixelMusic.createMusicType("track7", PixelSounds.track7);
        TRACK_8 = PixelMusic.createMusicType("track8", PixelSounds.track8);
        TRACK_9 = PixelMusic.createMusicType("track9", PixelSounds.track9);
        TRACK_10 = PixelMusic.createMusicType("track10", PixelSounds.track10);
        TRACK_11 = PixelMusic.createMusicType("track11", PixelSounds.track11);
        BATTLE_1 = PixelMusic.createMusicType("battle1", PixelSounds.battle1);
        BATTLE_2 = PixelMusic.createMusicType("battle2", PixelSounds.battle2);
        BATTLE_3 = PixelMusic.createMusicType("battle3", PixelSounds.battle3);
        BATTLE_4 = PixelMusic.createMusicType("battle4", PixelSounds.battle4);
        BATTLE_5 = PixelMusic.createMusicType("battle5", PixelSounds.battle5);
        BATTLE_6 = PixelMusic.createMusicType("battle6", PixelSounds.battle6);
        BATTLE_7 = PixelMusic.createMusicType("battle7", PixelSounds.battle7);
        BATTLE_8 = PixelMusic.createMusicType("battle8", PixelSounds.battle8);
        BATTLE_9 = PixelMusic.createMusicType("battle9", PixelSounds.battle9);
        BATTLE_10 = PixelMusic.createMusicType("battle10", PixelSounds.battle10);
        DAY = PixelMusic.createMusicType("day", PixelSounds.day, 3000, 6000);
        NIGHT = PixelMusic.createMusicType("night", PixelSounds.night, 3000, 6000);
    }

    public static MusicTicker.MusicType getCurrentBattleMusic() {
        switch (ClientProxy.battleManager.battleSong) {
            case Intense: {
                return BATTLE_1;
            }
            case Boss: {
                return BATTLE_2;
            }
            case Hurry: {
                return BATTLE_3;
            }
            case Focus: {
                return BATTLE_4;
            }
            case Calm: {
                return BATTLE_5;
            }
            case Optimistic: {
                return BATTLE_6;
            }
            case Fierce: {
                return BATTLE_7;
            }
            case Bit: {
                return BATTLE_8;
            }
            case Ultra: {
                return BATTLE_9;
            }
            case Legendary: {
                return BATTLE_10;
            }
        }
        return null;
    }

    private static MusicTicker.MusicType createMusicType(String name, SoundEvent event) {
        return PixelMusic.createMusicType(name, event, 0, 0);
    }

    private static MusicTicker.MusicType createMusicType(String name, SoundEvent event, int minDelay, int maxDelay) {
        return EnumHelper.addEnum(MusicTicker.MusicType.class, name, new Class[]{SoundEvent.class, Integer.TYPE, Integer.TYPE}, new Object[]{event, minDelay, maxDelay});
    }
}

