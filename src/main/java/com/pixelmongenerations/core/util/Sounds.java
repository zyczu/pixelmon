/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  net.minecraftforge.client.resource.IResourceType
 *  net.minecraftforge.client.resource.ISelectiveResourceReloadListener
 *  net.minecraftforge.client.resource.VanillaResourceType
 */
package com.pixelmongenerations.core.util;

import com.pixelmongenerations.core.config.PixelmonConfig;
import java.util.function.Predicate;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.resource.IResourceType;
import net.minecraftforge.client.resource.ISelectiveResourceReloadListener;
import net.minecraftforge.client.resource.VanillaResourceType;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(value=Side.CLIENT)
public class Sounds
implements ISelectiveResourceReloadListener {
    public void onResourceManagerReload(IResourceManager resourceManager, Predicate<IResourceType> resourcePredicate) {
        if (resourcePredicate.test((IResourceType)VanillaResourceType.SOUNDS)) {
            Sounds.generateSounds();
        }
    }

    public static void generateSounds() {
        SoundHandler soundHandler = Minecraft.getMinecraft().getSoundHandler();
        if (PixelmonConfig.allowVanillaMusic) {
            soundHandler.getAccessor(new ResourceLocation("pixelmon", "pixelmon.music.day")).addSound(soundHandler.getAccessor(new ResourceLocation("music.creative")));
            soundHandler.getAccessor(new ResourceLocation("pixelmon", "pixelmon.music.night")).addSound(soundHandler.getAccessor(new ResourceLocation("music.game")));
        }
    }
}

