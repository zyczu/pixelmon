/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util;

import net.minecraft.entity.player.EntityPlayerMP;

public class AirSaver {
    private EntityPlayerMP player;
    private int startAir;

    public AirSaver(EntityPlayerMP player) {
        this.player = player;
        this.startAir = Math.max(1, player.getAir());
    }

    public void tick() {
        this.player.setAir(this.startAir);
    }
}

