/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util;

import java.time.Instant;
import java.util.HashMap;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayer;

public class SpawnMethodCooldowns {
    private static HashMap<UUID, Instant> spawnCooldowns = new HashMap();

    public static boolean isOnCooldown(EntityPlayer player) {
        Instant cooldown = spawnCooldowns.get(player.getUniqueID());
        if (cooldown == null) {
            return false;
        }
        if (!Instant.now().isAfter(cooldown)) {
            return true;
        }
        spawnCooldowns.remove(player.getUniqueID());
        return false;
    }

    public static void addCooldown(EntityPlayer player, int ticks) {
        spawnCooldowns.put(player.getUniqueID(), Instant.now().plusSeconds(ticks / 20));
    }

    public static void clearCooldown(EntityPlayer player) {
        spawnCooldowns.remove(player.getUniqueID());
    }
}

