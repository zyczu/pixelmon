/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util;

import com.pixelmongenerations.core.util.MoveCostListEntry;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;

public class MoveCostList {
    public static List<MoveCostListEntry> list = new ArrayList<MoveCostListEntry>();

    public static void addToList(EntityPlayerMP player, ItemStack item) {
        MoveCostListEntry entry;
        while ((entry = MoveCostList.getEntry(player)) != null) {
            list.remove(entry);
        }
        ArrayList<ItemStack> items = new ArrayList<ItemStack>(1);
        items.add(item);
        list.add(new MoveCostListEntry(player, items));
    }

    public static void addToList(EntityPlayerMP player, List<ItemStack> items) {
        MoveCostListEntry entry;
        while ((entry = MoveCostList.getEntry(player)) != null) {
            list.remove(entry);
        }
        list.add(new MoveCostListEntry(player, items));
    }

    public static boolean listContains(EntityPlayerMP player) {
        for (MoveCostListEntry entry : list) {
            if (!entry.player.getUniqueID().equals(player.getUniqueID())) continue;
            return true;
        }
        return false;
    }

    public static MoveCostListEntry getEntry(EntityPlayerMP player) {
        for (MoveCostListEntry entry : list) {
            if (!entry.player.getUniqueID().equals(player.getUniqueID())) continue;
            return entry;
        }
        return null;
    }

    public static boolean checkEntry(EntityPlayerMP player) {
        if (MoveCostList.listContains(player)) {
            MoveCostListEntry entry = MoveCostList.getEntry(player);
            list.remove(entry);
            if (entry.items.size() == 1) {
                ItemStack cost = entry.items.get(0);
                return MoveCostList.takePayment(cost, player.inventory);
            }
            ArrayList<ItemStack> paid = new ArrayList<ItemStack>();
            for (ItemStack cost : entry.items) {
                boolean result = MoveCostList.takePayment(cost, player.inventory);
                if (result) {
                    paid.add(cost);
                    continue;
                }
                paid.forEach(item -> player.inventory.addItemStackToInventory((ItemStack)item));
                return false;
            }
        }
        return true;
    }

    public static void removeEntry(EntityPlayerMP player) {
        list.remove(MoveCostList.getEntry(player));
    }

    private static boolean takePayment(ItemStack cost, InventoryPlayer inventory) {
        for (int i = 0; i < inventory.mainInventory.size(); ++i) {
            ItemStack playerItem = inventory.mainInventory.get(i);
            if (playerItem.isEmpty() || cost.getItem() != playerItem.getItem() || cost.getCount() > playerItem.getCount() || cost.getItemDamage() != playerItem.getItemDamage()) continue;
            if (playerItem.getCount() == cost.getCount()) {
                playerItem = ItemStack.EMPTY;
            } else {
                playerItem.setCount(playerItem.getCount() - cost.getCount());
            }
            inventory.setInventorySlotContents(i, playerItem);
            inventory.markDirty();
            return true;
        }
        return false;
    }
}

