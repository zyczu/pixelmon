/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util.helper;

import java.util.ArrayList;
import java.util.Optional;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.structure.StructureBoundingBox;

public class WorldHelper {
    public static final EnumFacing[] NWSE = new EnumFacing[]{EnumFacing.NORTH, EnumFacing.WEST, EnumFacing.SOUTH, EnumFacing.EAST};

    public static boolean isHorizontal(EnumFacing dir) {
        return dir.getXOffset() != 0;
    }

    public static Optional<Biome> demandBiome(ResourceLocation name) {
        return WorldHelper.parseBiome(name);
    }

    public static Optional<Biome> parseBiome(ResourceLocation name) {
        for (Biome biome : Biome.REGISTRY) {
            if (biome == null || biome.getRegistryName() == null || !biome.getRegistryName().equals(name)) continue;
            return Optional.of(biome);
        }
        return Optional.empty();
    }

    public static ArrayList<EnumFacing> getDirectionsTowards(int distX, int distZ) {
        EnumFacing leftRight = distX == 0 ? null : (distX < 0 ? EnumFacing.WEST : EnumFacing.EAST);
        EnumFacing upDown = distZ == 0 ? null : (distZ < 0 ? EnumFacing.NORTH : EnumFacing.SOUTH);
        ArrayList<EnumFacing> result = new ArrayList<EnumFacing>(2);
        if (leftRight != null) {
            result.add(leftRight);
        }
        if (upDown != null) {
            result.add(upDown);
        }
        return result;
    }

    public static int firstBlockDownwardsFromY(World world, BlockPos pos, boolean countNonSolid) {
        Block block;
        while (!(block = world.getBlockState(pos).getBlock()).equals(Blocks.AIR) || !countNonSolid && !block.isPassable(world, pos)) {
            pos = pos.down();
        }
        return pos.getY();
    }

    public static int getWaterDepth(BlockPos pos, World worldObj) {
        int count = 0;
        while (worldObj.getBlockState(pos).getBlock() == Blocks.WATER) {
            pos = pos.up();
            ++count;
        }
        return count;
    }

    public static boolean isWaterOrIce(World world, BlockPos pos) {
        Block block = world.getBlockState(pos).getBlock();
        return block == Blocks.WATER || block == Blocks.ICE;
    }

    public static void fixLighting(World world, StructureBoundingBox bb) {
        for (int i = bb.minX >> 4; i <= bb.maxX >> 4; ++i) {
            for (int j = bb.minZ >> 4; j <= bb.maxZ >> 4; ++j) {
                Chunk chunk = world.getChunk(i, j);
                chunk.generateSkylightMap();
                chunk.resetRelightChecks();
            }
        }
    }
}

