/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.util.vector.Matrix4f
 *  org.lwjgl.util.vector.Vector3f
 *  org.lwjgl.util.vector.Vector4f
 */
package com.pixelmongenerations.core.util.helper;

import java.lang.reflect.Field;
import net.minecraft.util.math.Vec3d;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

public abstract class VectorHelper {
    static final Vector3f X_AXIS = new Vector3f(1.0f, 0.0f, 0.0f);
    static final Vector3f Y_AXIS = new Vector3f(0.0f, 1.0f, 0.0f);
    static final Vector3f Z_AXIS = new Vector3f(0.0f, 0.0f, 1.0f);
    public static final float toDegrees = 57.29578f;
    public static final float toRadians = (float)Math.PI / 180;

    public static Vec3d add(Vec3d one, Vec3d other) {
        return one.add(other.x, other.y, other.z);
    }

    public static double[] rotate(double x, double y, double radians) {
        double cos = Math.cos(radians);
        double sin = Math.sin(radians);
        double newX = x * cos - y * sin;
        double newY = y * cos + x * sin;
        return new double[]{newX, newY};
    }

    public static Vector3f createOrNull(Number x, Number y, Number z) {
        if (x == null || y == null || z == null) {
            return null;
        }
        return new Vector3f(x.floatValue(), y.floatValue(), z.floatValue());
    }

    public static void print(Vec3d target) {
        System.out.println("Vector x = " + target.x);
        System.out.println("Vector y = " + target.y);
        System.out.println("Vector z = " + target.z);
    }

    public static void print(Object target) {
        System.out.println(target);
    }

    public static void printAlternate(Matrix4f target) {
        Field[] fields = target.getClass().getFields();
        System.out.println("MATRIX DATA");
        System.out.println("~~~Standard Print~~~");
        VectorHelper.print((Object)target);
        System.out.println("~~~In-Depth Print~~~");
        for (Field f : fields) {
            String descript = f.getName() + " = ";
            try {
                descript = descript + f.getFloat((Object)target);
            }
            catch (Exception e) {
                descript = descript + "ERROR";
            }
            System.out.println(descript);
        }
    }

    public static Matrix4f matrix4FromLocRot(float xl, float yl, float zl, float xr, float yr, float zr) {
        Vector3f loc = new Vector3f(xl, yl, zl);
        Matrix4f part1 = new Matrix4f();
        part1.translate(loc);
        part1.rotate(zr, Z_AXIS);
        part1.rotate(yr, Y_AXIS);
        part1.rotate(xr, X_AXIS);
        return part1;
    }

    public static Matrix4f matrix4FromFloatArray(float[] vals) {
        return VectorHelper.matrix4FromLocRot(vals[0], vals[1], vals[2], vals[3], vals[4], vals[5]);
    }

    public static Matrix4f matrix4fFromFloat(float val) {
        return VectorHelper.matrix4FromLocRot(val, val, val, val, val, val);
    }

    public static Vector4f mul(Vector4f target, float factor, Vector4f dest) {
        if (dest == null) {
            dest = new Vector4f();
        }
        dest.x = target.x * factor;
        dest.y = target.y * factor;
        dest.z = target.z * factor;
        dest.w = target.w * factor;
        return dest;
    }

    public static Matrix4f mul(Matrix4f target, float factor, Matrix4f dest) {
        if (dest == null) {
            dest = new Matrix4f();
        }
        dest.m00 = target.m00 * factor;
        dest.m01 = target.m01 * factor;
        dest.m02 = target.m03 * factor;
        dest.m10 = target.m10 * factor;
        dest.m11 = target.m11 * factor;
        dest.m12 = target.m12 * factor;
        dest.m13 = target.m13 * factor;
        dest.m20 = target.m20 * factor;
        dest.m21 = target.m21 * factor;
        dest.m22 = target.m22 * factor;
        dest.m23 = target.m23 * factor;
        dest.m30 = target.m30 * factor;
        dest.m31 = target.m31 * factor;
        dest.m32 = target.m32 * factor;
        dest.m33 = target.m33 * factor;
        return target;
    }

    public static float[] getLoc(Matrix4f target) {
        return new float[]{target.m30, target.m31, target.m32};
    }

    public static Vector3f v3LocFromM4(Matrix4f target) {
        return new Vector3f(target.m30, target.m31, target.m32);
    }

    public static Vector3f getInverse(Vector3f target) {
        return new Vector3f(-target.x, -target.y, -target.z);
    }

    public static Vector3f Vec3dfFromStrings(String x, String y, String z) {
        float xl = Float.parseFloat(x);
        float yl = Float.parseFloat(y);
        float zl = Float.parseFloat(z);
        return new Vector3f(xl, yl, zl);
    }

    public static Vector4f copyVector4f(Vector4f src) {
        return new Vector4f(src.x, src.y, src.z, src.w);
    }
}

