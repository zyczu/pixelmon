/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.BufferUtils
 */
package com.pixelmongenerations.core.util.helper;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import javax.imageio.ImageIO;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.BufferUtils;

public class ImageHelper {
    private static String LOAD_ERROR = "There was an error loading the %s \"%s\".";

    public static BufferedImage getImage(String fileLoc) throws IOException {
        try {
            fileLoc = fileLoc.replaceAll("//", "/");
            InputStream in = ImageHelper.class.getResourceAsStream(fileLoc);
            assert (in != null);
            return ImageIO.read(in);
        }
        catch (IOException e) {
            throw new IOException(String.format(LOAD_ERROR, "file", fileLoc));
        }
    }

    public static BufferedImage getImage(ResourceLocation resLoc) throws IOException {
        try {
            InputStream in = Minecraft.getMinecraft().getResourceManager().getResource(resLoc).getInputStream();
            return ImageIO.read(in);
        }
        catch (IOException e) {
            throw new IOException(String.format(LOAD_ERROR, "ResourceLocation", resLoc));
        }
    }

    public static BufferedImage rotateImg(BufferedImage img, double rotation) {
        double midX = (double)img.getWidth() * 0.5;
        double midY = (double)img.getHeight() * 0.5;
        double radians = Math.toRadians(rotation);
        AffineTransform trans = new AffineTransform();
        trans.rotate(radians, midX, midY);
        AffineTransformOp op = new AffineTransformOp(trans, 1);
        return op.filter(img, null);
    }

    public static ByteBuffer getBuffer(BufferedImage img) {
        int width = img.getWidth();
        int height = img.getHeight();
        int[] pixels = new int[img.getWidth() * img.getHeight()];
        img.getRGB(0, 0, width, height, pixels, 0, width);
        ByteBuffer buffer = BufferUtils.createByteBuffer((int)(width * height * 4));
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                int pixel = pixels[y * width + x];
                buffer.put((byte)(pixel >> 16 & 0xFF));
                buffer.put((byte)(pixel >> 8 & 0xFF));
                buffer.put((byte)(pixel & 0xFF));
                buffer.put((byte)(pixel >> 24 & 0xFF));
            }
        }
        buffer.flip();
        buffer.rewind();
        return buffer;
    }
}

