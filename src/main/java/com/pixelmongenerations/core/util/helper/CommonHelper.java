/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.primitives.Primitives
 */
package com.pixelmongenerations.core.util.helper;

import com.google.common.primitives.Primitives;
import com.pixelmongenerations.core.util.RegexPatterns;
import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.registry.RegistryNamespaced;

public class CommonHelper {
    private static Field NBTMapField;
    private static Field NBTListField;
    private static Field ItemRegistryMapField;

    public static void ensureIndex(ArrayList a, int i) {
        while (a.size() <= i) {
            a.add(null);
        }
    }

    public static <T> T set(ArrayList<T> a, T value, int index) {
        if (a.size() <= index) {
            a.add(index, value);
            return null;
        }
        return a.set(index, value);
    }

    public static <T> void insert(ArrayList<T> a, T value) {
        int nullIndex = a.indexOf(null);
        if (nullIndex == -1) {
            a.add(value);
        } else {
            a.set(nullIndex, value);
        }
    }

    public static <E> Map<String, E> getMap(NBTTagCompound nbt) {
        try {
            return (Map)NBTMapField.get(nbt);
        }
        catch (Exception e) {
            System.err.println(nbt);
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<NBTBase> getList(NBTTagList nbt) {
        try {
            return (ArrayList)NBTListField.get(nbt);
        }
        catch (Exception e) {
            System.err.println(nbt);
            System.err.println(NBTListField);
            e.printStackTrace();
            return null;
        }
    }

    public static int indexOfAbsoluteMax(Object array) throws ClassCastException {
        Number n;
        int i;
        int index = -1;
        int maxes = 0;
        Double max = null;
        for (i = 0; i < Array.getLength(array); ++i) {
            n = (Number)Array.get(array, i);
            if (max != null && n.doubleValue() <= max) continue;
            max = n.doubleValue();
        }
        for (i = 0; i < Array.getLength(array); ++i) {
            n = (Number)Array.get(array, i);
            if (n.doubleValue() == max.doubleValue()) {
                ++maxes;
                index = i;
            }
            if (maxes <= 1) continue;
            return -1;
        }
        return index;
    }

    public static Object[] wrapperArray(Object aprimitive) {
        if (!aprimitive.getClass().isArray()) {
            throw new IllegalArgumentException("The variable 'primitiveArray' must ACTUALLY BE AN ARRAY!");
        }
        Class cl = aprimitive.getClass().getComponentType();
        if (!cl.isPrimitive()) {
            return (Object[])aprimitive;
        }
        cl = Primitives.wrap(cl);
        Object awrapper = Array.newInstance(cl, Array.getLength(aprimitive));
        for (int i = 0; i < Array.getLength(aprimitive); ++i) {
            Array.set(awrapper, i, Array.get(aprimitive, i));
        }
        return (Object[])awrapper;
    }

    public static <K, V> Map<K, V> addEntries(Map<K, V> map, K[] keys, V[] vals) {
        for (int i = 0; i < keys.length; ++i) {
            map.put(keys[i], vals[i]);
        }
        return map;
    }

    public static <K, V> Map<K, V> addEntries(Map<K, V> map, Object[] kvs) {
        for (int i = 0; i < kvs.length / 2; ++i) {
            map.put((K) kvs[i * 2], (V) kvs[i * 2 + 1]);
        }
        return map;
    }

    public static String[] getLinesInCurlyBracketBlock(BufferedReader reader, String line, String regexCriteria) {
        ArrayList<String> params = new ArrayList<String>();
        try {
            if (line == null) {
                line = reader.readLine();
            }
            while (line != null && !line.contains("{")) {
                line = reader.readLine();
            }
            while (line != null) {
                if (line.matches(regexCriteria)) {
                    params.add(line);
                }
                if (!line.contains("}")) {
                    line = reader.readLine();
                    continue;
                }
                break;
            }
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        return params.toArray(new String[params.size()]);
    }

    public static <V> V getIgnoreCase(Map<String, V> map, String key) {
        if (key == null) {
            return map.get(null);
        }
        for (Map.Entry<String, V> entry : map.entrySet()) {
            if (!key.equalsIgnoreCase(entry.getKey())) continue;
            return entry.getValue();
        }
        return null;
    }

    public static String textInQuotes(String str) {
        return RegexPatterns.REMOVEQUOTES.matcher(str).replaceAll("$1").trim();
    }

    public static String spaceBetweenCaps(String str) {
        return String.join((CharSequence)" ", str.split("(?<=.)(?=\\p{Lu})"));
    }

    static {
        for (Field f : NBTTagCompound.class.getDeclaredFields()) {
            if (!Map.class.isAssignableFrom(f.getType())) continue;
            NBTMapField = f;
            NBTMapField.setAccessible(true);
            break;
        }
        for (Field f : NBTTagList.class.getDeclaredFields()) {
            if (!List.class.isAssignableFrom(f.getType())) continue;
            NBTListField = f;
            NBTListField.setAccessible(true);
            break;
        }
        for (Field f : RegistryNamespaced.class.getDeclaredFields()) {
            if (!Map.class.isAssignableFrom(f.getType())) continue;
            ItemRegistryMapField = f;
            ItemRegistryMapField.setAccessible(true);
            break;
        }
    }
}

