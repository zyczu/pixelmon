/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util.helper;

import java.util.HashMap;

public class GeometryHelper {
    public static final double PHI = 1.618033988749895;

    public static double logistic(double x, double a, double b, double c) {
        return c / (1.0 + a * Math.pow(Math.E, -b * x));
    }

    public static double angleDeg(double xD, double yD) {
        return Math.toDegrees(Math.atan2(yD, xD));
    }

    public static double sin(double x, double a, double shift, double scale) {
        return scale * Math.sin((float)((x + shift) / a * Math.PI));
    }

    public static double slider(double x, double min, double max) {
        return GeometryHelper.clampDouble((x - min) / (max - min), 0.0, 1.0);
    }

    public static double inverseSlider(double x, double min, double max) {
        return x * (max - min) + min;
    }

    public static double lineSlider(double x, double x1, double x2, double y1, double y2) {
        return GeometryHelper.inverseSlider(GeometryHelper.slider(x, x1, x2), y1, y2);
    }

    public static double clampDouble(double in, double min, double max) {
        return in < min ? min : Math.min(in, max);
    }

    public static int even(int i) {
        return i & 0xFFFFFFFE;
    }

    public static double perfectSin(double x, double length, double minIN, double maxIN) {
        System.out.print("Min = " + minIN + ", max = " + maxIN);
        double input = GeometryHelper.slider(x, minIN, maxIN);
        double result = GeometryHelper.sin(input, length, length * 0.5, 1.0);
        System.out.println(", In = " + x + ", input = " + input + ", result = " + result);
        return result;
    }

    public static double dist(double x1, double y1, double x2, double y2) {
        return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }

    public static double dist(double x, double y, double z) {
        return Math.sqrt(x * x + y * y + z * z);
    }

    public static HashMap<Integer, HashMap<Integer, Double>> weirdBlur(HashMap<Integer, HashMap<Integer, Double>> points, int radius) {
        for (Integer i : points.keySet()) {
            HashMap<Integer, Double> column = points.get(i);
            for (Integer j : column.keySet()) {
                double average = 0.0;
                float div = 0.0f;
                for (int x = -radius; x <= radius; ++x) {
                    for (int y = -radius; y <= radius; ++y) {
                        Double add;
                        div += 1.0f;
                        try {
                            add = points.get(i + x).get(j + y);
                        }
                        catch (NullPointerException e) {
                            add = 0.0;
                        }
                        average += add != null ? add : 0.0;
                    }
                }
                System.out.println(average);
                points.get(i).put(j, average / (double)div);
            }
        }
        return points;
    }

    public static float cap(float number, float min, float max) {
        if (min > max) {
            throw new IllegalArgumentException("Min must be less than max.");
        }
        if (number < min) {
            return min;
        }
        return Math.min(number, max);
    }
}

