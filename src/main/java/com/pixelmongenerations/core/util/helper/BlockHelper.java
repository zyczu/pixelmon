/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Maps
 */
package com.pixelmongenerations.core.util.helper;

import com.google.common.collect.Maps;
import com.pixelmongenerations.core.Pixelmon;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;

public class BlockHelper {
    public static <T extends TileEntity> T findClosestTileEntity(Class<T> tileEntity, Entity entity, double range, Predicate<T> predicate) {
        Map<BlockPos, T> map = BlockHelper.findAllTileEntityWithinRange(tileEntity, entity, range, predicate);
        if (map.size() == 0) {
            return null;
        }
        Map.Entry<BlockPos, T> closest = null;
        double distance = range + 0.1;
        for (Map.Entry<BlockPos, T> entry : map.entrySet()) {
            double dis = BlockHelper.getDistance(entity, entry.getKey());
            if (dis >= distance) continue;
            closest = entry;
            distance = dis;
        }
        return (T)(closest == null ? null : (TileEntity)closest.getValue());
    }

    public static <T extends TileEntity> Map<BlockPos, T> findAllTileEntityWithinRange(Class<T> tileEntity, Entity entity, double range, Predicate<T> predicate) {
        int chunkXPos = entity.getPosition().getX() >> 4;
        int chunkZPos = entity.getPosition().getZ() >> 4;
        WorldServer world = (WorldServer)entity.getEntityWorld();
        int chunkRange = Math.max((int)(range / 16.0), 1) + 1;
        HashMap map = Maps.newHashMap();
        for (int x = chunkXPos - chunkRange + 1; x < chunkXPos + chunkRange; ++x) {
            for (int z = chunkZPos - chunkRange + 1; z < chunkZPos + chunkRange; ++z) {
                if (!world.getChunkProvider().chunkExists(x, z)) continue;
                Chunk chunk = world.getChunk(x, z);
                for (Map.Entry<BlockPos, TileEntity> entry : chunk.getTileEntityMap().entrySet()) {
                    if (!tileEntity.isAssignableFrom(entry.getValue().getClass()) || BlockHelper.getDistance(entity, entry.getKey()) > range || !predicate.test((T) entry.getValue())) continue;
                    map.put(entry.getKey(), entry.getValue());
                }
            }
        }
        return map;
    }

    public static int countTileEntitiesOfType(World world, ChunkPos pos, Class<? extends TileEntity> clazz) {
        int count = 0;
        Chunk chunk = world.getChunk(pos.x, pos.z);
        for (TileEntity te : chunk.getTileEntityMap().values()) {
            if (!clazz.isInstance(te)) continue;
            ++count;
        }
        return count;
    }

    public static <T extends TileEntity> T getTileEntity(Class<T> clazz, IBlockAccess world, BlockPos pos) {
        TileEntity te = world.getTileEntity(pos);
        if (te != null && !clazz.isInstance(te)) {
            te.invalidate();
            te = world.getTileEntity(pos);
            if (te != null && !clazz.isInstance(te)) {
                te.invalidate();
                Pixelmon.LOGGER.info("Bad TileEntity " + pos + " expected " + clazz.getSimpleName() + " got " + te.getClass().getSimpleName());
                return null;
            }
            if (te != null && world instanceof WorldServer) {
                te.markDirty();
                ((WorldServer)world).getPlayerChunkMap().markBlockForUpdate(pos);
            }
        }
        try {
            return (T)te;
        }
        catch (Exception e) {
            te.invalidate();
            return null;
        }
    }

    private static double getDistance(Entity entity, BlockPos pos) {
        return entity.getDistance((double)pos.getX() + 0.5, (double)pos.getY() + 0.5, (double)pos.getZ() + 0.5);
    }
}

