/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util.helper;

import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.Optional;

public class SpriteHelper {
    public static String getSpriteExtra(String name, int form) {
        return SpriteHelper.getSpriteExtra(name, form, Gender.Male, 0, null);
    }

    public static String getSpriteExtra(String name, int form, Gender gender, int specialTexture) {
        return SpriteHelper.getSpriteExtra(name, form, gender, specialTexture, "");
    }

    public static String getSpriteExtra(String name, int form, Gender gender, int specialTexture, String customTexture) {
        Optional<EnumSpecies> optional = EnumSpecies.getFromName(name);
        if (optional.isPresent()) {
            EnumSpecies pokemon = optional.get();
            String formName = "";
            if (form != -1) {
                formName = pokemon.getFormEnum(form).getSpriteSuffix();
            } else if (EnumSpecies.mfSprite.contains((Object)pokemon)) {
                formName = "-" + gender.name().toLowerCase();
            }
            if (customTexture != null && !customTexture.isEmpty()) {
                return formName + customTexture;
            }
            return formName + pokemon.getSpecialTexture(pokemon.getFormEnum(form), specialTexture).getTexture();
        }
        return "";
    }
}

