/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.core.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import javax.annotation.Nullable;
import net.minecraft.command.CommandException;
import net.minecraft.command.EntitySelector;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class PixelmonPlayerUtils {
    @Nullable
    public static EntityPlayerMP getUniquePlayerStartingWith(String username, ICommandSender sender) {
        if (username.startsWith("@")) {
            try {
                List<EntityPlayerMP> playerMatches = EntitySelector.matchEntities(sender, username, EntityPlayerMP.class);
                if (playerMatches.size() != 1) {
                    return null;
                }
                return playerMatches.get(0);
            }
            catch (CommandException ce) {
                ce.printStackTrace();
            }
        }
        return PixelmonPlayerUtils.getUniquePlayerStartingWith(username);
    }

    @Nullable
    public static EntityPlayerMP getUniquePlayerStartingWith(String username) {
        ArrayList<EntityPlayerMP> playerMatches = new ArrayList<EntityPlayerMP>();
        for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
            if (!player.getName().toLowerCase().startsWith(username.toLowerCase())) continue;
            playerMatches.add(player);
        }
        if (playerMatches.size() != 1) {
            return null;
        }
        return (EntityPlayerMP)playerMatches.get(0);
    }

    public static boolean hasItem(EntityPlayerMP player, Predicate<ItemStack> checker) {
        if (player == null) {
            return false;
        }
        InventoryPlayer inventory = player.inventory;
        if (inventory.mainInventory.stream().anyMatch(checker)) {
            return true;
        }
        return inventory.offHandInventory.stream().anyMatch(checker);
    }
}

