/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util;

import com.pixelmongenerations.api.events.HyperspaceEvent;
import com.pixelmongenerations.api.events.SpaceTimeDistortionEvent;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.EntityMysteriousRing;
import com.pixelmongenerations.common.entity.EntitySpaceTimeDistortion;
import com.pixelmongenerations.common.entity.EntityWishingStar;
import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.drops.AlphaInfo;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.command.MaxEnergyBeam;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.event.ForgeListener;
import com.pixelmongenerations.core.event.RepelHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class PixelmonTasks {
    private static Map<EntitySpaceTimeDistortion, Timer> distortionItemTimerMap = new HashMap<EntitySpaceTimeDistortion, Timer>();
    private static Map<EntitySpaceTimeDistortion, Timer> distortionPokemonTimerMap = new HashMap<EntitySpaceTimeDistortion, Timer>();
    public static int spawnedSpaceTimeDistortions = 0;

    public static void startEggStepsTimer() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask(){

            @Override
            public void run() {
                for (EntityPlayerMP player : ForgeListener.players) {
                    PixelmonStorage.pokeBallManager.getPlayerStorage(player).ifPresent(storage -> storage.calculateWalkedSteps(player));
                }
            }
        }, 0L, 1000L);
    }

    public static void startPokerusTimer() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask(){

            @Override
            public void run() {
                for (EntityPlayerMP player : ForgeListener.players) {
                    PixelmonStorage.pokeBallManager.getPlayerStorage(player).ifPresent(PlayerStorage::pokeRusTick);
                }
            }
        }, 0L, 1000L);
    }

    public static void startRepelTimer() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask(){

            @Override
            public void run() {
                RepelHandler.onTick();
            }
        }, 0L, 1000L);
    }

    public static void startWishingStarTimer() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask(){

            @Override
            public void run() {
                for (EntityPlayerMP player : ForgeListener.players) {
                    player.world.getMinecraftServer().addScheduledTask(() -> PixelmonStorage.pokeBallManager.getPlayerStorage(player).ifPresent(storage -> {
                        if (player.world.canSeeSky(player.getPosition()) && (storage.lastXPos != player.getPosition().getX() || storage.lastZPos != player.getPosition().getZ()) && storage.pokedex.getCaughtPercentage() >= (double)PixelmonConfig.wishingStarDexCompletion && player.world.rand.nextInt(PixelmonConfig.wishingStar) == 1) {
                            EntityWishingStar wishingStar = new EntityWishingStar(player.world);
                            wishingStar.setPosition(player.getPosition().getX(), player.getPosition().getY() + 20, player.getPosition().getZ());
                            player.world.spawnEntity(wishingStar);
                            int direction = player.world.rand.nextInt(4);
                            if (direction == 0) {
                                wishingStar.addVelocity(3.0, 0.0, 0.0);
                            } else if (direction == 1) {
                                wishingStar.addVelocity(-3.0, 0.0, 0.0);
                            } else if (direction == 2) {
                                wishingStar.addVelocity(0.0, 0.0, 3.0);
                            } else {
                                wishingStar.addVelocity(0.0, 0.0, -3.0);
                            }
                        }
                    }));
                }
            }
        }, 0L, 1000L);
    }

    public static void startMaxEnergyBeamTimer() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask(){

            @Override
            public void run() {
                for (EntityPlayerMP player : ForgeListener.players) {
                    player.world.getMinecraftServer().addScheduledTask(() -> PixelmonStorage.pokeBallManager.getPlayerStorage(player).ifPresent(storage -> {
                        if (player.world.canSeeSky(player.getPosition()) && (storage.lastXPos != player.getPosition().getX() || storage.lastZPos != player.getPosition().getZ()) && player.world.rand.nextInt(PixelmonConfig.dynamaxEnergyBeam) == 1) {
                            int z;
                            int y;
                            int spawnRadius = 30;
                            int x = player.getPosition().getX() + player.world.rand.nextInt(spawnRadius * 2 + 1) - spawnRadius;
                            if (!MaxEnergyBeam.attemptEnergySpawn(player, x, y = player.getPosition().getY(), z = player.getPosition().getZ() + player.world.rand.nextInt(spawnRadius * 2 + 1) - spawnRadius)) {
                                y = player.world.getHeight(x, z);
                                Block block = player.world.getBlockState(new BlockPos(x, y - 1, z)).getBlock();
                                if (block.getRegistryName().toString().contains("leaves")) {
                                    y -= 8;
                                }
                                if ((block = player.world.getBlockState(new BlockPos(x, y, z)).getBlock()) == Blocks.AIR) {
                                    y -= 5;
                                }
                                MaxEnergyBeam.attemptEnergySpawn(player, x, y, z);
                            }
                        }
                    }));
                }
            }
        }, 0L, 1000L);
    }

    public static void startSpaceTimeDistortionTimer() {
        long interval = (long)PixelmonConfig.spaceTimeDistortionSpawnerTimer * 1000L;
        Timer timer = new Timer();
        timer.schedule(new TimerTask(){

            @Override
            public void run() {
                if (spawnedSpaceTimeDistortions < PixelmonConfig.maxSpaceTimeDistortions && RandomHelper.getRandomChance(PixelmonConfig.spaceTimeDistortionSpawnChance)) {
                    if (ForgeListener.players.size() == 0) {
                        return;
                    }
                    EntityPlayerMP randomPlayer = ForgeListener.players.size() == 1 ? ForgeListener.players.get(0) : ForgeListener.players.get(RandomHelper.getRandomNumberBetween(0, ForgeListener.players.size()));
                    if (randomPlayer == null) {
                        return;
                    }
                    int randX = RandomHelper.getRandomNumberBetween(1, 100) == 50 ? randomPlayer.getPosition().getX() - RandomHelper.getRandomNumberBetween(30, 50) : randomPlayer.getPosition().getX() + RandomHelper.getRandomNumberBetween(30, 50);
                    int randZ = RandomHelper.getRandomNumberBetween(1, 100) == 50 ? randomPlayer.getPosition().getZ() - RandomHelper.getRandomNumberBetween(30, 50) : randomPlayer.getPosition().getZ() + RandomHelper.getRandomNumberBetween(30, 50);
                    randomPlayer.world.getMinecraftServer().addScheduledTask(() -> {
                        int y = randomPlayer.world.getTopSolidOrLiquidBlock(new BlockPos(randX, 0, randZ)).getY();
                        EntitySpaceTimeDistortion distortion = new EntitySpaceTimeDistortion(randomPlayer.world);
                        distortion.setPositionAndUpdate(randX, y, randZ);
                        distortion.spawnPos = new BlockPos(randX, y, randZ);
                        SpaceTimeDistortionEvent.SpawnEvent event = new SpaceTimeDistortionEvent.SpawnEvent(distortion);
                        MinecraftForge.EVENT_BUS.post(event);
                        if (!event.isCanceled()) {
                            randomPlayer.world.spawnEntity(distortion);
                            ++spawnedSpaceTimeDistortions;
                            String biomeName = distortion.world.getBiome((BlockPos)distortion.getPosition()).biomeName;
                            randomPlayer.world.getMinecraftServer().getPlayerList().sendMessage(new TextComponentTranslation("chat.type.announcement", (Object)((Object)TextFormatting.LIGHT_PURPLE) + "Pixelmon" + (Object)((Object)TextFormatting.RESET), (Object)((Object)TextFormatting.GREEN) + I18n.translateToLocalFormatted("spawn.spacetimedistortionmessage", biomeName)));
                            Pixelmon.LOGGER.info("A Space Time Distortion was spawned at location: " + distortion.getPosition().getX() + ", " + distortion.getPosition().getY() + ", " + distortion.getPosition().getZ());
                        }
                    });
                }
            }
        }, interval, interval);
    }

    public static void startRingTimer() {
        long interval = (long)PixelmonConfig.mysteriousRingSpawnInterval * 1000L;
        Timer timer = new Timer();
        timer.schedule(new TimerTask(){

            @Override
            public void run() {
                if (RandomHelper.getRandomChance(PixelmonConfig.mysteriousRingSpawnChance)) {
                    if (ForgeListener.players.size() == 0) {
                        return;
                    }
                    EntityPlayerMP randomPlayer = ForgeListener.players.size() == 1 ? ForgeListener.players.get(0) : ForgeListener.players.get(RandomHelper.getRandomNumberBetween(0, ForgeListener.players.size()));
                    if (randomPlayer == null) {
                        return;
                    }
                    int randX = RandomHelper.getRandomNumberBetween(1, 100) == 50 ? randomPlayer.getPosition().getX() - RandomHelper.getRandomNumberBetween(30, 50) : randomPlayer.getPosition().getX() + RandomHelper.getRandomNumberBetween(30, 50);
                    int randZ = RandomHelper.getRandomNumberBetween(1, 100) == 50 ? randomPlayer.getPosition().getZ() - RandomHelper.getRandomNumberBetween(30, 50) : randomPlayer.getPosition().getZ() + RandomHelper.getRandomNumberBetween(30, 50);
                    randomPlayer.world.getMinecraftServer().addScheduledTask(() -> {
                        double y = randomPlayer.world.getTopSolidOrLiquidBlock(new BlockPos(randZ, 0, randZ)).getY();
                        EntityMysteriousRing ring = new EntityMysteriousRing(randomPlayer.world);
                        ring.setPositionAndUpdate(randX, y, randZ);
                        HyperspaceEvent.Spawn event = new HyperspaceEvent.Spawn(ring, randomPlayer);
                        MinecraftForge.EVENT_BUS.post(event);
                        if (!event.isCanceled()) {
                            ring.life = 0;
                            randomPlayer.world.spawnEntity(ring);
                            Pixelmon.LOGGER.info("A Space Time Distortion was spawned at location: " + ring.getPosition().getX() + ", " + ring.getPosition().getY() + ", " + ring.getPosition().getZ());
                        }
                    });
                }
            }
        }, interval, interval);
    }

    public static void startSTDSpawns(final SpaceTimeDistortionEvent.StartEvent event) {
        final EntitySpaceTimeDistortion distortion = event.getDistortion();
        final int blockRadius = 38;
        boolean startItems = true;
        for (Map.Entry<EntitySpaceTimeDistortion, Timer> entry : distortionItemTimerMap.entrySet()) {
            if (!entry.getKey().getUniqueID().toString().equals(distortion.getUniqueID().toString())) continue;
            startItems = false;
            break;
        }
        if (startItems) {
            long itemInterval = (long)PixelmonServerConfig.spaceTimeDistortionItemTimer * 1000L;
            Timer timer = new Timer();
            timer.schedule(new TimerTask(){

                @Override
                public void run() {
                    EntityItem item;
                    EntityPlayer closetPlayer = distortion.world.getClosestPlayerToEntity(distortion, blockRadius);
                    if (closetPlayer != null && (item = PixelmonTasks.getRandomItem(event, blockRadius, distortion.getPosition())) != null) {
                        closetPlayer.world.getMinecraftServer().addScheduledTask(() -> distortion.world.spawnEntity(item));
                    }
                }
            }, itemInterval, itemInterval);
            distortionItemTimerMap.put(distortion, timer);
        }
        boolean startPokemon = true;
        for (Map.Entry entry : distortionPokemonTimerMap.entrySet()) {
            if (!((EntitySpaceTimeDistortion)entry.getKey()).getUniqueID().toString().equals(distortion.getUniqueID().toString())) continue;
            startPokemon = false;
            break;
        }
        if (startPokemon) {
            long pokemonInterval = (long)PixelmonServerConfig.spaceTimeDistortionPokemonTimer * 1000L;
            Timer pokemonTimer = new Timer();
            pokemonTimer.schedule(new TimerTask(){

                @Override
                public void run() {
                    EntityPixelmon pokemon;
                    EntityPlayer closetPlayer = distortion.world.getClosestPlayerToEntity(distortion, blockRadius);
                    if (closetPlayer != null && (pokemon = PixelmonTasks.getRandomPokemon(event, blockRadius, distortion.getPosition())) != null) {
                        closetPlayer.world.getMinecraftServer().addScheduledTask(() -> distortion.world.spawnEntity(pokemon));
                    }
                }
            }, pokemonInterval, pokemonInterval);
            distortionPokemonTimerMap.put(distortion, pokemonTimer);
        }
    }

    public static void stopSTDSpawns(SpaceTimeDistortionEvent.EndEvent event) {
        EntitySpaceTimeDistortion distortion = event.getDistortion();
        distortionItemTimerMap.entrySet().removeIf(entry -> {
            if (((EntitySpaceTimeDistortion)entry.getKey()).getUniqueID().toString().equalsIgnoreCase(distortion.getUniqueID().toString())) {
                ((Timer)entry.getValue()).cancel();
                return true;
            }
            return false;
        });
        distortionPokemonTimerMap.entrySet().removeIf(entry -> {
            if (((EntitySpaceTimeDistortion)entry.getKey()).getUniqueID().toString().equalsIgnoreCase(distortion.getUniqueID().toString())) {
                ((Timer)entry.getValue()).cancel();
                return true;
            }
            return false;
        });
    }

    private static EntityItem getRandomItem(SpaceTimeDistortionEvent.StartEvent event, int blockRadius, BlockPos centerPoint) {
        Map<String, Double> items = event.getItems();
        double sum = items.values().stream().mapToDouble(c -> c).sum();
        double rng = sum * event.getDistortion().world.rand.nextDouble();
        String id = null;
        for (Map.Entry<String, Double> entry : items.entrySet()) {
            if (Double.compare(rng, entry.getValue()) <= 0) {
                id = entry.getKey();
                break;
            }
            rng -= entry.getValue().doubleValue();
        }
        if (id == null) {
            return null;
        }
        try {
            ItemStack stack = new ItemStack(Item.getByNameOrId(id));
            BlockPos randPos = PixelmonTasks.getRandomPosition(event.getDistortion().world, centerPoint, blockRadius);
            EntityItem item = new EntityItem(event.getDistortion().world, randPos.getX(), randPos.getY(), randPos.getZ(), stack);
            item.setPosition(randPos.getX(), randPos.getY(), randPos.getZ());
            return item;
        }
        catch (NullPointerException er) {
            Pixelmon.LOGGER.error("Couldn't get EntityItem for Space Time Distortion!");
            return null;
        }
    }

    private static EntityPixelmon getRandomPokemon(SpaceTimeDistortionEvent.StartEvent event, int blockRadius, BlockPos centerPoint) {
        Map<PokemonSpec, Double> items = event.getPokemon();
        double sum = items.values().stream().mapToDouble(c -> c).sum();
        double rng = sum * event.getDistortion().world.rand.nextDouble();
        PokemonSpec spec = null;
        for (Map.Entry<PokemonSpec, Double> entry : items.entrySet()) {
            if (Double.compare(rng, entry.getValue()) <= 0) {
                spec = entry.getKey();
                break;
            }
            rng -= entry.getValue().doubleValue();
        }
        if (spec == null) {
            return null;
        }
        EntityPixelmon pokemon = spec.create(event.getDistortion().world);
        if (pokemon == null) {
            Pixelmon.LOGGER.error("Couldn't get EntityPixelmon for Space Time Distortion!");
            return null;
        }
        boolean alpha = RandomHelper.getRandomChance(PixelmonServerConfig.spaceTimeDistortionAlphaPokemonChance);
        if (alpha) {
            boolean isDefaultAlpha = false;
            for (AlphaInfo info : DropItemRegistry.getAlphaPokemon()) {
                if (info.pokemon != pokemon.getSpecies()) continue;
                isDefaultAlpha = true;
                break;
            }
            if (!isDefaultAlpha) {
                alpha = false;
            }
        }
        pokemon.setAlpha(alpha);
        BlockPos pos = PixelmonTasks.getRandomPosition(event.getDistortion().world, centerPoint, blockRadius);
        pokemon.setPosition(pos.getX(), pos.getY(), pos.getZ());
        return pokemon;
    }

    private static BlockPos getRandomPosition(World world, BlockPos center, int blockRadius) {
        int x = RandomHelper.getRandomChance(50) ? center.getX() + RandomHelper.getRandomNumberBetween(0, blockRadius) : center.getX() - RandomHelper.getRandomNumberBetween(0, blockRadius);
        int z = RandomHelper.getRandomChance(50) ? center.getZ() + RandomHelper.getRandomNumberBetween(0, blockRadius) : center.getZ() - RandomHelper.getRandomNumberBetween(0, blockRadius);
        BlockPos pos = new BlockPos(x, 0, z);
        int y = world.getTopSolidOrLiquidBlock(pos).getY();
        return new BlockPos(x, y, z);
    }
}

