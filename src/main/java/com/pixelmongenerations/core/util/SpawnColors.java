/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.reflect.TypeToken
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 */
package com.pixelmongenerations.core.util;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmongenerations.common.block.ranch.JsonLoader;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.Adapters;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.minecraft.item.EnumDyeColor;

public class SpawnColors {
    public static Gson gsonInstance = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(Color.class, (Object)new Adapters.ColorAdapter()).create();
    private static Map<EnumSpecies, List<Color>> colors = new HashMap<EnumSpecies, List<Color>>();
    public static final Color LIGHT_BLUE = Color.decode("#3AB3DA");
    public static final Color ORANGE = Color.decode("#F9801D");
    public static final Color YELLOW = Color.decode("#FED83D");
    public static final Color WHITE = Color.decode("#FFFFFF");
    public static final Color RED = Color.decode("#ff0f00");
    public static final Color BLUE = Color.decode("#3C44AA");
    public static final Color PURPLE = Color.decode("#8932B8");
    public static final Color BROWN = Color.decode("#835432");
    public static final Color LIGHT_GREEN = Color.decode("#55FF55");
    public static final Color GREEN = Color.decode("#00AA00");
    public static final Color GOLD = Color.decode("#FFD700");
    public static final Color PINK = Color.decode("#FFC0CB");
    public static final Color GRAY = Color.decode("#AAAAAA");
    public static final Color BLACK = Color.decode("#000000");
    private static final List<Color> DEFAULT = Collections.singletonList(GOLD);

    public static Color toColor(EnumDyeColor color) {
        return new Color(color.getColorValue());
    }

    public static List<Color> toColor(String ... colors) {
        return Stream.of(colors).map(Color::decode).collect(Collectors.toList());
    }

    public static List<Color> getColors(EnumSpecies species) {
        return colors.getOrDefault((Object)species, DEFAULT);
    }

    public static void setup() throws FileNotFoundException {
        colors = JsonLoader.setup("spawn_colors", new TypeToken<Map<EnumSpecies, List<Color>>>(){}, HashMap::new, gsonInstance, PixelmonConfig.useExternalJSONFilesSpawnColors);
    }
}

