/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util.functional;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;

@FunctionalInterface
public interface BiFunction<A, B, C>
extends java.util.function.BiFunction<A, B, C> {
    default public BiPredicate<A, B> predicate(Predicate<C> after) {
        Objects.requireNonNull(after);
        return (a, b) -> after.test(this.apply(a, b));
    }

    default public BiConsumer<A, B> consume(Consumer<C> after) {
        Objects.requireNonNull(after);
        return (a, b) -> after.accept(this.apply(a, b));
    }
}

