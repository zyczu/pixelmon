/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.util;

import java.awt.Color;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.minecraft.item.EnumDyeColor;

public class ColorUtil {
    public static final Color LIGHT_BLUE = Color.decode("#3AB3DA");
    public static final Color ORANGE = Color.decode("#F9801D");
    public static final Color YELLOW = Color.decode("#FED83D");
    public static final Color WHITE = Color.decode("#FFFFFF");
    public static final Color RED = Color.decode("#ff0f00");
    public static final Color BLUE = Color.decode("#3C44AA");
    public static final Color PURPLE = Color.decode("#8932B8");
    public static final Color BROWN = Color.decode("#835432");
    public static final Color LIGHT_GREEN = Color.decode("#55FF55");
    public static final Color GREEN = Color.decode("#00AA00");
    public static final Color GOLD = Color.decode("#FFD700");
    public static final Color PINK = Color.decode("#FFC0CB");
    public static final Color GRAY = Color.decode("#AAAAAA");
    public static final Color BLACK = Color.decode("#000000");

    public static Color toColor(EnumDyeColor color) {
        return new Color(color.getColorValue());
    }

    public static List<Color> toColor(String ... colors) {
        return Stream.of(colors).map(Color::decode).collect(Collectors.toList());
    }
}

