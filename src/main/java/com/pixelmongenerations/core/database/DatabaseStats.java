/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.database;

import com.pixelmongenerations.api.def.EvolutionDef;
import com.pixelmongenerations.api.events.pokemon.BaseStatsLoadEvent;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.api.world.WeatherType;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.Evolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.BiomeCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.ChanceCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoRockCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.FriendshipCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.GenderCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.HeldItemCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.HighAltitudeCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.MoveCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.MoveTypeCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.PartyCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.StatRatioCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.TimeCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.WeatherCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.InteractEvolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.LevelingEvolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.MilceryEvolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.NatureEvolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.TradeEvolution;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.data.pokemon.PokemonRegistry;
import com.pixelmongenerations.core.enums.EnumEvolutionRock;
import com.pixelmongenerations.core.enums.EnumEvolutionStone;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import net.minecraft.init.Biomes;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class DatabaseStats {
    public static Optional<BaseStats> getBaseStats(String name) {
        return DatabaseStats.getBaseStats(name, -1);
    }

    public static Optional<BaseStats> getBaseStats(String name, int form) {
        Optional<BaseStats> baseStats = Optional.ofNullable(PokemonRegistry.getBaseFor(EnumSpecies.getFromNameAnyCase(name), form));
        BaseStatsLoadEvent event = new BaseStatsLoadEvent(name, form, baseStats);
        MinecraftForge.EVENT_BUS.post(event);
        return event.getBaseStats();
    }

    /*
     * Could not resolve type clashes
     */
    public static void getPixelmonEvolutions(BaseStats store) throws SQLException {
        ArrayList<Evolution> evolutions = new ArrayList<Evolution>();
        for (EvolutionDef def : store.evolutionDefs) {
            boolean disableEvolution;
            PokemonSpec evolveInto = null;
            Evolution currentEvolution = null;
            int evolveLevel = def.evolveLevel != null ? def.evolveLevel : 0;
            int form = -1;
            String evolveCondition = def.evolveCondition;
            if (def.alcremieEvo) {
                evolveInto = PokemonSpec.from(def.targetSpecies.name);
                currentEvolution = new MilceryEvolution(store.pokemon, evolveInto, null, new EvoCondition[0]);
            } else if (def.natureEvolve != null) {
                evolveInto = PokemonSpec.from(def.targetSpecies.name);
                if (evolveLevel > 1) {
                    currentEvolution = new NatureEvolution(store.pokemon, evolveInto, evolveLevel, def.natureEvolve, new EvoCondition[0]);
                }
            } else {
                int pixelmonToID = def.targetId;
                form = PokemonRegistry.getFormForPokemonId(pixelmonToID);
                if (evolveCondition == null) {
                    evolveInto = PokemonSpec.from(EnumSpecies.getFromPixelmonId((int)pixelmonToID).name);
                    if (evolveLevel > 1) {
                        currentEvolution = new LevelingEvolution(store.pokemon, evolveInto, evolveLevel, new EvoCondition[0]);
                    }
                } else {
                    TimeCondition.EnumTime enumTime;
                    String[] splits = evolveCondition.split(":");
                    Optional<EnumSpecies> poke = EnumSpecies.getFromName(splits[splits.length - 1]);
                    PokemonSpec to = evolveInto;
                    if (poke.isPresent()) {
                        to = PokemonSpec.from(poke.get().name);
                        if (form != -1) {
                            to.form = form;
                        }
                    }
                    if (splits[0].equalsIgnoreCase("time")) {
                        enumTime = TimeCondition.EnumTime.getTime(splits[1]);
                        currentEvolution = new LevelingEvolution(store.pokemon, to, evolveLevel, new TimeCondition(enumTime));
                    } else if (splits[0].equalsIgnoreCase("weather")) {
                        WeatherType weather = WeatherType.valueOf(splits[1].toUpperCase());
                        currentEvolution = new LevelingEvolution(store.pokemon, to, evolveLevel, new WeatherCondition(weather));
                    } else if (splits[0].equalsIgnoreCase("highaltitude")) {
                        currentEvolution = new LevelingEvolution(store.pokemon, to, evolveLevel, new HighAltitudeCondition());
                    } else if (splits[0].equalsIgnoreCase("friendship")) {
                        if (splits.length > 2) {
                            enumTime = TimeCondition.EnumTime.getTime(splits[1]);
                            currentEvolution = new LevelingEvolution(store.pokemon, to, 1, new FriendshipCondition(), new TimeCondition(enumTime));
                        } else {
                            currentEvolution = new LevelingEvolution(store.pokemon, to, 1, new FriendshipCondition());
                        }
                    } else if (splits[0].equalsIgnoreCase("fairymovefriendshipflowerbiome")) {
                        currentEvolution = new LevelingEvolution(store.pokemon, to, 1, new FriendshipCondition(), new MoveTypeCondition(EnumType.Fairy), new BiomeCondition(Biomes.MUTATED_FOREST, Biomes.MUTATED_PLAINS));
                    } else if (splits[0].equalsIgnoreCase("trade")) {
                        currentEvolution = new TradeEvolution(store.pokemon, to, null, new EvoCondition[0]);
                        if (splits.length > 2) {
                            ItemHeld heldItem = PixelmonItemsHeld.getHeldItem(splits[1]);
                            if (heldItem == null) {
                                EnumSpecies withPoke = EnumSpecies.getFromNameAnyCase(splits[1]);
                                if (withPoke == null) {
                                    Pixelmon.LOGGER.error("Can't find item " + splits[1] + " for evolution!");
                                    currentEvolution = null;
                                } else {
                                    ((TradeEvolution)currentEvolution).with = withPoke;
                                }
                            } else {
                                currentEvolution.conditions.add(new HeldItemCondition(PixelmonItemsHeld.getHeldItem(splits[1])));
                            }
                        }
                    } else if (splits[0].equalsIgnoreCase("move")) {
                        currentEvolution = new LevelingEvolution(store.pokemon, to, 1, new MoveCondition(Integer.parseInt(splits[1])));
                    } else if (EnumEvolutionStone.hasEvolutionStone(splits[0])) {
                        currentEvolution = new InteractEvolution(store.pokemon, to, EnumEvolutionStone.getEvolutionStone(splits[0]).getItem(0), new EvoCondition[0]);
                        if (splits.length > 2) {
                            currentEvolution.conditions.add(new GenderCondition(Gender.getGender(splits[1])));
                        }
                    } else if (EnumEvolutionRock.hasEvolutionRock(splits[0])) {
                        currentEvolution = new LevelingEvolution(store.pokemon, to, 1, new EvoRockCondition(EnumEvolutionRock.getEvolutionRock(splits[0]), 100));
                    } else if (splits[0].equalsIgnoreCase("levelupgender")) {
                        currentEvolution = new LevelingEvolution(store.pokemon, to, evolveLevel, new EvoCondition[0]);
                        Gender gender = splits[1].equalsIgnoreCase("male") ? Gender.Male : Gender.Female;
                        currentEvolution.conditions.add(new GenderCondition(gender));
                    } else if (splits[0].equalsIgnoreCase("levelupbiome")) {
                        currentEvolution = new LevelingEvolution(store.pokemon, to, 1, new EvoCondition[0]);
                        ArrayList<Biome> biomes = new ArrayList<Biome>();
                        if (splits.length < 4) {
                            String biomeString = splits[1].toLowerCase();
                            for (Object biome : GameRegistry.findRegistry(Biome.class).getValues()) {
                                if (!((IForgeRegistryEntry.Impl)biome).getRegistryName().getPath().toLowerCase().replaceAll("_", "").contains(biomeString)) continue;
                                biomes.add((Biome)biome);
                            }
                        } else {
                            int level = Integer.parseInt(splits[1]);
                            String biomeString = splits[2].toLowerCase();
                            for (Biome biome : GameRegistry.findRegistry(Biome.class).getValues()) {
                                if (!biome.getRegistryName().getPath().toLowerCase().replaceAll("_", "").contains(biomeString)) continue;
                                biomes.add(biome);
                            }
                            ((LevelingEvolution)currentEvolution).level = level;
                        }
                        currentEvolution.conditions.add(new BiomeCondition(biomes));
                    } else if (splits[0].equalsIgnoreCase("leveluphelditem")) {
                        String itemString = splits[1];
                        try {
                            ItemHeld item = PixelmonItemsHeld.getHeldItem(itemString);
                            if (item == null) {
                                throw new Exception("Can't find item " + itemString + " for evolution!");
                            }
                            TimeCondition timeCond = null;
                            if (splits.length == 4) {
                                String time = splits[2];
                                timeCond = new TimeCondition(TimeCondition.EnumTime.valueOf(time));
                            }
                            if (timeCond != null) {
                                currentEvolution = new LevelingEvolution(store.pokemon, to, 1, new HeldItemCondition(item), timeCond);
                            }
                            currentEvolution = new LevelingEvolution(store.pokemon, to, 1, new HeldItemCondition(item));
                        }
                        catch (Exception var22) {
                            var22.printStackTrace();
                            return;
                        }
                    } else if (splits[0].equalsIgnoreCase("party")) {
                        currentEvolution = new LevelingEvolution(store.pokemon, to, evolveLevel, new EvoCondition[0]);
                        if (EnumType.hasType(splits[1])) {
                            currentEvolution.conditions.add(new PartyCondition(EnumType.valueOf(splits[1])));
                        } else if (EnumSpecies.getFromNameAnyCase(splits[1]) != null) {
                            currentEvolution.conditions.add(new PartyCondition(EnumSpecies.getFromNameAnyCase(splits[1])));
                        }
                    } else if (splits[0].equalsIgnoreCase("random")) {
                        EnumSpecies[] pokemonChoices = new EnumSpecies[splits.length - 1];
                        boolean invalid = false;
                        for (int i = 1; i < splits.length; ++i) {
                            Optional<EnumSpecies> poke2 = EnumSpecies.getFromName(splits[splits.length - i]);
                            if (!poke2.isPresent()) {
                                invalid = true;
                                break;
                            }
                            pokemonChoices[i - 1] = poke2.get();
                        }
                        if (invalid) continue;
                        float chance = 1.0f / (float)pokemonChoices.length;
                        int i = 0;
                        while (++i < pokemonChoices.length - 1) {
                            evolutions.add(new LevelingEvolution(store.pokemon, PokemonSpec.from(pokemonChoices[i].name), evolveLevel, new ChanceCondition(chance)));
                        }
                        currentEvolution = new LevelingEvolution(store.pokemon, PokemonSpec.from(pokemonChoices[pokemonChoices.length - 1].name), evolveLevel, new EvoCondition[0]);
                    } else if (splits[0].equalsIgnoreCase("tyrogue")) {
                        evolutions.add(new LevelingEvolution(store.pokemon, PokemonSpec.from("Hitmonlee"), 20, new StatRatioCondition(StatsType.Attack, StatsType.Defence, 1.0f)));
                        evolutions.add(new LevelingEvolution(store.pokemon, PokemonSpec.from("Hitmonchan"), 20, new StatRatioCondition(StatsType.Defence, StatsType.Attack, 1.0f)));
                        currentEvolution = new LevelingEvolution(store.pokemon, PokemonSpec.from("Hitmontop"), 20, new EvoCondition[0]);
                    } else if (splits[0].equalsIgnoreCase("mantyke")) {
                        to = PokemonSpec.from(EnumSpecies.Mantine.name);
                        currentEvolution = new LevelingEvolution(store.pokemon, to, 1, new PartyCondition(EnumSpecies.Remoraid));
                    } else if (splits[0].equalsIgnoreCase("burmy")) {
                        evolutions.add(new LevelingEvolution(store.pokemon, PokemonSpec.from("Wormadam"), 20, new GenderCondition(Gender.Female)));
                        currentEvolution = new LevelingEvolution(store.pokemon, PokemonSpec.from("Mothim"), 20, new GenderCondition(Gender.Male));
                    } else if (splits[0].equalsIgnoreCase("sinistea")) {
                        if (store.form == 0) {
                            currentEvolution = new InteractEvolution(store.pokemon, PokemonSpec.from(EnumSpecies.Polteageist.name), EnumEvolutionStone.CrackedPot.getItem(0), new EvoCondition[0]);
                        } else if (store.form == 1) {
                            currentEvolution = new InteractEvolution(store.pokemon, PokemonSpec.from(EnumSpecies.Polteageist.name), EnumEvolutionStone.ChippedPot.getItem(0), new EvoCondition[0]);
                        }
                    } else if (splits[0].equalsIgnoreCase("vulpix")) {
                        if (store.form == -1) {
                            currentEvolution = new InteractEvolution(store.pokemon, PokemonSpec.from(EnumSpecies.Ninetales.name), EnumEvolutionStone.Firestone.getItem(0), new EvoCondition[0]);
                        } else if (store.form == 3) {
                            currentEvolution = new InteractEvolution(store.pokemon, PokemonSpec.from(EnumSpecies.Ninetales.name), EnumEvolutionStone.Icestone.getItem(0), new EvoCondition[0]);
                        }
                    } else if (splits[0].equalsIgnoreCase("sandshrew")) {
                        if (store.form == -1) {
                            currentEvolution = new LevelingEvolution(store.pokemon, PokemonSpec.from(EnumSpecies.Sandslash.name), 22, new EvoCondition[0]);
                        } else if (store.form == 3) {
                            currentEvolution = new InteractEvolution(store.pokemon, PokemonSpec.from(EnumSpecies.Sandslash.name), EnumEvolutionStone.Icestone.getItem(0), new EvoCondition[0]);
                        }
                    } else if (splits[0].equalsIgnoreCase("petilil_hisuian")) {
                        currentEvolution = new InteractEvolution(store.pokemon, PokemonSpec.from(EnumSpecies.Lilligant.name), EnumEvolutionStone.Sunstone.getItem(0), new HeldItemCondition(PixelmonItemsHeld.getHeldItem("Fighting Gem")));
                    }
                    evolveInto = to;
                }
            }
            boolean bl = disableEvolution = evolveInto != null && !PixelmonConfig.allGenerationsEnabled() && store.pokemon.name.equals(evolveInto.name) && Entity3HasStats.isAvailableGeneration(store.nationalPokedexNumber) && !Entity3HasStats.isAvailableGeneration(evolveInto.name);
            if (store.pokemon == EnumSpecies.Espurr) {
                assert (currentEvolution != null);
                assert (currentEvolution instanceof LevelingEvolution);
                LevelingEvolution male = new LevelingEvolution(EnumSpecies.Espurr, PokemonSpec.from("Meowstic", "f:0"), ((LevelingEvolution)currentEvolution).level, new GenderCondition(Gender.Male));
                evolutions.add(male);
                currentEvolution = new LevelingEvolution(EnumSpecies.Espurr, PokemonSpec.from("Meowstic", "f:1"), male.level, new GenderCondition(Gender.Female));
            }
            if (store.pokemon == EnumSpecies.Cubone) {
                LevelingEvolution alolan = new LevelingEvolution(EnumSpecies.Cubone, PokemonSpec.from("Marowak", "f:3"), 28, new TimeCondition(TimeCondition.EnumTime.Night));
                evolutions.add(alolan);
            }
            if (store.pokemon == EnumSpecies.Rockruff && store.abilities[2] != null) {
                LevelingEvolution dusk = new LevelingEvolution(EnumSpecies.Rockruff, PokemonSpec.from("Lycanroc", "f:2"), 25, new TimeCondition(TimeCondition.EnumTime.Dusk));
                evolutions.add(dusk);
            }
            if (form != -1) {
                currentEvolution.setForm(form);
            }
            if (disableEvolution) continue;
            evolutions.add(currentEvolution);
        }
        if (store.pixelmonName.equals("Eevee")) {
            for (int i = 0; i < evolutions.size(); ++i) {
                Evolution e = (Evolution)evolutions.get(i);
                if (!e.to.name.equals("Sylveon")) continue;
                evolutions.remove(i);
                evolutions.add(0, e);
                break;
            }
        }
        Object[] array = evolutions.toArray();
        store.evolutions = new Evolution[array.length];
        for (int i = 0; i < array.length; ++i) {
            store.evolutions[i] = (Evolution)array[i];
        }
    }
}

