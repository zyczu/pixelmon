/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.database;

import com.pixelmongenerations.api.def.MoveContainer;
import com.pixelmongenerations.api.events.pokemon.LevelUpMovesEvent;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.AttackBase;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.Entity6CanBattle;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.data.pokemon.PokemonRegistry;
import com.pixelmongenerations.core.database.MoveTable;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumTutorType;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.minecraftforge.common.MinecraftForge;

public class DatabaseMoves {
    private static ArrayList<Attack> tutorMoveList;
    private static List<Attack> transferMoveList;

    public static Moveset getInitialMoves(PokemonLink pixelmon, int level) {
        Moveset moveset;
        ArrayList<Attack> attackList = DatabaseMoves.getMovesetUpToLevel(pixelmon.getBaseStats().id, level);
        ArrayList<Attack> defaultAttacks = new ArrayList<Attack>();
        if (pixelmon.isAlpha()) {
            ArrayList<Attack> alphaList;
            try {
                alphaList = DatabaseMoves.getAllAlphaAttacks(pixelmon.getBaseStats().id);
            }
            catch (NullPointerException er) {
                Pixelmon.LOGGER.warn(pixelmon.getNickname() + " has no Alpha moves!");
                alphaList = new ArrayList();
            }
            int learnedAttacks = 0;
            if (alphaList.size() > 0) {
                int alphaMoves = RandomHelper.getRandomNumberBetween(1, alphaList.size());
                for (int i = 0; i < alphaMoves; ++i) {
                    Attack alphaAttack = alphaList.get(RandomHelper.getRandomNumberBetween(0, alphaList.size() - 1));
                    alphaList.remove(alphaAttack);
                    defaultAttacks.add(alphaAttack);
                    ++learnedAttacks;
                }
                attackList.removeIf(alphaList::contains);
            }
            for (int i = learnedAttacks; i < 4; ++i) {
                Attack normalAttack = attackList.get(RandomHelper.getRandomNumberBetween(0, attackList.size() - 1));
                attackList.remove(normalAttack);
                defaultAttacks.add(normalAttack);
            }
            moveset = new Moveset(pixelmon);
            moveset.addAll(defaultAttacks);
        } else {
            while (attackList.size() > 4) {
                if (PixelmonConfig.useRecentLevelMoves) {
                    attackList.remove(attackList.size() - 1);
                    continue;
                }
                RandomHelper.removeRandomElementFromList(attackList);
            }
            if (attackList.isEmpty()) {
                attackList.add(DatabaseMoves.getAttack("Tackle"));
            }
            moveset = new Moveset(pixelmon);
            moveset.addAll(attackList);
        }
        return moveset;
    }

    public static ArrayList<Attack> getMovesetUpToLevel(int id, int level) {
        return DatabaseMoves.getMovesetUpToLevel(id, level, true);
    }

    public static ArrayList<Attack> getMovesetUpToLevel(int pokemonId, int level, boolean removeDuplicates) {
        ArrayList<Attack> attackList = new ArrayList<Attack>();
        BaseStats stats = PokemonRegistry.getBaseFor(pokemonId);
        for (int lvl : stats.moves.levelMoves.keySet()) {
            try {
                if (level < lvl) continue;
                stats.moves.levelMoves.get(lvl).forEach(move -> attackList.add(DatabaseMoves.getAttack(move)));
            }
            catch (Exception e) {
                System.out.printf("Error getMovesetUpToLevel> Pixelmon ID: [%s] Level: [%s]%n", pokemonId, level);
                e.printStackTrace();
            }
        }
        if (removeDuplicates) {
            for (int i = 0; i < attackList.size(); ++i) {
                for (int j = i + 1; j < attackList.size(); ++j) {
                    if (attackList.get((int)i).getAttackBase().attackIndex != attackList.get((int)j).getAttackBase().attackIndex) continue;
                    attackList.remove(j--);
                }
            }
        }
        return attackList;
    }

    public static boolean learnsAttackAtLevel(Entity6CanBattle pokemon, int level) {
        return !DatabaseMoves.getAttacksAtLevel(pokemon, level).isEmpty();
    }

    public static ArrayList<Attack> getAttacksAtLevel(Entity6CanBattle pokemon, int level) {
        int pokemonId = pokemon.baseStats.id;
        boolean idChanged = false;
        int tempId = DatabaseMoves.getPokemonID(pokemon);
        if (pokemonId != tempId) {
            pokemonId = tempId;
            idChanged = true;
        }
        ArrayList<Attack> attacks = new ArrayList<Attack>(DatabaseMoves.attacksAtLevelQuery(pokemonId, level));
        if (idChanged && attacks.isEmpty()) {
            pokemonId = pokemon.baseStats.baseFormID;
            attacks = DatabaseMoves.attacksAtLevelQuery(pokemonId, level);
        }
        LevelUpMovesEvent event = new LevelUpMovesEvent(pokemon, level, attacks);
        MinecraftForge.EVENT_BUS.post(event);
        return event.getAttacks();
    }

    private static ArrayList<Attack> attacksAtLevelQuery(int pokemonId, int level) {
        ArrayList<Attack> attacks = new ArrayList<Attack>();
        BaseStats stats = PokemonRegistry.getBaseFor(pokemonId);
        if (stats.moves.levelMoves.containsKey(level)) {
            attacks.addAll(stats.moves.levelMoves.get(level).stream().map(DatabaseMoves::getAttack).collect(Collectors.toList()));
        }
        return attacks;
    }

    public static ArrayList<Attack> getAllAttacks(Entity6CanBattle pokemon) {
        ArrayList<Attack> attacks = new ArrayList<Attack>();
        BaseStats stats = PokemonRegistry.getBaseFor(DatabaseMoves.getPokemonID(pokemon));
        stats.moves.levelMoves.values().forEach(attackNames -> attackNames.forEach(attackName -> attacks.add(DatabaseMoves.getAttack(attackName))));
        return attacks;
    }

    public static ArrayList<Attack> getAllTMHMAttacks(Entity6CanBattle pokemon) {
        return DatabaseMoves.getAllTMHMAttacks(DatabaseMoves.getPokemonID(pokemon));
    }

    public static ArrayList<Attack> getAllTMHMAttacks(int pokemonId) {
        BaseStats stats = PokemonRegistry.getBaseFor(pokemonId);
        return new ArrayList<Attack>(stats.moves.tmTRMoves.stream().map(DatabaseMoves::getAttack).collect(Collectors.toList()));
    }

    public static ArrayList<Attack> getAllTutorAttacks(int pokemonId) {
        ArrayList<Attack> attacks = new ArrayList<Attack>();
        BaseStats stats = PokemonRegistry.getBaseFor(pokemonId);
        stats.moves.tutorMoves.values().forEach(attackNames -> attackNames.forEach(attackName -> attacks.add(DatabaseMoves.getAttack(attackName))));
        return attacks;
    }

    public static ArrayList<Attack> getAllTutorAttacks(Entity6CanBattle pokemon) {
        return DatabaseMoves.getAllTutorAttacks(DatabaseMoves.getPokemonID(pokemon));
    }

    public static ArrayList<Attack> getAllEggAttacks(int pokemonId) {
        BaseStats stats = PokemonRegistry.getBaseFor(pokemonId);
        return new ArrayList<Attack>(stats.moves.eggMoves.stream().map(DatabaseMoves::getAttack).collect(Collectors.toList()));
    }

    public static ArrayList<Attack> getAllEggAttacks(Entity6CanBattle pokemon) {
        return DatabaseMoves.getAllEggAttacks(DatabaseMoves.getPokemonID(pokemon));
    }

    public static boolean hasEvolutionAttacks(Entity6CanBattle pokemon) {
        BaseStats stats = PokemonRegistry.getBaseFor(DatabaseMoves.getPokemonID(pokemon));
        return !stats.moves.evolutionMoves.isEmpty();
    }

    public static ArrayList<Attack> getAllEvolutionAttacks(int pokemonId) {
        BaseStats stats = PokemonRegistry.getBaseFor(pokemonId);
        return stats.moves.evolutionMoves.stream().map(DatabaseMoves::getAttack).collect(Collectors.toCollection(ArrayList::new));
    }

    public static ArrayList<Attack> getAllEvolutionAttacks(Entity6CanBattle pokemon) {
        return DatabaseMoves.getAllEvolutionAttacks(DatabaseMoves.getPokemonID(pokemon));
    }

    public static ArrayList<Attack> getAllAlphaAttacks(int pokemonId) {
        BaseStats stats = PokemonRegistry.getBaseFor(pokemonId);
        return stats.moves.alphaMoves.stream().map(DatabaseMoves::getAttack).collect(Collectors.toCollection(ArrayList::new));
    }

    public static ArrayList<Attack> getAllTMHMTutorAttacks(Entity6CanBattle pokemon) {
        ArrayList<Attack> attacks = new ArrayList<Attack>();
        attacks.addAll(DatabaseMoves.getAllTMHMAttacks(pokemon));
        attacks.addAll(DatabaseMoves.getAllTutorAttacks(pokemon));
        return attacks;
    }

    public static Attack getAttack(String moveName) {
        if (Attack.hasAttack(moveName)) {
            return new Attack(moveName);
        }
        Pixelmon.LOGGER.error("DatabaseMoves.java> getAttack(String moveName) is missing name " + moveName);
        return null;
    }

    public static Attack getAttack(int moveIndex) {
        if (Attack.hasAttack(moveIndex)) {
            return new Attack(moveIndex);
        }
        Pixelmon.LOGGER.error("DatabaseMoves.java> getAttack(int moveIndex) is missing index " + moveIndex);
        return null;
    }

    private static List<String> getTutorMoves(Map<EnumTutorType, List<String>> tutorMoves, boolean includeEvents) {
        List moves = tutorMoves.getOrDefault((Object)EnumTutorType.Regular, new ArrayList());
        if (includeEvents) {
            moves.addAll(tutorMoves.getOrDefault((Object)EnumTutorType.Event, new ArrayList()));
        }
        return moves;
    }

    public static ArrayList<Attack> getAllTutorAttacks(boolean includeEvents) {
        if (tutorMoveList == null) {
            tutorMoveList = Stream.of(EnumSpecies.values()).flatMap(i -> i.getAllForms().stream().map(a -> Entity3HasStats.getBaseStats(i, (int)a))).filter(Optional::isPresent).map(Optional::get).map(base -> base.moves.tutorMoves).flatMap(g -> DatabaseMoves.getTutorMoves(g, includeEvents).stream()).distinct().map(Attack::new).collect(Collectors.toCollection(ArrayList::new));
        }
        return tutorMoveList;
    }

    public static List<Attack> getAllTransferAttacks() {
        if (transferMoveList == null) {
            transferMoveList = Stream.of(EnumSpecies.values()).flatMap(i -> i.getAllForms().stream().map(a -> Entity3HasStats.getBaseStats(i, (int)a))).filter(Optional::isPresent).map(Optional::get).map(base -> base.moves.tutorMoves).map(g -> g.getOrDefault((Object)EnumTutorType.Transfer, new ArrayList<>())).flatMap(Collection::stream).distinct().map(Attack::new).collect(Collectors.toList());
        }
        return transferMoveList;
    }

    public static boolean CanLearnAttack(int id, String attackName) {
        try {
            AttackBase attack = Attack.getAttackBase(attackName).orElseThrow(NullPointerException::new);
            String moveName = attack.getUnlocalizedName();
            MoveContainer moves = PokemonRegistry.getBaseFor((int)id).moves;
            return DatabaseMoves.canLearnFromTable(MoveTable.Level, moves, moveName) || DatabaseMoves.canLearnFromTable(MoveTable.TMTR, moves, moveName) || DatabaseMoves.canLearnFromTable(MoveTable.Egg, moves, moveName) || DatabaseMoves.canLearnFromTable(MoveTable.Tutor, moves, moveName) || DatabaseMoves.canLearnFromTable(MoveTable.Evolution, moves, moveName);
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static boolean canLearnFromTable(MoveTable table, MoveContainer moves, String moveName) throws SQLException {
        AtomicBoolean canLearn = new AtomicBoolean(false);
        switch (table) {
            case Egg: {
                moves.eggMoves.forEach(move -> {
                    if (move.replace(" ", "").equalsIgnoreCase(moveName.replace(" ", ""))) {
                        canLearn.set(true);
                    }
                });
                break;
            }
            case Evolution: {
                moves.evolutionMoves.forEach(move -> {
                    if (move.replace(" ", "").equalsIgnoreCase(moveName.replace(" ", ""))) {
                        canLearn.set(true);
                    }
                });
                break;
            }
            case Level: {
                moves.levelMoves.values().forEach(moveNames -> {
                    for (String s : moveNames) {
                        if (!s.replace(" ", "").equalsIgnoreCase(moveName.replace(" ", ""))) continue;
                        canLearn.set(true);
                        break;
                    }
                });
                break;
            }
            case TMTR: {
                moves.tmTRMoves.forEach(move -> {
                    if (move.replace(" ", "").equalsIgnoreCase(moveName.replace(" ", ""))) {
                        canLearn.set(true);
                    }
                });
                break;
            }
            case Tutor: {
                moves.tutorMoves.values().forEach(moveNames -> {
                    for (String s : moveNames) {
                        if (!s.replace(" ", "").equalsIgnoreCase(moveName.replace(" ", ""))) continue;
                        canLearn.set(true);
                        break;
                    }
                });
            }
        }
        return canLearn.get();
    }

    public static int getPokemonID(Entity6CanBattle pokemon) {
        return DatabaseMoves.getPokemonID(pokemon.getSpecies(), pokemon.baseStats, pokemon.getForm(), pokemon.isMega);
    }

    public static int getPokemonID(EnumSpecies pokemon, BaseStats stats, int form, boolean isMega) {
        Optional<BaseStats> baseStats;
        int id = stats.baseFormID;
        boolean hasForms = Entity3HasStats.hasForms(stats.pokemon);
        if (hasForms && form != -1 && (baseStats = Entity3HasStats.getBaseStats(pokemon, form)).isPresent() && id != baseStats.get().id && !isMega) {
            id = baseStats.get().id;
        }
        return id;
    }

    public static int getPokemonID(PixelmonData pokemon) {
        return DatabaseMoves.getPokemonID(pokemon.getSpecies(), pokemon.getBaseStats(), pokemon.form, false);
    }
}

