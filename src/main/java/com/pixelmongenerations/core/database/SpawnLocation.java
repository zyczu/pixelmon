/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core.database;

import java.util.ArrayList;
import net.minecraft.util.text.translation.I18n;

public enum SpawnLocation {
    Land,
    LandNPC,
    LandVillager,
    Water,
    UnderGround,
    Air,
    AirPersistent,
    Legendary,
    Boss,
    Alpha,
    Totem;


    public static SpawnLocation[] getSpawnLocations(ArrayList<String> list) {
        SpawnLocation[] locations = new SpawnLocation[list.size()];
        int i = 0;
        for (String s : list) {
            for (SpawnLocation sp : SpawnLocation.values()) {
                if (!sp.toString().equalsIgnoreCase(s)) continue;
                locations[i++] = sp;
            }
        }
        return locations;
    }

    public static SpawnLocation getSpawnLocation(String location) {
        for (SpawnLocation sp : SpawnLocation.values()) {
            if (!sp.toString().equalsIgnoreCase(location)) continue;
            return sp;
        }
        return null;
    }

    public static SpawnLocation getFromIndex(int integer) {
        try {
            return SpawnLocation.values()[integer];
        }
        catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public static SpawnLocation nextLocation(SpawnLocation spawnLocation) {
        switch (spawnLocation) {
            case Land: {
                return Water;
            }
            case AirPersistent: {
                return Land;
            }
        }
        return SpawnLocation.getFromIndex((spawnLocation.ordinal() + 1) % SpawnLocation.values().length);
    }

    public static boolean contains(SpawnLocation[] locations, SpawnLocation location) {
        for (SpawnLocation l : locations) {
            if (l != location) continue;
            return true;
        }
        return false;
    }

    public static boolean containsOnly(SpawnLocation[] locations, SpawnLocation location) {
        if (locations.length == 1) {
            return SpawnLocation.contains(locations, location);
        }
        return false;
    }

    public String getLocalizedName() {
        return I18n.translateToLocal("enum.spawnlocation." + this.toString().toLowerCase());
    }
}

