/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.core;

import net.minecraftforge.server.permission.DefaultPermissionLevel;
import net.minecraftforge.server.permission.PermissionAPI;

public class PixelmonPermissions {
    public static final String ADMIN_STATUE = "pixelmon.admin.admin_statue";

    public static void registerPermissions() {
        PermissionAPI.registerNode(ADMIN_STATUE, DefaultPermissionLevel.OP, "Player can place statues that possess PokemonSpecs. Statues placed by players with this perm can't be deleted by those without this perm.");
    }
}

