/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.starter;

import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.HashMap;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;

public class SelectPokemonController {
    private static HashMap<UUID, EnumSpecies[]> hashPokemonArrays = new HashMap();

    public static boolean removePlayer(EntityPlayerMP p) {
        if (SelectPokemonController.isOnList(p)) {
            hashPokemonArrays.remove(p.getUniqueID());
            return true;
        }
        return false;
    }

    public static EnumSpecies[] getPokemonList(EntityPlayerMP p) {
        if (SelectPokemonController.isOnList(p)) {
            return hashPokemonArrays.get(p.getUniqueID());
        }
        return null;
    }

    public static boolean isOnList(EntityPlayerMP p) {
        return !hashPokemonArrays.isEmpty() && hashPokemonArrays.containsKey(p.getUniqueID());
    }
}

