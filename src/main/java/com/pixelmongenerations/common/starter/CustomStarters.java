/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.reflect.TypeToken
 */
package com.pixelmongenerations.common.starter;

import com.google.common.reflect.TypeToken;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.StarterList;
import com.pixelmongenerations.core.enums.EnumSpecies;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import net.minecraft.util.text.translation.I18n;

public class CustomStarters {
    private static List<String> starters = Arrays.asList("Bulbasaur", "Squirtle", "Charmander", "Chikorita", "Totodile", "Cyndaquil", "Treecko", "Mudkip", "Torchic", "Turtwig", "Piplup", "Chimchar", "Snivy", "Oshawott", "Tepig", "Chespin", "Froakie", "Fennekin", "Rowlet", "Popplio", "Litten", "Grookey", "Sobble", "Scorbunny");
    public static PokemonForm[] starterList = new PokemonForm[24];
    public static int starterLevel = 5;
    public static boolean shinyStarter = false;

    public static void loadConfig(CommentedConfigurationNode mainNode) throws ObjectMappingException {
        CommentedConfigurationNode customStarters = mainNode.getNode("Starters");
        starterLevel = Math.max(1, Math.min(customStarters.getNode("level").setComment(I18n.translateToLocal("pixelmon.config.starterLevel.comment")).getInt(5), PixelmonConfig.maxLevel));
        shinyStarter = customStarters.getNode("shiny").setComment(I18n.translateToLocal("pixelmon.config.starterShiny.comment")).getBoolean(false);
        starters = customStarters.getNode("starterList").setComment(I18n.translateToLocal("pixelmon.config.starterList.comment")).getList(new TypeToken<String>(){}, starters);
        int starterNum = 0;
        String pokemonName = null;
        int form = 0;
        for (String starter : starters) {
            Optional<EnumSpecies> pokemonOptional;
            if (starterNum > 24) break;
            if (starter.contains("-")) {
                if (!(starter.equalsIgnoreCase("ho-oh") || starter.equalsIgnoreCase("porygon-z") || starter.equalsIgnoreCase("hakamo-o") || starter.equalsIgnoreCase("jangmo-o") || starter.equalsIgnoreCase("kommo-o"))) {
                    String[] split = starter.split("-");
                    pokemonName = split[0];
                    form = split[1].equalsIgnoreCase("alolan") ? 3 : (split[1].equalsIgnoreCase("galarian") ? 10 : Integer.parseInt(split[1]));
                }
            } else {
                pokemonName = starter;
            }
            if (!(pokemonOptional = EnumSpecies.getFromName(pokemonName)).isPresent()) continue;
            EnumSpecies pokemonEnum = pokemonOptional.get();
            PokemonForm pokemonForm = new PokemonForm(pokemonEnum);
            if (form > 0) {
                pokemonForm.form = form;
            }
            CustomStarters.starterList[starterNum] = pokemonForm;
            ++starterNum;
        }
        StarterList.setStarterList(starterList);
    }
}

