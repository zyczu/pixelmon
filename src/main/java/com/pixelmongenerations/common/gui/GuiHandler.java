/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.gui;

import com.pixelmongenerations.client.gui.cookingpot.ContainerCookingPot;
import com.pixelmongenerations.client.gui.feeder.ContainerFeeder;
import com.pixelmongenerations.client.gui.machines.mechanicalanvil.ContainerMechanicalAnvil;
import com.pixelmongenerations.client.gui.trashcan.ContainerTrashcan;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCookingPot;
import com.pixelmongenerations.common.block.tileEntities.TileEntityFeeder;
import com.pixelmongenerations.common.block.tileEntities.TileEntityMechanicalAnvil;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.enums.EnumGui;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class GuiHandler {
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        EnumGui gui = EnumGui.getFromOrdinal(ID);
        switch (gui) {
            case MechaAnvil: {
                return new ContainerMechanicalAnvil(player.inventory, (TileEntityMechanicalAnvil)world.getTileEntity(new BlockPos(x, y, z)));
            }
            case TrashCan: {
                return new ContainerTrashcan(player.inventory);
            }
            case CookingPot: {
                return new ContainerCookingPot(player.inventory, (TileEntityCookingPot)world.getTileEntity(new BlockPos(x, y, z)));
            }
            case Feeder: {
                TileEntityFeeder feeder = (TileEntityFeeder)world.getTileEntity(new BlockPos(x, y, z));
                feeder.updateClientPokemon();
                return new ContainerFeeder(player.inventory, feeder);
            }
        }
        return new ContainerEmpty();
    }
}

