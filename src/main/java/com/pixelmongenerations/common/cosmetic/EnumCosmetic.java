/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableList
 */
package com.pixelmongenerations.common.cosmetic;

import com.google.common.collect.ImmutableList;
import com.pixelmongenerations.common.cosmetic.CosmeticCategory;
import com.pixelmongenerations.core.util.helper.CommonHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public enum EnumCosmetic {
    BetaBackpack(CosmeticCategory.Back, "backpack", "beta", true),
    BetaCap(CosmeticCategory.Head, "cap", "beta", true),
    BetaSash(CosmeticCategory.Back, "sash", "beta", true),
    NitroBackpack(CosmeticCategory.Back, "backpack", "nitro", true),
    NitroCap(CosmeticCategory.Head, "cap", "nitro", true),
    NitroSash(CosmeticCategory.Back, "sash", "nitro", true),
    HelperSash(CosmeticCategory.Back, "sash", "helper", true),
    TextureTeamSash(CosmeticCategory.Back, "sash", "textureteam", true),
    ModelerSash(CosmeticCategory.Back, "sash", "modeler", true),
    DeveloperSash(CosmeticCategory.Back, "sash", "developer", true),
    HeadDeveloperSash(CosmeticCategory.Back, "sash", "headdeveloper", true),
    AdminSash(CosmeticCategory.Back, "sash", "admin", true),
    OwnerSash(CosmeticCategory.Back, "sash", "owner", true),
    BulbasaurBackpack(CosmeticCategory.Back, "backpack", "bulbasaur", false),
    CharmanderBackpack(CosmeticCategory.Back, "backpack", "charmander", false),
    InstinctBackpack(CosmeticCategory.Back, "backpack", "instinct", false),
    MysticBackpack(CosmeticCategory.Back, "backpack", "mystic", false),
    SquirtleBackpack(CosmeticCategory.Back, "backpack", "squirtle", false),
    ValorBackpack(CosmeticCategory.Back, "backpack", "valor", false),
    WynautBackpack(CosmeticCategory.Back, "backpack", "wynaut", false),
    CharizardBackpack(CosmeticCategory.Back, "charizardbackpack", "charizard", false),
    EeveeBackpack(CosmeticCategory.Back, "eeveebackpack", "eevee", false),
    UmbreonBackpack(CosmeticCategory.Back, "eeveebackpack", "umbreon", false),
    GengarBackpack(CosmeticCategory.Back, "gengarbackpack", "gengar", false),
    HoopaBackpack(CosmeticCategory.Back, "hoopabackpack", "hoopa", false),
    MegaGengarBackpack(CosmeticCategory.Back, "megagengarbackpack", "megagengar", false),
    PikachuBackpack(CosmeticCategory.Back, "pikachubackpack", "pikachu", false),
    BlackPokepack(CosmeticCategory.Back, "pokepack", "black", false),
    BluePokepack(CosmeticCategory.Back, "pokepack", "blue", false),
    GreenPokepack(CosmeticCategory.Back, "pokepack", "green", false),
    LimePokepack(CosmeticCategory.Back, "pokepack", "lime", false),
    RedPokepack(CosmeticCategory.Back, "pokepack", "red", false),
    YellowPokepack(CosmeticCategory.Back, "pokepack", "yellow", false),
    InstinctSash(CosmeticCategory.Back, "sash", "instinct", false),
    MysticSash(CosmeticCategory.Back, "sash", "mystic", false),
    ValorSash(CosmeticCategory.Back, "sash", "valor", false),
    ArcanineCap(CosmeticCategory.Head, "cap", "arcanine", false),
    BlackAquaCap(CosmeticCategory.Head, "cap", "blackaqua", false),
    BlackBlueCap(CosmeticCategory.Head, "cap", "blackblue", false),
    BlackEyesCap(CosmeticCategory.Head, "cap", "blackeyes", false),
    BlackGreenCap(CosmeticCategory.Head, "cap", "blackgreen", false),
    BlackIceCap(CosmeticCategory.Head, "cap", "blackice", false),
    BlackMaroonCap(CosmeticCategory.Head, "cap", "blackmaroon", false),
    BlackPinkCap(CosmeticCategory.Head, "cap", "blackpink", false),
    BlackPink2Cap(CosmeticCategory.Head, "cap", "blackpink2", false),
    BlackPurpleCap(CosmeticCategory.Head, "cap", "blackpurple", false),
    BlackRainbowCap(CosmeticCategory.Head, "cap", "blackrainbow", false),
    BlackRedCap(CosmeticCategory.Head, "cap", "blackred", false),
    BlackWhiteCap(CosmeticCategory.Head, "cap", "blackwhite", false),
    BlueCap(CosmeticCategory.Head, "cap", "blue", false),
    GreenPurpleCap(CosmeticCategory.Head, "cap", "greenpurple", false),
    StrawberryCap(CosmeticCategory.Head, "cap", "strawberry", false),
    WhiteCap(CosmeticCategory.Head, "cap", "white", false),
    YellowCap(CosmeticCategory.Head, "cap", "yellow", false),
    YellowWhiteCap(CosmeticCategory.Head, "cap", "yellowwhite", false),
    Fedora(CosmeticCategory.Head, "fedora", "fedora", false),
    Fez(CosmeticCategory.Head, "fez", "fez", false),
    Tophat(CosmeticCategory.Head, "tophat", "tophat", false),
    StPatricksTophat(CosmeticCategory.Head, "tophat", "stpatricks", false),
    StrawHat(CosmeticCategory.Head, "strawhat", "strawhat", false),
    Crown(CosmeticCategory.Head, "crown", "crown", false),
    DetectiveHat(CosmeticCategory.Head, "detectivehat", "detectivehat", false),
    CowboyHat(CosmeticCategory.Head, "cowboyhat", "cowboyhat", false),
    LetsGoCapUmbreon(CosmeticCategory.Head, "letsgocap", "umbreon", false),
    LetsGoCapBlack(CosmeticCategory.Head, "letsgocap", "black", false),
    LetsGoCapFlareon(CosmeticCategory.Head, "letsgocap", "flareon", false),
    LetsGoCapGlaceon(CosmeticCategory.Head, "letsgocap", "glaceon", false),
    LetsGoCapSylveon(CosmeticCategory.Head, "letsgocap", "sylveon", false),
    BlueThickGlasses(CosmeticCategory.Face, "thickglasses", "blue", false),
    BlackSunglasses(CosmeticCategory.Face, "sunglasses", "black", false),
    BrownSunglasses(CosmeticCategory.Face, "sunglasses", "brown", false),
    WhiteSunglasses(CosmeticCategory.Face, "sunglasses", "white", false),
    PinkSunglasses(CosmeticCategory.Face, "sunglasses", "pink", false),
    PurpleAviatorShades(CosmeticCategory.Face, "aviatorshades", "purple", false),
    AmberAviatorShades(CosmeticCategory.Face, "aviatorshades", "amber", false),
    BlueAviatorShades(CosmeticCategory.Face, "aviatorshades", "blue", false),
    PinkAviatorShades(CosmeticCategory.Face, "aviatorshades", "pink", false),
    SilverAviatorShades(CosmeticCategory.Face, "aviatorshades", "silver", false),
    WhiteSportyBackpack(CosmeticCategory.Back, "sportybackpack", "white", false),
    WhiteScoutBackpack(CosmeticCategory.Back, "scoutbackpack", "white", false),
    WhiteLeatherBackpack(CosmeticCategory.Back, "leatherbackpack", "white", false),
    PinkFlowers(CosmeticCategory.Head, "flowers", "pink", false),
    BlueFlowers(CosmeticCategory.Head, "flowers", "blue", false),
    GreenFlowers(CosmeticCategory.Head, "flowers", "green", false),
    EggSash(CosmeticCategory.Back, "sash", "eggbase", false),
    ElectricEggHat(CosmeticCategory.Head, "egghat", "electric", false),
    FireEggHat(CosmeticCategory.Head, "egghat", "fire", false),
    GreenEggHat(CosmeticCategory.Head, "egghat", "green", false),
    PhioneEggHat(CosmeticCategory.Head, "egghat", "phione", false),
    StarEggHat(CosmeticCategory.Head, "egghat", "star", false),
    SwirlEggHat(CosmeticCategory.Head, "egghat", "swirl", false),
    TogepiEggHat(CosmeticCategory.Head, "egghat", "togepi", false),
    YellowEggHat(CosmeticCategory.Head, "egghat", "yellow", false),
    DragonairHat(CosmeticCategory.Head, "dragonairhat", "dragonairhat", true),
    VenusaurFlower(CosmeticCategory.Back, "venusaurflower", "normal", true),
    ShinyVenusaurFlower(CosmeticCategory.Back, "venusaurflower", "shiny", true),
    VileplumeHat(CosmeticCategory.Head, "vileplumehat", "vileplumehat", true),
    NinetalesTail(CosmeticCategory.Back, "ninetalestail", "ninetalestail", true),
    ButterfreeWings(CosmeticCategory.Back, "butterfreewings", "butterfreewings", true),
    BellossomFlowers(CosmeticCategory.Head, "bellossomflowers", "bellossomflowers", true),
    KlangGears(CosmeticCategory.Head, "klanggears", "normal", true),
    ShinyKlangGears(CosmeticCategory.Head, "klanggears", "shiny", true),
    RaltsHead(CosmeticCategory.Head, "raltshead", "raltshead", true),
    AegislashShield(CosmeticCategory.Back, "aegislashshield", "normal", true),
    ShinyAegislashShield(CosmeticCategory.Back, "aegislashshield", "shiny", true),
    ArceusHead(CosmeticCategory.Head, "arceushead", "normal", true);

    private CosmeticCategory category;
    private String modelName;
    private String textureName;
    public boolean exclusive;
    private static List<String> cosmeticNameList;

    private EnumCosmetic(CosmeticCategory category, String modelName, String textureName, boolean exclusive) {
        this.category = category;
        this.modelName = modelName;
        this.textureName = textureName;
        this.exclusive = exclusive;
    }

    public String getName() {
        return CommonHelper.spaceBetweenCaps(this.name());
    }

    public CosmeticCategory getCategory() {
        return this.category;
    }

    public String getModelName() {
        return this.modelName;
    }

    public String getTextureName() {
        return this.textureName;
    }

    public boolean isExclusive() {
        return this.exclusive;
    }

    public static Optional<EnumCosmetic> getCosmetic(String name) {
        for (EnumCosmetic cosmetic : EnumCosmetic.values()) {
            if (!cosmetic.name().equalsIgnoreCase(name)) continue;
            return Optional.of(cosmetic);
        }
        return Optional.empty();
    }

    public static ImmutableList<String> getNameList() {
        return ImmutableList.copyOf(cosmeticNameList);
    }

    static {
        cosmeticNameList = new ArrayList<String>();
        for (EnumCosmetic enumCosmetic : EnumCosmetic.values()) {
            if (enumCosmetic.isExclusive()) continue;
            cosmeticNameList.add(enumCosmetic.name());
        }
    }
}

