/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.cosmetic;

public enum CosmeticCategory {
    Head,
    Face,
    Neck,
    Back,
    Body,
    Shirt,
    Arm;


    public static CosmeticCategory getFromIndexSafe(int index) {
        return CosmeticCategory.values()[index > CosmeticCategory.values().length || index < 0 ? 0 : index];
    }
}

