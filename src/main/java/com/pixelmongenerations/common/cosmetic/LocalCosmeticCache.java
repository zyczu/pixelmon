/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.cosmetic;

import com.pixelmongenerations.common.cosmetic.CosmeticData;
import java.util.HashMap;
import java.util.UUID;

public class LocalCosmeticCache {
    private static HashMap<UUID, CosmeticData> COSMETICS_PLAYER = new HashMap();

    public static CosmeticData getCosmeticData(UUID uuid) {
        return COSMETICS_PLAYER.get(uuid);
    }

    public static void updateCosmeticData(CosmeticData cosmeticData) {
        COSMETICS_PLAYER.put(cosmeticData.getEntityId(), cosmeticData);
    }
}

