/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.achievement;

import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.TrainerParticipant;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.stats.StatBase;
import net.minecraft.util.text.TextComponentTranslation;

public class PixelmonAchievements {
    private static boolean called = false;
    public static final StatBase itemFinderChieve = PixelmonAchievements.getStat("achievement.itemFinder", "ItemFinderChieve", null).registerStat();
    public static final StatBase hiddenLootChieve = PixelmonAchievements.getStat("achievement.itemhiddenloot", "HiddenPokeLootChieve", Blocks.GLASS_PANE, itemFinderChieve).registerStat();
    public static final StatBase normalLootChieve = PixelmonAchievements.getStat("achievement.normalloot", "NormalPokeLootChieve", PixelmonBlocks.pokeChest, null).registerStat();
    public static final StatBase masterBallChieve = PixelmonAchievements.getStat("achievement.lootmasterball", "LootMasterBallChieve", null).registerStat();
    public static final StatBase grottoChieve = PixelmonAchievements.getStat("achievement.grotto", "GrottoChieve", Blocks.SAPLING, null).registerStat();
    public static final StatBase apricornChieve = PixelmonAchievements.getStat("achievement.apricorn", "ApricornChieve", null).registerStat();
    public static final StatBase pokeballChieve = PixelmonAchievements.getStat("achievement.pokeball", "PokeballChieve", apricornChieve).registerStat();
    public static final StatBase legendaryChieve = PixelmonAchievements.getStat("achievement.catchlegendary", "LegendaryChieve", pokeballChieve).registerStat();
    public static final StatBase shinyChieve = PixelmonAchievements.getStat("achievement.catchshiny", "ShinyChieve", pokeballChieve).registerStat();
    public static final StatBase firstcatchChieve = PixelmonAchievements.getStat("achievement.firstcatch", "FirstCatchChieve", pokeballChieve).registerStat();
    public static final StatBase trainerChieve = PixelmonAchievements.getStat("achievement.beattrainer", "TrainerChieve", null).registerStat();
    public static final StatBase pvpChieve = PixelmonAchievements.getStat("achievement.winpvp", "PvPChieve", null).registerStat();
    public static final StatBase dex1Chieve = PixelmonAchievements.getStat("achievement.completedex1", "dex1Chieve", null).registerStat();
    public static final StatBase dex10Chieve = PixelmonAchievements.getStat("achievement.completedex10", "dex10Chieve", dex1Chieve).registerStat();
    public static final StatBase dex20Chieve = PixelmonAchievements.getStat("achievement.completedex20", "dex20Chieve", dex10Chieve).registerStat();
    public static final StatBase dex30Chieve = PixelmonAchievements.getStat("achievement.completedex30", "dex30Chieve", dex20Chieve).registerStat();
    public static final StatBase dex40Chieve = PixelmonAchievements.getStat("achievement.completedex40", "dex40Chieve", dex30Chieve).registerStat();
    public static final StatBase dex50Chieve = PixelmonAchievements.getStat("achievement.completedex50", "dex50Chieve", dex40Chieve).registerStat();
    public static final StatBase dex60Chieve = PixelmonAchievements.getStat("achievement.completedex60", "dex60Chieve", dex50Chieve).registerStat();
    public static final StatBase dex70Chieve = PixelmonAchievements.getStat("achievement.completedex70", "dex70Chieve", dex60Chieve).registerStat();
    public static final StatBase dex80Chieve = PixelmonAchievements.getStat("achievement.completedex80", "dex80Chieve", dex70Chieve).registerStat();
    public static final StatBase dex90Chieve = PixelmonAchievements.getStat("achievement.completedex90", "dex90Chieve", dex80Chieve).registerStat();
    public static final StatBase dex100Chieve = PixelmonAchievements.getStat("achievement.completedex100", "dex100Chieve", dex90Chieve).registerStat();
    public static StatBase teammagma = PixelmonAchievements.getStat("achievement.teammagma", "teammagma", null).registerStat();
    public static StatBase teamaqua = PixelmonAchievements.getStat("achievement.teamaqua", "teamaqua", null).registerStat();
    public static StatBase teammagma1 = PixelmonAchievements.getStat("achievement.teammagma1", "teammagma1", teammagma).registerStat();
    public static StatBase teamaqua1 = PixelmonAchievements.getStat("achievement.teamaqua1", "teamaqua1", teamaqua).registerStat();
    public static StatBase getSilicon = PixelmonAchievements.getStat("achievement.getsilicon", "getSilicon", null).registerStat();
    public static StatBase pokeGift = PixelmonAchievements.getStat("achievement.pokegift", "pokeGift", PixelmonBlocks.pokegiftBlock, null).registerStat();
    public static StatBase givenPokeGift = PixelmonAchievements.getStat("achievement.givenpokegift", "givenPokeGift", PixelmonBlocks.pokegiftBlock, null).registerStat();
    public static StatBase teammagma2 = PixelmonAchievements.getStat("achievement.teammagma2", "teammagma2", teammagma1).registerStat();
    public static StatBase teammagma3 = PixelmonAchievements.getStat("achievement.teammagma3", "teammagma3", teammagma2).registerStat();
    public static StatBase teamaqua2 = PixelmonAchievements.getStat("achievement.teamaqua2", "teamaqua2", teamaqua1).registerStat();
    public static StatBase teamaqua3 = PixelmonAchievements.getStat("achievement.teamaqua3", "teamaqua3", teamaqua2).registerStat();
    public static StatBase teamrocket = PixelmonAchievements.getStat("achievement.teamrocket", "teamrocket", null).registerStat();
    public static StatBase teamrocket1 = PixelmonAchievements.getStat("achievement.teamrocket1", "teamrocket1", teamrocket).registerStat();
    public static StatBase teamrocket2 = PixelmonAchievements.getStat("achievement.teamrocket2", "teamrocket2", teamrocket1).registerStat();
    public static StatBase teamrocket3 = PixelmonAchievements.getStat("achievement.teamrocket3", "teamrocket3", teamrocket2).registerStat();
    public static StatBase teamgalactic = PixelmonAchievements.getStat("achievement.teamgalactic", "teamgalactic", null).registerStat();
    public static StatBase teamgalactic1 = PixelmonAchievements.getStat("achievement.teamgalactic1", "teamgalactic1", teamgalactic).registerStat();
    public static StatBase teamgalactic2 = PixelmonAchievements.getStat("achievement.teamgalactic2", "teamgalactic2", teamgalactic1).registerStat();
    public static StatBase teamgalactic3 = PixelmonAchievements.getStat("achievement.teamgalactic3", "teamgalactic3", teamgalactic2).registerStat();
    public static StatBase teamplasma = PixelmonAchievements.getStat("achievement.teamplasma", "teamplasma", null).registerStat();
    public static StatBase teamplasma1 = PixelmonAchievements.getStat("achievement.teamplasma1", "teamplasma1", teamplasma).registerStat();
    public static StatBase teamplasma2 = PixelmonAchievements.getStat("achievement.teamplasma2", "teamplasma2", teamplasma1).registerStat();
    public static StatBase teamplasma3 = PixelmonAchievements.getStat("achievement.teamplasma3", "teamplasma3", teamplasma2).registerStat();

    private static StatBase getStat(String lang, String name, StatBase parent) {
        return new StatBase(name, new TextComponentTranslation(lang, new Object[0]));
    }

    private static StatBase getStat(String lang, String name, Block item, StatBase parent) {
        return new StatBase(name, new TextComponentTranslation(lang, new Object[0]));
    }

    public static void SetupAchievements() {
        if (called) {
            return;
        }
        called = true;
    }

    public static void captureChieves(EntityPlayerMP player, EntityPixelmon pokemon) {
        String name = pokemon.getPokemonName();
        player.addStat(firstcatchChieve, 1);
        EnumSpecies.legendaries.stream().filter(name::equalsIgnoreCase).forEach(p -> player.addStat(legendaryChieve, 1));
        if (pokemon.isShiny()) {
            player.addStat(shinyChieve, 1);
        }
        PixelmonAchievements.pokedexChieves(player);
    }

    public static void battleChieves(EntityPlayerMP player, BattleParticipant opponent, boolean win) {
        if (win) {
            if (opponent instanceof PlayerParticipant) {
                player.addStat(pvpChieve, 1);
            } else if (opponent instanceof TrainerParticipant) {
                player.addStat(trainerChieve, 1);
            }
        }
    }

    public static void pokedexChieves(EntityPlayerMP player) {
        Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (optstorage.isPresent()) {
            // empty if block
        }
    }
}

