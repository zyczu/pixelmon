/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  com.google.common.collect.Maps
 *  com.google.common.collect.Sets
 *  com.google.gson.JsonDeserializationContext
 *  com.google.gson.JsonObject
 */
package com.pixelmongenerations.common.achievement.triggers;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.minecraft.advancements.ICriterionTrigger;
import net.minecraft.advancements.PlayerAdvancements;
import net.minecraft.advancements.critereon.AbstractCriterionInstance;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;

public class PokedexTrigger
implements ICriterionTrigger<PokedexTrigger.Instance> {
    private static final ResourceLocation ID = new ResourceLocation("pixelmon:pokedex_trigger");
    private final Map<PlayerAdvancements, Listeners> listeners = Maps.newHashMap();

    @Override
    public ResourceLocation getId() {
        return ID;
    }

    @Override
    public void addListener(PlayerAdvancements playerAdvancementsIn, ICriterionTrigger.Listener<Instance> listener) {
        Listeners pokedextrigger$listeners = this.listeners.get(playerAdvancementsIn);
        if (pokedextrigger$listeners == null) {
            pokedextrigger$listeners = new Listeners(playerAdvancementsIn);
            this.listeners.put(playerAdvancementsIn, pokedextrigger$listeners);
        }
        pokedextrigger$listeners.add(listener);
    }

    @Override
    public void removeListener(PlayerAdvancements playerAdvancementsIn, ICriterionTrigger.Listener<Instance> listener) {
        Listeners pokedextrigger$listeners = this.listeners.get(playerAdvancementsIn);
        if (pokedextrigger$listeners != null) {
            pokedextrigger$listeners.remove(listener);
            if (pokedextrigger$listeners.isEmpty()) {
                this.listeners.remove(playerAdvancementsIn);
            }
        }
    }

    @Override
    public void removeAllListeners(PlayerAdvancements playerAdvancementsIn) {
        this.listeners.remove(playerAdvancementsIn);
    }

    @Override
    public Instance deserializeInstance(JsonObject json, JsonDeserializationContext context) {
        int percent = json.has("percent") ? JsonUtils.getInt(json, "percent") : 0;
        return new Instance(percent);
    }

    public void trigger(EntityPlayerMP player) {
        Listeners pokedextrigger$listeners = this.listeners.get(player.getAdvancements());
        if (pokedextrigger$listeners != null) {
            pokedextrigger$listeners.trigger(player);
        }
    }

    public static class Instance
    extends AbstractCriterionInstance {
        private final int percent;

        public Instance(int percent) {
            super(ID);
            this.percent = percent;
        }

        public boolean test(EntityPlayerMP player) {
            if (PixelmonStorage.pokeBallManager.getPlayerStorage(player).isPresent()) {
                PlayerStorage store = PixelmonStorage.pokeBallManager.getPlayerStorage(player).get();
                float caught = store.pokedex.countCaught();
                return caught / (float)EnumSpecies.values().length >= (float)this.percent;
            }
            return false;
        }
    }

    static class Listeners {
        private final PlayerAdvancements playerAdvancements;
        private final Set<ICriterionTrigger.Listener<Instance>> listeners = Sets.newHashSet();

        public Listeners(PlayerAdvancements playerAdvancementsIn) {
            this.playerAdvancements = playerAdvancementsIn;
        }

        public boolean isEmpty() {
            return this.listeners.isEmpty();
        }

        public void add(ICriterionTrigger.Listener<Instance> listener) {
            this.listeners.add(listener);
        }

        public void remove(ICriterionTrigger.Listener<Instance> listener) {
            this.listeners.remove(listener);
        }

        public void trigger(EntityPlayerMP player) {
            List<ICriterionTrigger.Listener<Instance>> list = null;
            for (ICriterionTrigger.Listener<Instance> listener : this.listeners) {
                if (list == null) {
                    list = Lists.newArrayList();
                }
                list.add(listener);
            }
            if (list != null) {
                for (ICriterionTrigger.Listener<Instance> listener : list) {
                    if (!listener.getCriterionInstance().test(player)) continue;
                    listener.grantCriterion(this.playerAdvancements);
                }
            }
        }
    }
}

