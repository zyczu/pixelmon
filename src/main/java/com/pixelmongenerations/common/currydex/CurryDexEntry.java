/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.currydex;

import com.pixelmongenerations.core.enums.EnumCurryTasteRating;
import com.pixelmongenerations.core.enums.EnumCurryType;
import com.pixelmongenerations.core.enums.EnumFlavor;
import java.time.Instant;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.translation.I18n;

public class CurryDexEntry {
    public Instant instant;
    public String pokemonName;
    public ResourceLocation biome;
    public BlockPos pos;
    public boolean newEntry = true;
    public EnumCurryType type;
    public EnumFlavor flavor;
    public EnumCurryTasteRating rating;

    public static CurryDexEntry fromNbt(NBTTagCompound entry) {
        CurryDexEntry curryDexEntry = new CurryDexEntry();
        curryDexEntry.instant = Instant.ofEpochMilli(entry.getLong("instant"));
        curryDexEntry.pokemonName = entry.getString("pokemonName");
        curryDexEntry.biome = new ResourceLocation(entry.getString("biome"));
        curryDexEntry.pos = BlockPos.fromLong(entry.getLong("pos"));
        curryDexEntry.rating = EnumCurryTasteRating.fromId(entry.getInteger("rating"));
        curryDexEntry.type = EnumCurryType.getCurryTypeFromIndex(entry.getInteger("type"));
        curryDexEntry.flavor = EnumFlavor.geFlavorFromIndex(entry.getInteger("flavor"));
        curryDexEntry.newEntry = entry.getBoolean("newEntry");
        return curryDexEntry;
    }

    public NBTTagCompound toNbt() {
        NBTTagCompound compound = new NBTTagCompound();
        compound.setLong("instant", this.instant.toEpochMilli());
        compound.setString("pokemonName", this.pokemonName);
        compound.setString("biome", this.biome.toString());
        compound.setLong("pos", this.pos.toLong());
        compound.setInteger("rating", this.rating.ordinal());
        compound.setInteger("type", this.type.getIndex());
        compound.setInteger("flavor", this.flavor.getIndex());
        compound.setBoolean("newEntry", this.newEntry);
        return compound;
    }

    public Instant getInstant() {
        return this.instant;
    }

    public void setInstant(Instant instant) {
        this.instant = instant;
    }

    public String toString() {
        String name = I18n.translateToLocal("item.curry.name");
        if (this.type != EnumCurryType.None) {
            name = this.type.getLocalizedName() + " " + name;
        }
        if (this.flavor != EnumFlavor.None && this.type != EnumCurryType.Gigantamax) {
            name = this.flavor.getLocalizedName() + " " + name;
        }
        return name;
    }

    public String getDescription() {
        return I18n.translateToLocal("gui.currydex.description." + (this.flavor != EnumFlavor.None ? this.flavor.name().toLowerCase() + "_" : "") + (this.type != EnumCurryType.None ? this.type.name().toLowerCase() + "_" : "") + "curry");
    }
}

