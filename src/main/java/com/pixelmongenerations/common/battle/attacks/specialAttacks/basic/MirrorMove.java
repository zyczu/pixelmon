/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import java.util.ArrayList;

public class MirrorMove
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.lastAttack != null) {
            if (!target.lastAttack.isAttack("Mirror Move", "Sketch")) {
                user.useTempAttack(target.lastAttack);
                return AttackResult.ignore;
            }
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        bestOpponentChoices = MoveChoice.getAffectedChoices(userChoice, bestOpponentChoices);
        for (PixelmonWrapper target : userChoice.targets) {
            ArrayList<MoveChoice> possibleChoices;
            if (MoveChoice.canOutspeed(bestOpponentChoices, pw, userChoice.createList())) {
                possibleChoices = MoveChoice.createChoicesFromChoices(pw, bestOpponentChoices, false);
            } else if (target.lastAttack != null) {
                possibleChoices = target.lastAttack.createMoveChoices(pw, false);
            } else {
                return;
            }
            pw.getBattleAI().weightRandomMove(pw, userChoice, possibleChoices);
        }
    }
}

