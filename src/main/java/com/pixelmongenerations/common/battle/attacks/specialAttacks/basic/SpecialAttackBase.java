/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public abstract class SpecialAttackBase
extends EffectBase {
    public SpecialAttackBase() {
    }

    public SpecialAttackBase(boolean persists) {
        super(persists);
    }

    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        return AttackResult.proceed;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
    }

    public void applyAfterEffect(PixelmonWrapper user) {
    }

    @Override
    public boolean cantMiss(PixelmonWrapper user) {
        return false;
    }

    public float modifyPriority(PixelmonWrapper pw) {
        return pw.priority;
    }
}

