/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableMap
 *  com.google.common.collect.ImmutableMap$Builder
 */
package com.pixelmongenerations.common.battle.attacks;

import com.google.common.collect.ImmutableMap;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.AttackBase;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.attacks.RegularAttackBase;
import com.pixelmongenerations.common.battle.attacks.TargetingInfo;
import com.pixelmongenerations.common.battle.attacks.Value;
import com.pixelmongenerations.common.battle.attacks.ValueType;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers.CriticalHit;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.GuardianOfAlola;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.WeatherBall;
import com.pixelmongenerations.common.battle.attacks.zeffects.CompleteHeal;
import com.pixelmongenerations.common.battle.attacks.zeffects.CriticalHitRaise;
import com.pixelmongenerations.common.battle.attacks.zeffects.GainAttention;
import com.pixelmongenerations.common.battle.attacks.zeffects.RaiseStats;
import com.pixelmongenerations.common.battle.attacks.zeffects.ResetStats;
import com.pixelmongenerations.common.battle.attacks.zeffects.ZCurse;
import com.pixelmongenerations.common.battle.attacks.zeffects.ZEffect;
import com.pixelmongenerations.common.battle.status.Paralysis;
import com.pixelmongenerations.common.battle.status.PsychicTerrain;
import com.pixelmongenerations.common.battle.status.TerrainRemoval;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

public class ZAttackBase
extends AttackBase {
    private static EnumMap<EnumType, Function<AttackBase, ZAttackBase>> typeMap;
    private static Map<String, ZEffect> statusEffects;
    private static BiFunction<EnumType, String, Function<AttackBase, ZAttackBase>> attack;
    public ZEffect zeffect;

    public static Map<String, ZAttackBase> generate(AttackBase base) {
        ZAttackBase zAttackBase;
        ImmutableMap.Builder mapBuilder = ImmutableMap.builder();
        System.out.println("Generating Z moves for " + base.getUnlocalizedName());
        switch (base.getUnlocalizedName()) {
            case "Struggle": {
                return mapBuilder.build();
            }
            case "Thunderbolt": {
                mapBuilder.put((Object)"Raichu", (Object)ZAttackBase.specialCase("Stroked Sparksurfer", EnumType.Electric, AttackCategory.Special, 175, new Paralysis())).put((Object)"Pikachu", (Object)ZAttackBase.specialCase("10,000,000 Volt Thunderbolt", EnumType.Electric, AttackCategory.Special, 195, new CriticalHit(new Value(2, ValueType.WholeNumber))));
                break;
            }
            case "Spirit Shackle": {
                mapBuilder.put((Object)"Decidueye", (Object)ZAttackBase.specialCase("Sinister Arrow Raid", EnumType.Ghost, AttackCategory.Physical, 180, new EffectBase[0]));
                break;
            }
            case "Last Resort": {
                mapBuilder.put((Object)"Eevee", (Object)ZAttackBase.specialCase("Extreme Evoboost", EnumType.Normal, AttackCategory.Status, 0, new EffectBase[0]));
                break;
            }
            case "Darkest Lariat": {
                mapBuilder.put((Object)"Incineroar", (Object)ZAttackBase.specialCase("Malicious Moonsault", EnumType.Dark, AttackCategory.Physical, 180, new EffectBase[0]));
                break;
            }
            case "Clanging Scales": {
                mapBuilder.put((Object)"Kommoo", (Object)ZAttackBase.specialCase("Clangorous Soulblaze", EnumType.Dragon, AttackCategory.Special, 185, new EffectBase[0]));
                break;
            }
            case "Moongeist Beam": {
                mapBuilder.put((Object)"Lunala", (Object)ZAttackBase.specialCase("Menacing Moonraze Maelstrom", EnumType.Ghost, AttackCategory.Special, 200, new EffectBase[0]));
                break;
            }
            case "Stone Edge": {
                mapBuilder.put((Object)"Lycanroc", (Object)ZAttackBase.specialCase("Splintered Stormshards", EnumType.Rock, AttackCategory.Physical, 190, new TerrainRemoval()));
                break;
            }
            case "Spectral Thief": {
                mapBuilder.put((Object)"Marshadow", (Object)ZAttackBase.specialCase("Soul-Stealing 7-Star-Strike", EnumType.Ghost, AttackCategory.Special, 195, new EffectBase[0]));
                break;
            }
            case "Psychic": {
                mapBuilder.put((Object)"Mew", (Object)ZAttackBase.specialCase("Genesis Supernova", EnumType.Psychic, AttackCategory.Special, 185, new PsychicTerrain()));
                break;
            }
            case "Play Rough": {
                mapBuilder.put((Object)"Mimikyu", (Object)ZAttackBase.specialCase("Let's Snuggle Forever", EnumType.Fairy, AttackCategory.Physical, 190, new EffectBase[0]));
                break;
            }
            case "Volt Tackle": {
                mapBuilder.put((Object)"Pikachu", (Object)ZAttackBase.specialCase("Catastropika", EnumType.Electric, AttackCategory.Physical, 210, new EffectBase[0]));
                break;
            }
            case "Giga Impact": {
                mapBuilder.put((Object)"Snorlax", (Object)ZAttackBase.specialCase("Pulverizing Pancake", EnumType.Normal, AttackCategory.Physical, 210, new EffectBase[0]));
                break;
            }
            case "Sparkling Aria": {
                mapBuilder.put((Object)"Primarina", (Object)ZAttackBase.specialCase("Oceanic Operatta", EnumType.Water, AttackCategory.Special, 195, new EffectBase[0]));
                break;
            }
            case "Sunsteel Strike": {
                mapBuilder.put((Object)"Solgaleo", (Object)ZAttackBase.specialCase("Searing Sunraze Smash", EnumType.Steel, AttackCategory.Physical, 200, new EffectBase[0]));
                break;
            }
            case "Nature's Madness": {
                mapBuilder.put((Object)"Tapu", (Object)ZAttackBase.specialCase("Guardian of Alola", EnumType.Fairy, AttackCategory.Physical, 0, new GuardianOfAlola()));
                break;
            }
            case "Photon Geyser": {
                mapBuilder.put((Object)"Necrozma", (Object)ZAttackBase.specialCase("Light That Burns The Sky", EnumType.Psychic, AttackCategory.Special, 200, new EffectBase[0]));
            }
        }
        if (base.attackCategory != AttackCategory.Status) {
            zAttackBase = typeMap.get((Object)base.attackType).apply(base);
            if (base.getUnlocalizedName().equals("Weather Ball")) {
                zAttackBase.effects.add(new WeatherBall());
            }
            mapBuilder.put((Object)base.attackType.name(), (Object)zAttackBase);
        } else {
            zAttackBase = new ZAttackBase();
            zAttackBase.setAttackName("Z-" + base.getUnlocalizedName());
            zAttackBase.attackType = base.attackType;
            zAttackBase.attackCategory = base.attackCategory;
            zAttackBase.basePower = base.basePower;
            zAttackBase.accuracy = base.accuracy;
            zAttackBase.setMakesContact(base.getMakesContact());
            zAttackBase.animations = base.animations;
            zAttackBase.targetingInfo = base.targetingInfo;
            zAttackBase.effects = base.effects;
            zAttackBase.zeffect = statusEffects.getOrDefault(base.getUnlocalizedName(), null);
            mapBuilder.put((Object)base.attackType.name(), (Object)zAttackBase);
        }
        return mapBuilder.build();
    }

    private static ZAttackBase specialCase(String name, EnumType type, AttackCategory category, int basePower, EffectBase ... effects) {
        ZAttackBase base = new ZAttackBase();
        base.effects = new ArrayList();
        base.effects.addAll(Arrays.asList(effects));
        base.animations = new ArrayList();
        base.targetingInfo = new TargetingInfo();
        base.targetingInfo.hitsAdjacentAlly = true;
        base.targetingInfo.hitsAdjacentFoe = true;
        base.targetingInfo.hitsOppositeFoe = true;
        base.accuracy = -1;
        base.attackType = type;
        base.attackCategory = category;
        base.basePower = basePower;
        base.setAttackName(name);
        return base;
    }

    private static int adjustPower(AttackBase attack) {
        switch (attack.getUnlocalizedName()) {
            case "Mega Drain": {
                return 120;
            }
            case "Weather Ball": 
            case "Hex": {
                return 160;
            }
            case "Gear Grind": {
                return 180;
            }
            case "V-create": {
                return 220;
            }
            case "Flying Press": {
                return 170;
            }
            case "Core Enforcer": {
                return 140;
            }
        }
        int power = attack.basePower;
        if (power >= 0 && power <= 55) {
            return 100;
        }
        if (power >= 60 && power <= 65) {
            return 120;
        }
        if (power >= 70 && power <= 75) {
            return 140;
        }
        if (power >= 80 && power <= 85) {
            return 160;
        }
        if (power >= 90 && power <= 95) {
            return 175;
        }
        if (power == 100) {
            return 180;
        }
        if (power == 110) {
            return 185;
        }
        if (power >= 120 && power <= 125) {
            return 190;
        }
        if (power == 130) {
            return 195;
        }
        if (power >= 140) {
            return 200;
        }
        return power;
    }

    public static Attack getZMove(Attack attack, boolean isMetronome) {
        String key = "Any";
        if (attack.getAttackBase().getUnlocalizedName().equals("Thunderbolt")) {
            key = RandomHelper.rand.nextBoolean() ? "Pikachu" : "Raichu";
        }
        String finalKey = key;
        return Optional.of(attack.getAttackBase()).filter(RegularAttackBase.class::isInstance).map(RegularAttackBase.class::cast).flatMap(a -> a.getZAttackBase(finalKey)).map(a -> {
            if (isMetronome) {
                a.zeffect = null;
            }
            return a;
        }).map(Attack::new).orElse(attack);
    }

    static {
        BiFunction<EnumType, String, Function<AttackBase, ZAttackBase>> attack = ((BiFunction<EnumType, String, ZAttackBase>)(type, name) -> {
            ZAttackBase base = new ZAttackBase();
            base.effects = new ArrayList();
            base.animations = new ArrayList();
            base.targetingInfo = new TargetingInfo();
            base.targetingInfo.hitsAdjacentAlly = true;
            base.targetingInfo.hitsAdjacentFoe = true;
            base.targetingInfo.hitsOppositeFoe = true;
            base.accuracy = -1;
            base.attackType = type;
            base.setAttackName((String)name);
            return base;
        }).andThen(zAttack -> attackBase -> {
            if (attackBase == null) {
                zAttack.attackCategory = AttackCategory.Status;
                zAttack.basePower = 100;
                return zAttack;
            }
            zAttack.attackCategory = attackBase.attackCategory;
            zAttack.basePower = ZAttackBase.adjustPower(attackBase);
            return zAttack;
        });
        typeMap = new EnumMap<>(EnumType.class);
        typeMap.put(EnumType.Bug, attack.apply(EnumType.Bug, "Savage Spin-Out"));
        typeMap.put(EnumType.Dark, attack.apply(EnumType.Dark, "Black Hole Eclipse"));
        typeMap.put(EnumType.Dragon, attack.apply(EnumType.Dragon, "Devastating Drake"));
        typeMap.put(EnumType.Electric, attack.apply(EnumType.Electric, "Gigavolt Havoc"));
        typeMap.put(EnumType.Fairy, attack.apply(EnumType.Fairy, "Twinkle Tackle"));
        typeMap.put(EnumType.Fighting, attack.apply(EnumType.Fighting, "All-Out Pummeling"));
        typeMap.put(EnumType.Fire, attack.apply(EnumType.Fire, "Inferno Overdrive"));
        typeMap.put(EnumType.Flying, attack.apply(EnumType.Flying, "Supersonic Skystrike"));
        typeMap.put(EnumType.Ghost, attack.apply(EnumType.Ghost, "Never-Ending Nightmare"));
        typeMap.put(EnumType.Grass, attack.apply(EnumType.Grass, "Bloom Doom"));
        typeMap.put(EnumType.Ground, attack.apply(EnumType.Ground, "Tectonic Rage"));
        typeMap.put(EnumType.Ice, attack.apply(EnumType.Ice, "Subzero Slammer"));
        typeMap.put(EnumType.Normal, attack.apply(EnumType.Normal, "Breakneck Blitz"));
        typeMap.put(EnumType.Poison, attack.apply(EnumType.Poison, "Acid Downpour"));
        typeMap.put(EnumType.Psychic, attack.apply(EnumType.Psychic, "Shattered Psyche"));
        typeMap.put(EnumType.Rock, attack.apply(EnumType.Rock, "Continental Crush"));
        typeMap.put(EnumType.Steel, attack.apply(EnumType.Steel, "Corkscrew Crash"));
        typeMap.put(EnumType.Water, attack.apply(EnumType.Water, "Hydro Vortex"));
        statusEffects = new HashMap<String, ZEffect>();
        statusEffects.put("Acid Armor", new ResetStats());
        statusEffects.put("Acupressure", new CriticalHitRaise());
        statusEffects.put("After You", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Agility", new ResetStats());
        statusEffects.put("Ally Switch", new RaiseStats(2, StatsType.Speed));
        statusEffects.put("Amnesia", new ResetStats());
        statusEffects.put("Aqua Ring", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Aromatherapy", new CompleteHeal());
        statusEffects.put("Aromatic Mist", new RaiseStats(2, StatsType.SpecialDefence));
        statusEffects.put("Assist", null);
        statusEffects.put("Attract", new ResetStats());
        statusEffects.put("Aurora Veil", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Autotomize", new ResetStats());
        statusEffects.put("Baby-Doll Eyes", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Baneful Bunker", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Barrier", new ResetStats());
        statusEffects.put("Baton Pass", new ResetStats());
        statusEffects.put("Belly Drum", new CompleteHeal());
        statusEffects.put("Bestow", new RaiseStats(2, StatsType.Speed));
        statusEffects.put("Block", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Bulk Up", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Calm Mind", new ResetStats());
        statusEffects.put("Camouflage", new RaiseStats(1, StatsType.Evasion));
        statusEffects.put("Captivate", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Celebrate", new RaiseStats(1, StatsType.Attack, StatsType.Defence, StatsType.SpecialAttack, StatsType.SpecialDefence, StatsType.Speed));
        statusEffects.put("Charge", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Charm", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Coil", new ResetStats());
        statusEffects.put("Confide", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Confuse Ray", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Conversion", new RaiseStats(1, StatsType.Attack, StatsType.Defence, StatsType.SpecialAttack, StatsType.SpecialDefence, StatsType.Speed));
        statusEffects.put("Conversion 2", new CompleteHeal());
        statusEffects.put("Copycat", new RaiseStats(1, StatsType.Accuracy));
        statusEffects.put("Cosmic Power", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Cotton Guard", new ResetStats());
        statusEffects.put("Cotton Spore", new ResetStats());
        statusEffects.put("Crafty Shield", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Curse", new ZCurse());
        statusEffects.put("Dark Void", new ResetStats());
        statusEffects.put("Defend Order", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Defense Curl", new RaiseStats(1, StatsType.Accuracy));
        statusEffects.put("Defog", new RaiseStats(1, StatsType.Accuracy));
        statusEffects.put("Destiny Bond", new GainAttention());
        statusEffects.put("Detect", new RaiseStats(1, StatsType.Evasion));
        statusEffects.put("Disable", new ResetStats());
        statusEffects.put("Double Team", new ResetStats());
        statusEffects.put("Dragon Dance", new ResetStats());
        statusEffects.put("Eerie Impulse", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Electric Terrain", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Electrify", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Embargo", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Encore", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Endure", new ResetStats());
        statusEffects.put("Entrainment", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Fairy Lock", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Fake Tears", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Feather Dance", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Flash", new RaiseStats(1, StatsType.Evasion));
        statusEffects.put("Flatter", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Floral Healing", new ResetStats());
        statusEffects.put("Flower Shield", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Focus Energy", new RaiseStats(1, StatsType.Accuracy));
        statusEffects.put("Follow Me", new ResetStats());
        statusEffects.put("Foresight", new CriticalHitRaise());
        statusEffects.put("Forest's Curse", new RaiseStats(1, StatsType.Attack, StatsType.Defence, StatsType.SpecialAttack, StatsType.SpecialDefence, StatsType.Speed));
        statusEffects.put("Gastro Acid", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Gear Up", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Geomancy", new RaiseStats(1, StatsType.Attack, StatsType.Defence, StatsType.SpecialAttack, StatsType.SpecialDefence, StatsType.Speed));
        statusEffects.put("Glare", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Grass Whistle", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Grassy Terrain", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Gravity", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Growl", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Growth", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Grudge", new GainAttention());
        statusEffects.put("Guard Split", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Guard Swap", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Hail", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Happy Hour", new RaiseStats(1, StatsType.Attack, StatsType.Defence, StatsType.SpecialAttack, StatsType.SpecialDefence, StatsType.Speed));
        statusEffects.put("Harden", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Haze", new CompleteHeal());
        statusEffects.put("Heal Bell", new CompleteHeal());
        statusEffects.put("Heal Block", new RaiseStats(2, StatsType.SpecialAttack));
        statusEffects.put("Heal Order", new ResetStats());
        statusEffects.put("Heal Pulse", new ResetStats());
        statusEffects.put("Healing Wish", null);
        statusEffects.put("Heart Swap", new CriticalHitRaise());
        statusEffects.put("Helping Hand", new ResetStats());
        statusEffects.put("Hold Hands", new RaiseStats(1, StatsType.Attack, StatsType.Defence, StatsType.SpecialAttack, StatsType.SpecialDefence, StatsType.Speed));
        statusEffects.put("Hone Claws", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Howl", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Hypnosis", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Imprison", new RaiseStats(2, StatsType.SpecialDefence));
        statusEffects.put("Ingrain", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Instruct", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Ion Deluge", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Iron Defense", new ResetStats());
        statusEffects.put("Kinesis", new RaiseStats(1, StatsType.Evasion));
        statusEffects.put("Laser Focus", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Leech Seed", new ResetStats());
        statusEffects.put("Leer", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Light Screen", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Lock-On", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Lovely Kiss", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Lucky Chant", new RaiseStats(1, StatsType.Evasion));
        statusEffects.put("Lunar Dance", null);
        statusEffects.put("Magic Coat", new RaiseStats(2, StatsType.SpecialDefence));
        statusEffects.put("Magic Room", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Magnet Rise", new RaiseStats(1, StatsType.Evasion));
        statusEffects.put("Magnetic Flux", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Mat Block", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Mean Look", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Meditate", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Me First", new RaiseStats(2, StatsType.Speed));
        statusEffects.put("Memento", null);
        statusEffects.put("Metal Sound", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Metronome", null);
        statusEffects.put("Milk Drink", new ResetStats());
        statusEffects.put("Mimic", new RaiseStats(1, StatsType.Accuracy));
        statusEffects.put("Mind Reader", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Minimize", new ResetStats());
        statusEffects.put("Miracle Eye", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Mirror Move", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Mist", new CompleteHeal());
        statusEffects.put("Misty Terrain", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Moonlight", new ResetStats());
        statusEffects.put("Morning Sun", new ResetStats());
        statusEffects.put("Mud Sport", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Nasty Plot", new ResetStats());
        statusEffects.put("Nature Power", null);
        statusEffects.put("Nightmare", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Noble Roar", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Odor Sleuth", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Pain Split", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Parting Shot", null);
        statusEffects.put("Perish Song", new ResetStats());
        statusEffects.put("Play Nice", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Poison Gas", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Poison Powder", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Powder", new RaiseStats(2, StatsType.SpecialDefence));
        statusEffects.put("Power Split", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Power Swap", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Power Trick", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Protect", new ResetStats());
        statusEffects.put("Psych Up", new CompleteHeal());
        statusEffects.put("Psychic Terrain", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Psycho Shift", new RaiseStats(2, StatsType.SpecialAttack));
        statusEffects.put("Purify", new RaiseStats(1, StatsType.Attack, StatsType.Defence, StatsType.SpecialAttack, StatsType.SpecialDefence, StatsType.Speed));
        statusEffects.put("Quash", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Quick Guard", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Quiver Dance", new ResetStats());
        statusEffects.put("Rage Powder", new ResetStats());
        statusEffects.put("Rain Dance", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Recover", new ResetStats());
        statusEffects.put("Recycle", new RaiseStats(2, StatsType.Speed));
        statusEffects.put("Reflect", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Reflect Type", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Refresh", new CompleteHeal());
        statusEffects.put("Rest", new ResetStats());
        statusEffects.put("Roar", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Rock Polish", new ResetStats());
        statusEffects.put("Role Play", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Roost", new ResetStats());
        statusEffects.put("Rototiller", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Safeguard", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Sand Attack", new RaiseStats(1, StatsType.Evasion));
        statusEffects.put("Sandstorm", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Scary Face", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Screech", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Sharpen", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Shell Smash", new ResetStats());
        statusEffects.put("Shift Gear", new ResetStats());
        statusEffects.put("Shore Up", new ResetStats());
        statusEffects.put("Simple Beam", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Sing", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Sketch", new RaiseStats(1, StatsType.Attack, StatsType.Defence, StatsType.SpecialAttack, StatsType.SpecialDefence, StatsType.Speed));
        statusEffects.put("Skill Swap", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Slack Off", new ResetStats());
        statusEffects.put("Sleep Powder", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Sleep Talk", new CriticalHitRaise());
        statusEffects.put("Smokescreen", new RaiseStats(1, StatsType.Evasion));
        statusEffects.put("Snatch", new RaiseStats(2, StatsType.Speed));
        statusEffects.put("Soak", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Soft-Boiled", new ResetStats());
        statusEffects.put("Speed Swap", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Spider Web", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Spikes", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Spiky Shield", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Spite", new CompleteHeal());
        statusEffects.put("Splash", new RaiseStats(3, StatsType.Attack));
        statusEffects.put("Spore", new ResetStats());
        statusEffects.put("Spotlight", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Stealth Rock", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Sticky Web", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Stockpile", new CompleteHeal());
        statusEffects.put("Strength Sap", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("String Shot", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Stun Spore", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Substitute", new ResetStats());
        statusEffects.put("Sunny Day", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Supersonic", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Swagger", new ResetStats());
        statusEffects.put("Swallow", new ResetStats());
        statusEffects.put("Sweet Kiss", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Sweet Scent", new RaiseStats(1, StatsType.Accuracy));
        statusEffects.put("Switcheroo", new RaiseStats(2, StatsType.Speed));
        statusEffects.put("Swords Dance", new ResetStats());
        statusEffects.put("Synthesis", new ResetStats());
        statusEffects.put("Tail Glow", new ResetStats());
        statusEffects.put("Tail Whip", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Tailwind", new CriticalHitRaise());
        statusEffects.put("Taunt", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Tearful Look", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Teeter Dance", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Telekinesis", new RaiseStats(1, StatsType.SpecialAttack));
        statusEffects.put("Teleport", new CompleteHeal());
        statusEffects.put("Thunder Wave", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Tickle", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Topsy-Turvy", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Torment", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Toxic", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Toxic Spikes", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Toxic Thread", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Transform", new CompleteHeal());
        statusEffects.put("Trick", new RaiseStats(2, StatsType.Speed));
        statusEffects.put("Trick Room", new RaiseStats(1, StatsType.Accuracy));
        statusEffects.put("Trick-or-Treat", new RaiseStats(1, StatsType.Attack, StatsType.Defence, StatsType.SpecialAttack, StatsType.SpecialDefence, StatsType.Speed));
        statusEffects.put("Venom Drench", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Water Sport", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Whirlwind", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Wide Guard", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Will-O-Wisp", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Wish", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Withdraw", new RaiseStats(1, StatsType.Defence));
        statusEffects.put("Wonder Room", new RaiseStats(1, StatsType.SpecialDefence));
        statusEffects.put("Work Up", new RaiseStats(1, StatsType.Attack));
        statusEffects.put("Worry Seed", new RaiseStats(1, StatsType.Speed));
        statusEffects.put("Yawn", new RaiseStats(1, StatsType.Speed));
    }
}

