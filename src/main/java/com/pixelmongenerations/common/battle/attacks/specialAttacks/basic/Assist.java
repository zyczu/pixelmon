/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.ZAttackBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class Assist
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.getParticipant().allPokemon.length == 1) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        ArrayList<Attack> possibleAttacks = this.getPossibleAttacks(user);
        if (!possibleAttacks.isEmpty()) {
            Attack attack = RandomHelper.getRandomElementFromList(possibleAttacks);
            if (user.attack.getAttackBase() instanceof ZAttackBase) {
                attack = ZAttackBase.getZMove(attack, false);
            }
            user.useTempAttack(attack);
            return AttackResult.ignore;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        ArrayList<Attack> possibleAttacks = this.getPossibleAttacks(pw);
        if (possibleAttacks.isEmpty()) {
            return;
        }
        ArrayList<MoveChoice> possibleChoices = MoveChoice.createMoveChoicesFromList(possibleAttacks, pw);
        pw.getBattleAI().weightRandomMove(pw, userChoice, possibleChoices);
    }

    private ArrayList<Attack> getPossibleAttacks(PixelmonWrapper user) {
        ArrayList<Attack> possibleAttacks = new ArrayList<Attack>();
        for (PixelmonWrapper other : user.getParticipant().allPokemon) {
            if (other == user) continue;
            possibleAttacks.addAll(other.getMoveset().stream().filter(attack -> {
                if (attack == null) return false;
                if (attack.isAttack("Assist", "Spotlight", "Baneful Bunker", "Beak Blast", "Belch", "Bestow", "Bounce", "Celebrate", "Chatter", "Circle Throw", "Copycat", "Counter", "Covet", "Destiny Bond", "Detect", "Dig", "Dive", "Dragon Tail", "Endure", "Feint", "Fly", "Focus Punch", "Follow Me", "Helping Hand", "Hold Hands", "King's Shield", "Mat Block", "Me First", "Metronome", "Mimic", "Mirror Coat", "Mirror Move", "Nature Power", "Phantom Force", "Protect", "Rage Powder", "Roar", "Shadow Force", "Shell Trap", "Sketch", "Sky Drop", "Sleep Talk", "Snatch", "Spiky Shield", "Struggle", "Switcheroo", "Thief", "Transform", "Trick", "Whirlwind")) return false;
                return true;
            }).collect(Collectors.toList()));
        }
        return possibleAttacks;
    }
}

