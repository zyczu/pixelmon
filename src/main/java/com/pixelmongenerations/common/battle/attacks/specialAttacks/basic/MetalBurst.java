/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.DamageReflect;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class MetalBurst
extends DamageReflect {
    public MetalBurst() {
        super(1.5f);
    }

    @Override
    public boolean isCorrectCategory(AttackCategory category) {
        return category != AttackCategory.Status;
    }
}

