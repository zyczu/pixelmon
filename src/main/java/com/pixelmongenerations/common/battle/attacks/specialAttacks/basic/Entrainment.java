/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AsOne;
import com.pixelmongenerations.common.entity.pixelmon.abilities.BattleBond;
import com.pixelmongenerations.common.entity.pixelmon.abilities.FlowerGift;
import com.pixelmongenerations.common.entity.pixelmon.abilities.GulpMissile;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Imposter;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Multitype;
import com.pixelmongenerations.common.entity.pixelmon.abilities.NeutralizingGas;
import com.pixelmongenerations.common.entity.pixelmon.abilities.StanceChange;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Trace;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Truant;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ZenMode;
import java.util.ArrayList;

public class Entrainment
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        AbilityBase targetAbility = target.getBattleAbility(false);
        if (!(targetAbility instanceof AsOne || targetAbility instanceof FlowerGift || targetAbility instanceof Imposter || targetAbility instanceof Multitype || targetAbility instanceof StanceChange || targetAbility instanceof Trace || targetAbility instanceof Truant || targetAbility instanceof BattleBond || targetAbility instanceof ZenMode || targetAbility instanceof GulpMissile || targetAbility instanceof NeutralizingGas || target.isDynamaxed())) {
            user.bc.sendToAll("pixelmon.effect.entrainment", target.getNickname(), target.getBattleAbility(false).getLocalizedName(), user.getBattleAbility(false).getLocalizedName());
            target.setTempAbility(user.getBattleAbility(false));
            return AttackResult.succeeded;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        AbilityBase userAbility = pw.getBattleAbility(false);
        boolean userNegativeAbility = userAbility.isNegativeAbility();
        boolean allied = userChoice.hitsAlly();
        for (PixelmonWrapper target : userChoice.targets) {
            AbilityBase targetAbility = target.getBattleAbility(false);
            boolean targetNegativeAbility = targetAbility.isNegativeAbility();
            if (allied && targetNegativeAbility && !userNegativeAbility) {
                userChoice.raiseWeight(40.0f);
                continue;
            }
            if (allied || targetNegativeAbility) continue;
            if (userNegativeAbility) {
                userChoice.raiseWeight(40.0f);
                continue;
            }
            userChoice.raiseWeight(25.0f);
        }
    }
}

