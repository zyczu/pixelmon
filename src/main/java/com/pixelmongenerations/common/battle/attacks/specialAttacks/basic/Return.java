/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class Return
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        int friendship = user.getFriendshipValue();
        user.attack.getAttackBase().basePower = Math.max(1, (int)((double)friendship / 2.5));
        return AttackResult.proceed;
    }
}

