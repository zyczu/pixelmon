/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.core.config.PixelmonConfig;

public class PayDay
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (PixelmonConfig.allowPayDayMoney) {
            if (!user.bc.simulateMode && user.getParticipant() instanceof PlayerParticipant) {
                PlayerParticipant player = (PlayerParticipant)user.getParticipant();
                player.payDay += user.getLevelNum() * PixelmonConfig.payDayMultiplier;
            }
            user.bc.sendToAll("pixelmon.effect.payday", new Object[0]);
        }
        return AttackResult.proceed;
    }
}

