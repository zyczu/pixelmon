/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class FusionBolt
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.turn > 0) {
            PixelmonWrapper turnBefore = user.bc.turnList.get(user.bc.turn - 1);
            if (turnBefore.attack != null) {
                if (turnBefore.attack.isAttack("Blue Flare", "Fusion Flare")) {
                    user.attack.getAttackBase().basePower *= 2;
                }
            }
        }
        return AttackResult.proceed;
    }
}

