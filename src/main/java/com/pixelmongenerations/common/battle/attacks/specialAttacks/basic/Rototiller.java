/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;

public class Rototiller
extends SpecialAttackBase {
    private static final StatsType[] raiseStats = new StatsType[]{StatsType.Attack, StatsType.SpecialAttack};

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.targetIndex > 0) {
            return AttackResult.succeeded;
        }
        boolean succeeded = false;
        for (PixelmonWrapper pw : user.bc.getActiveUnfaintedPokemon()) {
            if (pw.isAirborne()) continue;
            if (!pw.hasType(EnumType.Grass)) continue;
            pw.getBattleStats().modifyStat(1, raiseStats);
            succeeded = true;
        }
        if (succeeded) {
            return AttackResult.succeeded;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (PixelmonWrapper pokemon : pw.bc.getActiveUnfaintedPokemon()) {
            if (pw.isAirborne()) continue;
            int weight = 25;
            if (!pw.isSameTeam(pokemon)) {
                weight = -weight;
            }
            userChoice.raiseWeight(weight);
        }
    }
}

