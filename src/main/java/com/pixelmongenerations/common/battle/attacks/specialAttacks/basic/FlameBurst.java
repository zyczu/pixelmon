/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;

public class FlameBurst
extends SpecialAttackBase {
    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.rules.battleType.numPokemon > 1) {
            BattleParticipant targetParticipant = target.getParticipant();
            user.bc.getTeamPokemon(targetParticipant).stream().filter(pw -> (Math.abs(pw.battlePosition - target.battlePosition) == 1 || pw.getParticipant() != targetParticipant) && !(pw.getBattleAbility() instanceof MagicGuard)).forEach(pw -> {
                pw.doBattleDamage(user, pw.getMaxHealth() / 16, DamageTypeEnum.STATUS);
                user.bc.sendToAll("pixelmon.effect.hurt", pw.getNickname());
            });
        }
    }
}

