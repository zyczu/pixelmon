/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.EchoedVoiceStatus;
import com.pixelmongenerations.common.battle.status.StatusType;

public class EchoedVoice
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        EchoedVoiceStatus echoedVoice = (EchoedVoiceStatus)user.bc.globalStatusController.getGlobalStatus(StatusType.EchoedVoice);
        if (echoedVoice == null) {
            user.bc.globalStatusController.addGlobalStatus(new EchoedVoiceStatus(user.bc.battleTurn));
        } else {
            user.attack.getAttackBase().basePower = echoedVoice.power;
            if (!user.bc.simulateMode) {
                if (echoedVoice.power < 200 && echoedVoice.turnInc != user.bc.battleTurn) {
                    echoedVoice.power += 40;
                }
                echoedVoice.turnInc = user.bc.battleTurn;
            }
        }
        return AttackResult.proceed;
    }
}

