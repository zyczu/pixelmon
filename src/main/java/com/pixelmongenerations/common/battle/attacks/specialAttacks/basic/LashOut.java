/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class LashOut
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.getBattleStats().getSumDecreases() != 0) {
            user.attack.getAttackBase().basePower *= 2;
            return AttackResult.proceed;
        }
        user.attack.getAttackBase().basePower = 75;
        return AttackResult.proceed;
    }
}

