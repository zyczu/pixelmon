/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnSpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class Rollout
extends MultiTurnSpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        int turnCount;
        if (!this.doesPersist(user)) {
            this.setPersists(user, true);
            this.setTurnCount(user, 5);
        }
        if ((turnCount = this.getTurnCount(user)) == 0) {
            turnCount = 5;
        }
        user.attack.getAttackBase().basePower = 30 << 5 - turnCount;
        if (user.hasStatus(StatusType.DefenseCurl)) {
            user.attack.getAttackBase().basePower *= 2;
        }
        this.decrementTurnCount(user);
        if (this.getTurnCount(user) <= 0) {
            this.setPersists(user, false);
        }
        return AttackResult.proceed;
    }

    @Override
    public void removeEffect(PixelmonWrapper user, PixelmonWrapper target) {
        this.setPersists(user, false);
    }

    @Override
    public boolean shouldNotLosePP(PixelmonWrapper user) {
        return this.doesPersist(user);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.tier >= 3) {
            userChoice.weight /= 2.0f;
        }
    }
}

