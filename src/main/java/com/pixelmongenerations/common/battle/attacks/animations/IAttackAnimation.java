/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.animations;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;

public interface IAttackAnimation {
    public void doMove(EntityLiving var1, Entity var2);
}

