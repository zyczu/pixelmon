/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class WringOut
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        float power = 1 + 120 * (target.getHealth() / target.getMaxHealth());
        user.attack.getAttackBase().basePower = (int)power;
        return AttackResult.proceed;
    }
}

