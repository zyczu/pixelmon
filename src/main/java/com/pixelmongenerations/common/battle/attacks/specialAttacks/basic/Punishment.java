/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class Punishment
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        int DamageMultiplier = target.getBattleStats().getSumIncreases();
        user.attack.getAttackBase().basePower = 60 + 20 * DamageMultiplier;
        if (user.attack.getAttackBase().basePower >= 200) {
            user.attack.getAttackBase().basePower = 200;
        }
        return AttackResult.proceed;
    }
}

