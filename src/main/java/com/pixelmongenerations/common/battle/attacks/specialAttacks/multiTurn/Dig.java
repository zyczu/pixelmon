/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnCharge;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.UnderGround;

public class Dig
extends MultiTurnCharge {
    public Dig() {
        super("pixelmon.effect.dighole", UnderGround.class, StatusType.UnderGround);
    }
}

