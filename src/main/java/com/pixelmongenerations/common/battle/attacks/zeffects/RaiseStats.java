/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.zeffects;

import com.pixelmongenerations.common.battle.attacks.zeffects.ZEffect;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.util.Arrays;

public class RaiseStats
implements ZEffect {
    private final StatsType[] stats;
    private final int[] value;

    public RaiseStats(int value, StatsType ... stats) {
        this.stats = stats;
        this.value = new int[stats.length];
        Arrays.fill(this.value, value);
    }

    @Override
    public void applyEffect(PixelmonWrapper pokemon) {
        pokemon.getBattleStats().modifyStat(this.value, this.stats, pokemon, false, false, true);
    }

    @Override
    public void getEffectText(PixelmonWrapper pixelmonWrapper) {
        if (pixelmonWrapper.attack.isAttack("Extreme Evoboost")) {
            return;
        }
        for (StatsType type : this.stats) {
            pixelmonWrapper.bc.sendToAll("pixelmon.zeffect.stat_raise", new Object[]{pixelmonWrapper.getNickname(), type});
        }
    }
}

