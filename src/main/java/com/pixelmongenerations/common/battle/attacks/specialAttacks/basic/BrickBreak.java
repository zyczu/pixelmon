/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class BrickBreak
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.removeTeamStatus(StatusType.LightScreen)) {
            user.bc.sendToAll("pixelmon.status.lightscreenoff", target.getNickname());
        }
        if (target.removeTeamStatus(StatusType.Reflect)) {
            user.bc.sendToAll("pixelmon.status.reflectoff", target.getNickname());
        }
        return AttackResult.proceed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (PixelmonWrapper target : userChoice.targets) {
            if (!target.hasStatus(StatusType.LightScreen, StatusType.Reflect)) continue;
            userChoice.raiseWeight(50.0f);
        }
    }
}

