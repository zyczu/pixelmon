/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import java.util.ArrayList;

public class Bestow
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        ItemHeld userItem = user.getHeldItem();
        if (user.hasHeldItem() && !target.hasHeldItem() && user.isItemRemovable(user)) {
            user.bc.sendToAll("pixelmon.effect.bestow", user.getNickname(), userItem.getLocalizedName(), target.getNickname());
            target.setNewHeldItem(userItem);
            user.setNewHeldItem(null);
            user.bc.enableReturnHeldItems(user, target);
            return AttackResult.succeeded;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        ItemHeld itemHeld = pw.getHeldItem();
        boolean negative = itemHeld != null && itemHeld.hasNegativeEffect();
        boolean isAlly = userChoice.hitsAlly();
        if (isAlly ^ negative) {
            userChoice.raiseWeight(25.0f);
        }
    }
}

