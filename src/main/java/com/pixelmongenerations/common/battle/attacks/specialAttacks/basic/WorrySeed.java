/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.ChangeAbility;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GastroAcid;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AsOne;
import com.pixelmongenerations.common.entity.pixelmon.abilities.BattleBond;
import com.pixelmongenerations.common.entity.pixelmon.abilities.GulpMissile;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Insomnia;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicBounce;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Multitype;
import com.pixelmongenerations.common.entity.pixelmon.abilities.StanceChange;

public class WorrySeed
extends ChangeAbility {
    public WorrySeed() {
        super(Insomnia.class);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        AbilityBase targetAbility = target.getBattleAbility(false);
        if (targetAbility instanceof AsOne || targetAbility instanceof MagicBounce || targetAbility instanceof Multitype || targetAbility instanceof BattleBond || targetAbility instanceof StanceChange || targetAbility instanceof GulpMissile) {
            target.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        } else if (target.addStatus(new GastroAcid(), target)) {
            target.bc.sendToAll("pixelmon.status.gastroacid", target.getNickname());
            targetAbility.onAbilityLost(target);
        }
    }
}

