/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.ZAttackBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;

public class Metronome
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        Attack randomAttack;
        user.bc.sendToAll("pixelmon.effect.wagfinger", user.getNickname());
        do {
            randomAttack = new Attack(RandomHelper.getRandomNumberBetween(1, 767));
        } while (randomAttack.isAttack("After You", "Assist", "Bestow", "Chatter", "Copycat", "Counter", "Covet", "Destiny Bond", "Detect", "Endure", "Feint", "Focus Punch", "Follow Me", "Freeze Shock", "Helping Hand", "Ice Burn", "Me First", "Metronome", "Mimic", "Mirror Coat", "Mirror Move", "Nature Power", "Protect", "Quash", "Quick Guard", "Rage Powder", "Relic Song", "Secret Sword", "Sketch", "Sleep Talk", "Snarl", "Snatch", "Snore", "Struggle", "Switcheroo", "Techno Blast", "Thief", "Transform", "Trick", "V-create", "Wide Guard", "Max Airstream", "Max Darkness", "Max Flare", "Max Flutterby", "Max Geyser", "Max Guard", "Max Hailstorm", "Max Knuckle", "Max Lightning", "Max Mindstorm", "Max Ooze", "Max Overgroth", "Max Phantasm", "Max Quake", "Max Rockfall", "Max Starfall", "Max Steelspike", "Max Wyrmwind"));
        if (user.attack.getAttackBase() instanceof ZAttackBase) {
            randomAttack = ZAttackBase.getZMove(randomAttack, true);
        }
        user.useTempAttack(randomAttack);
        return AttackResult.ignore;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        userChoice.raiseWeight(20.0f);
    }
}

