/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.TerrainExamine;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Burn;
import com.pixelmongenerations.common.battle.status.Flinch;
import com.pixelmongenerations.common.battle.status.Freeze;
import com.pixelmongenerations.common.battle.status.Paralysis;
import com.pixelmongenerations.common.battle.status.Sleep;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class SecretPower
extends SpecialAttackBase {
    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (!target.isFainted() && RandomHelper.getRandomChance(30)) {
            TerrainExamine.TerrainType terrainType = TerrainExamine.getTerrain(user);
            switch (terrainType) {
                case Sand: {
                    target.getBattleStats().modifyStat(-1, StatsType.Accuracy);
                    break;
                }
                case Cave: 
                case Burial: 
                case Space: {
                    Flinch.flinch(user, target);
                }
                case Water: {
                    target.getBattleStats().modifyStat(-1, StatsType.Attack);
                    break;
                }
                case Sky: 
                case Swamp: {
                    target.getBattleStats().modifyStat(-1, StatsType.Speed);
                    break;
                }
                case Snow: 
                case Ice: {
                    Freeze.freeze(user, target);
                    break;
                }
                case Volcano: {
                    Burn.burn(user, target, user.attack, false);
                    break;
                }
                case Grass: {
                    Sleep.sleep(user, target, user.attack, false);
                    break;
                }
                case Misty: {
                    target.getBattleStats().modifyStat(-1, StatsType.SpecialAttack);
                    break;
                }
                default: {
                    Paralysis.paralyze(user, target, user.attack, false);
                }
            }
        }
    }
}

