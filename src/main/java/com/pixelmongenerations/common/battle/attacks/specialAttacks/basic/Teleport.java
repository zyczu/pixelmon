/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.TrainerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.ai.AITeleportAway;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.battle.EnumBattleEndCause;
import com.pixelmongenerations.core.network.packetHandlers.battles.EnforcedSwitch;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class Teleport
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        BattleParticipant userParticipant = user.getParticipant();
        if (user.hasStatus(StatusType.MeanLook)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        if (!userParticipant.hasMorePokemonReserve() && !(userParticipant instanceof WildPixelmonParticipant)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        if (user.bc.simulateMode) {
            return AttackResult.succeeded;
        }
        user.bc.sendToAll("teleport.away", user.getNickname());
        user.nextSwitchIsMove = true;
        if (userParticipant instanceof TrainerParticipant) {
            user.bc.switchPokemon(user.getPokemonID(), user.getBattleAI().getNextSwitch(user), true);
        } else if (userParticipant instanceof PlayerParticipant) {
            if (!user.bc.simulateMode) {
                user.wait = true;
                Pixelmon.NETWORK.sendTo(new EnforcedSwitch(user.bc.getPositionOfPokemon(user, user.getParticipant())), user.getPlayerOwner());
            }
        } else if (userParticipant instanceof WildPixelmonParticipant) {
            user.bc.endBattle(EnumBattleEndCause.FLEE);
            while (!AITeleportAway.teleportRandomly(user.pokemon, RandomHelper.rand)) {
            }
        } else {
            user.nextSwitchIsMove = false;
        }
        return AttackResult.succeeded;
    }
}

