/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnSpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.UproarStatus;
import java.util.ArrayList;

public class Uproar
extends MultiTurnSpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (!this.doesPersist(user)) {
            this.setPersists(user, true);
            this.setTurnCount(user, 2);
            user.addStatus(new UproarStatus(), user);
            user.bc.sendToAll("pixelmon.effect.uproar", user.getNickname());
        }
        if (this.getTurnCount(user) == 0) {
            this.setPersists(user, false);
            user.removeStatus(StatusType.Uproar);
            user.bc.sendToAll("pixelmon.effect.uproarend", user.getNickname());
        }
        this.decrementTurnCount(user);
        return AttackResult.proceed;
    }

    @Override
    public void removeEffect(PixelmonWrapper user, PixelmonWrapper target) {
        this.setPersists(user, false);
        user.removeStatus(StatusType.Uproar);
    }

    @Override
    public boolean shouldNotLosePP(PixelmonWrapper user) {
        return this.doesPersist(user);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.tier >= 3) {
            userChoice.weight /= 2.0f;
        }
    }
}

