/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class LowKick
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        float weight = target.getWeight(AbilityBase.ignoreAbility(user, target));
        if (target.isDynamaxed()) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        user.attack.getAttackBase().basePower = (double)weight >= 200.0 ? 120 : ((double)weight >= 100.0 ? 100 : ((double)weight >= 50.0 ? 80 : ((double)weight >= 25.0 ? 60 : ((double)weight >= 10.0 ? 40 : 20))));
        return AttackResult.proceed;
    }
}

