/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TargetingInfo {
    public boolean hitsAll;
    public boolean hitsOppositeFoe;
    public boolean hitsAdjacentFoe;
    public boolean hitsExtendedFoe;
    public boolean hitsSelf;
    public boolean hitsAdjacentAlly;
    public boolean hitsExtendedAlly;

    public TargetingInfo(ResultSet rs) {
        try {
            this.hitsAll = rs.getBoolean("HITSALL");
            this.hitsOppositeFoe = rs.getBoolean("HITSOPPOSITEFOE");
            this.hitsAdjacentFoe = rs.getBoolean("HITSADJACENTFOE");
            this.hitsExtendedFoe = rs.getBoolean("HITSEXTENDEDFOE");
            this.hitsSelf = rs.getBoolean("HITSSELF");
            this.hitsAdjacentAlly = rs.getBoolean("HITSADJACENTALLY");
            this.hitsExtendedAlly = rs.getBoolean("HITSEXTENDEDALLY");
        }
        catch (SQLException sqlException) {
            System.err.println("Missing Data in Database for TargetingInfo.");
            sqlException.printStackTrace();
        }
    }

    public TargetingInfo() {
    }
}

