/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;

public class Acrobatics
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (!user.hasHeldItem() || user.getUsableHeldItem() == PixelmonItemsHeld.flyingGem) {
            user.attack.getAttackBase().basePower *= 2;
        }
        return AttackResult.proceed;
    }
}

