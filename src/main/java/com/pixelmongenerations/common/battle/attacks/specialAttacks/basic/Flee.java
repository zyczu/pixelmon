/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.log.MoveResults;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.battle.status.EntryHazard;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SuctionCups;
import com.pixelmongenerations.core.enums.battle.EnumBattleEndCause;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class Flee
extends SpecialAttackBase {
    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.inParentalBond) {
            return;
        }

        MoveResults results;
        boolean doesDamage;
        if (user.attack == null) {
            results = new MoveResults(target);
            doesDamage = false;
        } else {
            results = user.attack.moveResult;
            doesDamage = user.attack.getAttackBase().basePower > 0;
        }

        if (doesDamage) {
            if (target.getHealth() == 0 || target.hasStatus(StatusType.Ingrain, StatusType.Substitute)) {
                return;
            }

            if (target.getBattleAbility() instanceof SuctionCups) {
                target.bc.sendToAll("pixelmon.ability.suctioncups", target.getNickname());
                return;
            }
        }

        if (target.hasStatus(StatusType.Ingrain)) {
            target.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            results.result = AttackResult.failed;
            return;
        }
        if (target.isDynamaxed()) {
            target.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            results.result = AttackResult.failed;
            return;
        }
        if (target.getBattleAbility() instanceof SuctionCups) {
            target.bc.sendToAll("pixelmon.abilities.suctioncups", target.getNickname());
            results.result = AttackResult.failed;
            return;
        }
        BattleParticipant targetParticipant = target.getParticipant();
        if (targetParticipant.getType() == ParticipantType.WildPokemon || user.getParticipant().getType() == ParticipantType.WildPokemon) {
            if (target.getLevelNum() > user.getLevelNum()) {
                if (!doesDamage) {
                    target.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                    results.result = AttackResult.failed;
                }
            } else if (!user.bc.simulateMode) {
                targetParticipant.isDefeated = true;
                if (!target.bc.isTeamDefeated(targetParticipant)) {
                    target.pokemon.setDead();
                    target.pokemon.isFainted = true;
                    target.bc.updateRemovedPokemon(target);
                    target.bc.sendDamagePacket(target, target.getHealth());
                    target.pokemon.setHealth(0.0f);
                } else {
                    target.bc.sendToAll("pixelmon.effect.flee", target.getNickname());
                    target.bc.endBattle(EnumBattleEndCause.FLEE);
                }
            }
            return;
        }
        PixelmonWrapper randomPokemon = targetParticipant.getRandomPartyPokemon();
        if (randomPokemon != null) {
            if (user.bc.simulateMode) {
                return;
            }
            target.bc.sendToAll("pixelmon.effect.flee", target.getNickname());
            target.forceRandomSwitch(randomPokemon.getPokemonID());
        } else if (!doesDamage) {
            target.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            results.result = AttackResult.failed;
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        BattleParticipant part = pw.getParticipant();
        if (userChoice.hitsAlly() || part instanceof WildPixelmonParticipant) {
            return;
        }
        for (PixelmonWrapper target : userChoice.targets) {
            if (pw.attack.getAttackBase().basePower > 0 && (userChoice.tier >= 3 || target.hasStatus(StatusType.Substitute, StatusType.Ingrain) || target.getBattleAbility() instanceof SuctionCups || !part.hasMorePokemonReserve())) {
                return;
            }
            userChoice.raiseWeight(target.getBattleStats().getSumStages() * 20);
            for (EntryHazard hazard2 : target.getEntryHazards().stream().filter(hazard -> hazard.type != StatusType.StickyWeb).collect(Collectors.toList())) {
                userChoice.raiseWeight(hazard2.getAIWeight() * hazard2.getNumLayers());
            }
        }
    }
}

