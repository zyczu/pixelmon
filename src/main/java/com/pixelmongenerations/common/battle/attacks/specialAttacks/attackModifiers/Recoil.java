/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers;

import com.pixelmongenerations.api.events.pokemon.RecoilDamageEvent;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.Value;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers.AttackModifierBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.entity.pixelmon.abilities.RockHead;
import java.util.ArrayList;
import net.minecraftforge.common.MinecraftForge;

public class Recoil
extends AttackModifierBase {
    public int recoil;

    public Recoil(Value ... values) {
        this.recoil = values[0].value;
    }

    public void applyRecoil(PixelmonWrapper user, float damage) {
        if (!this.isImmune(user) && user.isAlive()) {
            user.bc.sendToAll("recoil.damage", user.getNickname());
            float recoilDamage = this.getRecoil(damage);
            RecoilDamageEvent event = new RecoilDamageEvent(user, recoilDamage);
            MinecraftForge.EVENT_BUS.post(event);
            if (!event.isCanceled()) {
                user.doBattleDamage(user, recoilDamage, DamageTypeEnum.RECOIL);
            }
        }
    }

    private boolean isImmune(PixelmonWrapper user) {
        AbilityBase userAbility = user.getBattleAbility();
        return userAbility instanceof RockHead || userAbility instanceof MagicGuard;
    }

    private float getRecoil(float damage) {
        float recoilDamage = damage * (float)this.recoil / 100.0f;
        if (recoilDamage < 1.0f) {
            recoilDamage = 1.0f;
        }
        return recoilDamage;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.tier >= 3 && !this.isImmune(pw)) {
            userChoice.weight -= pw.getHealthPercent(this.getRecoil(userChoice.result.damage));
        }
    }
}

