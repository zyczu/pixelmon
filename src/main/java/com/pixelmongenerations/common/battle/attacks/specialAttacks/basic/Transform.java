/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Transformed;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Illusion;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.clientStorage.Add;
import java.util.ArrayList;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.datasync.EntityDataManager;

public class Transform
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        AbilityBase targetAbility = target.getBattleAbility();
        if (targetAbility instanceof Illusion && ((Illusion)targetAbility).disguisedPokemon != null) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        if (!user.bc.simulateMode) {
            EntityDataManager dataManager = user.pokemon.getDataManager();
            if (user.removeStatus(StatusType.Transformed)) {
                dataManager.set(EntityPixelmon.dwTransformation, 0);
            }
            user.bc.sendToAll("pixelmon.status.transform", user.getNickname(), target.getNickname());
            dataManager.set(EntityPixelmon.dwTransformation, -1);
            user.addStatus(new Transformed(user, target), target);
            EntityPlayerMP player = user.getPlayerOwner();
            if (player != null) {
                PixelmonData data = new PixelmonData(new WrapperLink(user));
                data.inBattle = true;
                Pixelmon.NETWORK.sendTo(new Add(data, true), player);
            }
        }
        return AttackResult.succeeded;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        userChoice.raiseWeight(40.0f);
    }
}

