/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.BattleAIBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.BattleStats;
import java.util.ArrayList;

public class TopsyTurvy
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.getBattleStats().isStatModified()) {
            user.bc.sendToAll("pixelmon.effect.topsyturvy", target.getRealNickname());
            target.getBattleStats().reverseStats();
        } else {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        }
        return AttackResult.succeeded;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        for (PixelmonWrapper target : userChoice.targets) {
            if (!target.getBattleStats().isStatModified()) continue;
            pw.bc.simulateMode = false;
            BattleStats saveStats = new BattleStats(target.getBattleStats());
            ArrayList<PixelmonWrapper> opponents = pw.getOpponentPokemon();
            BattleAIBase ai = pw.getBattleAI();
            try {
                target.getBattleStats().reverseStats();
                pw.bc.simulateMode = true;
                ArrayList<MoveChoice> bestUserChoicesAfter = ai.getBestAttackChoices(pw);
                ArrayList<ArrayList<MoveChoice>> bestOpponentChoicesAfter = ai.getBestAttackChoices(opponents);
                ai.weightFromUserOptions(pw, userChoice, bestUserChoices, bestUserChoicesAfter);
                ai.weightFromOpponentOptions(pw, userChoice, MoveChoice.splitChoices(opponents, bestOpponentChoices), bestOpponentChoicesAfter);
            }
            finally {
                pw.bc.simulateMode = false;
                target.getBattleStats().copyStats(saveStats);
                pw.bc.simulateMode = true;
            }
        }
    }
}

