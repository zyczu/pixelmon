/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class Endeavor
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        float userHealth = user.getHealth();
        float targetHealth = target.getHealth();
        if (userHealth >= targetHealth) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        int damage = (int)(targetHealth - userHealth);
        target.doBattleDamage(user, damage, DamageTypeEnum.ATTACKFIXED);
        return AttackResult.hit;
    }
}

