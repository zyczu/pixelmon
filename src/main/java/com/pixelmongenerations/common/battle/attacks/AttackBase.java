/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks;

import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.attacks.TargetingInfo;
import com.pixelmongenerations.common.battle.attacks.animations.AttackAnimationLeapForward;
import com.pixelmongenerations.common.battle.attacks.animations.AttackAnimationParser;
import com.pixelmongenerations.common.battle.attacks.animations.IAttackAnimation;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers.Priority;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Pledge;
import com.pixelmongenerations.common.battle.status.MeanLook;
import com.pixelmongenerations.common.battle.status.PartialTrap;
import com.pixelmongenerations.core.enums.EnumTutorType;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.util.text.translation.I18n;

public class AttackBase {
    public int attackIndex;
    private String attackName;
    public EnumType attackType;
    public AttackCategory attackCategory;
    public int basePower;
    public int ppBase;
    public int ppMax;
    public int accuracy;
    private boolean makesContact;
    public ArrayList<EffectBase> effects;
    public List<IAttackAnimation> animations;
    public TargetingInfo targetingInfo;
    public EnumTutorType tutorType;
    public int tmIndex;
    public int trIndex;

    public AttackBase(int attackIndex, String moveName, ResultSet rs) throws SQLException {
        this.attackType = EnumType.Mystery;
        this.effects = new ArrayList();
        this.animations = new ArrayList<IAttackAnimation>();
        this.attackIndex = attackIndex;
        this.attackName = moveName;
        this.ppBase = rs.getInt("PP");
        this.ppMax = rs.getInt("PPMAX");
        this.attackType = EnumType.parseTypeFromDBID(rs.getInt("TYPEID"));
        if (this.attackName.equals("Struggle")) {
            this.attackType = EnumType.Mystery;
        }
        this.basePower = rs.getInt("POWER");
        this.accuracy = rs.getInt("ACCURACY");
        if (rs.wasNull()) {
            this.accuracy = -1;
        }
        this.makesContact = rs.getBoolean("MAKESCONTACT");
        rs.getString("DESCRIPTION");
        this.effects = AttackBase.parseEffectString(rs.getString("EFFECT"));
        String animationString = rs.getString("ATTACKANIMATIONS");
        if (animationString == null) {
            this.animations = new ArrayList<IAttackAnimation>();
            this.animations.add(new AttackAnimationLeapForward());
        } else {
            this.animations = AttackAnimationParser.GetAnimation(animationString);
        }
        this.targetingInfo = new TargetingInfo(rs);
        this.tutorType = EnumTutorType.values()[rs.getInt("TUTORTYPE")];
    }

    public AttackBase(EnumType attackType, int basePower, AttackCategory attackCategory) {
        this.attackType = EnumType.Mystery;
        this.effects = new ArrayList();
        this.animations = new ArrayList<IAttackAnimation>();
        this.attackType = attackType;
        this.basePower = basePower;
        this.attackCategory = attackCategory;
        this.attackName = "";
    }

    public AttackBase() {
    }

    public String getLocalizedName() {
        return AttackBase.getLocalizedName(this.attackName);
    }

    public static String getLocalizedName(String attackName) {
        return I18n.translateToLocal("attack." + attackName.toLowerCase() + ".name");
    }

    public String getUnlocalizedName() {
        return this.attackName;
    }

    public String getLocalizedDescription() {
        return I18n.translateToLocal("attack." + this.attackName.toLowerCase() + ".description");
    }

    public AttackCategory getAttackCategory() {
        return this.attackCategory;
    }

    public boolean getMakesContact() {
        return this.makesContact;
    }

    public boolean isAttack(String name) {
        return this.attackName.toLowerCase().equals(name.toLowerCase()) || this.getLocalizedName().toLowerCase().equals(name.toLowerCase());
    }

    public void setAttackName(String name) {
        this.attackName = name;
    }

    public void setAttackCategory(AttackCategory category) {
        this.attackCategory = category;
    }

    public void setBasePower(int power) {
        this.basePower = power;
    }

    public void setAccuracy(int newAccuracy) {
        this.accuracy = newAccuracy;
    }

    public int getPriority() {
        return this.effects.stream().filter(Priority.class::isInstance).map(Priority.class::cast).findFirst().map(e -> e.priority).orElse(0);
    }

    public boolean hasSecondaryEffect() {
        return this.effects.stream().noneMatch(EffectBase::isChance);
    }

    protected void setMakesContact(boolean makesContact) {
        this.makesContact = makesContact;
    }

    public static ArrayList<EffectBase> parseEffectString(String effect) {
        ArrayList<EffectBase> effects = new ArrayList<EffectBase>();
        block10: for (String e : effect.split(";")) {
            EffectBase etmp = EffectBase.getEffect(e);
            if (etmp != null) {
                effects.add(etmp);
                continue;
            }
            switch (e) {
                case "PartialTrap": {
                    EffectBase base = new PartialTrap();
                    effects.add(base);
                    continue block10;
                }
                case "WaterPledge": {
                    EffectBase base = new Pledge();
                    effects.add(base);
                    continue block10;
                }
                case "MeanLook": {
                    EffectBase base = new MeanLook();
                    effects.add(base);
                    continue block10;
                }
            }
        }
        return effects;
    }

    public AttackBase copy() {
        AttackBase attackBase = new AttackBase();
        attackBase.attackIndex = this.attackIndex;
        attackBase.attackName = this.attackName;
        attackBase.attackType = this.attackType;
        attackBase.attackCategory = this.attackCategory;
        attackBase.basePower = this.basePower;
        attackBase.ppBase = this.ppBase;
        attackBase.ppMax = this.ppMax;
        attackBase.accuracy = this.accuracy;
        attackBase.makesContact = this.makesContact;
        attackBase.effects = this.effects;
        attackBase.animations = this.animations;
        attackBase.targetingInfo = this.targetingInfo;
        attackBase.tutorType = this.tutorType;
        attackBase.tmIndex = this.tmIndex;
        attackBase.trIndex = this.trIndex;
        return attackBase;
    }
}

