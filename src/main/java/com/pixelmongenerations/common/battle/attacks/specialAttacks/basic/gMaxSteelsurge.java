/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.EntryHazard;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.item.heldItems.ItemHeavyDutyBoots;
import com.pixelmongenerations.core.enums.EnumType;

public class gMaxSteelsurge
extends EntryHazard {
    public gMaxSteelsurge() {
        super(StatusType.Steelsurge, 1);
    }

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public boolean isUnharmed(PixelmonWrapper pw) {
        if (pw.getHeldItem() instanceof ItemHeavyDutyBoots) {
            pw.bc.sendToAll("pixelmon.effect.heavyduty_boots", pw.getNickname());
            return true;
        }
        return pw.getBattleAbility() instanceof MagicGuard;
    }

    @Override
    public int getDamage(PixelmonWrapper pw) {
        float effectiveness = EnumType.getTotalEffectiveness(pw.type, EnumType.Steel, pw.bc.rules.hasClause("inverse"));
        float modifier = effectiveness * 12.5f;
        return pw.getPercentMaxHealth(modifier);
    }

    @Override
    protected String getFirstLayerMessage() {
        return "pixelmon.effect.floatingstones";
    }

    @Override
    protected String getAffectedMessage() {
        return "pixelmon.status.hurtbysteelsurge";
    }

    @Override
    public int getAIWeight() {
        return 30;
    }

    @Override
    public EntryHazard getNewInstance() {
        return new gMaxSteelsurge();
    }
}

