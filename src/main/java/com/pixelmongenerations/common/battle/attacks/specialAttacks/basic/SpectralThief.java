/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.BattleStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class SpectralThief
extends EffectBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        user.bc.sendToAll("pixelmon.effect.spectralthief", user.getNickname(), target.getNickname());
        SpectralThief.stealBuffs(user, target, true);
        return AttackResult.proceed;
    }

    public static void stealBuffs(PixelmonWrapper user, PixelmonWrapper target, boolean isAttack) {
        BattleStats userStats = user.getBattleStats();
        BattleStats targetStats = target.getBattleStats();
        for (StatsType type : StatsType.values()) {
            if (type == StatsType.None || type == StatsType.HP) continue;
            int userStage = userStats.getStage(type);
            int targetStage = targetStats.getStage(type);
            if (userStage >= 6 || targetStage <= 0) continue;
            int tempStage = userStage + targetStage;
            if (tempStage > 6) {
                tempStage = targetStage - tempStage % 6;
            }
            userStats.modifyStat(tempStage, type, user, isAttack);
            targetStats.modifyStat(-tempStage, type, user, isAttack);
        }
    }

    @Override
    protected void applyEffect(PixelmonWrapper pixelmonWrapper, PixelmonWrapper pixelmonWrapper1) {
    }

    @Override
    public boolean cantMiss(PixelmonWrapper pixelmonWrapper) {
        return false;
    }
}

