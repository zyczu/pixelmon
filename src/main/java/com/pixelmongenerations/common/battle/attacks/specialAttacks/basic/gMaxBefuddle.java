/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Paralysis;
import com.pixelmongenerations.common.battle.status.Poison;
import com.pixelmongenerations.common.battle.status.Sleep;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class gMaxBefuddle
extends SpecialAttackBase {
    transient int befuddle;

    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        for (PixelmonWrapper targets : target.bc.getTeamPokemon(target.getParticipant())) {
            this.befuddle = RandomHelper.getRandomNumberBetween(1, 99);
            if (user.bc.simulateMode) {
                this.befuddle = 0;
                continue;
            }
            if (this.befuddle <= 33) {
                targets.addStatus(new Poison(), targets);
                continue;
            }
            if (this.befuddle <= 66) {
                targets.addStatus(new Paralysis(), targets);
                continue;
            }
            if (this.befuddle > 99) continue;
            targets.addStatus(new Sleep(), targets);
        }
        return AttackResult.proceed;
    }
}

