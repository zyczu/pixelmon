/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.animations;

import com.pixelmongenerations.common.battle.attacks.animations.IAttackAnimation;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.projectiles.EntityArcanineRock;
import com.pixelmongenerations.common.entity.projectiles.EntityAvaluggIce;
import com.pixelmongenerations.common.entity.projectiles.EntityElectrodeLightning;
import com.pixelmongenerations.common.entity.projectiles.EntityKleavorAxe;
import com.pixelmongenerations.common.entity.projectiles.EntityLilligantFlower;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;

public class AttackAnimationLaunchProjectile
implements IAttackAnimation {
    @Override
    public void doMove(EntityLiving user, Entity target) {
        if (user instanceof EntityPixelmon) {
            EntityPixelmon pokemon = (EntityPixelmon)user;
            EnumSpecies species = pokemon.getSpecies();
            switch (species) {
                case Kleavor: {
                    EntityKleavorAxe axe = new EntityKleavorAxe(user.world, user);
                    axe.moveTowards(target.getPosition());
                    axe.shoot(user, target.rotationPitch, target.rotationYaw, 0.0f, 1.5f, 1.0f);
                    user.world.spawnEntity(axe);
                    break;
                }
                case Lilligant: {
                    EntityLilligantFlower flower = new EntityLilligantFlower(user.world, user);
                    flower.moveTowards(target.getPosition());
                    flower.shoot(user, target.rotationPitch, target.rotationYaw, 0.0f, 1.5f, 1.0f);
                    user.world.spawnEntity(flower);
                    break;
                }
                case Arcanine: {
                    EntityArcanineRock rock = new EntityArcanineRock(user.world, user);
                    rock.moveTowards(target.getPosition());
                    rock.shoot(user, target.rotationPitch, target.rotationYaw, 0.0f, 1.5f, 1.0f);
                    user.world.spawnEntity(rock);
                    break;
                }
                case Electrode: {
                    EntityElectrodeLightning lightning = new EntityElectrodeLightning(user.world, user);
                    lightning.moveTowards(target.getPosition());
                    lightning.shoot(user, target.rotationPitch, target.rotationYaw, 0.0f, 1.5f, 1.0f);
                    user.world.spawnEntity(lightning);
                    break;
                }
                case Avalugg: {
                    EntityAvaluggIce ice = new EntityAvaluggIce(user.world, user);
                    ice.moveTowards(target.getPosition());
                    ice.shoot(user, target.rotationPitch, target.rotationYaw, 0.0f, 1.5f, 1.0f);
                    user.world.spawnEntity(ice);
                }
            }
        }
    }
}

