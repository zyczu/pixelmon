/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class Reversal
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        int percentage = (int)user.getHealthPercent();
        user.attack.getAttackBase().basePower = percentage >= 71 ? 20 : (percentage >= 36 ? 40 : (percentage >= 21 ? 80 : (percentage >= 11 ? 100 : (percentage >= 5 ? 150 : 200))));
        return AttackResult.proceed;
    }
}

