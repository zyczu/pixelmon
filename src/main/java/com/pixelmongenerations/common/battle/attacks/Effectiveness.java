/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks;

public enum Effectiveness {
    Normal(1.0f),
    Super(2.0f),
    Max(4.0f),
    Not(0.5f),
    Barely(0.25f),
    None(0.0f);

    public float value;

    private Effectiveness(float f) {
        this.value = f;
    }
}

