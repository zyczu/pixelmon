/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.Value;
import com.pixelmongenerations.common.battle.attacks.ValueType;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Recover;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Roosting;

public class Roost
extends Recover {
    public Roost() {
        super(new Value(50, ValueType.WholeNumber));
    }

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        AttackResult result = super.applyEffectDuring(user, target);
        if (result == AttackResult.succeeded) {
            user.addStatus(new Roosting(user), user);
        }
        return result;
    }
}

