/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.zeffects;

import com.pixelmongenerations.common.battle.attacks.zeffects.ZEffect;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.FollowMe;

public class GainAttention
implements ZEffect {
    @Override
    public void applyEffect(PixelmonWrapper pokemon) {
        pokemon.addStatus(new FollowMe(), pokemon);
    }

    @Override
    public void getEffectText(PixelmonWrapper pokemon) {
        pokemon.bc.sendToAll("pixelmon.status.followme", pokemon.getNickname());
    }
}

