/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers;

import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public abstract class AttackModifierBase
extends EffectBase {
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        return AttackResult.proceed;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
    }

    @Override
    public boolean cantMiss(PixelmonWrapper user) {
        return false;
    }
}

