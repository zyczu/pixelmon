/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.ItemHeld;

public class StuffCheeks
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.hasHeldItem() && ItemHeld.canEatBerry(user)) {
            user.getConsumedItem().eatBerry(user);
            user.bc.sendToAll("pixelmon.status.stuffcheeks", user.getNickname());
            target.getBattleStats().modifyStat(2, StatsType.Defence, target, true, true);
            return AttackResult.succeeded;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", user.getNickname());
        return AttackResult.failed;
    }
}

