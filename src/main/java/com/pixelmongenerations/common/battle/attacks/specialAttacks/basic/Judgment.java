/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.ItemPlate;
import com.pixelmongenerations.core.enums.EnumType;

public class Judgment
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        user.attack.getAttackBase().attackType = this.getType(user.getHeldItem());
        return AttackResult.proceed;
    }

    public EnumType getType(ItemHeld item) {
        if (item instanceof ItemPlate) {
            return ((ItemPlate)item).getType();
        }
        return EnumType.Normal;
    }

    public EnumType getTypeFromForm(short form) {
        switch (form) {
            case 1: {
                return EnumType.Grass;
            }
            case 2: {
                return EnumType.Fire;
            }
            case 3: {
                return EnumType.Water;
            }
            case 4: {
                return EnumType.Flying;
            }
            case 5: {
                return EnumType.Bug;
            }
            case 6: {
                return EnumType.Poison;
            }
            case 7: {
                return EnumType.Electric;
            }
            case 8: {
                return EnumType.Psychic;
            }
            case 9: {
                return EnumType.Rock;
            }
            case 10: {
                return EnumType.Ground;
            }
            case 11: {
                return EnumType.Dark;
            }
            case 12: {
                return EnumType.Ghost;
            }
            case 13: {
                return EnumType.Steel;
            }
            case 14: {
                return EnumType.Fighting;
            }
            case 15: {
                return EnumType.Ice;
            }
            case 16: {
                return EnumType.Dragon;
            }
            case 17: {
                return EnumType.Fairy;
            }
        }
        return EnumType.Normal;
    }
}

