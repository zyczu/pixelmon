/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;

public class BurnUp
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.hasType(EnumType.Fire)) {
            ArrayList<EnumType> userTypes = new ArrayList<EnumType>(user.type);
            userTypes.remove((Object)EnumType.Fire);
            if (userTypes.isEmpty()) {
                userTypes.add(EnumType.Mystery);
            }
            user.setTempType(userTypes);
            user.removeStatus(StatusType.Freeze);
            return AttackResult.proceed;
        }
        user.bc.sendToAll("But it failed!", user.getNickname());
        return AttackResult.failed;
    }
}

