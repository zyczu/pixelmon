/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class ShellSideArm
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.getBaseStats().defence <= target.getBaseStats().spDef) {
            user.attack.overrideAttackCategory(AttackCategory.Physical);
        } else {
            user.attack.overrideAttackCategory(AttackCategory.Special);
        }
        return AttackResult.proceed;
    }
}

