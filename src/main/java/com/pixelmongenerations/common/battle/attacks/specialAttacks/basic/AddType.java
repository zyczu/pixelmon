/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;
import java.util.List;

public abstract class AddType
extends SpecialAttackBase {
    private transient EnumType type;

    public AddType(EnumType type) {
        this.type = type;
    }

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasType(this.type)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        List<EnumType> newType = this.getNewType(target);
        if (!user.bc.simulateMode) {
            target.addedType = this.type;
        }
        target.setTempType(newType);
        user.bc.sendToAll("pixelmon.effect.addtype", this.type.getLocalizedName(), target.getNickname());
        return AttackResult.succeeded;
    }

    private List<EnumType> getNewType(PixelmonWrapper target) {
        ArrayList<EnumType> newType = new ArrayList<EnumType>(target.type);
        newType.add(this.type);
        if (target.addedType != null && target.addedType != this.type) {
            newType.remove((Object)target.addedType);
        }
        return newType;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (PixelmonWrapper target : userChoice.targets) {
            pw.getBattleAI().weightSingleTypeChange(pw, userChoice, this.getNewType(target), target, bestUserChoices, bestOpponentChoices);
        }
    }
}

