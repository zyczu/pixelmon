/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.HarshSunlight;

public class MaxFlare
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (!(user.bc.globalStatusController.getWeather() instanceof HarshSunlight)) {
            HarshSunlight sunlight = new HarshSunlight(5);
            sunlight.applyEffect(user, target);
        }
        return AttackResult.proceed;
    }
}

