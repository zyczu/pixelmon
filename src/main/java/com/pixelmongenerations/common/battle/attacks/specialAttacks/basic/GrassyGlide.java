/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GrassyTerrain;

public class GrassyGlide
extends SpecialAttackBase {
    @Override
    public float modifyPriority(PixelmonWrapper pw) {
        return pw.bc.globalStatusController.getTerrain() instanceof GrassyTerrain ? pw.priority + 1.0f : pw.priority;
    }

    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        return AttackResult.proceed;
    }
}

