/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class Magnitude
extends SpecialAttackBase {
    private transient int magnitude;

    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.targetIndex == 0) {
            int i = RandomHelper.getRandomNumberBetween(1, 100);
            this.magnitude = 0;
            if (i <= 5) {
                this.magnitude = 4;
            } else if (i <= 15 && i > 5) {
                this.magnitude = 5;
            } else if (i <= 35 && i > 15) {
                this.magnitude = 6;
            } else if (i <= 65 && i > 35) {
                this.magnitude = 7;
            } else if (i <= 85 && i > 65) {
                this.magnitude = 8;
            } else if (i <= 95 && i > 85) {
                this.magnitude = 9;
            } else if (i <= 100 && i > 95) {
                this.magnitude = 10;
            }
            user.bc.sendToAll("pixelmon.effect.magnitude", this.magnitude);
        }
        user.attack.getAttackBase().basePower = user.bc.simulateMode ? 71 : (this.magnitude == 4 ? 10 : (this.magnitude == 5 ? 30 : (this.magnitude == 6 ? 50 : (this.magnitude == 7 ? 70 : (this.magnitude == 8 ? 90 : (this.magnitude == 9 ? 110 : (this.magnitude == 10 ? 150 : 10)))))));
        return AttackResult.proceed;
    }
}

