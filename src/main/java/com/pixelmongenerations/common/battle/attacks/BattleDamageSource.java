/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import net.minecraft.entity.Entity;
import net.minecraft.util.DamageSource;

public class BattleDamageSource
extends DamageSource {
    PixelmonWrapper source;

    public BattleDamageSource(String dmgIdentifier, PixelmonWrapper source) {
        super(dmgIdentifier);
        this.source = source;
    }

    @Override
    public Entity getTrueSource() {
        return this.source.pokemon;
    }

    @Override
    public boolean isDifficultyScaled() {
        return false;
    }

    @Override
    public boolean canHarmInCreative() {
        return true;
    }
}

