/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sleep;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.EarlyBird;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Hydration;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.ItemBerryStatus;
import java.util.ArrayList;

public class Rest
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        Sleep sleep;
        block3: {
            block2: {
                sleep = new Sleep(2);
                if (user.hasFullHealth()) break block2;
                if (!user.hasStatus(StatusType.Sleep, StatusType.HealBlock) && !Sleep.uproarActive(user) && !user.cannotHaveStatus(sleep, user, true)) break block3;
            }
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        user.removePrimaryStatus(false);
        user.addStatus(sleep, user);
        user.healEntityBy(user.getMaxHealth());
        user.bc.sendToAll("pixelmon.effect.healthsleep", user.getNickname());
        return AttackResult.succeeded;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        userChoice.raiseWeight(100.0f - pw.getHealthPercent());
        if (pw.hasPrimaryStatus()) {
            userChoice.raiseWeight(30.0f);
        }
        AbilityBase ability = pw.getBattleAbility();
        ItemHeld heldItem = pw.getUsableHeldItem();
        if (!(pw.getMoveset().hasAttack("Sleep Talk", "Snore") || ability instanceof EarlyBird || ability instanceof Hydration && pw.bc.globalStatusController.hasStatus(StatusType.Rainy))) {
            ItemBerryStatus berry;
            if (heldItem instanceof ItemBerryStatus && (berry = (ItemBerryStatus)heldItem).canHealStatus(StatusType.Sleep)) {
                return;
            }
            if (MoveChoice.canKOFromFull(bestOpponentChoices, pw, 3)) {
                userChoice.lowerTier(1);
            }
            userChoice.weight /= 2.0f;
        }
    }
}

