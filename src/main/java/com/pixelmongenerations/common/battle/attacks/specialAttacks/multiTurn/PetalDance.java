/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnSpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Confusion;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import net.minecraft.util.text.TextComponentTranslation;

public class PetalDance
extends MultiTurnSpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (!this.doesPersist(user)) {
            this.setPersists(user, true);
            this.setTurnCount(user, RandomHelper.getRandomNumberBetween(2, 3));
        }
        this.decrementTurnCount(user);
        if (this.getTurnCount(user) == 0) {
            this.setPersists(user, false);
            TextComponentTranslation message = new TextComponentTranslation("pixelmon.effect.thrash", user.getNickname());
            user.addStatus(new Confusion(), user, message);
        }
        return AttackResult.proceed;
    }

    @Override
    public void removeEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.getTurnCount(user) == 0 && this.doesPersist(user) && user.addStatus(new Confusion(), user)) {
            user.bc.sendToAll("pixelmon.effect.thrash", user.getNickname());
        }
        this.setPersists(user, false);
    }

    @Override
    public boolean shouldNotLosePP(PixelmonWrapper user) {
        return this.doesPersist(user);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.tier >= 3) {
            userChoice.weight /= 2.0f;
        }
    }
}

