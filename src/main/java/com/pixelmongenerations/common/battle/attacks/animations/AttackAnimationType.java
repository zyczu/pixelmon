/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.animations;

public enum AttackAnimationType {
    StepForward,
    StepBackward,
    LeapForward;


    public static AttackAnimationType getAttackModifierType(String string) {
        if (string.equalsIgnoreCase("StepForward")) {
            return StepForward;
        }
        if (string.equalsIgnoreCase("StepBackward")) {
            return StepBackward;
        }
        if (string.equalsIgnoreCase("LeapForward")) {
            return LeapForward;
        }
        return null;
    }

    public static boolean isAttackAnimationType(String string) {
        if (string.equalsIgnoreCase("StepForward")) {
            return true;
        }
        if (string.equalsIgnoreCase("StepBackward")) {
            return true;
        }
        return string.equalsIgnoreCase("LeapForward");
    }
}

