/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import java.util.ArrayList;

public class Haze
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.attack.isAttack("Haze")) {
            if (user.targetIndex == 0 || user.bc.simulateMode) {
                for (PixelmonWrapper current : user.bc.getActiveUnfaintedPokemon()) {
                    current.getBattleStats().clearBattleStatsNoCrit();
                }
                user.bc.sendToAll("pixelmon.effect.modifierscleared", new Object[0]);
            }
            return AttackResult.succeeded;
        }
        if (user.attack.isAttack("Clear Smog")) {
            target.getBattleStats().clearBattleStatsNoCrit();
            user.bc.sendToAll("pixelmon.effect.clearsmog", target.getNickname());
            return AttackResult.proceed;
        }
        return AttackResult.proceed;
    }

    @Override
    public boolean cantMiss(PixelmonWrapper user) {
        return true;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        float weight = 0.0f;
        ArrayList<PixelmonWrapper> allies = pw.getTeamPokemon();
        for (PixelmonWrapper target : userChoice.targets) {
            float weightDifference = target.getBattleStats().getSumStages() * 20;
            if (allies.contains(target)) {
                weightDifference = -weightDifference;
            }
            weight += weightDifference;
        }
        userChoice.raiseWeight(weight);
    }
}

