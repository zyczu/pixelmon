/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.PsychicTerrain;

public class ExpandingForce
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.bc.globalStatusController.getTerrain() instanceof PsychicTerrain) {
            user.attack.baseAttack.basePower = (int)((double)user.attack.baseAttack.basePower * 1.5);
            if (user.getOpponentPokemon().size() > 1) {
                user.targets = target.getTeamPokemon();
            }
        }
        return AttackResult.proceed;
    }
}

