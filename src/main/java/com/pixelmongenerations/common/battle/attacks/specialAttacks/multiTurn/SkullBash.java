/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnCharge;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.util.ArrayList;

public class SkullBash
extends MultiTurnCharge {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (!this.doesPersist(user)) {
            this.setPersists(user, true);
            this.setTurnCount(user, 2);
        }
        this.decrementTurnCount(user);
        if (this.getTurnCount(user) == 1) {
            user.bc.sendToAll("pixelmon.effect.skullbash", user.getNickname());
            user.getBattleStats().modifyStat(1, StatsType.Defence);
            if (!user.getUsableHeldItem().affectMultiturnMove(user)) {
                return AttackResult.charging;
            }
        }
        this.setPersists(user, false);
        return AttackResult.proceed;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        super.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
        StatsEffect statsEffect = new StatsEffect(StatsType.Defence, 1, true);
        statsEffect.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
    }
}

