/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;

public class GuardianOfAlola
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (!target.hasStatus(StatusType.Protect)) {
            float damage = (float)((double)target.getHealth() * 0.75);
            target.doBattleDamage(user, damage, DamageTypeEnum.ATTACKFIXED);
        } else {
            float damage = (float)((double)target.getHealth() * 0.1875);
            target.doBattleDamage(user, damage, DamageTypeEnum.ATTACKFIXED);
        }
        return AttackResult.hit;
    }
}

