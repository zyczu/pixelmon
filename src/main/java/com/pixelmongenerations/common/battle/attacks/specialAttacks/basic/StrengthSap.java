/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LiquidOoze;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class StrengthSap
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.getBattleStats().getStage(StatsType.Attack) == -6) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        int currentAttackStat = target.getBattleStats().getStatWithMod(StatsType.Attack);
        target.getBattleStats().decreaseStat(1, StatsType.Attack, user, true);
        if (user.getUsableHeldItem().getHeldItemType() == EnumHeldItems.bigRoot) {
            currentAttackStat = (int)((double)currentAttackStat * 1.3);
        }
        if (user.getAbility() instanceof LiquidOoze) {
            user.healEntityBy(currentAttackStat * -1);
        } else {
            user.healEntityBy(currentAttackStat);
        }
        return AttackResult.succeeded;
    }
}

