/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.FuryCutterStatus;
import com.pixelmongenerations.common.battle.status.StatusType;

public class FuryCutter
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        FuryCutterStatus furyCutter = (FuryCutterStatus)user.getStatus(StatusType.FuryCutter);
        if (furyCutter == null) {
            user.addStatus(new FuryCutterStatus(user.bc.battleTurn), user);
        } else {
            user.attack.getAttackBase().basePower = furyCutter.power;
            if (!user.bc.simulateMode) {
                if (furyCutter.power < 160) {
                    furyCutter.power *= 2;
                }
                furyCutter.turnInc = user.bc.battleTurn;
            }
        }
        return AttackResult.proceed;
    }

    @Override
    public void applyMissEffect(PixelmonWrapper user, PixelmonWrapper target) {
        user.removeStatus(StatusType.FuryCutter);
    }
}

