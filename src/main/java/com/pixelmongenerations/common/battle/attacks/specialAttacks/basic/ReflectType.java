/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;
import java.util.List;

public class ReflectType
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        List<EnumType> newTypes = target.type;
        user.setTempType(newTypes);
        user.bc.sendToAll("pixelmon.effect.reflecttype", user.getNickname(), target.getNickname());
        return AttackResult.succeeded;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (PixelmonWrapper target : userChoice.targets) {
            pw.getBattleAI().weightTypeChange(pw, userChoice, target.type, bestUserChoices, bestOpponentChoices);
        }
    }
}

