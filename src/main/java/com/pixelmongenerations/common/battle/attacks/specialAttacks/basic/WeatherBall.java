/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.ZAttackBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Hail;
import com.pixelmongenerations.common.battle.status.Rainy;
import com.pixelmongenerations.common.battle.status.Sandstorm;
import com.pixelmongenerations.common.battle.status.Sunny;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.EnumType;

public class WeatherBall
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        Weather weather = user.bc.globalStatusController.getWeather();
        EnumType enumType = weather instanceof Sunny && user.getHeldItem() != PixelmonItemsHeld.utilityUmbrella ? EnumType.Fire : (weather instanceof Rainy && user.getHeldItem() != PixelmonItemsHeld.utilityUmbrella ? EnumType.Water : (weather instanceof Sandstorm ? EnumType.Rock : (user.attack.getAttackBase().attackType = weather instanceof Hail ? EnumType.Ice : EnumType.Normal)));
        if (user.attack.getAttackBase().attackType != EnumType.Normal && !(user.attack.getAttackBase() instanceof ZAttackBase)) {
            user.attack.getAttackBase().basePower *= 2;
        }
        return AttackResult.proceed;
    }
}

