/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.animations;

import com.pixelmongenerations.common.battle.attacks.animations.AttackAnimationLeapForward;
import com.pixelmongenerations.common.battle.attacks.animations.AttackAnimationType;
import com.pixelmongenerations.common.battle.attacks.animations.IAttackAnimation;
import java.util.ArrayList;

public class AttackAnimationParser {
    public static ArrayList<IAttackAnimation> GetAnimation(String animationListString) {
        String[] animationList;
        ArrayList<IAttackAnimation> animations = new ArrayList<IAttackAnimation>();
        for (String animationString : animationList = animationListString.split(";")) {
            if (!AttackAnimationType.isAttackAnimationType(animationString) || AttackAnimationType.getAttackModifierType(animationString) != AttackAnimationType.LeapForward) continue;
            animations.add(new AttackAnimationLeapForward());
        }
        if (animations.isEmpty()) {
            animations.add(new AttackAnimationLeapForward());
        }
        return animations;
    }
}

