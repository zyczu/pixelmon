/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.TerrainExamine;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;

public class Camouflage
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        EnumType newType = this.getTypeChange(user);
        user.setTempType(newType);
        user.bc.sendToAll("pixelmon.effect.changetype", user.getNickname(), newType.getLocalizedName());
        return AttackResult.succeeded;
    }

    private EnumType getTypeChange(PixelmonWrapper user) {
        TerrainExamine.TerrainType terrainType = TerrainExamine.getTerrain(user);
        switch (terrainType) {
            case Cave: {
                return EnumType.Rock;
            }
            case Swamp: 
            case Sand: {
                return EnumType.Ground;
            }
            case Water: {
                return EnumType.Water;
            }
            case Ice: 
            case Snow: {
                return EnumType.Ice;
            }
            case Volcano: {
                return EnumType.Fire;
            }
            case Burial: {
                return EnumType.Ghost;
            }
            case Sky: {
                return EnumType.Flying;
            }
            case Space: {
                return EnumType.Dragon;
            }
            case Grass: {
                return EnumType.Grass;
            }
            case Misty: {
                return EnumType.Fairy;
            }
            case Electric: {
                return EnumType.Electric;
            }
            case Psychic: {
                return EnumType.Psychic;
            }
        }
        return EnumType.Normal;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        pw.getBattleAI().weightTypeChange(pw, userChoice, this.getTypeChange(pw).makeTypeList(), bestUserChoices, bestOpponentChoices);
    }
}

