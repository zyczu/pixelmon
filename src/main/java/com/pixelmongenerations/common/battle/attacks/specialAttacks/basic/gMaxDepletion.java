/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class gMaxDepletion
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.simulateMode) {
            return AttackResult.proceed;
        }
        if (target.lastAttack != null && target.lastAttack.pp > 0) {
            user.bc.sendToAll("pixelmon.effect.ppdrop", target.getNickname(), target.lastAttack.getAttackBase().getLocalizedName());
            target.lastAttack.pp = Math.max(target.lastAttack.pp - 2, 0);
            return AttackResult.proceed;
        }
        user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        return AttackResult.failed;
    }
}

