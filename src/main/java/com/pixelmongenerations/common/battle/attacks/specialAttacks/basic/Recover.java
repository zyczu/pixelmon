/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.Value;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MegaLauncher;
import java.util.ArrayList;

public class Recover
extends SpecialAttackBase {
    private int increment = 0;

    public Recover(Value ... values) {
        this.increment = values[0].value;
    }

    @Override
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasStatus(StatusType.HealBlock)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            return AttackResult.failed;
        }
        if (target.hasFullHealth()) {
            user.bc.sendToAll("pixelmon.effect.healfailed", target.getNickname());
            return AttackResult.failed;
        }
        float healAmount = this.getHealAmount(user, target);
        target.healEntityBy((int)healAmount);
        if (user == target) {
            user.bc.sendToAll("pixelmon.effect.washealed", target.getNickname());
        } else {
            user.bc.sendToAll("pixelmon.effect.healother", user.getNickname(), target.getNickname());
        }
        return AttackResult.succeeded;
    }

    private float getHealAmount(PixelmonWrapper user, PixelmonWrapper target) {
        float healPercent = this.increment;
        if (user != target && user.getBattleAbility() instanceof MegaLauncher) {
            healPercent = (float)((double)healPercent * 1.5);
        }
        return target.getPercentMaxHealth(healPercent);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (PixelmonWrapper target : userChoice.targets) {
            float healAmount = this.getHealAmount(pw, target);
            if (!pw.getTeamPokemon().contains(target)) continue;
            userChoice.raiseWeight(target.getHealthPercent(healAmount));
        }
    }
}

