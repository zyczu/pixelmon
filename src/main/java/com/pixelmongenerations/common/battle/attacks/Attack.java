/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks;

import com.pixelmongenerations.common.battle.attacks.AttackBase;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.attacks.TargetingInfo;
import com.pixelmongenerations.common.battle.attacks.animations.IAttackAnimation;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers.AttackModifierBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers.CriticalHit;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers.MultipleHit;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Assurance;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.BeatUp;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Fling;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.TripleKick;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnCharge;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnSpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.log.MoveResults;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.rules.clauses.SkyBattle;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AngerPoint;
import com.pixelmongenerations.common.entity.pixelmon.abilities.KeenEye;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Merciless;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Overcoat;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ParentalBond;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ShieldDust;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Sniper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SuperLuck;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Unaware;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.enums.forms.EnumHoopa;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import net.minecraft.entity.EntityLivingBase;

public class Attack {
    public static final float EFFECTIVE_NORMAL = 1.0f;
    public static final float EFFECTIVE_SUPER = 2.0f;
    public static final float EFFECTIVE_MAX = 4.0f;
    public static final float EFFECTIVE_NOT = 0.5f;
    public static final float EFFECTIVE_BARELY = 0.25f;
    public static final float EFFECTIVE_NONE = 0.0f;
    public static final int ATTACK_PHYSICAL = 0;
    public static final int ATTACK_SPECIAL = 1;
    public static final int ATTACK_STATUS = 2;
    public static final int NEVER_MISS = -1;
    public static final int IGNORE_SEMIINVULNERABLE = -2;
    public static final int NUM_ATTACKS = 767;
    private static AttackBase[] fullAttackList;
    private static Map<String, AttackBase> fullAttackMap;
    public AttackBase baseAttack;
    private AttackBase attackOverride;
    public int pp;
    public int ppBase;
    public int movePower;
    public int moveAccuracy;
    public boolean cantMiss;
    private boolean disabled;
    public MoveResults moveResult;
    public float damageResult;
    private AttackCategory attackCategoryOverride;
    public int savedPower;
    public int savedAccuracy;
    private EnumType typeOverride;
    public transient boolean isMaxMove = false;

    public Attack(AttackBase base) {
        this.initializeAttack(base);
    }

    public Attack(int attackIndex) {
        try {
            this.initializeAttack(fullAttackList[attackIndex]);
        }
        catch (IndexOutOfBoundsException | NullPointerException e) {
            Pixelmon.LOGGER.error("No attack found for index " + attackIndex);
        }
    }

    public Attack(String moveName) {
        if (Attack.hasAttack(moveName)) {
            this.initializeAttack(Attack.getAttackBase(moveName).get());
        } else {
            Pixelmon.LOGGER.error("No attack found with name: " + moveName);
        }
    }

    public static Optional<AttackBase> getAttackBase(String moveName) {
        if (Attack.hasAttack(moveName)) {
            return Optional.of(fullAttackMap.get(moveName.toLowerCase()));
        }
        return Optional.empty();
    }

    public void initializeAttack(AttackBase base) {
        this.baseAttack = base;
        this.movePower = this.getAttackBase().basePower;
        this.pp = this.getAttackBase().ppBase;
        this.ppBase = this.getAttackBase().ppBase;
    }

    public static boolean hasAttack(int attackIndex) {
        return fullAttackList[attackIndex] != null;
    }

    public static boolean hasAttack(String moveName) {
        return fullAttackMap.containsKey(moveName.toLowerCase());
    }

    public static AttackBase[] getAttacks(String[] nameList) {
        AttackBase[] attacks = new AttackBase[nameList.length];
        for (int i = 0; i < nameList.length; ++i) {
            attacks[i] = fullAttackMap.get(nameList[i].toLowerCase());
            if (attacks[i] != null) continue;
            Pixelmon.LOGGER.error("No attack found with name: " + nameList[i]);
        }
        return attacks;
    }

    public boolean use(PixelmonWrapper user, PixelmonWrapper target, MoveResults moveResults) {
        this.moveResult = moveResults;
        this.damageResult = -1.0f;
        if (user.bc == null || target.bc == null) return false;

        if (!this.checkSkyBattle(user.bc)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            moveResults.result = AttackResult.failed;
            return false;
        }
        if (!this.canUseMoveIfSignature(user)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            moveResults.result = AttackResult.failed;
            return false;
        }
        AbilityBase userAbility = user.getBattleAbility();
        AbilityBase targetAbility = target.getBattleAbility();
        if (!this.canHit(user, target) && !this.canHitNoTarget()) {
            moveResults.result = AttackResult.notarget;
            return true;
        }
        if (user == target) {
            for (PixelmonWrapper pixelmonWrapper : user.bc.getActiveUnfaintedPokemon()) {
                for (StatusBase status : pixelmonWrapper.getStatuses()) {
                    if (!status.stopsSelfStatusMove(pixelmonWrapper, user, this)) continue;
                    moveResults.result = AttackResult.failed;
                    return true;
                }
            }
            this.applySelfStatusMove(user, moveResults);
            return true;
        }
        if (user.targets.size() == 1) {
            ArrayList<PixelmonWrapper> opponents = user.bc.getOpponentPokemon(user.getParticipant());
            if (opponents.size() > 1) {
                for (PixelmonWrapper pw : opponents) {
                    if (pw == target) continue;
                    for (StatusBase statusBase : pw.getStatuses()) {
                        if (!statusBase.redirectAttack(user, pw, this)) continue;
                        target = pw;
                        break;
                    }
                    if (!pw.getBattleAbility().redirectAttack(user, pw, this)) continue;
                    target = pw;
                    break;
                }
            }
        }
        for (EffectBase effectBase : this.getAttackBase().effects) {
            try {
                if (effectBase.applyEffectStart(user, target) == AttackResult.proceed) continue;
                return true;
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        int[] modifiedMoveStats = new int[]{this.getAttackBase().basePower, this.getAttackBase().accuracy};
        modifiedMoveStats = userAbility.modifyPowerAndAccuracyUser(this.getAttackBase().basePower, this.getAttackBase().accuracy, user, target, this);
        for (PixelmonWrapper teammate : user.bc.getTeamPokemon(user)) {
            modifiedMoveStats = teammate.getBattleAbility().modifyPowerAndAccuracyTeammate(modifiedMoveStats[0], modifiedMoveStats[1], user, target, this);
        }
        modifiedMoveStats = targetAbility.modifyPowerAndAccuracyTarget(modifiedMoveStats[0], modifiedMoveStats[1], user, target, this);
        int saveAccuracy = 0;
        if (modifiedMoveStats[1] < 0) {
            saveAccuracy = modifiedMoveStats[1];
        }
        modifiedMoveStats = user.getUsableHeldItem().modifyPowerAndAccuracyUser(modifiedMoveStats, user, target, this);
        modifiedMoveStats = target.getUsableHeldItem().modifyPowerAndAccuracyTarget(modifiedMoveStats, user, target, this);
        for (int i = 0; i < this.getAttackBase().effects.size(); ++i) {
            EffectBase effect = this.getAttackBase().effects.get(i);
            if (effect instanceof StatusBase) continue;
            modifiedMoveStats = effect.modifyPowerAndAccuracyUser(modifiedMoveStats[0], modifiedMoveStats[1], user, target, this);
            modifiedMoveStats = effect.modifyPowerAndAccuracyTarget(modifiedMoveStats[0], modifiedMoveStats[1], user, target, this);
        }
        int beforeSize = user.getStatuses().size();
        for (int i = 0; i < beforeSize; ++i) {
            StatusBase statusBase = user.getStatus(i);
            modifiedMoveStats = statusBase.modifyPowerAndAccuracyUser(modifiedMoveStats[0], modifiedMoveStats[1], user, target, this);
            if (user.getStatuses().size() != beforeSize) break;
        }
        for (StatusBase statusBase : target.getStatuses()) {
            modifiedMoveStats = statusBase.modifyPowerAndAccuracyTarget(modifiedMoveStats[0], modifiedMoveStats[1], user, target, this);
        }
        for (StatusBase statusBase : user.bc.globalStatusController.getGlobalStatuses()) {
            modifiedMoveStats = statusBase.modifyPowerAndAccuracyTarget(modifiedMoveStats[0], modifiedMoveStats[1], user, target, this);
        }
        if (saveAccuracy < 0 && modifiedMoveStats[1] >= 0) {
            modifiedMoveStats[1] = saveAccuracy;
        }
        this.movePower = modifiedMoveStats[0];
        this.moveAccuracy = Math.min(modifiedMoveStats[1], 100);
        this.cantMiss = false;
        if (user.pokemon != null && target.pokemon != null) {
            user.pokemon.getLookHelper().setLookPositionWithEntity(target.pokemon, 0.0f, 0.0f);
        }
        double accuracy = this.moveAccuracy;
        if (this.moveAccuracy >= 0) {
            double combinedAccuracy;
            int evasion = target.getBattleStats().getEvasionStage();
            if (user.bc.globalStatusController.hasStatus(StatusType.Gravity)) {
                evasion = Math.max(-6, evasion - 2);
            }
            if (user.getBattleAbility() instanceof KeenEye) {
                evasion = Math.min(0, evasion);
            }
            if ((combinedAccuracy = (double)(user.getBattleStats().getAccuracyStage() - evasion)) > 6.0) {
                combinedAccuracy = 6.0;
            } else if (combinedAccuracy < -6.0) {
                combinedAccuracy = -6.0;
            }
            accuracy = (double)this.moveAccuracy * ((double)user.getBattleStats().GetAccOrEva(combinedAccuracy) / 100.0);
        }
        ArrayList<StatusBase> allStatuses = new ArrayList<StatusBase>(target.getStatuses());
        allStatuses.addAll(new ArrayList<GlobalStatusBase>(target.bc.globalStatusController.getGlobalStatuses()));
        boolean shouldNotLosePP = false;
        for (int i = 0; i < this.getAttackBase().effects.size(); ++i) {
            EffectBase effectBase = this.getAttackBase().effects.get(i);
            if (!(effectBase instanceof MultiTurnSpecialAttackBase)) continue;
            shouldNotLosePP = ((MultiTurnSpecialAttackBase)effectBase).shouldNotLosePP(user);
        }
        for (StatusBase statusBase : user.getStatuses()) {
            try {
                if (!statusBase.stopsIncomingAttackUser(target, user)) continue;
                return !shouldNotLosePP;
            }
            catch (Exception exc) {
                user.bc.battleLog.onCrash(exc, "Error calculating stopsIncomingAttack for " + statusBase.type.toString() + " for attack " + this.getAttackBase().getLocalizedName());
            }
        }
        for (StatusBase statusBase : allStatuses) {
            try {
                if (!statusBase.stopsIncomingAttack(target, user)) continue;
                this.onMiss(user, target, moveResults, statusBase);
                return !shouldNotLosePP;
            }
            catch (Exception exc) {
                user.bc.battleLog.onCrash(exc, "Error calculating stopsIncomingAttack for " + statusBase.type.toString() + " for attack " + this.getAttackBase().getLocalizedName());
            }
        }
        if (!target.getBattleAbility(user).allowsIncomingAttack(target, user, this) || !target.getUsableHeldItem().allowsIncomingAttack(target, user, this)) {
            try {
                this.onMiss(user, target, moveResults, targetAbility);
                return !shouldNotLosePP;
            }
            catch (Exception var27) {
                user.bc.battleLog.onCrash(var27, "Error calculating allowsIncomingAttack for attack " + this.getAttackBase().getLocalizedName());
            }
        }
        if (!user.getBattleAbility().allowsAttack(user, target, this)) {
            try {
                this.onMiss(user, target, moveResults, userAbility);
                return !shouldNotLosePP;
            }
            catch (Exception var27) {
                user.bc.battleLog.onCrash(var27, "Error calculating allowsAttack for attack " + this.getAttackBase().getLocalizedName());
            }
        }
        if (this.hasNoEffect(user, target)) {
            user.bc.sendToAll("pixelmon.battletext.noeffect", target.getNickname());
            this.onMiss(user, target, moveResults, (Object)EnumType.Mystery);
            return !shouldNotLosePP;
        }
        if (!shouldNotLosePP) {
            targetAbility.preProcessAttack(target, user, this);
        }
        this.cantMiss = this.cantMiss(user) || this.moveAccuracy < 0;
        if (user.bc.simulateMode) {
            this.moveResult.accuracy = this.moveAccuracy;
            accuracy = 100.0;
        }
        EffectBase critModifier = null;
        if (!this.cantMiss && !RandomHelper.getRandomChance((int)accuracy)) {
            this.onMiss(user, target, moveResults, null);
        } else {
            AttackResult finalResult = AttackResult.proceed;
            AttackResult applyEffectResult = AttackResult.proceed;
            for (EffectBase e : this.getAttackBase().effects) {
                try {
                    if (e instanceof AttackModifierBase) {
                        if (e instanceof CriticalHit) {
                            critModifier = e;
                        } else {
                            applyEffectResult = ((AttackModifierBase)e).applyEffectDuring(user, target);
                            if (applyEffectResult != AttackResult.proceed) {
                                finalResult = applyEffectResult;
                            }
                        }
                    } else if (e instanceof SpecialAttackBase) {
                        applyEffectResult = ((SpecialAttackBase)e).applyEffectDuring(user, target);
                        if (applyEffectResult != AttackResult.proceed) {
                            finalResult = applyEffectResult;
                        }
                    } else if (e instanceof MultiTurnSpecialAttackBase) {
                        applyEffectResult = ((MultiTurnSpecialAttackBase)e).applyEffectDuring(user, target);
                        if (applyEffectResult != AttackResult.proceed) {
                            break;
                        }
                    }

                    if (finalResult == AttackResult.succeeded || finalResult == AttackResult.failed || finalResult == AttackResult.charging || finalResult == AttackResult.notarget) {
                        moveResults.result = finalResult;
                        continue;
                    }
                    if (finalResult != AttackResult.hit) continue;
                    if (target.isAlive()) {
                        moveResults.result = AttackResult.hit;
                        continue;
                    }
                    moveResults.result = AttackResult.killed;
                }
                catch (Exception exc) {
                    user.bc.battleLog.onCrash(exc, "Error in applyEffect for " + e.getClass().toString() + " for attack " + this.getAttackBase().getLocalizedName());
                }
            }
            if (applyEffectResult == AttackResult.proceed) {
                if (userAbility instanceof ParentalBond) {
                    user.inMultipleHit = true;
                    user.inParentalBond = true;
                    for (EffectBase e : this.getAttackBase().effects) {
                        if (e instanceof BeatUp || e instanceof Fling || e instanceof MultiTurnCharge || e instanceof MultipleHit || e instanceof TripleKick) {
                            user.inParentalBond = false;
                            continue;
                        }
                        if (!(e instanceof Assurance)) continue;
                        this.getAttackBase().basePower *= 2;
                    }
                }
                this.executeAttackEffects(user, target, moveResults, critModifier, 1.0f);
                if (user.inParentalBond && this.getAttackCategory() != AttackCategory.Status && target.isAlive() && user.isAlive() && user.targets.size() == 1) {
                    for (EffectBase e : this.getAttackBase().effects) {
                        if (!(e instanceof Assurance)) continue;
                        this.getAttackBase().basePower *= 2;
                    }
                    user.inMultipleHit = false;
                    user.inParentalBond = false;
                    if (!(targetAbility instanceof ShieldDust)) {
                        this.executeAttackEffects(user, target, moveResults, critModifier, 0.5f);
                        user.bc.sendToAll("multiplehit.times", user.getNickname(), 2);
                    }
                }
                user.inParentalBond = false;
                this.doMove(user, target);
                for (EffectBase base : this.getAttackBase().effects) {
                    if (!(base instanceof SpecialAttackBase)) continue;
                    ((SpecialAttackBase)base).applyAfterEffect(user);
                }
            }
        }

        for(int i = 0; i < target.getStatusSize(); ++i) {
            int sizeBefore = target.getStatusSize();
            target.getStatus(i).onAttackEnd(target);
            if (sizeBefore > target.getStatusSize()) {
                --i;
            }
        }

        if (!user.bc.simulateMode) {
            EnumUpdateType[] arrenumUpdateType = new EnumUpdateType[]{EnumUpdateType.HP, EnumUpdateType.Moveset};
            if (user.getPlayerOwner() != null) {
                user.update(arrenumUpdateType);
            }
            if (target.getPlayerOwner() != null) {
                target.update(arrenumUpdateType);
            }
        }
        if (target.isFainted()) {
            shouldNotLosePP = false;
        }
        return !shouldNotLosePP;
    }

    public boolean hasNoEffect(PixelmonWrapper user, PixelmonWrapper target) {
        boolean hasNoEffect = this.getTypeEffectiveness(user, target) == 0.0D && (this.getAttackCategory() != AttackCategory.Status || this.isAttack("Poison Gas", "Poison Powder", "Thunder Wave", "Toxic"));
        if (!hasNoEffect && target.hasType(EnumType.Grass) && Overcoat.isPowderMove(this)) {
            hasNoEffect = true;
        }

        if (hasNoEffect && target.hasType(EnumType.Flying) && this.isAttack("Thousand Arrows") && !target.hasStatus(StatusType.SmackedDown)) {
            user.attack.overrideType(EnumType.Mystery);
            hasNoEffect = false;
        }

        for (EffectBase effectBase : this.getAttackBase().effects) {
            if (effectBase instanceof MultiTurnSpecialAttackBase && ((MultiTurnSpecialAttackBase)effectBase).ignoresType(user)) {
                hasNoEffect = false;
                break;
            }
        }

        return hasNoEffect;
    }

    private void executeAttackEffects(PixelmonWrapper user, PixelmonWrapper target, MoveResults moveResults, EffectBase critModifier, float damageMultiplier) {
        double crit = Attack.calcCriticalHit(critModifier, user, target);
        int power = (int)((float)this.doDamageCalc(user, target, crit) * damageMultiplier);
        this.damageResult = power;
        if (this.getAttackCategory() == AttackCategory.Status) {
            power = 0;
        } else if (target.isAlive()) {
            if (crit > 1.0) {
                if (user.targets.size() > 1) {
                    user.bc.sendToAll("pixelmon.battletext.criticalhittarget", target.getNickname());
                } else {
                    user.bc.sendToAll("pixelmon.battletext.criticalhit", new Object[0]);
                }
            }
            this.damageResult = target.doBattleDamage(user, power, DamageTypeEnum.ATTACK);
            moveResults.result = target.isAlive() ? AttackResult.hit : AttackResult.killed;
        } else {
            this.damageResult = -1.0f;
            moveResults.result = AttackResult.notarget;
        }
        AttackResult tempResult = this.applyAttackEffect(user, target);
        if (tempResult != null) {
            moveResults.result = tempResult;
        }
        Attack.applyContactLate(user, target);
        if (this.canRemoveBerry()) {
            target.getUsableHeldItem().tookDamage(user, target, this.damageResult, DamageTypeEnum.ATTACK);
        }
    }

    public void doMove(PixelmonWrapper user, PixelmonWrapper target) {
        EntityLivingBase owner;
        if (user.bc.simulateMode || user.pokemon == null || target.pokemon == null) {
            return;
        }
        if (user.pokemon.posX == 0.0 && user.pokemon.posY == 0.0 && user.pokemon.posZ == 0.0 && (owner = user.getParticipant().getEntity()) != null && !user.bc.simulateMode) {
            user.pokemon.setLocationAndAngles(owner.posX, owner.posY, owner.posZ, owner.rotationYaw, 0.0f);
        }
        for (IAttackAnimation anim : this.getAttackBase().animations) {
            anim.doMove(user.pokemon, target.pokemon);
        }
    }

    public int doDamageCalc(PixelmonWrapper userWrapper, PixelmonWrapper targetWrapper, double crit) {
        StatsType defenseStat;
        StatsType attackStat;
        if (this.movePower <= 0) {
            return 0;
        }
        AbilityBase userAbility = userWrapper.getBattleAbility();
        double stab = 1.0;
        if (this.hasSTAB(userWrapper)) {
            stab = 1.5;
        }
        stab = userAbility.modifyStab(stab);
        double type = this.getTypeEffectiveness(userWrapper, targetWrapper);
        double modifier = stab * type * crit;
        double attack = 0.0;
        double defense = 0.0;
        double Level2 = userWrapper.getLevelNum();
        if (this.getAttackCategory() == AttackCategory.Special) {
            attackStat = StatsType.SpecialAttack;
            defenseStat = this.isAttack("Psyshock", "Psystrike", "Secret Sword") ? StatsType.Defence : StatsType.SpecialDefence;
        } else if (this.isAttack("Body Press")) {
            attackStat = StatsType.Defence;
            defenseStat = StatsType.Defence;
        } else {
            attackStat = StatsType.Attack;
            defenseStat = StatsType.Defence;
        }
        attack = userWrapper.getBattleStats().getStatWithMod(attackStat);
        defense = targetWrapper.getBattleStats().getStatWithMod(defenseStat);
        if (crit > 1.0) {
            attack = Math.max((double)userWrapper.getBattleStats().getStatFromEnum(attackStat), attack);
            defense = Math.min((double)targetWrapper.getBattleStats().getStatFromEnum(defenseStat), defense);
        }
        if (this.isAttack("Chip Away", "Sacred Sword", "Darkest Lariat") || userAbility instanceof Unaware) {
            defense = targetWrapper.getBattleStats().getStatFromEnum(defenseStat);
        }
        if (this.isAttack("Beat Up") || targetWrapper.getBattleAbility(userWrapper) instanceof Unaware) {
            attack = userWrapper.getBattleStats().getStatFromEnum(attackStat);
        }
        double DmgRand = (double)RandomHelper.getRandomNumberBetween(85, 100) / 100.0;
        if (userWrapper.bc.simulateMode) {
            DmgRand = 1.0;
        }
        double DamageBase = (2.0 * Level2 / 5.0 + 2.0) * attack * (double)this.movePower * 0.02 / defense + 2.0;
        double Damage2 = DamageBase * modifier * DmgRand;
        Damage2 = userWrapper.getUsableHeldItem().preProcessAttackUser(userWrapper, targetWrapper, this, Damage2);
        Damage2 = targetWrapper.getUsableHeldItem().preProcessAttackTarget(userWrapper, targetWrapper, this, Damage2);
        if (userWrapper.targets.size() > 1) {
            Damage2 *= 0.75;
        }
        if (Damage2 < 1.0 && Damage2 > 0.0 && this.movePower > 0) {
            Damage2 = 1.0;
        }
        return (int)Damage2;
    }

    public double getBaseDamage(double level, double basePower, double attack, double defense) {
        return Math.floor(Math.floor(Math.floor(2.0 * level / 5.0 + 2.0) * basePower * attack / defense) / 50.0 + 2.0);
    }

    public void applySelfStatusMove(PixelmonWrapper user, MoveResults moveResults) {
        if (user.pokemon != null) {
            EntityPixelmon userPokemon = user.pokemon;
            userPokemon.getLookHelper().setLookPositionWithEntity(userPokemon, 0.0f, 0.0f);
        }
        for (int j = 0; j < this.getAttackBase().effects.size(); ++j) {
            EffectBase e = this.getAttackBase().effects.get(j);
            try {
                if (e instanceof StatsEffect) {
                    AttackResult statsResult = ((StatsEffect)e).applyStatEffect(user, user, this.getAttackBase());
                    if (moveResults.result == AttackResult.succeeded) continue;
                    moveResults.result = statsResult;
                    continue;
                }
                if (e instanceof SpecialAttackBase) {
                    if (e.applyEffectStart(user, user) != AttackResult.proceed) {
                        return;
                    }
                    this.moveResult.result = ((SpecialAttackBase)e).applyEffectDuring(user, user);
                    continue;
                }
                if (e instanceof MultiTurnSpecialAttackBase) {
                    this.moveResult.result = ((MultiTurnSpecialAttackBase)e).applyEffectDuring(user, user);
                    continue;
                }
                if (e instanceof StatusBase) {
                    e.applyEffect(user, user);
                    continue;
                }
                if (!(e instanceof CriticalHit) || user.getBattleStats().increaseCritStage(2)) continue;
                user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                continue;
            }
            catch (Exception exc) {
                user.bc.battleLog.onCrash(exc, "Error in applyEffect for " + e.getClass().toString() + " for attack " + this.getAttackBase().getLocalizedName());
            }
        }
        user.getUsableHeldItem().onStatModified(user);
        if (!user.bc.simulateMode) {
            NPCTrainer trainer;
            if (user.getPlayerOwner() != null) {
                user.update(EnumUpdateType.HP, EnumUpdateType.Moveset);
            }
            if (user.pokemon != null && (trainer = user.pokemon.getTrainer()) != null) {
                trainer.getPokemonStorage().updateAndSendToClient(user.pokemon, EnumUpdateType.Moveset, EnumUpdateType.HP);
            }
        }
        if (moveResults.result == AttackResult.proceed) {
            moveResults.result = AttackResult.succeeded;
        }
    }

    public AttackResult applyAttackEffect(PixelmonWrapper user, PixelmonWrapper target) {
        AttackResult returnResult = null;
        for (EffectBase e : this.getAttackBase().effects) {
            try {
                if (e instanceof StatsEffect) {
                    StatsEffect statsEffect = (StatsEffect)e;
                    if (!statsEffect.checkChance()) continue;
                    boolean abilityAllowsChange = target.getBattleAbility(user).allowsStatChange(target, user, statsEffect);
                    if (abilityAllowsChange) {
                        for (PixelmonWrapper ally : target.bc.getTeamPokemon(target)) {
                            if (ally.getBattleAbility().allowsStatChangeTeammate(ally, target, user, statsEffect)) continue;
                            abilityAllowsChange = false;
                            break;
                        }
                    }
                    if (!abilityAllowsChange) continue;
                    AttackResult statsResult = statsEffect.applyStatEffect(user, target, this.getAttackBase());
                    if (returnResult == AttackResult.succeeded) continue;
                    returnResult = statsResult;
                    continue;
                }
                if (e instanceof StatusBase) {
                    boolean shouldApply = true;
                    for (int i = 0; i < target.getStatusSize(); ++i) {
                        StatusBase et = target.getStatus(i);
                        if (user != target || !et.stopsStatusChange(et.type, target, user)) continue;
                        shouldApply = false;
                        break;
                    }
                    if (!shouldApply) continue;
                    e.applyEffect(user, target);
                    continue;
                }
                e.applyEffect(user, target);
            }
            catch (Exception exc) {
                user.bc.battleLog.onCrash(exc, "Error in applyEffect for " + e.getClass().toString() + " for attack " + this.getAttackBase().getLocalizedName());
            }
        }
        user.getUsableHeldItem().onStatModified(user);
        return returnResult;
    }

    public static void applyContact(PixelmonWrapper userPokemon, PixelmonWrapper targetPokemon) {
        if (!targetPokemon.hasStatus(StatusType.Substitute)) {
            userPokemon.getBattleAbility().applyEffectOnContactUser(userPokemon, targetPokemon);
            targetPokemon.getBattleAbility().applyEffectOnContactTarget(userPokemon, targetPokemon);
            targetPokemon.getUsableHeldItem().applyEffectOnContact(userPokemon, targetPokemon);
        }
    }

    public static void applyContactLate(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.attack != null && user.attack.getAttackBase().getMakesContact()) {
            if (!target.hasStatus(StatusType.Substitute)) {
                target.getBattleAbility().applyEffectOnContactTargetLate(user, target);
            }
        }
    }

    public static void postProcessAttackAllHits(PixelmonWrapper user, PixelmonWrapper target, Attack attack, float power, DamageTypeEnum damageType, boolean onSubstitute) {
        if (!user.inParentalBond) {
            for (EffectBase effect : attack.getAttackBase().effects) {
                effect.dealtDamage(user, target, attack, damageType);
            }
            if (!onSubstitute) {
                target.getBattleAbility().tookDamageTargetAfterMove(user, target, attack);
                target.getUsableHeldItem().postProcessAttackTarget(user, target, attack, power);
            }
            user.getUsableHeldItem().dealtDamage(user, target, attack, damageType);
        }
    }

    public void onMiss(PixelmonWrapper user, PixelmonWrapper target, MoveResults results, Object cause) {
        try {
            for (EffectBase effect : this.getAttackBase().effects) {
                if (!(effect instanceof MultiTurnSpecialAttackBase) || !((MultiTurnSpecialAttackBase)effect).isCharging(user, target)) continue;
                results.result = ((MultiTurnSpecialAttackBase)effect).applyEffectDuring(user, target);
                if (results.result != AttackResult.charging) continue;
                return;
            }
            if (cause instanceof StatusBase) {
                ((StatusBase)cause).stopsIncomingAttackMessage(target, user);
            } else if (cause instanceof AbilityBase) {
                ((AbilityBase)cause).allowsIncomingAttackMessage(target, user, this);
            } else if (cause instanceof EnumType) {
                user.bc.sendToAll("pixelmon.battletext.noeffect", target.getNickname());
            } else {
                user.bc.sendToAll("pixelmon.battletext.missedattack", target.getNickname());
                results.result = AttackResult.missed;
            }
            for (EffectBase effect : this.getAttackBase().effects) {
                effect.applyMissEffect(user, target);
            }
            user.getUsableHeldItem().onMiss(user, target, cause);
        }
        catch (Exception exc) {
            user.bc.battleLog.onCrash(exc, "Error in applyMissEffect for attack " + this.getAttackBase().getLocalizedName());
        }
        if (results.result != AttackResult.missed) {
            results.result = AttackResult.failed;
        }
    }

    public boolean hasSTAB(PixelmonWrapper user) {
        return user.type.contains((Object)this.getAttackBase().attackType);
    }

    public void setDisabled(boolean value, PixelmonWrapper pixelmon) {
        this.setDisabled(value, pixelmon, false);
    }

    public void setDisabled(boolean value, PixelmonWrapper pixelmon, boolean switching) {
        this.disabled = value;
        if (pixelmon.getParticipant() instanceof PlayerParticipant) {
            pixelmon.update(EnumUpdateType.Moveset);
        }
    }

    public boolean getDisabled() {
        return this.disabled;
    }

    public static double calcCriticalHit(EffectBase e, PixelmonWrapper user, PixelmonWrapper target) {
        block20: {
            AbilityBase targetAbility;
            AbilityBase userAbility;
            block19: {
                block18: {
                    block17: {
                        if (target.getBattleAbility(user).preventsCriticalHits(user)) break block17;
                        if (!target.hasStatus(StatusType.LuckyChant)) break block18;
                    }
                    return 1.0;
                }
                int critStage = 1;
                critStage += user.getUsableHeldItem().adjustCritStage(user);
                userAbility = user.getBattleAbility();
                if (userAbility instanceof SuperLuck) {
                    ++critStage;
                }
                if (user.attack.isAttack("Focus Energy") && !user.getBattleStats().increaseCritStage(2)) {
                    user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
                    user.attack.moveResult.result = AttackResult.failed;
                }
                float percent = 0.0625f;
                if (e instanceof CriticalHit) {
                    critStage += ((CriticalHit)e).stage;
                }
                if ((critStage += user.getBattleStats().getCritStage()) == 2) {
                    percent = 0.125f;
                } else if (critStage == 3) {
                    percent = 0.5f;
                } else if (critStage >= 4) {
                    percent = 1.0f;
                }
                if (user.bc.simulateMode && percent < 1.0f) {
                    percent = 0.0f;
                }
                if (RandomHelper.getRandomChance(percent)) break block19;
                if (!(user.getBattleAbility() instanceof Merciless)) break block20;
                if (!target.hasStatus(StatusType.Poison, StatusType.PoisonBadly)) break block20;
            }
            if ((targetAbility = target.getBattleAbility()) instanceof AngerPoint && !user.bc.simulateMode) {
                ((AngerPoint)targetAbility).wasCrit = true;
            }
            double crit = 1.5;
            if (userAbility instanceof Sniper) {
                crit *= 1.5;
            }
            return crit;
        }
        return 1.0;
    }

    public boolean canHit(PixelmonWrapper pixelmon1, PixelmonWrapper pixelmon2) {
        return pixelmon2 != null && !pixelmon2.isFainted();
    }

    public static boolean canMovesHit(PixelmonWrapper entity, PixelmonWrapper target) {
        boolean[] b = new boolean[4];
        int i1 = 0;
        b[3] = true;
        b[2] = true;
        b[1] = true;
        b[0] = true;
        for (int i = 0; i < entity.getMoveset().size(); ++i) {
            Attack a = entity.getMoveset().get(i);
            if (!a.canHit(entity, target)) {
                b[i1] = false;
            }
            ++i1;
        }
        return b[0] && b[1] && b[2] && b[3];
    }

    public boolean doesPersist(PixelmonWrapper entityPixelmon) {
        if (this.isAttack("Fly", "Bounce")) {
            return entityPixelmon.hasStatus(StatusType.Flying);
        }
        for (EffectBase e : this.getAttackBase().effects) {
            try {
                if (!e.doesPersist(entityPixelmon)) continue;
                return true;
            }
            catch (Exception exc) {
                entityPixelmon.bc.battleLog.onCrash(exc, "Error in doesPersist for " + e.getClass().toString() + " for attack " + this.getAttackBase().getLocalizedName());
            }
        }
        return entityPixelmon.hasStatus(StatusType.Recharge);
    }

    public boolean cantMiss(PixelmonWrapper user) {
        if (this.cantMiss) {
            return true;
        }
        for (EffectBase e : this.getAttackBase().effects) {
            try {
                if (!e.cantMiss(user)) continue;
                return true;
            }
            catch (Exception exc) {
                user.bc.battleLog.onCrash(exc, "Error in cantMiss for " + e.getClass().toString() + " for attack " + this.getAttackBase().getLocalizedName());
            }
        }
        return false;
    }

    public void sendEffectiveChat(PixelmonWrapper user, PixelmonWrapper target) {
        String s = null;
        if (this.getAttackCategory() != AttackCategory.Status) {
            float effectiveness = (float)this.getTypeEffectiveness(user, target);
            if (effectiveness == 0.0f) {
                if (!user.attack.isAttack("Thousand Arrows")) {
                    user.bc.sendToAll("pixelmon.battletext.noeffect", target.getNickname());
                    return;
                }
            }
            if (effectiveness == 0.5f || effectiveness == 0.25f) {
                if (!this.doesFixedDamage()) {
                    s = "pixelmon.battletext.wasnoteffective";
                }
            } else if (!(effectiveness != 2.0f && effectiveness != 4.0f || this.doesFixedDamage())) {
                s = "pixelmon.battletext.supereffective";
            }
            if (s != null) {
                if (user.targets.size() > 1) {
                    user.bc.sendToAll(s.concat("target"), target.getNickname());
                } else {
                    user.bc.sendToAll(s, new Object[0]);
                }
            }
        }
    }

    public static boolean dealsDamage(Attack attack) {
        return attack != null && attack.getAttackBase().basePower > 0;
    }

    public void saveAttack() {
        this.savedPower = this.getAttackBase().basePower;
        this.savedAccuracy = this.getAttackBase().accuracy;
    }

    public void restoreAttack() {
        this.getAttackBase().basePower = this.savedPower;
        this.getAttackBase().accuracy = this.savedAccuracy;
        this.resetOverrides();
    }

    public boolean isAttack(String ... attacks) {
        for (String a : attacks) {
            if (!this.getAttackBase().getUnlocalizedName().equalsIgnoreCase(a)) continue;
            return true;
        }
        return false;
    }

    public static boolean hasAttack(List<Attack> attackList, String ... attackNames) {
        for (Attack attack : attackList) {
            if (attack == null || !attack.isAttack(attackNames)) continue;
            return true;
        }
        return false;
    }

    public static boolean hasOffensiveAttackType(List<Attack> attackList, EnumType type) {
        for (Attack attack : attackList) {
            if (attack == null || attack.getAttackBase().attackType != type || attack.getAttackCategory() == AttackCategory.Status) continue;
            return true;
        }
        return false;
    }

    public void createMoveChoices(PixelmonWrapper pw, ArrayList<MoveChoice> choices, boolean includeAllies) {
        ArrayList<PixelmonWrapper> targets = new ArrayList<PixelmonWrapper>();
        TargetingInfo info = this.getAttackBase().targetingInfo;
        if (info.hitsSelf) {
            targets.add(pw);
        }
        if (info.hitsAdjacentAlly && (includeAllies || info.hitsAll)) {
            targets.addAll(pw.bc.getTeamPokemonExcludeSelf(pw));
        }
        if (info.hitsAdjacentFoe) {
            targets.addAll(pw.bc.getOpponentPokemon(pw));
        }
        if (info.hitsAll) {
            choices.add(new MoveChoice(pw, this, targets));
        } else {
            for (PixelmonWrapper target : targets) {
                ArrayList<PixelmonWrapper> targetArray = new ArrayList<PixelmonWrapper>(1);
                targetArray.add(target);
                choices.add(new MoveChoice(pw, this, targetArray));
            }
        }
    }

    public ArrayList<MoveChoice> createMoveChoices(PixelmonWrapper pw, boolean includeAllies) {
        ArrayList<MoveChoice> choices = new ArrayList<MoveChoice>();
        this.createMoveChoices(pw, choices, includeAllies);
        return choices;
    }

    public ArrayList<Attack> createList() {
        ArrayList<Attack> list = new ArrayList<Attack>(1);
        list.add(this);
        return list;
    }

    public boolean equals(Object compare) {
        if (!(compare instanceof Attack)) {
            return false;
        }
        try {
            return this.getAttackBase().getUnlocalizedName().equalsIgnoreCase(((Attack)compare).getAttackBase().getUnlocalizedName());
        }
        catch (NullPointerException er) {
            System.out.println("Pixelmon failed to get an attack from object: " + compare);
            return false;
        }
    }

    public int hashCode() {
        if (this.getAttackBase() == null) {
            return 0;
        }
        return this.getAttackBase().getUnlocalizedName().hashCode();
    }

    public String toString() {
        return this.getAttackBase().getUnlocalizedName();
    }

    public double getTypeEffectiveness(PixelmonWrapper user, PixelmonWrapper target) {
        List<EnumType> effectiveTypes = target.getEffectiveTypes(user, target);
        double effectiveness = EnumType.getTotalEffectiveness(effectiveTypes, this.getType());
        for (EffectBase e : this.getAttackBase().effects) {
            effectiveness = e.modifyTypeEffectiveness(effectiveTypes, this.getAttackBase().attackType, effectiveness);
        }
        if (user.bc.rules.hasClause("inverse")) {
            effectiveness = EnumType.inverseEffectiveness((float)effectiveness);
        }
        return effectiveness;
    }

    public boolean canRemoveBerry() {
        return this.isAttack("Bug Bite", "Pluck", "Knock Off");
    }

    public Attack copy() {
        Attack newAttack = new Attack(this.getAttackBase());
        newAttack.pp = this.pp;
        newAttack.ppBase = this.ppBase;
        return newAttack;
    }

    public boolean checkSkyBattle(BattleControllerBase bc) {
        return !bc.rules.hasClause("sky") || SkyBattle.isMoveAllowed(this);
    }

    public boolean canHitNoTarget() {
        return this.isAttack("Doom Desire", "Future Sight", "Imprison", "Spikes", "Stealth Rock", "Sticky Web", "Toxic Spikes");
    }

    public boolean doesFixedDamage() {
        return this.isAttack("Nature's Madness");
    }

    public boolean canUseMoveIfSignature(PixelmonWrapper user) {
        if (this.isAttack("Hyperspace Fury")) {
            return user.getSpecies() == EnumSpecies.Hoopa && user.getForm() == EnumHoopa.UNBOUND.getForm();
        }
        if (this.isAttack("Dark Void")) {
            return user.getSpecies() == EnumSpecies.Darkrai;
        }
        return true;
    }

    public static Collection<AttackBase> getMoves() {
        return fullAttackMap.values();
    }

    public static void registerMoves(List<AttackBase> moves) {
        if (fullAttackList == null) {
            int index = moves.stream().mapToInt(a -> a.attackIndex).max().getAsInt() + 1;
            fullAttackList = new AttackBase[index];
            fullAttackMap = new HashMap<String, AttackBase>();
            for (AttackBase a : moves) {
                Attack.fullAttackList[a.attackIndex] = a;
                fullAttackMap.put(a.getUnlocalizedName().toLowerCase(), a);
                fullAttackMap.put(a.getLocalizedName().toLowerCase(), a);
            }
        }
    }

    public AttackBase getAttackBase() {
        return this.baseAttack;
    }

    public void resetOverrides() {
        this.overrideAttackCategory(null);
        this.overrideType(null);
    }

    public AttackCategory getAttackCategory() {
        return this.attackCategoryOverride != null ? this.attackCategoryOverride : this.getAttackBase().attackCategory;
    }

    public void overrideAttackCategory(AttackCategory category) {
        this.attackCategoryOverride = category;
    }

    public EnumType getType() {
        return this.typeOverride != null ? this.typeOverride : this.getAttackBase().attackType;
    }

    public void overrideType(EnumType type) {
        this.typeOverride = type;
    }
}

