/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers.AttackModifierBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Hail;
import com.pixelmongenerations.common.battle.status.HarshSunlight;
import com.pixelmongenerations.common.battle.status.Rainy;
import com.pixelmongenerations.common.battle.status.Sunny;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;

public class AlwaysHit
extends AttackModifierBase {
    @Override
    public boolean cantMiss(PixelmonWrapper user) {
        block8: {
            block7: {
                Weather weather = user.bc.globalStatusController.getWeather();
                PixelmonWrapper opponent = user.bc.getOppositePokemon(user);
                if (weather instanceof Hail) {
                    if (user.attack.isAttack("Blizzard")) {
                        return true;
                    }
                }
                if (weather instanceof Rainy) {
                    if (user.attack.isAttack("Thunder", "Hurricane") && opponent.getHeldItem() != PixelmonItemsHeld.utilityUmbrella) {
                        return true;
                    }
                }
                if (weather instanceof HarshSunlight) break block7;
                if (!(weather instanceof Sunny)) break block8;
                if (!user.attack.isAttack("Thunder", "Hurricane") || opponent.getHeldItem() == PixelmonItemsHeld.utilityUmbrella) break block8;
            }
            user.attack.getAttackBase().accuracy = 50;
            return false;
        }
        return !user.attack.isAttack("Blizzard", "Thunder", "Hurricane");
    }
}

