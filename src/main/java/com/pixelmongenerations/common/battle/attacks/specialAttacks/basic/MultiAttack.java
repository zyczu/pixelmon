/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.heldItems.MemoryDrive;
import com.pixelmongenerations.core.enums.EnumType;

public class MultiAttack
extends SpecialAttackBase {
    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.getHeldItem() instanceof MemoryDrive) {
            user.attack.getAttackBase().attackType = ((MemoryDrive)user.getHeldItem()).getType();
        } else {
            user.attack.overrideType(EnumType.Normal);
        }
        return AttackResult.proceed;
    }
}

