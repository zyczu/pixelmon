/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks.specialAttacks.basic;

import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.SpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.log.MoveResults;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class FoulPlay
extends SpecialAttackBase {
    private boolean inProgress = false;

    @Override
    public AttackResult applyEffectStart(PixelmonWrapper user, PixelmonWrapper target) {
        MoveResults[] results;
        if (this.inProgress) {
            this.inProgress = false;
            return AttackResult.proceed;
        }
        this.inProgress = true;
        double oldAttackModifier = user.getBattleStats().getAttackModifier();
        int oldAttack = user.getBattleStats().attackStat;
        user.getBattleStats().changeStat(StatsType.Attack, (int)target.getBattleStats().getAttackModifier());
        user.getBattleStats().attackStat = target.getBattleStats().attackStat;
        for (MoveResults result : results = user.useAttackOnly()) {
            user.attack.moveResult.damage += result.damage;
            user.attack.moveResult.fullDamage += result.fullDamage;
            user.attack.moveResult.accuracy = result.accuracy;
        }
        this.inProgress = false;
        user.getBattleStats().changeStat(StatsType.Attack, (int)oldAttackModifier);
        user.getBattleStats().attackStat = oldAttack;
        return AttackResult.hit;
    }
}

