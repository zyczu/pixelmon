/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.attacks;

import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.attacks.EffectRegistry;
import com.pixelmongenerations.common.battle.attacks.Value;
import com.pixelmongenerations.common.battle.attacks.ValueType;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.modifiers.ChanceModifier;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.modifiers.ModifierBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.modifiers.ModifierType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.util.RegexPatterns;
import java.util.ArrayList;

public class EffectParser {
    private String effectTypeString;
    private String effectValueString;
    private Value[] values = new Value[2];
    private boolean hasModifier;
    private ArrayList<ModifierStoreClass> modifierStore = new ArrayList();

    public EffectBase ParseEffect(String effect) {
        if (effect.equalsIgnoreCase("None")) {
            return null;
        }
        String[] splits = effect.split(":");
        String[] effectSplits = splits[0].split("=");
        this.effectTypeString = effectSplits[0];
        if (effectSplits.length > 1) {
            this.effectValueString = effectSplits[1];
        }
        if (splits.length > 1) {
            this.hasModifier = true;
            int modifierNum = splits.length - 1;
            String[] modifierStrings = new String[modifierNum];
            System.arraycopy(splits, 1, modifierStrings, 0, splits.length - 1);
            for (String s : modifierStrings) {
                ModifierStoreClass m = new ModifierStoreClass();
                String[] modifierSplits = s.split("=");
                m.modifierTypeString = modifierSplits[0];
                if (modifierSplits.length > 1) {
                    m.modifierValueString = modifierSplits[1];
                } else {
                    m.modifierValueType = ValueType.None;
                }
                this.modifierStore.add(m);
            }
        } else {
            this.hasModifier = false;
        }
        this.calcEffectType();
        this.calcValue();
        return this.getEffect();
    }

    private EffectBase getEffect() {
        EffectBase effect = null;
        EffectBase effectBase = effect = StatsType.isStatsEffect(this.effectTypeString) ? new StatsEffect(StatsType.getStatsEffect(this.effectTypeString), this.values[0].value, this.modifierStoreContains(ModifierType.User)) : EffectRegistry.getEffect(this.effectTypeString, this.values);
        if (this.hasModifier) {
            for (ModifierStoreClass m : this.modifierStore) {
                if (m.effectModifier == ModifierType.Chance) {
                    effect.addModifier(new ChanceModifier(m.modifierValue));
                }
                if (m.effectModifier != ModifierType.Awake) continue;
                effect.addModifier(new ModifierBase(ModifierType.Awake));
            }
        }
        return effect;
    }

    private boolean modifierStoreContains(ModifierType user) {
        for (ModifierStoreClass m : this.modifierStore) {
            if (m.effectModifier != ModifierType.User) continue;
            return true;
        }
        return false;
    }

    private void calcEffectType() {
        if (this.hasModifier) {
            for (ModifierStoreClass m : this.modifierStore) {
                m.effectModifier = ModifierType.getModifierType(m.modifierTypeString);
            }
        }
    }

    private void calcValue() {
        try {
            if (this.effectValueString != null) {
                ValueType valueType;
                int value1 = 0;
                int value2 = 0;
                String stringValue = "";
                boolean isString = false;
                if (this.effectValueString.endsWith("%")) {
                    valueType = ValueType.Percent;
                    this.effectValueString = RegexPatterns.PERCENT_SYMBOL.matcher(this.effectValueString).replaceAll("");
                } else if (this.effectValueString.contains("to")) {
                    valueType = ValueType.Range;
                    int toInd = this.effectValueString.indexOf("to");
                    value2 = Integer.parseInt(this.effectValueString.substring(toInd + 2));
                    this.effectValueString = this.effectValueString.substring(0, toInd);
                } else {
                    valueType = ValueType.WholeNumber;
                }
                this.effectValueString = RegexPatterns.PLUS_SYMBOL.matcher(this.effectValueString).replaceAll("");
                try {
                    value1 = Integer.parseInt(this.effectValueString);
                }
                catch (NumberFormatException e) {
                    isString = true;
                    valueType = ValueType.String;
                    stringValue = this.effectValueString;
                    this.values[0] = new Value(stringValue, valueType);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                if (!isString) {
                    this.values[0] = new Value(value1, valueType);
                    this.values[1] = new Value(value2, null);
                }
            }
            this.modifierStore.stream().filter(m -> m.modifierValueString != null).forEach(m -> {
                if (m.modifierValueString.endsWith("%")) {
                    m.modifierValueType = ValueType.Percent;
                    m.modifierValueString = RegexPatterns.PERCENT_SYMBOL.matcher(m.modifierValueString).replaceAll("");
                } else {
                    m.modifierValueType = ValueType.WholeNumber;
                }
                m.modifierValue = Integer.parseInt(m.modifierValueString);
            });
        }
        catch (Exception exception) {
            // empty catch block
        }
    }

    public class ModifierStoreClass {
        public ModifierType effectModifier;
        public String modifierTypeString;
        public String modifierValueString;
        public ValueType modifierValueType;
        public int modifierValue;
    }
}

