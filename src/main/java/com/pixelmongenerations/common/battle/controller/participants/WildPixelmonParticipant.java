/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.participants;

import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.Stats;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import java.util.Iterator;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.text.TextComponentBase;
import net.minecraft.util.text.TextComponentTranslation;

public class WildPixelmonParticipant
extends BattleParticipant {
    private boolean isBlockBattleParticipant = false;
    private static int idCounter = 0;

    public WildPixelmonParticipant(boolean isGrassBattleParticipant, EntityPixelmon ... pixelmon) {
        super(pixelmon.length);
        this.init(pixelmon);
        this.isBlockBattleParticipant = isGrassBattleParticipant;
    }

    public WildPixelmonParticipant(EntityPixelmon ... pixelmon) {
        super(pixelmon.length);
        this.init(pixelmon);
    }

    private void init(EntityPixelmon[] pixelmon) {
        this.allPokemon = new PixelmonWrapper[pixelmon.length];
        for (int i = 0; i < pixelmon.length; ++i) {
            PixelmonWrapper pw;
            this.allPokemon[i] = pw = new PixelmonWrapper((BattleParticipant)this, pixelmon[i], i);
            this.controlledPokemon.add(pw);
            pixelmon[i].setPokemonId(new int[]{-1, idCounter++});
        }
    }

    @Override
    public ParticipantType getType() {
        return ParticipantType.WildPokemon;
    }

    @Override
    public boolean hasMorePokemon() {
        return this.countAblePokemon() == 1;
    }

    @Override
    public boolean hasMorePokemonReserve() {
        return false;
    }

    @Override
    public boolean canGainXP() {
        return false;
    }

    @Override
    public void startBattle(BattleControllerBase bc) {
        super.startBattle(bc);
        boolean isBoss = false;
        for (PixelmonWrapper pw : this.controlledPokemon) {
            EnumBossMode bossMode = pw.pokemon.getBossMode();
            if (bossMode != EnumBossMode.NotBoss) {
                isBoss = true;
                int lvl = 1;
                for (BattleParticipant p : bc.participants) {
                    if (p.team == this.team || !(p instanceof PlayerParticipant)) continue;
                    int highestLevel = ((PlayerParticipant)p).getHighestLevel();
                    lvl = Math.max(lvl, highestLevel);
                }
                Stats stats = pw.getStats();
                stats.IVs.maximizeIVs();
                stats.EVs.randomizeMaxEVs();
                pw.setTempLevel(lvl += bossMode.extraLevels);
            }
            pw.setHealth(pw.getMaxHealth());
        }
        this.setBattleAI(isBoss ? PixelmonConfig.battleAIBoss.createAI(this) : PixelmonConfig.battleAIWild.createAI(this));
    }

    @Override
    public void endBattle() {
        for (PixelmonWrapper pw : this.controlledPokemon) {
            pw.pokemon.endBattle();
            if (!pw.isFainted() && !this.shouldDespawn() && !this.isBlockBattleParticipant) continue;
            pw.pokemon.setDead();
        }
    }

    private boolean shouldDespawn() {
        if (PixelmonConfig.despawnOnFleeOrLoss) {
            if (this.bc.rules.battleType == EnumBattleType.Single) {
                return this.bc.getOpponents(this).get(0) instanceof PlayerParticipant;
            }
            return true;
        }
        return false;
    }

    @Override
    public void getNextPokemon(int position) {
    }

    @Override
    public int[] getNextPokemonID() {
        return null;
    }

    @Override
    public TextComponentBase getName() {
        String key = "";
        if (!this.controlledPokemon.isEmpty()) {
            key = "pixelmon." + ((PixelmonWrapper)this.controlledPokemon.get((int)0)).pokemon.getLocalizedName().toLowerCase() + ".name";
        }
        return new TextComponentTranslation(key, new Object[0]);
    }

    public PixelmonWrapper getPokemon() {
        int size = this.controlledPokemon.size();
        PixelmonWrapper wrapper = null;
        for (PixelmonWrapper pixelmonWrapper : this.controlledPokemon) {
            try {
                wrapper = pixelmonWrapper;
                break;
            }
            catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            }
        }
        return wrapper;
    }

    @Override
    public MoveChoice getMove(PixelmonWrapper pw) {
        if (this.bc == null) {
            return null;
        }
        if (!pw.getMoveset().isEmpty()) {
            return this.getBattleAI().getNextMove(pw);
        }
        this.bc.setFlee(pw.getPokemonID());
        return null;
    }

    @Override
    public PixelmonWrapper switchPokemon(PixelmonWrapper pw, int[] newPixelmonId) {
        return null;
    }

    @Override
    public boolean checkPokemon() {
        boolean allGood = true;
        for (PixelmonWrapper pw : this.controlledPokemon) {
            if (!pw.getMoveset().isEmpty()) continue;
            pw.pokemon.loadMoveset();
            pw.reloadMoveset();
            if (!pw.getMoveset().isEmpty()) continue;
            if (PixelmonConfig.printErrors) {
                Pixelmon.LOGGER.info("Couldn't load " + pw.pokemon.getLocalizedName() + "'s moves.");
            }
            allGood = false;
        }
        return allGood;
    }

    @Override
    public void updatePokemon(PixelmonWrapper pw) {
    }

    @Override
    public EntityLiving getEntity() {
        if (this.controlledPokemon.isEmpty()) {
            return null;
        }
        return ((PixelmonWrapper)this.controlledPokemon.get((int)0)).pokemon;
    }

    @Override
    public void updateOtherPokemon() {
    }

    @Override
    public void setPosition(double[] ds) {
    }

    @Override
    public String getDisplayName() {
        Iterator iterator = this.controlledPokemon.iterator();
        if (iterator.hasNext()) {
            PixelmonWrapper pw = (PixelmonWrapper)iterator.next();
            return pw.getNickname();
        }
        return super.getDisplayName();
    }

    public boolean isBoss() {
        for (PixelmonWrapper pw : this.controlledPokemon) {
            EnumBossMode bossMode = pw.pokemon.getBossMode();
            if (bossMode == EnumBossMode.NotBoss) continue;
            return true;
        }
        return false;
    }
}

