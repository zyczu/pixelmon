/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.ai;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.log.MoveResults;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import java.util.ArrayList;

public class MoveChoice
implements Comparable<MoveChoice> {
    public PixelmonWrapper user;
    public Attack attack;
    public ArrayList<PixelmonWrapper> targets;
    public MoveResults result;
    public int[] switchPokemon;
    public int tier;
    public float weight;
    public static final int TIER_USELESS = 0;
    public static final int TIER_MINIMAL = 1;
    public static final int TIER_NORMAL = 2;
    public static final int TIER_KO = 3;

    public MoveChoice(PixelmonWrapper user, Attack attack, ArrayList<PixelmonWrapper> targets) {
        this.user = user;
        this.attack = attack;
        this.targets = targets;
    }

    public MoveChoice(PixelmonWrapper user, int[] switchPokemon) {
        this.user = user;
        this.switchPokemon = switchPokemon;
    }

    public boolean isAttack() {
        return this.attack != null;
    }

    public boolean isStatusMove() {
        return this.isAttack() && this.attack.getAttackCategory() == AttackCategory.Status;
    }

    public boolean isOffensiveMove() {
        return this.isAttack() && this.attack.getAttackCategory() != AttackCategory.Status;
    }

    public boolean isSameType(MoveChoice other) {
        return this.isAttack() == other.isAttack();
    }

    public boolean isSimilarWeight(MoveChoice other) {
        if (this.isMiddleTier()) {
            return this.tier == other.tier && Math.abs(this.weight - other.weight) <= 20.0f;
        }
        return this.tier == other.tier && this.weight == other.weight;
    }

    public void raiseWeight(float newWeight) {
        this.weight += newWeight;
        if (this.weight > 0.0f) {
            this.raiseTier(2);
        }
    }

    public void raiseWeightLimited(float newWeight) {
        this.raiseWeight(newWeight);
        this.weight = Math.min(this.weight, 100.0f);
    }

    public void setWeight(float newWeight) {
        this.weight = 0.0f;
        this.raiseWeight(newWeight);
    }

    public void raiseTier(int newTier) {
        this.tier = Math.max(this.tier, newTier);
    }

    public void lowerTier(int newTier) {
        this.tier = Math.min(this.tier, newTier);
    }

    public boolean isMiddleTier() {
        return this.tier == 1 || this.tier == 2;
    }

    public void resetWeight() {
        this.tier = 0;
        this.weight = 0.0f;
    }

    public boolean hitsAlly() {
        if (!this.isAttack()) {
            return false;
        }
        ArrayList<PixelmonWrapper> allies = this.user.getTeamPokemonExcludeSelf();
        for (PixelmonWrapper target : this.targets) {
            if (!allies.contains(target)) continue;
            return true;
        }
        return false;
    }

    public ArrayList<MoveChoice> createList() {
        ArrayList<MoveChoice> list = new ArrayList<MoveChoice>(1);
        list.add(this);
        return list;
    }

    @Override
    public int compareTo(MoveChoice otherChoice) {
        if (otherChoice == null) {
            return 0;
        }
        if (this.tier == otherChoice.tier) {
            return (int)(this.weight - otherChoice.weight);
        }
        return this.tier - otherChoice.tier;
    }

    public String toString() {
        String s = this.isAttack() ? this.attack.getAttackBase().getUnlocalizedName() : "Switch";
        s = s + ", Tier " + this.tier + ", Weight " + this.weight;
        return s;
    }

    public static void checkBestChoice(MoveChoice choice, ArrayList<MoveChoice> bestChoices) {
        MoveChoice.checkBestChoice(choice, bestChoices, false);
    }

    public static void checkBestChoice(MoveChoice choice, ArrayList<MoveChoice> bestChoices, boolean excludeStatus) {
        if (choice.tier == 0 || excludeStatus && choice.isStatusMove()) {
            return;
        }
        if (bestChoices.isEmpty()) {
            bestChoices.add(choice);
        } else {
            int compare = bestChoices.get(0).compareTo(choice);
            if (compare <= 0) {
                if (compare < 0) {
                    bestChoices.clear();
                }
                bestChoices.add(choice);
            }
        }
    }

    public static float getMaxPriority(ArrayList<MoveChoice> choices) {
        float priority = 0.0f;
        boolean initialized = false;
        if (choices != null) {
            for (MoveChoice userChoice : choices) {
                if (userChoice.result == null) continue;
                priority = initialized ? Math.max(priority, userChoice.result.priority) : userChoice.result.priority;
                initialized = true;
            }
        }
        return priority;
    }

    public static boolean canOutspeed(ArrayList<MoveChoice> opponentChoices, PixelmonWrapper pw, ArrayList<MoveChoice> userChoices) {
        float userPriority = MoveChoice.getMaxPriority(userChoices);
        for (MoveChoice opponentChoice : opponentChoices) {
            if (opponentChoice.result.priority <= userPriority) {
                if (opponentChoice.result.priority != userPriority) continue;
                if (pw.bc.getFirstMover(pw, opponentChoice.user) != opponentChoice.user) continue;
            }
            return true;
        }
        return false;
    }

    public static boolean canOHKO(ArrayList<MoveChoice> opponentChoices, PixelmonWrapper pw) {
        for (MoveChoice opponentChoice : opponentChoices) {
            if (!opponentChoice.isOffensiveMove() || !opponentChoice.targets.contains(pw) || opponentChoice.tier < 3) continue;
            return true;
        }
        return false;
    }

    public static boolean canOutspeedAndOHKO(ArrayList<MoveChoice> opponentChoices, PixelmonWrapper pw, ArrayList<MoveChoice> userChoices) {
        float userPriority = MoveChoice.getMaxPriority(userChoices);
        for (MoveChoice opponentChoice : opponentChoices) {
            if (!opponentChoice.isOffensiveMove() || !opponentChoice.targets.contains(pw) || opponentChoice.tier < 3) continue;
            if (opponentChoice.result.priority <= userPriority) {
                if (opponentChoice.result.priority != userPriority) continue;
                if (pw.bc.getFirstMover(pw, opponentChoice.user) != opponentChoice.user) continue;
            }
            return true;
        }
        return false;
    }

    public static boolean canOutspeedAnd2HKO(ArrayList<MoveChoice> opponentChoices, PixelmonWrapper pw) {
        return MoveChoice.canOutspeedAnd2HKO(opponentChoices, pw, null);
    }

    public static boolean canOutspeedAnd2HKO(ArrayList<MoveChoice> opponentChoices, PixelmonWrapper pw, ArrayList<MoveChoice> userChoices) {
        return MoveChoice.canOutspeedAndKO(2, opponentChoices, pw, userChoices);
    }

    public static boolean canOutspeedAndKO(int numTurns, ArrayList<MoveChoice> opponentChoices, PixelmonWrapper pw, ArrayList<MoveChoice> userChoices) {
        float userPriority = MoveChoice.getMaxPriority(userChoices);
        int weightThreshold = 100 / numTurns;
        for (MoveChoice opponentChoice : opponentChoices) {
            if (!opponentChoice.isOffensiveMove() || !opponentChoice.targets.contains(pw)) continue;
            if (opponentChoice.tier >= 3) {
                return true;
            }
            if (opponentChoice.weight < (float)weightThreshold || !opponentChoice.isMiddleTier()) continue;
            if (opponentChoice.result.priority <= userPriority) {
                if (opponentChoice.result.priority != userPriority) continue;
                if (pw.bc.getFirstMover(pw, opponentChoice.user) != opponentChoice.user) continue;
            }
            return true;
        }
        return false;
    }

    public static boolean canKOFromFull(ArrayList<MoveChoice> opponentChoices, PixelmonWrapper pw, int numTurns) {
        for (MoveChoice opponentChoice : opponentChoices) {
            if (!opponentChoice.isOffensiveMove() || !opponentChoice.targets.contains(pw) || opponentChoice.result.fullDamage * numTurns < pw.getMaxHealth()) continue;
            return true;
        }
        return false;
    }

    public static boolean hasSuccessfulAttackChoice(ArrayList<MoveChoice> choices, String ... attackNames) {
        for (MoveChoice choice : choices) {
            if (!choice.isAttack() || !choice.attack.isAttack(attackNames) || choice.result != null && choice.result.result == AttackResult.failed) continue;
            return true;
        }
        return false;
    }

    public static ArrayList<MoveChoice> getAffectedChoices(MoveChoice choice, ArrayList<MoveChoice> opponentChoices) {
        ArrayList<MoveChoice> affectedChoices = new ArrayList<MoveChoice>(opponentChoices.size());
        if (choice.targets != null) {
            for (MoveChoice opponentChoice : opponentChoices) {
                if (!opponentChoice.isAttack() || !choice.targets.contains(opponentChoice.user)) continue;
                affectedChoices.add(opponentChoice);
            }
        }
        return affectedChoices;
    }

    public static ArrayList<MoveChoice> getTargetedChoices(PixelmonWrapper pw, ArrayList<MoveChoice> opponentChoices) {
        ArrayList<MoveChoice> targetedChoices = new ArrayList<MoveChoice>(opponentChoices.size());
        for (MoveChoice opponentChoice : opponentChoices) {
            if (opponentChoice.targets == null || !opponentChoice.targets.contains(pw)) continue;
            targetedChoices.add(opponentChoice);
        }
        return targetedChoices;
    }

    public static boolean hasPriority(ArrayList<MoveChoice> ... choices) {
        for (ArrayList<MoveChoice> list : choices) {
            for (MoveChoice choice : list) {
                if (!choice.isAttack() || choice.attack.getAttackBase().getPriority() == 0) continue;
                return true;
            }
        }
        return false;
    }

    public static ArrayList<ArrayList<MoveChoice>> splitChoices(ArrayList<PixelmonWrapper> pokemonList, ArrayList<MoveChoice> choices) {
        ArrayList<ArrayList<MoveChoice>> splitList = new ArrayList<ArrayList<MoveChoice>>(pokemonList.size());
        for (PixelmonWrapper pw : pokemonList) {
            ArrayList<MoveChoice> pokemonChoices = new ArrayList<MoveChoice>();
            for (MoveChoice choice : choices) {
                if (choice.user != pw) continue;
                pokemonChoices.add(choice);
            }
            splitList.add(pokemonChoices);
        }
        return splitList;
    }

    public static ArrayList<MoveChoice> mergeChoices(ArrayList<ArrayList<MoveChoice>> choices) {
        ArrayList<MoveChoice> mergeList = new ArrayList<MoveChoice>();
        for (ArrayList<MoveChoice> choiceList : choices) {
            mergeList.addAll(choiceList);
        }
        return mergeList;
    }

    public static ArrayList<MoveChoice> createChoicesFromChoices(PixelmonWrapper pw, ArrayList<MoveChoice> choices, boolean includeAllies) {
        ArrayList<MoveChoice> newChoices = new ArrayList<MoveChoice>();
        for (MoveChoice choice : choices) {
            if (!choice.isAttack()) continue;
            newChoices.addAll(choice.attack.createMoveChoices(pw, includeAllies));
        }
        return newChoices;
    }

    public static float getMaxDamagePercent(PixelmonWrapper pw, ArrayList<MoveChoice> opponentChoices) {
        float damagePercent = 0.0f;
        for (MoveChoice opponentChoice : opponentChoices) {
            if (!opponentChoice.isOffensiveMove() || !opponentChoice.targets.contains(pw)) continue;
            if (opponentChoice.tier >= 3) {
                return 100.0f;
            }
            float effectivePercent = opponentChoice.result.damage / opponentChoice.targets.size();
            damagePercent = Math.max(damagePercent, effectivePercent);
        }
        return damagePercent;
    }

    public static ArrayList<MoveChoice> createMoveChoicesFromList(ArrayList<Attack> possibleAttacks, PixelmonWrapper pw) {
        ArrayList<MoveChoice> possibleChoices = new ArrayList<MoveChoice>();
        for (Attack possibleAttack : possibleAttacks) {
            possibleAttack.createMoveChoices(pw, possibleChoices, false);
        }
        return possibleChoices;
    }

    public static boolean hasAttackCategory(ArrayList<MoveChoice> choices, AttackCategory category) {
        for (MoveChoice choice : choices) {
            if (!choice.isAttack() || choice.attack.getAttackCategory() != category) continue;
            return true;
        }
        return false;
    }

    public static boolean hasOffensiveAttackType(ArrayList<MoveChoice> choices, EnumType type) {
        for (MoveChoice choice : choices) {
            if (!choice.isOffensiveMove() || choice.attack.getAttackBase().attackType != type) continue;
            return true;
        }
        return false;
    }

    public static boolean hasSpreadMove(ArrayList<MoveChoice> choices) {
        for (MoveChoice choice : choices) {
            if (!choice.isOffensiveMove() || !choice.attack.getAttackBase().targetingInfo.hitsAll || !choice.attack.getAttackBase().targetingInfo.hitsAdjacentFoe) continue;
            return true;
        }
        return false;
    }

    public static float sumWeights(ArrayList<MoveChoice> choices) {
        float weightSum = 0.0f;
        for (MoveChoice choice : choices) {
            weightSum += choice.weight;
        }
        return weightSum;
    }

    public static boolean canBreakProtect(ArrayList<PixelmonWrapper> opponents, ArrayList<MoveChoice> choices) {
        if (MoveChoice.hasSuccessfulAttackChoice(choices, "Feint", "Hyperspace Fury", "Hyperspace Hole")) {
            return true;
        }
        for (PixelmonWrapper opponent : opponents) {
            if (!opponent.hasStatus(StatusType.Vanish)) continue;
            return true;
        }
        return false;
    }

    public static boolean hasMove(ArrayList<MoveChoice> choices, String ... moveNames) {
        for (MoveChoice choice : choices) {
            if (!choice.isAttack() || !choice.attack.isAttack(moveNames)) continue;
            return true;
        }
        return false;
    }
}

