/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.ai;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;

public class OpponentMemory {
    int[] pokemonID;
    private ArrayList<Attack> guessedAttacks;
    private ArrayList<Attack> knownAttacks;

    public OpponentMemory(PixelmonWrapper pokemon) {
        this.pokemonID = pokemon.getPokemonID();
        this.guessedAttacks = new ArrayList(pokemon.type.size());
        this.knownAttacks = new ArrayList(4);
        BaseStats stats = pokemon.getBaseStats();
        boolean physical = stats.attack >= stats.spAtt;
        for (EnumType type : pokemon.type) {
            if (type == null) continue;
            String guess = null;
            switch (type) {
                case Bug: {
                    guess = physical ? "X-Scissor" : "Bug Buzz";
                    break;
                }
                case Dark: {
                    guess = physical ? "Crunch" : "Dark Pulse";
                    break;
                }
                case Dragon: {
                    guess = physical ? "Dragon Claw" : "Dragon Pulse";
                    break;
                }
                case Electric: {
                    guess = physical ? "Wild Charge" : "Thunderbolt";
                    break;
                }
                case Fairy: {
                    guess = physical ? "Play Rough" : "Moonblast";
                    break;
                }
                case Fighting: {
                    guess = physical ? "Close Combat" : "Focus Blast";
                    break;
                }
                case Fire: {
                    guess = physical ? "Flare Blitz" : "Flamethrower";
                    break;
                }
                case Flying: {
                    guess = physical ? "Brave Bird" : "Air Slash";
                    break;
                }
                case Ghost: {
                    guess = physical ? "Shadow Claw" : "Shadow Ball";
                    break;
                }
                case Grass: {
                    guess = physical ? "Leaf Blade" : "Energy Ball";
                    break;
                }
                case Ground: {
                    guess = physical ? "Earthquake" : "Earth Power";
                    break;
                }
                case Ice: {
                    guess = physical ? "Icicle Crash" : "Ice Beam";
                    break;
                }
                case Normal: {
                    guess = physical ? "Return" : "Hyper Voice";
                    break;
                }
                case Poison: {
                    guess = physical ? "Poison Jab" : "Sludge Bomb";
                    break;
                }
                case Psychic: {
                    guess = physical ? "Zen Headbutt" : "Psychic";
                    break;
                }
                case Rock: {
                    guess = physical ? "Stone Edge" : "Power Gem";
                    break;
                }
                case Steel: {
                    guess = physical ? "Iron Head" : "Flash Cannon";
                    break;
                }
                case Water: {
                    guess = physical ? "Waterfall" : "Surf";
                    break;
                }
            }
            if (guess == null) continue;
            this.guessedAttacks.add(new Attack(guess));
        }
    }

    public void seeAttack(Attack attack) {
        if (this.knownAttacks.contains(attack)) {
            return;
        }
        if (attack.getAttackCategory() != AttackCategory.Status) {
            for (int i = 0; i < this.guessedAttacks.size(); ++i) {
                if (attack.getAttackBase().attackType != this.guessedAttacks.get((int)i).getAttackBase().attackType) continue;
                this.guessedAttacks.remove(i);
                break;
            }
        }
        if (this.guessedAttacks.size() + this.knownAttacks.size() >= 4) {
            if (this.guessedAttacks.isEmpty()) {
                this.knownAttacks.remove(0);
            } else {
                this.guessedAttacks.remove(RandomHelper.getRandomNumberBetween(0, this.guessedAttacks.size() - 1));
            }
        }
        this.knownAttacks.add(attack);
    }

    public ArrayList<Attack> getAttacks() {
        ArrayList<Attack> allAttacks = new ArrayList<Attack>(4);
        allAttacks.addAll(this.guessedAttacks);
        allAttacks.addAll(this.knownAttacks);
        return allAttacks;
    }
}

