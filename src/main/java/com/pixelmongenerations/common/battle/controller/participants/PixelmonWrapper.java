/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.participants;

import com.pixelmongenerations.api.events.DynamaxEvent;
import com.pixelmongenerations.api.events.MegaEvolutionEvent;
import com.pixelmongenerations.api.events.PixelmonFaintEvent;
import com.pixelmongenerations.api.events.battles.UseBattleItemEvent;
import com.pixelmongenerations.api.events.battles.UseMoveEvent;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.BattleDamageSource;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.attacks.MaxAttackHelper;
import com.pixelmongenerations.common.battle.attacks.TargetingInfo;
import com.pixelmongenerations.common.battle.attacks.ZAttackBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers.MultipleHit;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.attackModifiers.Recoil;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.BatonPass;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.BeatUp;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.TripleKick;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.multiTurn.MultiTurnSpecialAttackBase;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.CalcPriority;
import com.pixelmongenerations.common.battle.controller.ai.BattleAIBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackAction;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.log.BagItemAction;
import com.pixelmongenerations.common.battle.controller.log.MoveResults;
import com.pixelmongenerations.common.battle.controller.log.SwitchAction;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.TrainerParticipant;
import com.pixelmongenerations.common.battle.status.EntryHazard;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.NoStatus;
import com.pixelmongenerations.common.battle.status.ProtectVariation;
import com.pixelmongenerations.common.battle.status.Sleep;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Substitute;
import com.pixelmongenerations.common.battle.status.TempMoveset;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.Entity6CanBattle;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Comatose;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ComingSoon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Illusion;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Imposter;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Levitate;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LongReach;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Multitype;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Overcoat;
import com.pixelmongenerations.common.entity.pixelmon.abilities.RKSSystem;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SheerForce;
import com.pixelmongenerations.common.entity.pixelmon.abilities.StickyHold;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Trace;
import com.pixelmongenerations.common.entity.pixelmon.helpers.DynamaxQuery;
import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQuery;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.BattleStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.FriendShip;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.Level;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.Stats;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.NBTLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.ItemPokeball;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.common.item.heldItems.ItemMegaStone;
import com.pixelmongenerations.common.item.heldItems.ItemPlate;
import com.pixelmongenerations.common.item.heldItems.MemoryDrive;
import com.pixelmongenerations.common.item.heldItems.NoItem;
import com.pixelmongenerations.common.item.heldItems.ZCrystal;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.EnumMegaPokemon;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.forms.EnumGroudon;
import com.pixelmongenerations.core.enums.forms.EnumKyogre;
import com.pixelmongenerations.core.enums.forms.EnumMeloetta;
import com.pixelmongenerations.core.enums.forms.EnumNecrozma;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.battles.DynamaxPacket;
import com.pixelmongenerations.core.network.packetHandlers.battles.LevelUpUpdate;
import com.pixelmongenerations.core.network.packetHandlers.battles.MegaEvolve;
import com.pixelmongenerations.core.network.packetHandlers.battles.UltraBurst;
import com.pixelmongenerations.core.network.packetHandlers.battles.UseItem;
import com.pixelmongenerations.core.network.packetHandlers.battles.UsedZMove;
import com.pixelmongenerations.core.network.packetHandlers.battles.gui.StatusPacket;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class PixelmonWrapper {
    public EntityPixelmon pokemon;
    public NBTTagCompound nbt;
    private PokemonLink link;
    public BaseStats baseStats;
    private Stats stats;
    private int currentHP;
    private BattleParticipant participant;
    public BattleControllerBase bc;
    public Attack attack;
    public Attack selectedAttack;
    public Attack lastAttack;
    public Attack baseLastAttack;
    public int targetIndex;
    public Set<Attack> usedMoves = new HashSet<Attack>();
    public int escapeAttempts = 0;
    public int damageTakenThisTurn = 0;
    public int moveOrder;
    public float priority;
    public boolean canAttack = true;
    public boolean willTryFlee = false;
    public boolean isSwitching = false;
    public boolean willEvolve = false;
    public boolean nextSwitchIsMove;
    public int[] willUseItemPokemon;
    public ItemStack willUseItemInStack;
    public int willUseItemInStackInfo;
    public boolean wait;
    public int battlePosition;
    private int partyPosition;
    private double[] basePos;
    private List<EnumType> initialType = null;
    public EnumType addedType;
    public AbilityBase tempAbility = null;
    private AbilityBase initialAbility;
    private ItemHeld initialHeldItem = NoItem.noItem;
    private boolean returnHeldItem = false;
    public List<PixelmonWrapper> targets = new ArrayList<PixelmonWrapper>();
    public int[] newPokemonID;
    public boolean inMultipleHit = false;
    public boolean inParentalBond = false;
    public int protectsInARow = 0;
    public boolean hasAwardedExp = false;
    private Set<PixelmonWrapper> attackers = new HashSet<PixelmonWrapper>();
    public boolean switchedThisTurn = false;
    public boolean changeBurmy = false;
    public Attack choiceLocked;
    public boolean choiceSwapped = false;
    public int metronomeBoost;
    private ItemHeld consumedHeldItem = NoItem.noItem;
    public boolean eatenBerry = false;
    public boolean eatingBerry = false;
    public int lastDirectDamage = -1;
    public AttackCategory lastDirectCategory;
    public int lastDirectPosition;
    public EvolutionQuery evolution = null;
    private Level level;
    private int levelNum;
    private int dynamaxLevel;
    private int experience;
    private boolean tempLevel;
    private int origLevel;
    int startHealth;
    private Moveset moveset = new Moveset();
    private BattleStats battleStats = new BattleStats(this);
    private List<StatusBase> status = new ArrayList<StatusBase>();
    private AbilityBase ability;
    private ItemHeld heldItem = NoItem.noItem;
    public List<EnumType> type = new ArrayList<EnumType>();
    private FriendShip friendship;
    public boolean isMega;
    public boolean isPrimal;
    public boolean isAsh;
    public boolean isUltra;
    private int form;
    public boolean animateHP = true;
    public boolean totem;
    public boolean alpha;
    public boolean noble;
    public boolean suppressedAbility = false;
    private boolean isDynamaxed;
    public int dynamaxTurnsLeft = 3;
    public boolean willDynamax = false;

    private PixelmonWrapper(BattleParticipant participant) {
        this.participant = participant;
    }

    public PixelmonWrapper(BattleParticipant participant, EntityPixelmon pokemon, int partyPosition) {
        this(participant);
        this.link = new EntityLink(pokemon);
        this.pokemon = pokemon;
        this.pokemon.setPixelmonWrapper(this);
        this.loadLink(this.link);
        this.isMega = pokemon.isMega;
        this.totem = pokemon.isTotem();
        this.alpha = pokemon.isAlpha();
        this.noble = pokemon.isNoble();
    }

    public PixelmonWrapper(BattleParticipant participant, EntityPixelmon pixelmon, int partyPosition, BattleControllerBase bc) {
        this(participant, pixelmon, partyPosition);
        this.pokemon.battleController = bc;
        this.bc = bc;
    }

    public PixelmonWrapper(BattleParticipant participant, NBTTagCompound nbt, int partyPosition) {
        this(participant);
        this.nbt = nbt;
        this.link = new NBTLink(nbt, participant.getStorage());
        this.loadLink(this.link);
        this.isMega = false;
        this.partyPosition = partyPosition;
    }

    private void loadLink(PokemonLink link) {
        this.moveset = link.getMoveset().copy();
        this.moveset.pokemonLink = link;
        StatusPersist status = link.getPrimaryStatus();
        if (status != NoStatus.noStatus) {
            this.status.add(status);
        }
        this.initialHeldItem = this.heldItem = link.getHeldItem();
        this.initialAbility = this.ability = link.getAbility();
        this.baseStats = link.getBaseStats();
        this.stats = link.getStats();
        this.currentHP = link.getHealth();
        this.type = link.getType();
        this.friendship = new FriendShip(this);
        FriendShip cloneFriendship = link.getFriendship();
        this.friendship.setFriendship(cloneFriendship.getFriendship());
        this.isMega = false;
        this.form = link.getForm();
        this.level = new Level(this);
        this.levelNum = link.getLevel();
        this.experience = link.getExp();
        this.level.updateExpToNextLevel();
        this.totem = link.isTotem();
        this.alpha = link.isAlpha();
        this.noble = link.isNoble();
        this.dynamaxLevel = link.getDynamaxLevel();
        if (this.getSpecies() == EnumSpecies.Zacian) {
            if (this.heldItem == PixelmonItemsHeld.crownedSword) {
                if (this.moveset.hasAttack("Iron Head")) {
                    this.moveset.replaceMove("Iron Head", new Attack("Behemoth Blade"));
                }
            }
        } else if (this.getSpecies() == EnumSpecies.Zamazenta && this.heldItem == PixelmonItemsHeld.crownedShield) {
            if (this.moveset.hasAttack("Iron Head")) {
                this.moveset.replaceMove("Iron Head", new Attack("Behemoth Bash"));
            }
        }
    }

    public BaseStats getBaseStats() {
        return this.baseStats;
    }

    public Stats getStats() {
        return this.stats;
    }

    public BattleStats getBattleStats() {
        return this.battleStats;
    }

    public List<EnumType> getInitialType() {
        return this.initialType;
    }

    public ItemHeld getInitialHeldItem() {
        return this.initialHeldItem;
    }

    public FriendShip getFriendship() {
        return this.friendship;
    }

    public int getFriendshipValue() {
        return this.friendship.getFriendship();
    }

    public Set<PixelmonWrapper> getAttackers() {
        return this.attackers;
    }

    public boolean hasType(EnumType ... types) {
        int var3 = types.length;
        for (EnumType type : types) {
            if (!this.type.contains((Object)type)) continue;
            return true;
        }
        return false;
    }

    public void clearTurnVariables() {
        this.canAttack = true;
        this.willTryFlee = false;
        if (this.isAlive()) {
            this.isSwitching = false;
        }
        this.switchedThisTurn = false;
    }

    public void setMoveTargets(PixelmonWrapper ... pokemon) {
        this.targets.clear();
        Collections.addAll(this.targets, pokemon);
    }

    public void selectAIAction() {
        if (this.attack == null || !this.attack.doesPersist(this)) {
            if (this.bc == null) {
                this.bc = this.participant.bc;
            }
            this.chooseMove();
        }
    }

    public void chooseMove() {
        this.chooseMove(this.participant.getMove(this));
    }

    public void chooseMove(MoveChoice moveChoice) {
        if (moveChoice != null) {
            if (moveChoice.isAttack()) {
                this.setAttack(moveChoice.attack, moveChoice.targets, true, false);
            } else {
                this.bc.switchPokemon(this.getPokemonID(), moveChoice.switchPokemon, false);
            }
        }
    }

    public ArrayList<PixelmonWrapper> getTargets(Attack chosenAttack) {
        ArrayList<PixelmonWrapper> targets = new ArrayList<PixelmonWrapper>();
        if (chosenAttack != null) {
            TargetingInfo info = chosenAttack.getAttackBase().targetingInfo;
            ArrayList<PixelmonWrapper> teamPokemon = this.bc.getTeamPokemon(this.participant);
            ArrayList<PixelmonWrapper> opponentPokemon = this.bc.getOpponentPokemon(this.participant);
            boolean[][] targetted = new boolean[][]{new boolean[teamPokemon.size()], new boolean[opponentPokemon.size()]};
            int mypos = teamPokemon.indexOf(this);
            if (info.hitsAll) {
                if (info.hitsOppositeFoe && targetted[1].length > mypos) {
                    targetted[1][mypos] = true;
                }
                if (info.hitsAdjacentFoe) {
                    if (mypos - 1 >= 0) {
                        targetted[1][mypos - 1] = true;
                    }
                    if (mypos + 1 < targetted[1].length) {
                        targetted[1][mypos + 1] = true;
                    }
                }
                if (info.hitsExtendedFoe) {
                    if (mypos - 2 >= 0) {
                        targetted[1][mypos - 2] = true;
                    }
                    if (mypos + 2 < targetted[1].length) {
                        targetted[1][mypos + 2] = true;
                    }
                }
                if (info.hitsSelf) {
                    targetted[0][mypos] = true;
                }
                if (info.hitsAdjacentAlly) {
                    if (mypos - 1 >= 0) {
                        targetted[0][mypos - 1] = true;
                    }
                    if (mypos + 1 < targetted[0].length) {
                        targetted[0][mypos + 1] = true;
                    }
                }
                if (info.hitsExtendedAlly) {
                    if (mypos - 2 >= 0) {
                        targetted[0][mypos - 2] = true;
                    }
                    if (mypos + 2 < targetted[0].length) {
                        targetted[0][mypos + 2] = true;
                    }
                }
                for (int i = 0; i < 2; ++i) {
                    for (int j = 0; j < targetted[i].length; ++j) {
                        if (!targetted[i][j]) continue;
                        if (i == 0) {
                            targets.add(teamPokemon.get(j));
                            continue;
                        }
                        targets.add(opponentPokemon.get(j));
                    }
                }
            } else {
                ArrayList<PixelmonWrapper> tempTargets = new ArrayList<PixelmonWrapper>();
                if (info.hitsSelf) {
                    tempTargets.add(this);
                }
                if (info.hitsAdjacentAlly) {
                    if (mypos > 0) {
                        tempTargets.add(teamPokemon.get(mypos - 1));
                    }
                    if (mypos < teamPokemon.size() - 1) {
                        tempTargets.add(teamPokemon.get(mypos + 1));
                    }
                }
                if (info.hitsExtendedAlly) {
                    if (mypos > 1) {
                        tempTargets.add(teamPokemon.get(mypos - 2));
                    }
                    if (mypos < teamPokemon.size() - 2) {
                        tempTargets.add(teamPokemon.get(mypos + 2));
                    }
                }
                if (info.hitsOppositeFoe && mypos < opponentPokemon.size()) {
                    tempTargets.add(opponentPokemon.get(mypos));
                }
                if (info.hitsAdjacentFoe) {
                    if (mypos > 0) {
                        tempTargets.add(opponentPokemon.get(mypos - 1));
                    }
                    if (mypos < opponentPokemon.size() - 1) {
                        tempTargets.add(opponentPokemon.get(mypos + 1));
                    }
                }
                if (info.hitsExtendedFoe) {
                    if (mypos > 1) {
                        tempTargets.add(opponentPokemon.get(mypos - 2));
                    }
                    if (mypos < opponentPokemon.size() - 2) {
                        tempTargets.add(opponentPokemon.get(mypos + 2));
                    }
                }
                for (int j = 0; j < tempTargets.size(); ++j) {
                    if (tempTargets.get(j) != null) continue;
                    tempTargets.remove(j);
                    --j;
                }
                ArrayList invalid = new ArrayList();
                if (!tempTargets.isEmpty()) {
                    if (info.hitsOppositeFoe || info.hitsExtendedFoe || info.hitsAdjacentFoe) {
                        invalid.addAll(tempTargets.stream().filter(pw -> pw.getParticipant().team == this.getParticipant().team || pw.isFainted()).collect(Collectors.toList()));
                    }
                    tempTargets.removeAll(invalid);
                    if (!tempTargets.isEmpty()) {
                        targets.add((PixelmonWrapper)tempTargets.get(RandomHelper.getRandomNumberBetween(0, tempTargets.size() - 1)));
                    }
                }
            }
        }
        return targets;
    }

    public void useAttack() {
        this.selectedAttack = this.attack;
        if (!this.bc.simulateMode) {
            for (BattleParticipant participant : this.bc.participants) {
                participant.getBattleAI().registerMove(this);
            }
        }
        this.useAttack(true);
    }

    public void useAttack(boolean affectPP) {
        String failMessage = null;
        boolean noEffect = false;
        boolean wasZmove = false;
        if (this.attack.getAttackBase() instanceof ZAttackBase) {
            ZAttackBase zMove = (ZAttackBase)this.attack.getAttackBase();
            UseMoveEvent.UseZMoveEvent event = new UseMoveEvent.UseZMoveEvent(this, zMove, "pixelmon.effect.effectfailed");
            MinecraftForge.EVENT_BUS.post(event);
            if (!event.isCanceled()) {
                if (this.participant.bc != null) {
                    this.participant.bc.sendToAll("pixelmon.zmove.used", this.getPokemonName());
                    if (zMove.zeffect != null) {
                        zMove.zeffect.applyEffect(this);
                        zMove.zeffect.getEffectText(this);
                    }
                    wasZmove = true;
                }
            } else {
                noEffect = true;
                failMessage = event.getFailMessage();
            }
        } else if (this.attack != null) {
            UseMoveEvent event;
            if (this.isDynamaxed) {
                event = new UseMoveEvent.UseMaxMoveEvent(this, this.attack.getAttackBase(), "pixelmon.effect.effectfailed");
                MinecraftForge.EVENT_BUS.post(event);
                if (event.isCanceled()) {
                    noEffect = true;
                    failMessage = event.getFailMessage();
                }
            } else {
                event = new UseMoveEvent(this, this.attack.getAttackBase(), "pixelmon.effect.effectfailed");
                MinecraftForge.EVENT_BUS.post(event);
                if (event.isCanceled()) {
                    noEffect = true;
                    failMessage = event.getFailMessage();
                }
            }
        }
        if (this.attack != null && !this.isFainted()) {
            this.usedMoves.add(this.attack);
            this.getBattleAbility().startMove(this);
            if (this.getSpecies() == EnumSpecies.Meloetta) {
                if (this.attack.isAttack("Relic Song") && this.getForm() == EnumMeloetta.ARIA.getForm() && !(this.getAbility() instanceof SheerForce)) {
                    this.setForm(EnumMeloetta.PIROUETTE.getForm());
                    this.bc.sendToAll("pixelmon.abilities.changeform", this.getNickname());
                } else if (this.attack.isAttack("Relic Song") && this.getForm() == EnumMeloetta.PIROUETTE.getForm() && !(this.getAbility() instanceof SheerForce)) {
                    this.setForm(EnumMeloetta.ARIA.getForm());
                    this.bc.sendToAll("pixelmon.abilities.changeform", this.getNickname());
                }
            }
            if (!noEffect) {
                if (!this.attack.isAttack("Focus Punch")) {
                    if (!this.hasStatus(StatusType.Bide)) {
                        this.bc.sendToAll("pixelmon.battletext.used", this.getNickname(), this.attack.getAttackBase().getLocalizedName());
                    }
                }
            }
            boolean reducePP = false;
            if (!noEffect && this.bc.rules.battleType.numPokemon > 1 && !this.attack.getAttackBase().targetingInfo.hitsAll) {
                if (!this.attack.isAttack("Me First") && !this.attack.getAttackBase().targetingInfo.hitsAdjacentAlly && this.attack.getAttackBase().targetingInfo.hitsAdjacentFoe && this.attack.getAttackBase().targetingInfo.hitsOppositeFoe) {
                    this.targets = this.getTargets(this.attack);
                } else if (this.targets.size() > 1) {
                    PixelmonWrapper firstPokemon = this.targets.get(0);
                    this.targets.clear();
                    this.targets.add(firstPokemon);
                }
            }
            ArrayList deadPokes = new ArrayList();
            ArrayList correctedTargets = new ArrayList();
            if (!this.attack.canHitNoTarget()) {
                this.targets.stream().filter(PixelmonWrapper::isFainted).forEach(target -> {
                    deadPokes.add(target);
                    if (!target.isSameTeam(this) && this.targets.size() == 1) {
                        for (PixelmonWrapper otherTarget : target.getTeamPokemon()) {
                            if (otherTarget.isFainted()) continue;
                            correctedTargets.add(otherTarget);
                        }
                    }
                });
                deadPokes.forEach(this.targets::remove);
                this.targets.addAll(new ArrayList(correctedTargets));
            }
            this.targets = CalcPriority.getTurnOrder(this.targets);
            MoveResults[] arr = new MoveResults[this.targets.size()];
            this.targetIndex = 0;
            while (this.targetIndex < this.targets.size() && !noEffect) {
                PixelmonWrapper target2 = this.targets.get(this.targetIndex);
                MoveResults results = new MoveResults(target2, 0, this.priority, AttackResult.proceed);
                this.attack.saveAttack();
                reducePP = this.attack.use(this, target2, results) || reducePP;
                this.attack.restoreAttack();
                if (this.targetIndex >= arr.length) {
                    arr = Arrays.copyOf(arr, this.targetIndex + 1);
                }
                if (arr[this.targetIndex] == null) {
                    arr[this.targetIndex] = results;
                }
                ++this.targetIndex;
            }
            boolean hadTarget = false;
            boolean ignore = false;
            MoveResults[] var8 = arr;
            int var9 = arr.length;
            for (int var10 = 0; var10 < var9; ++var10) {
                MoveResults result = var8[var10];
                if (result == null) continue;
                if (result.result == AttackResult.ignore) {
                    ignore = true;
                }
                if (result.result != AttackResult.notarget) {
                    hadTarget = true;
                }
                if (result.result == AttackResult.charging || result.result == AttackResult.ignore) continue;
                this.lastAttack = this.attack;
                this.bc.lastAttack = this.attack;
            }
            if (wasZmove && this.participant instanceof PlayerParticipant) {
                Pixelmon.NETWORK.sendTo(new UsedZMove(), ((PlayerParticipant)this.participant).player);
            }
            if (!hadTarget) {
                this.bc.sendToAll(failMessage != null ? failMessage : "pixelmon.effect.effectfailed", new Object[0]);
            }
            if (!ignore) {
                this.bc.battleLog.addEvent(new AttackAction(this.bc.battleTurn, this.bc.getPositionOfPokemon(this), this, this.attack.getAttackBase().attackIndex, arr));
            }
            if (reducePP && affectPP && this.attack.pp > 0) {
                --this.attack.pp;
                this.update(EnumUpdateType.Moveset);
            }
        }
    }

    public MoveResults[] useAttackOnly() {
        MoveResults saveResult = this.attack.moveResult;
        MoveResults[] resultsArray = new MoveResults[this.targets.size()];
        this.targetIndex = 0;
        while (this.targetIndex < this.targets.size()) {
            PixelmonWrapper target = this.targets.get(this.targetIndex);
            resultsArray[this.targetIndex] = new MoveResults(target, 0, this.priority, AttackResult.proceed);
            this.attack.saveAttack();
            this.attack.use(this, target, resultsArray[this.targetIndex]);
            this.attack.restoreAttack();
            ++this.targetIndex;
        }
        this.attack.moveResult = saveResult;
        return resultsArray;
    }

    /*
     * Enabled aggressive block sorting
     */
    public void useItem() {
        NBTTagCompound targetNBT = null;
        PixelmonItem item = null;
        EntityPlayerMP user = null;
        ItemStack usedStack = null;
        PlayerParticipant playerPart = (PlayerParticipant)this.participant;
        user = playerPart.player;
        PlayerStorage storage = playerPart.getStorage();
        if (storage == null) return;
        PixelmonWrapper target = this;
        if (this.willUseItemInStackInfo == -1) {
            targetNBT = storage.getNBT(this.willUseItemPokemon);
            target = this.getParticipant().getPokemonFromParty(this.willUseItemPokemon);
        }
        UseBattleItemEvent event = new UseBattleItemEvent(this, this.willUseItemInStack);
        MinecraftForge.EVENT_BUS.post(event);
        this.willUseItemInStack = null;
        if (event.isCanceled()) return;
        usedStack = event.getUsedStack();
        int additionalInfo = this.willUseItemInStackInfo;
        item = (PixelmonItem)usedStack.getItem();
        boolean isPokeBall = item instanceof ItemPokeball;
        if (isPokeBall && this.bc.getOpponentPokemon(this.participant).size() >= 1) {
            for (PixelmonWrapper opponent : this.bc.getOpponentPokemon(this.participant)) {
                if (!opponent.isAlive()) continue;
                target = opponent;
                break;
            }
            targetNBT = new NBTTagCompound();
            target.pokemon.writeEntityToNBT(targetNBT);
        }
        if (targetNBT != null) {
            this.bc.battleLog.addEvent(new BagItemAction(this.bc.battleTurn, this.bc.getPositionOfPokemon(this), this, targetNBT.getString("Name"), item));
        }
        if (isPokeBall) {
            this.bc.sendToAll("pixelmon.pokeballs.throw", this.participant.getDisplayName(), item.getLocalizedName());
        } else {
            this.bc.sendToAll("pixelmon.items.useitem", this.participant.getDisplayName(), item.getLocalizedName());
        }
        if (!isPokeBall) {
            if (this.hasStatus(StatusType.Embargo)) {
                this.bc.sendToAll("pixelmon.status.embargo", this.getNickname());
                return;
            }
        }
        if (!item.useFromBag(this, target, additionalInfo)) return;
        if (event.shouldConsume()) {
            user.inventory.clearMatchingItems(item, 0, 1, null);
        }
        Pixelmon.NETWORK.sendTo(new UseItem(item), user);
    }

    public void useTempAttack(Attack tempAttack) {
        this.useTempAttack(tempAttack, false);
    }

    public void useTempAttack(Attack tempAttack, boolean usePP) {
        EffectBase effect;
        this.targets = this.getTargets(tempAttack);
        Attack saveAttack = this.attack;
        this.attack = tempAttack;
        this.useAttack(usePP);
        Iterator<EffectBase> var3 = this.attack.getAttackBase().effects.iterator();
        do {
            if (var3.hasNext()) continue;
            this.attack = saveAttack;
            return;
        } while (!((effect = var3.next()) instanceof MultiTurnSpecialAttackBase));
    }

    public void useTempAttack(Attack tempAttack, PixelmonWrapper target) {
        ArrayList<PixelmonWrapper> targets = new ArrayList<PixelmonWrapper>();
        targets.add(target);
        this.useTempAttack(tempAttack, targets);
    }

    public void useTempAttack(Attack tempAttack, ArrayList<PixelmonWrapper> targets) {
        EffectBase effect;
        this.targets = targets;
        Attack prevAttack = this.attack;
        this.attack = tempAttack;
        this.useAttack(false);
        Iterator<EffectBase> var4 = this.attack.getAttackBase().effects.iterator();
        do {
            if (var4.hasNext()) continue;
            this.attack = prevAttack;
            return;
        } while (!((effect = var4.next()) instanceof MultiTurnSpecialAttackBase));
    }

    public void turnTick() {
        boolean resetProtect;
        boolean isActive;
        if (this.bc == null) {
            this.bc = this.participant.bc;
        }
        this.returnToBasePos();
        boolean bl = isActive = this.isAlive() && this.bc != null && !this.bc.battleEnded;
        if (isActive) {
            this.getBattleAbility().applyRepeatedEffect(this);
        }
        Moveset moveset = this.getMoveset();
        for (Attack value : moveset) {
            value.setDisabled(false, this);
        }
        this.checkSkyBattleDisable();
        ItemHeld usableItem = this.getUsableHeldItem();
        if (isActive && this.isAlive()) {
            usableItem.applyRepeatedEffect(this);
            this.getBattleAbility().applyRepeatedEffectItem(this);
        }
        for (int i = 0; i < this.getStatusSize(); ++i) {
            StatusBase s = this.getStatus(i);
            try {
                if (this.bc == null || this.bc.battleEnded || !this.isAlive() && !s.isTeamStatus()) continue;
                int beforeSize = this.getStatusSize();
                s.applyRepeatedEffect(this);
                if (beforeSize <= this.getStatusSize()) continue;
                --i;
                continue;
            }
            catch (Exception var7) {
                this.bc.battleLog.onCrash(var7, "Error calculating applyRepeatedEffect() for " + s.type.toString());
            }
        }
        if (isActive && this.isAlive()) {
            usableItem.applyRepeatedEffectAfterStatus(this);
        }
        this.damageTakenThisTurn = 0;
        boolean bl2 = resetProtect = this.attack == null;
        if (!resetProtect) {
            resetProtect = true;
            for (EffectBase e : this.attack.getAttackBase().effects) {
                if (!(e instanceof ProtectVariation)) continue;
                resetProtect = false;
                break;
            }
        }
        if (resetProtect) {
            this.protectsInARow = 0;
        }
        this.switchedThisTurn = false;
        this.lastDirectDamage = -1;
    }

    public BattleParticipant getParticipant() {
        return this.participant;
    }

    public PixelmonWrapper doSwitch() {
        if (!this.bc.simulateMode) {
            this.isSwitching = false;
            this.canAttack = false;
            if (this.isMega && this.isFainted()) {
                this.resetBattleEvolution();
                this.isMega = false;
                this.participant.evolution = new int[]{-1, -1};
            }
            if (this.isDynamaxed()) {
                if (!this.isFainted()) {
                    float percent = this.getHealthPercent();
                    this.setDynamaxed(false);
                    this.setHealth(this.getPercentMaxHealth(percent));
                    this.updateHPIncrease();
                    if (this.participant.getType() == ParticipantType.Player) {
                        Pixelmon.NETWORK.sendTo(new DynamaxPacket(this.getPokemonID(), true), ((PlayerParticipant)this.participant).player);
                    }
                } else {
                    this.setDynamaxed(false);
                }
            }
            if (this.isUltra && this.isFainted()) {
                this.resetBattleEvolution();
                this.isUltra = false;
                this.participant.ultraBurst = new int[]{-1, -1};
            }
            if (this.getSpecies() == EnumSpecies.Greninja && this.getForm() == 2 && this.isFainted()) {
                this.setFormNoEntity(1);
                this.participant.ashNinja = new int[]{-1, -1};
            }
        }
        ArrayList<StatusBase> statusCopy = new ArrayList<StatusBase>();
        boolean batonPassing = false;
        BattleStats tempStats = new BattleStats(this);
        boolean wasSimulateMode = this.bc.simulateMode;
        this.bc.simulateMode = false;
        tempStats.copyStats(this.getBattleStats());
        this.bc.simulateMode = wasSimulateMode;
        if (this.nextSwitchIsMove && this.attack != null) {
            if (this.attack.isAttack("Baton Pass")) {
                batonPassing = true;
            }
        }
        if (!this.bc.simulateMode) {
            for (int i = 0; i < this.getStatusSize(); ++i) {
                try {
                    StatusBase status = this.getStatus(i);
                    if (!(status.isTeamStatus() || batonPassing && BatonPass.isBatonPassable(status))) {
                        if (StatusType.isPrimaryStatus(status.type)) continue;
                        this.removeStatus(status, false);
                        --i;
                        continue;
                    }
                    statusCopy.add(status);
                    continue;
                }
                catch (Exception var9) {
                    this.bc.battleLog.onCrash(var9, "Error in doSwitch().");
                }
            }
        }
        int[] oldID = this.getPokemonID();
        if (!this.bc.simulateMode) {
            this.update(EnumUpdateType.Status);
        }
        if (this.newPokemonID == null) {
            return null;
        }
        PixelmonWrapper newPokemon = this.participant.switchPokemon(this, this.newPokemonID);
        this.newPokemonID = null;
        if (newPokemon.pokemon != null) {
            newPokemon.pokemon.setHealth(newPokemon.getHealth());
            newPokemon.pokemon.isFainted = false;
            newPokemon.pokemon.battleController = this.bc;
        }
        newPokemon.battlePosition = this.battlePosition;
        newPokemon.switchedThisTurn = true;
        if (PixelmonMethods.isIDSame(newPokemon.getPokemonID(), this.participant.evolution) && newPokemon.getSpecies() != EnumSpecies.Groudon && newPokemon.getSpecies() != EnumSpecies.Kyogre) {
            int temp = newPokemon.getSpecies() == EnumSpecies.Rayquaza ? 1 : ((ItemMegaStone)newPokemon.getHeldItem()).form;
            newPokemon.isMega = true;
            newPokemon.setForm(temp);
            if (newPokemon.pokemon != null) {
                newPokemon.pokemon.isMega = true;
                newPokemon.tempAbility = null;
                newPokemon.pokemon.setForm(temp);
            }
        }
        if (!this.bc.simulateMode) {
            this.bc.sendSwitchPacket(oldID, newPokemon);
        }
        if (this.bc.participants.size() == 2) {
            boolean allFainted = true;
            for (BattleParticipant battleParticipant : this.bc.participants) {
                if (!(battleParticipant instanceof TrainerParticipant)) continue;
                BattleParticipant opponent = battleParticipant.getOpponents().get(0);
                if (opponent.getStorage().countAblePokemon() > 0) {
                    allFainted = false;
                }
                if (!allFainted) continue;
                newPokemon.pokemon.setDead();
            }
        }
        if (batonPassing) {
            this.bc.simulateMode = false;
            newPokemon.getBattleStats().copyStats(tempStats);
            this.bc.simulateMode = wasSimulateMode;
        }
        if (!statusCopy.isEmpty()) {
            for (StatusBase e : statusCopy) {
                newPokemon.addStatus(e, newPokemon);
            }
            for (StatusBase e : statusCopy) {
                e.applyEffectOnSwitch(newPokemon);
            }
        }
        for (GlobalStatusBase status : this.bc.globalStatusController.getGlobalStatuses()) {
            status.applyEffectOnSwitch(newPokemon);
        }
        if (newPokemon.switchedThisTurn && newPokemon.bc.getPlayer(this.getPlayerOwner()) != null) {
            newPokemon.afterSwitch();
        }
        if (!this.bc.simulateMode) {
            this.bc.getOpponentPokemon(this.participant).stream().filter(pw -> pw.targets.contains(this)).forEach(pw -> {
                pw.targets.remove(this);
                pw.targets.add(newPokemon);
            });
            this.bc.getTeamPokemon(this.participant).stream().filter(pw -> pw != this).filter(pw -> pw.targets.contains(this)).forEach(pw -> {
                pw.targets.remove(this);
                pw.targets.add(newPokemon);
            });
            newPokemon.addAttackers();
            this.bc.participants.stream().filter(p2 -> p2.team != this.participant.team).forEach(BattleParticipant::updateOtherPokemon);
        }
        return newPokemon;
    }

    public void beforeSwitch() {
        if (!this.bc.simulateMode) {
            this.getBattleStats().clearBattleStats();
            if (this.isAlive()) {
                this.getBattleAbility().applySwitchOutEffect(this);
            }
            for (int i = 0; i < this.getStatusSize(); ++i) {
                StatusBase status = this.getStatus(i);
                if (!this.isAlive() && !status.isTeamStatus()) continue;
                status.applySwitchOutEffect(this);
            }
            this.getUsableHeldItem().applySwitchOutEffect(this);
            this.resetOnSwitch();
        }
    }

    public void afterSwitch() {
        if (!this.bc.simulateMode && this.isAlive()) {
            BattleControllerBase.checkSwitchInAbility(this);
            this.getUsableHeldItem().applySwitchInEffect(this);
            if (this.participant.getType() == ParticipantType.Player && this.baseStats.pokemon == EnumSpecies.Burmy) {
                this.changeBurmy = true;
            }
            this.checkSkyBattleDisable();
        }
    }

    private void checkSkyBattleDisable() {
        for (Attack move : this.getMoveset()) {
            if (move == null || move.checkSkyBattle(this.bc)) continue;
            move.setDisabled(true, this);
        }
    }

    public void takeTurn() {
        if (this.isSwitching) {
            this.bc.battleLog.addEvent(new SwitchAction(this.bc.battleTurn, this.bc.getPositionOfPokemon(this), this, this.newPokemonID));
            this.doSwitch();
        } else if (this.willUseItemInStack != null) {
            this.useItem();
        } else {
            StatusBase status;
            int i;
            this.canAttack = this.getBattleAbility().canAttackThisTurn(this, this.attack);
            if (this.canAttack) {
                for (i = 0; i < this.getStatusSize(); ++i) {
                    status = this.getStatus(i);
                    try {
                        if (!(status instanceof StatusPersist) || status.canAttackThisTurn(this, this.attack)) continue;
                        this.canAttack = false;
                        this.bc.battleLog.addEvent(new AttackAction(this.bc.battleTurn, this.bc.getPositionOfPokemon(this), this, 0, new MoveResults[]{new MoveResults(null, 0, AttackResult.unable)}));
                        break;
                    }
                    catch (Exception var5) {
                        this.bc.battleLog.onCrash(var5, "Error calculating canAttackThisTurn for " + status.type.toString());
                    }
                }
            }
            if (this.canAttack) {
                for (i = 0; i < this.getStatusSize(); ++i) {
                    status = this.getStatus(i);
                    try {
                        if (status instanceof StatusPersist || status.canAttackThisTurn(this, this.attack)) continue;
                        this.canAttack = false;
                        this.bc.battleLog.addEvent(new AttackAction(this.bc.battleTurn, this.bc.getPositionOfPokemon(this), this, 0, new MoveResults[]{new MoveResults(null, 0, AttackResult.unable)}));
                        break;
                    }
                    catch (Exception var4) {
                        this.bc.battleLog.onCrash(var4, "Error calculating canAttackThisTurn for " + status.type.toString());
                    }
                }
            }
            if (this.canAttack) {
                this.participant.bc.clearHurtTimer();
                this.getUsableHeldItem().onAttackUsed(this, this.attack);
                this.getBattleAbility().onAttackUsed(this, this.attack);
                for (i = 0; i < this.status.size(); ++i) {
                    status = this.status.get(i);
                    int initialSize = this.status.size();
                    status.onAttackUsed(this, this.attack);
                    if (initialSize <= this.status.size()) continue;
                    --i;
                }
                this.useAttack();
            } else if (this.attack != null) {
                if (this.hasStatus(StatusType.MultiTurn)) {
                    for (EffectBase effect : this.attack.getAttackBase().effects) {
                        if (!(effect instanceof MultiTurnSpecialAttackBase)) continue;
                        ((MultiTurnSpecialAttackBase)effect).removeEffect(this, this);
                    }
                }
            }
            if (this.isDynamaxed()) {
                --this.dynamaxTurnsLeft;
            }
        }
    }

    public boolean primalEvolve() {
        boolean isPrimal;
        boolean bl = isPrimal = this.baseStats.pokemon == EnumSpecies.Groudon || this.baseStats.pokemon == EnumSpecies.Kyogre;
        if (this.baseStats.pokemon == EnumSpecies.Groudon) {
            BaseStats stats;
            this.pokemon.isPrimal = true;
            this.isPrimal = true;
            this.evolution = new EvolutionQuery(this.pokemon, EnumGroudon.Primal.getForm());
            this.setForm(EnumGroudon.Primal.getForm());
            this.pokemon.baseStats = stats = Entity3HasStats.getBaseStats(this.pokemon.getSpecies().name(), (int)EnumGroudon.Primal.getForm()).get();
            this.baseStats = stats;
            return true;
        }
        if (this.baseStats.pokemon == EnumSpecies.Kyogre) {
            BaseStats stats;
            this.pokemon.isPrimal = true;
            this.isPrimal = true;
            this.evolution = new EvolutionQuery(this.pokemon, EnumKyogre.Primal.getForm());
            this.setForm(EnumKyogre.Primal.getForm());
            this.pokemon.baseStats = stats = Entity3HasStats.getBaseStats(this.pokemon.getSpecies().name(), (int)EnumKyogre.Primal.getForm()).get();
            this.baseStats = stats;
            return true;
        }
        return true;
    }

    public boolean dynamax() {
        if (this.bc.simulateMode) {
            return false;
        }
        DynamaxEvent event = new DynamaxEvent(this.getPokemonID(), this.participant);
        MinecraftForge.EVENT_BUS.post(event);
        if (!event.isCanceled()) {
            float percent = this.getHealthPercent();
            this.setDynamaxed(true);
            this.setHealth(this.getPercentMaxHealth(percent));
            this.updateHPIncrease();
            this.evolution = new DynamaxQuery(this.pokemon);
            if (this.participant.getType() == ParticipantType.Player) {
                PlayerParticipant player = (PlayerParticipant)this.participant;
                Pixelmon.NETWORK.sendTo(new DynamaxPacket(this.getPokemonID()), player.player);
            }
            String nickname = this.getNickname();
            this.bc.sendToAll("pixelmon.battletext." + (this.link.hasGmaxFactor() && this.getSpecies().hasGmaxForm() ? "gigantamaxing" : "dynamaxing"), nickname);
            return true;
        }
        return false;
    }

    public boolean megaEvolve() {
        boolean isPrimal;
        if (this.bc.simulateMode) {
            return false;
        }
        this.willEvolve = false;
        boolean bl = isPrimal = this.baseStats.pokemon == EnumSpecies.Groudon || this.baseStats.pokemon == EnumSpecies.Kyogre;
        if (isPrimal && this.getForm() == 1) {
            return false;
        }
        if (this.canMegaEvolve() && (this.participant.evolution == null || isPrimal)) {
            BaseStats stats;
            if (this.baseStats.pokemon == EnumSpecies.Rayquaza) {
                BaseStats stats2;
                this.pokemon.isMega = true;
                this.isMega = true;
                this.participant.evolution = this.getPokemonID();
                String nickname = this.getNickname();
                this.evolution = new EvolutionQuery(this.pokemon, EnumForms.Mega.getForm());
                this.tempAbility = null;
                this.setForm(EnumForms.Mega.getForm());
                if (this.participant.getType() == ParticipantType.Player) {
                    PlayerParticipant player = (PlayerParticipant)this.participant;
                    Pixelmon.NETWORK.sendTo(new MegaEvolve(this.getPokemonID()), player.player);
                }
                this.bc.sendToAll("pixelmon.battletext.megareact", nickname, "Dragon Ascent", this.participant.getDisplayName());
                this.pokemon.baseStats = stats2 = Entity3HasStats.getBaseStats(this.pokemon.getSpecies().name(), (int)EnumForms.Mega.getForm()).get();
                this.baseStats = stats2;
                return true;
            }
            if (this.baseStats.pokemon == EnumSpecies.Groudon) {
                BaseStats stats3;
                this.pokemon.isMega = true;
                this.isMega = true;
                this.evolution = new EvolutionQuery(this.pokemon, EnumGroudon.Primal.getForm());
                this.setForm(EnumGroudon.Primal.getForm());
                this.bc.sendToAll(new TextComponentTranslation("gui.primal.transform", this.getNickname()));
                this.pokemon.baseStats = stats3 = Entity3HasStats.getBaseStats(this.pokemon.getSpecies().name(), (int)EnumGroudon.Primal.getForm()).get();
                this.baseStats = stats3;
                return true;
            }
            if (this.baseStats.pokemon == EnumSpecies.Kyogre) {
                BaseStats stats4;
                this.pokemon.isMega = true;
                this.isMega = true;
                this.evolution = new EvolutionQuery(this.pokemon, EnumKyogre.Primal.getForm());
                this.setForm(EnumKyogre.Primal.getForm());
                this.bc.sendToAll(new TextComponentTranslation("gui.primal.transform", this.getNickname()));
                this.pokemon.baseStats = stats4 = Entity3HasStats.getBaseStats(this.pokemon.getSpecies().name(), (int)EnumKyogre.Primal.getForm()).get();
                this.baseStats = stats4;
                return true;
            }
            ItemMegaStone megaStone = (ItemMegaStone)this.getHeldItem();
            if (MinecraftForge.EVENT_BUS.post(new MegaEvolutionEvent(this, megaStone))) {
                return false;
            }
            this.pokemon.isMega = true;
            this.isMega = true;
            this.participant.evolution = this.getPokemonID();
            String nickname = this.getNickname();
            this.bc.sendToAll("pixelmon.battletext.megareact", nickname, this.baseStats.pokemon != EnumSpecies.Rayquaza && this.getHeldItem() instanceof ItemMegaStone ? this.getHeldItem().getLocalizedName() : "Dragon Ascent", this.participant.getDisplayName());
            this.evolution = new EvolutionQuery(this.pokemon, megaStone.form);
            this.tempAbility = null;
            this.setForm(megaStone.form);
            if (this.participant.getType() == ParticipantType.Player) {
                PlayerParticipant player = (PlayerParticipant)this.participant;
                Pixelmon.NETWORK.sendTo(new MegaEvolve(this.getPokemonID()), player.player);
            }
            this.pokemon.baseStats = stats = Entity3HasStats.getBaseStats(this.pokemon.getSpecies().name(), megaStone.form).get();
            this.baseStats = stats;
            return true;
        }
        if (PixelmonWrapper.canUltraBurst(this.link.getHeldItem(), this.getSpecies(), (short)this.getForm()) && this.participant.ultraBurst == null && this.participant.canUltraBurst()) {
            BaseStats stats;
            byte ultraForm = ((EnumNecrozma)this.pokemon.getFormEnum()).getUltra().getForm();
            this.pokemon.isUltra = true;
            this.isUltra = true;
            this.evolution = new EvolutionQuery(this.pokemon, ultraForm);
            this.participant.ultraBurst = this.getPokemonID();
            this.setForm(ultraForm);
            if (this.participant.getType() == ParticipantType.Player) {
                PlayerParticipant player = (PlayerParticipant)this.participant;
                Pixelmon.NETWORK.sendTo(new UltraBurst(this.getPokemonID()), player.player);
            }
            this.bc.sendToAll(new TextComponentTranslation("pixelmon.battletext.brightlight", this.getNickname()));
            this.pokemon.baseStats = stats = Entity3HasStats.getBaseStats(this.pokemon.getSpecies().name(), (int)ultraForm).get();
            this.baseStats = stats;
            this.bc.modifyStats();
            return true;
        }
        return false;
    }

    public void setAttack(int buttonId, ArrayList<PixelmonWrapper> targets, boolean megaEvolving, boolean dynamaxing) {
        if (buttonId >= 4) {
            int tempButtonId = buttonId - 4;
            Attack tempMove = this.getMoveset().get(tempButtonId);
            if (dynamaxing || this.isDynamaxed()) {
                DynamaxEvent event = new DynamaxEvent(this.getPokemonID(), this.participant);
                MinecraftForge.EVENT_BUS.post(event);
                if (!event.isCanceled()) {
                    Attack maxMove = MaxAttackHelper.getMaxMove(new PixelmonData(this.pokemon), tempMove);
                    this.setAttack(maxMove, tempMove, targets, megaEvolving, dynamaxing);
                    --tempMove.pp;
                    this.update(EnumUpdateType.Moveset);
                    return;
                }
            } else if (this.heldItem instanceof ZCrystal) {
                Attack zAttack;
                ZCrystal crystal = (ZCrystal)this.heldItem;
                if (crystal.canEffectMove(new PokemonSpec(this.getPokemonName()).setForm(this.form), tempMove) && (zAttack = crystal.getUsableAttack(tempMove)) != null) {
                    this.setAttack(zAttack, targets, megaEvolving, dynamaxing);
                    --tempMove.pp;
                    this.update(EnumUpdateType.Moveset);
                    return;
                }
            }
            buttonId -= 4;
        }
        Attack attack = this.getMoveset().get(buttonId);
        this.setAttack(attack, targets, megaEvolving, dynamaxing);
    }

    public void setAttack(Attack attack, Attack baseMove, ArrayList<PixelmonWrapper> targets, boolean megaEvolving, boolean dynamaxing) {
        this.setAttack(attack, targets, megaEvolving, dynamaxing);
        this.baseLastAttack = baseMove;
    }

    public void setAttack(Attack attack, ArrayList<PixelmonWrapper> targets, boolean megaEvolving, boolean dynamaxing) {
        this.attack = attack;
        this.targets = targets;
        if (!this.bc.simulateMode) {
            this.wait = false;
            this.willEvolve = megaEvolving;
            this.willDynamax = dynamaxing;
        }
    }

    public boolean isFainted() {
        return this.getHealth() <= 0;
    }

    public boolean isAlive() {
        return !this.isFainted();
    }

    public void setStruggle(ArrayList<PixelmonWrapper> targets) {
        this.setAttack(new Attack("Struggle"), targets, false, false);
    }

    public void returnToBasePos() {
        if (this.basePos != null && this.pokemon != null) {
            this.pokemon.setLocationAndAngles(this.basePos[0], this.basePos[1], this.basePos[2], this.participant.team == 0 ? 270.0f : 90.0f, 0.0f);
            this.pokemon.prevRotationYaw = this.pokemon.rotationYaw;
        }
    }

    public void setBasePosition(double[] ds) {
        if (this.pokemon != null) {
            this.basePos = ds;
            this.pokemon.setLocationAndAngles(ds[0], ds[1], ds[2], 0.0f, 0.0f);
        }
    }

    public void setTempType(EnumType newType) {
        this.setTempType(newType.makeTypeList());
    }

    public void setTempType(List<EnumType> newType) {
        if (!this.bc.simulateMode) {
            if (this.initialType == null) {
                this.initialType = this.type;
            }
            this.type = newType;
        }
    }

    public void setTempAbility(AbilityBase newAbility) {
        this.setTempAbility(newAbility, false);
    }

    public void setTempAbility(AbilityBase newAbility, boolean formChange) {
        if (!this.bc.simulateMode) {
            this.getBattleAbility().onAbilityLost(this);
            if (newAbility.needNewInstance()) {
                newAbility = newAbility.getNewInstance();
            }
            this.tempAbility = newAbility;
            AbilityBase pokemonAbility = this.getBattleAbility();
            if (formChange || !(pokemonAbility instanceof Trace) && !(pokemonAbility instanceof Imposter)) {
                BattleControllerBase.checkSwitchInAbility(this);
            }
        }
    }

    public void resetOnSwitch() {
        if (this.bc == null || !this.bc.simulateMode) {
            int i;
            this.lastAttack = null;
            this.protectsInARow = 0;
            if (this.initialType != null) {
                this.type = this.initialType;
                this.initialType = null;
            }
            this.tempAbility = null;
            Moveset moveset = this.getMoveset();
            for (i = 0; i < moveset.size(); ++i) {
                moveset.get(i).setDisabled(false, this, true);
            }
            this.usedMoves.clear();
            this.escapeAttempts = 0;
            this.damageTakenThisTurn = 0;
            this.nextSwitchIsMove = false;
            this.wait = false;
            this.isSwitching = false;
            this.choiceLocked = null;
            this.inMultipleHit = false;
            this.inParentalBond = false;
            for (i = 0; i < this.status.size(); ++i) {
                if (this.status.get((int)i).type.isPrimaryStatus()) continue;
                this.status.remove(i--);
            }
            this.battleStats.clearBattleStats();
        }
    }

    public float getWeight(boolean ignoreAbility) {
        float weight = this.baseStats.weight;
        if (!ignoreAbility) {
            weight = this.getBattleAbility().modifyWeight(weight);
        }
        weight = this.getUsableHeldItem().modifyWeight(weight);
        for (StatusBase status : this.getStatuses()) {
            status.modifyWeight(weight);
        }
        return weight;
    }

    public void consumeItem() {
        ItemHeld item = this.getHeldItem();
        this.setConsumedItem(item);
        this.bc.getActiveUnfaintedPokemon().stream().forEach(pw -> pw.getBattleAbility().onItemConsumed((PixelmonWrapper)pw, this, item));
        this.removeHeldItem();
    }

    public void setConsumedItem(ItemHeld heldItem) {
        if (!this.bc.simulateMode) {
            this.consumedHeldItem = heldItem == null ? NoItem.noItem : heldItem;
        }
    }

    public ItemHeld getConsumedItem() {
        return this.consumedHeldItem;
    }

    public void setNewHeldItem(ItemHeld heldItem) {
        this.setHeldItem(heldItem);
        this.getUsableHeldItem().applySwitchInEffect(this);
    }

    public boolean isGrounded() {
        return this.hasStatus(StatusType.SmackedDown) || this.bc.globalStatusController.hasStatus(StatusType.Gravity) || this.getUsableHeldItem().getHeldItemType() == EnumHeldItems.ironBall;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public boolean isAirborne() {
        if (this.isGrounded()) return false;
        if (this.hasType(EnumType.Flying)) return true;
        if (this.getBattleAbility() instanceof Levitate) return true;
        if (this.hasStatus(StatusType.MagnetRise, StatusType.Telekinesis)) return true;
        if (this.getUsableHeldItem().getHeldItemType() != EnumHeldItems.airBalloon) return false;
        return true;
    }

    public boolean addTeamStatus(StatusBase status, PixelmonWrapper cause) {
        boolean succeeded = false;
        for (PixelmonWrapper pw : this.getTeamPokemon()) {
            succeeded = pw.addStatus(status.copy(), cause) || succeeded;
        }
        return succeeded;
    }

    public boolean removeTeamStatus(StatusBase status) {
        return this.removeTeamStatus(status.type);
    }

    public boolean removeTeamStatus(StatusType ... statuses) {
        boolean hadStatus = false;
        int var4 = statuses.length;
        for (StatusType status : statuses) {
            for (PixelmonWrapper pw : this.getTeamPokemon()) {
                hadStatus = pw.removeStatus(status) || hadStatus;
            }
        }
        return hadStatus;
    }

    public ArrayList<PixelmonWrapper> getTeamPokemon() {
        return this.bc.getTeamPokemon(this);
    }

    public ArrayList<PixelmonWrapper> getTeamPokemonExcludeSelf() {
        return this.bc.getTeamPokemonExcludeSelf(this);
    }

    public ArrayList<PixelmonWrapper> getOpponentPokemon() {
        return this.bc.getOpponentPokemon(this);
    }

    public int getControlledIndex() {
        return this.participant.controlledPokemon.indexOf(this);
    }

    public BattleAIBase getBattleAI() {
        return this.getParticipant().getBattleAI();
    }

    public void setUpSwitchMove() {
        this.wait = true;
        this.nextSwitchIsMove = true;
        this.canAttack = false;
        this.bc.removeFromTurnList(this);
    }

    public void forceRandomSwitch(int[] switchPokemon) {
        this.setUpSwitchMove();
        this.bc.switchPokemon(this.getPokemonID(), switchPokemon, true);
    }

    public boolean isImmuneToPowder() {
        if (!this.hasType(EnumType.Grass) && !(this.getBattleAbility() instanceof Overcoat)) {
            ItemHeld item = this.getHeldItem();
            return item.getHeldItemType() == EnumHeldItems.safetyGoggles;
        }
        return true;
    }

    public String toString() {
        return this.getPokemonName();
    }

    public boolean equals(Object other) {
        if (super.equals(other)) {
            return true;
        }
        if (other instanceof PixelmonWrapper) {
            PixelmonWrapper otherPW = (PixelmonWrapper)other;
            return PixelmonMethods.isIDSame(this, otherPW);
        }
        return false;
    }

    public boolean hasMoved() {
        return this.bc.turnList.indexOf(this) <= this.bc.turn;
    }

    public boolean isFirstTurn() {
        return this.bc.battleTurn == 0 || this.bc.battleLog.getActionForPokemon(this.bc.battleTurn - 1, this) == null;
    }

    public boolean isSameTeam(PixelmonWrapper other) {
        return this.getParticipant().team == other.getParticipant().team;
    }

    public Moveset getMoveset() {
        for (int i = 0; i < this.status.size(); ++i) {
            if (!(this.getStatus(i) instanceof TempMoveset)) continue;
            return ((TempMoveset)this.getStatus(i)).getMoveset();
        }
        return this.moveset;
    }

    public boolean removeStatus(StatusType s) {
        for (StatusBase base : this.status) {
            if (base.type != s) continue;
            this.removeStatus(base);
            return true;
        }
        return false;
    }

    public boolean removeStatuses(StatusType ... statuses) {
        return this.removeStatuses(true, statuses);
    }

    public boolean removeStatuses(boolean showMessage, StatusType ... statuses) {
        boolean wasRemoved = false;
        block0: for (int i = 0; i < this.status.size(); ++i) {
            StatusBase base = this.status.get(i);
            int var7 = statuses.length;
            for (StatusType type : statuses) {
                if (base.type != type) continue;
                int beforeSize = this.status.size();
                this.removeStatus(base, showMessage);
                if (this.status.size() < beforeSize) {
                    --i;
                }
                wasRemoved = true;
                continue block0;
            }
        }
        return wasRemoved;
    }

    public void removeStatus(int i) {
        this.removeStatus(this.status.get(i));
    }

    public StatusBase getStatus(StatusType type) {
        StatusBase base;
        if (this.getAbility() instanceof Comatose && type == StatusType.Sleep) {
            for (StatusBase statusBase : this.status) {
                if (statusBase.type != StatusType.Sleep) continue;
                return statusBase;
            }
            return new Sleep();
        }
        Iterator<StatusBase> var2 = this.status.iterator();
        do {
            if (!var2.hasNext()) {
                return null;
            }
            base = var2.next();
        } while (base.type != type);
        return base;
    }

    public boolean hasStatus(StatusType ... statuses) {
        List<StatusType> statusTypes = Arrays.asList(statuses);
        if (statusTypes.contains((Object)StatusType.Sleep) && this.getAbility() instanceof Comatose) {
            return true;
        }
        for (StatusType current : statusTypes) {
            for (StatusBase base : this.status) {
                if (base.type != current) continue;
                return true;
            }
        }
        return false;
    }

    public boolean hasPrimaryStatus() {
        return this.hasStatus(StatusType.Poison, StatusType.Burn, StatusType.PoisonBadly, StatusType.Freeze, StatusType.Sleep, StatusType.Paralysis);
    }

    public int countStatuses(StatusType ... statuses) {
        int count = 0;
        for (StatusBase base : this.status) {
            for (StatusType current : statuses) {
                if (base.type != current) continue;
                ++count;
            }
        }
        return count;
    }

    public int getStatusSize() {
        return this.status.size();
    }

    public List<StatusBase> getStatuses() {
        return this.status;
    }

    public StatusBase getStatus(int i) {
        return this.status.get(i);
    }

    public int getStatusIndex(StatusType findStatus) {
        for (int i = 0; i < this.status.size(); ++i) {
            StatusBase base = this.status.get(i);
            if (base.type != findStatus) continue;
            return i;
        }
        return -1;
    }

    public StatusPersist getPrimaryStatus() {
        StatusBase s;
        if (this.getBattleAbility() instanceof Comatose) {
            return new Sleep();
        }
        Iterator<StatusBase> var1 = this.status.iterator();
        do {
            if (var1.hasNext()) continue;
            return NoStatus.noStatus;
        } while (!((s = var1.next()) instanceof StatusPersist));
        return (StatusPersist)s;
    }

    public void setStatus(int i, StatusBase newStatus) {
        this.status.set(i, newStatus);
    }

    public void clearStatus() {
        if (!this.bc.simulateMode) {
            if (this.hasPrimaryStatus() && this.pokemon != null) {
                this.pokemon.sendStatusPacket(-1);
            }
            this.status.clear();
        }
    }

    public boolean addStatus(StatusBase e, PixelmonWrapper opponent) {
        return this.addStatus(e, opponent, null);
    }

    public boolean addStatus(StatusBase e, PixelmonWrapper opponent, TextComponentTranslation message) {
        if (this.cannotHaveStatus(e, opponent)) {
            return false;
        }
        e.applyBeforeEffect(this, opponent);
        if (!this.bc.simulateMode) {
            this.status.add(e);
        }
        if (this.bc.sendMessages) {
            if (message != null) {
                this.bc.sendToAll(message);
            }
            if (!this.bc.simulateMode && e.type.isPrimaryStatus()) {
                this.pokemon.sendStatusPacket(e.type.ordinal());
            }
            this.getBattleAbility().onStatusAdded(e, this, opponent);
            this.getUsableHeldItem().onStatusAdded(this, opponent, e);
            if (!this.bc.simulateMode && this.pokemon != null) {
                if (this.getPlayerOwner() != null) {
                    this.update(EnumUpdateType.Status);
                }
                this.bc.updatePokemonHealth(this.pokemon);
            }
        }
        return true;
    }

    public void addStatusNoCheck(StatusBase e, PixelmonWrapper opponent, TextComponentTranslation message) {
        e.applyBeforeEffect(this, opponent);
        if (!this.bc.simulateMode) {
            this.status.add(e);
        }
        if (this.bc.sendMessages) {
            if (message != null) {
                this.bc.sendToAll(message);
            }
            if (!this.bc.simulateMode && e.type.isPrimaryStatus()) {
                this.pokemon.sendStatusPacket(e.type.ordinal());
            }
            this.getBattleAbility().onStatusAdded(e, this, opponent);
            this.getUsableHeldItem().onStatusAdded(this, opponent, e);
            if (!this.bc.simulateMode && this.pokemon != null) {
                if (this.getPlayerOwner() != null) {
                    this.update(EnumUpdateType.Status);
                }
                this.bc.updatePokemonHealth(this.pokemon);
            }
        }
    }

    public void removeStatus(StatusBase e) {
        this.removeStatus(e, true);
    }

    public void removeStatus(StatusBase e, boolean showMessage) {
        if (!this.bc.simulateMode) {
            if (this.status.remove(e) && showMessage) {
                String message;
                String string = message = this.eatingBerry ? e.getCureMessageItem() : e.getCureMessage();
                if (!message.isEmpty()) {
                    String nickname = this.getNickname();
                    if (this.eatingBerry) {
                        this.bc.sendToAll(message, nickname, this.getHeldItem().getLocalizedName());
                    } else {
                        this.bc.sendToAll(message, nickname);
                    }
                }
            }
            if (this.pokemon != null) {
                if (StatusType.isPrimaryStatus(e.type)) {
                    this.pokemon.sendStatusPacket(-1);
                }
                this.update(EnumUpdateType.Status);
                this.bc.updatePokemonHealth(this.pokemon);
            } else {
                if (StatusType.isPrimaryStatus(e.type)) {
                    this.sendStatusPacket(-1, this.getPokemonID());
                }
                this.update(EnumUpdateType.Status);
            }
        }
    }

    public void sendStatusPacket(int statusID, int[] pokemonID) {
        if (this.bc != null && !this.bc.simulateMode) {
            this.bc.participants.stream().filter(p -> p.getType() == ParticipantType.Player).forEach(p -> Pixelmon.NETWORK.sendTo(new StatusPacket(pokemonID, statusID), ((PlayerParticipant)p).player));
            this.bc.spectators.forEach(spectator -> spectator.sendMessage(new StatusPacket(pokemonID, statusID)));
        }
    }

    public StatusBase removePrimaryStatus() {
        return this.removePrimaryStatus(true);
    }

    public StatusBase removePrimaryStatus(boolean showMessage) {
        try {
            for (StatusBase base : this.status) {
                if (!StatusType.isPrimaryStatus(base.type)) continue;
                this.removeStatus(base, showMessage);
                return base;
            }
            return null;
        }
        catch (Exception var4) {
            var4.printStackTrace();
            return null;
        }
    }

    public boolean cannotHaveStatus(StatusBase t, PixelmonWrapper opponent) {
        return this.cannotHaveStatus(t, opponent, false);
    }

    public boolean cannotHaveStatus(StatusBase t, PixelmonWrapper opponent, boolean ignorePrimaryOverlap) {
        StatusType type;
        block10: {
            block9: {
                type = t.type;
                if (t.isImmune(this) || this.bc == null || this.isFainted() && !t.isTeamStatus()) break block9;
                if (!this.hasStatus(type)) break block10;
            }
            return true;
        }
        for (StatusBase statusBase : this.status) {
            try {
                if (!statusBase.stopsStatusChange(type, this, opponent)) continue;
                return true;
            }
            catch (Exception exception) {
                System.out.println("Problem with cannotHaveStatus for StatusType " + type.name());
                exception.printStackTrace();
            }
        }
        for (GlobalStatusBase globalStatusBase : this.bc.globalStatusController.getGlobalStatuses()) {
            try {
                if (!globalStatusBase.stopsStatusChange(type, this, opponent)) continue;
                return true;
            }
            catch (Exception exception) {
                System.out.println("Problem with cannotHaveStatus for StatusType " + type.name());
                exception.printStackTrace();
            }
        }
        for (PixelmonWrapper pixelmonWrapper : this.bc.getTeamPokemon(this)) {
            if (pixelmonWrapper.getBattleAbility().allowsStatusTeammate(type, pixelmonWrapper, this, opponent)) continue;
            return true;
        }
        return !this.getBattleAbility().allowsStatus(type, this, opponent);
    }

    public List<EntryHazard> getEntryHazards() {
        ArrayList<EntryHazard> hazards = new ArrayList<EntryHazard>();
        for (StatusBase s : this.status) {
            if (!(s instanceof EntryHazard)) continue;
            hazards.add((EntryHazard)s);
        }
        return hazards;
    }

    public void updateBattleDamage(float damage) {
        this.updateBattleDamage((int)damage);
    }

    public void updateBattleDamage(int damage) {
        if (this.pokemon != null) {
            NPCTrainer trainer;
            this.bc.updatePokemonHealth(this.pokemon);
            this.bc.sendDamagePacket(this, damage);
            this.update(EnumUpdateType.HP);
            if (this.isFainted()) {
                int turnIndex = this.bc.turnList.indexOf(this);
                if (turnIndex > this.bc.turn) {
                    this.bc.turnList.remove(turnIndex);
                }
                if (this.removePrimaryStatus(false) != null) {
                    this.pokemon.sendStatusPacket(-1);
                    this.update(EnumUpdateType.Status);
                }
            }
            if ((trainer = this.pokemon.getTrainer()) != null) {
                trainer.getPokemonStorage().updateNBT(this.pokemon);
            }
        }
    }

    public List<EnumType> getEffectiveTypes(PixelmonWrapper user, PixelmonWrapper target) {
        List<EnumType> effectiveTypes = target.type;
        effectiveTypes = user.getBattleAbility().getEffectiveTypes(user, target);
        if (effectiveTypes.equals(target.type)) {
            for (int i = 0; i < target.getStatusSize(); ++i) {
                effectiveTypes = target.getStatus(i).getEffectiveTypes(user, target);
                if (effectiveTypes.equals(target.type)) continue;
                return effectiveTypes;
            }
            effectiveTypes = target.getUsableHeldItem().getEffectiveTypes(user, target);
            for (GlobalStatusBase status : target.bc.globalStatusController.getGlobalStatuses()) {
                effectiveTypes = status.getEffectiveTypes(user, target);
            }
        }
        return effectiveTypes;
    }

    public float doBattleDamage(PixelmonWrapper source, float damage, DamageTypeEnum damageType) {
        float damageResult;
        AbilityBase thisAbility = this.getBattleAbility();
        ItemHeld thisHeldItem = this.getUsableHeldItem();
        if (source == null) {
            source = this;
        }
        if (this.isFainted()) {
            return -1.0f;
        }
        boolean isMultiHit = false;
        Substitute substitute = null;
        boolean hitSubstitute = false;
        if (source != this) {
            if (damageType == DamageTypeEnum.ATTACK || damageType == DamageTypeEnum.ATTACKFIXED) {
                substitute = (Substitute)this.getStatus(StatusType.Substitute);
                boolean bl = hitSubstitute = substitute != null && !substitute.ignoreSubstitute(source);
            }
            if (damageType == DamageTypeEnum.ATTACK) {
                ArrayList<PixelmonWrapper> allies;
                for (EffectBase e : source.attack.getAttackBase().effects) {
                    if (!(e instanceof MultipleHit) && !(e instanceof TripleKick) && !(e instanceof BeatUp)) continue;
                    isMultiHit = true;
                    break;
                }
                if (!isMultiHit) {
                    source.attack.sendEffectiveChat(source, this);
                }
                if ((allies = this.bc.getTeamPokemon(this.getParticipant())).size() > 1) {
                    for (PixelmonWrapper ally : allies) {
                        if (ally == this) continue;
                        damage = ally.getBattleAbility().modifyDamageTeammate((int)damage, source, this, source.attack);
                    }
                }
                damage = source.getBattleAbility().modifyDamageUser((int)damage, source, this, source.attack);
                if (!AbilityBase.ignoreAbility(source, this)) {
                    damage = thisAbility.modifyDamageTarget((int)damage, source, this, source.attack);
                    if (!hitSubstitute) {
                        damage = thisAbility.modifyDamageIncludeFixed((int)damage, source, this, source.attack);
                    }
                }
                if (!hitSubstitute) {
                    for (StatusBase status : this.getStatuses()) {
                        damage = status.modifyDamageIncludeFixed((int)damage, source, this, source.attack);
                    }
                    damage = (float)thisHeldItem.modifyDamageIncludeFixed(damage, source, this, source.attack);
                }
            } else if (damageType == DamageTypeEnum.ATTACKFIXED && !hitSubstitute) {
                for (StatusBase status : this.getStatuses()) {
                    damage = status.modifyDamageIncludeFixed((int)damage, source, this, source.attack);
                }
                damage = thisAbility.modifyDamageIncludeFixed((int)damage, source, this, source.attack);
                damage = (float)thisHeldItem.modifyDamageIncludeFixed(damage, source, this, source.attack);
            }
        }
        damage = (float)Math.floor(damage);
        if (hitSubstitute) {
            damageResult = substitute.attackSubstitute(damage, this);
            source.attack.moveResult.damage = (int)damageResult;
            source.attack.moveResult.fullDamage = (int)damage;
            source.attack.moveResult.target = this;
            source.attack.moveResult.result = AttackResult.succeeded;
            source.getUsableHeldItem().postProcessAttackUser(source, this, source.attack, damageResult);
            if (!isMultiHit) {
                Attack.postProcessAttackAllHits(source, this, source.attack, damageResult, damageType, true);
            }
            for (EffectBase effect : source.attack.getAttackBase().effects) {
                if (!(effect instanceof Recoil)) continue;
                ((Recoil)effect).applyRecoil(source, damageResult);
            }
        } else {
            if (source.attack != null && (source.attack.getAttackBase().isAttack("False Swipe") || source.attack.getAttackBase().isAttack("Hold Back"))) {
                damage = Math.min(damage, (float)(this.getHealth() - 1));
            }
            damageResult = Math.min((float)this.getHealth(), damage);
            if (source != this && source.attack != null && source.attack.moveResult != null) {
                source.attack.moveResult.damage = (int)damageResult;
                source.attack.moveResult.fullDamage = (int)damage;
                source.attack.moveResult.target = this;
                source.attack.moveResult.result = AttackResult.hit;
            }
            if (!this.bc.simulateMode) {
                if (this.pokemon != null) {
                    this.pokemon.attackEntityFrom(new BattleDamageSource("battle", source), 0.0f);
                }
                this.setHealth(Math.max(this.currentHP - (int)damageResult, 0));
                this.updateBattleDamage(damage);
                if (source.attack != null && !this.isSameTeam(source)) {
                    this.lastDirectPosition = source.battlePosition;
                    this.lastDirectDamage = (int)damageResult;
                    this.lastDirectCategory = source.attack.getAttackCategory();
                }
            }
            for (int i = 0; i < this.getStatusSize(); ++i) {
                this.getStatus(i).onDamageReceived(source, this, source.attack, (int)damageResult, damageType);
            }
            if (source != this && damageType.isDirect()) {
                if (source.attack.getAttackBase().getMakesContact() && !(source.getBattleAbility() instanceof LongReach)) {
                    Attack.applyContact(source, this);
                }
                source.getBattleAbility().tookDamageUser((int)damageResult, source, this, source.attack);
                thisAbility.tookDamageTarget((int)damageResult, source, this, source.attack);
                source.getUsableHeldItem().postProcessAttackUser(source, this, source.attack, damageResult);
                if (!isMultiHit) {
                    Attack.postProcessAttackAllHits(source, this, source.attack, damageResult, damageType, false);
                }
                for (EffectBase effect : source.attack.getAttackBase().effects) {
                    if (!(effect instanceof Recoil)) continue;
                    ((Recoil)effect).applyRecoil(source, damageResult);
                }
            }
            if (source.attack == null || !source.attack.canRemoveBerry()) {
                thisHeldItem.tookDamage(source, this, damageResult, damageType);
            }
            if (this.getHealth() <= 0) {
                String name = this.getNickname();
                this.bc.sendToAll("battlecontroller.hasfainted", name);
                this.getBattleAbility().onPokemonFaintSelf(this);
                this.bc.getActivePokemon().stream().filter(pw -> pw != this).forEach(pw -> pw.getBattleAbility().onPokemonFaintOther((PixelmonWrapper)pw, this));
                if (this.participant.getType() == ParticipantType.WildPokemon) {
                    this.pokemon.attackEntityFrom(new BattleDamageSource("battle", source), this.stats.HP);
                } else {
                    this.friendship.onFaint();
                    this.pokemon.update(EnumUpdateType.Friendship);
                    MinecraftForge.EVENT_BUS.post(new PixelmonFaintEvent(this.getPlayerOwner(), this.pokemon));
                }
            } else if (!this.bc.simulateMode) {
                this.damageTakenThisTurn = (int)((float)this.damageTakenThisTurn + damage);
            }
        }
        return damageResult;
    }

    public AbilityBase getAbility() {
        return this.ability;
    }

    public AbilityBase getBattleAbility() {
        return this.getBattleAbility(true, null);
    }

    public AbilityBase getBattleAbility(PixelmonWrapper moveUser) {
        return this.getBattleAbility(true, moveUser);
    }

    public AbilityBase getBattleAbility(boolean canIgnore) {
        return this.getBattleAbility(canIgnore, null);
    }

    public AbilityBase getBattleAbility(boolean canIgnore, PixelmonWrapper moveUser) {
        return canIgnore && AbilityBase.ignoreAbility(moveUser, this) ? ComingSoon.noAbility : (this.tempAbility != null ? this.tempAbility : this.ability);
    }

    public boolean hasHeldItem() {
        return this.heldItem != NoItem.noItem;
    }

    public ItemHeld getHeldItem() {
        return this.heldItem;
    }

    public ItemHeld getUsableHeldItem() {
        return ItemHeld.canUseItem(this) ? this.getHeldItem() : NoItem.noItem;
    }

    public void removeHeldItem() {
        this.setHeldItem(NoItem.noItem);
    }

    public void setHeldItem(ItemHeld newItem) {
        if (!this.bc.simulateMode) {
            this.heldItem = newItem == null ? NoItem.noItem : newItem;
        }
    }

    public void setHeldItem(ItemStack itemStack) {
        Item itemType;
        ItemHeld newItem = NoItem.noItem;
        if (itemStack != null && (itemType = itemStack.getItem()) instanceof ItemHeld) {
            newItem = (ItemHeld)itemType;
        }
        this.setHeldItem(newItem);
    }

    public static boolean canUltraBurst(ItemHeld heldItem, EnumSpecies species, short form) {
        return heldItem instanceof ZCrystal && ((ZCrystal)heldItem).crystalType.getUltraBurstSpecs().stream().anyMatch(a -> a.name.equals(species.name) && a.form == form);
    }

    public boolean canDynamax() {
        return this.participant.canDynamax();
    }

    public boolean canMegaEvolve() {
        if (this.getForm() == 10) {
            return false;
        }
        if (this.getSpecies() == EnumSpecies.Rayquaza) {
            if (this.moveset.hasAttack("Dragon Ascent", "attack.dragon ascent.name")) {
                return !(this.heldItem instanceof ZCrystal);
            }
            return false;
        }
        if (this.getSpecies() == EnumSpecies.Groudon) {
            return this.heldItem == PixelmonItemsHeld.redOrb;
        }
        if (this.getSpecies() == EnumSpecies.Kyogre) {
            return this.heldItem == PixelmonItemsHeld.blueOrb;
        }
        return PixelmonWrapper.canMegaEvolve(this.getHeldItem(), this.baseStats.pokemon, this.baseStats.form);
    }

    public static boolean canMegaEvolve(ItemStack heldItem, EnumSpecies pokemon, int form) {
        return PixelmonWrapper.canMegaEvolve(ItemHeld.getItemHeld(heldItem), pokemon, form);
    }

    public static boolean canMegaEvolve(ItemHeld heldItem, EnumSpecies pokemon, int form) {
        return form <= 0 && PixelmonWrapper.hasCompatibleMegaStone(heldItem, pokemon);
    }

    public static boolean isMegaEvolved(ItemStack heldItem, EnumSpecies pokemon, int form) {
        return form > 0 && PixelmonWrapper.hasCompatibleMegaStone(ItemHeld.getItemHeld(heldItem), pokemon);
    }

    public boolean hasCompatibleMegaStone() {
        return PixelmonWrapper.hasCompatibleMegaStone(this.getHeldItem(), this.baseStats.pokemon);
    }

    public static boolean hasCompatibleMegaStone(ItemHeld heldItem, EnumSpecies pokemon) {
        EnumMegaPokemon mega = EnumMegaPokemon.getMega(pokemon);
        if (mega != null && mega.getMegaEvoItems()[0].equals(ItemStack.EMPTY.getItem())) {
            return true;
        }
        if (heldItem.getHeldItemType() == EnumHeldItems.megaStone) {
            ItemMegaStone megaStone = (ItemMegaStone)heldItem;
            return megaStone.pokemon == pokemon;
        }
        return false;
    }

    public boolean isItemRemovable(PixelmonWrapper user) {
        if (user != this && this.getBattleAbility(user) instanceof StickyHold) {
            return false;
        }
        if (this.hasStatus(StatusType.Substitute)) {
            return false;
        }
        if (this.hasSpecialItem()) {
            return false;
        }
        ItemHeld heldItem = this.getHeldItem();
        return heldItem.getHeldItemType() != EnumHeldItems.mail;
    }

    public boolean hasSpecialItem() {
        ItemHeld item = this.getHeldItem();
        return item != null && (item instanceof ItemPlate && this.getBattleAbility() instanceof Multitype || this.getSpecies() == EnumSpecies.Giratina && item == PixelmonItemsHeld.griseous_orb || this.getSpecies() == EnumSpecies.Zacian && item == PixelmonItemsHeld.crownedSword || this.getSpecies() == EnumSpecies.Zamazenta && item == PixelmonItemsHeld.crownedShield || this.getSpecies() == EnumSpecies.Genesect && item.getHeldItemType() == EnumHeldItems.drive || this.hasCompatibleMegaStone() || item instanceof MemoryDrive && this.getBattleAbility() instanceof RKSSystem || item instanceof ZCrystal || item == PixelmonItemsHeld.redOrb || item == PixelmonItemsHeld.blueOrb);
    }

    public String getNickname() {
        AbilityBase battleAbility = this.getBattleAbility();
        if (battleAbility instanceof Illusion) {
            Illusion illusion = (Illusion)battleAbility;
            if (illusion.disguisedPokemon != null) {
                if (!illusion.disguisedNickname.isEmpty()) {
                    return illusion.disguisedNickname;
                }
                return Entity1Base.getLocalizedName(illusion.disguisedPokemon.name);
            }
        }
        return this.getRealNickname();
    }

    public String getRealNickname() {
        return this.link.getNickname();
    }

    public Gender getGender() {
        return this.link.getGender();
    }

    public int getPokeRus() {
        return this.link.getPokeRus();
    }

    public double getDynamaxHealthMultiplier() {
        return 0.5 + (double)this.dynamaxLevel * 0.05;
    }

    public int getHealth() {
        return this.currentHP;
    }

    public int getBaseHealth() {
        if (this.isDynamaxed()) {
            float percentAmount = this.getHealthPercent();
            return Entity6CanBattle.getPercentMaxHealth(percentAmount, this.stats.HP);
        }
        return this.currentHP;
    }

    public int getMaxHealth() {
        if (this.isDynamaxed()) {
            return (int)((double)this.stats.HP + (double)this.stats.HP * this.getDynamaxHealthMultiplier());
        }
        return this.stats.HP;
    }

    public float getHealthPercent() {
        return this.getHealthPercent(this.getHealth());
    }

    public float getHealthPercent(float amount) {
        return amount / (float)this.getMaxHealth() * 100.0f;
    }

    public float getHealPercent(float amount) {
        return this.getHealthPercent(Math.min(amount, (float)this.getHealthDeficit()));
    }

    public int getHealthDeficit() {
        return this.getMaxHealth() - this.getHealth();
    }

    public boolean hasFullHealth() {
        return this.getHealth() >= this.getMaxHealth();
    }

    public int getPercentMaxHealth(float percent) {
        return Entity6CanBattle.getPercentMaxHealth(percent, this.getMaxHealth());
    }

    public void healByPercent(float percent) {
        this.healEntityBy(this.getPercentMaxHealth(percent));
    }

    public void healEntityBy(int i) {
        if (i + this.getHealth() > this.getMaxHealth()) {
            i = this.getMaxHealth() - this.getHealth();
        }
        if (i != 0 && !this.bc.simulateMode) {
            if (this.pokemon != null && this.animateHP) {
                this.bc.sendHealPacket(this, i);
            }
            this.setHealth(this.getHealth() + i);
        }
    }

    public void setHealth(int newHP) {
        this.currentHP = newHP;
    }

    public void setMaxHealth(int newHP) {
        this.stats.HP = newHP;
    }

    public void setAttackFailed() {
        if (this.attack != null && this.attack.moveResult != null) {
            this.attack.moveResult.result = AttackResult.failed;
        }
    }

    public boolean doesLevel() {
        return this.link.doesLevel();
    }

    public int[] getPokemonID() {
        return this.link.getPokemonID();
    }

    public void update(EnumUpdateType ... types) {
        PlayerStorage storage = this.participant.getStorage();
        if (storage != null) {
            storage.updateClient(new WrapperLink(this), this.bc == null || !this.bc.battleEnded, types);
        }
    }

    public int getForm() {
        return this.form;
    }

    public void setForm(int form) {
        this.setFormNoEntity(form);
        if (this.pokemon != null) {
            this.pokemon.setForm(form);
        }
    }

    public void setFormNoEntity(int form) {
        this.form = form;
        this.baseStats = Entity3HasStats.getBaseStats(this.baseStats.pokemon.name, this.form).orElse(this.baseStats);
        Stats newStats = new Stats();
        newStats.IVs = this.stats.IVs;
        newStats.EVs = this.stats.EVs;
        this.stats = newStats.setLevelStats(this.getNature(), this.getPseudoNature(), this.baseStats, this.levelNum);
        this.type = this.baseStats.getTypeList();
        this.initialType = this.type;
        this.ability = AbilityBase.getAbility(this.baseStats.abilities[0]).orElse(this.ability);
    }

    public void resetBattleEvolution() {
        if (this.getSpecies() == EnumSpecies.Greninja && this.getForm() == 2) {
            this.setFormNoEntity(1);
        }
        if (this.isMega) {
            this.setFormNoEntity(-1);
            this.ability = this.initialAbility;
        }
        if (this.isPrimal) {
            this.setFormNoEntity(1);
        }
        if (this.isUltra) {
            this.setFormNoEntity(EnumSpecies.Necrozma.getFormEnum(this.form).getDefaultFromTemporary().getForm());
        }
    }

    public BlockPos getWorldPosition() {
        EntityLivingBase entity = this.pokemon;
        if (this.pokemon == null) {
            entity = this.getParticipant().getEntity();
        }
        return entity.getPosition();
    }

    public World getWorld() {
        return this.getParticipant().getEntity().world;
    }

    public EntityPlayerMP getPlayerOwner() {
        BattleParticipant participant = this.getParticipant();
        if (participant.getType() == ParticipantType.Player) {
            PlayerParticipant player = (PlayerParticipant)participant;
            return player.player;
        }
        return null;
    }

    public boolean isSingleType() {
        return this.type.size() == 1;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public boolean isSingleType(EnumType type) {
        if (!this.isSingleType()) return false;
        if (!this.hasType(type)) return false;
        return true;
    }

    public void addAttackers() {
        ArrayList<PixelmonWrapper> opponents = this.getOpponentPokemon();
        this.attackers.addAll(opponents);
        for (PixelmonWrapper opponent : opponents) {
            opponent.attackers.add(this);
        }
    }

    public String getOriginalTrainer() {
        return this.link.getOriginalTrainer();
    }

    public String getOriginalTrainerUUID() {
        return this.link.getOriginalTrainerUUID();
    }

    public String getRealTextureNoCheck() {
        return this.link.getRealTextureNoCheck(this);
    }

    public String getPokemonName() {
        return this.getSpecies().name;
    }

    public EnumSpecies getSpecies() {
        return this.baseStats.pokemon;
    }

    public EnumNature getNature() {
        return this.link.getNature();
    }

    public EnumNature getPseudoNature() {
        return this.link.getPseudoNature();
    }

    public void reloadMoveset() {
        if (this.pokemon != null) {
            this.moveset = this.pokemon.getMoveset();
        }
    }

    public Level getLevel() {
        return this.level;
    }

    public int getLevelNum() {
        return this.levelNum;
    }

    public int getDynamaxLevel() {
        return this.dynamaxLevel;
    }

    public int getExp() {
        return this.experience;
    }

    public void setLevelNum(int level) {
        this.levelNum = level;
    }

    public void setDynamaxLevel(int level) {
        this.dynamaxLevel = level;
    }

    public void setExp(int experience) {
        this.experience = experience;
    }

    public void setTempLevel(int level) {
        this.origLevel = this.getLevelNum();
        this.setLevelNum(level);
        this.setExp(0);
        this.tempLevel = true;
        this.currentHP = Math.min(this.currentHP, this.stats.HP);
        float hpFraction = (float)this.currentHP / (float)this.stats.HP;
        this.stats.setLevelStats(this.getNature(), this.getPseudoNature(), this.baseStats, level);
        this.currentHP = Math.round((float)this.stats.HP * hpFraction);
        if (hpFraction > 0.0f && this.currentHP == 0) {
            this.currentHP = 1;
        }
    }

    public PokemonLink getInnerLink() {
        return this.link;
    }

    public int getPartyPosition() {
        return this.partyPosition;
    }

    public void enableReturnHeldItem() {
        this.returnHeldItem = true;
    }

    public void writeToNBT() {
        if (this.nbt != null) {
            if (!this.tempLevel && this.pokemon != null) {
                this.pokemon.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.stats.HP);
            }
            if (this.tempLevel) {
                this.setTempLevel(this.origLevel);
            }
            if (this.bc.rules.fullHeal) {
                if (this.pokemon != null) {
                    this.pokemon.setHealth(this.startHealth);
                }
                this.nbt.setFloat("Health", this.startHealth);
                this.nbt.setBoolean("IsFainted", this.startHealth <= 0);
            } else {
                this.link.setHealth(this.currentHP);
                if (this.pokemon != null) {
                    this.pokemon.setHealth(this.currentHP);
                }
                this.getPrimaryStatus().writeToNBT(this.nbt);
            }
            if (!this.tempLevel) {
                this.stats.writeToNBT(this.nbt);
                this.level.writeToNBT(this.nbt);
            }
            if (this.heldItem != this.initialHeldItem) {
                ItemHeld writeHeldItem;
                ItemStack stack = null;
                ItemHeld itemHeld = writeHeldItem = this.returnHeldItem ? this.initialHeldItem : this.heldItem;
                if (writeHeldItem != null) {
                    stack = new ItemStack(writeHeldItem);
                }
                this.link.setHeldItem(stack);
            }
            this.moveset.writeToNBT(this.nbt, !this.bc.rules.fullHeal);
            this.friendship.writeToNBT(this.nbt);
        }
    }

    public void updateHPIncrease() {
        LevelUpUpdate update = new LevelUpUpdate(this.getPokemonID(), this.levelNum, this.currentHP, this.getMaxHealth());
        this.bc.participants.stream().filter(participant -> participant.getType() == ParticipantType.Player).forEach(participant -> Pixelmon.NETWORK.sendTo(update, ((PlayerParticipant)participant).player));
        this.bc.spectators.forEach(spectator -> spectator.sendMessage(update));
    }

    public String getCustomTexture() {
        return this.pokemon.getCustomTexture();
    }

    public boolean isDynamaxed() {
        return this.isDynamaxed;
    }

    public void setDynamaxed(boolean isDynamaxed) {
        this.isDynamaxed = isDynamaxed;
        this.pokemon.setDynamaxed(isDynamaxed);
    }
}

