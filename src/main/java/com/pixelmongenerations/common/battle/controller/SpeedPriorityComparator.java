/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller;

import com.pixelmongenerations.common.battle.controller.SpeedComparator;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class SpeedPriorityComparator
extends SpeedComparator {
    @Override
    protected boolean doesGoFirst(PixelmonWrapper p, PixelmonWrapper foe) {
        if (p.priority != foe.priority) {
            return p.priority > foe.priority;
        }
        return super.doesGoFirst(p, foe);
    }
}

