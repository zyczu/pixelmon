/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller;

import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.SpeedComparator;
import com.pixelmongenerations.common.battle.controller.SpeedPriorityComparator;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GrassyTerrain;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class CalcPriority {
    private static SpeedPriorityComparator speedPriorityComparator = new SpeedPriorityComparator();
    private static SpeedComparator speedComparator = new SpeedComparator();

    public static void checkMoveSpeed(BattleControllerBase bc) {
        ArrayList<PixelmonWrapper> fullParticipants = new ArrayList<PixelmonWrapper>(6);
        for (BattleParticipant p2 : bc.participants) {
            fullParticipants.addAll(p2.controlledPokemon);
        }
        ArrayList<PixelmonWrapper> filteredParticipants = new ArrayList<PixelmonWrapper>(fullParticipants.size());
        filteredParticipants.addAll(fullParticipants.stream().filter(PixelmonWrapper::isAlive).collect(Collectors.toList()));
        CalcPriority.sortByMoveSpeed(bc, filteredParticipants);
        bc.turnList = filteredParticipants;
    }

    private static void sortByMoveSpeed(BattleControllerBase bc, List<PixelmonWrapper> participants) {
        participants.stream().forEach(p -> {
            if (!bc.simulateMode) {
                p.priority = CalcPriority.calculatePriority(p);
            }
        });
        participants.sort(speedPriorityComparator);
        if (!bc.simulateMode) {
            participants.stream().filter(p -> p.attack != null).forEach(p -> {
                for (EffectBase e : p.attack.getAttackBase().effects) {
                    e.applyEarlyEffect((PixelmonWrapper)p);
                }
            });
        }
    }

    public static void recalculateMoveSpeed(BattleControllerBase bc, int turn) {
        ArrayList<PixelmonWrapper> toMove = new ArrayList<PixelmonWrapper>();
        while (bc.turnList.size() > turn) {
            toMove.add(bc.turnList.remove(turn));
        }
        CalcPriority.sortByMoveSpeed(bc, toMove);
        bc.turnList.addAll(toMove.stream().filter(PixelmonWrapper::isAlive).collect(Collectors.toList()));
    }

    static List<PixelmonWrapper> getDefaultTurnOrder(BattleControllerBase bc) {
        ArrayList<PixelmonWrapper> turnOrder = new ArrayList<PixelmonWrapper>(6);
        for (BattleParticipant p : bc.participants) {
            turnOrder.addAll(p.controlledPokemon);
        }
        turnOrder.sort(speedComparator);
        return turnOrder;
    }

    public static List<PixelmonWrapper> getTurnOrder(List<PixelmonWrapper> pokemon) {
        ArrayList<PixelmonWrapper> turnOrder = new ArrayList<PixelmonWrapper>(6);
        turnOrder.addAll(pokemon);
        turnOrder.sort(speedComparator);
        return turnOrder;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public static float calculatePriority(PixelmonWrapper p) {
        float priority = 0.0f;
        if (p.willTryFlee && p.getParticipant().getType() != ParticipantType.WildPokemon) {
            return 8.0f;
        }
        if (p.willUseItemInStack != null) return 6.0f;
        if (p.isSwitching) {
            return 6.0f;
        }
        if (p.attack == null) return priority;
        if (p.attack.isAttack("Grassy Glide") && p.bc.globalStatusController.getTerrain() instanceof GrassyTerrain) {
            return 1.0f;
        }
        if (p.lastAttack != null) {
            if (p.lastAttack.isAttack("Beak Blast")) {
                return -3.0f;
            }
        }
        priority = p.attack.getAttackBase().getPriority();
        priority = p.getUsableHeldItem().modifyPriority(p, priority);
        priority = p.getBattleAbility().modifyPriority(p, priority);
        if (!p.attack.isAttack("Pursuit")) return priority;
        Iterator<PixelmonWrapper> iterator = p.targets.iterator();
        while (iterator.hasNext()) {
            PixelmonWrapper target = iterator.next();
            if (!target.isSwitching) continue;
            if (target.isAlive()) return 7.0f;
        }
        return priority;
    }
}

