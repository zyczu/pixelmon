/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.log;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.log.AttackAction;
import com.pixelmongenerations.common.battle.controller.log.BattleActionBase;
import com.pixelmongenerations.common.battle.controller.log.SwitchAction;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.util.ArrayList;

public class BattleLog {
    private int crashCount = 0;
    BattleControllerBase bc;
    ArrayList<BattleActionBase[]> log;
    private final int size;

    public BattleLog(BattleControllerBase bc) {
        this.bc = bc;
        if (bc.rules.battleType.numPokemon == 2) {
            this.size = 4;
            this.log = new ArrayList();
            this.log.add(new BattleActionBase[this.size]);
        } else if (bc.rules.battleType.numPokemon == 1) {
            this.size = 2;
            this.log = new ArrayList();
            this.log.add(new BattleActionBase[this.size]);
        } else {
            if (PixelmonConfig.printErrors) {
                Pixelmon.LOGGER.error("Fatal error in initialisation of battleLog. Is this a triple or rotation battle?");
            }
            this.size = 0;
        }
    }

    public void addEvent(BattleActionBase event) {
        try {
            this.log.get((int)event.turn)[event.pokemonPosition] = event;
        }
        catch (ArrayIndexOutOfBoundsException e) {
            this.log.get((int)event.turn)[0] = event;
        }
    }

    public BattleActionBase getAction(int turn, int position) {
        if (position >= this.log.get(turn).length) {
            position = this.log.get(turn).length - 1;
        }
        return this.log.get(turn)[position];
    }

    public BattleActionBase getActionForPokemon(int turn, PixelmonWrapper pw) {
        for (BattleActionBase bab : this.log.get(turn)) {
            if (bab == null || bab.pokemon != pw) continue;
            return bab;
        }
        return null;
    }

    public Attack getLastAttack(PixelmonWrapper pw) {
        for (int i = this.log.size() - 1; i >= 0; --i) {
            for (BattleActionBase bab : this.log.get(i)) {
                if (bab != null && bab.pokemon == pw && bab instanceof AttackAction) {
                    return ((AttackAction)bab).getAttack();
                }
                if (!(bab instanceof SwitchAction) || !PixelmonMethods.isIDSame(pw.getPokemonID(), ((SwitchAction)bab).switchingTo)) continue;
                return null;
            }
        }
        return null;
    }

    public ArrayList<Attack> getLastAttacks(PixelmonWrapper pw) {
        ArrayList<Attack> attacks = new ArrayList<Attack>();
        for (int i = this.log.size() - 1; i >= 0; --i) {
            for (BattleActionBase bab : this.log.get(i)) {
                if (bab != null && bab.pokemon == pw && bab instanceof AttackAction) {
                    attacks.add(((AttackAction)bab).getAttack());
                    continue;
                }
                if (!(bab instanceof SwitchAction) || !PixelmonMethods.isIDSame(pw.getPokemonID(), ((SwitchAction)bab).switchingTo)) continue;
                return attacks;
            }
        }
        return attacks;
    }

    public void turnTick() {
        this.log.add(new BattleActionBase[this.size]);
    }

    public void onCrash(Exception exc, String error) {
        ++this.crashCount;
        if (PixelmonConfig.printErrors) {
            System.out.println(error);
            exc.printStackTrace();
        }
        if (this.crashCount >= 3) {
            if (PixelmonConfig.printErrors) {
                System.out.println("===Too many errors detected in battle, force-ending===");
                this.bc.participants.stream().filter(participant -> participant instanceof PlayerParticipant).forEach(participant -> ChatHandler.sendChat(participant.getEntity(), "pixelmon.general.battleerror", new Object[0]));
            }
            this.bc.endBattleWithoutXP();
        }
    }
}

