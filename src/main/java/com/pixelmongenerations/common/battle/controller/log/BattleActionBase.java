/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.log;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;

public class BattleActionBase {
    public int turn;
    public int pokemonPosition;
    public PixelmonWrapper pokemon;

    public BattleActionBase(int turn, int pokemonPosition, PixelmonWrapper pokemon) {
        this.turn = turn;
        this.pokemonPosition = pokemonPosition;
        this.pokemon = pokemon;
    }
}

