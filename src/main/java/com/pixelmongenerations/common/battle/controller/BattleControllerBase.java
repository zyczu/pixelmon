/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.battle.controller;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.events.BeatWildPixelmonEvent;
import com.pixelmongenerations.api.events.SpectateEvent;
import com.pixelmongenerations.api.events.battles.BattleEndEvent;
import com.pixelmongenerations.api.events.battles.TurnEndEvent;
import com.pixelmongenerations.api.events.player.PlayerBattleEndedAbnormalEvent;
import com.pixelmongenerations.api.events.player.PlayerBattleEndedEvent;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.client.gui.battles.PixelmonInGui;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.BattleStage;
import com.pixelmongenerations.common.battle.controller.CalcPriority;
import com.pixelmongenerations.common.battle.controller.Experience;
import com.pixelmongenerations.common.battle.controller.GlobalStatusController;
import com.pixelmongenerations.common.battle.controller.log.BattleLog;
import com.pixelmongenerations.common.battle.controller.log.FleeAction;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.Spectator;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.battle.rules.BattleRules;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.HoneyGather;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Imposter;
import com.pixelmongenerations.common.entity.pixelmon.abilities.NeutralizingGas;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Pickup;
import com.pixelmongenerations.common.entity.pixelmon.abilities.RunAway;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Synchronize;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.common.item.ItemPokeball;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.EnumForceBattleResult;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.enums.EnumMark;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.battle.BattleResults;
import com.pixelmongenerations.core.enums.battle.EnumBattleEndCause;
import com.pixelmongenerations.core.enums.battle.EnumBattleMusic;
import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.packetHandlers.SwitchCamera;
import com.pixelmongenerations.core.network.packetHandlers.battles.DynamaxPacket;
import com.pixelmongenerations.core.network.packetHandlers.battles.EndSpectate;
import com.pixelmongenerations.core.network.packetHandlers.battles.FailFleeSwitch;
import com.pixelmongenerations.core.network.packetHandlers.battles.FormBattleUpdate;
import com.pixelmongenerations.core.network.packetHandlers.battles.SetBattlingPokemon;
import com.pixelmongenerations.core.network.packetHandlers.battles.SetPokemonBattleData;
import com.pixelmongenerations.core.network.packetHandlers.battles.gui.SwitchOutPacket;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.common.MinecraftForge;

public class BattleControllerBase {
    public ArrayList<BattleParticipant> participants = new ArrayList();
    public GlobalStatusController globalStatusController = new GlobalStatusController(this);
    public ArrayList<Spectator> spectators = new ArrayList();
    protected int battleTicks = 0;
    public int battleTurn = -1;
    public int playerNumber = 0;
    public boolean battleEnded = false;
    public int battleIndex;
    public BattleLog battleLog;
    public Attack lastAttack;
    public int numFainted;
    public BattleRules rules;
    @Deprecated
    public EnumBattleType battleType;
    public boolean simulateMode = false;
    public boolean sendMessages = true;
    public boolean wasFishing = false;
    private boolean calculatedTurnOrder = false;
    private List<PixelmonWrapper> switchingOut = new ArrayList<PixelmonWrapper>();
    private static final int TICK_TOP = 20;
    private Set<PixelmonWrapper> checkEvolve = new HashSet<PixelmonWrapper>();
    private boolean init = false;
    private BattleStage stage = BattleStage.PICKACTION;
    public int turn = 0;
    public ArrayList<PixelmonWrapper> turnList = new ArrayList();
    boolean paused = false;
    EnumBattleMusic battleSongId;
    private boolean hordeBattle = false;
    private boolean sosBattle = false;
    public Map<Integer, Boolean> sosCallMap = new HashMap<Integer, Boolean>();
    public int sosChain = 0;
    public int lastSosRate = 0;
    public boolean superEffectiveDamage = false;
    public int turnCounter = 0;
    public Map<Integer, PokemonSpec> hordePokemonParticipants = new HashMap<Integer, PokemonSpec>();

    public BattleControllerBase(BattleParticipant[] team1, BattleParticipant[] team2, BattleRules rules) {
        for (BattleParticipant p : team1) {
            p.team = 0;
            this.participants.add(p);
        }
        for (BattleParticipant p : team2) {
            p.team = 1;
            this.participants.add(p);
        }
        rules.validateRules();
        this.rules = rules;
        this.battleType = this.rules.battleType;
        this.battleLog = new BattleLog(this);
    }

    protected void initBattle() throws Exception {
        for (BattleParticipant p : this.participants) {
            if (p.checkPokemon()) continue;
            String name = p.getDisplayName();
            throw new Exception("Battle could not start! Name: " + name + " Class: " + p.getClass().getName());
        }
        this.battleSongId = EnumBattleMusic.selectRandomBattleRegular();
        for (BattleParticipant p : this.participants) {
            p.startBattle(this);
            if (!(p instanceof WildPixelmonParticipant) || this.participants.size() != 2) continue;
            for (PixelmonWrapper wildWrapper : p.controlledPokemon) {
                EntityPixelmon pokemon = wildWrapper.pokemon;
                if (pokemon == null) continue;
                if (pokemon.getEntityData().hasKey("SynchronizedNature")) {
                    pokemon.setNature(EnumNature.getNatureFromIndex(pokemon.getEntityData().getInteger("SynchronizedNature")));
                    pokemon.setPseudoNature(EnumNature.getNatureFromIndex(pokemon.getEntityData().getInteger("SynchronizedNature")));
                    pokemon.getEntityData().removeTag("SynchronizedNature");
                }
                this.participants.stream().filter(part -> part != p && part instanceof PlayerParticipant).forEach(part -> {
                    PixelmonWrapper leadingPokemon;
                    if (part.controlledPokemon.get(0) != null && (leadingPokemon = part.controlledPokemon.get(0)).getBattleAbility() instanceof Synchronize) {
                        pokemon.getEntityData().setInteger("SynchronizedNature", leadingPokemon.getNature().index);
                        pokemon.setNature(leadingPokemon.getNature());
                        pokemon.setPseudoNature(leadingPokemon.getNature());
                        this.sendToAll("pixelmon.abilities.syncrhonize", pokemon.getNickname(), leadingPokemon.getNickname());
                    }
                });
                if (pokemon.isTotem() || pokemon.isBossPokemon()) {
                    this.battleSongId = EnumBattleMusic.Boss;
                } else if (EnumSpecies.legendaries.contains(pokemon.getSpecies().toString())) {
                    this.battleSongId = EnumBattleMusic.Legendary;
                } else if (EnumSpecies.ultrabeasts.contains(pokemon.getSpecies().toString())) {
                    this.battleSongId = EnumBattleMusic.Ultra;
                }
                if (pokemon.isTotem()) {
                    for (StatsType stat : pokemon.baseStats.getHighestStats()) {
                        if (stat == null) continue;
                        wildWrapper.getBattleStats().modifyStat(1, stat);
                    }
                }
            }
        }
        this.setMusic(this.battleSongId);
        this.modifyStats();
        List<PixelmonWrapper> turnOrder = this.getDefaultTurnOrder();
        for (PixelmonWrapper pw : turnOrder) {
            pw.getBattleAbility().beforeSwitch(pw);
        }
        for (BattleParticipant p : this.participants) {
            p.updateOtherPokemon();
            for (PixelmonWrapper pw : p.controlledPokemon) {
                pw.addAttackers();
            }
        }
        for (BattleParticipant p : this.participants) {
            if (!(p instanceof PlayerParticipant)) continue;
            ((PlayerParticipant)p).openGui();
            ++this.playerNumber;
        }
        this.battleTurn = 0;
        for (PixelmonWrapper pw : turnOrder) {
            pw.afterSwitch();
        }
        this.init = true;
    }

    public void update() {
        block37: {
            if (this.battleEnded) {
                return;
            }
            try {
                if (!this.init) {
                    try {
                        this.initBattle();
                    }
                    catch (Exception e) {
                        BattleRegistry.deRegisterBattle(this);
                        e.printStackTrace();
                        this.init = false;
                        this.endBattleWithoutXP();
                        return;
                    }
                }
                this.onUpdate();
                if (this.isEvolving() || this.isWaiting() || this.paused || this.simulateMode || this.participants.size() < 2) {
                    return;
                }
                if (this.battleTicks++ <= 20) {
                    return;
                }
                if (this.stage == BattleStage.PICKACTION) {
                    for (BattleParticipant p : this.participants) {
                        p.clearTurnVariables();
                        p.selectAction();
                    }
                    this.stage = BattleStage.DOACTION;
                    this.turn = 0;
                } else if (this.stage == BattleStage.DOACTION) {
                    this.modifyStats();
                    if (this.turn == 0 && !this.calculatedTurnOrder) {
                        try {
                            CalcPriority.checkMoveSpeed(this);
                            this.calculatedTurnOrder = true;
                        }
                        catch (Exception exc) {
                            this.battleLog.onCrash(exc, "Problem checking move speed.");
                        }
                    }
                    if (this.turn < this.turnList.size()) {
                        this.modifyStatsCancellable(this.turnList.get(this.turn));
                    }
                    for (BattleParticipant p : this.participants) {
                        for (PixelmonWrapper poke : p.controlledPokemon) {
                            if (poke.pokemon.isLoaded(true)) continue;
                            poke.pokemon.catchInPokeball();
                            if (this.getPlayers().isEmpty()) {
                                this.endBattleWithoutXP();
                                return;
                            }
                            poke.pokemon.world.unloadEntities(new ArrayList<Entity>(Collections.singletonList(poke.pokemon)));
                            poke.pokemon.releaseFromPokeball();
                            poke.pokemon.hurtResistantTime = 0;
                        }
                    }
                    boolean endTurn = false;
                    int numTurns = this.turnList.size();
                    if (this.turn < numTurns) {
                        this.participants.stream().filter(bp -> bp.getType() == ParticipantType.Player).forEach(bp -> {
                            bp.wait = true;
                        });
                        PixelmonWrapper currentPokemon = this.turnList.get(this.turn);
                        if (!this.simulateMode) {
                            boolean hasEvolved = false;
                            boolean hasDynamaxed = false;
                            if (currentPokemon.priority < 6.0f) {
                                for (PixelmonWrapper pw : this.getDefaultTurnOrder()) {
                                    if (pw.willDynamax && !pw.isDynamaxed()) {
                                        boolean bl = hasDynamaxed = pw.dynamax() || hasDynamaxed;
                                        if (hasDynamaxed) {
                                            pw.getParticipant().canMega = false;
                                            this.modifyStats();
                                            pw.willDynamax = false;
                                        }
                                    }
                                    if (!pw.willEvolve || !(hasEvolved = pw.megaEvolve() || hasEvolved)) continue;
                                    pw.getParticipant().canDynamax = false;
                                    this.modifyStats();
                                }
                            } else if (currentPokemon.attack != null && currentPokemon.willEvolve) {
                                hasEvolved = currentPokemon.megaEvolve();
                            }
                            if (hasEvolved || hasDynamaxed) {
                                CalcPriority.recalculateMoveSpeed(currentPokemon.bc, currentPokemon.bc.turn);
                                this.modifyStats();
                                return;
                            }
                        }
                        for (PixelmonWrapper pw : this.getActivePokemon()) {
                            if (!pw.suppressedAbility) continue;
                            pw.getBattleAbility(false).applySwitchInEffect(pw);
                            pw.suppressedAbility = false;
                        }
                        this.superEffectiveDamage = false;
                        this.takeTurn(currentPokemon);
                        numTurns = this.turnList.size();
                        ++this.turn;
                        if (this.turn >= numTurns) {
                            endTurn = true;
                            for (BattleParticipant p : this.participants) {
                                for (PixelmonWrapper pw : p.controlledPokemon) {
                                    if (pw.newPokemonID != null && pw.isFainted()) {
                                        this.takeTurn(pw);
                                        continue;
                                    }
                                    if (!pw.nextSwitchIsMove) continue;
                                    endTurn = false;
                                }
                            }
                        }
                    } else {
                        endTurn = true;
                    }
                    this.calculatedTurnOrder = false;
                    if (endTurn) {
                        this.applyRepeatedEffects();
                        this.endTurn();
                    }
                    this.checkPokemon();
                    if (endTurn) {
                        MinecraftForge.EVENT_BUS.post(new TurnEndEvent(this));
                        ++this.turnCounter;
                        this.checkFaint();
                    }
                }
                this.battleTicks = 0;
            }
            catch (Exception e) {
                if (this.battleLog != null) {
                    this.battleLog.onCrash(e, "Caught error in battle. Continuing...");
                }
                if (!PixelmonConfig.printErrors) break block37;
                Pixelmon.LOGGER.info("Caught error in battle. Continuing...", (Throwable)e);
            }
        }
    }

    private boolean isEvolving() {
        for (BattleParticipant p : this.participants) {
            for (PixelmonWrapper pw : p.controlledPokemon) {
                if (pw.evolution == null) continue;
                if (pw.evolution.isEnded()) {
                    pw.evolution = null;
                    p.wait = false;
                    pw.wait = false;
                    String path = "pixelmon.battletext.";
                    if (pw.pokemon.getSpecies().equals((Object)EnumSpecies.Greninja)) {
                        path = path + "ashgreninja.evolve";
                    } else if (pw.pokemon.getSpecies().equals((Object)EnumSpecies.Groudon) || pw.pokemon.getSpecies().equals((Object)EnumSpecies.Kyogre)) {
                        if (pw.isDynamaxed()) continue;
                        if (pw.getHeldItem() == PixelmonItemsHeld.redOrb && pw.pokemon.getSpecies().equals((Object)EnumSpecies.Groudon) || pw.getHeldItem() == PixelmonItemsHeld.blueOrb && pw.pokemon.getSpecies().equals((Object)EnumSpecies.Kyogre)) {
                            path = "gui.primal.transform";
                        }
                    } else if (pw.pokemon.getSpecies().equals((Object)EnumSpecies.Necrozma)) {
                        path = path + "ultraburst";
                    } else {
                        if (pw.isDynamaxed()) continue;
                        path = path + "megaevolve";
                    }
                    this.sendToAll(path, pw.getNickname(), Entity1Base.getLocalizedName(pw.getPokemonName()));
                    if (pw.getHeldItem() == PixelmonItemsHeld.redOrb && pw.pokemon.getSpecies().equals((Object)EnumSpecies.Groudon)) {
                        this.sendToAll("The sunlight turned extremely harsh!", pw.pokemon.getNickname());
                    } else if (pw.getHeldItem() == PixelmonItemsHeld.blueOrb && pw.pokemon.getSpecies().equals((Object)EnumSpecies.Kyogre)) {
                        this.sendToAll("A heavy rain began to fall!", pw.pokemon.getNickname());
                    }
                    BattleControllerBase.checkSwitchInAbility(pw);
                    continue;
                }
                return true;
            }
        }
        return false;
    }

    public static void checkSwitchInAbility(PixelmonWrapper pw) {
        if (NeutralizingGas.checkNeturalizingGas(pw)) {
            pw.suppressedAbility = true;
        } else {
            pw.suppressedAbility = false;
            AbilityBase ability = pw.getBattleAbility();
            if (ability != null) {
                ability.applySwitchInEffect(pw);
            }
        }
    }

    public void modifyStats() {
        for (BattleParticipant p : this.participants) {
            for (PixelmonWrapper pw : p.controlledPokemon) {
                int[] stats;
                if (pw.getAbility() instanceof Imposter) continue;
                if (pw.getSpecies() == EnumSpecies.Ditto || pw.getSpecies() == EnumSpecies.Mew) {
                    if (pw.hasStatus(StatusType.Transformed)) continue;
                    stats = pw.getBattleStats().getBaseBattleStats();
                    for (int i = 0; i < pw.getStatusSize(); ++i) {
                        stats = pw.getStatus(i).modifyStats(pw, stats);
                    }
                    stats = pw.getBattleAbility().modifyStats(pw, stats);
                    for (PixelmonWrapper teammate : this.getTeamPokemon(pw)) {
                        stats = teammate.getBattleAbility().modifyStatsTeammate(pw, stats);
                    }
                    stats = pw.getUsableHeldItem().modifyStats(pw, stats);
                    for (GlobalStatusBase gsb : this.globalStatusController.getGlobalStatuses()) {
                        stats = gsb.modifyStats(pw, stats);
                    }
                    stats = pw.getBattleAbility().modifyStatsItem(pw, stats);
                    for (GlobalStatusBase gsb : this.globalStatusController.getGlobalStatuses()) {
                        stats = gsb.modifyStats(pw, stats);
                    }
                    pw.getBattleStats().setStatsForTurn(stats);
                    continue;
                }
                stats = pw.getBattleStats().getBaseBattleStats();
                for (int i = 0; i < pw.getStatusSize(); ++i) {
                    stats = pw.getStatus(i).modifyStats(pw, stats);
                }
                stats = pw.getBattleAbility().modifyStats(pw, stats);
                for (PixelmonWrapper teammate : this.getTeamPokemon(pw)) {
                    stats = teammate.getBattleAbility().modifyStatsTeammate(pw, stats);
                }
                stats = pw.getUsableHeldItem().modifyStats(pw, stats);
                for (GlobalStatusBase gsb : this.globalStatusController.getGlobalStatuses()) {
                    stats = gsb.modifyStats(pw, stats);
                }
                stats = pw.getBattleAbility().modifyStatsItem(pw, stats);
                for (GlobalStatusBase gsb : this.globalStatusController.getGlobalStatuses()) {
                    stats = gsb.modifyStats(pw, stats);
                }
                pw.getBattleStats().setStatsForTurn(stats);
            }
        }
    }

    public void modifyStatsCancellable(PixelmonWrapper attacker) {
        for (BattleParticipant p : this.participants) {
            for (PixelmonWrapper pw : p.controlledPokemon) {
                int[] stats = pw.getBattleStats().getBattleStats();
                if (!AbilityBase.ignoreAbility(attacker, pw) || !attacker.targets.contains(pw)) {
                    stats = pw.getBattleAbility().modifyStatsCancellable(pw, stats);
                    for (PixelmonWrapper teammate : this.getTeamPokemon(pw)) {
                        stats = teammate.getBattleAbility().modifyStatsCancellableTeammate(pw, stats);
                    }
                }
                for (int i = 0; i < pw.getStatusSize(); ++i) {
                    StatusBase status = pw.getStatus(i);
                    if (status.ignoreStatus(attacker, pw)) continue;
                    stats = status.modifyStatsCancellable(pw, stats);
                }
                pw.getBattleStats().setStatsForTurn(stats);
            }
        }
    }

    public void participantReady(PlayerParticipant p) {
        p.wait = false;
        for (PixelmonWrapper pw : this.getDefaultTurnOrder()) {
            for (BattleParticipant z : this.participants) {
                z.faintedLastTurn = false;
                if (pw.dynamaxTurnsLeft > 0 || !pw.isDynamaxed() || pw.isFainted()) continue;
                float percent = pw.getHealthPercent();
                pw.setDynamaxed(false);
                pw.pokemon.revertDynamaxSize();
                if (z.getType() == ParticipantType.Player) {
                    PlayerParticipant player = (PlayerParticipant)z;
                    Pixelmon.NETWORK.sendTo(new DynamaxPacket(pw.getPokemonID(), true), player.player);
                }
                int newHealth = Math.round((float)pw.getMaxHealth() * percent / 100.0f);
                pw.setHealth(newHealth);
                pw.updateHPIncrease();
            }
        }
    }

    public void setAllReady() {
        for (PlayerParticipant player : this.getPlayers()) {
            this.participantReady(player);
        }
    }

    private void applyRepeatedEffects() {
        boolean teamAble = false;
        boolean opponentAble = false;
        BattleParticipant participant = this.participants.get(0);
        for (PixelmonWrapper pw : this.getTeamPokemon(participant)) {
            if (!pw.isAlive()) continue;
            teamAble = true;
            break;
        }
        if (!teamAble) {
            for (BattleParticipant p : this.getTeam(this.participants.get(0))) {
                if (p.countAblePokemon() <= 0) continue;
                teamAble = true;
                break;
            }
        }
        for (PixelmonWrapper pw : this.getOpponentPokemon(participant)) {
            if (!pw.isAlive()) continue;
            opponentAble = true;
            break;
        }
        if (!opponentAble) {
            for (BattleParticipant p : this.getOpponents(this.participants.get(0))) {
                if (p.countAblePokemon() <= 0) continue;
                opponentAble = true;
                break;
            }
        }
        if (teamAble && opponentAble) {
            for (GlobalStatusBase gsb : this.globalStatusController.getGlobalStatuses()) {
                gsb.applyRepeatedEffect(this.globalStatusController);
            }
            for (PixelmonWrapper pw : this.getDefaultTurnOrder()) {
                pw.turnTick();
            }
        }
    }

    private void endTurn() {
        this.stage = BattleStage.PICKACTION;
        this.updateClients();
        this.battleLog.turnTick();
        ++this.battleTurn;
    }

    public void updateClients() {
        if (!this.hordeBattle) {
            return;
        }
        this.spectators.forEach(spectator -> {
            PlayerParticipant watchedParticipant = this.getPlayer(spectator.watchedName);
            if (watchedParticipant == null) {
                return;
            }
            ArrayList<PixelmonWrapper> teamList = watchedParticipant.getTeamPokemonList();
            Pixelmon.NETWORK.sendTo(new SetBattlingPokemon(teamList), spectator.getEntity());
            Pixelmon.NETWORK.sendTo(new SetPokemonBattleData(PixelmonInGui.convertToGUI(teamList), true), spectator.getEntity());
            Pixelmon.NETWORK.sendTo(new SetPokemonBattleData(watchedParticipant.getOpponentData(), true), spectator.getEntity());
        });
        this.participants.stream().filter(PlayerParticipant.class::isInstance).map(PlayerParticipant.class::cast).forEach(player -> {
            ArrayList<PixelmonWrapper> teamList = player.getTeamPokemonList();
            Pixelmon.NETWORK.sendTo(new SetBattlingPokemon(teamList), player.player);
            Pixelmon.NETWORK.sendTo(new SetPokemonBattleData(PixelmonInGui.convertToGUI(teamList), true), player.player);
            Pixelmon.NETWORK.sendTo(new SetPokemonBattleData(player.getOpponentData(), true), player.player);
        });
    }

    /*
     * WARNING - void declaration
     */
    public void checkReviveSendOut(BattleParticipant p) {
        if (this.rules.battleType.numPokemon > 1 && p != null && p.getType() == ParticipantType.Player && this.getTeam(p).size() == 1 && p.controlledPokemon.size() < this.rules.battleType.numPokemon && p.countAblePokemon() > p.controlledPokemon.size()) {
            EnumMark mark;
            PlayerParticipant player = (PlayerParticipant)p;
            ArrayList<StatusBase> wholeTeamStatuses = new ArrayList<StatusBase>();
            Iterator<PixelmonWrapper> iterator = p.controlledPokemon.iterator();
            if (iterator.hasNext()) {
                PixelmonWrapper pw = iterator.next();
                wholeTeamStatuses = new ArrayList();
                for (StatusBase statusBase : pw.getStatuses()) {
                    if (!statusBase.isWholeTeamStatus()) continue;
                    wholeTeamStatuses.add(statusBase.copy());
                }
            }
            int oldPosition = 0;
            int newPosition = 0;
            do {
                oldPosition = newPosition;
                for (PixelmonWrapper pixelmonWrapper : p.controlledPokemon) {
                    if (newPosition != pixelmonWrapper.battlePosition) continue;
                    ++newPosition;
                }
            } while (oldPosition != newPosition);
            PixelmonWrapper pixelmonWrapper = null;
            for (PixelmonWrapper pw : player.allPokemon) {
                if (!pw.isAlive() || pw.pokemon != null) continue;
                EntityPixelmon revivedPokemon = player.storage.sendOut(pw.getPokemonID(), player.getWorld());
                revivedPokemon.setHealth(pw.getHealth());
                pw.update(EnumUpdateType.HP);
                revivedPokemon.battleController = this;
                pixelmonWrapper = pw;
                pw.pokemon = revivedPokemon;
                break;
            }
            if (pixelmonWrapper == null) {
                return;
            }
            pixelmonWrapper.battlePosition = newPosition;
            for (StatusBase status : wholeTeamStatuses) {
                pixelmonWrapper.addStatus(status, pixelmonWrapper);
            }
            player.controlledPokemon.add(newPosition, pixelmonWrapper);
            pixelmonWrapper.bc = player.bc;
            PlayerStorage playerStorage = player.getStorage();
            if (playerStorage == null) {
                this.endBattleWithoutXP();
                return;
            }
            if (!playerStorage.isInWorld(pixelmonWrapper.getPokemonID())) {
                pixelmonWrapper.pokemon.setLocationAndAngles(player.player.posX, player.player.posY, player.player.posZ, player.player.rotationYaw, 0.0f);
                pixelmonWrapper.pokemon.releaseFromPokeball();
            }
            if (!AbilityBase.ignoreAbility(pixelmonWrapper)) {
                pixelmonWrapper.getBattleAbility().beforeSwitch(pixelmonWrapper);
            }
            if ((mark = pixelmonWrapper.getInnerLink().getMark()) != EnumMark.None) {
                ChatHandler.sendBattleMessage(player.player, "playerparticipant.gomark", pixelmonWrapper.getNickname(), mark.getTitle());
            } else {
                ChatHandler.sendBattleMessage(player.player, "playerparticipant.go", pixelmonWrapper.getNickname());
            }
            this.sendToOthers("battlecontroller.sendout", player, player.player.getDisplayName().getUnformattedText(), pixelmonWrapper.getNickname());
            for (BattleParticipant p2 : this.participants) {
                p2.updateOtherPokemon();
            }
            pixelmonWrapper.afterSwitch();
            pixelmonWrapper.addAttackers();
            this.sendSwitchPacket(new int[]{-1, -1}, pixelmonWrapper);
        }
    }

    void checkFaint() {
        ArrayList<PixelmonWrapper> fainted = new ArrayList<PixelmonWrapper>();
        for (BattleParticipant p : this.participants) {
            ArrayList<PixelmonWrapper> toRemove = new ArrayList<PixelmonWrapper>();
            for (PixelmonWrapper pw : p.controlledPokemon) {
                if (!pw.isFainted() || pw.isSwitching) continue;
                if (pw.removePrimaryStatus() != null) {
                    pw.pokemon.sendStatusPacket(-1);
                }
                pw.update(EnumUpdateType.Status);
                p.updatePokemon(pw);
                if (p.addSwitchingOut(pw)) {
                    fainted.add(pw);
                    continue;
                }
                toRemove.add(pw);
            }
            for (PixelmonWrapper pw : toRemove) {
                pw.isSwitching = false;
                pw.wait = false;
                p.controlledPokemon.remove(pw);
                this.updateRemovedPokemon(pw);
            }
        }
        this.switchingOut.addAll(fainted);
        for (PixelmonWrapper pw : fainted) {
            BattleParticipant p = pw.getParticipant();
            p.faintedLastTurn = true;
            pw.willTryFlee = false;
            p.wait = true;
            p.getNextPokemon(pw.battlePosition);
        }
    }

    private void onUpdate() {
        this.participants.forEach(BattleParticipant::tick);
        if (this.isPvP()) {
            this.participants.stream().filter(p -> ((PlayerParticipant)p).player == null || !((PlayerParticipant)p).player.isEntityAlive()).forEach(p -> this.endBattle(EnumBattleEndCause.FORCE));
        } else {
            this.participants.stream().filter(p -> p.getType() == ParticipantType.Player).filter(p -> ((PlayerParticipant)p).player == null || ((PlayerParticipant)p).player.isDead).forEach(p -> this.endBattle(EnumBattleEndCause.FORCE));
        }
    }

    public HashMap<BattleParticipant, BattleResults> endBattle(EnumBattleEndCause cause) {
        return this.endBattle(cause, new HashMap<BattleParticipant, BattleResults>());
    }

    public HashMap<BattleParticipant, BattleResults> endBattle(EnumBattleEndCause cause, HashMap<BattleParticipant, BattleResults> results) {
        if (results == null) {
            results = new HashMap();
        }
        boolean abnormal = false;
        this.battleEnded = true;
        this.globalStatusController.endBattle();
        this.setMusic(EnumBattleMusic.None);
        for (BattleParticipant p : this.participants) {
            for (PixelmonWrapper pw : p.controlledPokemon) {
                EntityPixelmon pixelmon;
                pw.getBattleAbility().applyEndOfBattleEffect(pw);
                pw.resetOnSwitch();
                if (pw.getSpecies() == EnumSpecies.Zamazenta || pw.getSpecies() == EnumSpecies.Zacian) {
                    pw.reloadMoveset();
                    pw.update(EnumUpdateType.Moveset);
                }
                for (StatusBase statusBase : pw.getStatuses()) {
                    statusBase.applyEndOfBattleEffect(pw);
                }
                if (!(p instanceof WildPixelmonParticipant) || pw.pokemon == null || !(pixelmon = pw.pokemon).getEntityData().hasKey("SynchronizedNature")) continue;
                pixelmon.setNature(EnumNature.getNatureFromIndex(pixelmon.getEntityData().getInteger("SynchronizedNature")));
                pixelmon.setPseudoNature(EnumNature.getNatureFromIndex(pixelmon.getEntityData().getInteger("SynchronizedNature")));
                pixelmon.getEntityData().removeTag("SynchronizedNature");
            }
            p.endBattle();
            if (cause == EnumBattleEndCause.FLEE) {
                results.put(p, BattleResults.FLEE);
                if (p instanceof PlayerParticipant) {
                    MinecraftForge.EVENT_BUS.post(new PlayerBattleEndedEvent(((PlayerParticipant)p).player, this, BattleResults.FLEE));
                }
            } else if ((cause != EnumBattleEndCause.FORCE || PixelmonConfig.forceEndBattleResult == EnumForceBattleResult.WINNER) && this.init && cause != EnumBattleEndCause.FORFEIT) {
                BattleResults result = BattleResults.DRAW;
                int allyCount = 0;
                int opponentCount = 0;
                for (BattleParticipant teamp : this.getTeam(p)) {
                    allyCount += teamp.countAblePokemon();
                }
                for (BattleParticipant enemyp : this.getOpponents(p)) {
                    opponentCount += enemyp.countAblePokemon();
                }
                if (allyCount > opponentCount) {
                    result = BattleResults.VICTORY;
                } else if (opponentCount > allyCount) {
                    result = BattleResults.DEFEAT;
                } else {
                    float allyPercent = 0.0f;
                    float opponentPercent = 0.0f;
                    int allyParticipants = 0;
                    int opponentParticipants = 0;
                    for (BattleParticipant teamp : this.getTeam(p)) {
                        allyPercent += teamp.countHealthPercent();
                        ++allyParticipants;
                    }
                    for (BattleParticipant enemyp : this.getOpponents(p)) {
                        opponentPercent += enemyp.countHealthPercent();
                        ++opponentParticipants;
                    }
                    if (allyParticipants == 0) {
                        allyParticipants = 1;
                    }
                    if (opponentParticipants == 0) {
                        opponentParticipants = 1;
                    }
                    allyPercent /= (float) allyParticipants;
                    opponentPercent /= (float) opponentParticipants;
                    result = allyPercent > opponentPercent ? BattleResults.VICTORY : (opponentPercent > allyPercent ? BattleResults.DEFEAT : BattleResults.DRAW);
                }
                results.put(p, result);
            } else if (cause == EnumBattleEndCause.FORCE) {
                if (PixelmonConfig.forceEndBattleResult == EnumForceBattleResult.DRAW) {
                    results.put(p, BattleResults.DRAW);
                } else {
                    if (PixelmonConfig.forceEndBattleResult == EnumForceBattleResult.ABNORMAL) {
                        if (p instanceof PlayerParticipant) {
                            MinecraftForge.EVENT_BUS.post(new PlayerBattleEndedAbnormalEvent(((PlayerParticipant)p).player, this));
                        }
                        abnormal = true;
                    }
                    results.put(p, BattleResults.DRAW);
                }
            }
            if (p instanceof PlayerParticipant) {
                MinecraftForge.EVENT_BUS.post(new PlayerBattleEndedEvent(((PlayerParticipant)p).player, this, results.get(p)));
                if (results.get(p) != BattleResults.VICTORY || cause != EnumBattleEndCause.NORMAL) continue;
                Pickup.pickupItem((PlayerParticipant)p);
                HoneyGather.pickupHoney((PlayerParticipant)p);
                continue;
            }
            if (!(p instanceof WildPixelmonParticipant)) continue;
            this.resetAggression(p);
        }
        this.spectators.forEach(spectator -> spectator.sendMessage(new EndSpectate()));
        if (this.playerNumber == 0) {
            BattleRegistry.deRegisterBattle(this);
        }
        this.checkEvolution();
        BattleEndEvent event = new BattleEndEvent(this, cause, abnormal, results);
        MinecraftForge.EVENT_BUS.post(event);
        if (PixelmonServerConfig.showIVsEVs) {
            this.participants.stream().forEach(bp -> {
                if (bp instanceof PlayerParticipant) {
                    EntityPlayerMP entityPlayer = ((PlayerParticipant)bp).player;
                    PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage(entityPlayer).get();
                    storage.sendUpdatedList();
                }
            });
        }
        return results;
    }

    public void setMusic(EnumBattleMusic musicId) {
        this.battleSongId = musicId;
        this.spectators.forEach(s -> s.sendMusicPacket(musicId));
        this.getPlayers().forEach(s -> s.sendMusicPacket(musicId));
    }

    public EnumBattleMusic getMusic() {
        return this.battleSongId;
    }

    @Deprecated
    public void endBattleWithoutXP(HashMap<BattleParticipant, BattleResults> results) {
        this.endBattle(EnumBattleEndCause.FORCE, results);
    }

    @Deprecated
    public void endBattle() {
        this.endBattle(EnumBattleEndCause.NORMAL);
    }

    @Deprecated
    public void endBattleWithoutXP() {
        this.endBattle(EnumBattleEndCause.FORCE);
    }

    private void checkEvolution() {
        for (PixelmonWrapper pw : this.checkEvolve) {
            pw.getLevel().tryEvolution();
        }
    }

    private void resetAggression(BattleParticipant p) {
        EntityLivingBase entity;
        if (p.getType() == ParticipantType.WildPokemon && (entity = p.getEntity()) != null && !entity.isDead) {
            ((EntityPixelmon)entity).aggressionTimer = RandomHelper.getRandomNumberBetween(400, 1000);
        }
    }

    public boolean isPvP() {
        for (BattleParticipant p : this.participants) {
            if (p instanceof PlayerParticipant) continue;
            return false;
        }
        return true;
    }

    public void pauseBattle() {
        this.paused = true;
    }

    public boolean isWaiting() {
        for (BattleParticipant p : this.participants) {
            if (!p.getWait()) continue;
            return true;
        }
        return false;
    }

    public void endPause() {
        this.paused = false;
    }

    private void takeTurn(PixelmonWrapper p) {
        if (this.tryFlee(p)) {
            return;
        }
        for (BattleParticipant part : this.participants) {
            if (part.bc != null) continue;
            this.endBattle();
            return;
        }
        p.takeTurn();
    }

    public boolean tryFlee(PixelmonWrapper p) {
        for (int i = 0; i < this.turn; ++i) {
            PixelmonWrapper current = this.turnList.get(i);
            if (!current.willTryFlee || current.getParticipant() != p.getParticipant()) continue;
            return false;
        }
        if (p.willTryFlee) {
            this.forfeitOrFlee(p);
            p.priority = 6.0f;
            return true;
        }
        return false;
    }

    private void forfeitOrFlee(PixelmonWrapper p) {
        boolean isForfeit = false;
        for (BattleParticipant participant : this.getOpponents(p.getParticipant())) {
            if (participant.getType() == ParticipantType.WildPokemon) continue;
            isForfeit = true;
            break;
        }
        if (isForfeit) {
            this.forfeitBattle(p);
        } else {
            this.calculateEscape(p);
        }
    }

    private void calculateEscape(PixelmonWrapper pixelmonWrapper) {
        PixelmonWrapper opponentPixelmon = pixelmonWrapper.bc.getOpponentPokemon(pixelmonWrapper.getParticipant()).get(0);
        double fleeingSpeed = pixelmonWrapper.getStats().Speed;
        double opponentSpeed = opponentPixelmon.getStats().Speed;
        double escapeAttempts = ++pixelmonWrapper.escapeAttempts;
        double calculatedFleeValue = fleeingSpeed * 128.0 / opponentSpeed + 30.0 * escapeAttempts;
        int random = RandomHelper.getRandomNumberBetween(1, 255);
        if (pixelmonWrapper.getBattleAbility() instanceof RunAway) {
            this.sendToAll("pixelmon.abilities.runaway", pixelmonWrapper.getNickname());
            this.endBattle(EnumBattleEndCause.FLEE);
        } else if (Lists.newArrayList((Object[])new EnumHeldItems[]{EnumHeldItems.smokeBall, EnumHeldItems.pokeDoll, EnumHeldItems.fluffyTail, EnumHeldItems.pokeToy}).contains((Object)pixelmonWrapper.getHeldItem().getHeldItemType())) {
            this.sendToAll("battlecontroller.escaped", pixelmonWrapper.getNickname());
            this.endBattle(EnumBattleEndCause.FLEE);
        } else if (calculatedFleeValue <= 255.0 && (double)random >= calculatedFleeValue) {
            this.sendToAll("battlecontroller.!escaped", pixelmonWrapper.getNickname());
            BattleParticipant fleeingParticipant = pixelmonWrapper.getParticipant();
            for (PixelmonWrapper pw : fleeingParticipant.controlledPokemon) {
                pixelmonWrapper.bc.battleLog.addEvent(new FleeAction(pixelmonWrapper.bc.battleTurn, pixelmonWrapper.bc.getPositionOfPokemon(pw), pixelmonWrapper));
            }
        } else {
            this.sendToAll("battlecontroller.escaped", pixelmonWrapper.getNickname());
            BattleParticipant fleeingParticipant = pixelmonWrapper.getParticipant();
            if (fleeingParticipant != null) {
                for (PixelmonWrapper pw : fleeingParticipant.controlledPokemon) {
                    pixelmonWrapper.bc.battleLog.addEvent(new FleeAction(pixelmonWrapper.bc.battleTurn, pixelmonWrapper.bc.getPositionOfPokemon(pw), pixelmonWrapper));
                }
            }
            this.endBattle(EnumBattleEndCause.FLEE);
        }
    }

    private void forfeitBattle(PixelmonWrapper pw) {
        if (this.rules.hasClause("forfeit")) {
            return;
        }
        BattleParticipant forfeitParticipant = pw.getParticipant();
        boolean tie = false;
        ArrayList<BattleParticipant> opponents = this.getOpponents(forfeitParticipant);
        block0: for (BattleParticipant opponent : opponents) {
            for (PixelmonWrapper opponentPokemon : opponent.controlledPokemon) {
                if (!opponentPokemon.willTryFlee) continue;
                tie = true;
                continue block0;
            }
        }
        if (tie) {
            this.sendToAll("battlecontroller.draw", new Object[0]);
        } else if (forfeitParticipant.getType() == ParticipantType.Player) {
            PlayerParticipant forfeitPlayer = (PlayerParticipant)forfeitParticipant;
            ChatHandler.sendBattleMessage(forfeitPlayer.player, "battlecontroller.forfeitself", new Object[0]);
            this.sendToOthers("battlecontroller.forfeit", forfeitPlayer, forfeitPlayer.getDisplayName());
        }
        HashMap<BattleParticipant, BattleResults> results = new HashMap<BattleParticipant, BattleResults>();
        for (BattleParticipant participant : this.participants) {
            BattleResults result = tie ? BattleResults.DRAW : (opponents.contains(participant) ? BattleResults.VICTORY : BattleResults.DEFEAT);
            results.put(participant, result);
        }
        this.endBattle(EnumBattleEndCause.FORFEIT, results);
    }

    private void checkPokemon() {
        boolean cameraSwitched = false;
        for (BattleParticipant p : this.participants) {
            p.resetMoveTimer();
            if (this.battleEnded || p.isDefeated) continue;
            this.checkReviveSendOut(p);
            ArrayList<PixelmonWrapper> faintedPokemon = new ArrayList<PixelmonWrapper>();
            for (PixelmonWrapper poke : p.controlledPokemon) {
                if (!poke.isFainted()) continue;
                if (p.getType() == ParticipantType.Player) {
                    Pixelmon.NETWORK.sendTo(new SwitchCamera(), (EntityPlayerMP)p.getEntity());
                    cameraSwitched = true;
                }
                if (poke.newPokemonID == null && this.turnList.contains(poke) && this.turn <= this.turnList.indexOf(poke)) {
                    this.turnList.remove(poke);
                }
                if (!poke.hasAwardedExp) {
                    Experience.awardExp(this.participants, p, poke);
                    poke.hasAwardedExp = true;
                    if (p instanceof WildPixelmonParticipant && this.participants.size() == 2) {
                        this.participants.stream().filter(part -> part != p && part instanceof PlayerParticipant).forEach(part -> {
                            MinecraftForge.EVENT_BUS.post(new BeatWildPixelmonEvent(((PlayerParticipant)part).player, (WildPixelmonParticipant)p));
                            ((PlayerParticipant)part).checkPlayerItems(p.allPokemon[0]);
                        });
                    }
                }
                poke.pokemon.setHealth(0.0f);
                poke.pokemon.setDead();
                poke.pokemon.isFainted = true;
                poke.pokemon.catchInPokeball();
                p.updatePokemon(poke);
                if (p.hasMorePokemonReserve()) continue;
                faintedPokemon.add(poke);
                this.updateRemovedPokemon(poke);
            }
            for (PixelmonWrapper pw : faintedPokemon) {
                if (!this.battleEnded) {
                    this.checkDefeated(p, pw);
                }
                if (this.battleEnded) continue;
                p.controlledPokemon.remove(pw);
                pw.pokemon = null;
            }
        }
        if (cameraSwitched) {
            this.spectators.forEach(spectator -> spectator.sendMessage(new SwitchCamera()));
        }
    }

    public void updateRemovedPokemon(PixelmonWrapper poke) {
        int[] id = poke.getPokemonID();
        this.participants.stream().filter(p2 -> p2 instanceof PlayerParticipant).forEach(p2 -> Pixelmon.NETWORK.sendTo(new SwitchOutPacket(id), ((PlayerParticipant)p2).player));
        this.spectators.forEach(spectator -> spectator.sendMessage(new SwitchOutPacket(id)));
    }

    public boolean isOneAlive(List<PixelmonWrapper> teamPokemon) {
        for (PixelmonWrapper pw : teamPokemon) {
            if (!pw.isAlive()) continue;
            return true;
        }
        return false;
    }

    public void checkDefeated(BattleParticipant p, PixelmonWrapper poke) {
        if (this.isOneAlive(p.controlledPokemon)) {
            p.isDefeated = false;
        } else if (p.countAblePokemon() == 0 && !p.isDefeated) {
            p.isDefeated = true;
            ChatHandler.sendBattleMessage(p.getEntity(), "battlecontroller.outofpokemon", new Object[0]);
            if (!this.isTeamDefeated(p)) {
                return;
            }
            this.participants.stream().filter(p2 -> this.getOpponents(p).contains(p2)).forEach(p2 -> ChatHandler.sendBattleMessage(p2.getEntity(), "battlecontroller.win", new Object[0]));
            this.endBattle();
        }
    }

    public void sendToAll(String string, Object ... data) {
        if (this.canSendMessages()) {
            ChatHandler.sendBattleMessage(this.participants, string, data);
        }
    }

    public void sendToAll(TextComponentTranslation message) {
        if (this.canSendMessages()) {
            ChatHandler.sendBattleMessage(this.participants, message);
        }
    }

    public void sendToOthers(String string, BattleParticipant battleParticipant, Object ... data) {
        if (this.canSendMessages()) {
            this.participants.stream().filter(p -> p != battleParticipant).forEach(p -> ChatHandler.sendBattleMessage(p.getEntity(), string, data));
            this.spectators.forEach(spectator -> ChatHandler.sendBattleMessage(spectator.getEntity(), string, data));
        }
    }

    public void sendToPlayer(EntityPlayer player, String string, Object ... data) {
        if (this.canSendMessages()) {
            ChatHandler.sendBattleMessage(player, string, data);
        }
    }

    public void sendToPlayer(EntityPlayer player, TextComponentTranslation message) {
        if (this.canSendMessages()) {
            ChatHandler.sendBattleMessage(player, message);
        }
    }

    private boolean canSendMessages() {
        return !this.simulateMode && this.sendMessages;
    }

    public void clearHurtTimer() {
        for (BattleParticipant part : this.participants) {
            for (PixelmonWrapper pokemon : part.controlledPokemon) {
                pokemon.pokemon.hurtTime = 0;
            }
        }
    }

    public void setUseItem(int[] pokemonID, EntityPlayer user, ItemStack usedStack, int additionalInfo) {
        this.participants.stream().filter(p -> p.getType() == ParticipantType.Player && p.getEntity() == user).forEach(p -> {
            for (PixelmonWrapper pw : p.controlledPokemon) {
                if (PixelmonMethods.isIDSame(pw, pokemonID)) {
                    pw.willUseItemInStack = usedStack;
                    pw.willUseItemInStackInfo = additionalInfo;
                    pw.wait = false;
                    continue;
                }
                if (!(usedStack.getItem() instanceof ItemPokeball)) continue;
                pw.wait = false;
                pw.attack = null;
            }
        });
    }

    public void setUseItem(int[] pokemonID, EntityPlayer player, ItemStack usedStack, int[] targetPokemonID) {
        for (BattleParticipant p : this.participants) {
            PixelmonWrapper pw;
            if (p.getType() != ParticipantType.Player || p.getEntity() != player || (pw = p.getPokemonFromID(pokemonID)) == null) continue;
            pw.willUseItemInStack = usedStack;
            pw.willUseItemPokemon = targetPokemonID;
            pw.willUseItemInStackInfo = -1;
            pw.wait = false;
        }
    }

    public void switchPokemon(int[] switchingPokemonID, int[] newPokemonID, boolean switchInstantly) {
        PixelmonWrapper pw = this.getPokemonFromID(switchingPokemonID);
        if (pw == null) {
            return;
        }
        BattleParticipant p = pw.getParticipant();
        pw.isSwitching = true;
        pw.newPokemonID = newPokemonID;
        boolean stopWait = true;
        boolean checkFaint = false;
        if (pw.isFainted()) {
            p.switchingIn.add(newPokemonID);
            if (!this.switchingOut.isEmpty()) {
                this.switchingOut.remove(pw);
                if (this.switchingOut.isEmpty()) {
                    for (BattleParticipant participant : this.participants) {
                        participant.switchAllFainted();
                        participant.wait = false;
                    }
                    checkFaint = true;
                } else {
                    stopWait = false;
                }
            }
        } else if (switchInstantly) {
            pw.doSwitch();
        }
        if (checkFaint) {
            this.checkPokemon();
            this.checkFaint();
        }
        if (stopWait) {
            pw.wait = false;
            p.wait = false;
        }
    }

    public void setFlee(int[] fleeingID) {
        for (BattleParticipant p : this.participants) {
            PixelmonWrapper pw = p.getPokemonFromID(fleeingID);
            if (pw == null) continue;
            if (pw.isFainted() && p.getType() == ParticipantType.Player) {
                this.forfeitOrFlee(pw);
                if (this.battleEnded) continue;
                Pixelmon.NETWORK.sendTo(new FailFleeSwitch(), ((PlayerParticipant)p).player);
                continue;
            }
            p.wait = false;
            for (PixelmonWrapper pw2 : p.controlledPokemon) {
                pw2.willTryFlee = true;
                pw2.wait = false;
            }
        }
    }

    public ParticipantType[][] getBattleType(BattleParticipant teammate) {
        int i;
        ParticipantType[][] type = new ParticipantType[2][];
        ArrayList<ParticipantType> team1 = new ArrayList<ParticipantType>();
        ArrayList<ParticipantType> team2 = new ArrayList<ParticipantType>();
        for (BattleParticipant p : this.participants) {
            if (p.team == teammate.team) {
                team1.add(p.getType());
                continue;
            }
            team2.add(p.getType());
        }
        type[0] = new ParticipantType[team1.size()];
        for (i = 0; i < team1.size(); ++i) {
            type[0][i] = (ParticipantType)((Object)team1.get(i));
        }
        type[1] = new ParticipantType[team2.size()];
        for (i = 0; i < team2.size(); ++i) {
            type[1][i] = (ParticipantType)((Object)team2.get(i));
        }
        return type;
    }

    public void updatePokemonHealth(EntityPixelmon entityPixelmon) {
        if (this.init) {
            this.participants.stream().filter(p -> p instanceof PlayerParticipant).forEach(p -> ((PlayerParticipant)p).updatePokemonHealth(entityPixelmon));
        }
    }

    public ArrayList<BattleParticipant> getOpponents(BattleParticipant participant) {
        return (ArrayList)this.participants.stream().filter(p -> p.team != participant.team).collect(Collectors.toList());
    }

    public ArrayList<BattleParticipant> getTeam(BattleParticipant participant) {
        return participant == null ? new ArrayList() : (ArrayList)this.participants.stream().filter(p -> p.team == participant.team).collect(Collectors.toList());
    }

    public ArrayList<PixelmonWrapper> getActivePokemon() {
        ArrayList<PixelmonWrapper> activePokemon = new ArrayList<PixelmonWrapper>();
        for (BattleParticipant p : this.participants) {
            activePokemon.addAll(p.controlledPokemon);
        }
        return activePokemon;
    }

    public ArrayList<PixelmonWrapper> getActiveUnfaintedPokemon() {
        return (ArrayList)this.getActivePokemon().stream().filter(pw -> !pw.isFainted()).collect(Collectors.toList());
    }

    public ArrayList<PixelmonWrapper> getActiveFaintedPokemon() {
        return (ArrayList)this.getActivePokemon().stream().filter(pw -> pw.isFainted()).collect(Collectors.toList());
    }

    public ArrayList<PixelmonWrapper> getAdjacentPokemon(PixelmonWrapper pokemon) {
        return (ArrayList)this.getActiveUnfaintedPokemon().stream().filter(pw -> Math.abs(pw.battlePosition - pokemon.battlePosition) <= 1 && pw != pokemon).collect(Collectors.toList());
    }

    public ArrayList<PixelmonWrapper> getTeamPokemon(BattleParticipant participant) {
        ArrayList<BattleParticipant> team = this.getTeam(participant);
        ArrayList<PixelmonWrapper> teamPokemon = new ArrayList<PixelmonWrapper>();
        for (BattleParticipant p : team) {
            for (PixelmonWrapper pw : p.controlledPokemon) {
                if (pw == null) continue;
                teamPokemon.add(pw);
            }
        }
        return teamPokemon;
    }

    public ArrayList<PixelmonWrapper> getTeamPokemon(PixelmonWrapper pokemon) {
        return this.getTeamPokemon(pokemon.getParticipant());
    }

    public ArrayList<PixelmonWrapper> getTeamPokemon(EntityPixelmon pokemon) {
        return this.getTeamPokemon(pokemon.getParticipant());
    }

    public ArrayList<PixelmonWrapper> getTeamPokemonExcludeSelf(PixelmonWrapper pokemon) {
        ArrayList<PixelmonWrapper> teamPokemon = this.getTeamPokemon(pokemon);
        teamPokemon.remove(pokemon);
        return teamPokemon;
    }

    public ArrayList<PixelmonWrapper> getOpponentPokemon(BattleParticipant participant) {
        ArrayList<BattleParticipant> opponents = this.getOpponents(participant);
        ArrayList<PixelmonWrapper> opponentPokemon = new ArrayList<PixelmonWrapper>();
        for (BattleParticipant p : opponents) {
            opponentPokemon.addAll(p.controlledPokemon);
        }
        return opponentPokemon;
    }

    public ArrayList<PixelmonWrapper> getOpponentPokemon(PixelmonWrapper pw) {
        return this.getOpponentPokemon(pw.getParticipant());
    }

    public boolean isInBattle(PixelmonWrapper pokemon) {
        for (PixelmonWrapper pw : this.getActivePokemon()) {
            if (!pw.equals(pokemon)) continue;
            return true;
        }
        return false;
    }

    public BattleParticipant getParticipantForEntity(EntityLivingBase entity) {
        for (BattleParticipant p : this.participants) {
            if (p.getEntity() != entity) continue;
            return p;
        }
        return null;
    }

    public void sendDamagePacket(PixelmonWrapper target, int damage) {
        for (BattleParticipant p : this.participants) {
            p.sendDamagePacket(target, damage);
        }
        this.spectators.forEach(spectator -> spectator.sendDamagePacket(target, damage));
    }

    public void sendHealPacket(PixelmonWrapper target, int amount) {
        for (BattleParticipant p : this.participants) {
            p.sendHealPacket(target, amount);
        }
        this.spectators.forEach(spectator -> spectator.sendHealPacket(target, amount));
    }

    public PixelmonWrapper getOppositePokemon(PixelmonWrapper pw) {
        int index;
        ArrayList<PixelmonWrapper> oppPokemon = pw.bc.getOpponentPokemon(pw.getParticipant());
        ArrayList<PixelmonWrapper> teamPokemon = pw.bc.getTeamPokemon(pw.getParticipant());
        for (index = teamPokemon.indexOf(pw); index >= oppPokemon.size(); --index) {
        }
        return oppPokemon.get(index);
    }

    public PixelmonWrapper getPokemonAtPosition(int position) {
        ArrayList<PixelmonWrapper> arr = new ArrayList<PixelmonWrapper>();
        for (BattleParticipant bp : this.participants) {
            arr.addAll(bp.controlledPokemon);
        }
        return position >= arr.size() ? null : (PixelmonWrapper)arr.get(position);
    }

    public int getPositionOfPokemon(PixelmonWrapper poke) {
        ArrayList<PixelmonWrapper> arr = new ArrayList<PixelmonWrapper>();
        for (BattleParticipant bp : this.participants) {
            arr.addAll(bp.controlledPokemon);
        }
        return arr.indexOf(poke);
    }

    public int getPositionOfPokemon(PixelmonWrapper poke, BattleParticipant bp) {
        return bp.controlledPokemon.indexOf(poke);
    }

    public BattleStage getStage() {
        return this.stage;
    }

    public void enableReturnHeldItems(PixelmonWrapper attacker, PixelmonWrapper target) {
        BattleParticipant targetParticipant = target.getParticipant();
        if (!this.simulateMode && !(targetParticipant instanceof WildPixelmonParticipant)) {
            target.enableReturnHeldItem();
            attacker.enableReturnHeldItem();
        }
    }

    public boolean checkValid() {
        if (this.battleEnded) {
            return false;
        }
        ArrayList<PixelmonWrapper> pokemon = new ArrayList<PixelmonWrapper>();
        ArrayList<PixelmonWrapper> activePokemon = this.getActivePokemon();
        if (activePokemon.size() <= 1) {
            return false;
        }
        for (PixelmonWrapper pw : activePokemon) {
            if (pokemon.contains(pw)) {
                this.endBattleWithoutXP();
                return false;
            }
            pokemon.add(pw);
        }
        return true;
    }

    public PlayerParticipant getPlayer(String name) {
        for (BattleParticipant p : this.participants) {
            if (p.getType() != ParticipantType.Player) continue;
            PlayerParticipant player = (PlayerParticipant)p;
            if (!player.player.getDisplayNameString().equals(name)) continue;
            return player;
        }
        return null;
    }

    public PlayerParticipant getPlayer(EntityPlayer player) {
        for (BattleParticipant p : this.participants) {
            if (p.getType() != ParticipantType.Player) continue;
            PlayerParticipant currentPlayer = (PlayerParticipant)p;
            if (player != currentPlayer.player) continue;
            return currentPlayer;
        }
        return null;
    }

    public List<PlayerParticipant> getPlayers() {
        ArrayList<PlayerParticipant> players = new ArrayList<PlayerParticipant>(this.participants.size());
        for (BattleParticipant p : this.participants) {
            if (p.getType() != ParticipantType.Player) continue;
            players.add((PlayerParticipant)p);
        }
        return players;
    }

    public boolean isTeamDefeated(BattleParticipant participant) {
        for (BattleParticipant p2 : this.getTeam(participant)) {
            if (p2.isDefeated) continue;
            return false;
        }
        return true;
    }

    public int getTurnForPokemon(PixelmonWrapper pokemon) {
        for (int i = 0; i < this.turnList.size(); ++i) {
            if (this.turnList.get(i) != pokemon) continue;
            return i;
        }
        return -1;
    }

    public BattleParticipant otherParticipant(BattleParticipant participant) {
        for (BattleParticipant p : this.participants) {
            if (p == participant) continue;
            return p;
        }
        return null;
    }

    public PixelmonWrapper getFirstMover(PixelmonWrapper ... pokemonList) {
        for (PixelmonWrapper turnPokemon : this.turnList) {
            for (PixelmonWrapper pokemon : pokemonList) {
                if (turnPokemon != pokemon) continue;
                return pokemon;
            }
        }
        return null;
    }

    public PixelmonWrapper getFirstMover(ArrayList<PixelmonWrapper> pokemonList) {
        for (PixelmonWrapper turnPokemon : this.turnList) {
            for (PixelmonWrapper pokemon : pokemonList) {
                if (turnPokemon != pokemon) continue;
                return pokemon;
            }
        }
        return null;
    }

    public PixelmonWrapper getLastMover(PixelmonWrapper ... pokemonList) {
        for (int i = this.turnList.size() - 1; i >= 0; --i) {
            PixelmonWrapper turnPokemon = this.turnList.get(i);
            for (PixelmonWrapper pokemon : pokemonList) {
                if (turnPokemon != pokemon) continue;
                return pokemon;
            }
        }
        return null;
    }

    public PixelmonWrapper getLastMover(ArrayList<PixelmonWrapper> pokemonList) {
        for (int i = this.turnList.size() - 1; i >= 0; --i) {
            PixelmonWrapper turnPokemon = this.turnList.get(i);
            for (PixelmonWrapper pokemon : pokemonList) {
                if (turnPokemon != pokemon) continue;
                return pokemon;
            }
        }
        return null;
    }

    public void sendSwitchPacket(int[] oldID, PixelmonWrapper newPokemon) {
        for (BattleParticipant participant : this.participants) {
            if (!(participant instanceof PlayerParticipant)) continue;
            Pixelmon.NETWORK.sendTo(new SwitchOutPacket(oldID, newPokemon), ((PlayerParticipant)participant).player);
        }
        this.spectators.forEach(spectator -> spectator.sendMessage(new SwitchOutPacket(oldID, newPokemon)));
    }

    public void addSpectator(Spectator spectator) {
        this.spectators.add(spectator);
        spectator.sendMusicPacket(this.battleSongId);
        BattleRegistry.registerSpectator(spectator, this);
    }

    public void removeSpectator(Spectator spectator) {
        this.spectators.remove(spectator);
        BattleRegistry.unregisterSpectator(spectator);
        spectator.sendMusicPacket(EnumBattleMusic.None);
        MinecraftForge.EVENT_BUS.post(new SpectateEvent.StopSpectate(spectator.getEntity(), this));
    }

    public boolean hasSpectator(EntityPlayer player) {
        for (Spectator spectator : this.spectators) {
            if (spectator.getEntity() != player) continue;
            return true;
        }
        return false;
    }

    public boolean removeSpectator(EntityPlayerMP player) {
        for (int i = 0; i < this.spectators.size(); ++i) {
            Spectator spectator = this.spectators.get(i);
            if (spectator.getEntity() != player) continue;
            this.spectators.remove(i);
            spectator.sendMusicPacket(EnumBattleMusic.None);
            BattleRegistry.unregisterSpectator(spectator);
            MinecraftForge.EVENT_BUS.post(new SpectateEvent.StopSpectate(player, this));
            return true;
        }
        return false;
    }

    public ArrayList<Spectator> getPlayerSpectators(PlayerParticipant player) {
        ArrayList<Spectator> playerSpectators = new ArrayList<Spectator>(this.spectators.size());
        String playerName = player.player.getDisplayNameString();
        playerSpectators.addAll(this.spectators.stream().filter(spectator -> spectator.watchedName.equals(playerName)).collect(Collectors.toList()));
        return playerSpectators;
    }

    public PixelmonWrapper getPokemonFromID(int[] id) {
        BattleParticipant p;
        PixelmonWrapper pw = null;
        Iterator<BattleParticipant> iterator = this.participants.iterator();
        while (iterator.hasNext() && (pw = (p = iterator.next()).getPokemonFromID(id)) == null) {
        }
        return pw;
    }

    public List<PixelmonWrapper> getDefaultTurnOrder() {
        return CalcPriority.getDefaultTurnOrder(this);
    }

    public void removeFromTurnList(PixelmonWrapper pw) {
        for (int i = this.turn + 1; i < this.turnList.size(); ++i) {
            if (!pw.equals(this.turnList.get(i))) continue;
            this.turnList.remove(i);
            return;
        }
    }

    public boolean isLastMover() {
        return this.turn >= this.turnList.size() - 1;
    }

    public boolean containsParticipantType(Class<? extends BattleParticipant> participantType) {
        for (BattleParticipant bp : this.participants) {
            if (bp.getClass() != participantType) continue;
            return true;
        }
        return false;
    }

    public void updateFormChange(EntityPixelmon pokemon) {
        this.participants.stream().filter(participant -> participant.getType() == ParticipantType.Player).forEach(participant -> Pixelmon.NETWORK.sendTo(new FormBattleUpdate(pokemon.getPokemonId(), pokemon.getFormIncludeTransformed()), ((PlayerParticipant)participant).player));
        this.spectators.forEach(spectator -> spectator.sendMessage(new FormBattleUpdate(pokemon.getPokemonId(), pokemon.getFormIncludeTransformed())));
    }

    public boolean isLevelingDisabled() {
        if (!PixelmonConfig.allowPVPExperience) {
            boolean allPlayers = true;
            for (BattleParticipant p : this.participants) {
                if (p.getType() == ParticipantType.Player) continue;
                allPlayers = false;
            }
            if (allPlayers) {
                return true;
            }
        }
        if (!PixelmonConfig.allowTrainerExperience) {
            for (BattleParticipant p : this.participants) {
                if (p.getType() != ParticipantType.Trainer) continue;
                return true;
            }
        }
        return this.rules.raiseToCap;
    }

    public void addCheckEvolve(PokemonLink pokemon) {
        if (pokemon instanceof WrapperLink) {
            WrapperLink link = (WrapperLink)pokemon;
            this.checkEvolve.add(link.getPokemon());
        }
    }

    public boolean isHordeBattle() {
        return this.hordeBattle;
    }

    public void setHordeBattle(boolean hordeBattle) {
        this.hordeBattle = hordeBattle;
    }

    public boolean isSosBattle() {
        return this.rules.battleType == EnumBattleType.SOS;
    }

    public void setSosBattle(boolean sosBattle) {
        this.sosBattle = sosBattle;
    }

    public int getWildPokemonCount() {
        return (int)this.participants.stream().filter(bp -> bp instanceof WildPixelmonParticipant).count();
    }
}

