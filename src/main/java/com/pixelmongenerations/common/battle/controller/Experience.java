/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller;

import com.pixelmongenerations.api.events.player.PlayerBattleGainEvent;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.entity.pixelmon.stats.EVsStore;
import com.pixelmongenerations.common.entity.pixelmon.stats.Level;
import com.pixelmongenerations.common.entity.pixelmon.stats.Stats;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.Evolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.LevelingEvolution;
import com.pixelmongenerations.common.item.ItemExpAll;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.EVAdjusting;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumExpSource;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.network.EnumUpdateType;
import java.util.ArrayList;
import java.util.Set;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.MinecraftForge;

public final class Experience {
    public static void awardExp(ArrayList<BattleParticipant> participants, BattleParticipant losingTeamOwner, PixelmonWrapper faintedPokemon) {
        if (faintedPokemon == null || faintedPokemon.bc.isLevelingDisabled()) {
            return;
        }
        for (BattleParticipant teamOwner : participants) {
            if (teamOwner.team == losingTeamOwner.team || !(teamOwner instanceof PlayerParticipant)) continue;
            PlayerParticipant player = (PlayerParticipant)teamOwner;
            Set<PixelmonWrapper> attackers = faintedPokemon.getAttackers();
            for (PixelmonWrapper pw : attackers) {
                if (pw.getParticipant() != teamOwner) continue;
                Experience.calcExp(faintedPokemon, pw, 1.0);
            }
            boolean hasExpAll = false;
            ArrayList<ItemStack> items = new ArrayList<ItemStack>();
            items.addAll(player.player.inventory.mainInventory);
            items.addAll(player.player.inventory.offHandInventory);
            for (ItemStack item : items) {
                NBTTagCompound nbt;
                if (item == null || !(item.getItem() instanceof ItemExpAll) || !item.hasTagCompound() || !(nbt = item.getTagCompound()).getBoolean("Activated")) continue;
                hasExpAll = true;
                break;
            }
            for (PixelmonWrapper pw : teamOwner.allPokemon) {
                if (attackers.contains(pw)) continue;
                boolean getsExp = hasExpAll || pw.getHeldItem().getHeldItemType() == EnumHeldItems.expShare;
                if (!getsExp) continue;
                Experience.calcExp(faintedPokemon, pw, 0.5);
            }
            player.givePlayerExp(faintedPokemon);
        }
    }

    private static void calcExp(PixelmonWrapper faintedPokemon, PixelmonWrapper expReceiver, double scaleFactor) {
        if (expReceiver.isFainted()) {
            return;
        }
        ItemHeld heldItem = expReceiver.getInitialHeldItem();
        EnumHeldItems heldItemType = heldItem.getHeldItemType();
        Level level = expReceiver.getLevel();
        EVsStore evStore = faintedPokemon.getBaseStats().evGain.cloneEVs();
        if (heldItemType == EnumHeldItems.evAdjusting) {
            EVAdjusting item = (EVAdjusting)heldItem;
            if (item.type.statAffected == StatsType.None) {
                evStore.doubleValues();
            } else {
                evStore.addEVs(item.type.statAffected, 4);
            }
        }
        if (expReceiver.getPokeRus() > 0) {
            evStore.doubleValues();
        }
        Stats stats = expReceiver.getStats();
        if (expReceiver.getLevelNum() > expReceiver.bc.rules.levelCap) {
            return;
        }
        double a = 1.0;
        if (faintedPokemon.getParticipant().getType() != ParticipantType.WildPokemon) {
            a = 1.5;
        }
        double charmMultiplier = 1.0;
        if (expReceiver.getParticipant() instanceof PlayerParticipant) {
            EntityPlayerMP player = ((PlayerParticipant)expReceiver.getParticipant()).player;
            for (ItemStack itemStack : player.inventory.mainInventory) {
                if (itemStack.getItem() != PixelmonItems.expCharm) continue;
                charmMultiplier = 1.5;
                break;
            }
        }
        double t = expReceiver.getOriginalTrainer().equals(expReceiver.getParticipant().getDisplayName()) ? 1.0 : 1.5;
        double baseExp = faintedPokemon.getBaseStats().baseExp;
        double eggMultiplier = heldItemType == EnumHeldItems.luckyEgg ? 1.5 : 1.0;
        double faintedLevel = faintedPokemon.getLevelNum();
        double exp = a * t * baseExp * charmMultiplier * eggMultiplier * faintedLevel * (scaleFactor *= (double)PixelmonConfig.expModifier) / 7.0;
        for (Evolution e : expReceiver.getBaseStats().evolutions) {
            if (e == null || !(e instanceof LevelingEvolution) || level.getLevel() < ((LevelingEvolution)e).level) continue;
            exp *= 1.2;
            break;
        }
        if (expReceiver.getParticipant() instanceof PlayerParticipant) {
            PlayerBattleGainEvent event = new PlayerBattleGainEvent(expReceiver, (int)exp, evStore);
            MinecraftForge.EVENT_BUS.post(event);
            if (event.isCanceled()) {
                return;
            }
            stats.EVs.gainEV(event.getEVGain());
            int beforeHP = stats.HP;
            stats.setLevelStats(expReceiver.getNature(), expReceiver.getPseudoNature(), expReceiver.getBaseStats(), level.getLevel());
            if (stats.HP > beforeHP) {
                expReceiver.setHealth((int)Math.ceil((double)expReceiver.getHealth() / (double)beforeHP * (double)stats.HP));
                expReceiver.updateHPIncrease();
            }
            expReceiver.update(EnumUpdateType.HP, EnumUpdateType.Stats);
            level.awardEXP(event.getExp(), true, EnumExpSource.PlayerBattle);
        } else {
            level.awardEXP((int)exp);
        }
    }
}

