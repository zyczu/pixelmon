/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.ai;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.CalcPriority;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.ai.OpponentMemory;
import com.pixelmongenerations.common.battle.controller.ai.TacticalAI;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.log.MoveResults;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.List;

public abstract class BattleAIBase {
    BattleParticipant participant;
    BattleControllerBase bc;

    public BattleAIBase(BattleParticipant participant) {
        this.participant = participant;
        this.bc = participant.bc;
    }

    public abstract MoveChoice getNextMove(PixelmonWrapper var1);

    public abstract int[] getNextSwitch(PixelmonWrapper var1);

    public void registerMove(PixelmonWrapper user) {
    }

    public void registerSwitch(PixelmonWrapper switchOut, PixelmonWrapper switchIn) {
    }

    protected ArrayList<MoveChoice> getChoices(PixelmonWrapper pw) {
        ArrayList<MoveChoice> choices = new ArrayList<MoveChoice>();
        if (pw != null) {
            choices.addAll(this.getAttackChoices(pw));
        }
        choices.addAll(this.getSwitchChoices(pw));
        return choices;
    }

    protected ArrayList<MoveChoice> getAttackChoices(PixelmonWrapper pw) {
        ArrayList<MoveChoice> choices = new ArrayList<MoveChoice>();
        List<Attack> moveset = this.getMoveset(pw);
        for (Attack a : moveset) {
            if (a == null || a.pp <= 0 || a.getDisabled()) continue;
            a.createMoveChoices(pw, choices, true);
        }
        if (choices.isEmpty()) {
            ArrayList<PixelmonWrapper> targets = new ArrayList<PixelmonWrapper>();
            targets.addAll(this.bc.getAdjacentPokemon(pw));
            targets.addAll(this.bc.getOpponentPokemon(this.participant));
            Attack struggle = new Attack("Struggle");
            struggle.createMoveChoices(pw, choices, false);
        }
        return choices;
    }

    protected ArrayList<MoveChoice> getAttackChoicesOpponentOnly(PixelmonWrapper pw) {
        ArrayList<MoveChoice> choices = this.getAttackChoices(pw);
        ArrayList<PixelmonWrapper> allies = this.bc.getTeamPokemonExcludeSelf(pw);
        if (!allies.isEmpty()) {
            for (int i = 0; i < choices.size(); ++i) {
                MoveChoice choice = choices.get(i);
                if (choice.targets.size() != 1 || !allies.contains(choice.targets.get(0))) continue;
                choices.remove(i--);
            }
        }
        return choices;
    }

    public ArrayList<MoveChoice> getSwitchChoices(PixelmonWrapper pw) {
        ArrayList<MoveChoice> switchChoices = new ArrayList<MoveChoice>();
        if (pw == null || BattleParticipant.canSwitch(pw)[0]) {
            List<int[]> switchIDs = this.getPossibleSwitchIDs();
            for (int[] switchID : switchIDs) {
                switchChoices.add(new MoveChoice(pw, switchID));
            }
        }
        return switchChoices;
    }

    protected List<int[]> getPossibleSwitchIDs() {
        ArrayList<int[]> party = new ArrayList<int[]>();
        for (PixelmonWrapper pw : this.participant.allPokemon) {
            if (!pw.isAlive() || pw.pokemon != null) continue;
            if (pw.switchedThisTurn || pw.isFainted()) {
                party.remove(pw.getPokemonID());
            }
            party.add(pw.getPokemonID());
        }
        block1: for (int[] switchingInID : this.participant.switchingIn) {
            for (int i = 0; i < party.size(); ++i) {
                if (!PixelmonMethods.isIDSame(party.get(i), switchingInID)) continue;
                party.remove(i);
                continue block1;
            }
        }
        return party;
    }

    protected MoveChoice getRandomAttackChoice(PixelmonWrapper pw) {
        return RandomHelper.getRandomElementFromList(this.getAttackChoicesOpponentOnly(pw));
    }

    protected void weightOffensiveMove(PixelmonWrapper pw, MoveChoice choice) {
        this.weightOffensiveMove(pw, choice, pw.getTeamPokemonExcludeSelf());
    }

    protected void weightOffensiveMove(PixelmonWrapper pw, MoveChoice choice, ArrayList<PixelmonWrapper> allies) {
        Attack saveAttack = pw.attack;
        List<PixelmonWrapper> saveTargets = pw.targets;
        pw.setAttack(choice.attack, choice.targets, false, false);
        for (int j = 0; j < choice.targets.size(); ++j) {
            PixelmonWrapper target = choice.targets.get(j);
            MoveResults result = new MoveResults(target);
            result.priority = CalcPriority.calculatePriority(pw);
            choice.result = result;
            choice.attack.saveAttack();
            choice.attack.use(pw, target, result);
            if (result.result == AttackResult.charging) {
                choice.attack.use(pw, target, result);
            }
            choice.attack.restoreAttack();
            if (allies.contains(target)) {
                if (result.damage >= target.getHealth()) {
                    --choice.tier;
                }
                choice.weight -= target.getHealthPercent(result.damage);
                if (choice.result.weightMod >= 0.0f) continue;
                choice.raiseWeight(-choice.result.weightMod);
                choice.result.result = AttackResult.hit;
                continue;
            }
            choice.raiseWeight(choice.result.weightMod);
            if (result.damage >= target.getHealth()) {
                choice.tier = 3;
                int effectiveAccuracy = result.accuracy;
                if (effectiveAccuracy < 0) {
                    effectiveAccuracy = 100;
                }
                choice.weight += (float)effectiveAccuracy;
                continue;
            }
            if (result.damage <= 0) continue;
            choice.raiseTier(2);
            choice.weight += target.getHealthPercent(result.damage);
        }
        pw.attack = saveAttack;
        pw.targets = saveTargets;
    }

    protected void weightOffensiveMoves(PixelmonWrapper pw, ArrayList<MoveChoice> choices) {
        for (MoveChoice choice : choices) {
            if (!choice.isOffensiveMove()) continue;
            this.weightOffensiveMove(pw, choice);
        }
    }

    protected ArrayList<MoveChoice> getWeightedOffensiveChoices(PixelmonWrapper pw) {
        ArrayList<MoveChoice> choices = this.getAttackChoicesOpponentOnly(pw);
        ArrayList<PixelmonWrapper> allies = this.bc.getTeamPokemonExcludeSelf(pw);
        for (MoveChoice choice : choices) {
            if (choice.attack.getAttackCategory() == AttackCategory.Status) continue;
            this.weightOffensiveMove(pw, choice, allies);
        }
        return choices;
    }

    public ArrayList<MoveChoice> getBestAttackChoices(PixelmonWrapper pw) {
        ArrayList<MoveChoice> choices = this.getAttackChoicesOpponentOnly(pw);
        ArrayList<PixelmonWrapper> allies = pw.getTeamPokemonExcludeSelf();
        ArrayList<MoveChoice> bestChoices = new ArrayList<MoveChoice>();
        for (MoveChoice choice : choices) {
            if (choice.attack.getAttackCategory() == AttackCategory.Status) continue;
            this.weightOffensiveMove(pw, choice, allies);
            MoveChoice.checkBestChoice(choice, bestChoices);
        }
        return bestChoices;
    }

    public ArrayList<ArrayList<MoveChoice>> getBestAttackChoices(ArrayList<PixelmonWrapper> pokemonList) {
        ArrayList<ArrayList<MoveChoice>> choices = new ArrayList<ArrayList<MoveChoice>>(pokemonList.size());
        for (PixelmonWrapper pw : pokemonList) {
            ArrayList<MoveChoice> after = this.getBestAttackChoices(pw);
            choices.add(after);
        }
        return choices;
    }

    public List<Attack> getMoveset(PixelmonWrapper pw) {
        if (this.participant.getOpponentPokemon().contains(pw) && this instanceof TacticalAI) {
            OpponentMemory memory = ((TacticalAI)this).getMemory(pw);
            return memory.getAttacks();
        }
        ArrayList<Attack> moveset = new ArrayList<Attack>(4);
        for (Attack attack : pw.getMoveset()) {
            if (attack == null) continue;
            moveset.add(attack);
        }
        return moveset;
    }

    public void weightRandomMove(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> possibleChoices) {
        if (possibleChoices.isEmpty()) {
            return;
        }
        float totalWeight = 0.0f;
        boolean hasKO = false;
        this.weightOffensiveMoves(pw, possibleChoices);
        for (MoveChoice possibleChoice : possibleChoices) {
            if (possibleChoice.tier >= 3) {
                hasKO = true;
            }
            totalWeight += possibleChoice.weight;
        }
        userChoice.raiseWeight(totalWeight / (float)possibleChoices.size());
        if (userChoice.weight >= 50.0f && hasKO) {
            userChoice.raiseTier(3);
        }
    }

    public void weightFromUserOptions(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> bestChoicesBefore, ArrayList<MoveChoice> bestChoicesAfter) {
        this.weightFromUserOptions(pw, userChoice, bestChoicesBefore, bestChoicesAfter, true);
    }

    public void weightFromUserOptions(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> bestChoicesBefore, ArrayList<MoveChoice> bestChoicesAfter, boolean weightNegative) {
        if (bestChoicesBefore == null || bestChoicesAfter == null || bestChoicesBefore.isEmpty() || bestChoicesAfter.isEmpty()) {
            return;
        }
        MoveChoice choiceBefore = bestChoicesBefore.get(0);
        MoveChoice choiceAfter = bestChoicesAfter.get(0);
        float healthBefore = 0.0f;
        float healthAfter = 0.0f;
        ArrayList<PixelmonWrapper> allies = pw.bc.getTeamPokemonExcludeSelf(pw);
        for (PixelmonWrapper target : choiceBefore.targets) {
            if (allies.contains(target)) continue;
            healthBefore += target.getHealthPercent();
        }
        for (PixelmonWrapper target : choiceAfter.targets) {
            if (allies.contains(target)) continue;
            healthAfter += target.getHealthPercent();
        }
        float damageBefore = choiceBefore.tier >= 3 ? healthBefore : choiceBefore.weight;
        float damageAfter = choiceAfter.tier >= 3 ? healthAfter : choiceAfter.weight;
        if (healthBefore != 0.0f && healthAfter != 0.0f && damageBefore != 0.0f && damageAfter != 0.0f) {
            double koTurnsBefore = Math.ceil(healthBefore / damageBefore);
            double koTurnsAfter = Math.ceil(healthAfter / damageAfter);
            if (koTurnsBefore > koTurnsAfter) {
                userChoice.raiseWeight((1.0f - (float)(koTurnsAfter / koTurnsBefore)) * 100.0f);
            } else if (koTurnsBefore < koTurnsAfter) {
                userChoice.weight -= (1.0f - (float)(koTurnsBefore / koTurnsAfter)) * 100.0f;
            }
        }
    }

    public void weightFromOpponentOptions(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<ArrayList<MoveChoice>> bestChoicesBefore, ArrayList<ArrayList<MoveChoice>> bestChoicesAfter) {
        this.weightFromOpponentOptions(pw, userChoice, bestChoicesBefore, bestChoicesAfter, true);
    }

    public void weightFromOpponentOptions(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<ArrayList<MoveChoice>> bestChoicesBefore, ArrayList<ArrayList<MoveChoice>> bestChoicesAfter, boolean weightNegative) {
        if (bestChoicesBefore == null || bestChoicesAfter == null || bestChoicesBefore.size() != bestChoicesAfter.size()) {
            return;
        }
        float totalHealthBefore = 0.0f;
        float totalHealthAfter = 0.0f;
        float damageBefore = 0.0f;
        float damageAfter = 0.0f;
        for (int i = 0; i < bestChoicesBefore.size(); ++i) {
            ArrayList<MoveChoice> bestChoicesBeforeSingle = bestChoicesBefore.get(i);
            ArrayList<MoveChoice> bestChoicesAfterSingle = bestChoicesAfter.get(i);
            if (bestChoicesBeforeSingle.isEmpty() || bestChoicesAfterSingle.isEmpty()) continue;
            MoveChoice choiceBefore = bestChoicesBeforeSingle.get(0);
            MoveChoice choiceAfter = bestChoicesAfterSingle.get(0);
            float healthBefore = 0.0f;
            float healthAfter = 0.0f;
            ArrayList<PixelmonWrapper> allies = pw.bc.getTeamPokemonExcludeSelf(choiceBefore.user);
            for (PixelmonWrapper target : choiceBefore.targets) {
                if (allies.contains(target)) continue;
                healthBefore += target.getHealthPercent();
            }
            for (PixelmonWrapper target : choiceAfter.targets) {
                if (allies.contains(target)) continue;
                healthAfter += target.getHealthPercent();
            }
            totalHealthBefore += healthBefore;
            totalHealthAfter += healthAfter;
            damageBefore += choiceBefore.tier >= 3 ? healthBefore : choiceBefore.weight;
            damageAfter += choiceAfter.tier >= 3 ? healthAfter : choiceAfter.weight;
        }
        if (totalHealthBefore != 0.0f && totalHealthAfter != 0.0f && damageBefore != 0.0f && damageAfter != 0.0f) {
            double koTurnsBefore = Math.ceil(totalHealthBefore / damageBefore);
            double koTurnsAfter = Math.ceil(totalHealthAfter / damageAfter);
            if (koTurnsBefore < koTurnsAfter) {
                userChoice.raiseWeight((1.0f - (float)(koTurnsBefore / koTurnsAfter)) * 100.0f);
            } else if (koTurnsBefore > koTurnsAfter && weightNegative) {
                userChoice.weight -= (1.0f - (float)(koTurnsAfter / koTurnsBefore)) * 100.0f;
            }
        }
    }

    public void weightTypeChange(PixelmonWrapper pw, MoveChoice userChoice, List<EnumType> possibleTypes, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        this.weightTypeChange(pw, userChoice, possibleTypes, pw, bestUserChoices, bestOpponentChoices);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void weightTypeChange(PixelmonWrapper pw, MoveChoice userChoice, List<EnumType> possibleTypes, PixelmonWrapper target, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        List<EnumType> saveType = target.type;
        ArrayList<PixelmonWrapper> opponents = pw.getOpponentPokemon();
        ArrayList<MoveChoice> bestUserChoicesAfter = null;
        ArrayList<ArrayList<MoveChoice>> bestOpponentChoicesAfter = null;
        BattleAIBase ai = pw.getBattleAI();
        boolean will2HKO = true;
        try {
            for (EnumType type : possibleTypes) {
                if (type == null) continue;
                target.type = type.makeTypeList();
                bestUserChoicesAfter = ai.getBestAttackChoices(pw);
                bestOpponentChoicesAfter = ai.getBestAttackChoices(opponents);
                ai.weightFromUserOptions(pw, userChoice, bestUserChoices, bestUserChoicesAfter);
                ai.weightFromOpponentOptions(pw, userChoice, MoveChoice.splitChoices(opponents, bestOpponentChoices), bestOpponentChoicesAfter);
                if (MoveChoice.canOutspeedAnd2HKO(MoveChoice.mergeChoices(bestOpponentChoicesAfter), pw, bestUserChoicesAfter)) continue;
                will2HKO = false;
            }
        }
        finally {
            target.type = saveType;
        }
        if (userChoice.weight <= 0.0f) {
            userChoice.tier = 0;
            return;
        }
        userChoice.weight /= (float)possibleTypes.size();
        if (will2HKO) {
            userChoice.lowerTier(1);
            return;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void weightSingleTypeChange(PixelmonWrapper pw, MoveChoice userChoice, List<EnumType> newType, PixelmonWrapper target, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        List<EnumType> saveType = target.type;
        ArrayList<PixelmonWrapper> opponents = pw.getOpponentPokemon();
        ArrayList<MoveChoice> bestUserChoicesAfter = null;
        ArrayList<ArrayList<MoveChoice>> bestOpponentChoicesAfter = null;
        BattleAIBase ai = pw.getBattleAI();
        boolean will2HKO = true;
        try {
            target.type = newType;
            bestUserChoicesAfter = ai.getBestAttackChoices(pw);
            bestOpponentChoicesAfter = ai.getBestAttackChoices(opponents);
            ai.weightFromUserOptions(pw, userChoice, bestUserChoices, bestUserChoicesAfter);
            ai.weightFromOpponentOptions(pw, userChoice, MoveChoice.splitChoices(opponents, bestOpponentChoices), bestOpponentChoicesAfter);
            if (!MoveChoice.canOutspeedAnd2HKO(MoveChoice.mergeChoices(bestOpponentChoicesAfter), pw, bestUserChoicesAfter)) {
                will2HKO = false;
            }
        }
        finally {
            target.type = saveType;
        }
        if (userChoice.weight <= 0.0f) {
            userChoice.tier = 0;
            return;
        }
        if (will2HKO) {
            userChoice.lowerTier(1);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void weightStatusOpponentOptions(PixelmonWrapper pw, MoveChoice userChoice, PixelmonWrapper target, StatusBase status, ArrayList<PixelmonWrapper> opponents, ArrayList<MoveChoice> bestOpponentChoices) {
        try {
            pw.bc.simulateMode = false;
            pw.bc.sendMessages = false;
            target.addStatus(status, pw);
            pw.bc.simulateMode = true;
            pw.bc.modifyStats();
            pw.bc.modifyStatsCancellable(pw);
            ArrayList<ArrayList<MoveChoice>> bestOpponentChoicesAfter = this.getBestAttackChoices(opponents);
            this.weightFromOpponentOptions(pw, userChoice, MoveChoice.splitChoices(opponents, bestOpponentChoices), bestOpponentChoicesAfter);
        }
        finally {
            pw.bc.simulateMode = false;
            target.removeStatus(status.type);
            pw.bc.simulateMode = true;
            pw.bc.sendMessages = true;
            pw.bc.modifyStats();
            pw.bc.modifyStatsCancellable(pw);
        }
    }
}

