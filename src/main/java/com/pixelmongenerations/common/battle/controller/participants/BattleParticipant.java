/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.battle.controller.participants;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.events.battles.SetBattleAIEvent;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.ai.BattleAIBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.RunAway;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentBase;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public abstract class BattleParticipant {
    public boolean startedBattle = false;
    public BattleControllerBase bc;
    public int team = 0;
    public boolean wait = false;
    public List<PixelmonWrapper> controlledPokemon;
    public PixelmonWrapper[] allPokemon;
    public boolean isDefeated = false;
    public Long lngLastMoveMilli;
    public int numControlledPokemon;
    private double[] originalPos;
    public boolean faintedLastTurn = false;
    public ArrayList<int[]> switchingIn = new ArrayList(2);
    public List<PixelmonWrapper> switchingOut = new ArrayList<PixelmonWrapper>();
    private BattleAIBase battleAI;
    public int[] evolution = null;
    public int[] ultraBurst = null;
    public int[] ashNinja = null;
    public boolean canDynamax = true;
    public boolean canMega = true;

    public Long getCurrentTime() {
        return Long.parseLong("" + new Date().getTime());
    }

    public void resetMoveTimer() {
        this.lngLastMoveMilli = this.getCurrentTime();
    }

    public Long getTurnTimeSeconds() {
        if (this.lngLastMoveMilli == null) {
            this.resetMoveTimer();
            return 0L;
        }
        return (this.getCurrentTime() - this.lngLastMoveMilli) / 1000L;
    }

    public BattleParticipant(int numControlledPokemon) {
        this.setNumControlledPokemon(numControlledPokemon);
        this.lngLastMoveMilli = this.getCurrentTime();
    }

    public void setNumControlledPokemon(int numControlledPokemon) {
        this.numControlledPokemon = numControlledPokemon;
        this.controlledPokemon = new ArrayList<PixelmonWrapper>(numControlledPokemon);
    }

    public boolean hasMorePokemon() {
        return this.countAblePokemon() > 0;
    }

    public abstract boolean hasMorePokemonReserve();

    public abstract boolean canGainXP();

    public void startBattle(BattleControllerBase bc) {
        this.bc = bc;
        for (PixelmonWrapper p : this.allPokemon) {
            p.bc = bc;
            int levelCap = bc.rules.levelCap;
            if ((levelCap < PixelmonServerConfig.maxLevel || bc.rules.raiseToCap) && (p.getLevelNum() > levelCap || bc.rules.raiseToCap)) {
                p.setTempLevel(levelCap);
            }
            int maxHealth = p.getMaxHealth();
            int startHealth = p.getHealth();
            if (startHealth > maxHealth) {
                p.setHealth(maxHealth);
            }
            if (!bc.rules.fullHeal) continue;
            p.startHealth = startHealth;
            p.setHealth(maxHealth);
            if (p.pokemon != null) {
                p.pokemon.setHealth(maxHealth);
            }
            p.clearStatus();
            for (Attack attack : p.getMoveset()) {
                if (attack == null) continue;
                attack.pp = attack.ppBase;
            }
        }
        for (PixelmonWrapper p : this.controlledPokemon) {
            p.pokemon.battleController = bc;
        }
    }

    public abstract void endBattle();

    public abstract TextComponentBase getName();

    public abstract MoveChoice getMove(PixelmonWrapper var1);

    public abstract PixelmonWrapper switchPokemon(PixelmonWrapper var1, int[] var2);

    public abstract boolean checkPokemon();

    public abstract void updatePokemon(PixelmonWrapper var1);

    public abstract EntityLivingBase getEntity();

    public abstract void updateOtherPokemon();

    public abstract ParticipantType getType();

    public abstract void getNextPokemon(int var1);

    public abstract int[] getNextPokemonID();

    public int countTeam() {
        return this.allPokemon.length;
    }

    public int countAblePokemon() {
        int i = 0;
        for (PixelmonWrapper pw : this.allPokemon) {
            if (!pw.isAlive()) continue;
            ++i;
        }
        return i;
    }

    public float countHealthPercent() {
        float percent = 0.0f;
        float teamCount = 0.0f;
        for (PixelmonWrapper pw : this.allPokemon) {
            if (!pw.isAlive()) continue;
            teamCount += 1.0f;
            percent += pw.getHealthPercent();
        }
        if (teamCount != 0.0f) {
            return percent / 100.0f / teamCount;
        }
        return 0.0f;
    }

    public void tick() {
    }

    public void clearTurnVariables() {
        for (PixelmonWrapper p : this.controlledPokemon) {
            p.clearTurnVariables();
        }
    }

    public void selectAction() {
        for (PixelmonWrapper p : this.controlledPokemon) {
            p.selectAIAction();
        }
    }

    public boolean getWait() {
        if (this.wait) {
            return true;
        }
        for (PixelmonWrapper p : this.controlledPokemon) {
            if (!p.wait || !p.isAlive() && !p.isSwitching) continue;
            return true;
        }
        return false;
    }

    public PixelmonWrapper getFaintedPokemon() {
        for (PixelmonWrapper pw : this.controlledPokemon) {
            if (!pw.isFainted()) continue;
            return pw;
        }
        return null;
    }

    public boolean hasRemainingPokemon() {
        for (PixelmonWrapper pw : this.controlledPokemon) {
            if (!pw.isAlive()) continue;
            return true;
        }
        return false;
    }

    public void setOriginalPosition() {
        this.originalPos = new double[3];
        this.originalPos[0] = this.getEntity().posX;
        this.originalPos[1] = this.getEntity().posY;
        this.originalPos[2] = this.getEntity().posZ;
    }

    public void setNewPositions(BlockPos arenaPosition) {
    }

    public void setPosition(double[] ds) {
        this.getEntity().setPositionAndUpdate(ds[0], ds[1], ds[2]);
    }

    public void setToOriginalPosition() {
        this.setPosition(this.originalPos);
    }

    public void sendDamagePacket(PixelmonWrapper target, int damage) {
    }

    public void sendHealPacket(PixelmonWrapper target, int amount) {
    }

    public String getDisplayName() {
        return "";
    }

    public static boolean[] canSwitch(PixelmonWrapper p) {
        boolean canSwitch = true;
        boolean canFlee = true;
        for (int i = 0; i < p.getStatusSize(); ++i) {
            StatusBase statusBase = p.getStatus(i);
            try {
                if (!statusBase.stopsSwitching()) continue;
                canSwitch = false;
                canFlee = false;
                continue;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        for (StatusBase statusBase : p.bc.globalStatusController.getGlobalStatuses()) {
            try {
                if (!statusBase.stopsSwitching()) continue;
                canSwitch = false;
                canFlee = false;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        ArrayList<PixelmonWrapper> opponents = p.bc.getOpponentPokemon(p.getParticipant());
        for (PixelmonWrapper pw : opponents) {
            if (!pw.getBattleAbility().stopsSwitching(pw, p)) continue;
            canSwitch = false;
            canFlee = false;
        }
        if (p.hasType(EnumType.Ghost)) {
            canSwitch = true;
            canFlee = true;
        }
        if (!canSwitch && p.getHeldItem().getHeldItemType() == EnumHeldItems.shedShell) {
            canSwitch = true;
        }
        if (!canFlee) {
            if (Lists.newArrayList((Object[])new EnumHeldItems[]{EnumHeldItems.smokeBall, EnumHeldItems.pokeDoll, EnumHeldItems.fluffyTail, EnumHeldItems.pokeToy}).contains((Object)p.getHeldItem().getHeldItemType()) || p.getBattleAbility() instanceof RunAway) {
                canFlee = true;
            }
        }
        return new boolean[]{canSwitch, canFlee};
    }

    public PlayerStorage getStorage() {
        return null;
    }

    public BattleParticipant[] getParticipantList() {
        return new BattleParticipant[]{this};
    }

    public ArrayList<BattleParticipant> getOpponents() {
        return this.bc.getOpponents(this);
    }

    public ArrayList<PixelmonWrapper> getOpponentPokemon() {
        return this.bc.getOpponentPokemon(this);
    }

    public ArrayList<PixelmonWrapper> getTeamPokemon() {
        return this.bc.getTeamPokemon(this);
    }

    public ArrayList<PixelmonWrapper> getActiveUnfaintedPokemon() {
        ArrayList<PixelmonWrapper> unfaintedPokemon = new ArrayList<PixelmonWrapper>(this.controlledPokemon.size());
        for (PixelmonWrapper pw : this.controlledPokemon) {
            if (pw.isFainted()) continue;
            unfaintedPokemon.add(pw);
        }
        return unfaintedPokemon;
    }

    public PixelmonWrapper getPokemonFromID(int[] id) {
        for (PixelmonWrapper pw : this.controlledPokemon) {
            if (!PixelmonMethods.isIDSame(id, pw.getPokemonID())) continue;
            return pw;
        }
        return null;
    }

    public int getPartyPosition(PixelmonWrapper pokemon) {
        for (int i = 0; i < this.allPokemon.length; ++i) {
            if (pokemon != this.allPokemon[i]) continue;
            return i;
        }
        return -1;
    }

    protected void loadParty(PlayerStorage storage) {
        List<NBTTagCompound> team = storage.getTeam();
        this.allPokemon = new PixelmonWrapper[team.size()];
        for (int i = 0; i < this.allPokemon.length; ++i) {
            this.allPokemon[i] = new PixelmonWrapper(this, team.get(i), i);
        }
    }

    protected void loadParty(List<PokemonLink> party) {
        this.allPokemon = new PixelmonWrapper[party.size()];
        for (int i = 0; i < this.allPokemon.length; ++i) {
            this.allPokemon[i] = new PixelmonWrapper(this, party.get(i).getNBT(), i);
        }
    }

    public PixelmonWrapper getPokemonFromParty(int[] id) {
        for (PixelmonWrapper pw : this.allPokemon) {
            if (!PixelmonMethods.isIDSame(pw.getPokemonID(), id)) continue;
            return pw;
        }
        return null;
    }

    protected PixelmonWrapper getPokemonFromParty(EntityPixelmon entity) {
        PixelmonWrapper pw = this.getPokemonFromParty(entity.getPokemonId());
        if (pw == null) {
            return new PixelmonWrapper(this, entity, 0);
        }
        if (entity.getFormEnum().isTemporary()) {
            if (entity.getSpecies() == EnumSpecies.Zygarde) {
                NBTTagCompound nbt = entity.writeToNBT(new NBTTagCompound());
                if (nbt.getInteger("isTenPercent") == 1) {
                    entity.setForm(nbt.getInteger("isTenPercent"));
                }
            } else {
                entity.setForm(entity.getFormEnum().getDefaultFromTemporary().getForm(), true);
            }
        }
        pw.pokemon = entity;
        return pw;
    }

    public World getWorld() {
        return this.getEntity().world;
    }

    public boolean addSwitchingOut(PixelmonWrapper pw) {
        if (this.hasMorePokemonReserve()) {
            this.switchingOut.add(pw);
            return true;
        }
        return false;
    }

    public void switchAllFainted() {
        for (PixelmonWrapper pw : this.switchingOut) {
            pw.doSwitch();
        }
        this.switchingOut.clear();
        this.switchingIn.clear();
    }

    public PixelmonWrapper getRandomPartyPokemon() {
        ArrayList<PixelmonWrapper> choices = new ArrayList<PixelmonWrapper>();
        for (PixelmonWrapper pw : this.allPokemon) {
            if (pw.pokemon != null || pw.isFainted()) continue;
            choices.add(pw);
        }
        return (PixelmonWrapper)RandomHelper.getRandomElementFromList(choices);
    }

    public boolean canMegaEvolve() {
        return this.canMega;
    }

    public BattleAIBase getBattleAI() {
        return this.battleAI;
    }

    public void setBattleAI(BattleAIBase ai) {
        SetBattleAIEvent event = new SetBattleAIEvent(this.bc, this, ai);
        MinecraftForge.EVENT_BUS.post(event);
        this.battleAI = event.getAI();
    }

    public boolean canUltraBurst() {
        return true;
    }

    public boolean canDynamax() {
        return true;
    }
}

