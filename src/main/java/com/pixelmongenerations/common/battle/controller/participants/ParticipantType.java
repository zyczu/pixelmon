/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.participants;

public enum ParticipantType {
    Player,
    Trainer,
    WildPokemon;


    public static ParticipantType get(int index) {
        for (ParticipantType p : ParticipantType.values()) {
            if (p.ordinal() != index) continue;
            return p;
        }
        return null;
    }
}

