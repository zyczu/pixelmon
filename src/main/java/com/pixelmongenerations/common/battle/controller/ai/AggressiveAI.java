/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.controller.ai;

import com.pixelmongenerations.common.battle.controller.ai.BattleAIBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.List;

public class AggressiveAI
extends BattleAIBase {
    public AggressiveAI(BattleParticipant participant) {
        super(participant);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public MoveChoice getNextMove(PixelmonWrapper pw) {
        ArrayList<MoveChoice> choices = this.getAttackChoicesOpponentOnly(pw);
        ArrayList<PixelmonWrapper> allies = pw.getTeamPokemonExcludeSelf();
        ArrayList<MoveChoice> bestChoices = new ArrayList<MoveChoice>();
        boolean wasSimulateMode = this.bc.simulateMode;
        this.bc.simulateMode = true;
        try {
            this.bc.modifyStats();
            this.bc.modifyStatsCancellable(pw);
            for (MoveChoice choice : choices) {
                if (choice.attack.getAttackCategory() == AttackCategory.Status) continue;
                this.weightOffensiveMove(pw, choice, allies);
                if (choice.attack.isAttack("Explosion", "Final Gambit", "Self-Destruct")) {
                    choice.weight = 0.0f;
                }
                MoveChoice.checkBestChoice(choice, bestChoices);
            }
        }
        finally {
            this.bc.simulateMode = wasSimulateMode;
        }
        if (bestChoices.isEmpty()) {
            return RandomHelper.getRandomElementFromList(choices);
        }
        return (MoveChoice)RandomHelper.getRandomElementFromList(bestChoices);
    }

    protected MoveChoice getNextMoveAttackOnly(PixelmonWrapper pw) {
        return this.getNextMove(pw);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int[] getNextSwitch(PixelmonWrapper pw) {
        ArrayList<int[]> bestSwitches = new ArrayList<int[]>();
        int controlledIndex = pw.getControlledIndex();
        List<int[]> party = this.getPossibleSwitchIDs();
        if (party.isEmpty()) {
            return new int[]{-1, -1};
        }
        if (party.size() == 1) {
            return party.get(0);
        }
        boolean allFainted = true;
        for (PixelmonWrapper opponent : pw.getOpponentPokemon()) {
            allFainted = allFainted && opponent.isFainted();
        }
        if (allFainted || controlledIndex == -1) {
            return RandomHelper.getRandomElementFromList(party);
        }
        boolean wasSimulateMode = this.bc.simulateMode;
        this.bc.simulateMode = true;
        try {
            MoveChoice bestChoice = null;
            for (int[] partyID : party) {
                this.bc.simulateMode = true;
                pw.newPokemonID = partyID;
                PixelmonWrapper nextPokemon = pw.doSwitch();
                try {
                    if (!this.validateSwitch(nextPokemon)) continue;
                    MoveChoice choice = this.getNextMoveAttackOnly(nextPokemon);
                    if (choice.compareTo(bestChoice) > 0) {
                        bestSwitches.clear();
                    }
                    if (choice.compareTo(bestChoice) < 0) continue;
                    bestChoice = choice;
                    bestSwitches.add(partyID);
                }
                finally {
                    this.resetSwitchSimulation(pw, controlledIndex, nextPokemon);
                }
            }
        }
        finally {
            this.bc.simulateMode = wasSimulateMode;
        }
        return RandomHelper.getRandomElementFromList(bestSwitches.isEmpty() ? party : bestSwitches);
    }

    protected boolean validateSwitch(PixelmonWrapper nextPokemon) {
        return true;
    }

    protected void resetSwitchSimulation(PixelmonWrapper current, int controlledIndex, PixelmonWrapper simulated) {
        this.participant.controlledPokemon.set(controlledIndex, current);
    }
}

