/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle;

import com.pixelmongenerations.api.events.BattleStartEvent;
import com.pixelmongenerations.api.events.PokedexEvent;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.Spectator;
import com.pixelmongenerations.common.battle.rules.BattleRules;
import com.pixelmongenerations.common.pokedex.EnumPokedexRegisterStatus;
import com.pixelmongenerations.common.pokedex.Pokedex;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.common.MinecraftForge;

public class BattleRegistry {
    private static int battleIndex = 0;
    private static final Map<Integer, BattleControllerBase> battleIDMap = new HashMap<Integer, BattleControllerBase>();
    private static final Map<String, BattleControllerBase> battleNameMap = new HashMap<String, BattleControllerBase>();
    private static final List<BattleControllerBase> battleList = new ArrayList<BattleControllerBase>();

    public static BattleControllerBase startBattle(BattleParticipant[] team1, BattleParticipant[] team2, BattleRules rules) {
        BattleControllerBase controller = new BattleControllerBase(team1, team2, rules);
        for (PlayerParticipant participant : controller.getPlayers()) {
            if (participant.getStorage().canPartyBattle() && !participant.getStorage().isEvolving(participant.getWorld())) continue;
            return null;
        }
        BattleStartEvent battleStartedEvent = new BattleStartEvent(controller, team1, team2);
        if (MinecraftForge.EVENT_BUS.post(battleStartedEvent)) {
            return null;
        }
        BattleRegistry.registerBattle(controller);
        for (BattleParticipant participant : controller.participants) {
            participant.startedBattle = true;
            if (!(participant instanceof PlayerParticipant)) continue;
            Pokedex pokedex = participant.getStorage().pokedex;
            for (BattleParticipant participant2 : controller.participants) {
                PixelmonWrapper wrapper = participant2.allPokemon[0];
                if (MinecraftForge.EVENT_BUS.post(new PokedexEvent(((PlayerParticipant)participant).player, pokedex, wrapper.getSpecies(), EnumPokedexRegisterStatus.seen))) continue;
                pokedex.set(wrapper.getSpecies(), EnumPokedexRegisterStatus.seen);
            }
        }
        return controller;
    }

    public static boolean hasBattle() {
        return !battleIDMap.isEmpty();
    }

    public static void registerBattle(BattleControllerBase bc) {
        bc.battleIndex = battleIndex++;
        battleIDMap.put(bc.battleIndex, bc);
        battleList.add(bc);
        for (PlayerParticipant p : bc.getPlayers()) {
            battleNameMap.put(p.getDisplayName(), bc);
        }
    }

    public static BattleControllerBase getBattle(int index) {
        return battleIDMap.get(index);
    }

    public static BattleControllerBase getBattle(EntityPlayer player) {
        return player == null ? null : battleNameMap.get(player.getDisplayNameString());
    }

    public static BattleControllerBase getBattleExcludeSpectate(EntityPlayer player) {
        BattleControllerBase bc = BattleRegistry.getBattle(player);
        if (bc == null) {
            return null;
        }
        return bc.hasSpectator(player) ? null : bc;
    }

    public static BattleControllerBase getBattle(String playerName) {
        return battleNameMap.get(playerName);
    }

    public static BattleControllerBase getSpectatedBattle(EntityPlayer player) {
        return battleNameMap.get(player.getDisplayNameString());
    }

    public static void registerSpectator(Spectator spectator, BattleControllerBase bc) {
        battleNameMap.put(spectator.getEntity().getDisplayNameString(), bc);
    }

    public static void unregisterSpectator(Spectator spectator) {
        battleNameMap.remove(spectator.getEntity().getDisplayNameString());
    }

    public static boolean removeSpectator(EntityPlayerMP player) {
        BattleControllerBase battle = battleNameMap.get(player.getDisplayNameString());
        if (battle != null) {
            return battle.removeSpectator(player);
        }
        return false;
    }

    public static void deRegisterBattle(BattleControllerBase bc) {
        if (bc != null) {
            battleIDMap.remove(bc.battleIndex);
            battleList.remove(bc);
            for (PlayerParticipant p : bc.getPlayers()) {
                battleNameMap.remove(p.getDisplayName());
            }
            for (Spectator s : bc.spectators) {
                battleNameMap.remove(s.getEntity().getDisplayNameString());
            }
        }
    }

    public static void updateBattles() {
        int size = battleList.size();
        for (int i = 0; i < size; ++i) {
            battleList.get(i).update();
            int currentSize = battleList.size();
            if (currentSize >= size) continue;
            size = currentSize;
            --i;
        }
    }

    @Deprecated
    public static int getIndex(BattleControllerBase bc) {
        return bc.battleIndex;
    }
}

