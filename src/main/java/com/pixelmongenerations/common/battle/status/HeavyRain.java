/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Rainy;
import com.pixelmongenerations.common.battle.status.Weather;

public class HeavyRain
extends Rainy {
    public HeavyRain() {
        this(-1);
    }

    public HeavyRain(int turns) {
        super(turns);
    }

    @Override
    protected Weather getNewInstance(int turns) {
        return new HeavyRain();
    }

    @Override
    protected int getStartTurns(PixelmonWrapper user) {
        return -1;
    }
}

