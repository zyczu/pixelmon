/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class Intimidated
extends StatusBase {
    public Intimidated(StatusType type) {
        super(type);
    }

    public Intimidated() {
        super(StatusType.Intimidated);
    }

    public static void becomeIntimidated(PixelmonWrapper target) {
        if (!target.hasStatus(StatusType.Intimidated)) {
            target.addStatus(new Intimidated(), target);
        }
    }
}

