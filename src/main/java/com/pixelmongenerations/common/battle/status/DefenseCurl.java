/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class DefenseCurl
extends StatusBase {
    public DefenseCurl() {
        super(StatusType.DefenseCurl);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        user.addStatus(new DefenseCurl(), user);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (!pw.hasStatus(StatusType.DefenseCurl)) {
            if (MoveChoice.hasSuccessfulAttackChoice(bestUserChoices, "Rollout", "Ice Ball")) {
                userChoice.raiseWeight(50.0f);
            }
        }
    }
}

