/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class Autotomize
extends StatusBase {
    public Autotomize() {
        super(StatusType.Autotomize);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.addStatus(new Autotomize(), user) && !user.isDynamaxed()) {
            user.bc.sendToAll("pixelmon.status.autotomize", user.getNickname());
        }
    }

    @Override
    public float modifyWeight(float initWeight) {
        return Math.max(0.0f, initWeight - 100.0f);
    }
}

