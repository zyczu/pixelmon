/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import java.util.ArrayList;

public class Nightmare
extends StatusBase {
    public Nightmare() {
        super(StatusType.Nightmare);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasStatus(StatusType.Sleep) && target.addStatus(new Nightmare(), user)) {
            target.bc.sendToAll("pixelmon.status.nightmareadd", target.getNickname());
        } else {
            target.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (pw.hasStatus(StatusType.Sleep)) {
            if (!(pw.getBattleAbility() instanceof MagicGuard)) {
                pw.bc.sendToAll("pixelmon.status.nightmare", pw.getNickname());
                pw.doBattleDamage(pw, pw.getPercentMaxHealth(25.0f), DamageTypeEnum.STATUS);
            }
        } else {
            pw.removeStatus(this);
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        for (PixelmonWrapper target : userChoice.targets) {
            if (target.getBattleAbility() instanceof MagicGuard) continue;
            userChoice.raiseWeight(25.0f);
        }
    }
}

