/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class NoRetreat
extends StatusBase {
    transient PixelmonWrapper locker;

    public NoRetreat() {
        super(StatusType.NoRetreat);
    }

    public NoRetreat(PixelmonWrapper locker) {
        super(StatusType.NoRetreat);
        this.locker = locker;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (!target.hasStatus(StatusType.NoRetreat)) {
            if (target.hasStatus(StatusType.MeanLook, StatusType.JawLock)) {
                target.bc.sendToAll("pixelmon.effect.noretreatrepeat", user.getNickname());
                target.getBattleStats().modifyStat(1, StatsType.Attack, user, true);
                target.getBattleStats().modifyStat(1, StatsType.Defence, user, true);
                target.getBattleStats().modifyStat(1, StatsType.SpecialAttack, user, true);
                target.getBattleStats().modifyStat(1, StatsType.SpecialDefence, user, true);
                target.getBattleStats().modifyStat(1, StatsType.Speed, user, true);
                return;
            }
            target.addStatus(new NoRetreat(user), user);
            target.bc.sendToAll("pixelmon.effect.noretreat", user.getNickname());
            target.getBattleStats().modifyStat(1, StatsType.Attack, user, true);
            target.getBattleStats().modifyStat(1, StatsType.Defence, user, true);
            target.getBattleStats().modifyStat(1, StatsType.SpecialAttack, user, true);
            target.getBattleStats().modifyStat(1, StatsType.SpecialDefence, user, true);
            target.getBattleStats().modifyStat(1, StatsType.Speed, user, true);
        } else {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        }
    }

    @Override
    public boolean stopsSwitching() {
        return true;
    }
}

