/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.util.ArrayList;

public class Tailwind
extends StatusBase {
    transient int turnsToGo = 4;

    public Tailwind() {
        super(StatusType.Tailwind);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.targetIndex == 0 || user.bc.simulateMode) {
            if (user.hasStatus(this.type)) {
                user.bc.sendToAll("pixelmon.status.tailwindalready", new Object[0]);
                user.attack.moveResult.result = AttackResult.failed;
            } else {
                user.addTeamStatus(new Tailwind(), user);
                user.bc.sendToAll("pixelmon.status.tailwindstarted", user.getNickname());
            }
        }
    }

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        int n = StatsType.Speed.getStatIndex();
        stats[n] = stats[n] * 2;
        return stats;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (--this.turnsToGo == 0) {
            pw.removeStatus(this);
            pw.bc.sendToAll("pixelmon.status.tailwindfaded", pw.getNickname());
        }
    }

    @Override
    public StatusBase copy() {
        Tailwind copy = new Tailwind();
        copy.turnsToGo = this.turnsToGo;
        return copy;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (!MoveChoice.hasPriority(bestOpponentChoices) && MoveChoice.canOutspeed(bestOpponentChoices, pw, bestUserChoices)) {
            userChoice.raiseWeight(75.0f);
        }
    }
}

