/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Terrain;

public class NoTerrain
extends Terrain {
    public NoTerrain() {
        super(StatusType.None, "", "", "");
    }

    @Override
    public Terrain getNewInstance() {
        return new NoTerrain();
    }

    @Override
    protected int countBenefits(PixelmonWrapper user, PixelmonWrapper target) {
        return 0;
    }
}

