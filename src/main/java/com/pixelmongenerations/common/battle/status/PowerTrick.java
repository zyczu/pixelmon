/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.BattleAIBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.util.ArrayList;

public class PowerTrick
extends StatusBase {
    public PowerTrick() {
        super(StatusType.PowerTrick);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        user.bc.sendToAll("pixelmon.status.powertrick", user.getNickname());
        if (!user.removeStatus(this.type)) {
            user.addStatus(new PowerTrick(), user);
        }
    }

    @Override
    public int[] modifyBaseStats(PixelmonWrapper user, int[] stats) {
        int temp = stats[StatsType.Attack.getStatIndex()];
        stats[StatsType.Attack.getStatIndex()] = stats[StatsType.Defence.getStatIndex()];
        stats[StatsType.Defence.getStatIndex()] = temp;
        return stats;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        StatusBase prevStatus = pw.getStatus(this.type);
        ArrayList<PixelmonWrapper> opponents = pw.getOpponentPokemon();
        BattleAIBase ai = pw.getBattleAI();
        try {
            pw.bc.simulateMode = false;
            pw.bc.sendMessages = false;
            this.applyEffect(pw, pw);
            pw.bc.simulateMode = true;
            pw.bc.modifyStats();
            pw.bc.modifyStatsCancellable(pw);
            ArrayList<MoveChoice> bestUserChoicesAfter = ai.getBestAttackChoices(pw);
            ArrayList<ArrayList<MoveChoice>> bestOpponentChoicesAfter = ai.getBestAttackChoices(opponents);
            ai.weightFromUserOptions(pw, userChoice, bestUserChoices, bestUserChoicesAfter);
            ai.weightFromOpponentOptions(pw, userChoice, MoveChoice.splitChoices(opponents, bestOpponentChoices), bestOpponentChoicesAfter);
        }
        finally {
            pw.bc.simulateMode = false;
            pw.removeStatus(this.type);
            if (prevStatus != null) {
                pw.addStatus(prevStatus, pw);
            }
            pw.bc.simulateMode = true;
            pw.bc.sendMessages = true;
            pw.bc.modifyStats();
            pw.bc.modifyStatsCancellable(pw);
        }
    }
}

