/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class Telekinesis
extends StatusBase {
    private int effectTurns = 3;

    public Telekinesis() {
        super(StatusType.Telekinesis);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasStatus(StatusType.Telekinesis, StatusType.Ingrain) || target.bc.globalStatusController.hasStatus(StatusType.Gravity) || target.getUsableHeldItem().getHeldItemType() == EnumHeldItems.ironBall) {
            target.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
        } else if (target.addStatus(new Telekinesis(), user)) {
            target.bc.sendToAll("pixelmon.status.telekinesis", target.getNickname());
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (--this.effectTurns <= 0) {
            pw.bc.sendToAll("pixelmon.status.telekinesisend", pw.getNickname());
            pw.removeStatus(this);
        }
    }

    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return new int[]{power, -1};
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (user.attack.getAttackBase().attackType.equals((Object)EnumType.Ground) && user.attack.getAttackCategory() != AttackCategory.Status && !pokemon.isGrounded()) {
            user.bc.sendToAll("pixelmon.battletext.noeffect", pokemon.getNickname());
            return true;
        }
        return false;
    }
}

