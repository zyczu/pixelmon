/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Poison;
import com.pixelmongenerations.common.battle.status.Protect;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Comatose;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Immunity;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LeafGuard;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LongReach;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PastelVeil;
import com.pixelmongenerations.common.item.heldItems.ItemProtectivePads;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;

public class BanefulBunker
extends Protect {
    public BanefulBunker() {
        super(StatusType.BanefulBunker);
    }

    @Override
    protected boolean addStatus(PixelmonWrapper user) {
        return user.addStatus(new BanefulBunker(), user);
    }

    @Override
    public void stopsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user) {
        super.stopsIncomingAttackMessage(pokemon, user);
        if (user.bc.globalStatusController.hasStatus(StatusType.MistyTerrain)) {
            if (!user.hasType(EnumType.Flying)) {
                return;
            }
        }
        if (user.bc.globalStatusController.hasStatus(StatusType.SafeGuard)) {
            return;
        }
        if (user.hasStatus(StatusType.Substitute)) {
            return;
        }
        if (!user.attack.getAttackBase().getMakesContact() || user.hasPrimaryStatus()) {
            return;
        }
        if (user.hasHeldItem() && user.getHeldItem() instanceof ItemProtectivePads) {
            user.bc.sendToAll("pixelmon.effect.protectivepads", user.getNickname());
            return;
        }
        if (user.getBattleAbility() instanceof LongReach) {
            return;
        }
        if (user.hasType(EnumType.Poison)) {
            return;
        }
        if (!user.hasType(EnumType.Steel) && !(user.getBattleAbility() instanceof Immunity) && !(user.getBattleAbility() instanceof Comatose) && !(user.getBattleAbility() instanceof PastelVeil) && (user.getSpecies() != EnumSpecies.Minior || user.getForm() > 6) && (!(user.getBattleAbility() instanceof LeafGuard) || !user.bc.globalStatusController.hasStatus(StatusType.Sunny))) {
            user.addStatus(new Poison(), pokemon);
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        super.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
        if (userChoice.weight != -1.0f) {
            block0: for (ArrayList<MoveChoice> choices : MoveChoice.splitChoices(pw.getOpponentPokemon(), bestOpponentChoices)) {
                for (MoveChoice choice : choices) {
                    if (!choice.isAttack() || !choice.attack.getAttackBase().getMakesContact()) continue;
                    userChoice.raiseWeight(12.5f);
                    continue block0;
                }
            }
        }
    }
}

