/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.PledgeBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class WaterPledge
extends PledgeBase {
    public WaterPledge() {
        super(StatusType.WaterPledge);
    }

    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        a.getAttackBase().effects.stream().filter(EffectBase::isChance).forEach(effect -> effect.changeChance(2));
        return new int[]{power, accuracy};
    }
}

