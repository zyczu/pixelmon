/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.BattleAIBase;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Infiltrator;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;

public abstract class Screen
extends StatusBase {
    transient StatsType stat;
    transient int effectTurns;
    transient String langStart;
    transient String langFail;
    transient String langEnd;

    public Screen(StatusType type, StatsType stat, int effectTurns, String langStart, String langFail, String langEnd) {
        super(type);
        this.stat = stat;
        this.effectTurns = effectTurns;
        this.langStart = langStart;
        this.langFail = langFail;
        this.langEnd = langEnd;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.targetIndex == 0 || user.bc.simulateMode) {
            if (user.hasStatus(this.type)) {
                user.bc.sendToAll(this.langFail, user.getNickname());
                user.attack.moveResult.result = AttackResult.failed;
            } else {
                this.effectTurns = user.getUsableHeldItem().getHeldItemType() == EnumHeldItems.lightClay ? 8 : 5;
                user.addTeamStatus(this.getNewInstance(this.effectTurns), user);
                user.bc.sendToAll(this.langStart, user.getNickname());
            }
        }
    }

    protected abstract Screen getNewInstance(int var1);

    @Override
    public int[] modifyStatsCancellable(PixelmonWrapper user, int[] stats) {
        float multiplier = user.getTeamPokemon().size() > 1 ? 1.5f : 2.0f;
        int n = this.stat.getStatIndex();
        stats[n] = (int)((float)stats[n] * multiplier);
        return stats;
    }

    @Override
    public boolean ignoreStatus(PixelmonWrapper user, PixelmonWrapper target) {
        return user.getBattleAbility() instanceof Infiltrator;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (--this.effectTurns <= 0) {
            pw.bc.sendToAll(this.langEnd, pw.getNickname());
            pw.removeTeamStatus(this);
        }
    }

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public StatusBase copy() {
        return this.getNewInstance(this.effectTurns);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        ArrayList<PixelmonWrapper> opponents = pw.getOpponentPokemon();
        if (opponents.isEmpty()) {
            return;
        }
        for (PixelmonWrapper opponent : opponents) {
            if (!this.ignoreStatus(opponent, pw)) continue;
            return;
        }
        BattleAIBase ai = pw.getBattleAI();
        try {
            pw.bc.simulateMode = false;
            pw.addTeamStatus(this.getNewInstance(5), pw);
            pw.bc.simulateMode = true;
            pw.bc.modifyStats();
            pw.bc.modifyStatsCancellable(opponents.get(0));
            ArrayList<ArrayList<MoveChoice>> bestOpponentChoicesAfter = ai.getBestAttackChoices(opponents);
            ai.weightFromOpponentOptions(pw, userChoice, MoveChoice.splitChoices(opponents, bestOpponentChoices), bestOpponentChoicesAfter);
        }
        finally {
            pw.bc.simulateMode = false;
            pw.removeTeamStatus(this);
            pw.bc.simulateMode = true;
            pw.bc.modifyStats();
            pw.bc.modifyStatsCancellable(pw);
        }
        if (userChoice.weight == 0.0f) {
            userChoice.raiseWeight(10.0f);
        }
    }
}

