/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.DrySkin;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Hydration;
import com.pixelmongenerations.common.entity.pixelmon.abilities.RainDish;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SwiftSwim;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.List;

public class Rainy
extends Weather {
    public Rainy() {
        this(5);
    }

    public Rainy(int turnsToGo) {
        super(StatusType.Rainy, turnsToGo, EnumHeldItems.dampRock, "pixelmon.effect.raining", "pixelmon.status.heavyrain", "pixelmon.status.rainstopped");
    }

    @Override
    protected Weather getNewInstance(int turns) {
        return new Rainy(turns);
    }

    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (a.getAttackBase().attackType == EnumType.Water && user.getHeldItem() != PixelmonItemsHeld.utilityUmbrella) {
            power = (int)((double)power * 1.5);
        } else if (a.getAttackBase().attackType == EnumType.Fire && user.getHeldItem() != PixelmonItemsHeld.utilityUmbrella) {
            power = (int)((double)power * 0.5);
        }
        return new int[]{power, accuracy};
    }

    @Override
    protected int countBenefits(PixelmonWrapper user, PixelmonWrapper target) {
        List<Attack> moveset;
        int benefits;
        block11: {
            block10: {
                benefits = 0;
                AbilityBase ability = target.getBattleAbility();
                if (user.getHeldItem() == PixelmonItemsHeld.utilityUmbrella) {
                    return benefits;
                }
                if (ability instanceof DrySkin || ability instanceof Hydration || ability instanceof RainDish || ability instanceof SwiftSwim) {
                    ++benefits;
                }
                if (Attack.hasOffensiveAttackType(moveset = user.getBattleAI().getMoveset(target), EnumType.Water)) break block10;
                if (!Attack.hasAttack(moveset, "Weather Ball")) break block11;
            }
            ++benefits;
        }
        if (Attack.hasAttack(moveset, "Hurricane")) {
            ++benefits;
        }
        if (Attack.hasAttack(moveset, "Thunder")) {
            ++benefits;
        }
        if (Attack.hasOffensiveAttackType(moveset, EnumType.Fire)) {
            --benefits;
        }
        if (Attack.hasAttack(moveset, "Solar Beam")) {
            --benefits;
        }
        if (Attack.hasAttack(moveset, "Moonlight", "Morning Sun", "Synthesis")) {
            --benefits;
        }
        return benefits;
    }
}

