/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.status.Split;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class PowerSplit
extends Split {
    public PowerSplit() {
        super(StatusType.PowerSplit, StatsType.Attack, StatsType.SpecialAttack, "pixelmon.status.powersplit");
    }

    @Override
    protected Split getNewInstance(int splitStat1, int splitStat2) {
        PowerSplit split = new PowerSplit();
        split.statValue1 = splitStat1;
        split.statValue2 = splitStat2;
        return split;
    }
}

