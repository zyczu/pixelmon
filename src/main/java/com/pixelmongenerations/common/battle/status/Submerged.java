/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class Submerged
extends StatusBase {
    public Submerged() {
        super(StatusType.Submerged);
    }

    @Override
    public boolean stopsSwitching() {
        return true;
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (user.attack.isAttack("Surf", "Whirlpool")) {
            user.attack.movePower *= 2;
            return false;
        }
        if (user.attack.canHitNoTarget()) {
            user.attack.moveAccuracy = -1;
            return false;
        }
        return user.attack.moveAccuracy != -2 && !pokemon.bc.simulateMode;
    }

    @Override
    public void stopsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.battletext.missedattack", pokemon.getNickname());
    }
}

