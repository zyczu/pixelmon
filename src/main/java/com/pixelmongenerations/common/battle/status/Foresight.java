/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;
import java.util.List;

public class Foresight
extends StatusBase {
    public Foresight() {
        super(StatusType.Foresight);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        target.bc.sendToAll("pixelmon.status.foresight", target.getNickname());
        target.getBattleStats().resetStat(StatsType.Evasion);
        target.addStatus(new Foresight(), user);
    }

    @Override
    public List<EnumType> getEffectiveTypes(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.attack != null && user.attack.getAttackBase().attackType == EnumType.Normal || user.attack.getAttackBase().attackType == EnumType.Fighting) {
            return EnumType.ignoreType(target.type, EnumType.Ghost);
        }
        return target.type;
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly()) {
            return;
        }
        for (PixelmonWrapper target : userChoice.targets) {
            int evasion = target.getBattleStats().getStage(StatsType.Evasion);
            if (evasion > 0) {
                userChoice.raiseWeight(10 * evasion);
            }
            Moveset moveset = pw.getMoveset();
            boolean foresighted = false;
            if (target.type.contains((Object)EnumType.Ghost)) {
                if (moveset.hasOffensiveAttackType(EnumType.Normal, EnumType.Fighting) && !foresighted) continue;
            }
            userChoice.raiseWeight(100.0f);
            foresighted = true;
        }
    }
}

