/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.IceBody;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Overcoat;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SnowCloak;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.List;

public class Hail
extends Weather {
    public Hail() {
        this(5);
    }

    public Hail(int turnsToGo) {
        super(StatusType.Hail, turnsToGo, EnumHeldItems.icyRock, "pixelmon.effect.starthail", "pixelmon.status.heavyhail", "pixelmon.status.hailstopped");
    }

    @Override
    protected Weather getNewInstance(int turns) {
        return new Hail(turns);
    }

    @Override
    public void applyRepeatedEffect(BattleControllerBase bc) {
        for (PixelmonWrapper p : bc.getDefaultTurnOrder()) {
            if (this.isImmune(p)) continue;
            p.bc.sendToAll("pixelmon.status.hurthail", p.getNickname());
            p.doBattleDamage(p, p.getPercentMaxHealth(6.25f), DamageTypeEnum.WEATHER);
        }
    }

    @Override
    public boolean isImmune(PixelmonWrapper p) {
        AbilityBase ability = p.getBattleAbility();
        return p.hasType(EnumType.Ice) || p.isFainted() || ability instanceof MagicGuard || ability instanceof Overcoat || ability instanceof SnowCloak || ability instanceof IceBody || p.getUsableHeldItem().getHeldItemType() == EnumHeldItems.safetyGoggles;
    }

    @Override
    protected int countBenefits(PixelmonWrapper user, PixelmonWrapper target) {
        AbilityBase ability;
        int benefits = 0;
        if (!this.isImmune(target)) {
            --benefits;
        }
        if ((ability = target.getBattleAbility()) instanceof IceBody || ability instanceof SnowCloak) {
            ++benefits;
        }
        List<Attack> moveset = user.getBattleAI().getMoveset(target);
        if (Attack.hasAttack(moveset, "Blizzard", "Weather Ball")) {
            ++benefits;
        }
        if (Attack.hasAttack(moveset, "Solar Beam")) {
            --benefits;
        }
        if (Attack.hasAttack(moveset, "Moonlight", "Morning Sun", "Synthesis")) {
            --benefits;
        }
        return benefits;
    }
}

