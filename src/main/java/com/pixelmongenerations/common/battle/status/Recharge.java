/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class Recharge
extends StatusBase {
    transient int turnWait;
    transient int turnsWaited = 0;

    public Recharge() {
        super(StatusType.Recharge);
        this.turnWait = 1;
    }

    public Recharge(int turnWait) {
        super(StatusType.Recharge);
        this.turnWait = turnWait;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        user.addStatus(new Recharge(1), user);
    }

    @Override
    public boolean canAttackThisTurn(PixelmonWrapper user, Attack a) {
        if (this.turnsWaited > this.turnWait) {
            user.removeStatus(this);
            return true;
        }
        user.bc.sendToAll("pixelmon.status.recharging", user.getNickname());
        return false;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (this.turnsWaited++ >= this.turnWait) {
            pw.removeStatus(this);
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        userChoice.weight /= 2.0f;
    }
}

