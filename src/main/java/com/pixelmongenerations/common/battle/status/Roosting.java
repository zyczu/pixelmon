/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.List;

public class Roosting
extends StatusBase {
    public Roosting(PixelmonWrapper pw) {
        super(StatusType.Roosting);
        List<EnumType> newType = EnumType.ignoreType(pw.type, EnumType.Flying);
        if (newType.get(0) == EnumType.Mystery) {
            newType.clear();
        }
        if (newType.size() != pw.type.size()) {
            if (newType.size() == 0) {
                newType.add(EnumType.Normal);
            }
            pw.setTempType(newType);
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        List<EnumType> initialType = pw.getInitialType();
        if (initialType != null) {
            pw.setTempType(initialType);
        }
        pw.removeStatus(this);
    }
}

