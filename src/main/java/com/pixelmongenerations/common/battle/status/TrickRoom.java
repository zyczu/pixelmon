/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.GlobalStatusController;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;

public class TrickRoom
extends GlobalStatusBase {
    private transient int effectTurns = 5;

    public TrickRoom() {
        super(StatusType.TrickRoom);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.globalStatusController.removeGlobalStatus(this.type)) {
            user.bc.sendToAll("pixelmon.effect.trickroomnormal", new Object[0]);
        } else {
            user.bc.sendToAll("pixelmon.status.trickroom", user.getNickname());
            user.bc.globalStatusController.addGlobalStatus(new TrickRoom());
            if (user.getHeldItem().getHeldItemType() == EnumHeldItems.roomservice) {
                user.getBattleStats().modifyStat(-1, StatsType.Speed, user, false, false);
                user.consumeItem();
                user.bc.sendToAll("pixelmon.roomservice.activated", user.getNickname());
            }
        }
    }

    @Override
    public void applyRepeatedEffect(GlobalStatusController globalStatusController) {
        if (--this.effectTurns <= 0) {
            globalStatusController.bc.sendToAll("pixelmon.status.trickroomreturntonormal", new Object[0]);
            globalStatusController.removeGlobalStatus(this);
        }
    }

    public boolean participantMovesFirst(PixelmonWrapper user, PixelmonWrapper target) throws Exception {
        int targetSpeed;
        int userSpeed = user.getBattleStats().getStatWithMod(StatsType.Speed);
        if (userSpeed > (targetSpeed = target.getBattleStats().getStatWithMod(StatsType.Speed))) {
            return false;
        }
        if (targetSpeed > userSpeed) {
            return true;
        }
        return RandomHelper.getRandomChance(50);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        for (ArrayList<MoveChoice> choices : MoveChoice.splitChoices(pw.getOpponentPokemon(), bestOpponentChoices)) {
            if (MoveChoice.canOutspeed(choices, pw, bestUserChoices)) continue;
            return;
        }
        userChoice.raiseWeight(50.0f);
    }
}

