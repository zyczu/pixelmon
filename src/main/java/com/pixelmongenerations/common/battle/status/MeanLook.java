/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class MeanLook
extends StatusBase {
    transient PixelmonWrapper locker;

    public MeanLook() {
        super(StatusType.MeanLook);
    }

    public MeanLook(PixelmonWrapper locker) {
        super(StatusType.MeanLook);
        this.locker = locker;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.addStatus(new MeanLook(user), user)) {
            user.bc.sendToAll("pixelmon.effect.noescape", target.getNickname());
        } else {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        }
    }

    @Override
    public boolean stopsSwitching() {
        return true;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (this.locker.isFainted() || !pw.bc.isInBattle(this.locker)) {
            pw.removeStatus(this);
        }
    }
}

