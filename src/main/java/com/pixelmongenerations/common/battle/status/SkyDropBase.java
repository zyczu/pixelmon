/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public abstract class SkyDropBase
extends StatusBase {
    public SkyDropBase(StatusType type) {
        super(type);
    }

    @Override
    public boolean stopsSwitching() {
        return true;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (user.attack.isAttack("Gust", "Twister")) {
            user.attack.movePower *= 2;
            return false;
        }
        if (user.attack.moveAccuracy == -2) return false;
        if (user.attack.isAttack("Hurricane", "Sky Uppercut", "Smack Down", "Thunder", "Whirlwind")) return false;
        if (user.bc.simulateMode) return false;
        return true;
    }

    @Override
    public void stopsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.battletext.missedattack", pokemon.getNickname());
    }
}

