/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.ArrayList;

public class Powder
extends StatusBase {
    public Powder() {
        super(StatusType.Powder);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (!target.isImmuneToPowder() && target.addStatus(new Powder(), user)) {
            user.bc.sendToAll("pixelmon.status.powder", target.getNickname());
        } else {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        }
    }

    @Override
    public boolean stopsIncomingAttackUser(PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (user.attack.getAttackBase().attackType == EnumType.Fire) {
            user.bc.sendToAll("pixelmon.status.powderexplode", user.getNickname());
            user.doBattleDamage(user, user.getPercentMaxHealth(25.0f), DamageTypeEnum.STATUS);
            return true;
        }
        return false;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        pw.removeStatus(this);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (MoveChoice.hasOffensiveAttackType(bestOpponentChoices, EnumType.Fire)) {
            userChoice.raiseWeight(25.0f);
        }
    }
}

