/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.ProtectVariation;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class Endure
extends ProtectVariation {
    public Endure() {
        super(StatusType.Endure);
    }

    @Override
    protected boolean addStatus(PixelmonWrapper user) {
        return user.addStatus(new Endure(), user);
    }

    @Override
    protected void displayMessage(PixelmonWrapper user) {
        user.bc.sendToAll("pixelmon.status.endure", user.getNickname());
    }

    @Override
    public boolean stopsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user) {
        return false;
    }

    @Override
    public int modifyDamageIncludeFixed(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        float targetHealth = target.getHealth();
        if ((float)damage >= targetHealth) {
            target.bc.sendToAll("pixelmon.status.endurehit", target.getNickname());
            return (int)targetHealth - 1;
        }
        return damage;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        pw.removeStatus(this);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (pw.getHealth() > 1) {
            if (pw.getMoveset().hasAttack("Flail", "Reversal") && MoveChoice.canOHKO(bestOpponentChoices, pw)) {
                userChoice.raiseWeight(100 / (1 << pw.protectsInARow));
            }
        }
    }
}

