/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.attacks.EffectBase;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.Thaw;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.HarshSunlight;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Sunny;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Comatose;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.TextComponentTranslation;

public class Freeze
extends StatusPersist {
    public Freeze() {
        super(StatusType.Freeze);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (this.checkChance()) {
            Freeze.freeze(user, target);
        }
    }

    public static boolean freeze(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasType(EnumType.Ice) || target.getBattleAbility() instanceof Comatose || target.hasPrimaryStatus()) {
            return false;
        }
        if (target.getHeldItem() != PixelmonItemsHeld.utilityUmbrella && target.bc.globalStatusController.getWeather() instanceof Sunny || target.bc.globalStatusController.getWeather() instanceof HarshSunlight) {
            return false;
        }
        TextComponentTranslation message = ChatHandler.getMessage("pixelmon.effect.frozesolid", target.getNickname());
        return target.addStatus(new Freeze(), user, message);
    }

    @Override
    public boolean canAttackThisTurn(PixelmonWrapper user, Attack a) {
        for (EffectBase e : a.getAttackBase().effects) {
            if (!(e instanceof Thaw)) {
                if (!a.isAttack("Pyro Ball")) continue;
            }
            user.removeStatus(this);
            return true;
        }
        if (RandomHelper.getRandomChance(20)) {
            user.removeStatus(this);
            return true;
        }
        user.bc.sendToAll("pixelmon.status.frozensolid", user.getNickname());
        return false;
    }

    @Override
    public void onDamageReceived(PixelmonWrapper userWrapper, PixelmonWrapper pokemon, Attack a, int damage, DamageTypeEnum damageType) {
        block2: {
            block3: {
                if (damageType != DamageTypeEnum.ATTACK || a == null) break block2;
                if (a.getAttackBase().attackType == EnumType.Fire) break block3;
                if (!a.isAttack("Scald", "Steam Eruption", "Scorching Sands")) break block2;
            }
            pokemon.removeStatus(this);
        }
    }

    @Override
    public StatusPersist restoreFromNBT(NBTTagCompound nbt) {
        return new Freeze();
    }

    @Override
    public boolean isImmune(PixelmonWrapper pokemon) {
        if (pokemon.bc.rules.hasClause("freeze")) {
            for (PixelmonWrapper pw : pokemon.getParticipant().allPokemon) {
                if (pw.getPrimaryStatus().type != StatusType.Freeze) continue;
                return true;
            }
        }
        return pokemon.hasType(EnumType.Ice);
    }

    @Override
    public String getCureMessage() {
        return "pixelmon.status.breakice";
    }

    @Override
    public String getCureMessageItem() {
        return "pixelmon.status.frozencureitem";
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        float weight = this.getWeightWithChance(100.0f);
        if (userChoice.isMiddleTier() && !userChoice.hitsAlly()) {
            for (PixelmonWrapper target : userChoice.targets) {
                if (!Freeze.freeze(pw, target)) continue;
                userChoice.raiseWeight(weight);
            }
        }
    }
}

