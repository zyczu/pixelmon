/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import java.util.ArrayList;

public class Minimize
extends StatusBase {
    private static final String[] squashMoves = new String[]{"Body Slam", "Dragon Rush", "Flying Press", "Phantom Force", "Stomp", "Steamroller"};

    public Minimize() {
        super(StatusType.Minimize);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.getBattleStats().modifyStat(2, StatsType.Evasion)) {
            if (!user.hasStatus(StatusType.Minimize)) {
                user.addStatus(new Minimize(), user);
            }
        }
    }

    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (a.isAttack(squashMoves)) {
            power *= 2;
            accuracy = -1;
        }
        return new int[]{power, accuracy};
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (MoveChoice.hasMove(opponentChoices, squashMoves)) {
            return;
        }
        StatsEffect statsEffect = new StatsEffect(StatsType.Evasion, 2, true);
        statsEffect.weightEffect(pw, userChoice, userChoices, bestUserChoices, opponentChoices, bestOpponentChoices);
    }
}

