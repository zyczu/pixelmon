/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import com.pixelmongenerations.core.network.ChatHandler;
import java.util.ArrayList;
import net.minecraft.util.text.TextComponentTranslation;

public class Taunt
extends StatusBase {
    transient int turnsRemaining = 3;

    public Taunt() {
        super(StatusType.Taunt);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasStatus(this.type)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        } else {
            TextComponentTranslation message = ChatHandler.getMessage("pixelmon.status.taunted", target.getNickname());
            target.addStatus(new Taunt(), user, message);
        }
    }

    @Override
    public boolean canAttackThisTurn(PixelmonWrapper user, Attack a) {
        if (a.getAttackCategory() != AttackCategory.Status) {
            return true;
        }
        user.bc.sendToAll("pixelmon.status.tauntcantuse", user.getNickname(), a.getAttackBase().getLocalizedName());
        return false;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (--this.turnsRemaining == 0) {
            pw.bc.sendToAll("pixelmon.status.tauntnolonger", pw.getNickname());
            pw.removeStatus(this);
            return;
        }
        for (Attack attack : pw.getMoveset()) {
            if (attack == null || attack.getAttackCategory() != AttackCategory.Status) continue;
            attack.setDisabled(true, pw);
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (userChoice.hitsAlly() || MoveChoice.canOutspeedAnd2HKO(bestOpponentChoices, pw, bestUserChoices)) {
            return;
        }
        for (PixelmonWrapper target : userChoice.targets) {
            for (Attack attack : pw.getBattleAI().getMoveset(target)) {
                if (attack.getAttackCategory() != AttackCategory.Status) continue;
                userChoice.raiseWeight(20.0f);
            }
        }
    }
}

