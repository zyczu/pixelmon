/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sleep;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import java.util.ArrayList;

public class Yawn
extends StatusBase {
    transient int effectTurns = 0;

    public Yawn() {
        super(StatusType.Yawn);
    }

    @Override
    public boolean cantMiss(PixelmonWrapper user) {
        return true;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasStatus(StatusType.Sleep)) {
            user.bc.sendToAll("pixelmon.effect.alreadysleeping", target.getNickname());
            user.attack.moveResult.result = AttackResult.failed;
        } else if (target.hasStatus(StatusType.Yawn)) {
            user.bc.sendToAll("pixelmon.effect.alreadydrowsy", target.getNickname());
            user.attack.moveResult.result = AttackResult.failed;
        } else if (!target.hasPrimaryStatus()) {
            if (target.addStatus(new Yawn(), user)) {
                user.bc.sendToAll("pixelmon.effect.becamedrowsy", target.getNickname());
            } else {
                user.attack.moveResult.result = AttackResult.failed;
            }
        } else {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (++this.effectTurns >= 2) {
            if (pw.hasPrimaryStatus()) {
                pw.removeStatus(StatusType.Yawn);
                return;
            }
            if (pw.addStatus(new Sleep(), pw)) {
                pw.bc.sendToAll("pixelmon.status.fellasleep", pw.getNickname());
            }
            pw.removeStatus(this);
        }
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (!userChoice.hitsAlly()) {
            userChoice.raiseWeight(40.0f);
        }
    }
}

