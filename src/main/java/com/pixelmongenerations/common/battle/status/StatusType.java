/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.status.Burn;
import com.pixelmongenerations.common.battle.status.Freeze;
import com.pixelmongenerations.common.battle.status.Paralysis;
import com.pixelmongenerations.common.battle.status.Poison;
import com.pixelmongenerations.common.battle.status.PoisonBadly;
import com.pixelmongenerations.common.battle.status.Sleep;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import java.util.HashMap;
import java.util.Map;

public enum StatusType {
    TypeChange,
    BeakBlastContact,
    LaserFocus,
    BurnUp,
    BanefulBunker,
    Obstruct,
    ShellTrap,
    StompingTantrum,
    AuroraVeil,
    PsychicTerrain,
    Burn,
    Confusion,
    Cursed,
    Infatuated,
    Flee,
    Flinch,
    Flying,
    Freeze,
    Leech,
    LightScreen,
    Mist,
    Paralysis,
    Poison,
    PoisonBadly,
    Protect,
    SafeGuard,
    Sleep,
    SmackedDown,
    Substitute,
    Sunny,
    Wait,
    TrickRoom,
    Perish,
    Yawn,
    Disable,
    Immobilize,
    Recharge,
    AquaRing,
    UnderGround,
    Transformed,
    MeanLook,
    JawLock,
    FutureSight,
    MagnetRise,
    Spikes,
    ToxicSpikes,
    StealthRock,
    PartialTrap,
    Reflect,
    Submerged,
    Raging,
    Telekinesis,
    Tailwind,
    DestinyBond,
    Taunt,
    TempMoveset,
    HealingWish,
    Roosting,
    Wish,
    Encore,
    Focus,
    MagicCoat,
    Ingrain,
    Stockpile,
    Snatch,
    Minimize,
    Gravity,
    Hail,
    Rainy,
    Sandstorm,
    Torment,
    Foresight,
    MiracleEye,
    GastroAcid,
    GuardSplit,
    PowerSplit,
    WonderRoom,
    LockOn,
    Endure,
    WideGuard,
    Charge,
    Nightmare,
    MeFirst,
    PowerTrick,
    Autotomize,
    DefenseCurl,
    SkyDropping,
    SkyDropped,
    FollowMe,
    Enrage,
    Imprison,
    HealBlock,
    MudSport,
    WaterSport,
    FirePledge,
    GrassPledge,
    WaterPledge,
    HelpingHand,
    QuickGuard,
    Embargo,
    Grudge,
    LuckyChant,
    MagicRoom,
    LunarDance,
    Vanish,
    MultiTurn,
    Bide,
    Uproar,
    EchoedVoice,
    FuryCutter,
    DarkAura,
    FairyAura,
    CraftyShield,
    None,
    ElectricTerrain,
    Electrify,
    FairyLock,
    NoRetreat,
    GrassyTerrain,
    IonDeluge,
    KingsShield,
    MatBlock,
    MistyTerrain,
    Powder,
    SpikyShield,
    StickyWeb,
    TerrainRemoval,
    TarShot,
    Octolock,
    Steelsurge,
    PlasmaFists,
    StrangeWinds,
    Intimidated;

    static Map<StatusType, StatusPersist> restoreStatusList;

    public boolean isStatus(StatusType ... statuses) {
        for (StatusType status : statuses) {
            if (this != status) continue;
            return true;
        }
        return false;
    }

    public static StatusType getStatusEffect(String string) {
        for (StatusType t : StatusType.values()) {
            if (!t.toString().equalsIgnoreCase(string)) continue;
            return t;
        }
        return null;
    }

    public static boolean isStatusEffect(String string) {
        return StatusType.getStatusEffect(string) != null;
    }

    public static StatusType getEffect(int integer) {
        StatusType[] statuses = StatusType.values();
        if (integer >= 0 && integer < statuses.length) {
            return statuses[integer];
        }
        return null;
    }

    public static StatusPersist getEffectInstance(int integer) {
        StatusType type = StatusType.getEffect(integer);
        if (type != null) {
            return restoreStatusList.get((Object)type);
        }
        return null;
    }

    public static boolean isPrimaryStatus(StatusType status) {
        return status.isStatus(Poison, Burn, PoisonBadly, Freeze, Sleep, Paralysis);
    }

    public boolean isPrimaryStatus() {
        return StatusType.isPrimaryStatus(this);
    }

    public static float[] getTexturePos(StatusType type) {
        if (type == Burn) {
            return new float[]{1.0f, 1.0f};
        }
        if (type == Freeze) {
            return new float[]{1.0f, 72.0f};
        }
        if (type == Paralysis) {
            return new float[]{152.0f, 1.0f};
        }
        if (type == Poison || type == PoisonBadly) {
            return new float[]{152.0f, 72.0f};
        }
        if (type == Sleep) {
            return new float[]{152.0f, 142.0f};
        }
        return new float[]{-1.0f, -1.0f};
    }

    public static boolean isNonVolatile(StatusType type) {
        return type == Burn || type == Freeze || type == Paralysis || type == Poison || type == PoisonBadly || type == Sleep || type == Yawn;
    }

    static {
        restoreStatusList = new HashMap<StatusType, StatusPersist>(6);
        restoreStatusList.put(Burn, new Burn());
        restoreStatusList.put(Freeze, new Freeze());
        restoreStatusList.put(Paralysis, new Paralysis());
        restoreStatusList.put(Poison, new Poison());
        restoreStatusList.put(PoisonBadly, new PoisonBadly());
        restoreStatusList.put(Sleep, new Sleep());
    }
}

