/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.GlobalStatusController;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.log.AttackResult;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Levitate;
import com.pixelmongenerations.common.item.heldItems.ItemTerrainExtender;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;

public abstract class Terrain
extends GlobalStatusBase {
    public transient int turnsToGo = 5;
    protected transient String langStart;
    protected transient String langContinue;
    protected transient String langEnd;

    public Terrain(StatusType type, String langStart, String langContinue, String langEnd) {
        super(type);
        this.langStart = langStart;
        this.langContinue = langContinue;
        this.langEnd = langEnd;
    }

    @Override
    public void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        Terrain terrain = user.bc.globalStatusController.getTerrain();
        if (user.getHeldItem() instanceof ItemTerrainExtender) {
            this.turnsToGo = 8;
        }
        if (terrain != null && terrain.type == this.type) {
            user.bc.sendToAll("pixelmon.effect.effectfailed", new Object[0]);
            user.attack.moveResult.result = AttackResult.failed;
        } else {
            if (terrain.type != this.type) {
                user.bc.globalStatusController.removeGlobalStatus(terrain.type);
            }
            user.bc.sendToAll(this.langStart, new Object[0]);
            user.bc.globalStatusController.addGlobalStatus(this.getNewInstance());
        }
    }

    public abstract Terrain getNewInstance();

    @Override
    public void applyRepeatedEffect(GlobalStatusController global) {
        if (this.turnsToGo != -1) {
            int simulate = this.turnsToGo - 1;
            if (simulate == 0) {
                global.removeGlobalStatus(this);
                global.bc.sendToAll(this.langEnd, new Object[0]);
                return;
            }
            this.turnsToGo = simulate;
        }
        global.bc.sendToAll(this.langContinue, new Object[0]);
        this.applyRepeatedEffect(global.bc);
    }

    protected void applyRepeatedEffect(BattleControllerBase bc) {
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        int allyBenefits = 0;
        int opponentBenefits = 0;
        Terrain currentTerrain = pw.bc.globalStatusController.getTerrain();
        for (PixelmonWrapper ally : pw.getTeamPokemon()) {
            allyBenefits += this.countBenefits(pw, ally);
            allyBenefits -= currentTerrain.countBenefits(pw, ally);
        }
        for (PixelmonWrapper opponent : pw.getOpponentPokemon()) {
            opponentBenefits += this.countBenefits(pw, opponent);
            opponentBenefits -= currentTerrain.countBenefits(pw, opponent);
        }
        if (allyBenefits > opponentBenefits) {
            userChoice.raiseWeight(40 * (allyBenefits - opponentBenefits));
        }
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    protected boolean affectsPokemon(PixelmonWrapper pw) {
        if (!pw.isGrounded()) {
            if (pw.type.contains((Object)EnumType.Flying)) return true;
            if (pw.getBattleAbility() instanceof Levitate) return true;
            if (pw.getUsableHeldItem().getHeldItemType() == EnumHeldItems.airBalloon) return true;
            if (pw.hasStatus(StatusType.MagnetRise, StatusType.Telekinesis)) {
                return true;
            }
        }
        if (pw.isAirborne()) return false;
        if (pw.hasStatus(StatusType.Flying, StatusType.UnderGround, StatusType.Submerged, StatusType.Vanish)) return false;
        return true;
    }

    protected abstract int countBenefits(PixelmonWrapper var1, PixelmonWrapper var2);
}

