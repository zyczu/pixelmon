/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.GlobalStatusController;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;

public class MagicRoom
extends GlobalStatusBase {
    private transient int duration = 5;

    public MagicRoom() {
        super(StatusType.MagicRoom);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.bc.globalStatusController.removeGlobalStatus(StatusType.MagicRoom)) {
            user.bc.sendToAll("pixelmon.status.magicroomend", new Object[0]);
        } else {
            user.bc.sendToAll("pixelmon.status.magicroom", user.getNickname());
            user.bc.globalStatusController.addGlobalStatus(new MagicRoom());
        }
    }

    @Override
    public void applyRepeatedEffect(GlobalStatusController globalStatusController) {
        if (--this.duration <= 0) {
            globalStatusController.bc.sendToAll("pixelmon.status.magicroomend", new Object[0]);
            globalStatusController.removeGlobalStatus(this);
        }
    }
}

