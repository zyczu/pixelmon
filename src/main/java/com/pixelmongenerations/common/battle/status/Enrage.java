/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.ai.MoveChoice;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Overcoat;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PropellerTail;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Stalwart;
import java.util.ArrayList;

public class Enrage
extends StatusBase {
    private transient boolean isPowder;

    public Enrage() {
        this(false);
    }

    public Enrage(boolean isPowder) {
        super(StatusType.Enrage);
        this.isPowder = isPowder;
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        user.bc.sendToAll("pixelmon.status.followme", user.getNickname());
        user.addStatus(new Enrage(Overcoat.isPowderMove(user.attack)), user);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    @Override
    public boolean redirectAttack(PixelmonWrapper user, PixelmonWrapper targetAlly, Attack attack) {
        if (user.getAbility() instanceof PropellerTail || user.getAbility() instanceof Stalwart) {
            user.bc.sendToAll("pixelmon.status.propellertail", user.getNickname());
            return false;
        }
        if (this.isPowder) {
            if (user.isImmuneToPowder()) return false;
        }
        if (attack.isAttack("Bide", "Bounce", "Dig", "Dive", "Fly", "Freeze Shock", "Ice Burn", "Phantom Force", "Razor Wind", "Shadow Force", "Skull Bash", "Sky Attack", "Sky Drop", "Solar Beam", "Future Sight", "Doom Desire")) return false;
        return true;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        pw.removeStatus(this);
    }

    @Override
    public void weightEffect(PixelmonWrapper pw, MoveChoice userChoice, ArrayList<MoveChoice> userChoices, ArrayList<MoveChoice> bestUserChoices, ArrayList<MoveChoice> opponentChoices, ArrayList<MoveChoice> bestOpponentChoices) {
        if (pw.bc.rules.battleType.numPokemon != 1) {
            for (PixelmonWrapper ally : pw.getTeamPokemonExcludeSelf()) {
                if (MoveChoice.hasSpreadMove(bestOpponentChoices) || MoveChoice.getTargetedChoices(ally, bestOpponentChoices).isEmpty()) continue;
                userChoice.raiseWeight(25.0f);
                break;
            }
        }
    }
}

