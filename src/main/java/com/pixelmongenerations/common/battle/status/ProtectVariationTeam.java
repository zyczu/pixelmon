/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.ProtectVariation;
import com.pixelmongenerations.common.battle.status.StatusType;

public abstract class ProtectVariationTeam
extends ProtectVariation {
    public ProtectVariationTeam(StatusType type) {
        super(type);
    }

    @Override
    protected boolean addStatus(PixelmonWrapper user) {
        return user.addTeamStatus(this.getNewInstance(), user);
    }

    @Override
    protected void applyEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.targetIndex == 0 || user.bc.simulateMode) {
            super.applyEffect(user, target);
        }
    }

    @Override
    public boolean isTeamStatus() {
        return true;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        pw.removeTeamStatus(this);
    }

    @Override
    protected boolean canFail() {
        return false;
    }

    public abstract ProtectVariationTeam getNewInstance();
}

