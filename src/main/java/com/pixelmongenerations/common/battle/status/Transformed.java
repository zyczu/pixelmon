/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.status;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.TempMoveset;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Illusion;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.network.packetHandlers.Transform;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class Transformed
extends StatusBase {
    private transient int tempAttack;
    private transient int tempDefence;
    private transient int tempSpecialAttack;
    private transient int tempSpecialDefence;
    private transient int tempSpeed;
    private transient Moveset moveset;

    public Transformed(PixelmonWrapper user, PixelmonWrapper target) {
        super(StatusType.Transformed);
        int[] targetStats = target.getBattleStats().getBaseBattleStats();
        this.tempAttack = targetStats[StatsType.Attack.getStatIndex()];
        this.tempDefence = targetStats[StatsType.Defence.getStatIndex()];
        this.tempSpecialAttack = targetStats[StatsType.SpecialAttack.getStatIndex()];
        this.tempSpecialDefence = targetStats[StatsType.SpecialDefence.getStatIndex()];
        this.tempSpeed = targetStats[StatsType.Speed.getStatIndex()];
        user.getBattleStats().copyStatsTransform(target.getBattleStats());
        if (!(target.getBattleAbility(false) instanceof Illusion)) {
            user.setTempAbility(target.getBattleAbility(false));
        }
        List<EnumType> newType = target.type;
        user.setTempType(newType);
        this.moveset = new Moveset(user.getInnerLink());
        Moveset targetMoveset = target.getMoveset();
        for (Attack attack : targetMoveset) {
            Attack a = new Attack(attack.getAttackBase());
            if (a.pp != 1) {
                a.pp = 5;
            }
            this.moveset.add(a);
        }
        user.addStatus(new TempMoveset(this.moveset), user);
    }

    @SideOnly(value=Side.CLIENT)
    public static void applyToClientEntity(Transform p) {
        Minecraft mc = Minecraft.getMinecraft();
        WorldClient world = mc.world;
        for (int i = 0; i < world.loadedEntityList.size(); ++i) {
            Entity e = (Entity)world.loadedEntityList.get(i);
            if (!(e instanceof EntityPixelmon) || e.getEntityId() != p.pixelmonID) continue;
            EntityPixelmon pokemon = (EntityPixelmon)e;
            pokemon.transform(p.transformedModel, p.transformedForm);
            IResourceManager resourceManager = mc.getResourceManager();
            try {
                resourceManager.getResource(new ResourceLocation(p.transformedTexture));
                pokemon.transformTexture(p.transformedTexture);
            }
            catch (Exception exception) {
                pokemon.transformTexture(p.transformedTexture.replace("pokemon-shiny/shiny", ""));
            }
            break;
        }
    }

    @Override
    public void applyBeforeEffect(PixelmonWrapper user, PixelmonWrapper target) {
        user.pokemon.transformServer(target.getSpecies(), target.getForm(), target.pokemon.getTextureNoCheck().toString());
    }

    @Override
    public void applyEndOfBattleEffect(PixelmonWrapper pokemon) {
        pokemon.pokemon.cancelTransform();
    }

    @Override
    public int[] modifyBaseStats(PixelmonWrapper user, int[] stats) {
        stats[StatsType.Attack.getStatIndex()] = this.tempAttack;
        stats[StatsType.Defence.getStatIndex()] = this.tempDefence;
        stats[StatsType.SpecialAttack.getStatIndex()] = this.tempSpecialAttack;
        stats[StatsType.SpecialDefence.getStatIndex()] = this.tempSpecialDefence;
        stats[StatsType.Speed.getStatIndex()] = this.tempSpeed;
        return stats;
    }
}

