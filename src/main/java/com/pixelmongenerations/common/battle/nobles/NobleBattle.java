/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.nobles;

import com.pixelmongenerations.common.battle.nobles.NobleBattlePlayer;
import com.pixelmongenerations.common.battle.nobles.NobleBattlePokemon;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.BossInfo;
import net.minecraft.world.BossInfoServer;

public class NobleBattle {
    public static Map<UUID, NobleBattle> nobleBattleMap = new HashMap<UUID, NobleBattle>();
    private NobleBattlePlayer player;
    private NobleBattlePokemon pokemon;
    private BossInfoServer frenzyGauge;
    public float currentHealth;
    private float maxHealth;

    public NobleBattle(NobleBattlePlayer player, NobleBattlePokemon pokemon) {
        this.player = player;
        this.pokemon = pokemon;
        String name = this.pokemon.getEntityPixelmon().getForm() == 11 ? "hisuian" + this.getPokemon().getEntityPixelmon().getPokemonName().toLowerCase() : this.getPokemon().getEntityPixelmon().getPokemonName().toLowerCase();
        this.frenzyGauge = new BossInfoServer(new TextComponentTranslation("pixelmon." + name + ".title", new Object[0]).setStyle(new Style().setColor(TextFormatting.YELLOW)), BossInfo.Color.RED, BossInfo.Overlay.PROGRESS);
        nobleBattleMap.put(this.player.getEntityPlayer().getUniqueID(), this);
    }

    public NobleBattlePlayer getPlayer() {
        return this.player;
    }

    public NobleBattlePokemon getPokemon() {
        return this.pokemon;
    }

    public BossInfoServer getFrenzyGauge() {
        return this.frenzyGauge;
    }

    public float getCurrentHealth() {
        return this.currentHealth;
    }

    public float getMaxHealth() {
        return this.maxHealth;
    }

    public void startBattle() {
        this.maxHealth = this.getPokemon().getEntityPixelmon().stats.HP;
        this.currentHealth = this.getPokemon().getEntityPixelmon().stats.HP;
        this.frenzyGauge.addPlayer(this.player.getEntityPlayer());
        this.frenzyGauge.setPercent(this.currentHealth / this.maxHealth);
        this.frenzyGauge.setVisible(true);
    }

    public void updateFrenzyGauge() {
        this.frenzyGauge.setPercent(this.currentHealth / this.maxHealth);
    }
}

