/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.nobles;

import net.minecraft.entity.player.EntityPlayerMP;

public class NobleBattlePlayer {
    private EntityPlayerMP player;

    public NobleBattlePlayer(EntityPlayerMP player) {
        this.player = player;
    }

    public EntityPlayerMP getEntityPlayer() {
        return this.player;
    }
}

