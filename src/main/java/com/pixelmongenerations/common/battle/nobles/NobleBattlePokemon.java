/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.nobles;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;

public class NobleBattlePokemon {
    private EntityPixelmon pokemon;

    public NobleBattlePokemon(EntityPixelmon pokemon) {
        this.pokemon = pokemon;
    }

    public EntityPixelmon getEntityPixelmon() {
        return this.pokemon;
    }
}

