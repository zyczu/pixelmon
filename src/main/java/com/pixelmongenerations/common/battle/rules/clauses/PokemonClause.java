/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses;

import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.ArrayHelper;

public class PokemonClause
extends BattleClause {
    private PokemonForm[] pokemon;
    private boolean validateForm = true;

    public PokemonClause(String id, EnumSpecies ... pokemon) {
        this(id, PokemonForm.convertEnumArray(pokemon));
        this.validateForm = false;
    }

    public PokemonClause(String id, PokemonForm ... pokemon) {
        super(id);
        ArrayHelper.validateArrayNonNull(pokemon);
        this.pokemon = pokemon;
    }

    @Override
    public boolean validateSingle(PokemonLink pokemon) {
        for (PokemonForm pokemonForm : this.pokemon) {
            if (pokemonForm.pokemon != pokemon.getBaseStats().pokemon || this.validateForm && pokemonForm.form != pokemon.getForm()) continue;
            return false;
        }
        return true;
    }
}

