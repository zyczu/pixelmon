/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonArray
 *  com.google.gson.JsonObject
 *  com.google.gson.JsonParser
 */
package com.pixelmongenerations.common.battle.rules.clauses.tiers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClauseRegistry;
import com.pixelmongenerations.common.battle.rules.clauses.tiers.EnumTier;
import com.pixelmongenerations.common.battle.rules.clauses.tiers.TierAllowedSet;
import com.pixelmongenerations.common.battle.rules.clauses.tiers.TierSet;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class RulesRegistry {
    private static final String ASSETS_FOLDER = File.separator + "assets";
    private static final String JSON_FOLDER = File.separator + "pixelmon";
    private static final String RULES_FOLDER = File.separator + "rules";
    private static final String TIER_FOLDER = File.separator + "tiers";
    private static final String CUSTOM_FOLDER = File.separator + "custom";
    private static final String RULESET_FOLDER = File.separator + "rulesets";
    private static final String JSON_EXTENSION = ".json";
    private static final String BATTLE_SPOT_FILE = CUSTOM_FOLDER.substring(1) + File.separator + "battlespot" + ".json";

    private RulesRegistry() {
    }

    public static void registerRules() {
        Pixelmon.LOGGER.info("Registering battle rules.");
        String path = RulesRegistry.getExternalRulesFolder();
        File rulesDir = new File(path);
        boolean useExternal = PixelmonConfig.useExternalJSONFilesRules;
        if (useExternal && !rulesDir.isDirectory()) {
            File baseDir = new File(Pixelmon.modDirectory + JSON_FOLDER);
            if (!baseDir.isDirectory()) {
                baseDir.mkdir();
            }
            Pixelmon.LOGGER.info("Creating rules directory.");
            rulesDir.mkdir();
        }
        RulesRegistry.registerTiers();
        RulesRegistry.registerRulesets();
    }

    private static void registerTiers() {
        String path = RulesRegistry.getExternalTierFolder();
        File tierDir = new File(path);
        boolean useExternal = PixelmonConfig.useExternalJSONFilesRules;
        File customFile = new File(path + CUSTOM_FOLDER);
        if (useExternal && !tierDir.isDirectory()) {
            tierDir.mkdir();
            customFile.mkdir();
            RulesRegistry.extractTierDirs(tierDir);
        }
        for (EnumTier tier : EnumTier.values()) {
            if (tier == EnumTier.PU) continue;
            try {
                RulesRegistry.registerTier(tierDir, tier, useExternal);
            }
            catch (IOException e) {
                RulesRegistry.printError(RulesRegistry.getTierJSONName(tier), e);
            }
        }
        BattleClauseRegistry.getTierRegistry().removeCustomClauses();
        if (useExternal) {
            File[] fileList = customFile.listFiles();
            if (fileList == null) {
                fileList = new File[]{};
            }
            for (File file : fileList) {
                String fileName = file.getName();
                if (!fileName.endsWith(JSON_EXTENSION)) continue;
                try {
                    RulesRegistry.registerTier(customFile, fileName, useExternal);
                }
                catch (IOException e) {
                    RulesRegistry.printError(fileName, e);
                }
            }
        } else {
            try {
                RulesRegistry.registerTier(tierDir, BATTLE_SPOT_FILE, useExternal);
            }
            catch (IOException e) {
                RulesRegistry.printError(BATTLE_SPOT_FILE, e);
            }
        }
    }

    private static void registerRulesets() {
        String path = RulesRegistry.getExternalRulesetFolder();
        File rulesetDir = new File(path);
        boolean useExternal = PixelmonConfig.useExternalJSONFilesRules;
        if (useExternal && !rulesetDir.isDirectory()) {
            rulesetDir.mkdir();
        }
    }

    private static void printError(String tierFile, Exception e) {
        Pixelmon.LOGGER.error("Error occurred when reading tier file: " + tierFile + ".");
        e.printStackTrace();
    }

    private static void registerTier(File tierDir, String tierFileName, boolean useExternal) throws IOException {
        RulesRegistry.registerTier(tierDir, null, tierFileName, true, useExternal);
    }

    private static void registerTier(File tierDir, EnumTier tier, boolean useExternal) throws IOException {
        RulesRegistry.registerTier(tierDir, tier, RulesRegistry.getTierJSONName(tier), false, useExternal);
    }

    private static void registerTier(File tierDir, EnumTier tier, String tierFile, boolean isCustom, boolean useExternal) throws IOException {
        block20: {
            boolean isBattleSpotFile = tierFile.equals(BATTLE_SPOT_FILE);
            try (InputStream iStream = useExternal ? new FileInputStream(new File(tierDir, tierFile)) : ServerNPCRegistry.class.getResourceAsStream(!isBattleSpotFile ? RulesRegistry.getInternalTierFolder() + "/" + tierFile : RulesRegistry.getBattleSpotFile());){
                JsonObject json = new JsonParser().parse((Reader)new InputStreamReader(iStream, StandardCharsets.UTF_8)).getAsJsonObject();
                if (isCustom) {
                    String id = tierFile;
                    int lastPeriod = id.lastIndexOf(46);
                    if (lastPeriod > -1) {
                        id = id.substring(0, lastPeriod);
                    }
                    RulesRegistry.loadCustomTier(id, json);
                } else if (useExternal && !json.get("usecustom").getAsBoolean()) {
                    RulesRegistry.registerTier(tierDir, tier, tierFile, isCustom, false);
                } else {
                    RulesRegistry.loadDefaultTier(tier, json);
                }
            }
            catch (IOException e) {
                throw e;
            }
            catch (Exception e) {
                RulesRegistry.printError(tierFile, e);
                if (!useExternal || isCustom) break block20;
                Pixelmon.LOGGER.info("Loading internal tier file instead of external tier file.");
                RulesRegistry.registerTier(tierDir, tier, tierFile, isCustom, false);
            }
        }
    }

    private static void loadDefaultTier(EnumTier tier, JsonObject json) {
        TierSet tierClause = (TierSet)BattleClauseRegistry.getTierRegistry().getClause(tier.getTierID());
        tierClause.setPokemon(RulesRegistry.getPokemonSet(json));
    }

    private static void loadCustomTier(String id, JsonObject json) {
        TierAllowedSet tier = new TierAllowedSet(id, RulesRegistry.getPokemonSet(json), json.get("banlist").getAsBoolean());
        BattleClauseRegistry.getTierRegistry().registerCustomClause(tier);
    }

    private static Set<PokemonForm> getPokemonSet(JsonObject json) {
        HashSet<PokemonForm> pokemonSet = new HashSet<PokemonForm>();
        JsonArray pokemonJSON = json.get("pokemon").getAsJsonArray();
        for (int i = 0; i < pokemonJSON.size(); ++i) {
            String pokemon = pokemonJSON.get(i).getAsString();
            Optional<PokemonForm> form = PokemonForm.getFromName(pokemon);
            if (form.isPresent()) {
                pokemonSet.add(form.get());
                continue;
            }
            Pixelmon.LOGGER.warn("Pok\u00e9mon not found when registering tier: " + pokemon + ".");
        }
        return pokemonSet;
    }

    private static void extractTierDirs(File tierDir) {
        for (EnumTier tier : EnumTier.values()) {
            if (tier == EnumTier.PU) continue;
            RulesRegistry.extractTierDir(tierDir, RulesRegistry.getTierJSONName(tier));
        }
        RulesRegistry.extractTierDir(tierDir, BATTLE_SPOT_FILE);
    }

    private static void extractTierDir(File tierDir, String fileName) {
        ServerNPCRegistry.extractFile((RulesRegistry.getInternalTierFolder() + File.separator + fileName).replace(File.separator, "/"), tierDir, fileName);
    }

    private static String getExternalRulesFolder() {
        return Pixelmon.modDirectory + JSON_FOLDER + RULES_FOLDER;
    }

    private static String getExternalTierFolder() {
        return RulesRegistry.getExternalRulesFolder() + TIER_FOLDER;
    }

    private static String getExternalRulesetFolder() {
        return RulesRegistry.getExternalRulesFolder() + RULESET_FOLDER;
    }

    private static String getInternalTierFolder() {
        return (ASSETS_FOLDER + JSON_FOLDER + RULES_FOLDER + TIER_FOLDER).replace(File.separator, "/");
    }

    private static String getBattleSpotFile() {
        return (ASSETS_FOLDER + JSON_FOLDER + RULES_FOLDER + TIER_FOLDER + "/" + BATTLE_SPOT_FILE).replace(File.separator, "/");
    }

    private static String getTierJSONName(EnumTier tier) {
        return tier.name().toLowerCase() + JSON_EXTENSION;
    }
}

