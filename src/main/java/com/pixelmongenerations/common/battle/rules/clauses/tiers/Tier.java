/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses.tiers;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.ItemMegaStone;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

public class Tier
extends BattleClause {
    protected Predicate<PokemonForm> condition;
    private static final Set<EnumSpecies> ignoreForms = new HashSet<EnumSpecies>(Arrays.asList(new EnumSpecies[]{EnumSpecies.Burmy, EnumSpecies.Castform, EnumSpecies.Gastrodon, EnumSpecies.Shellos, EnumSpecies.Unown, EnumSpecies.Deerling, EnumSpecies.Sawsbuck, EnumSpecies.Keldeo, EnumSpecies.Greninja, EnumSpecies.Florges, EnumSpecies.Flabebe, EnumSpecies.Floette, EnumSpecies.Furfrou, EnumSpecies.Wishiwashi, EnumSpecies.Minior, EnumSpecies.Aegislash, EnumSpecies.Vivillon, EnumSpecies.Meloetta, EnumSpecies.Alcremie, EnumSpecies.Pikachu, EnumSpecies.Basculin, EnumSpecies.Darmanitan, EnumSpecies.Cherrim, EnumSpecies.Cramorant, EnumSpecies.Sinistea, EnumSpecies.Polteageist, EnumSpecies.Eiscue, EnumSpecies.Morpeko, EnumSpecies.Eternatus}));

    public Tier(String id) {
        this(id, p -> true);
    }

    public Tier(String id, Predicate<PokemonForm> condition) {
        super(id);
        this.condition = condition;
    }

    @Override
    public final boolean validateSingle(PokemonLink pokemon) {
        ItemHeld heldItem;
        PokemonForm pokemonForm = pokemon.getPokemonForm();
        EnumSpecies species = pokemonForm.pokemon;
        if (pokemonForm.form != -1 && (ignoreForms.contains((Object)species) || !Entity3HasStats.hasForms(species))) {
            pokemonForm.form = -1;
        }
        if (PixelmonWrapper.canMegaEvolve(heldItem = pokemon.getHeldItem(), species, pokemonForm.form) && heldItem instanceof ItemMegaStone) {
            ItemMegaStone megaStone = (ItemMegaStone)heldItem;
            pokemonForm.form = megaStone.form;
        }
        return this.condition.test(pokemonForm);
    }

    @Override
    public final boolean validateTeam(List<PokemonLink> team) {
        return super.validateTeam(team);
    }

    public String getTierDescription() {
        return this.getLocalizedName();
    }
}

