/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.teamselection;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.NBTLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.nbt.NBTTagCompound;

class ParticipantSelection {
    final PlayerStorage storage;
    List<PokemonLink> team = new ArrayList<PokemonLink>();
    final boolean isNPC;
    boolean confirmed;
    String[] disabled = new String[6];

    ParticipantSelection(PlayerStorage storage) {
        this.storage = storage;
        this.isNPC = storage.trainer != null;
        boolean bl = this.isNPC;
        if (this.isNPC) {
            this.confirmed = true;
        }
    }

    void setTeam(int[] selection) {
        ArrayList<PokemonLink> team = new ArrayList<PokemonLink>();
        for (int s : selection) {
            if (s <= -1) continue;
            team.add(null);
        }
        for (int i = 0; i < selection.length; ++i) {
            int s = selection[i];
            if (s <= -1) continue;
            NBTTagCompound nbt = this.storage.partyPokemon[i];
            if (nbt == null) {
                return;
            }
            team.set(s, new NBTLink(nbt, this.storage));
        }
        for (PokemonLink link : team) {
            if (link != null) continue;
            return;
        }
        this.team = team;
    }

    void addTeamMember(int index) {
        NBTTagCompound nbt = this.storage.partyPokemon[index];
        if (PlayerStorage.canBattle(nbt)) {
            this.team.add(new NBTLink(nbt, this.storage));
        }
    }

    void removeTeamMember() {
        if (!this.team.isEmpty()) {
            this.team.remove(this.team.size() - 1);
        }
    }
}

