/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 *  io.netty.buffer.Unpooled
 */
package com.pixelmongenerations.common.battle.rules;

import com.pixelmongenerations.client.gui.pokemoneditor.ImportExportConverter;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClauseRegistry;
import com.pixelmongenerations.common.battle.rules.clauses.tiers.EnumTier;
import com.pixelmongenerations.common.battle.rules.clauses.tiers.Tier;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import com.pixelmongenerations.core.util.IEncodeable;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class BattleRules
implements IEncodeable {
    public int levelCap = PixelmonServerConfig.maxLevel;
    public boolean raiseToCap;
    public EnumBattleType battleType = EnumBattleType.Single;
    public int numPokemon = 6;
    public int turnTime;
    public int teamSelectTime;
    public boolean teamPreview;
    public boolean fullHeal;
    public Tier tier;
    private Set<BattleClause> clauses = new HashSet<BattleClause>();
    public static final int MAX_NUM_POKEMON = 6;
    private static final String LEVEL_CAP_TEXT = "LevelCap";
    private static final String RAISE_TO_CAP_TEXT = "RaiseToCap";
    private static final String BATTLE_TYPE_TEXT = "BattleType";
    private static final String NUM_POKEMON_TEXT = "NumPokemon";
    private static final String TURN_TIME_TEXT = "TurnTime";
    private static final String TEAM_SELECT_TIME_TEXT = "TeamSelectTime";
    private static final String TEAM_PREVIEW_TEXT = "TeamPreview";
    private static final String FULL_HEAL_TEXT = "FullHeal";
    private static final String TIER_TEXT = "Tier";
    private static final String CLAUSES_TEXT = "Clauses";

    public BattleRules() {
        this.tier = this.getDefaultTier();
    }

    public BattleRules(EnumBattleType battleType) {
        this();
        this.battleType = battleType;
    }

    public BattleRules(String text) {
        this.importText(text);
    }

    public BattleRules(ByteBuf buf) {
        this.decodeInto(buf);
    }

    public void validateRules() {
        this.levelCap = MathHelper.clamp(this.levelCap, 1, PixelmonServerConfig.maxLevel);
        if (this.battleType.numPokemon > 2) {
            this.battleType = EnumBattleType.Single;
        }
        this.numPokemon = MathHelper.clamp(this.numPokemon, this.battleType.numPokemon, 6);
        this.turnTime = Math.max(this.turnTime, 0);
        if (PixelmonServerConfig.afkHandlerOn) {
            this.turnTime = Math.min(this.turnTime, PixelmonServerConfig.afkTimerActivateSeconds);
        }
        this.teamSelectTime = Math.max(this.teamSelectTime, 0);
    }

    public String exportText() {
        StringBuilder builder = new StringBuilder();
        if (this.levelCap < PixelmonServerConfig.maxLevel) {
            ImportExportConverter.addColonSeparated(builder, LEVEL_CAP_TEXT, this.levelCap);
        }
        if (this.raiseToCap) {
            ImportExportConverter.addLine(builder, RAISE_TO_CAP_TEXT);
        }
        if (this.battleType != EnumBattleType.Single) {
            ImportExportConverter.addColonSeparated(builder, BATTLE_TYPE_TEXT, this.battleType.toString());
        }
        if (this.numPokemon < 6) {
            ImportExportConverter.addColonSeparated(builder, NUM_POKEMON_TEXT, this.numPokemon);
        }
        if (this.turnTime > 0) {
            ImportExportConverter.addColonSeparated(builder, TURN_TIME_TEXT, this.turnTime);
        }
        if (this.teamSelectTime > 0) {
            ImportExportConverter.addColonSeparated(builder, TEAM_SELECT_TIME_TEXT, this.teamSelectTime);
        }
        if (this.teamPreview) {
            ImportExportConverter.addLine(builder, TEAM_PREVIEW_TEXT);
        }
        if (this.fullHeal) {
            ImportExportConverter.addLine(builder, FULL_HEAL_TEXT);
        }
        if (this.tier != this.getDefaultTier()) {
            ImportExportConverter.addColonSeparated(builder, TIER_TEXT, this.tier.getID());
        }
        if (!this.clauses.isEmpty()) {
            ImportExportConverter.addLine(builder, CLAUSES_TEXT);
            List<String> clauseIDs = this.getClauseIDs();
            for (String clauseID : clauseIDs) {
                ImportExportConverter.addLine(builder, clauseID);
            }
        }
        return builder.toString();
    }

    public String importText(String text) {
        String currentSection = "Format";
        String[] importTextSplit = text.split("\n");
        BattleRules newRules = new BattleRules();
        try {
            int i = 0;
            boolean hasClauses = false;
            for (String line : importTextSplit) {
                if (line.startsWith(LEVEL_CAP_TEXT)) {
                    currentSection = LEVEL_CAP_TEXT;
                    newRules.levelCap = ImportExportConverter.getIntAfterColon(line);
                } else if (line.equals(RAISE_TO_CAP_TEXT)) {
                    newRules.raiseToCap = true;
                } else if (line.startsWith(BATTLE_TYPE_TEXT)) {
                    currentSection = BATTLE_TYPE_TEXT;
                    newRules.battleType = EnumBattleType.valueOf(ImportExportConverter.getStringAfterColon(line));
                } else if (line.startsWith(NUM_POKEMON_TEXT)) {
                    currentSection = NUM_POKEMON_TEXT;
                    newRules.numPokemon = ImportExportConverter.getIntAfterColon(line);
                } else if (line.startsWith(TURN_TIME_TEXT)) {
                    currentSection = TURN_TIME_TEXT;
                    newRules.turnTime = ImportExportConverter.getIntAfterColon(line);
                } else if (line.startsWith(TEAM_SELECT_TIME_TEXT)) {
                    currentSection = TEAM_SELECT_TIME_TEXT;
                    newRules.teamSelectTime = ImportExportConverter.getIntAfterColon(line);
                } else if (line.equals(TEAM_PREVIEW_TEXT)) {
                    newRules.teamPreview = true;
                } else if (line.equals(FULL_HEAL_TEXT)) {
                    newRules.fullHeal = true;
                } else if (line.startsWith(TIER_TEXT)) {
                    currentSection = TIER_TEXT;
                    String tierID = ImportExportConverter.getStringAfterColon(line);
                    Tier tier = BattleClauseRegistry.getTierRegistry().getClause(tierID);
                    if (tier == null) {
                        tier = this.getDefaultTier();
                    }
                    newRules.tier = tier;
                } else if (line.equals(CLAUSES_TEXT)) {
                    currentSection = CLAUSES_TEXT;
                    hasClauses = true;
                }
                ++i;
                if (hasClauses) break;
            }
            if (hasClauses) {
                while (i < importTextSplit.length) {
                    String clauseID = importTextSplit[i];
                    BattleClauseRegistry<BattleClause> clauseRegistry = BattleClauseRegistry.getClauseRegistry();
                    if (clauseRegistry.hasClause(clauseID)) {
                        newRules.clauses.add(clauseRegistry.getClause(clauseID));
                    }
                    ++i;
                }
            }
        }
        catch (IllegalArgumentException | IndexOutOfBoundsException e) {
            return currentSection;
        }
        this.levelCap = newRules.levelCap;
        this.raiseToCap = newRules.raiseToCap;
        this.battleType = newRules.battleType;
        this.numPokemon = newRules.numPokemon;
        this.turnTime = newRules.turnTime;
        this.teamSelectTime = newRules.teamSelectTime;
        this.teamPreview = newRules.teamPreview;
        this.fullHeal = newRules.fullHeal;
        this.tier = newRules.tier;
        this.clauses = newRules.clauses;
        this.validateRules();
        return null;
    }

    private void exportBytes(ByteBuf buffer) {
        buffer.writeInt(this.levelCap);
        buffer.writeBoolean(this.raiseToCap);
        buffer.writeInt(this.battleType.ordinal());
        buffer.writeInt(this.numPokemon);
        buffer.writeInt(this.turnTime);
        buffer.writeInt(this.teamSelectTime);
        buffer.writeBoolean(this.teamPreview);
        buffer.writeBoolean(this.fullHeal);
        ByteBufUtils.writeUTF8String(buffer, this.tier.getID());
        buffer.writeInt(this.clauses.size());
        for (BattleClause clause : this.clauses) {
            ByteBufUtils.writeUTF8String(buffer, clause.getID());
        }
    }

    private void importBytes(ByteBuf buffer) {
        try {
            this.levelCap = buffer.readInt();
            this.raiseToCap = buffer.readBoolean();
            this.battleType = EnumBattleType.getFromOrdinal(buffer.readInt());
            this.numPokemon = buffer.readInt();
            this.turnTime = buffer.readInt();
            this.teamSelectTime = buffer.readInt();
            this.teamPreview = buffer.readBoolean();
            this.fullHeal = buffer.readBoolean();
            this.tier = BattleClauseRegistry.getTierRegistry().getClause(ByteBufUtils.readUTF8String(buffer));
            if (this.tier == null) {
                this.tier = this.getDefaultTier();
            }
            int numClauses = buffer.readInt();
            this.clauses.clear();
            for (int i = 0; i < numClauses; ++i) {
                String clauseID = ByteBufUtils.readUTF8String(buffer);
                BattleClauseRegistry<BattleClause> clauseRegistry = BattleClauseRegistry.getClauseRegistry();
                if (!clauseRegistry.hasClause(clauseID)) continue;
                this.clauses.add(clauseRegistry.getClause(clauseID));
            }
        }
        catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            // empty catch block
        }
        this.validateRules();
    }

    @Override
    public void encodeInto(ByteBuf buf) {
        this.exportBytes(buf);
    }

    @Override
    public void decodeInto(ByteBuf buf) {
        this.importBytes(buf);
    }

    public void writeToNBT(NBTTagCompound nbt) {
        ByteBuf buffer = Unpooled.buffer();
        this.exportBytes(buffer);
        nbt.setByteArray("BattleRules", buffer.array());
    }

    public void readFromNBT(NBTTagCompound nbt) {
        if (nbt.hasKey("BattleRules")) {
            byte[] bytes = nbt.getByteArray("BattleRules");
            ByteBuf buffer = Unpooled.copiedBuffer((byte[])bytes);
            this.importBytes(buffer);
        }
    }

    public List<BattleClause> getClauseList() {
        ArrayList<BattleClause> clauseList = new ArrayList<>(this.clauses);
        Collections.sort(clauseList);
        return clauseList;
    }

    public List<String> getClauseIDs() {
        ArrayList<String> clauseIDs = new ArrayList<String>();
        for (BattleClause clause : this.clauses) {
            clauseIDs.add(clause.getID());
        }
        Collections.sort(clauseIDs);
        return clauseIDs;
    }

    public boolean hasClause(String id) {
        return this.clauses.contains(BattleClauseRegistry.getClauseRegistry().getClause(id));
    }

    public void setNewClauses(List<BattleClause> newClauses) {
        this.clauses.clear();
        this.clauses.addAll(newClauses);
    }

    public boolean isDefault() {
        boolean defaultClauses = this.clauses.isEmpty();
        if (this.clauses.size() == 1) {
            defaultClauses = this.hasClause("forfeit");
        }
        return defaultClauses && this.levelCap == PixelmonServerConfig.maxLevel && !this.raiseToCap && this.numPokemon == 6 && this.turnTime == 0 && this.teamSelectTime == 0 && !this.teamPreview && this.tier == this.getDefaultTier();
    }

    public String validateSingle(PokemonLink pokemon) {
        this.reloadTier();
        if (!this.tier.validateSingle(pokemon)) {
            return this.tier.getID();
        }
        for (BattleClause clause : this.clauses) {
            if (clause.validateSingle(pokemon)) continue;
            return clause.getID();
        }
        return null;
    }

    public String validateTeam(List<PokemonLink> team) {
        this.reloadTier();
        if (!this.tier.validateTeam(team)) {
            return this.tier.getID();
        }
        for (BattleClause clause : this.clauses) {
            if (clause.validateTeam(team)) continue;
            return clause.getID();
        }
        return null;
    }

    private void reloadTier() {
        this.tier = BattleClauseRegistry.getTierRegistry().getClause(this.tier.getID());
    }

    private Tier getDefaultTier() {
        return BattleClauseRegistry.getTierRegistry().getClause(EnumTier.Unrestricted.getTierID());
    }
}

