/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses;

import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.HashSet;
import java.util.List;

class SpeciesClause
extends BattleClause {
    SpeciesClause() {
        super("species");
    }

    @Override
    public boolean validateTeam(List<PokemonLink> team) {
        HashSet<EnumSpecies> species = new HashSet<EnumSpecies>();
        for (PokemonLink pokemon : team) {
            EnumSpecies current = pokemon.getBaseStats().pokemon;
            if (species.contains((Object)current)) {
                return false;
            }
            species.add(current);
        }
        return true;
    }
}

