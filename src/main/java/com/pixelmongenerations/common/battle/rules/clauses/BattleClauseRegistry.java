/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses;

import com.pixelmongenerations.common.battle.rules.clauses.AbilityClause;
import com.pixelmongenerations.common.battle.rules.clauses.AbilityComboClause;
import com.pixelmongenerations.common.battle.rules.clauses.BatonPass1Clause;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClauseAny;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClauseSingleAll;
import com.pixelmongenerations.common.battle.rules.clauses.ItemClause;
import com.pixelmongenerations.common.battle.rules.clauses.ItemPreventClause;
import com.pixelmongenerations.common.battle.rules.clauses.MoveClause;
import com.pixelmongenerations.common.battle.rules.clauses.PokemonClause;
import com.pixelmongenerations.common.battle.rules.clauses.SkyBattle;
import com.pixelmongenerations.common.battle.rules.clauses.SpeciesClause;
import com.pixelmongenerations.common.battle.rules.clauses.SpeedPassClause;
import com.pixelmongenerations.common.battle.rules.clauses.tiers.EnumTier;
import com.pixelmongenerations.common.battle.rules.clauses.tiers.Tier;
import com.pixelmongenerations.common.battle.rules.clauses.tiers.TierHierarchical;
import com.pixelmongenerations.common.battle.rules.teamselection.TeamSelection;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Chlorophyll;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Drizzle;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Drought;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Moody;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SandRush;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SandStream;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SandVeil;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ShadowTag;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SnowCloak;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SnowWarning;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SwiftSwim;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public final class BattleClauseRegistry<T extends BattleClause> {
    private final Map<String, T> clauses = new HashMap<String, T>();
    private final List<T> customClauses = new ArrayList<T>();
    public static final String BAG_CLAUSE = "bag";
    public static final String FORFEIT_CLAUSE = "forfeit";
    public static final String INVERSE_BATTLE = "inverse";
    public static final String SKY_BATTLE = "sky";
    public static final String SLEEP_CLAUSE = "sleep";
    public static final String FREEZE_CLAUSE = "freeze";
    private static int clauseVersion;
    private static final BattleClauseRegistry<BattleClause> clauseRegistry;
    private static final BattleClauseRegistry<Tier> tierRegistry;
    private Function<String, T> getDefault;

    private BattleClauseRegistry(Function<String, T> getDefault) {
        this.getDefault = getDefault;
    }

    public static BattleClauseRegistry<BattleClause> getClauseRegistry() {
        return clauseRegistry;
    }

    public static BattleClauseRegistry<Tier> getTierRegistry() {
        return tierRegistry;
    }

    private static void registerDefaultClauses() {
        clauseRegistry.registerClause(new BattleClause(BAG_CLAUSE));
        MoveClause batonPassClause = new MoveClause("batonpass", "Baton Pass");
        clauseRegistry.registerClause(batonPassClause);
        clauseRegistry.registerClause(new BatonPass1Clause());
        clauseRegistry.registerClause(new MoveClause("chatter", "Chatter"));
        clauseRegistry.registerClause(new AbilityClause("drizzle", Drizzle.class));
        AbilityComboClause drizzleSwimClause = new AbilityComboClause("drizzleswim", Drizzle.class, SwiftSwim.class);
        clauseRegistry.registerClause(drizzleSwimClause);
        clauseRegistry.registerClause(new AbilityClause("drought", Drought.class));
        clauseRegistry.registerClause(new BattleClauseSingleAll("endlessbattle", new ItemPreventClause("", EnumHeldItems.leppa), new MoveClause("", "Recycle"), new MoveClause("", "Fling", "Heal Pulse", "Pain Split")));
        clauseRegistry.registerClause(new AbilityClause("evasionability", SandVeil.class, SnowCloak.class));
        clauseRegistry.registerClause(new MoveClause("evasion", "Double Team", "Minimize"));
        clauseRegistry.registerClause(new BattleClause(FORFEIT_CLAUSE));
        clauseRegistry.registerClause(new BattleClause(INVERSE_BATTLE));
        clauseRegistry.registerClause(new ItemClause());
        clauseRegistry.registerClause(new PokemonClause("legendary", EnumSpecies.LEGENDARY_ENUMS));
        clauseRegistry.registerClause(new PokemonClause("ultrabeast", EnumSpecies.ULTRA_BEASTS_ENUMS));
        clauseRegistry.registerClause(new ItemPreventClause("mega", EnumHeldItems.megaStone));
        clauseRegistry.registerClause(new ItemPreventClause("zmoves", EnumHeldItems.zCrystals));
        clauseRegistry.registerClause(new ItemPreventClause("primals", EnumHeldItems.redOrb, EnumHeldItems.blueOrb));
        clauseRegistry.registerClause(new AbilityClause("moody", Moody.class));
        clauseRegistry.registerClause(new MoveClause("ohko", "Fissure", "Guillotine", "Horn Drill", "Sheer Cold"));
        clauseRegistry.registerClause(new AbilityClause("sandstream", SandStream.class));
        clauseRegistry.registerClause(new AbilityClause("shadowtag", ShadowTag.class));
        clauseRegistry.registerClause(new SkyBattle());
        clauseRegistry.registerClause(new BattleClause(SLEEP_CLAUSE));
        clauseRegistry.registerClause(new BattleClause(FREEZE_CLAUSE));
        clauseRegistry.registerClause(new BattleClauseSingleAll("smashpass", batonPassClause, new MoveClause("", "Shell Smash")));
        clauseRegistry.registerClause(new AbilityClause("snowwarning", SnowWarning.class));
        clauseRegistry.registerClause(new BattleClauseSingleAll("souldew", new PokemonClause("", EnumSpecies.Latias, EnumSpecies.Latios), new ItemPreventClause("", EnumHeldItems.soulDew)));
        clauseRegistry.registerClause(new SpeciesClause());
        clauseRegistry.registerClause(new SpeedPassClause());
        clauseRegistry.registerClause(new MoveClause("swagger", "Swagger"));
        clauseRegistry.registerClause(new BattleClauseAny("weatherspeed", drizzleSwimClause, new AbilityComboClause("", Drought.class, Chlorophyll.class), new AbilityComboClause("", SandStream.class, SandRush.class)));
    }

    private static void registerDefaultTiers() {
        TierHierarchical lastTier = null;
        EnumTier[] values = EnumTier.values();
        for (int i = values.length - 1; i >= 0; --i) {
            EnumTier tier = values[i];
            lastTier = new TierHierarchical(tier.getTierID(), new HashSet<PokemonForm>(), lastTier);
            tierRegistry.registerClause(lastTier);
        }
    }

    public T getClause(String id) {
        BattleClause clause = (BattleClause)this.clauses.get(id);
        if (clause == null) {
            clause = (BattleClause)this.getDefault.apply(id);
        }
        return (T)clause;
    }

    public boolean hasClause(String id) {
        return this.clauses.containsKey(id);
    }

    private void registerClause(T newClause) {
        String newClauseID = ((BattleClause)newClause).getID();
        if (newClauseID == null || newClauseID.isEmpty()) {
            throw new IllegalArgumentException("Clause must have a key to be registered.");
        }
        if (this.clauses.containsKey(newClauseID)) {
            throw new IllegalArgumentException("Clause is already registered: " + newClauseID);
        }
        for (String reserved : TeamSelection.getReservedKeys()) {
            if (!reserved.equals(newClauseID)) continue;
            throw new IllegalArgumentException("Clause cannot used reserved ID: " + reserved);
        }
        this.clauses.put(newClauseID, newClause);
    }

    public void registerCustomClause(T newClause) {
        this.registerClause(newClause);
        this.customClauses.add(newClause);
        BattleClauseRegistry.incrementVersion();
    }

    public static void incrementVersion() {
        ++clauseVersion;
    }

    public void replaceCustomClauses(List<T> newClauses) {
        this.replaceCustomClauses(newClauses, clauseVersion + 1);
    }

    public void replaceCustomClauses(List<T> newClauses, int newClauseVersion) {
        this.removeCustomClauses();
        for (T clause : newClauses) {
            this.registerCustomClause(clause);
        }
        clauseVersion = newClauseVersion;
    }

    public void removeCustomClauses() {
        for (BattleClause clause : this.customClauses) {
            this.clauses.remove(clause.getID());
        }
        this.customClauses.clear();
    }

    public List<T> getCustomClauses() {
        return new ArrayList<T>(this.customClauses);
    }

    public List<T> getClauseList() {
        ArrayList<T> clauseList = new ArrayList<>(this.clauses.values());
        Collections.sort(clauseList);
        return clauseList;
    }

    public static int getClauseVersion() {
        return clauseVersion;
    }

    static {
        clauseRegistry = new BattleClauseRegistry<BattleClause>(BattleClause::new);
        tierRegistry = new BattleClauseRegistry<Tier>(id -> null);
        BattleClauseRegistry.registerDefaultClauses();
        BattleClauseRegistry.registerDefaultTiers();
    }
}

