/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.teamselection;

import com.pixelmongenerations.common.battle.BattleFactory;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.TrainerParticipant;
import com.pixelmongenerations.common.battle.rules.BattleRules;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClauseRegistry;
import com.pixelmongenerations.common.battle.rules.teamselection.ParticipantSelection;
import com.pixelmongenerations.common.battle.rules.teamselection.TeamSelectPokemon;
import com.pixelmongenerations.common.battle.rules.teamselection.TeamSelectionList;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.NBTLink;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.CancelTeamSelect;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.CheckRulesVersionFixed;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.RejectTeamSelect;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.ShowTeamSelect;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class TeamSelection {
    final int id;
    private BattleRules rules;
    private ParticipantSelection[] participants;
    private boolean showRules;
    public static final String FAINT_KEY = "f";
    public static final String EGG_KEY = "e";
    public static final String NONE_KEY = "n";

    TeamSelection(int id, BattleRules rules, boolean showRules, PlayerStorage ... storages) {
        this.id = id;
        this.rules = rules;
        this.showRules = showRules;
        this.participants = new ParticipantSelection[storages.length];
        for (int i = 0; i < this.participants.length; ++i) {
            this.participants[i] = new ParticipantSelection(storages[i]);
        }
    }

    boolean hasPlayer(EntityPlayerMP player) {
        for (ParticipantSelection p : this.participants) {
            if (p.storage.getPlayer() != player) continue;
            return true;
        }
        return false;
    }

    void initializeClient() {
        PlayerStorage storage;
        ParticipantSelection cancelPart = null;
        boolean hasNPC = false;
        for (ParticipantSelection p : this.participants) {
            if (p.isNPC) {
                hasNPC = true;
                continue;
            }
            storage = p.storage;
            boolean hasPokemon = false;
            for (int i = 0; i < storage.partyPokemon.length; ++i) {
                NBTTagCompound current = storage.partyPokemon[i];
                if (current == null) {
                    p.disabled[i] = NONE_KEY;
                } else if (current.getBoolean("isEgg")) {
                    p.disabled[i] = EGG_KEY;
                } else if (current.getBoolean("IsFainted") && !this.rules.fullHeal) {
                    p.disabled[i] = FAINT_KEY;
                }
                if (p.disabled[i] != null) continue;
                p.disabled[i] = this.rules.validateSingle(new NBTLink(current));
                hasPokemon = hasPokemon || p.disabled[i] == null;
            }
            if (hasPokemon) continue;
            cancelPart = p;
        }
        if (cancelPart != null && !hasNPC) {
            this.cancelBattle(cancelPart);
            return;
        }
        for (ParticipantSelection p : this.participants) {
            if (p.isNPC) continue;
            storage = p.storage;
            EntityPlayerMP player = storage.getPlayer();
            ParticipantSelection otherPart = this.getOther(p);
            List<NBTTagCompound> opponentTags = otherPart.storage.getTeamIncludeEgg();
            ArrayList<TeamSelectPokemon> opponentTeam = new ArrayList<TeamSelectPokemon>();
            for (NBTTagCompound nbt : opponentTags) {
                opponentTeam.add(new TeamSelectPokemon(nbt, player));
            }
            IMessage packet = otherPart.isNPC ? new ShowTeamSelect(this.id, p.disabled, opponentTeam, otherPart.storage.trainer.getId(), otherPart.storage.trainer.getName(player.language), this.rules, this.showRules) : new ShowTeamSelect(this.id, p.disabled, opponentTeam, otherPart.storage.getPlayerID(), this.rules, this.showRules);
            if (this.showRules) {
                packet = new CheckRulesVersionFixed(BattleClauseRegistry.getClauseVersion(), (ShowTeamSelect)packet);
            }
            Pixelmon.NETWORK.sendTo(packet, player);
        }
    }

    void startBattle() {
        PlayerParticipant part2;
        BattleParticipant part1;
        ParticipantSelection npc = null;
        ParticipantSelection cancelPart = null;
        for (ParticipantSelection p : this.participants) {
            if (p.isNPC) {
                int[] teamIndices;
                int[] arrn;
                npc = p;
                if (this.rules.teamPreview || this.rules.numPokemon < 6) {
                    arrn = RandomHelper.getRandomDistinctNumbersBetween(0, 5, 6);
                } else {
                    int[] arrn2 = new int[6];
                    arrn2[0] = 0;
                    arrn2[1] = 1;
                    arrn2[2] = 2;
                    arrn2[3] = 3;
                    arrn2[4] = 4;
                    arrn = arrn2;
                    arrn2[5] = 5;
                }
                for (int index : teamIndices = arrn) {
                    p.addTeamMember(index);
                    if (this.rules.validateTeam(p.team) != null) {
                        p.removeTeamMember();
                    }
                    if (p.team.size() == this.rules.numPokemon) break;
                }
                if (!p.team.isEmpty()) continue;
                cancelPart = p;
                break;
            }
            String initValidate = this.rules.validateTeam(p.team);
            if (!p.team.isEmpty() && initValidate == null) continue;
            EntityPlayerMP player = p.storage.getPlayer();
            if (player != null && initValidate != null) {
                ChatHandler.sendChat(player, "gui.battlerules.teamviolatedforce", BattleClause.getLocalizedName(initValidate));
            }
            while (!p.team.isEmpty()) {
                p.removeTeamMember();
                if (this.rules.validateTeam(p.team) == null) continue;
            }
            if (p.team.isEmpty()) {
                int[] teamIndices;
                for (int index2 : teamIndices = RandomHelper.getRandomDistinctNumbersBetween(0, 5, 6)) {
                    p.addTeamMember(index2);
                    if (p.team.isEmpty()) continue;
                    if (this.rules.validateTeam(p.team) == null) break;
                    p.removeTeamMember();
                }
            }
            if (!p.team.isEmpty()) continue;
            cancelPart = p;
            break;
        }
        if (cancelPart != null) {
            this.cancelBattle(cancelPart);
            return;
        }
        if (npc == null) {
            part1 = this.getPlayerPart(this.participants[0]);
            part2 = this.getPlayerPart(this.participants[1]);
        } else {
            PlayerStorage storage = npc.storage;
            ParticipantSelection other = this.getOther(npc);
            EntityPlayerMP player = other.storage.getPlayer();
            if (!storage.trainer.canStartBattle(player, true)) {
                this.removeTeamSelect();
                return;
            }
            part1 = new TrainerParticipant(storage.trainer, player, this.rules.battleType.numPokemon, npc.team);
            part2 = new PlayerParticipant(player, other.team, this.rules.battleType.numPokemon);
        }
        BattleFactory.createBattle().team1(part1).team2(part2).rules(this.rules).startBattle();
        this.removeTeamSelect();
    }

    private void cancelBattle(ParticipantSelection cancelPart) {
        if (cancelPart != null) {
            String cancelName;
            if (cancelPart.isNPC) {
                EntityPlayerMP player = null;
                ParticipantSelection[] arrparticipantSelection = this.participants;
                int n = arrparticipantSelection.length;
                for (ParticipantSelection p : arrparticipantSelection) {
                    if (p.isNPC) continue;
                    player = p.storage.getPlayer();
                    break;
                }
                String langCode = player == null ? "en_US" : player.language;
                cancelName = cancelPart.storage.trainer.getName(langCode);
            } else {
                cancelName = cancelPart.storage.getDisplayName();
            }
            for (ParticipantSelection p : this.participants) {
                if (p.isNPC) continue;
                CancelTeamSelect packet = new CancelTeamSelect();
                EntityPlayerMP player = p.storage.getPlayer();
                Pixelmon.NETWORK.sendTo(packet, player);
                if (p == cancelPart) {
                    ChatHandler.sendChat(player, "gui.battlerules.cancelselectyou", new Object[0]);
                    continue;
                }
                ChatHandler.sendChat(player, "gui.battlerules.cancelselect", cancelName);
            }
            this.removeTeamSelect();
        }
    }

    private PlayerParticipant getPlayerPart(ParticipantSelection p) {
        return new PlayerParticipant(p.storage.getPlayer(), p.team, this.rules.battleType.numPokemon);
    }

    private ParticipantSelection getOther(ParticipantSelection selection) {
        for (ParticipantSelection other : this.participants) {
            if (other == selection) continue;
            return other;
        }
        return selection;
    }

    private ParticipantSelection getPlayer(EntityPlayerMP player) {
        for (ParticipantSelection p : this.participants) {
            if (p.storage.getPlayer() != player) continue;
            return p;
        }
        return null;
    }

    private boolean isReady() {
        for (ParticipantSelection p : this.participants) {
            if (p.confirmed) continue;
            return false;
        }
        return true;
    }

    public void registerTeamSelect(EntityPlayerMP player, int[] selection, boolean force) {
        ParticipantSelection ps = this.getPlayer(player);
        if (ps == null) {
            return;
        }
        ps.setTeam(selection);
        while (ps.team.size() > this.rules.numPokemon) {
            ps.team.remove(ps.team.size() - 1);
        }
        String clauseID = this.rules.validateTeam(ps.team);
        if (ps.team.isEmpty()) {
            clauseID = FAINT_KEY;
        }
        if (clauseID == null || force) {
            ps.confirmed = true;
            if (this.isReady()) {
                this.startBattle();
            }
        } else {
            Pixelmon.NETWORK.sendTo(new RejectTeamSelect(clauseID), player);
        }
    }

    public void unregisterTeamSelect(EntityPlayerMP player) {
        ParticipantSelection ps = this.getPlayer(player);
        if (ps != null) {
            ps.confirmed = false;
        }
    }

    private void removeTeamSelect() {
        TeamSelectionList.removeSelection(this.id);
    }

    public static String[] getReservedKeys() {
        return new String[]{FAINT_KEY, EGG_KEY, NONE_KEY};
    }
}

