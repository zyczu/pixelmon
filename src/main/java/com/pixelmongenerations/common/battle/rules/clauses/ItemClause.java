/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle.rules.clauses;

import com.pixelmongenerations.common.battle.rules.clauses.BattleClause;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import java.util.HashSet;
import java.util.List;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

class ItemClause
extends BattleClause {
    ItemClause() {
        super("item");
    }

    @Override
    public boolean validateTeam(List<PokemonLink> team) {
        HashSet<Item> itemSet = new HashSet<Item>();
        for (PokemonLink pokemon : team) {
            ItemStack itemStack = pokemon.getHeldItemStack();
            if (itemStack == null) continue;
            Item item = itemStack.getItem();
            if (itemSet.contains(item)) {
                return false;
            }
            itemSet.add(item);
        }
        return true;
    }
}

