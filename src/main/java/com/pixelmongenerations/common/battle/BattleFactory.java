/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.battle;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.battle.rules.BattleRules;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.battle.EnumBattleEndCause;
import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.minecraft.entity.player.EntityPlayerMP;

public class BattleFactory {
    private BattleParticipant[] team1;
    private BattleParticipant[] team2;
    private BattleRules rules;

    public static BattleControllerBase createHordeBattle(EntityPlayerMP player, EntityPixelmon hordePoke) {
        PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage(player).get();
        EntityPixelmon playerStartingPokemon = storage.getFirstAblePokemon(player.getEntityWorld());
        PlayerParticipant playerPart = new PlayerParticipant(player, playerStartingPokemon);
        ArrayList<BattleParticipant> swarmPokemon = new ArrayList<BattleParticipant>();
        int minAmt = PixelmonConfig.hordeMinSpawnAmount;
        int maxAmt = PixelmonConfig.hordeMaxSpawnAmount;
        int spawns = RandomHelper.getRandomNumberBetween(minAmt, maxAmt);
        int minLvl = PixelmonConfig.hordeMinSpawnLevel;
        int maxLvl = PixelmonConfig.hordeMaxSpawnLevel;
        HashMap<Integer, PokemonSpec> miniMap = new HashMap<Integer, PokemonSpec>();
        for (int i = 0; i < spawns; ++i) {
            PokemonSpec spec = PokemonSpec.from(hordePoke.getPokemonName());
            EntityPixelmon pixelmon = spec.create(player.getEntityWorld());
            pixelmon.getLvl().setLevel(RandomHelper.getRandomNumberBetween(minLvl, maxLvl));
            pixelmon.update(new EnumUpdateType[0]);
            WildPixelmonParticipant wpp = new WildPixelmonParticipant(pixelmon);
            swarmPokemon.add(wpp);
            miniMap.put(i, spec);
        }
        if (BattleRegistry.getBattle(player.getName()) != null) {
            BattleRegistry.getBattle(player.getName()).endBattle(EnumBattleEndCause.FORFEIT);
        }
        BattleControllerBase base = BattleFactory.createBattle().team1(playerPart).team2(swarmPokemon).type(EnumBattleType.Triple).startBattle();
        for (int i = 0; i < swarmPokemon.size(); ++i) {
            base.hordePokemonParticipants.put(i, (PokemonSpec)miniMap.get(i));
        }
        base.setHordeBattle(true);
        return base;
    }

    public static BattleFactory createBattle() {
        return new BattleFactory();
    }

    public BattleFactory team1(BattleParticipant ... participants) {
        if (participants == null) {
            throw new BattleFactoryException("Participants cannot be null");
        }
        if (participants.length == 0) {
            throw new BattleFactoryException("Participants cannot be empty");
        }
        this.team1 = participants;
        return this;
    }

    public BattleFactory team1(List<BattleParticipant> participants) {
        if (participants == null) {
            throw new BattleFactoryException("Participants cannot be null");
        }
        if (participants.size() == 0) {
            throw new BattleFactoryException("Participants cannot be empty");
        }
        return this.team1(participants.toArray(new BattleParticipant[participants.size()]));
    }

    public BattleFactory team2(BattleParticipant ... participants) {
        if (participants == null) {
            throw new BattleFactoryException("Participants cannot be null");
        }
        if (participants.length == 0) {
            throw new BattleFactoryException("Participants cannot be empty");
        }
        this.team2 = participants;
        return this;
    }

    public BattleFactory team2(List<BattleParticipant> participants) {
        if (participants == null) {
            throw new BattleFactoryException("Participants cannot be null");
        }
        if (participants.size() == 0) {
            throw new BattleFactoryException("Participants cannot be empty");
        }
        return this.team2(participants.toArray(new BattleParticipant[participants.size()]));
    }

    public BattleFactory type(EnumBattleType type) {
        if (this.rules == null) {
            this.rules = new BattleRules(type);
        } else {
            this.rules.battleType = type;
        }
        return this;
    }

    public BattleFactory rules(BattleRules rules) {
        if (rules == null) {
            throw new BattleFactoryException("Rules cannot be null");
        }
        if (this.rules == null) {
            this.rules = rules;
        } else {
            EnumBattleType oldType = this.rules.battleType;
            this.rules = rules;
            this.rules.battleType = oldType;
        }
        return this;
    }

    public BattleControllerBase startBattle() {
        if (this.team1 == null) {
            throw new BattleFactoryException("You must supply a value for team1");
        }
        if (this.team2 == null) {
            throw new BattleFactoryException("You must supply a value for team1");
        }
        if (this.rules == null) {
            this.rules = new BattleRules();
        }
        return BattleRegistry.startBattle(this.team1, this.team2, this.rules);
    }

    private class BattleFactoryException
    extends RuntimeException {
        public BattleFactoryException(String errorMessage) {
            super(errorMessage);
        }
    }
}

