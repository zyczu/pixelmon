/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity;

import com.pixelmongenerations.api.events.HyperspaceEvent;
import com.pixelmongenerations.client.models.MysteriousRingModelSmd;
import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.client.models.smd.AnimationType;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.common.entity.pixelmon.helpers.animation.AnimationVariables;
import com.pixelmongenerations.core.config.PixelmonConfig;
import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import org.jetbrains.annotations.NotNull;

public class EntityMysteriousRing
extends EntityLiving {
    private final int max = PixelmonConfig.mysteriousRingLifetime * 20;
    public int life = -1;
    private AnimationType animationType;
    private AnimationVariables animationVariables;
    public boolean animationCounting = false;
    public boolean animationSwap = false;
    static int animationDelayLimit = 3;
    int animationDelayCounter = 0;

    public EntityMysteriousRing(World worldIn) {
        super(worldIn);
        this.setSize(1.75f, 2.0f);
    }

    public AnimationVariables getAnimationVariables() {
        if (this.animationVariables == null) {
            this.animationVariables = new AnimationVariables();
        }
        return this.animationVariables;
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        if (this.world.isRemote) {
            if (this.getCurrentAnimation() != AnimationType.IDLE && this.animationVariables != null) {
                this.animationVariables.tick();
                this.animationVariables.tick();
            }
            if (this.animationCounting) {
                if (this.animationDelayCounter < animationDelayLimit) {
                    ++this.animationDelayCounter;
                    this.animationSwap = false;
                }
                if (this.animationDelayCounter >= animationDelayLimit) {
                    this.animationSwap = true;
                    this.animationDelayCounter = 0;
                }
            } else {
                this.animationDelayCounter = 0;
                this.animationSwap = false;
            }
            this.checkAnimation();
        }
        if (this.life > -1) {
            if (this.life >= this.max) {
                this.setDead();
            }
            ++this.life;
        }
    }

    @Override
    public void onCollideWithPlayer(@NotNull EntityPlayer entityIn) {
        HyperspaceEvent.Collide event = new HyperspaceEvent.Collide(this, entityIn);
        MinecraftForge.EVENT_BUS.post(event);
    }

    @Override
    protected boolean processInteract(@NotNull EntityPlayer player, @NotNull EnumHand hand) {
        if (hand == EnumHand.MAIN_HAND) {
            HyperspaceEvent.Click event = new HyperspaceEvent.Click(this, player.getUniqueID());
            if (!event.isCanceled() && event.shouldKill()) {
                this.setDead();
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean attackable() {
        return false;
    }

    @Override
    public boolean attackEntityFrom(@NotNull DamageSource source, float amount) {
        return false;
    }

    @Override
    public boolean hasNoGravity() {
        return true;
    }

    public ResourceLocation getTexture() {
        return new ResourceLocation("pixelmon:textures/mysterious_ring.png");
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
    public void addVelocity(double par1, double par3, double par5) {
        if (this.canBePushed()) {
            super.addVelocity(par1, par3, par5);
        }
    }

    @Override
    public boolean canBeLeashedTo(@NotNull EntityPlayer player) {
        return false;
    }

    public void initAnimation() {
        ModelBase base;
        if (this.world.isRemote && (base = this.getModel()) instanceof MysteriousRingModelSmd) {
            ValveStudioModel model = ((MysteriousRingModelSmd)base).theModel;
            model.animate();
        }
    }

    public ModelBase getModel() {
        return PixelmonModelRegistry.mysteriousRingModel.getModel();
    }

    protected void checkAnimation() {
        if (!(this.getModel() instanceof MysteriousRingModelSmd)) {
            return;
        }
        MysteriousRingModelSmd smdModel = (MysteriousRingModelSmd)this.getModel();
        if (smdModel.theModel.currentAnimation == null) {
            this.setAnimation(AnimationType.IDLE);
            smdModel.theModel.animate();
        }
    }

    public void setAnimation(AnimationType animation) {
        this.animationType = animation;
    }

    public AnimationType getCurrentAnimation() {
        if (this.animationType == null) {
            return AnimationType.IDLE;
        }
        return this.animationType;
    }
}

