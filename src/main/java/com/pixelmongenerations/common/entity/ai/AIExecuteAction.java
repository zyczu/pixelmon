/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai;

import com.pixelmongenerations.common.battle.BattleFactory;
import com.pixelmongenerations.common.battle.BattleQuery;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.TrainerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.battle.rules.teamselection.TeamSelectionList;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.storage.playerData.ExternalMoveData;
import java.util.Optional;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.math.RayTraceResult;

public class AIExecuteAction
extends EntityAIBase {
    private EntityCreature entity;

    public AIExecuteAction(EntityCreature entity7HasAI) {
        this.entity = entity7HasAI;
        this.setMutexBits(4);
    }

    @Override
    public boolean shouldExecute() {
        block39: {
            try {
                BattleParticipant participant;
                ExternalMoveData move;
                Optional<PlayerStorage> optstorage;
                double distance;
                if (this.entity.getAttackTarget() == null) {
                    return false;
                }
                if (this.entity instanceof EntityPixelmon) {
                    EntityPixelmon pokemon = (EntityPixelmon)this.entity;
                    if (pokemon.aggressionTimer > 0 || pokemon.battleController != null || pokemon.hitByPokeball != null || pokemon.getBossMode() != EnumBossMode.NotBoss || pokemon.getOwner() != null && BattleRegistry.getBattle((EntityPlayerMP)pokemon.getOwner()) != null) {
                        return false;
                    }
                } else if (((NPCTrainer)this.entity).battleController != null) {
                    return false;
                }
                EntityLivingBase attackTarget = this.entity.getAttackTarget();
                int moveIndex = -1;
                double d = distance = ((NPCTrainer)this.entity).aggroRange == 0 ? (double)PixelmonServerConfig.defaultNPCTrainerAggroRange : (double)((NPCTrainer)this.entity).getAggroRange();
                if (this.entity instanceof EntityPixelmon) {
                    EntityPixelmon pokemon = (EntityPixelmon)this.entity;
                    moveIndex = pokemon.moveIndex;
                    if (moveIndex != -1 && (optstorage = pokemon.getStorage()).isPresent()) {
                        move = optstorage.get().moves.get(pokemon.getPokemonId(), moveIndex);
                        distance = move.getTargetDistance();
                    }
                }
                if ((double)attackTarget.getDistance(this.entity) >= distance) break block39;
                this.entity.setAttackTarget(null);
                if (this.entity instanceof EntityPixelmon) {
                    EntityPixelmon user = (EntityPixelmon)this.entity;
                    if (moveIndex != -1) {
                        optstorage = user.getStorage();
                        if (optstorage.isPresent()) {
                            move = optstorage.get().moves.get(user.getPokemonId(), moveIndex);
                            move.execute((EntityPlayerMP)user.getOwner(), user, new RayTraceResult(attackTarget));
                        }
                        return true;
                    }
                }
                try {
                    participant = this.getThisParticipant(attackTarget);
                }
                catch (IllegalStateException e) {
                    return false;
                }
                if (attackTarget instanceof EntityPlayer) {
                    Item currentItem;
                    EntityPlayerMP player = (EntityPlayerMP)attackTarget;
                    if (BattleRegistry.getBattle(player) != null) {
                        return false;
                    }
                    if (this.entity instanceof EntityPixelmon && ((EntityPixelmon)this.entity).belongsTo(player)) {
                        return false;
                    }
                    Item item = currentItem = player.inventory.mainInventory.get(player.inventory.currentItem).isEmpty() ? null : player.inventory.mainInventory.get(player.inventory.currentItem).getItem();
                    if (currentItem == PixelmonItems.trainerEditor) {
                        return false;
                    }
                    if (currentItem instanceof ItemBlock && ((ItemBlock)currentItem).getBlock() == PixelmonBlocks.pixelmonSpawner) {
                        return false;
                    }
                    Optional<PlayerStorage> optstorage2 = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
                    if (optstorage2.isPresent()) {
                        PlayerParticipant playerPart;
                        PlayerStorage storage = optstorage2.get();
                        if (storage.guiOpened || storage.countAblePokemon() == 0) {
                            return false;
                        }
                        EnumBattleType battleType = EnumBattleType.Single;
                        if (this.entity instanceof NPCTrainer) {
                            NPCTrainer trainer = (NPCTrainer)this.entity;
                            battleType = trainer.getBattleType();
                            if (!trainer.battleRules.isDefault()) {
                                TeamSelectionList.addTeamSelection(trainer.battleRules, true, trainer.getPokemonStorage(), PixelmonStorage.pokeBallManager.getPlayerStorage(player).get());
                                return true;
                            }
                        }
                        EntityPixelmon firstPokemon = storage.getFirstAblePokemon(player.world);
                        if (battleType == EnumBattleType.Single) {
                            if (!storage.entityAlreadyExists(firstPokemon)) {
                                firstPokemon.releaseFromPokeball();
                                firstPokemon.setLocationAndAngles(player.posX, player.posY, player.posZ, player.rotationYaw, 0.0f);
                            }
                            playerPart = new PlayerParticipant(player, firstPokemon);
                        } else {
                            playerPart = new PlayerParticipant(player, storage.getAmountAblePokemon(player.world, battleType.numPokemon));
                        }
                        if (participant instanceof PlayerParticipant && this.entity instanceof EntityPixelmon) {
                            new BattleQuery((EntityPlayerMP)((EntityPixelmon)this.entity).getOwner(), (EntityPixelmon)this.entity, player, firstPokemon);
                        } else {
                            BattleFactory.createBattle().team1(playerPart).team2(participant).startBattle();
                        }
                    }
                    return true;
                }
                if (this.entity instanceof EntityPixelmon) {
                    EntityPixelmon userPokemon = (EntityPixelmon)this.entity;
                    if (attackTarget instanceof NPCTrainer) {
                        NPCTrainer trainer = (NPCTrainer)attackTarget;
                        if (!trainer.battleRules.isDefault()) {
                            TeamSelectionList.addTeamSelection(trainer.battleRules, true, trainer.getPokemonStorage(), PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)userPokemon.getOwner()).get());
                            return true;
                        }
                        if (participant instanceof PlayerParticipant && userPokemon.getOwner() != null) {
                            try {
                                BattleFactory.createBattle().team1(participant).team2(new TrainerParticipant(trainer, (EntityPlayer)userPokemon.getOwner(), 1)).startBattle();
                            }
                            catch (IllegalStateException e) {
                                return false;
                            }
                            return true;
                        }
                        return false;
                    }
                    if (attackTarget instanceof EntityPixelmon) {
                        EntityPixelmon target = (EntityPixelmon)attackTarget;
                        if (participant instanceof WildPixelmonParticipant && (target.getBossMode() != EnumBossMode.NotBoss || userPokemon.getBossMode() != EnumBossMode.NotBoss || target.isShiny() || userPokemon.isShiny() || target.getOwner() != null) || target.hitByPokeball != null || target.battleController != null || target.getHealth() <= 0.0f || target.isFainted || target.isDead || target.isInRanchBlock) {
                            return false;
                        }
                        if (target.getOwner() == null) {
                            BattleFactory.createBattle().team1(participant).team2(new WildPixelmonParticipant(target)).startBattle();
                        } else if (userPokemon.getOwner() != target.getOwner()) {
                            new BattleQuery((EntityPlayerMP)userPokemon.getOwner(), userPokemon, (EntityPlayerMP)target.getOwner(), target);
                        }
                        return true;
                    }
                    return false;
                }
                return false;
            }
            catch (Exception exception) {
                // empty catch block
            }
        }
        return false;
    }

    @Override
    public void startExecuting() {
        this.entity.setAttackTarget(null);
        if (this.entity instanceof EntityPixelmon) {
            ((EntityPixelmon)this.entity).update(EnumUpdateType.Target);
        }
    }

    private BattleParticipant getThisParticipant(EntityLivingBase attackTarget) throws IllegalStateException {
        if (this.entity instanceof NPCTrainer) {
            NPCTrainer trainer = (NPCTrainer)this.entity;
            return new TrainerParticipant(trainer, (EntityPlayerMP)attackTarget, trainer.getBattleType().numPokemon);
        }
        EntityPixelmon pixelmon = (EntityPixelmon)this.entity;
        if (pixelmon.hasOwner()) {
            return new PlayerParticipant((EntityPlayerMP)pixelmon.getOwner(), pixelmon);
        }
        return new WildPixelmonParticipant(pixelmon);
    }
}

