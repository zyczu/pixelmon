/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai;

import com.pixelmongenerations.common.entity.pixelmon.Entity7HasAI;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;

public class AIFlying
extends EntityAIBase {
    private static final long OWNER_FIND_INTERVAL = 5000L;
    private static final double OWNER_DISTANCE_TO_TAKEOFF = 100.0;
    private final Entity7HasAI pixelmon;
    private long nextOwnerCheckTime;
    private BlockPos currentFlightTarget;
    private Random rand;
    private boolean takingOff = false;
    private int nextWingBeat = 10;
    private int wingBeatTick = 0;
    boolean lastChangeDirection;
    int flightTicks = 0;
    double takeOffSpeed = 0.0;
    int targetHeight = 0;

    public AIFlying(Entity7HasAI entity) {
        this.pixelmon = entity;
        this.rand = entity.getRNG();
        this.nextOwnerCheckTime = System.currentTimeMillis();
        this.setMutexBits(1);
    }

    @Override
    public boolean shouldExecute() {
        return this.pixelmon.onGround && this.pixelmon.getOwner() == null && this.pixelmon.getFlyingParameters() != null && this.checkTakeOffConditions();
    }

    @Override
    public boolean shouldContinueExecuting() {
        return !this.pixelmon.onGround;
    }

    @Override
    public void startExecuting() {
        this.takeOff();
    }

    @Override
    public void resetTask() {
        super.resetTask();
    }

    @Override
    public void updateTask() {
        ++this.flightTicks;
        if (this.flightTicks > 30 && this.takingOff || this.takingOff && this.pixelmon.posY >= (double)this.targetHeight) {
            this.takingOff = false;
            this.flightTicks = 0;
        }
        if (this.takingOff) {
            this.pixelmon.travel((float)this.pixelmon.stats.Speed / 500.0f, 0.0f, 0.0f);
            this.pixelmon.motionY = this.takeOffSpeed;
        }
        if (this.pixelmon.getOwner() == null) {
            this.lookForOwnerEntity();
        }
        this.checkForLandingSpot();
        AxisAlignedBB box = this.pixelmon.getEntityBoundingBox();
        RayTraceResult mop = this.pixelmon.world.rayTraceBlocks(new Vec3d(this.pixelmon.posX, box.minY, this.pixelmon.posZ), new Vec3d(this.pixelmon.posX + this.pixelmon.motionX * 100.0, box.minY, this.pixelmon.posZ + this.pixelmon.motionZ * 100.0));
        if (mop == null) {
            mop = this.pixelmon.world.rayTraceBlocks(new Vec3d(this.pixelmon.posX, box.maxY, this.pixelmon.posZ), new Vec3d(this.pixelmon.posX + this.pixelmon.motionX * 100.0, box.maxY, this.pixelmon.posZ + this.pixelmon.motionZ * 100.0));
        }
        if (this.hasLandingSpot()) {
            if (mop == null) {
                double d0 = (double)this.currentFlightTarget.getX() + 0.5 - this.pixelmon.posX;
                double d1 = (double)this.currentFlightTarget.getY() + 0.1 - this.pixelmon.posY;
                double d2 = (double)this.currentFlightTarget.getZ() + 0.5 - this.pixelmon.posZ;
                this.pixelmon.motionX += (Math.signum(d0) - this.pixelmon.motionX) * (double)0.1f;
                this.pixelmon.motionY += (Math.signum(d1) * (double)0.7f - this.pixelmon.motionY) * (double)0.1f;
                this.pixelmon.motionZ += (Math.signum(d2) - this.pixelmon.motionZ) * (double)0.1f;
                float f = (float)(Math.atan2(this.pixelmon.motionZ, this.pixelmon.motionX) * 180.0 / Math.PI) - 90.0f;
                float f1 = MathHelper.wrapDegrees(f - this.pixelmon.rotationYaw);
                this.pixelmon.setMoveForward(0.5f);
                this.pixelmon.rotationYaw += f1;
            }
        } else {
            this.maintainFlight(mop != null);
        }
        super.updateTask();
    }

    private void checkForLandingSpot() {
        if (!(this.currentFlightTarget == null || this.pixelmon.world.isAirBlock(this.currentFlightTarget) && this.currentFlightTarget.getY() >= 1)) {
            this.currentFlightTarget = null;
        }
        if (this.currentFlightTarget == null || this.rand.nextInt(30) == 0) {
            this.currentFlightTarget = new BlockPos((int)(this.pixelmon.posX + this.pixelmon.motionX * 200.0 + (double)this.rand.nextInt(10) - 5.0), 0, (int)(this.pixelmon.posZ + this.pixelmon.motionZ * 200.0 + (double)this.rand.nextInt(10) - 5.0));
            this.currentFlightTarget = this.pixelmon.world.getTopSolidOrLiquidBlock(this.currentFlightTarget);
            IBlockState state = this.pixelmon.world.getBlockState(this.currentFlightTarget);
            Material m = state.getMaterial();
            this.currentFlightTarget = this.currentFlightTarget.up();
            if (this.pixelmon.getFlyingParameters() != null && !this.pixelmon.getFlyingParameters().willLandInMaterial(m) || !this.pixelmon.world.isAirBlock(this.currentFlightTarget)) {
                this.currentFlightTarget = null;
            }
        }
    }

    private boolean hasLandingSpot() {
        return this.currentFlightTarget != null;
    }

    private void maintainFlight(boolean hasObstacle) {
        ++this.wingBeatTick;
        if (hasObstacle || this.wingBeatTick >= this.nextWingBeat) {
            this.pickDirection(hasObstacle);
            this.nextWingBeat = this.pixelmon.getFlyingParameters().flapRate + (int)(Math.random() * 0.4 * (double)this.pixelmon.getFlyingParameters().flapRate - 0.2 * (double)this.pixelmon.getFlyingParameters().flapRate);
            this.pixelmon.travel(0.0f, 0.0f, 4.0f + (float)this.pixelmon.stats.Speed / 100.0f * this.pixelmon.getFlyingParameters().flySpeedModifier);
            this.pixelmon.motionY = (double)(this.pixelmon.getFlyingParameters().flapRate + 1) * 0.01;
            this.wingBeatTick = 0;
        }
    }

    public void pickDirection(boolean useLastChangeDirection) {
        double rotAmt;
        if (useLastChangeDirection) {
            rotAmt = this.pixelmon.getRNG().nextInt(5) + 5;
            if (this.lastChangeDirection) {
                rotAmt *= -1.0;
            }
        } else {
            rotAmt = this.pixelmon.getRNG().nextInt(10) - 5;
            this.lastChangeDirection = rotAmt > 0.0;
        }
        this.pixelmon.rotationYaw = (float)((double)this.pixelmon.rotationYaw + rotAmt);
    }

    private void lookForOwnerEntity() {
        if (this.pixelmon.getOwner() != null && System.currentTimeMillis() > this.nextOwnerCheckTime) {
            this.nextOwnerCheckTime = System.currentTimeMillis() + 5000L;
            this.currentFlightTarget = new BlockPos((int)this.pixelmon.getOwner().posX, (int)this.pixelmon.getOwner().posY + 1, (int)this.pixelmon.getOwner().posZ);
        }
    }

    private boolean checkTakeOffConditions() {
        if (this.pixelmon.getOwner() != null && this.pixelmon.getOwner().isEntityAlive() && this.pixelmon.getDistanceSq(this.pixelmon.getOwner()) > 100.0) {
            return true;
        }
        EntityPlayer nearest = this.pixelmon.world.getClosestPlayerToEntity(this.pixelmon, 6.0);
        return nearest != null && nearest != this.pixelmon.getOwner() || Math.random() < 0.015;
    }

    private void takeOff() {
        this.pixelmon.isFlying = true;
        this.takingOff = true;
        this.flightTicks = 0;
        this.targetHeight = (int)this.pixelmon.posY + (int)(Math.random() * (double)(this.pixelmon.getFlyingParameters().flyHeightMax - this.pixelmon.getFlyingParameters().flyHeightMin)) + this.pixelmon.getFlyingParameters().flyHeightMin;
        this.pixelmon.setPosition(this.pixelmon.posX, this.pixelmon.posY - 1.0, this.pixelmon.posZ);
        this.pixelmon.world.playSound(null, this.pixelmon.posX, this.pixelmon.posY, this.pixelmon.posZ, SoundEvents.ENTITY_BAT_TAKEOFF, SoundCategory.NEUTRAL, 0.8f, 1.0f);
        this.takeOffSpeed = 0.22 + (double)((float)this.pixelmon.stats.Speed / 300.0f);
        this.pixelmon.move(MoverType.SELF, this.pixelmon.getRNG().nextDouble() - 0.5, this.takeOffSpeed, this.pixelmon.getRNG().nextDouble() - 0.5);
    }
}

