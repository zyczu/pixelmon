/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.Vec3d;

public class AIMoveTowardsTarget
extends EntityAIBase {
    private EntityCreature theEntity;
    private EntityLivingBase targetEntity;
    private double movePosX;
    private double movePosY;
    private double movePosZ;
    private float maxTargetDistance;

    public AIMoveTowardsTarget(EntityCreature par1EntityCreature, float par3) {
        this.theEntity = par1EntityCreature;
        this.maxTargetDistance = par3;
        this.setMutexBits(3);
    }

    @Override
    public boolean shouldExecute() {
        if (this.theEntity.getAttackTarget() == null) {
            return false;
        }
        if (this.theEntity instanceof EntityPixelmon) {
            if (((EntityPixelmon)this.theEntity).aggressionTimer > 0) {
                return false;
            }
            if (((EntityPixelmon)this.theEntity).battleController != null) {
                return false;
            }
            if (((EntityPixelmon)this.theEntity).getOwner() != null && BattleRegistry.getBattle((EntityPlayerMP)((EntityPixelmon)this.theEntity).getOwner()) != null) {
                this.theEntity.setAttackTarget(null);
                return false;
            }
        } else if (((NPCTrainer)this.theEntity).battleController != null) {
            return false;
        }
        this.targetEntity = this.theEntity.getAttackTarget();
        if (this.targetEntity.getDistanceSq(this.theEntity) > (double)(this.maxTargetDistance * this.maxTargetDistance)) {
            return false;
        }
        Vec3d var1 = RandomPositionGenerator.findRandomTargetBlockTowards(this.theEntity, 16, 7, new Vec3d(this.targetEntity.posX, this.targetEntity.posY, this.targetEntity.posZ));
        if (var1 == null) {
            return false;
        }
        this.movePosX = var1.x;
        this.movePosY = var1.y;
        this.movePosZ = var1.z;
        return true;
    }

    @Override
    public boolean shouldContinueExecuting() {
        return false;
    }

    @Override
    public void resetTask() {
        this.targetEntity = null;
    }

    @Override
    public void startExecuting() {
        this.theEntity.getNavigator().tryMoveToXYZ(this.movePosX, this.movePosY, this.movePosZ, (float)this.theEntity.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getAttributeValue());
    }
}

