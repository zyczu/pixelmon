/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai;

import com.pixelmongenerations.common.entity.pixelmon.Entity7HasAI;
import com.pixelmongenerations.core.util.helper.WorldHelper;
import java.util.Random;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;

public class AISwimming
extends EntityAIBase {
    private Entity7HasAI pixelmon;
    private float swimSpeed = 1.5f;
    private float decayRate = 0.99f;
    private int depthRangeStart = 0;
    private int depthRangeEnd = 100;
    float moveSpeed;
    Random rand;
    int ticksToRefresh;
    boolean lastChangeDirection;
    boolean useLastChangeDirection;

    public AISwimming(Entity7HasAI entity) {
        if (entity.getSwimmingParameters() != null) {
            this.swimSpeed = entity.getSwimmingParameters().swimSpeed;
            this.decayRate = entity.getSwimmingParameters().decayRate;
            this.depthRangeStart = entity.getSwimmingParameters().depthRangeStart;
            this.depthRangeEnd = entity.getSwimmingParameters().depthRangeEnd;
            this.ticksToRefresh = 0;
        }
        this.pixelmon = entity;
        this.rand = entity.getRNG();
    }

    @Override
    public boolean shouldExecute() {
        return this.depthRangeStart != -1;
    }

    @Override
    public void updateTask() {
        if (this.pixelmon.getPokemonName().equals("Magikarp")) {
            this.swimSpeed = 0.7f;
        }
        if (this.pixelmon.battleController != null) {
            super.updateTask();
            return;
        }
        boolean useLastChangeDirection = false;
        RayTraceResult mop = this.pixelmon.world.rayTraceBlocks(new Vec3d(this.pixelmon.posX, this.pixelmon.getEntityBoundingBox().minY, this.pixelmon.posZ), new Vec3d(this.pixelmon.posX + this.pixelmon.motionX * 100.0, this.pixelmon.getEntityBoundingBox().minY, this.pixelmon.posZ + this.pixelmon.motionZ * 100.0));
        if (mop == null) {
            mop = this.pixelmon.world.rayTraceBlocks(new Vec3d(this.pixelmon.posX, this.pixelmon.getEntityBoundingBox().maxY, this.pixelmon.posZ), new Vec3d(this.pixelmon.posX + this.pixelmon.motionX * 100.0, this.pixelmon.getEntityBoundingBox().maxY, this.pixelmon.posZ + this.pixelmon.motionZ * 100.0));
        }
        if (mop != null) {
            useLastChangeDirection = true;
        }
        --this.ticksToRefresh;
        if (this.moveSpeed == 0.0f || useLastChangeDirection || this.pixelmon.motionX * this.pixelmon.motionX + this.pixelmon.motionZ * this.pixelmon.motionZ < (double)(this.moveSpeed / 4.0f)) {
            this.pickDirection(useLastChangeDirection);
            this.pickSpeed();
            this.pixelmon.travel(0.0f, 0.0f, this.moveSpeed);
        }
        super.updateTask();
    }

    @Override
    public boolean shouldContinueExecuting() {
        this.moveSpeed *= this.decayRate;
        this.pixelmon.motionX *= (double)this.decayRate;
        this.pixelmon.motionY *= (double)this.decayRate;
        this.pixelmon.motionZ *= (double)this.decayRate;
        this.pixelmon.renderYawOffset = this.pixelmon.rotationYaw;
        return true;
    }

    public void pickDirection(boolean useLastChangeDirection) {
        double rotAmt;
        if (useLastChangeDirection) {
            rotAmt = this.pixelmon.getRNG().nextInt(5) + 5;
            if (this.lastChangeDirection) {
                rotAmt *= -1.0;
            }
        } else {
            rotAmt = this.pixelmon.getRNG().nextInt(10) - 5;
            this.lastChangeDirection = rotAmt > 0.0;
        }
        this.pixelmon.rotationYaw = (float)((double)this.pixelmon.rotationYaw + rotAmt);
    }

    public void pickSpeed() {
        this.moveSpeed = 2.8f * (this.pixelmon.getRNG().nextFloat() * this.swimSpeed + this.swimSpeed / 2.0f);
        int wdepth = WorldHelper.getWaterDepth(this.pixelmon.getPosition(), this.pixelmon.world);
        this.pixelmon.motionY = wdepth >= this.depthRangeEnd ? (double)((0.02f + this.rand.nextFloat()) * 0.1f) : (wdepth <= this.depthRangeStart ? (double)((-0.02f - this.rand.nextFloat()) * 0.1f) : (double)((this.rand.nextFloat() - 0.5f) * 0.1f));
    }
}

