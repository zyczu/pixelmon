/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai.flying;

public class FlySpeeds {
    public int endurance = 2400;
    public double maxFlySpeed = 0.25;
    public double acceleration = 0.1;
    public double rotationSpeed = 0.05235987755982988;
}

