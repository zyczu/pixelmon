/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.pathfinding.Path;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class AIWander
extends EntityAIBase {
    private EntityCreature entity;
    private double xPosition;
    private double yPosition;
    private double zPosition;
    private boolean mustUpdate;
    private int executionChance = 120;

    public AIWander(EntityCreature par1EntityCreature) {
        this.entity = par1EntityCreature;
        this.setMutexBits(1);
    }

    @Override
    public boolean shouldExecute() {
        Vec3d vec3D;
        if (!this.mustUpdate) {
            if (this.entity.getRNG().nextInt(this.executionChance) != 0) {
                return false;
            }
            if (this.entity instanceof EntityPixelmon && !((EntityPixelmon)this.entity).canMove) {
                return false;
            }
        }
        if ((vec3D = RandomPositionGenerator.findRandomTarget(this.entity, 10, 7)) == null) {
            return false;
        }
        if (this.entity instanceof EntityPixelmon) {
            EntityPixelmon p = (EntityPixelmon)this.entity;
            if ((vec3D = this.checkBlockOwner(p, vec3D)) == null) {
                return false;
            }
            if ((vec3D = this.checkSpawner(p, vec3D)) == null) {
                return false;
            }
        }
        this.xPosition = vec3D.x;
        this.yPosition = vec3D.y;
        this.zPosition = vec3D.z;
        this.mustUpdate = false;
        return true;
    }

    private Vec3d checkSpawner(EntityPixelmon p, Vec3d vec3) {
        if (p.spawner != null) {
            int count;
            BlockPos spawnerPos = p.spawner.getPos();
            int spawnerX = spawnerPos.getX();
            int spawnerZ = spawnerPos.getZ();
            for (count = 2; (vec3.x - (double)spawnerX) * (vec3.x - (double)spawnerX) + (vec3.z - (double)spawnerZ) * (vec3.z - (double)spawnerZ) > (double)(p.spawner.spawnRadius * p.spawner.spawnRadius) && count >= 0; --count) {
                Vec3d newVec3 = RandomPositionGenerator.findRandomTarget(this.entity, 10, 7);
                if (newVec3 == null) continue;
                vec3 = newVec3;
            }
            if (count < 0) {
                return null;
            }
        }
        return vec3;
    }

    private Vec3d checkBlockOwner(EntityPixelmon p, Vec3d vec3) {
        if (p.blockOwner != null) {
            int count;
            for (count = 2; !p.blockOwner.getBounds().isIn(vec3) && count >= 0; --count) {
                Vec3d newVec3 = RandomPositionGenerator.findRandomTarget(this.entity, 5, 7);
                if (newVec3 == null) continue;
                vec3 = newVec3;
            }
            if (count < 0) {
                return null;
            }
        }
        return vec3;
    }

    @Override
    public boolean shouldContinueExecuting() {
        return !this.entity.getNavigator().noPath();
    }

    @Override
    public void startExecuting() {
        this.entity.getNavigator().tryMoveToXYZ(this.xPosition, this.yPosition, this.zPosition, this.entity.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getAttributeValue());
        if (this.entity instanceof EntityPixelmon) {
            EntityPixelmon p = (EntityPixelmon)this.entity;
            if (p.blockOwner != null && this.shouldContinueExecuting()) {
                Path path = p.getNavigator().getPath();
                int length = path.getCurrentPathLength();
                for (int i = 0; i < length; ++i) {
                    if (p.blockOwner.getBounds().isIn(path.getVectorFromIndex(p, i))) continue;
                    p.getNavigator().clearPath();
                    return;
                }
            }
        }
    }

    public void makeUpdate() {
        this.mustUpdate = true;
    }

    public void setExecutionChance(int newchance) {
        this.executionChance = newchance;
    }
}

