/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai;

import com.pixelmongenerations.common.entity.ai.AITarget;
import com.pixelmongenerations.common.entity.ai.TargetSorter;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import java.util.List;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;

public class AITargetNearest
extends AITarget {
    EntityLivingBase targetEntity;
    private TargetSorter targetSorter;

    public AITargetNearest(EntityCreature entity, float par3, boolean par5) {
        this(entity, par3, par5, false);
    }

    public AITargetNearest(EntityCreature entity, float par3, boolean par5, boolean par6) {
        super(entity, par3, par5, par6);
        this.targetDistance = par3;
        this.targetSorter = new TargetSorter(this, entity);
        this.setMutexBits(3);
    }

    @Override
    public boolean shouldExecute() {
        if (this.taskOwner instanceof EntityPixelmon ? ((EntityPixelmon)this.taskOwner).battleController != null : ((NPCTrainer)this.taskOwner).battleController != null) {
            return false;
        }
        EntityPlayer player = this.taskOwner.world.getClosestPlayerToEntity(this.taskOwner, this.targetDistance);
        if (this.isSuitableTarget(player, true)) {
            this.targetEntity = player;
            return true;
        }
        if (this.taskOwner instanceof NPCTrainer) {
            return false;
        }
        List<EntityPixelmon> nearEntities = this.taskOwner.world.getEntitiesWithinAABB(EntityPixelmon.class, this.taskOwner.getCollisionBoundingBox().expand(this.targetDistance, 4.0, this.targetDistance));
        nearEntities.sort(this.targetSorter);
        for (Entity entity : nearEntities) {
            if (!(entity instanceof EntityLiving) || !this.isSuitableTarget((EntityLiving)entity, true)) continue;
            this.targetEntity = (EntityLiving)entity;
            return true;
        }
        return false;
    }

    @Override
    public void startExecuting() {
        this.taskOwner.setAttackTarget(this.targetEntity);
        super.startExecuting();
    }
}

