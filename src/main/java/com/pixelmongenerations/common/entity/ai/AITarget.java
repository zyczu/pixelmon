/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai;

import com.pixelmongenerations.api.events.AggressionEvent;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQueryList;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.event.RepelHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.pathfinding.Path;
import net.minecraft.pathfinding.PathPoint;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.common.MinecraftForge;

public abstract class AITarget
extends EntityAIBase {
    protected EntityCreature taskOwner;
    protected float targetDistance;
    protected boolean shouldCheckSight;
    private boolean nearbyOnly;
    private int targetSearchStatus = 0;
    private int targetSearchDelay = 0;
    private int targetUnseenTicks = 0;

    public AITarget(EntityPixelmon par1EntityLiving, float par2, boolean par3) {
        this(par1EntityLiving, par2, par3, false);
        this.setMutexBits(3);
    }

    public AITarget(EntityCreature entity, float par2, boolean par3, boolean par4) {
        this.taskOwner = entity;
        this.targetDistance = par2;
        this.shouldCheckSight = par3;
        this.nearbyOnly = par4;
    }

    @Override
    public boolean shouldContinueExecuting() {
        EntityLivingBase var1;
        if (this.taskOwner instanceof EntityPixelmon) {
            EntityPixelmon pokemon = (EntityPixelmon)this.taskOwner;
            if (pokemon.battleController != null) return false;
            if (pokemon.getBossMode() != EnumBossMode.NotBoss) {
                return false;
            }
        } else if (((NPCTrainer)this.taskOwner).battleController != null) {
            return false;
        }
        if ((var1 = this.taskOwner.getAttackTarget()) == null) return false;
        if (!var1.isEntityAlive()) return false;
        if (this.taskOwner.getDistanceSq(var1) > (double)(this.targetDistance * this.targetDistance)) {
            return false;
        }
        if (!this.shouldCheckSight) return true;
        if (this.taskOwner instanceof NPCTrainer) {
            if (this.taskOwner.getEntitySenses().canSee(var1) && this.checkAngle(var1)) {
                this.targetUnseenTicks = 0;
                return true;
            }
            if (++this.targetUnseenTicks > 60) return false;
            return true;
        }
        if (this.taskOwner.getEntitySenses().canSee(var1)) {
            this.targetUnseenTicks = 0;
            return true;
        }
        if (++this.targetUnseenTicks > 60) return false;
        return true;
    }

    private boolean checkAngle(EntityLivingBase var1) {
        Vec3d look = this.taskOwner.getLook(1.0f);
        Vec3d toTarget = new Vec3d(this.taskOwner.posX - var1.posX, this.taskOwner.posY - var1.posY, this.taskOwner.posZ - var1.posZ);
        double top = look.x * toTarget.x + look.y * toTarget.y + look.z * toTarget.z;
        double a = Math.sqrt(Math.pow(look.x, 2.0) + Math.pow(look.y, 2.0) + Math.pow(look.z, 2.0));
        double angle = Math.acos(top / (a * Math.sqrt(Math.pow(toTarget.x, 2.0) + Math.pow(toTarget.y, 2.0) + Math.pow(toTarget.z, 2.0))));
        return Math.abs(angle - Math.PI) <= 0.2;
    }

    @Override
    public void startExecuting() {
        this.targetSearchStatus = 0;
        this.targetSearchDelay = 0;
        this.targetUnseenTicks = 0;
    }

    @Override
    public void resetTask() {
        this.taskOwner.setAttackTarget(null);
    }

    protected boolean isSuitableTarget(EntityLivingBase entity, boolean par2) {
        if (!(entity instanceof EntityPixelmon) && !(entity instanceof EntityPlayer) || entity == this.taskOwner || !entity.isEntityAlive()) {
            return false;
        }
        if (entity instanceof EntityPlayer && this.taskOwner instanceof NPCTrainer) {
            NPCTrainer trainer = (NPCTrainer)this.taskOwner;
            EntityPlayer player = (EntityPlayer)entity;
            ItemStack heldItem = player.getHeldItemMainhand();
            if (!heldItem.isEmpty() && heldItem.getItem() == PixelmonItems.trainerEditor && trainer.getAIMode().doesEngage() || !trainer.canStartBattle(player, false)) {
                return false;
            }
        }
        AxisAlignedBB entityBounds = entity.getEntityBoundingBox();
        AxisAlignedBB thisBounds = this.taskOwner.getEntityBoundingBox();
        if (entityBounds.maxY > thisBounds.minY && entityBounds.minY < thisBounds.maxY) {
            EntityLivingBase thisOwner = null;
            if (this.taskOwner instanceof EntityPixelmon) {
                thisOwner = ((EntityPixelmon)this.taskOwner).getOwner();
            }
            if (thisOwner != null) {
                EntityPixelmon targetPokemon;
                if (entity instanceof EntityPixelmon && ((targetPokemon = (EntityPixelmon)entity).getOwner() == thisOwner || targetPokemon.spawner != null || targetPokemon.isInRanchBlock)) {
                    return false;
                }
                if (entity == ((EntityTameable)this.taskOwner).getOwner()) {
                    return false;
                }
            } else {
                EntityPixelmon target;
                if (entity instanceof EntityPlayer && !par2 && ((EntityPlayer)entity).capabilities.disableDamage) {
                    return false;
                }
                if (entity instanceof EntityPlayerMP) {
                    Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)entity);
                    if (optstorage.isPresent()) {
                        if (optstorage.get().countAblePokemon() == 0) {
                            return false;
                        }
                        if (RepelHandler.hasRepel(entity.getUniqueID()) && optstorage.get().getHighestLevel() > ((EntityPixelmon)this.taskOwner).getLvl().getLevel()) {
                            return false;
                        }
                    }
                    if (MinecraftForge.EVENT_BUS.post(new AggressionEvent((EntityLiving)this.taskOwner, (EntityPlayerMP)entity))) {
                        return false;
                    }
                } else if (entity instanceof EntityPixelmon && MinecraftForge.EVENT_BUS.post(new AggressionEvent((EntityLiving)this.taskOwner, target = (EntityPixelmon)entity))) {
                    return false;
                }
            }
            if (!this.taskOwner.isWithinHomeDistanceCurrentPosition() || this.shouldCheckSight && !this.taskOwner.getEntitySenses().canSee(entity)) {
                return false;
            }
            if (this.nearbyOnly) {
                if (--this.targetSearchDelay <= 0) {
                    this.targetSearchStatus = 0;
                }
                if (this.targetSearchStatus == 0) {
                    int n = this.targetSearchStatus = this.canEasilyReach(entity) ? 1 : 2;
                }
                if (this.targetSearchStatus == 2) {
                    return false;
                }
            }
            if (entity instanceof EntityPlayerMP) {
                if (((EntityPlayer)entity).isCreative()) {
                    return false;
                }
                return EvolutionQueryList.get((EntityPlayerMP)entity) == null;
            }
            return true;
        }
        return false;
    }

    private boolean canEasilyReach(EntityLivingBase par1EntityLiving) {
        this.targetSearchDelay = 10 + this.taskOwner.getRNG().nextInt(5);
        Path var2 = this.taskOwner.getNavigator().getPathToEntityLiving(par1EntityLiving);
        if (var2 == null) {
            return false;
        }
        PathPoint var3 = var2.getFinalPathPoint();
        if (var3 == null) {
            return false;
        }
        int var4 = var3.x - MathHelper.floor(par1EntityLiving.posX);
        int var5 = var3.x - MathHelper.floor(par1EntityLiving.posZ);
        return (double)(var4 * var4 + var5 * var5) <= 2.25;
    }
}

