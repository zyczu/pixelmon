/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai;

import com.pixelmongenerations.common.entity.pixelmon.Entity7HasAI;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.network.ChatHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class AITargetEnvironment
extends EntityAIBase {
    public World world;
    public Entity7HasAI pickUpPixelmon;
    private static Biome OCEAN = Biome.REGISTRY.getObject(new ResourceLocation("ocean"));
    private static Biome RIVER = Biome.REGISTRY.getObject(new ResourceLocation("river"));
    private static Biome SWAMPLAND = Biome.REGISTRY.getObject(new ResourceLocation("swampland"));
    private static Biome FROZEN_RIVER = Biome.REGISTRY.getObject(new ResourceLocation("frozen_river"));
    private static Biome PLAINS = Biome.REGISTRY.getObject(new ResourceLocation("plains"));
    private static Biome FOREST = Biome.REGISTRY.getObject(new ResourceLocation("forest"));
    private static Biome FOREST_HILLS = Biome.REGISTRY.getObject(new ResourceLocation("forest_hills"));
    private static Biome JUNGLE = Biome.REGISTRY.getObject(new ResourceLocation("jungle"));
    private static Biome JUNGLE_HILLS = Biome.REGISTRY.getObject(new ResourceLocation("jungle_hills"));

    public AITargetEnvironment(Entity7HasAI entity) {
        this.world = Minecraft.getMinecraft().world;
        this.pickUpPixelmon = entity;
    }

    @Override
    public boolean shouldExecute() {
        if (this.pickUpPixelmon.hasOwner() && this.isCorrectBiomeForType()) {
            this.shouldSearch();
        }
        return true;
    }

    public boolean isCorrectBiomeForType() {
        Biome biome = this.world.getBiomeForCoordsBody(this.pickUpPixelmon.getPosition());
        boolean boolIsCorrect = false;
        for (EnumType type : this.pickUpPixelmon.type) {
            if (type == EnumType.Water) {
                if (biome != OCEAN && biome != RIVER && biome != SWAMPLAND && biome != FROZEN_RIVER) continue;
                boolIsCorrect = true;
                continue;
            }
            if (type == EnumType.Normal || type == EnumType.Fire || type == EnumType.Electric || type != EnumType.Grass || biome != PLAINS && biome != FOREST && biome != FOREST_HILLS && biome != JUNGLE && biome != JUNGLE_HILLS) continue;
            boolIsCorrect = true;
        }
        return boolIsCorrect;
    }

    public boolean shouldSearch() {
        int intSuccessChances = 1;
        int intTotalChances = 10;
        int rand = FMLCommonHandler.instance().getMinecraftServerInstance().getWorld((int)0).rand.nextInt(intTotalChances);
        this.sendPlayerMessage("aitargetenvironment.seessomething");
        return rand <= intSuccessChances;
    }

    public void sendPlayerMessage(String message) {
        EntityPlayerMP player = (EntityPlayerMP)this.pickUpPixelmon.getOwner();
        ChatHandler.sendChat(player, message, new Object[0]);
    }
}

