/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai.flying;

import com.pixelmongenerations.common.entity.ai.flying.FlySpeeds;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3i;

public class AIBetterFlying
extends EntityAIBase {
    EntityPixelmon pokemon;
    FlySpeeds flyingParameters;
    boolean wantsToFly = true;
    int actionTicks = -1;
    BlockPos target = null;
    float lastYaw;
    BlockPos.MutableBlockPos reusablePos = new BlockPos.MutableBlockPos(0, 0, 0);

    public AIBetterFlying(EntityPixelmon pokemon, FlySpeeds flyingParameters) {
        this.pokemon = pokemon;
        this.lastYaw = pokemon.rotationYaw;
        this.flyingParameters = flyingParameters;
    }

    public AIBetterFlying(EntityPixelmon pokemon) {
        this.pokemon = pokemon;
        this.flyingParameters = new FlySpeeds();
    }

    @Override
    public void startExecuting() {
        this.setMutexBits(1);
        if (this.pokemon.getPosition().getY() - this.pokemon.getEntityWorld().getTopSolidOrLiquidBlock(this.pokemon.getPosition()).getY() > 10) {
            this.wantsToFly = true;
            this.actionTicks = 1;
            this.shouldExecute();
        }
    }

    @Override
    public boolean shouldExecute() {
        --this.actionTicks;
        if (this.wantsToFly) {
            if (this.target == null && this.actionTicks <= 0) {
                this.findAerialTarget();
                if (this.target != null) {
                    this.actionTicks = (int)((double)this.flyingParameters.endurance * (((double)this.pokemon.getRNG().nextFloat() - 0.5) / 5.0));
                    this.moveToTarget();
                }
            } else if (this.target != null) {
                this.moveToTarget();
            }
            if (this.reachedTarget()) {
                this.findAerialTarget();
                if (this.target != null) {
                    this.moveToTarget();
                }
                return true;
            }
            if (this.actionTicks <= 0) {
                this.target = this.findLanding();
                if (this.target != null) {
                    this.wantsToFly = false;
                    this.actionTicks = this.flyingParameters.endurance / 3;
                    this.moveToTarget();
                }
            }
        } else {
            if (this.target != null && this.target.distanceSq(this.pokemon.getPosition()) > 4.0) {
                this.moveToTarget();
            }
            if (this.actionTicks <= 0) {
                this.wantsToFly = true;
                this.findAerialTarget();
            }
        }
        return false;
    }

    private double findAngle(double x, double z) {
        if (x >= 0.0 && z >= 0.0) {
            return MathHelper.atan2(x, z);
        }
        if (x <= 0.0 && z >= 0.0) {
            return Math.PI - MathHelper.atan2(x, z);
        }
        if (x <= 0.0 && z <= 0.0) {
            return Math.PI + MathHelper.atan2(x, z);
        }
        if (x >= 0.0 && z <= 0.0) {
            return Math.PI * 2 - MathHelper.atan2(x, z);
        }
        return 0.0;
    }

    public void moveToTarget() {
        double d;
        double diffX = (double)this.target.getX() - this.pokemon.getPositionVector().x;
        double diffZ = (double)this.target.getZ() - this.pokemon.getPositionVector().z;
        double angleDiff = this.findAngle(diffX, diffZ);
        double angleCurrent = this.findAngle(this.pokemon.motionX, this.pokemon.motionZ);
        double theta = angleDiff - angleCurrent;
        if (angleDiff - angleCurrent > 180.0) {
            theta = -1.0 * (360.0 - theta);
        }
        if (Math.abs(theta) > this.flyingParameters.rotationSpeed) {
            theta = this.flyingParameters.rotationSpeed;
        }
        double motionMagnitude = Math.sqrt(Math.pow(this.pokemon.motionX, 2.0) + Math.pow(this.pokemon.motionZ, 2.0));
        double diffMagnitude = Math.sqrt(Math.pow(diffX, 2.0) + Math.pow(diffZ, 2.0));
        if (this.pokemon.motionX == 0.0 && this.pokemon.motionZ == 0.0) {
            this.pokemon.motionZ = -0.01;
            this.pokemon.motionX = -0.01;
        }
        motionMagnitude += this.flyingParameters.acceleration;
        if (motionMagnitude > this.flyingParameters.maxFlySpeed) {
            motionMagnitude = this.flyingParameters.maxFlySpeed;
        }
        double desiredMotionX = Math.cos(theta) * this.pokemon.motionX - Math.sin(theta) * this.pokemon.motionZ;
        double desiredMotionZ = Math.sin(theta) * this.pokemon.motionX + Math.cos(theta) * this.pokemon.motionZ;
        double newMotionSpeed = Math.sqrt(Math.pow(desiredMotionX, 2.0) + Math.pow(desiredMotionZ, 2.0));
        double ratio = newMotionSpeed / motionMagnitude;
        double yDist = (double)this.target.getY() - this.pokemon.getPositionVector().y;
        this.pokemon.motionX = desiredMotionX / ratio;
        this.pokemon.motionY = yDist / diffMagnitude;
        this.pokemon.motionZ = desiredMotionZ /= ratio;
        float yaw = (float)Math.atan2(-this.pokemon.motionX, this.pokemon.motionZ);
        this.pokemon.rotationYaw = (float)((double)(yaw * 180.0f) / Math.PI);
    }

    public boolean reachedTarget() {
        return this.target != null && Math.sqrt(Math.pow(this.pokemon.getPositionVector().x - (double)this.target.getX(), 2.0) + Math.pow(this.pokemon.getPositionVector().z - (double)this.target.getZ(), 2.0)) <= 3.0;
    }

    public boolean findAerialTarget() {
        double baseDir = (double)this.pokemon.rotationYaw + (this.pokemon.getRNG().nextDouble() - 0.5) * 60.0;
        for (int i = 0; i < 3; ++i) {
            int radius = this.pokemon.getRNG().nextInt(30) + 50;
            double direction = baseDir + (double)(i * 60);
            int x = (int)((double)radius * Math.cos(direction * Math.PI / 180.0));
            int z = (int)((double)radius * Math.sin(direction * Math.PI / 180.0));
            this.reusablePos.setPos(this.pokemon.getEntityWorld().getTopSolidOrLiquidBlock(this.reusablePos.setPos(x, 1, z)));
            if (this.reusablePos.getY() <= 0 || this.reusablePos.getY() > 230) continue;
            this.target = this.reusablePos.add(0, 20, 0).toImmutable();
            return true;
        }
        return false;
    }

    public BlockPos findLanding() {
        IBlockState blockState;
        this.reusablePos.setPos(this.pokemon.getEntityWorld().getTopSolidOrLiquidBlock(this.pokemon.getPosition()));
        if (this.reusablePos.getY() > 0 && (blockState = this.pokemon.getEntityWorld().getBlockState(this.reusablePos.subtract(new Vec3i(0, 1, 0)))).isFullCube() && blockState.isFullBlock()) {
            return this.reusablePos.toImmutable();
        }
        return null;
    }
}

