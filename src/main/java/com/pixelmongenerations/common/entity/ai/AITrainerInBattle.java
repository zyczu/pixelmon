/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai;

import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import net.minecraft.entity.ai.EntityAIBase;

public class AITrainerInBattle
extends EntityAIBase {
    NPCTrainer trainer;

    public AITrainerInBattle(NPCTrainer trainer) {
        this.setMutexBits(1);
        this.trainer = trainer;
    }

    @Override
    public boolean shouldExecute() {
        return this.trainer.battleController != null;
    }

    @Override
    public boolean shouldContinueExecuting() {
        return this.trainer.battleController != null;
    }
}

