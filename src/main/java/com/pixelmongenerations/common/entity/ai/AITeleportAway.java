/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.ai;

import com.pixelmongenerations.common.entity.pixelmon.Entity7HasAI;
import java.util.Random;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;

public class AITeleportAway
extends EntityAIBase {
    Entity7HasAI pixelmon;
    Random rand;

    public AITeleportAway(Entity7HasAI entity) {
        this.pixelmon = entity;
        this.rand = entity.getRNG();
    }

    @Override
    public boolean shouldExecute() {
        return !this.pixelmon.hasOwner() && this.pixelmon.world.getClosestPlayerToEntity(this.pixelmon, 7.0) != null;
    }

    @Override
    public boolean shouldContinueExecuting() {
        return false;
    }

    @Override
    public void startExecuting() {
        AITeleportAway.teleportRandomly(this.pixelmon, this.rand);
    }

    public static boolean teleportRandomly(Entity7HasAI pixelmon, Random rand) {
        return AITeleportAway.teleportTo(pixelmon, rand, pixelmon.posX + (rand.nextDouble() - 0.5) * 64.0, 256.0, pixelmon.posZ + (rand.nextDouble() - 0.5) * 64.0);
    }

    protected static boolean teleportTo(Entity7HasAI pixelmon, Random rand, double suggestedXPos, double suggestedYPos, double suggestedZPos) {
        double currentXPos = pixelmon.posX;
        double currentYPos = pixelmon.posY;
        double currentZPos = pixelmon.posZ;
        int intPosX = MathHelper.floor(suggestedXPos);
        int intPosY = MathHelper.floor(suggestedYPos);
        int intPosZ = MathHelper.floor(suggestedZPos);
        BlockPos newLocation = pixelmon.world.getTopSolidOrLiquidBlock(new BlockPos(intPosX, intPosY, intPosZ));
        intPosY = newLocation.getY();
        if (pixelmon.world.isBlockLoaded(newLocation)) {
            pixelmon.posX = intPosX;
            pixelmon.posY = intPosY;
            pixelmon.posZ = intPosZ;
            pixelmon.setPosition(pixelmon.posX, pixelmon.posY, pixelmon.posZ);
            AITeleportAway.createEndermanEffect(pixelmon, rand, currentXPos, currentYPos, currentZPos);
            return pixelmon.world.getCollisionBoxes(pixelmon, pixelmon.getCollisionBoundingBox()).size() == 0 && !pixelmon.world.containsAnyLiquid(pixelmon.getCollisionBoundingBox());
        }
        return true;
    }

    private static void createEndermanEffect(Entity7HasAI pixelmon, Random rand, double currentXPos, double currentYPos, double currentZPos) {
        int baseArea = 128;
        for (int variance = 0; variance < baseArea; ++variance) {
            double bounding = (double)variance / ((double)baseArea - 1.0);
            float floatRandX = (rand.nextFloat() - 0.5f) * 0.2f;
            float floatRandY = (rand.nextFloat() - 0.5f) * 0.2f;
            float floatRandZ = (rand.nextFloat() - 0.5f) * 0.2f;
            double doubleRandX = currentXPos + (pixelmon.posX - currentXPos) * bounding + (rand.nextDouble() - 0.5) * (double)pixelmon.width * 2.0;
            double doubleRandY = currentYPos + (pixelmon.posY - currentYPos) * bounding + rand.nextDouble() * (double)pixelmon.height;
            double doubleRandZ = currentZPos + (pixelmon.posZ - currentZPos) * bounding + (rand.nextDouble() - 0.5) * (double)pixelmon.length * 2.0;
            pixelmon.world.spawnParticle(EnumParticleTypes.PORTAL, doubleRandX, doubleRandY, doubleRandZ, (double)floatRandX, (double)floatRandY, (double)floatRandZ, new int[0]);
        }
        pixelmon.world.playSound(null, currentXPos, currentYPos, currentZPos, SoundEvents.ENTITY_ENDERMEN_TELEPORT, SoundCategory.NEUTRAL, 1.0f, 1.0f);
    }
}

