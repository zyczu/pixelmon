/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.entity.bikes;

import com.pixelmongenerations.client.models.BikeModelSmd;
import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.client.models.smd.AnimationType;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.common.entity.pixelmon.helpers.animation.AnimationVariables;
import com.pixelmongenerations.common.entity.pixelmon.helpers.animation.IncrementingVariable;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.items.EnumBike;
import java.util.UUID;
import javax.annotation.Nullable;
import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityOwnable;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.jetbrains.annotations.NotNull;

public class EntityBike
extends EntityCreature
implements IEntityOwnable {
    public static DataParameter<String> dwBikeEnum = EntityDataManager.createKey(EntityBike.class, DataSerializers.STRING);
    private UUID ownerUUID;
    private AnimationType animationType;
    private AnimationVariables animationVariables;
    public boolean animationCounting = false;
    public boolean animationSwap = false;
    static int animationDelayLimit = 3;
    int animationDelayCounter = 0;

    public EntityBike(World world) {
        super(world);
        ((PathNavigateGround)this.getNavigator()).setCanSwim(true);
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.stepHeight = 1.0f;
        this.setEntityBoundingBox(new AxisAlignedBB(this.posX - 1.0, this.posY - 1.0, this.posZ - 2.0, this.posX + 1.0, this.posY + 1.0, this.posZ + 10.0));
        this.dataManager.register(dwBikeEnum, "Red");
    }

    public EntityBike(World world, EnumBike bike) {
        this(world);
        this.setEnumBike(bike);
    }

    @Override
    protected void entityInit() {
        super.entityInit();
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(PixelmonConfig.bikeSpeed);
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound compound) {
        super.writeEntityToNBT(compound);
        compound.setString("BikeEnum", this.dataManager.get(dwBikeEnum));
        compound.setUniqueId("owner", this.ownerUUID);
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound compound) {
        super.readEntityFromNBT(compound);
        this.dataManager.set(dwBikeEnum, EnumBike.get(compound.getString("BikeEnum")).name());
        this.ownerUUID = compound.getUniqueId("owner");
    }

    @Override
    public void setPosition(double x, double y, double z) {
        this.posX = x;
        this.posY = y;
        this.posZ = z;
        float scale = 2.0f;
        float halfWidth = this.width * scale / 2.0f;
        this.setEntityBoundingBox(new AxisAlignedBB(x - (double)halfWidth, y, z - (double)halfWidth, x + (double)halfWidth, y + 2.0, z + (double)halfWidth));
    }

    @Override
    public boolean canBeCollidedWith() {
        return true;
    }

    @Override
    public boolean canBePushed() {
        return this.isBeingRidden();
    }

    @Override
    protected boolean canDespawn() {
        return false;
    }

    @Override
    public boolean processInteract(EntityPlayer player, EnumHand hand) {
        if (!this.world.isRemote && !this.isDead && hand == EnumHand.MAIN_HAND) {
            if (this.getControllingPassenger() != null) {
                if (this.getControllingPassenger() == player) {
                    this.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
                return false;
            }
            if (this.hasOwner() && !this.getOwnerId().equals(player.getUniqueID()) && !player.isCreative()) {
                player.sendMessage(new TextComponentTranslation("entity.bike.notowner", new Object[0]));
                return false;
            }
            ItemStack item = player.getHeldItem(hand);
            if (player.isSneaking() && item.isEmpty()) {
                player.setHeldItem(hand, new ItemStack(this.getEnumBike().getItem()));
                this.setDead();
                return true;
            }
            if (!player.isSneaking()) {
                this.mount(player);
                return true;
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean attackEntityFrom(DamageSource source, float par2) {
        return false;
    }

    public void setEnumBike(EnumBike enumBike) {
        this.dataManager.set(dwBikeEnum, enumBike.name());
    }

    public EnumBike getEnumBike() {
        return EnumBike.get(this.dataManager.get(dwBikeEnum));
    }

    @Override
    public boolean canBeSteered() {
        return true;
    }

    @Override
    @Nullable
    public Entity getControllingPassenger() {
        Entity control = null;
        if (!this.getPassengers().isEmpty()) {
            control = this.getPassengers().get(0);
        }
        return control;
    }

    @Override
    public void travel(float strafe, float vertical, float forward) {
        if (this.isBeingRidden() && this.canBeSteered()) {
            EntityLivingBase entitylivingbase = (EntityLivingBase)this.getControllingPassenger();
            if (entitylivingbase == null) {
                super.travel(strafe, vertical, forward);
                return;
            }
            this.prevRotationYaw = this.rotationYaw = entitylivingbase.rotationYaw;
            this.rotationPitch = entitylivingbase.rotationPitch * 0.5f;
            this.setRotation(this.rotationYaw, this.rotationPitch);
            this.rotationYawHead = this.renderYawOffset = this.rotationYaw;
            strafe = entitylivingbase.moveStrafing * 0.5f;
            forward = entitylivingbase.moveForward;
            if (forward <= 0.0f) {
                forward *= 0.25f;
            }
            if (entitylivingbase.motionY > 0.0 && this.onGround) {
                this.jump();
            }
            this.jumpMovementFactor = this.getAIMoveSpeed() * 0.1f;
            if (this.canPassengerSteer()) {
                this.setAIMoveSpeed((float)this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getAttributeValue());
                if (forward > 0.0f) {
                    forward = 0.75f;
                }
                super.travel(0.0f, vertical, forward);
            } else if (entitylivingbase instanceof EntityPlayer) {
                this.motionX = 0.0;
                this.motionY = 0.0;
                this.motionZ = 0.0;
            }
            if (this.onGround) {
                this.isAirBorne = false;
            }
        } else {
            this.jumpMovementFactor = 0.02f;
            super.travel(strafe, vertical, forward);
        }
    }

    @Override
    public void fall(float distance, float damageMultiplier) {
        if (distance > 10.0f) {
            this.getEntityWorld().playSound(this.posX, this.posY, this.posZ, SoundEvents.BLOCK_ANVIL_LAND, SoundCategory.BLOCKS, 1.0f, 1.0f, false);
            if (!this.getEntityWorld().isRemote) {
                this.destroyBike();
            }
        }
    }

    private void destroyBike() {
        this.removePassengers();
        if (this.getControllingPassenger() != null) {
            this.getControllingPassenger().dismountRidingEntity();
        }
        if (!this.getEntityWorld().isRemote) {
            this.dropItem(Items.IRON_INGOT, 2);
        }
        this.setDead();
    }

    @Override
    public void updatePassenger(Entity passenger) {
        super.updatePassenger(passenger);
        if (this.isPassenger(passenger)) {
            Vec3d lookVec = this.getLookVec();
            lookVec = new Vec3d(lookVec.x, 0.0, lookVec.z).normalize();
            lookVec = lookVec.rotateYaw((float)Math.toRadians(180.0));
            float distance = 0.3f;
            passenger.setPosition(this.posX + lookVec.x * (double)distance, this.posY + this.getMountedYOffset() + passenger.getYOffset(), this.posZ + lookVec.z * (double)distance);
        }
    }

    @Override
    public double getMountedYOffset() {
        return 0.7;
    }

    public void mount(EntityPlayer strafeEntityPlayer) {
        strafeEntityPlayer.rotationYaw = this.rotationYaw;
        strafeEntityPlayer.rotationPitch = this.rotationPitch;
        if (!this.world.isRemote) {
            strafeEntityPlayer.startRiding(this);
        }
    }

    @Override
    public boolean shouldDismountInWater(@NotNull Entity rider) {
        return false;
    }

    public ResourceLocation getTexture() {
        return this.getEnumBike().getTexture();
    }

    public void setOwner(UUID uniqueID) {
        this.ownerUUID = uniqueID;
    }

    @Override
    @Nullable
    public UUID getOwnerId() {
        return this.ownerUUID;
    }

    @Override
    @Nullable
    public Entity getOwner() {
        if (!this.hasOwner()) {
            return null;
        }
        return this.getEntityWorld().getPlayerEntityByUUID(this.ownerUUID);
    }

    public boolean hasOwner() {
        return this.ownerUUID != null;
    }

    public AnimationVariables getAnimationVariables() {
        if (this.animationVariables == null) {
            this.animationVariables = new AnimationVariables();
        }
        return this.animationVariables;
    }

    public ModelBase getModel() {
        return PixelmonModelRegistry.bikeModel.getModel();
    }

    public void initAnimation() {
        ModelBase base;
        if (this.world.isRemote && (base = this.getModel()) instanceof BikeModelSmd) {
            ValveStudioModel model = ((BikeModelSmd)base).theModel;
            model.animate();
        }
    }

    protected void checkAnimation() {
        IncrementingVariable inc;
        if (!(this.getModel() instanceof BikeModelSmd)) {
            return;
        }
        BikeModelSmd smdModel = (BikeModelSmd)this.getModel();
        float f1 = this.prevLimbSwingAmount + (this.limbSwingAmount - this.prevLimbSwingAmount);
        if (f1 > 1.0f) {
            f1 = 1.0f;
        }
        if ((inc = this.getAnimationVariables().getCounter(-1)) == null) {
            this.getAnimationVariables().setCounter(-1, 2.14748365E9f, smdModel.animationIncrement);
        } else {
            inc.increment = smdModel.animationIncrement;
        }
        if (!this.animationCounting) {
            this.setAnimation(AnimationType.IDLE);
            this.animationCounting = true;
        }
        if (f1 > smdModel.movementThreshold) {
            if (this.animationSwap) {
                this.setAnimation(AnimationType.WALK);
            }
        } else if (this.animationSwap) {
            this.setAnimation(AnimationType.IDLE);
        }
        if (smdModel.theModel.currentAnimation == null) {
            this.setAnimation(AnimationType.IDLE);
        }
    }

    public void setAnimation(AnimationType animation) {
        this.animationType = animation;
    }

    public AnimationType getCurrentAnimation() {
        if (this.animationType == null) {
            return AnimationType.IDLE;
        }
        return this.animationType;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void onUpdate() {
        super.onUpdate();
        if (this.world.isRemote) {
            if (this.getCurrentAnimation() != AnimationType.IDLE && this.animationVariables != null) {
                this.animationVariables.tick();
                this.animationVariables.tick();
            }
            if (this.animationCounting) {
                if (this.animationDelayCounter < animationDelayLimit) {
                    ++this.animationDelayCounter;
                    this.animationSwap = false;
                }
                if (this.animationDelayCounter >= animationDelayLimit) {
                    this.animationSwap = true;
                    this.animationDelayCounter = 0;
                }
            } else {
                this.animationDelayCounter = 0;
                this.animationSwap = false;
            }
            this.checkAnimation();
        }
    }
}

