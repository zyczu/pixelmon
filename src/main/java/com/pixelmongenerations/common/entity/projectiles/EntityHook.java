/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.common.entity.projectiles;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.events.FishingEvent;
import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.archetypes.entities.items.SpawnActionItem;
import com.pixelmongenerations.api.spawning.archetypes.entities.pokemon.SpawnActionPokemon;
import com.pixelmongenerations.api.spawning.conditions.LocationType;
import com.pixelmongenerations.api.spawning.conditions.TriggerLocation;
import com.pixelmongenerations.api.spawning.util.SpatialData;
import com.pixelmongenerations.api.world.MutableLocation;
import com.pixelmongenerations.common.battle.BattleFactory;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.item.ItemFishingRod;
import com.pixelmongenerations.common.spawning.PixelmonSpawning;
import com.pixelmongenerations.core.config.BetterSpawnerConfig;
import com.pixelmongenerations.core.enums.EnumMark;
import com.pixelmongenerations.core.enums.EnumMarkActivity;
import com.pixelmongenerations.core.enums.items.EnumRodType;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import io.netty.buffer.ByteBuf;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityFishHook;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityHook
extends EntityFishHook
implements IEntityAdditionalSpawnData {
    public static final DataParameter<Integer> DATA_HOOK_STATE = EntityDataManager.createKey(EntityHook.class, DataSerializers.VARINT);
    BattleControllerBase bc;
    PlayerParticipant player1;
    public EnumRodType rodType;
    public SpawnAction<? extends Entity> caught = null;
    public int ticksTillCatch = 0;
    public int ticksTillEscape = 0;
    public float chanceOfNothing = 0.1f;
    public HookState state = HookState.FLYING;
    public int dotsShowing = 0;
    public int momentsShowingDots = 0;
    private HashMap<String, Integer> pixelmonRarity = new HashMap();

    public EntityHook(World world) {
        super(world, null);
    }

    @SideOnly(value=Side.CLIENT)
    public EntityHook(World world, EntityPlayer player, double x, double y, double z) {
        super(world, player, x, y, z);
    }

    public EntityHook(World world, EntityPlayer player, EnumRodType rodType) {
        super(world, player);
        this.rodType = rodType;
    }

    @Override
    protected void entityInit() {
        this.isImmuneToFire = true;
        this.getDataManager().register(DATA_HOOK_STATE, -1);
        this.getDataManager().register(DATA_HOOKED_ENTITY, 0);
    }

    public void init(EntityPlayer player) {
        this.setSize(0.25f, 0.25f);
        this.ignoreFrustumCheck = true;
        if (player != null) {
            this.angler = player;
            this.angler.fishEntity = this;
        }
    }

    public void shoot() {
        if (this.angler != null) {
            super.shoot();
        }
    }

    @Override
    public void writeSpawnData(ByteBuf buffer) {
        UUID uuid = this.angler != null ? this.angler.getUniqueID() : new UUID(0L, 0L);
        buffer.writeLong(uuid.getMostSignificantBits());
        buffer.writeLong(uuid.getLeastSignificantBits());
    }

    @Override
    public void readSpawnData(ByteBuf additionalData) {
        long most = additionalData.readLong();
        long least = additionalData.readLong();
        UUID uuid = new UUID(most, least);
        this.setAngler(this.world.getPlayerEntityByUUID(uuid));
    }

    public void setAngler(Entity entity) {
        if (entity instanceof EntityPlayer) {
            this.angler = (EntityPlayer)entity;
            this.angler.fishEntity = this;
            this.shoot();
        }
    }

    @Override
    public void onUpdate() {
        if (this.angler == null || this.angler.fishEntity != this) {
            this.setDead();
            return;
        }
        if (this.world.isRemote) {
            ++this.momentsShowingDots;
            if (this.momentsShowingDots > 40) {
                this.momentsShowingDots = 0;
                this.dotsShowing = (this.dotsShowing + 1) % 3;
            }
        }
        --this.ticksTillCatch;
        --this.ticksTillEscape;
        if (this.ticksTillEscape == 0 && this.caught != null) {
            this.caught = null;
            if (this.angler != null) {
                ChatHandler.sendFormattedChat(this.angler, TextFormatting.GRAY, "pixelmon.projectiles.gotaway", new Object[0]);
                if (!this.world.isRemote) {
                    this.setDead();
                }
            }
        }
        this.onEntityUpdate();
        if (this.world.isRemote || !this.shouldStopFishing()) {
            if (this.inGround) {
                ++this.ticksInGround;
                if (this.ticksInGround >= 1200) {
                    this.setDead();
                    return;
                }
            }
            float f = 0.0f;
            BlockPos blockpos = new BlockPos(this);
            IBlockState blockState = this.world.getBlockState(blockpos);
            if (BetterSpawnerConfig.getWaterBlocks().contains(blockState.getBlock().getRegistryName().toString()) || BetterSpawnerConfig.getLavaBlocks().contains(blockState.getBlock().getRegistryName().toString())) {
                f = BlockLiquid.getBlockLiquidHeight(blockState, this.world, blockpos);
            }
            if (this.state == HookState.FLYING) {
                if (this.caughtEntity != null) {
                    this.motionX = 0.0;
                    this.motionY = 0.0;
                    this.motionZ = 0.0;
                    this.state = HookState.HOOKED_IN_ENTITY;
                    return;
                }
                if (f > 0.0f) {
                    this.motionX *= 0.3;
                    this.motionY *= 0.2;
                    this.motionZ *= 0.3;
                    this.state = HookState.BOBBING;
                    if (!this.world.isRemote) {
                        this.setNewTimeToCatch();
                    }
                    return;
                }
                if (!this.world.isRemote) {
                    this.checkCollision();
                }
                if (!(this.inGround || this.onGround || this.collidedHorizontally)) {
                    ++this.ticksInAir;
                } else {
                    this.ticksInAir = 0;
                    this.motionX = 0.0;
                    this.motionY = 0.0;
                    this.motionZ = 0.0;
                }
            } else {
                if (this.state == HookState.HOOKED_IN_ENTITY) {
                    if (this.caughtEntity != null) {
                        if (this.caughtEntity.isDead) {
                            this.caughtEntity = null;
                            this.state = HookState.FLYING;
                        } else {
                            this.posX = this.caughtEntity.posX;
                            double d2 = this.caughtEntity.height;
                            this.posY = this.caughtEntity.getEntityBoundingBox().minY + d2 * 0.8;
                            this.posZ = this.caughtEntity.posZ;
                            this.setPosition(this.posX, this.posY, this.posZ);
                        }
                    }
                    return;
                }
                if (this.state == HookState.BOBBING) {
                    this.motionX *= 0.9;
                    this.motionZ *= 0.9;
                    double d0 = this.posY + this.motionY - (double)blockpos.getY() - (double)f;
                    if (Math.abs(d0) < 0.01) {
                        d0 += Math.signum(d0) * 0.1;
                    }
                    if (!this.world.isRemote && this.ticksTillCatch == 0 && this.angler != null && this.angler.getServer() != null) {
                        if (f > 0.0f && this.caughtEntity == null) {
                            this.motionY -= (double)Math.abs(this.rand.nextFloat()) * 0.2;
                            TriggerLocation loc = null;
                            if (BetterSpawnerConfig.getWaterBlocks().contains(blockState.getBlock().getRegistryName().toString())) {
                                loc = this.rodType == EnumRodType.OldRod ? LocationType.OLD_ROD : (this.rodType == EnumRodType.GoodRod ? LocationType.GOOD_ROD : LocationType.SUPER_ROD);
                            } else if (BetterSpawnerConfig.getLavaBlocks().contains(blockState.getBlock().getRegistryName().toString())) {
                                TriggerLocation triggerLocation = this.rodType == EnumRodType.OldRod ? LocationType.OLD_ROD_LAVA : (loc = this.rodType == EnumRodType.GoodRod ? LocationType.GOOD_ROD_LAVA : LocationType.SUPER_ROD_LAVA);
                            }
                            if (PixelmonSpawning.fishingSpawner == null) {
                                return;
                            }
                            SpatialData data = PixelmonSpawning.fishingSpawner.calculateSpatialData(this.world, blockpos, 10, true, block -> {
                                if (BetterSpawnerConfig.getWaterBlocks().contains(block.getRegistryName().toString())) {
                                    return true;
                                }
                                if (BetterSpawnerConfig.getLavaBlocks().contains(block.getRegistryName().toString())) {
                                    return true;
                                }
                                return BetterSpawnerConfig.getAirBlocks().contains(block.getRegistryName().toString());
                            });
                            --data.radius;
                            boolean canSeeSky = true;
                            BlockPos.MutableBlockPos mutablePos = new BlockPos.MutableBlockPos(0, 0, 0);
                            for (int y = blockpos.getY() + 1; y < 256; ++y) {
                                if (BetterSpawnerConfig.doesBlockSeeSky(this.world.getBlockState(mutablePos.setPos(blockpos.getX(), y, blockpos.getZ())))) continue;
                                canSeeSky = false;
                                break;
                            }
                            SpawnLocation spawnLocation = new SpawnLocation(new MutableLocation(this.world, blockpos), Lists.newArrayList(loc), data.baseBlock, data.uniqueSurroundingBlocks, this.world.getBiome(blockpos), canSeeSky, data.radius);
                            SpawnAction<? extends Entity> caught = PixelmonSpawning.fishingSpawner.getAction(spawnLocation, 1.0f - this.chanceOfNothing);
                            if (data.radius == 0) {
                                caught = null;
                            }
                            int displayedMarks = 1;
                            Event catchEvent = null;
                            if (caught != null) {
                                this.caught = caught;
                                int ticksTillEscape = 30;
                                float nominalRarity = caught.spawnInfo.calculateNominalRarity();
                                displayedMarks = nominalRarity < 5.0f ? 3 : (nominalRarity < 50.0f ? 2 : 1);
                                catchEvent = new FishingEvent.FishingCatchEvent((EntityPlayerMP)this.angler, this, caught, ticksTillEscape, displayedMarks);
                                if (MinecraftForge.EVENT_BUS.post(catchEvent)) {
                                    caught = null;
                                } else {
                                    this.caught = ((FishingEvent.FishingCatchEvent)catchEvent).getPlannedSpawn();
                                    this.getDataManager().set(DATA_HOOK_STATE, ((FishingEvent.FishingCatchEvent)catchEvent).getDisplayedMarks());
                                    this.ticksTillEscape = ((FishingEvent.FishingCatchEvent)catchEvent).getTicksTillEscape();
                                }
                            }
                            if (caught == null) {
                                if (catchEvent == null || !catchEvent.isCanceled()) {
                                    ChatHandler.sendFormattedChat(this.angler, TextFormatting.GRAY, "pixelmon.projectiles.nibble", new Object[0]);
                                }
                                this.setDead();
                            }
                        }
                    } else {
                        this.motionY -= d0 * (double)this.rand.nextFloat() * 0.2;
                    }
                }
            }
            if (blockState.getMaterial() != Material.WATER && blockState.getMaterial() != Material.LAVA) {
                this.motionY -= 0.03;
            }
            this.move(MoverType.SELF, this.motionX, this.motionY, this.motionZ);
            this.updateRotation();
            this.motionX *= 0.92;
            this.motionY *= 0.92;
            this.motionZ *= 0.92;
            this.setPosition(this.posX, this.posY, this.posZ);
        }
    }

    public void setNewTimeToCatch() {
        this.caught = null;
        this.ticksTillCatch = 20 * RandomHelper.getRandomNumberBetween(5, 15);
        FishingEvent.FishingCastEvent castEvent = new FishingEvent.FishingCastEvent((EntityPlayerMP)this.angler, this, this.ticksTillCatch, 0.2f);
        MinecraftForge.EVENT_BUS.post(castEvent);
        this.ticksTillCatch = castEvent.getTicksUntilCatch();
        this.chanceOfNothing = castEvent.getChanceOfNothing();
        this.getDataManager().set(DATA_HOOK_STATE, 0);
    }

    public boolean shouldStopFishing() {
        ItemStack itemstack = this.angler.getHeldItemMainhand();
        ItemStack itemstack1 = this.angler.getHeldItemOffhand();
        boolean flag = itemstack.getItem() instanceof ItemFishingRod && ((ItemFishingRod)itemstack.getItem()).getRodType() == this.rodType;
        boolean flag1 = itemstack1.getItem() instanceof ItemFishingRod && ((ItemFishingRod)itemstack1.getItem()).getRodType() == this.rodType;
        boolean bl = flag1;
        if (!this.angler.isDead && this.angler.isEntityAlive() && (flag || flag1) && this.getDistanceSq(this.angler) <= 1024.0) {
            return false;
        }
        this.setDead();
        return true;
    }

    private void updateRotation() {
        float f = MathHelper.sqrt(this.motionX * this.motionX + this.motionZ * this.motionZ);
        this.rotationYaw = (float)(MathHelper.atan2(this.motionX, this.motionZ) * 57.29577951308232);
        this.rotationPitch = (float)(MathHelper.atan2(this.motionY, f) * 57.29577951308232);
        while (this.rotationPitch - this.prevRotationPitch < -180.0f) {
            this.prevRotationPitch -= 360.0f;
        }
        while (this.rotationPitch - this.prevRotationPitch >= 180.0f) {
            this.prevRotationPitch += 360.0f;
        }
        while (this.rotationYaw - this.prevRotationYaw < -180.0f) {
            this.prevRotationYaw -= 360.0f;
        }
        while (this.rotationYaw - this.prevRotationYaw >= 180.0f) {
            this.prevRotationYaw += 360.0f;
        }
        this.rotationPitch = this.prevRotationPitch + (this.rotationPitch - this.prevRotationPitch) * 0.2f;
        this.rotationYaw = this.prevRotationYaw + (this.rotationYaw - this.prevRotationYaw) * 0.2f;
    }

    private void checkCollision() {
        Vec3d vec3d = new Vec3d(this.posX, this.posY, this.posZ);
        Vec3d vec3d1 = new Vec3d(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
        RayTraceResult raytraceresult = this.world.rayTraceBlocks(vec3d, vec3d1, false, true, false);
        vec3d = new Vec3d(this.posX, this.posY, this.posZ);
        vec3d1 = new Vec3d(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
        if (raytraceresult != null) {
            vec3d1 = new Vec3d(raytraceresult.hitVec.x, raytraceresult.hitVec.y, raytraceresult.hitVec.z);
        }
        Entity entity = null;
        List<Entity> list = this.world.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(this.motionX, this.motionY, this.motionZ).grow(1.0));
        double d0 = 0.0;
        for (Entity entity1 : list) {
            double d;
            AxisAlignedBB axisalignedbb;
            RayTraceResult raytraceresult1;
            if (!this.canBeHooked(entity1) || entity1 == this.angler && this.ticksInAir < 5 || (raytraceresult1 = (axisalignedbb = entity1.getEntityBoundingBox().grow(0.3f)).calculateIntercept(vec3d, vec3d1)) == null) continue;
            double d1 = vec3d.squareDistanceTo(raytraceresult1.hitVec);
            if (d1 >= d0 && d0 != 0.0) continue;
            entity = entity1;
            d0 = d1;
        }
        if (entity != null) {
            raytraceresult = new RayTraceResult(entity);
        }
        if (raytraceresult != null && raytraceresult.typeOfHit != RayTraceResult.Type.MISS) {
            if (raytraceresult.typeOfHit == RayTraceResult.Type.ENTITY) {
                this.caughtEntity = raytraceresult.entityHit;
                this.getDataManager().set(DATA_HOOKED_ENTITY, this.caughtEntity.getEntityId() + 1);
            } else {
                this.inGround = true;
                this.ticksCatchable = 0;
                this.ticksTillEscape = 0;
                this.caught = null;
                this.state = HookState.FLYING;
                this.getDataManager().set(DATA_HOOK_STATE, -1);
            }
        }
    }

    @Override
    public int handleHookRetraction() {
        if (this.world.isRemote) {
            return 0;
        }
        int b0 = 0;
        if (this.caughtEntity != null) {
            this.bringInHookedEntity();
            this.world.setEntityState(this, (byte)31);
            b0 = 3;
            this.setDead();
            return b0;
        }
        EntityPlayerMP player = (EntityPlayerMP)this.angler;
        Entity entity = this.caught == null ? null : this.caught.doSpawn(PixelmonSpawning.fishingSpawner);
        FishingEvent.FishingReelEvent reelEvent = new FishingEvent.FishingReelEvent(player, this, entity);
        MinecraftForge.EVENT_BUS.post(reelEvent);
        if (this.caught != null) {
            if (entity == null) {
                return b0;
            }
            if (this.caught instanceof SpawnActionPokemon) {
                EntityPixelmon pokemon = (EntityPixelmon)((SpawnActionPokemon)this.caught).getOrCreateEntity();
                pokemon.setSpawnLocation(com.pixelmongenerations.core.database.SpawnLocation.Water);
                pokemon.resetAI();
                PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage(player).get();
                if (storage.countAblePokemon() > 0) {
                    pokemon.setMark(EnumMark.rollMark(player, pokemon, EnumMarkActivity.Fishing));
                    BattleFactory.createBattle().team1(new WildPixelmonParticipant(pokemon)).team2(new PlayerParticipant(player, storage.getFirstAblePokemon(this.world))).startBattle();
                }
            } else if (this.caught instanceof SpawnActionItem) {
                EntityItem caughtItem = (EntityItem)entity;
                ItemStack stack = caughtItem.getItem();
                caughtItem.setDead();
                ChatHandler.sendFormattedChat(this.angler, TextFormatting.GREEN, "pixelmon.projectiles.fisheditem", stack.getCount(), stack.getItem().getItemStackDisplayName(stack));
                DropItemHelper.giveItemStackToPlayer(this.angler, (Integer)stack.getCount(), stack);
            }
        }
        if (this.inGround) {
            b0 = 2;
        }
        this.setDead();
        this.angler.fishEntity = null;
        return b0;
    }

    @Override
    public boolean writeToNBTOptional(NBTTagCompound compound) {
        return false;
    }

    public static enum HookState {
        BOBBING,
        FLYING,
        HOOKED_IN_ENTITY;

    }
}

