/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.projectiles;

import com.pixelmongenerations.api.events.battles.DefeatNobleEvent;
import com.pixelmongenerations.common.battle.attacks.animations.AttackAnimationBiggerLeapForward;
import com.pixelmongenerations.common.battle.attacks.animations.AttackAnimationChargeAtPlayer;
import com.pixelmongenerations.common.battle.attacks.animations.AttackAnimationLaunchProjectile;
import com.pixelmongenerations.common.battle.nobles.NobleBattle;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.UUID;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeEventFactory;

public class EntityMarshBalm
extends EntityThrowable {
    public EntityMarshBalm(World worldIn) {
        super(worldIn);
        this.setSize(0.25f, 0.25f);
    }

    public EntityMarshBalm(World world, EntityPlayer entityPlayer) {
        super(world, entityPlayer);
    }

    @Override
    public void onUpdate() {
        this.lastTickPosX = this.posX;
        this.lastTickPosY = this.posY;
        this.lastTickPosZ = this.posZ;
        if (this.throwableShake > 0) {
            --this.throwableShake;
        }
        if (this.inGround) {
            if (this.world.getBlockState(new BlockPos(this.xTile, this.yTile, this.zTile)).getBlock() == this.inTile) {
                ++this.ticksInGround;
                if (this.ticksInGround == 1200) {
                    this.setDead();
                }
                return;
            }
            this.inGround = false;
            this.motionX *= (double)(this.rand.nextFloat() * 0.2f);
            this.motionY *= (double)(this.rand.nextFloat() * 0.2f);
            this.motionZ *= (double)(this.rand.nextFloat() * 0.2f);
            this.ticksInGround = 0;
            this.ticksInAir = 0;
        } else {
            ++this.ticksInAir;
        }
        Vec3d vec3d = new Vec3d(this.posX, this.posY, this.posZ);
        Vec3d vec3d1 = new Vec3d(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
        RayTraceResult raytraceresult = this.world.rayTraceBlocks(vec3d, vec3d1, true);
        vec3d = new Vec3d(this.posX, this.posY, this.posZ);
        vec3d1 = new Vec3d(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
        if (raytraceresult != null) {
            vec3d1 = new Vec3d(raytraceresult.hitVec.x, raytraceresult.hitVec.y, raytraceresult.hitVec.z);
        }
        Entity entity = null;
        List<Entity> list = this.world.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(this.motionX, this.motionY, this.motionZ).grow(1.0));
        double d0 = 0.0;
        boolean flag = false;
        for (Entity entity1 : list) {
            double d1;
            if (!entity1.canBeCollidedWith()) continue;
            if (entity1 == this.ignoreEntity) {
                flag = true;
                continue;
            }
            if (this.thrower != null && this.ticksExisted < 2 && this.ignoreEntity == null) {
                this.ignoreEntity = entity1;
                flag = true;
                continue;
            }
            flag = false;
            AxisAlignedBB axisalignedbb = entity1.getEntityBoundingBox().grow(0.3f);
            RayTraceResult raytraceresult1 = axisalignedbb.calculateIntercept(vec3d, vec3d1);
            if (raytraceresult1 == null || !((d1 = vec3d.squareDistanceTo(raytraceresult1.hitVec)) < d0) && d0 != 0.0) continue;
            entity = entity1;
            d0 = d1;
        }
        if (this.ignoreEntity != null) {
            if (flag) {
                this.ignoreTime = 2;
            } else if (this.ignoreTime-- <= 0) {
                this.ignoreEntity = null;
            }
        }
        if (entity != null) {
            raytraceresult = new RayTraceResult(entity);
        }
        if (raytraceresult != null) {
            if (raytraceresult.typeOfHit == RayTraceResult.Type.BLOCK && this.world.getBlockState(raytraceresult.getBlockPos()).getBlock() == Blocks.PORTAL) {
                this.setPortal(raytraceresult.getBlockPos());
            } else if (!ForgeEventFactory.onProjectileImpact(this, raytraceresult) && !this.world.isRemote) {
                this.onImpact(raytraceresult);
            }
        }
        this.posX += this.motionX;
        this.posY += this.motionY;
        this.posZ += this.motionZ;
        float f = MathHelper.sqrt(this.motionX * this.motionX + this.motionZ * this.motionZ);
        this.rotationYaw = (float)(MathHelper.atan2(this.motionX, this.motionZ) * 57.29577951308232);
        this.rotationPitch = (float)(MathHelper.atan2(this.motionY, f) * 57.29577951308232);
        while (this.rotationPitch - this.prevRotationPitch < -180.0f) {
            this.prevRotationPitch -= 360.0f;
        }
        while (this.rotationPitch - this.prevRotationPitch >= 180.0f) {
            this.prevRotationPitch += 360.0f;
        }
        while (this.rotationYaw - this.prevRotationYaw < -180.0f) {
            this.prevRotationYaw -= 360.0f;
        }
        while (this.rotationYaw - this.prevRotationYaw >= 180.0f) {
            this.prevRotationYaw += 360.0f;
        }
        this.rotationPitch = this.prevRotationPitch + (this.rotationPitch - this.prevRotationPitch) * 0.2f;
        this.rotationYaw = this.prevRotationYaw + (this.rotationYaw - this.prevRotationYaw) * 0.2f;
        float f1 = 0.99f;
        float f2 = this.getGravityVelocity();
        if (this.isInWater()) {
            for (int j = 0; j < 4; ++j) {
                float f3 = 0.25f;
                this.world.spawnParticle(EnumParticleTypes.WATER_BUBBLE, this.posX - this.motionX * 0.25, this.posY - this.motionY * 0.25, this.posZ - this.motionZ * 0.25, this.motionX, this.motionY, this.motionZ, new int[0]);
            }
            f1 = 0.8f;
        }
        this.motionX *= (double)f1;
        this.motionY *= (double)f1;
        this.motionZ *= (double)f1;
        if (!this.hasNoGravity()) {
            this.motionY -= (double)f2;
        }
        this.setPosition(this.posX, this.posY, this.posZ);
    }

    @Override
    protected void onImpact(RayTraceResult result) {
        if (result.entityHit != null && result.entityHit instanceof EntityPixelmon) {
            this.setDead();
            EntityPixelmon pokemonHit = (EntityPixelmon)result.entityHit;
            if (pokemonHit.getSpecies() == EnumSpecies.Lilligant && pokemonHit.getForm() == 11) {
                UUID pokemonUUID = pokemonHit.getUniqueID();
                UUID playerUUID = this.thrower.getUniqueID();
                if (!NobleBattle.nobleBattleMap.containsKey(playerUUID)) {
                    return;
                }
                try {
                    NobleBattle.nobleBattleMap.entrySet().removeIf(b -> {
                        NobleBattle battle;
                        EntityPixelmon noblePokemon;
                        UUID uuid = (UUID)b.getKey();
                        if (uuid.toString().equalsIgnoreCase(playerUUID.toString()) && (noblePokemon = (battle = (NobleBattle)b.getValue()).getPokemon().getEntityPixelmon()).getUniqueID().toString().equalsIgnoreCase(pokemonUUID.toString())) {
                            double damageModifier = RandomHelper.getRandomChance(0.06) ? 3.75 : (double)RandomHelper.getRandomNumberBetween(2.4f, 2.8f);
                            float damageOutput = (float)(1.52 * damageModifier);
                            battle.currentHealth -= damageOutput;
                            if (battle.currentHealth <= 0.0f) {
                                battle.getFrenzyGauge().setVisible(false);
                                battle.getFrenzyGauge().removePlayer(battle.getPlayer().getEntityPlayer());
                                battle.getPlayer().getEntityPlayer().sendMessage(new TextComponentString("You defeated Lilligant!"));
                                DefeatNobleEvent event = new DefeatNobleEvent(battle.getPlayer().getEntityPlayer(), battle.getPokemon().getEntityPixelmon());
                                MinecraftForge.EVENT_BUS.post(event);
                                battle.getPokemon().getEntityPixelmon().setDead();
                                return true;
                            }
                            battle.updateFrenzyGauge();
                            if (RandomHelper.getRandomChance(0.33)) {
                                AttackAnimationBiggerLeapForward attack = new AttackAnimationBiggerLeapForward();
                                attack.doMove(battle.getPokemon().getEntityPixelmon(), battle.getPlayer().getEntityPlayer());
                            } else if (RandomHelper.getRandomChance(0.33)) {
                                AttackAnimationLaunchProjectile attack = new AttackAnimationLaunchProjectile();
                                attack.doMove(battle.getPokemon().getEntityPixelmon(), battle.getPlayer().getEntityPlayer());
                            } else if (RandomHelper.getRandomChance(0.33)) {
                                AttackAnimationChargeAtPlayer attack = new AttackAnimationChargeAtPlayer();
                                attack.doMove(battle.getPokemon().getEntityPixelmon(), battle.getPlayer().getEntityPlayer());
                            }
                        }
                        return false;
                    });
                }
                catch (ConcurrentModificationException concurrentModificationException) {
                    // empty catch block
                }
            }
        }
    }
}

