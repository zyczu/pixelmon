/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.projectiles;

import com.pixelmongenerations.common.battle.nobles.NobleBattle;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import java.util.UUID;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class EntityAvaluggIce
extends EntityThrowable {
    private double targetX;
    private double targetY;
    private double targetZ;
    private int despawnTimer;

    public EntityAvaluggIce(World worldIn) {
        super(worldIn);
    }

    public EntityAvaluggIce(World world, EntityLiving entityPlayer) {
        super(world, entityPlayer);
    }

    public void moveTowards(BlockPos pos) {
        double d0 = pos.getX();
        int i = pos.getY();
        double d2 = d0 - this.posX;
        double d1 = pos.getZ();
        double d3 = d1 - this.posZ;
        float f = MathHelper.sqrt(d2 * d2 + d3 * d3);
        if (f > 12.0f) {
            this.targetX = this.posX + d2 / (double)f * 12.0;
            this.targetZ = this.posZ + d3 / (double)f * 12.0;
            this.targetY = this.posY + 8.0;
        } else {
            this.targetX = d0;
            this.targetY = i;
            this.targetZ = d1;
        }
        this.despawnTimer = 0;
    }

    @Override
    public void onUpdate() {
        this.lastTickPosX = this.posX;
        this.lastTickPosY = this.posY;
        this.lastTickPosZ = this.posZ;
        super.onUpdate();
        this.posX += this.motionX;
        this.posY += this.motionY;
        this.posZ += this.motionZ;
        float f = MathHelper.sqrt(this.motionX * this.motionX + this.motionZ * this.motionZ);
        this.rotationYaw = (float)(MathHelper.atan2(this.motionX, this.motionZ) * 57.29577951308232);
        this.rotationPitch = (float)(MathHelper.atan2(this.motionY, f) * 57.29577951308232);
        while (this.rotationPitch - this.prevRotationPitch < -180.0f) {
            this.prevRotationPitch -= 360.0f;
        }
        while (this.rotationPitch - this.prevRotationPitch >= 180.0f) {
            this.prevRotationPitch += 360.0f;
        }
        while (this.rotationYaw - this.prevRotationYaw < -180.0f) {
            this.prevRotationYaw -= 360.0f;
        }
        while (this.rotationYaw - this.prevRotationYaw >= 180.0f) {
            this.prevRotationYaw += 360.0f;
        }
        this.rotationPitch = this.prevRotationPitch + (this.rotationPitch - this.prevRotationPitch) * 0.2f;
        this.rotationYaw = this.prevRotationYaw + (this.rotationYaw - this.prevRotationYaw) * 0.2f;
        if (!this.world.isRemote) {
            double d0 = this.targetX - this.posX;
            double d1 = this.targetZ - this.posZ;
            float f1 = (float)Math.sqrt(d0 * d0 + d1 * d1);
            float f2 = (float)MathHelper.atan2(d1, d0);
            double d2 = (double)f + (double)(f1 - f) * 0.0025;
            if (f1 < 1.0f) {
                d2 *= 0.8;
                this.motionY *= 0.8;
            }
            this.motionX = Math.cos(f2) * d2;
            this.motionZ = Math.sin(f2) * d2;
            this.motionY = this.posY < this.targetY ? (this.motionY = this.motionY + (1.0 - this.motionY) * (double)0.015f) : (this.motionY = this.motionY + (-1.0 - this.motionY) * (double)0.015f);
        }
        float f3 = 0.25f;
        if (this.isInWater()) {
            for (int i = 0; i < 4; ++i) {
                this.world.spawnParticle(EnumParticleTypes.WATER_BUBBLE, this.posX - this.motionX * 0.25, this.posY - this.motionY * 0.25, this.posZ - this.motionZ * 0.25, this.motionX, this.motionY, this.motionZ, new int[0]);
            }
        } else {
            this.world.spawnParticle(EnumParticleTypes.FIREWORKS_SPARK, this.posX - this.motionX * 0.25 + this.rand.nextDouble() * 0.6 - 0.3, this.posY - this.motionY * 0.25 - 0.5, this.posZ - this.motionZ * 0.25 + this.rand.nextDouble() * 0.6 - 0.3, this.motionX, this.motionY, this.motionZ, new int[0]);
        }
        if (!this.world.isRemote) {
            this.setPosition(this.posX, this.posY, this.posZ);
            ++this.despawnTimer;
            if (this.despawnTimer > 80 && !this.world.isRemote) {
                this.playSound(SoundEvents.BLOCK_ANVIL_HIT, 1.0f, 1.0f);
                this.setDead();
            }
        }
    }

    @Override
    protected void onImpact(RayTraceResult result) {
        UUID uuid;
        if (result.hitInfo != null) {
            this.setDead();
        }
        if (result.entityHit instanceof EntityPlayerSP && NobleBattle.nobleBattleMap.containsKey(uuid = result.entityHit.getUniqueID())) {
            NobleBattle battle = NobleBattle.nobleBattleMap.get(uuid);
            EntityPixelmon pokemon = battle.getPokemon().getEntityPixelmon();
            EntityPlayerMP player = battle.getPlayer().getEntityPlayer();
            player.attackEntityAsMob(pokemon);
            player.attackEntityFrom(DamageSource.causeThrownDamage(pokemon, pokemon), 10.0f);
        }
    }
}

