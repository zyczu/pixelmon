/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonArray
 *  com.google.gson.JsonObject
 *  com.google.gson.JsonParser
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pixelmongenerations.common.entity.npcs.registry.BaseShopItem;
import com.pixelmongenerations.common.entity.npcs.registry.LanguageNotFoundException;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryData;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.entity.npcs.registry.ShopItem;
import com.pixelmongenerations.common.entity.npcs.registry.ShopkeeperChat;
import com.pixelmongenerations.common.entity.npcs.registry.ShopkeeperData;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumShopKeeperType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;

public class NPCRegistryShopkeepers {
    public static HashMap<String, BaseShopItem> shopItems = new HashMap();

    void loadShopkeeper(NPCRegistryData thisData, String name, String langCode) throws Exception {
        try {
            JsonObject jsonelement1;
            JsonArray jsonarray;
            String path = Pixelmon.modDirectory + "/pixelmon/npcs/shopKeepers/";
            File skDir = new File(path);
            InputStream istream = null;
            if (!PixelmonConfig.useExternalJSONFilesNPCs) {
                istream = ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/npcs/shopKeepers/" + name + "_" + langCode.toLowerCase() + ".json");
            } else {
                File file = new File(skDir, name + "_" + langCode.toLowerCase() + ".json");
                if (file.exists()) {
                    istream = new FileInputStream(file);
                }
            }
            if (istream == null) {
                if (langCode.equals("en_us")) {
                    throw new Exception("Error in shopkeeper " + name + "_" + langCode.toLowerCase() + ".");
                }
                return;
            }
            ShopkeeperData data = new ShopkeeperData(name);
            JsonObject json = new JsonParser().parse((Reader)new InputStreamReader(istream, StandardCharsets.UTF_8)).getAsJsonObject();
            if (json.has("data")) {
                JsonObject object = JsonUtils.getJsonObject(json.get("data"), "data");
                object.get("type").getAsString();
                data.type = EnumShopKeeperType.getFromString(JsonUtils.getString(object, "type"));
                if (object.has("biomes")) {
                    JsonArray biomearray = JsonUtils.getJsonArray(object, "biomes");
                    for (int i = 0; i < biomearray.size(); ++i) {
                        data.addBiome(biomearray.get(i).getAsString());
                    }
                }
            }
            if (json.has("textures")) {
                jsonarray = JsonUtils.getJsonArray(json, "textures");
                for (int i = 0; i < jsonarray.size(); ++i) {
                    jsonelement1 = jsonarray.get(i).getAsJsonObject();
                    String skin = jsonelement1.get("name").getAsString();
                    data.addTexture(skin);
                }
            }
            if (json.has("names")) {
                jsonarray = JsonUtils.getJsonArray(json, "names");
                for (int i = 0; i < jsonarray.size(); ++i) {
                    jsonelement1 = jsonarray.get(i).getAsJsonObject();
                    String npcname = jsonelement1.get("name").getAsString();
                    data.addName(npcname);
                }
            }
            if (json.has("chat")) {
                jsonarray = JsonUtils.getJsonArray(json, "chat");
                for (int i = 0; i < jsonarray.size(); ++i) {
                    jsonelement1 = jsonarray.get(i).getAsJsonObject();
                    String hello = jsonelement1.get("hello").getAsString();
                    String goodbye = jsonelement1.get("goodbye").getAsString();
                    data.addChat(hello, goodbye);
                }
            }
            if (json.has("items")) {
                jsonarray = JsonUtils.getJsonArray(json, "items");
                for (int i = 0; i < jsonarray.size(); ++i) {
                    jsonelement1 = jsonarray.get(i).getAsJsonObject();
                    String itemName = jsonelement1.get("name").getAsString();
                    float multi = 1.0f;
                    if (jsonelement1.has("multi")) {
                        multi = jsonelement1.get("multi").getAsFloat();
                    }
                    float rarity = 1.0f;
                    if (jsonelement1.has("rarity")) {
                        rarity = jsonelement1.get("rarity").getAsFloat();
                    }
                    boolean canPriceVary = true;
                    if (jsonelement1.has("variation")) {
                        canPriceVary = jsonelement1.get("variation").getAsBoolean();
                    }
                    if (!shopItems.containsKey(itemName)) {
                        Pixelmon.LOGGER.info("Item mismatch: no item found for " + itemName + " in " + name + "_" + langCode.toLowerCase() + ".json");
                        continue;
                    }
                    ShopItem item = new ShopItem(shopItems.get(itemName), multi, rarity, canPriceVary);
                    data.addItem(item);
                }
            }
            thisData.shopkeepers.add(data);
            if (data.type == EnumShopKeeperType.Spawn) {
                thisData.shopkeeperSpawns.add(data);
            }
        }
        catch (Exception e) {
            throw new Exception("Error in shopKeeper " + name + "_" + langCode.toLowerCase(), e);
        }
    }

    public void registerShopItems() throws Exception {
        InputStream istream;
        Pixelmon.LOGGER.info("Registering shop items.");
        String path = Pixelmon.modDirectory + "/pixelmon/npcs/";
        File npcsDir = new File(path);
        if (PixelmonConfig.useExternalJSONFilesNPCs && !npcsDir.isDirectory()) {
            File baseDir = new File(Pixelmon.modDirectory + "/pixelmon");
            if (!baseDir.isDirectory()) {
                baseDir.mkdir();
            }
            Pixelmon.LOGGER.info("Creating NPCs directory.");
            npcsDir.mkdir();
            ServerNPCRegistry.extractNpcsDir(npcsDir);
        }
        InputStream inputStream = istream = !PixelmonConfig.useExternalJSONFilesNPCs ? ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/npcs/shopItems.json") : new FileInputStream(new File(npcsDir, "shopItems.json"));
        assert (istream != null);
        JsonObject json = new JsonParser().parse((Reader)new InputStreamReader(istream, StandardCharsets.UTF_8)).getAsJsonObject();
        if (json.has("items")) {
            JsonArray jsonarray = JsonUtils.getJsonArray(json, "items");
            for (int i = 0; i < jsonarray.size(); ++i) {
                ResourceLocation loc;
                Item item;
                JsonObject itemElement = jsonarray.get(i).getAsJsonObject();
                String name = itemElement.get("name").getAsString();
                String id = itemElement.has("id") ? itemElement.get("id").getAsString() : name;
                int buy = -1;
                if (itemElement.has("buy")) {
                    buy = itemElement.get("buy").getAsInt();
                }
                int sell = -1;
                if (itemElement.has("sell")) {
                    sell = itemElement.get("sell").getAsInt();
                }
                int itemData = 0;
                if (itemElement.has("itemData")) {
                    itemData = itemElement.get("itemData").getAsInt();
                }
                String nbtData = null;
                if (itemElement.has("nbtData")) {
                    nbtData = itemElement.get("nbtData").getAsString();
                }
                if ((item = Item.REGISTRY.getObject(loc = new ResourceLocation(name))) == null) {
                    Pixelmon.LOGGER.info("Item not found: " + name + " in shopItems.json");
                    continue;
                }
                ItemStack itemStack = new ItemStack(item, 1, itemData);
                if (nbtData != null) {
                    itemStack.setTagCompound(JsonToNBT.getTagFromJson(nbtData));
                }
                BaseShopItem baseItem = new BaseShopItem(id, itemStack, buy, sell);
                shopItems.put(id, baseItem);
            }
        }
    }

    public ShopkeeperData getRandom() {
        ShopkeeperData sk = null;
        NPCRegistryData data = ServerNPCRegistry.data.get(ServerNPCRegistry.en_us);
        if (data == null) {
            return null;
        }
        while (sk == null) {
            sk = RandomHelper.getRandomElementFromList(data.shopkeepers);
        }
        return sk;
    }

    public ShopkeeperData getById(String id) {
        for (ShopkeeperData sk : ServerNPCRegistry.getEnglishShopkeepers()) {
            if (!sk.id.equalsIgnoreCase(id)) continue;
            return sk;
        }
        return null;
    }

    public ShopkeeperData getRandom(EnumShopKeeperType type) {
        ShopkeeperData sk = null;
        NPCRegistryData npcData = ServerNPCRegistry.data.get(ServerNPCRegistry.en_us);
        if (npcData == null) {
            return null;
        }
        boolean hasType = false;
        for (ShopkeeperData shopkeeperData : npcData.shopkeepers) {
            if (shopkeeperData == null || shopkeeperData.type != type) continue;
            hasType = true;
            break;
        }
        if (!hasType) {
            return null;
        }
        while (sk == null || sk.type != type) {
            sk = RandomHelper.getRandomElementFromList(npcData.shopkeepers);
        }
        return sk;
    }

    public ShopkeeperData getTranslatedData(String langCode, String id) {
        NPCRegistryData npcData;
        if (!ServerNPCRegistry.data.containsKey(langCode.toLowerCase())) {
            try {
                ServerNPCRegistry.registerNPCS(langCode.toLowerCase());
            }
            catch (LanguageNotFoundException e2) {
                ServerNPCRegistry.data.put(langCode.toLowerCase(), ServerNPCRegistry.data.get(ServerNPCRegistry.en_us));
            }
            catch (Exception exception) {
                // empty catch block
            }
        }
        if ((npcData = ServerNPCRegistry.data.get(langCode)) != null) {
            for (ShopkeeperData npc : npcData.shopkeepers) {
                if (!npc.id.equals(id)) continue;
                return npc;
            }
        }
        for (ShopkeeperData npc : ServerNPCRegistry.getEnglishShopkeepers()) {
            if (!npc.id.equals(id)) continue;
            return npc;
        }
        return null;
    }

    public ShopkeeperChat getTranslatedChat(String langCode, String npcIndex, int index) {
        ArrayList<ShopkeeperChat> chat = this.getTranslatedData((String)langCode.toLowerCase(), (String)npcIndex).chat;
        if (index >= chat.size()) {
            index = 0;
        }
        return chat.get(index);
    }

    public String getJsonName(String npcIndex) {
        for (ShopkeeperData npc : ServerNPCRegistry.getEnglishShopkeepers()) {
            if (!npc.id.equals(npcIndex)) continue;
            return npc.id;
        }
        return "";
    }

    public ShopkeeperData getNext(String npcIndex) {
        ArrayList<ShopkeeperData> shopkeepers = ServerNPCRegistry.getEnglishShopkeepers();
        int numShopkeepers = shopkeepers.size();
        for (int i = 0; i < numShopkeepers; ++i) {
            ShopkeeperData npc = shopkeepers.get(i);
            if (!npc.id.equals(npcIndex)) continue;
            if (i < numShopkeepers - 1) {
                return shopkeepers.get(i + 1);
            }
            return shopkeepers.get(0);
        }
        return shopkeepers.get(0);
    }

    public ShopItem getItem(String npcIndex, String itemID) {
        ShopkeeperData shopkeeper = this.getById(npcIndex);
        return shopkeeper == null ? null : shopkeeper.getItem(itemID);
    }

    public BaseShopItem getItem(String name) {
        if (name == null) {
            return null;
        }
        if (name.startsWith("item.")) {
            name = name.replace("item.", "");
        }
        if (shopItems.get(name) == null) {
            Pixelmon.LOGGER.info("Missing item requested: " + name);
        }
        return shopItems.get(name);
    }

    public BaseShopItem getItem(ItemStack itemStack) {
        for (BaseShopItem shopItem : shopItems.values()) {
            if (!ItemStack.areItemsEqual(shopItem.itemStack, itemStack) || !ItemStack.areItemStackTagsEqual(shopItem.itemStack, itemStack)) continue;
            return shopItem;
        }
        return null;
    }

    public String getTranslatedName(String langCode, String npcIndex, int nameIndex) {
        ArrayList<String> names = this.getTranslatedData((String)langCode.toLowerCase(), (String)npcIndex).names;
        if (nameIndex >= names.size()) {
            nameIndex = 0;
        }
        return names.get(nameIndex);
    }

    public boolean hasRoaming() {
        NPCRegistryData data = ServerNPCRegistry.data.get(ServerNPCRegistry.en_us);
        return data != null && !data.shopkeeperSpawns.isEmpty();
    }

    public ShopkeeperData getRandomSpawning(String biomeID) {
        ArrayList<ShopkeeperData> keepers = new ArrayList<ShopkeeperData>();
        for (ShopkeeperData data : ServerNPCRegistry.data.get((Object)ServerNPCRegistry.en_us).shopkeeperSpawns) {
            if (!data.biomes.contains(biomeID)) continue;
            keepers.add(data);
        }
        ShopkeeperData sk = null;
        if (!keepers.isEmpty()) {
            while (sk == null) {
                sk = (ShopkeeperData)RandomHelper.getRandomElementFromList(keepers);
            }
        }
        return sk;
    }

    public String[] getRoamingBiomes() {
        ArrayList<String> biomeNames = new ArrayList<String>();
        for (ShopkeeperData data : ServerNPCRegistry.data.get((Object)ServerNPCRegistry.en_us).shopkeeperSpawns) {
            for (String biome : data.biomes) {
                if (biomeNames.contains(biome)) continue;
                biomeNames.add(biome);
            }
        }
        return biomeNames.toArray(new String[biomeNames.size()]);
    }
}

