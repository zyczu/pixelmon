/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.pixelmongenerations.core.util.IEncodeable;
import com.pixelmongenerations.core.util.helper.ArrayHelper;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class ClientShopkeeperData
implements IEncodeable {
    private String id;
    private List<String> textures;
    private List<String> names;

    public ClientShopkeeperData(String id, List<String> textures, List<String> names) {
        this.id = id;
        this.textures = textures;
        this.names = names;
    }

    public ClientShopkeeperData(String id) {
        this(id, new ArrayList<String>(), new ArrayList<String>());
    }

    public ClientShopkeeperData(ByteBuf buffer) {
        this.decodeInto(buffer);
    }

    public String getID() {
        return this.id;
    }

    public List<String> getTextures() {
        return this.textures;
    }

    public List<String> getNames() {
        return this.names;
    }

    @Override
    public void encodeInto(ByteBuf buffer) {
        ByteBufUtils.writeUTF8String(buffer, this.id);
        ArrayHelper.encodeStringList(buffer, this.textures);
        ArrayHelper.encodeStringList(buffer, this.names);
    }

    @Override
    public void decodeInto(ByteBuf buffer) {
        this.id = ByteBufUtils.readUTF8String(buffer);
        this.textures = ArrayHelper.decodeStringList(buffer);
        this.names = ArrayHelper.decodeStringList(buffer);
    }

    public int hashCode() {
        return this.id.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        ClientShopkeeperData other = (ClientShopkeeperData)obj;
        return Objects.equals(this.id, other.id);
    }
}

