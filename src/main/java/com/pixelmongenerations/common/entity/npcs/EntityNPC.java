/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.vecmath.Vector3f
 */
package com.pixelmongenerations.common.entity.npcs;

import com.pixelmongenerations.common.entity.ai.AIHarvestFarmLand;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.BaseTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryTrainers;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumTrainerAI;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.proxy.ClientProxy;
import java.util.ArrayList;
import java.util.Optional;
import javax.vecmath.Vector3f;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.resources.DefaultPlayerSkin;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.ai.EntityAIMoveIndoors;
import net.minecraft.entity.ai.EntityAIMoveTowardsRestriction;
import net.minecraft.entity.ai.EntityAIOpenDoor;
import net.minecraft.entity.ai.EntityAIRestrictOpenDoor;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.ai.EntityAIWatchClosest2;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.village.Village;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class EntityNPC
extends EntityCreature {
    public static final DataParameter<String> dwName = EntityDataManager.createKey(EntityNPC.class, DataSerializers.STRING);
    public static final DataParameter<String> dwNickname = EntityDataManager.createKey(EntityNPC.class, DataSerializers.STRING);
    public static final DataParameter<Integer> dwID = EntityDataManager.createKey(EntityNPC.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwModel = EntityDataManager.createKey(EntityNPC.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> dwTextureIndex = EntityDataManager.createKey(EntityNPC.class, DataSerializers.VARINT);
    public static final DataParameter<String> dwCustomSteveTexture = EntityDataManager.createKey(EntityNPC.class, DataSerializers.STRING);
    public static final DataParameter<Integer> dwProfession = EntityDataManager.createKey(EntityNPC.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> dwNPCAI = EntityDataManager.createKey(EntityNPC.class, DataSerializers.VARINT);
    public static final DataParameter<Boolean> dwAlexSkin = EntityDataManager.createKey(EntityNPC.class, DataSerializers.BOOLEAN);
    public static int idIndex = 0;
    Village villageObj;
    public SpawnLocation npcLocation;
    public int despawnCounter = -1;
    public static int despawnRadius = 20;
    public static int TICKSPERSECOND = 20;
    public static int intMinTicksToDespawn = 30 * TICKSPERSECOND;
    public static int intMaxTicksToDespawn = 180 * TICKSPERSECOND;
    private InventoryBasic npcInventory = new InventoryBasic("Items", false, 8);
    protected int chatIndex;
    public boolean ignoreDespawnCounter = false;
    public ArrayList<String> interactCommands = new ArrayList();
    private int randomTickDivider;
    private boolean setup = false;
    @SideOnly(value=Side.CLIENT)
    protected ModelBase model;
    @SideOnly(value=Side.CLIENT)
    protected ModelBase alexModel;

    public EntityNPC(World world) {
        super(world);
        this.dataManager.register(dwName, "");
        this.dataManager.register(dwNickname, "");
        this.dataManager.register(dwID, -1);
        this.dataManager.register(dwModel, 0);
        this.dataManager.register(dwTextureIndex, 0);
        this.dataManager.register(dwCustomSteveTexture, "");
        this.dataManager.register(dwProfession, -1);
        this.dataManager.register(dwNPCAI, EnumTrainerAI.Wander.ordinal());
        this.dataManager.register(dwAlexSkin, false);
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(100.0);
        this.initAI();
    }

    public void init(String name) {
        this.setName(name);
        this.setHealth(100.0f);
        if (this.getId() == -1) {
            this.setId(idIndex++);
        }
        this.setup = true;
    }

    @SideOnly(value=Side.CLIENT)
    public String getTexture() {
        String pathName;
        BaseTrainer t = this.getBaseTrainer();
        String string = pathName = t == NPCRegistryTrainers.Alex ? "alex" : "steve";
        if (!t.textures.isEmpty() && this.getTextureIndex() > -1) {
            if (this.getTextureIndex() >= t.textures.size()) {
                this.setTextureIndex(t.textures.size() - 1);
            }
            if (t.textures.get(this.getTextureIndex()).equals("Custom_RP")) {
                return "pixelmon:textures/" + pathName + "/" + this.getCustomSteveTexture() + ".png";
            }
            return "pixelmon:textures/" + pathName + "/" + t.textures.get(this.getTextureIndex()) + ".png";
        }
        if (this.getTextureIndex() == -1) {
            return "pixelmon:textures/" + pathName + "/" + this.getCustomSteveTexture();
        }
        if (t != NPCRegistryTrainers.Steve) {
            return "pixelmon:textures/" + pathName + "/" + t.name.toLowerCase() + ".png";
        }
        return "pixelmon:textures/" + pathName + "/" + this.getCustomSteveTexture().toLowerCase() + ".png";
    }

    public String getCustomSteveTexture() {
        return this.dataManager.get(dwCustomSteveTexture);
    }

    public void setCustomSteveTexture(String tex) {
        this.dataManager.set(dwCustomSteveTexture, tex);
    }

    @SideOnly(value=Side.CLIENT)
    public ModelBase getModel() {
        if (this.model == null) {
            this.model = new ModelPlayer(0.0f, false);
        }
        if (this.alexModel == null) {
            this.alexModel = new ModelPlayer(0.0f, true);
        }
        return this.dataManager.get(dwAlexSkin) != false ? this.alexModel : this.model;
    }

    @Override
    protected boolean canDespawn() {
        return this.isSetup() && !this.ignoreDespawnCounter;
    }

    @Override
    public String getName() {
        return this.dataManager.get(dwName);
    }

    public void setName(String name) {
        this.dataManager.set(dwName, name);
    }

    @Override
    public boolean attackEntityFrom(DamageSource par1DamageSource, float par2) {
        return false;
    }

    @Override
    public boolean canBeLeashedTo(EntityPlayer player) {
        return false;
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        nbt.setString("Name", this.getName());
        if (this.npcLocation == null || this.npcLocation == SpawnLocation.Land) {
            this.npcLocation = SpawnLocation.LandVillager;
        }
        nbt.setInteger("trainerLocation", this.npcLocation.ordinal());
        nbt.setString("BaseTrainer", this.getBaseTrainer().name);
        if (this.getBaseTrainer() == NPCRegistryTrainers.Steve || this.getBaseTrainer().textures.size() > 1) {
            nbt.setInteger("TextureIndex", this.getTextureIndex());
        }
        nbt.setString("CustomSteveTexture", this.getCustomSteveTexture());
        nbt.setShort("Profession", (short)this.getProfession());
        NBTTagList nbttaglist = new NBTTagList();
        for (int i = 0; i < this.npcInventory.getSizeInventory(); ++i) {
            ItemStack itemstack = this.npcInventory.getStackInSlot(i);
            if (itemstack.isEmpty()) continue;
            nbttaglist.appendTag(itemstack.writeToNBT(new NBTTagCompound()));
        }
        nbt.setTag("Inventory", nbttaglist);
        nbt.setBoolean("IsPersistent", this.ignoreDespawnCounter);
        nbt.setShort("AIMode", (short)this.getAIMode().ordinal());
        NBTTagCompound commandsNbt = new NBTTagCompound();
        NBTTagList interactList = new NBTTagList();
        for (String interactCommand : this.interactCommands) {
            interactList.appendTag(new NBTTagString(interactCommand));
        }
        commandsNbt.setTag("interactCommands", interactList);
        nbt.setTag("Commands", commandsNbt);
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        NBTTagCompound cmdNbt;
        super.readEntityFromNBT(nbt);
        this.setName(nbt.getString("Name"));
        if (nbt.hasKey("trainerLocation")) {
            this.npcLocation = SpawnLocation.getFromIndex(nbt.getInteger("trainerLocation"));
            if (this.npcLocation == null) {
                this.npcLocation = SpawnLocation.LandVillager;
            }
        } else {
            this.npcLocation = SpawnLocation.Land;
        }
        this.init(this.getName());
        BaseTrainer trainer = null;
        if (nbt.hasKey("ModelIndex")) {
            trainer = ServerNPCRegistry.trainers.getById(nbt.getInteger("ModelIndex"));
        } else if (nbt.hasKey("BaseTrainer")) {
            trainer = NPCRegistryTrainers.getByName(nbt.getString("BaseTrainer"));
        }
        if (trainer != null) {
            this.setBaseTrainer(trainer);
            if (this.getBaseTrainer().textures.size() > 1) {
                this.setTextureIndex(nbt.getInteger("TextureIndex"));
            }
        }
        this.setCustomSteveTexture(nbt.getString("CustomSteveTexture"));
        this.setProfession(nbt.getShort("Profession"));
        NBTTagList nbttaglist = nbt.getTagList("Inventory", 10);
        for (int i = 0; i < nbttaglist.tagCount(); ++i) {
            ItemStack itemstack = new ItemStack(nbttaglist.getCompoundTagAt(i));
            if (itemstack.isEmpty()) continue;
            this.npcInventory.addItem(itemstack);
        }
        if (this.getProfession() == 0) {
            this.initVilagerAI();
            this.tasks.addTask(6, new AIHarvestFarmLand(this, 0.6));
        }
        if (nbt.hasKey("IsPersistent")) {
            this.ignoreDespawnCounter = nbt.getBoolean("IsPersistent");
        }
        if (nbt.hasKey("AIMode")) {
            this.setAIMode(EnumTrainerAI.getFromOrdinal(nbt.getShort("AIMode")));
            this.initAI();
        }
        if (nbt.hasKey("Commands") && (cmdNbt = nbt.getCompoundTag("Commands")).hasKey("interactCommands")) {
            NBTTagList wins = cmdNbt.getTagList("interactCommands", 8);
            for (int i = 0; i < wins.tagCount(); ++i) {
                this.interactCommands.add(wins.getStringTagAt(i));
            }
        }
    }

    public void initAI() {
    }

    @Override
    public boolean getCanSpawnHere() {
        int var3;
        int var2;
        int var1 = MathHelper.floor(this.posX);
        Block block = this.world.getBlockState(new BlockPos(var1, (var2 = MathHelper.floor(this.getEntityBoundingBox().minY)) - 1, var3 = MathHelper.floor(this.posZ))).getBlock();
        return block == Blocks.GRASS || block == Blocks.SAND;
    }

    @Override
    protected boolean processInteract(EntityPlayer player, EnumHand hand) {
        return this.interactWithNPC(player, hand);
    }

    public boolean interactWithNPC(EntityPlayer player, EnumHand hand) {
        return false;
    }

    public void setId(int i) {
        this.dataManager.set(dwID, i);
    }

    @Override
    public int getMaxSpawnedInChunk() {
        return 1;
    }

    public int getId() {
        return this.dataManager.get(dwID);
    }

    public abstract String getDisplayText();

    public String getSubTitleText() {
        return null;
    }

    public EnumBossMode getBossMode() {
        return EnumBossMode.NotBoss;
    }

    public BaseTrainer getBaseTrainer() {
        int trainerId = this.dataManager.get(dwModel);
        return ServerNPCRegistry.trainers.getById(trainerId);
    }

    public void setBaseTrainer(BaseTrainer trainer) {
        this.dataManager.set(dwAlexSkin, trainer == NPCRegistryTrainers.Alex);
        this.dataManager.set(dwModel, trainer.id);
    }

    public boolean bindTexture() {
        if (this.getTextureIndex() == -1) {
            return false;
        }
        BaseTrainer t = this.getBaseTrainer();
        if (t == NPCRegistryTrainers.Steve || t == NPCRegistryTrainers.Alex) {
            String textureName = t.textures.get(this.getTextureIndex());
            if (textureName.equals("Custom_PN")) {
                Minecraft.getMinecraft().renderEngine.bindTexture(ClientProxy.bindPlayerTexture(this.getCustomSteveTexture()));
                return true;
            }
            if (textureName.equals("Custom_RP") && this.getCustomSteveTexture().equals("")) {
                Minecraft.getMinecraft().renderEngine.bindTexture(DefaultPlayerSkin.getDefaultSkin(this.getUniqueID()));
                return true;
            }
            if (textureName.equals("Steve")) {
                Minecraft.getMinecraft().renderEngine.bindTexture(DefaultPlayerSkin.getDefaultSkin(this.getUniqueID()));
                return true;
            }
        }
        return false;
    }

    public Vector3f getScale() {
        BaseTrainer base = this.getBaseTrainer();
        if (base.name.equals("Youngster") || base.name.equals("Lass") || base.name.equals("PreschoolerGirl")) {
            return new Vector3f(0.85f, 0.8f, 0.85f);
        }
        return new Vector3f(1.0f, 1.0f, 1.0f);
    }

    public static <T extends EntityNPC> Optional<T> locateNPCClient(World world, int id, Class<T> type) {
        try {
            for (int i = 0; i < world.loadedEntityList.size(); ++i) {
                Entity entity = world.loadedEntityList.get(i);
                if (!type.isInstance(entity) || ((EntityNPC)entity).getId() != id) continue;
                return Optional.of((T)entity);
            }
            return Optional.empty();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException("How did this happen..");
        }
    }

    public static <T extends EntityNPC> Optional<T> locateNPCServer(World world, int id, Class<T> type) {
        try {
            return (Optional)world.getMinecraftServer().callFromMainThread(() -> {
                for (int i = 0; i < world.loadedEntityList.size(); ++i) {
                    Entity entity = world.loadedEntityList.get(i);
                    if (!type.isInstance(entity) || ((EntityNPC)entity).getId() != id) continue;
                    return Optional.of((EntityNPC)entity);
                }
                return Optional.empty();
            }).get();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException("How did this happen..");
        }
    }

    public static <T extends EntityNPC> Optional<T> locateNPCServer(World world, String name, Class<T> type, String langCode) {
        try {
            return (Optional)world.getMinecraftServer().callFromMainThread(() -> {
                for (int i = 0; i < world.loadedEntityList.size(); ++i) {
                    String npcName;
                    Entity entity = world.loadedEntityList.get(i);
                    if (!type.isInstance(entity) || !name.equals(npcName = type == NPCTrainer.class ? ((NPCTrainer)entity).getName(langCode) : ((EntityNPC)entity).getName())) continue;
                    return Optional.of((EntityNPC)entity);
                }
                return Optional.empty();
            }).get();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public void unloadEntity() {
        this.despawnEntity();
        this.setDead();
    }

    public int getTextureIndex() {
        return this.dataManager.get(dwTextureIndex);
    }

    public void setTextureIndex(int index) {
        this.dataManager.set(dwTextureIndex, index);
    }

    public boolean checkOP(EntityPlayer player) {
        if (this.world.getMinecraftServer() != null && this.world.getMinecraftServer().isDedicatedServer()) {
            if (this.world.getMinecraftServer().getPlayerList().canSendCommands(player.getGameProfile())) {
                if (!ItemNPCEditor.checkPermission((EntityPlayerMP)player)) {
                    ChatHandler.sendChat(player, "trainer.edit.opwarning", new Object[0]);
                    return false;
                }
            } else {
                ChatHandler.sendChat(player, "trainer.edit.opwarning", new Object[0]);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onUpdate() {
        if (this.canDespawn() && !this.world.isRemote) {
            this.checkForRarityDespawn();
        }
        try {
            super.onUpdate();
        }
        catch (ClassCastException classCastException) {
            // empty catch block
        }
    }

    protected void checkForRarityDespawn() {
        if (this.despawnCounter > 0) {
            --this.despawnCounter;
        } else if (this.despawnCounter == 0) {
            if (!this.playersNearby()) {
                this.setDead();
            }
        } else {
            this.despawnCounter = (int)(Math.random() * (double)(intMaxTicksToDespawn - intMinTicksToDespawn) + (double)intMinTicksToDespawn);
        }
    }

    protected boolean playersNearby() {
        boolean playerNearby = false;
        for (int i = 0; i < this.world.playerEntities.size(); ++i) {
            EntityPlayer player = this.world.playerEntities.get(i);
            double distancex = player.posX - this.posX;
            double distancey = player.posY - this.posY;
            double distancez = player.posZ - this.posZ;
            double distancesquared = distancex * distancex + distancey * distancey + distancez * distancez;
            if (distancesquared >= (double)(despawnRadius * despawnRadius)) continue;
            playerNearby = true;
            break;
        }
        return playerNearby;
    }

    public void setProfession(int professionId) {
        this.dataManager.set(dwProfession, professionId);
    }

    public int getProfession() {
        return this.dataManager.get(dwProfession);
    }

    public void initDefaultAI() {
        ((PathNavigateGround)this.getNavigator()).setCanSwim(false);
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.tasks.addTask(1, new EntityAIWatchClosest2(this, EntityPlayer.class, 3.0f, 1.0f));
        this.tasks.addTask(2, new EntityAIWatchClosest(this, EntityLiving.class, 8.0f));
    }

    public void initWanderingAI() {
        ((PathNavigateGround)this.getNavigator()).setCanSwim(false);
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.tasks.addTask(1, new EntityAIWatchClosest2(this, EntityPlayer.class, 3.0f, 1.0f));
        this.tasks.addTask(2, new EntityAIWatchClosest(this, EntityLiving.class, 8.0f));
        this.tasks.addTask(7, new EntityAIWander(this, 0.6));
    }

    public void initVilagerAI() {
        ((PathNavigateGround)this.getNavigator()).setBreakDoors(true);
        ((PathNavigateGround)this.getNavigator()).setCanSwim(false);
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.tasks.addTask(1, new EntityAIAvoidEntity<EntityZombie>(this, EntityZombie.class, 8.0f, 0.8f, 0.6f));
        this.tasks.addTask(2, new EntityAIMoveIndoors(this));
        this.tasks.addTask(3, new EntityAIRestrictOpenDoor(this));
        this.tasks.addTask(4, new EntityAIOpenDoor(this, true));
        this.tasks.addTask(5, new EntityAIMoveTowardsRestriction(this, 0.6));
        this.tasks.addTask(7, new EntityAIWatchClosest2(this, EntityPlayer.class, 3.0f, 1.0f));
        this.tasks.addTask(7, new EntityAIWander(this, 0.6));
        this.tasks.addTask(8, new EntityAIWatchClosest(this, EntityLiving.class, 8.0f));
        this.setCanPickUpLoot(true);
    }

    public boolean isFarmItemInInventory() {
        for (int i = 0; i < this.npcInventory.getSizeInventory(); ++i) {
            ItemStack itemstack = this.npcInventory.getStackInSlot(i);
            if (itemstack == null || itemstack.getItem() != Items.WHEAT_SEEDS && itemstack.getItem() != Items.POTATO && itemstack.getItem() != Items.CARROT) continue;
            return true;
        }
        return false;
    }

    @Override
    public boolean replaceItemInInventory(int inventorySlot, ItemStack itemStackIn) {
        if (super.replaceItemInInventory(inventorySlot, itemStackIn)) {
            return true;
        }
        int j = inventorySlot - 300;
        if (j >= 0 && j < this.npcInventory.getSizeInventory()) {
            this.npcInventory.setInventorySlotContents(j, itemStackIn);
            return true;
        }
        return false;
    }

    public boolean hasItemToPlant() {
        boolean flag = this.getProfession() == 0;
        boolean bl = flag;
        return flag ? !this.hasEnoughItems(5) : !this.hasEnoughItems(1);
    }

    private boolean hasEnoughItems(int multiplier) {
        boolean flag = this.getProfession() == 0;
        for (int j = 0; j < this.npcInventory.getSizeInventory(); ++j) {
            ItemStack itemstack = this.npcInventory.getStackInSlot(j);
            if (itemstack == null) continue;
            if (itemstack.getItem() == Items.BREAD && itemstack.getCount() >= 3 * multiplier || itemstack.getItem() == Items.POTATO && itemstack.getCount() >= 12 * multiplier || itemstack.getItem() == Items.CARROT && itemstack.getCount() >= 12 * multiplier) {
                return true;
            }
            if (!flag || itemstack.getItem() != Items.WHEAT || itemstack.getCount() < 9 * multiplier) continue;
            return true;
        }
        return false;
    }

    public InventoryBasic getNPCInventory() {
        return this.npcInventory;
    }

    @Override
    protected void updateEquipmentIfNeeded(EntityItem itemEntity) {
        ItemStack itemstack = itemEntity.getItem();
        Item item = itemstack.getItem();
        if (this.canVillagerPickupItem(item)) {
            ItemStack itemstack1 = this.npcInventory.addItem(itemstack);
            if (itemstack1 == null) {
                itemEntity.setDead();
            } else {
                itemstack.setCount(itemstack1.getCount());
            }
        }
    }

    private boolean canVillagerPickupItem(Item itemIn) {
        return itemIn == Items.BREAD || itemIn == Items.POTATO || itemIn == Items.CARROT || itemIn == Items.WHEAT || itemIn == Items.WHEAT_SEEDS;
    }

    @Override
    protected void updateAITasks() {
        if (this.getProfession() != -1 && --this.randomTickDivider <= 0) {
            BlockPos blockpos = new BlockPos(this);
            this.world.getVillageCollection().addToVillagerPositionList(blockpos);
            this.randomTickDivider = 70 + this.rand.nextInt(50);
            this.villageObj = this.world.getVillageCollection().getNearestVillage(blockpos, 32);
            if (this.villageObj == null) {
                this.detachHome();
            } else {
                BlockPos blockpos1 = this.villageObj.getCenter();
                this.setHomePosAndDistance(blockpos1, (int)((float)this.villageObj.getVillageRadius() * 1.0f));
            }
        }
        super.updateAITasks();
    }

    public EnumTrainerAI getAIMode() {
        return EnumTrainerAI.getFromOrdinal(this.dataManager.get(dwNPCAI));
    }

    public void setAIMode(EnumTrainerAI mode) {
        this.dataManager.set(dwNPCAI, mode.ordinal());
    }

    public boolean isSetup() {
        return this.setup;
    }
}

