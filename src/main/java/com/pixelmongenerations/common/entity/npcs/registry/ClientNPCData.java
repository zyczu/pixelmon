/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.pixelmongenerations.core.util.IEncodeable;
import io.netty.buffer.ByteBuf;
import java.util.Objects;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class ClientNPCData
implements IEncodeable {
    private String id;
    private String texture;

    public ClientNPCData(String id, String texture) {
        this.id = id;
        this.texture = texture;
    }

    public ClientNPCData(String texture) {
        this("", texture);
    }

    public ClientNPCData(ByteBuf buffer) {
        this.decodeInto(buffer);
    }

    public String getID() {
        return this.id;
    }

    public String getTexture() {
        return this.texture;
    }

    @Override
    public void encodeInto(ByteBuf buffer) {
        ByteBufUtils.writeUTF8String(buffer, this.id);
        ByteBufUtils.writeUTF8String(buffer, this.texture);
    }

    @Override
    public void decodeInto(ByteBuf buffer) {
        this.id = ByteBufUtils.readUTF8String(buffer);
        this.texture = ByteBufUtils.readUTF8String(buffer);
    }

    public int hashCode() {
        return this.texture.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        ClientNPCData other = (ClientNPCData)obj;
        return Objects.equals(this.texture, other.texture);
    }
}

