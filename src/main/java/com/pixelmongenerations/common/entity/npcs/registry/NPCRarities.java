/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonArray
 *  com.google.gson.JsonObject
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumNPCType;
import net.minecraft.util.JsonUtils;

public class NPCRarities {
    private int[] typeRarities = new int[EnumNPCType.values().length];

    public void loadFromJson(JsonObject json) {
        JsonArray jsonarray = JsonUtils.getJsonArray(json, "rarities");
        for (int i = 0; i < jsonarray.size(); ++i) {
            JsonObject jsonelement1 = jsonarray.get(i).getAsJsonObject();
            String type = jsonelement1.get("type").getAsString();
            int rarity = jsonelement1.get("rarity").getAsInt();
            boolean foundType = false;
            for (EnumNPCType t : EnumNPCType.values()) {
                if (!type.equalsIgnoreCase(t.getDefaultName())) continue;
                this.typeRarities[t.ordinal()] = rarity;
                foundType = true;
                break;
            }
            if (foundType) continue;
            Pixelmon.LOGGER.error("Failed to match rarity for \"" + type + "\" to a valid NPC type.");
        }
    }

    public int getRarityForType(EnumNPCType npcType) {
        return this.typeRarities[npcType.ordinal()];
    }
}

