/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.npcs;

import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import net.minecraft.entity.IMerchant;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.village.MerchantRecipe;
import net.minecraft.village.MerchantRecipeList;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

public class NPCSticker
extends EntityNPC
implements IMerchant {
    private EntityPlayer customer;
    private MerchantRecipeList list;

    public NPCSticker(World world) {
        super(world);
    }

    @Override
    public String getDisplayText() {
        return "";
    }

    @Override
    public String getTexture() {
        return "pixelmon:textures/steve/suit.png";
    }

    @Override
    protected boolean processInteract(EntityPlayer player, EnumHand hand) {
        if (player instanceof EntityPlayerMP) {
            this.setList();
            this.setCustomer(player);
            player.displayVillagerTradeGui(this);
        }
        return true;
    }

    private void setList() {
        Item[] specialCrystals;
        Item[] basicCrystals;
        MerchantRecipe recipe;
        ItemStack totemSticker;
        Item[] typeCrystals;
        this.list = new MerchantRecipeList();
        for (Item i : typeCrystals = new Item[]{PixelmonItemsHeld.buginumZ, PixelmonItemsHeld.darikniumZ, PixelmonItemsHeld.dragoniumZ, PixelmonItemsHeld.electriumZ, PixelmonItemsHeld.fairiumZ, PixelmonItemsHeld.firiumZ, PixelmonItemsHeld.flyiniumZ, PixelmonItemsHeld.ghostiumZ, PixelmonItemsHeld.grassiumZ, PixelmonItemsHeld.groundiumZ, PixelmonItemsHeld.iciumZ, PixelmonItemsHeld.normaliumZ, PixelmonItemsHeld.poisoniumZ, PixelmonItemsHeld.psychiumZ, PixelmonItemsHeld.rockiumZ, PixelmonItemsHeld.steeliumZ, PixelmonItemsHeld.wateriumZ}) {
            ItemStack typeCrystal = new ItemStack(i);
            totemSticker = new ItemStack(PixelmonItems.totemSticker);
            typeCrystal.setCount(1);
            totemSticker.setCount(10);
            recipe = new MerchantRecipe(totemSticker, ItemStack.EMPTY, typeCrystal);
            this.list.add(recipe);
        }
        for (Item i : basicCrystals = new Item[]{PixelmonItemsHeld.aloraichiumZ, PixelmonItemsHeld.decidiumZ, PixelmonItemsHeld.eeviumZ, PixelmonItemsHeld.inciniumZ, PixelmonItemsHeld.kommoniumZ, PixelmonItemsHeld.lycaniumZ, PixelmonItemsHeld.mimikiumZ, PixelmonItemsHeld.pikaniumZ, PixelmonItemsHeld.pikashuniumZ, PixelmonItemsHeld.primaiumZ, PixelmonItemsHeld.snorliumZ}) {
            ItemStack basicCrystal = new ItemStack(i);
            totemSticker = new ItemStack(PixelmonItems.totemSticker);
            basicCrystal.setCount(1);
            totemSticker.setCount(15);
            recipe = new MerchantRecipe(totemSticker, ItemStack.EMPTY, basicCrystal);
            this.list.add(recipe);
        }
        for (Item i : specialCrystals = new Item[]{PixelmonItemsHeld.lunaliumZ, PixelmonItemsHeld.marshadiumZ, PixelmonItemsHeld.mewniumZ, PixelmonItemsHeld.solganiumZ, PixelmonItemsHeld.tapuniumZ, PixelmonItemsHeld.ultranecroziumZ}) {
            ItemStack specialCrystal = new ItemStack(i);
            totemSticker = new ItemStack(PixelmonItems.totemSticker);
            specialCrystal.setCount(1);
            totemSticker.setCount(20);
            recipe = new MerchantRecipe(totemSticker, ItemStack.EMPTY, specialCrystal);
            this.list.add(recipe);
        }
    }

    @Override
    public void setCustomer(@Nullable EntityPlayer player) {
        this.customer = player;
    }

    @Override
    @Nullable
    public EntityPlayer getCustomer() {
        return this.customer;
    }

    @Override
    @Nullable
    public MerchantRecipeList getRecipes(EntityPlayer player) {
        return this.list;
    }

    @Override
    public void setRecipes(@Nullable MerchantRecipeList recipeList) {
        this.list = recipeList;
    }

    @Override
    public void useRecipe(MerchantRecipe recipe) {
    }

    @Override
    public void verifySellingItem(ItemStack stack) {
    }

    @Override
    public World getWorld() {
        return this.world;
    }

    @Override
    public BlockPos getPos() {
        return this.getPos();
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
    public void addVelocity(double par1, double par3, double par5) {
        if (this.canBePushed()) {
            super.addVelocity(par1, par3, par5);
        }
    }
}

