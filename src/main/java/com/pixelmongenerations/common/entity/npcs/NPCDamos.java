/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.entity.npcs;

import com.google.common.collect.Lists;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumUnown;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;

public class NPCDamos extends EntityNPC {

    public static List<EnumSpecies> QUEST_POKEMON = Lists.newArrayList(EnumSpecies.Nihilego, EnumSpecies.Buzzwole, EnumSpecies.Pheromosa, EnumSpecies.Xurkitree, EnumSpecies.Celesteela, EnumSpecies.Kartana, EnumSpecies.Guzzlord, EnumSpecies.Stakataka, EnumSpecies.Blacephalon);

    public NPCDamos(World world) {
        super(world);
        this.npcLocation = SpawnLocation.LandVillager;
    }

    @Override
    public String getDisplayText() {
        return "";
    }

    @Override
    public String getTexture() {
        return "pixelmon:textures/steve/damos.png";
    }

    @Override
    public boolean interactWithNPC(EntityPlayer player, EnumHand hand) {
        Optional<PlayerStorage> oStorage;
        if (player.getHeldItem(hand).getItem() instanceof ItemNPCEditor) {
            this.setDead();
        }
        if (player instanceof EntityPlayerMP && (oStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player)).isPresent()) {
            PlayerStorage storage = oStorage.get();
            if (!storage.playerData.hasObtainedArceus()) {
                ArrayList<String> missingUnowns = new ArrayList<String>();
                for (EnumUnown unown : EnumUnown.values()) {
                    if (storage.playerData.unownCaught.contains(unown.getProperName())) continue;
                    missingUnowns.add(unown.getProperName());
                }
                boolean splash = false;
                boolean meadow = false;
                boolean earth = false;
                boolean zap = false;
                boolean draco = false;
                ArrayList<String> missingPlates = new ArrayList<String>();
                missingPlates.add("splash");
                missingPlates.add("meadow");
                missingPlates.add("earth");
                missingPlates.add("zap");
                missingPlates.add("draco");
                for (ItemStack itemStack : player.inventory.mainInventory) {
                    if (itemStack.getItem().getRegistryName().toString().equals("pixelmon:splash_plate")) {
                        splash = true;
                        missingPlates.remove("splash");
                    }
                    if (itemStack.getItem().getRegistryName().toString().equals("pixelmon:meadow_plate")) {
                        meadow = true;
                        missingPlates.remove("meadow");
                    }
                    if (itemStack.getItem().getRegistryName().toString().equals("pixelmon:earth_plate")) {
                        earth = true;
                        missingPlates.remove("earth");
                    }
                    if (itemStack.getItem().getRegistryName().toString().equals("pixelmon:zap_plate")) {
                        zap = true;
                        missingPlates.remove("zap");
                    }
                    if (!itemStack.getItem().getRegistryName().toString().equals("pixelmon:draco_plate")) continue;
                    draco = true;
                    missingPlates.remove("draco");
                }
                if (missingUnowns.isEmpty() && splash && meadow && earth && zap && draco) {
                    player.inventory.addItemStackToInventory(new ItemStack(PixelmonItems.jewelOfLife));
                    storage.playerData.setObtainedArceus(true);
                    player.sendMessage(new TextComponentTranslation("pixelmon.jeweloflife.obtained", new Object[0]));
                }
                if (!missingUnowns.isEmpty()) {
                    player.sendMessage(new TextComponentTranslation("pixelmon.unown.notobtained", String.join((CharSequence)", ", missingUnowns)));
                }
                if (!(splash && meadow && earth && zap && draco)) {
                    player.sendMessage(new TextComponentTranslation("pixelmon.plates.notobtained", String.join((CharSequence)", ", missingPlates)));
                }
            } else {
                player.sendMessage(new TextComponentTranslation("pixelmon.jeweloflife.questcomplete", new Object[0]));
            }
        }
        return true;
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        this.initDefaultAI();
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
    public void addVelocity(double par1, double par3, double par5) {
        if (this.canBePushed()) {
            super.addVelocity(par1, par3, par5);
        }
    }
}

