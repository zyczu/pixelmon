/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonObject
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.google.gson.JsonObject;
import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import net.minecraft.item.ItemStack;

public class PokemonDropInformation {
    EnumSpecies pokemon = null;
    Integer form;
    ItemStack mainDrop;
    ItemStack rareDrop;
    ItemStack optDrop1;
    ItemStack optDrop2;
    int mainDropMin = 0;
    int mainDropMax = 1;
    int rareDropMin = 0;
    int rareDropMax = 1;
    int optDrop1Min = 0;
    int optDrop1Max = 1;
    int optDrop2Min = 0;
    int optDrop2Max = 1;

    public PokemonDropInformation() {
    }

    public PokemonDropInformation(EnumSpecies pokemon, int form, JsonObject jsonObject) {
        this.pokemon = pokemon;
        this.form = form;
        if (jsonObject.has("maindropdata")) {
            String mainDropString = jsonObject.get("maindropdata").getAsString();
            this.mainDrop = DropItemRegistry.parseItem(mainDropString, "pokedrops.json");
            if (jsonObject.has("maindropmin")) {
                this.mainDropMin = jsonObject.get("maindropmin").getAsInt();
            }
            if (jsonObject.has("maindropmax")) {
                this.mainDropMax = jsonObject.get("maindropmax").getAsInt();
            }
        }
        if (jsonObject.has("optdrop1data")) {
            String optDrop1String = jsonObject.get("optdrop1data").getAsString();
            this.optDrop1 = DropItemRegistry.parseItem(optDrop1String, "pokedrops.json");
            if (jsonObject.has("optdrop1min")) {
                this.optDrop1Min = jsonObject.get("optdrop1min").getAsInt();
            }
            if (jsonObject.has("optdrop1max")) {
                this.optDrop1Max = jsonObject.get("optdrop1max").getAsInt();
            }
        }
        if (jsonObject.has("optdrop2data")) {
            String optDrop2String = jsonObject.get("optdrop2data").getAsString();
            this.optDrop2 = DropItemRegistry.parseItem(optDrop2String, "pokedrops.json");
            if (jsonObject.has("optdrop2min")) {
                this.optDrop2Min = jsonObject.get("optdrop2min").getAsInt();
            }
            if (jsonObject.has("optdrop2max")) {
                this.optDrop2Max = jsonObject.get("optdrop2max").getAsInt();
            }
        }
        if (jsonObject.has("raredropdata")) {
            String rareDropString = jsonObject.get("raredropdata").getAsString();
            this.rareDrop = DropItemRegistry.parseItem(rareDropString, "pokedrops.json");
            if (jsonObject.has("raredropmin")) {
                this.rareDropMin = jsonObject.get("raredropmin").getAsInt();
            }
            if (jsonObject.has("raredropmax")) {
                this.rareDropMax = jsonObject.get("raredropmax").getAsInt();
            }
        }
    }

    public ArrayList<ItemStack> getDrops() {
        int i;
        int numDrops;
        ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
        if (this.mainDrop != null) {
            numDrops = RandomHelper.getRandomNumberBetween(this.mainDropMin, this.mainDropMax);
            for (i = 0; i < numDrops; ++i) {
                drops.add(this.mainDrop.copy());
            }
        }
        if (this.optDrop1 != null) {
            numDrops = RandomHelper.getRandomNumberBetween(this.optDrop1Min, this.optDrop1Max);
            for (i = 0; i < numDrops; ++i) {
                drops.add(this.optDrop1.copy());
            }
        }
        if (this.optDrop2 != null) {
            numDrops = RandomHelper.getRandomNumberBetween(this.optDrop2Min, this.optDrop2Max);
            for (i = 0; i < numDrops; ++i) {
                drops.add(this.optDrop2.copy());
            }
        }
        if (this.rareDrop != null && RandomHelper.getRandomChance(0.1f)) {
            numDrops = RandomHelper.getRandomNumberBetween(this.rareDropMin, this.rareDropMax);
            for (i = 0; i < numDrops; ++i) {
                drops.add(this.rareDrop.copy());
            }
        }
        return drops;
    }
}

