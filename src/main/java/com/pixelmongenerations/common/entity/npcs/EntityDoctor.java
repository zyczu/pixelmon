/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.npcs;

import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumGui;
import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityDoctor
extends EntityNPC {
    public EntityDoctor(World par1World) {
        super(par1World);
        this.setName("Doctor");
    }

    @Override
    public boolean interactWithNPC(EntityPlayer player, EnumHand hand) {
        player.openGui(Pixelmon.INSTANCE, EnumGui.Doctor.getIndex(), player.world, 0, 0, 0);
        return true;
    }

    @Override
    public ModelBase getModel() {
        return null;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public String getTexture() {
        return null;
    }

    @Override
    public EnumBossMode getBossMode() {
        return EnumBossMode.NotBoss;
    }

    @Override
    public String getDisplayText() {
        return "";
    }
}

