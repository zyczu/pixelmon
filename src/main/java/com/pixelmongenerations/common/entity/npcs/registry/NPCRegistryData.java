/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.pixelmongenerations.common.entity.npcs.registry.BaseTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.GeneralNPCData;
import com.pixelmongenerations.common.entity.npcs.registry.GymNPCData;
import com.pixelmongenerations.common.entity.npcs.registry.ShopkeeperData;
import com.pixelmongenerations.common.entity.npcs.registry.TrainerData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NPCRegistryData {
    public ArrayList<BaseTrainer> trainerTypes = new ArrayList();
    public HashMap<BaseTrainer, List<TrainerData>> trainers = new HashMap();
    public ArrayList<GeneralNPCData> npcs = new ArrayList();
    public ArrayList<ShopkeeperData> shopkeepers = new ArrayList();
    public ArrayList<ShopkeeperData> shopkeeperSpawns = new ArrayList();
    public HashMap<String, GymNPCData> gymnpcs = new HashMap();
}

