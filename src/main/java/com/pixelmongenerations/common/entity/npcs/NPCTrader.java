/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.npcs;

import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.registry.BaseTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.data.trades.PokemonTrades;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumTrainerAI;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;

public class NPCTrader
extends EntityNPC {
    private TradePair tradePair;
    public static final DataParameter<Integer> dwLevel = EntityDataManager.createKey(NPCTrader.class, DataSerializers.VARINT);
    public static final DataParameter<Boolean> dwShiny = EntityDataManager.createKey(NPCTrader.class, DataSerializers.BOOLEAN);
    public static final DataParameter<String> dwOffer = EntityDataManager.createKey(NPCTrader.class, DataSerializers.STRING);
    public static final DataParameter<String> dwExchange = EntityDataManager.createKey(NPCTrader.class, DataSerializers.STRING);
    public static final DataParameter<Integer> dwForm = EntityDataManager.createKey(NPCTrader.class, DataSerializers.VARINT);

    public NPCTrader(World par1World) {
        super(par1World);
        this.setName(I18n.translateToLocal("pixelmon.npc.tradername"));
        this.dataManager.register(dwLevel, 10);
        this.dataManager.register(dwShiny, false);
        this.dataManager.register(dwExchange, "null");
        this.dataManager.register(dwOffer, "null");
        this.dataManager.register(dwForm, -1);
        this.init("Youngster");
    }

    @Override
    public void updateLeashedState() {
        if (this.getLeashed()) {
            this.clearLeashed(true, true);
        }
    }

    @Override
    public void init(String name) {
        super.init(name);
        if ((Integer)this.dataManager.get(dwModel) == -1) {
            BaseTrainer trainer = ServerNPCRegistry.trainers.getRandomBase();
            this.setBaseTrainer(trainer);
            if (trainer.textures.size() > 1) {
                this.dataManager.set(dwTextureIndex, this.world.rand.nextInt(trainer.textures.size()));
            }
        }
        if (!this.hasTrade()) {
            this.setNewTrade();
        }
        this.tradePair = new TradePair(EnumSpecies.getFromName(this.getOffer()).get(), EnumSpecies.getFromName(this.getExchange()).get());
        if (this.dataManager.get(EntityNPC.dwNickname).equalsIgnoreCase("")) {
            this.dataManager.set(EntityNPC.dwNickname, ServerNPCRegistry.getRandomName());
        }
    }

    public void setNewTrade() {
        List<EnumSpecies> pokemon = PokemonTrades.getRandomTrade();
        this.setExchange(pokemon.get(0).name());
        String newOffer = pokemon.get(1).name();
        this.setOffer(newOffer);
        if (Entity3HasStats.hasForms(newOffer)) {
            this.setForm(RandomHelper.getRandomNumberBetween(0, Entity3HasStats.getNumForms(newOffer) - 1));
        }
    }

    public TradePair getTradePair(ArrayList<String> pokemon) {
        return new TradePair(EnumSpecies.getFromName(pokemon.get(0)).get(), EnumSpecies.getFromName(pokemon.get(1)).get());
    }

    public TradePair getTrade() {
        return new TradePair(EnumSpecies.getFromName(this.getOffer()).get(), EnumSpecies.getFromName(this.getExchange()).get());
    }

    @Override
    public void unloadEntity() {
        this.despawnEntity();
        this.setDead();
    }

    @Override
    protected boolean processInteract(EntityPlayer player, EnumHand hand) {
        if (player instanceof EntityPlayerMP) {
            this.updateTradePair();
            ItemStack stack = player.getHeldItem(hand);
            int id = this.getId();
            if (stack.getItem() == PixelmonItems.trainerEditor) {
                this.setAIMode(EnumTrainerAI.StandStill);
                this.initAI();
                player.openGui(Pixelmon.INSTANCE, EnumGui.NPCTrade.getIndex(), player.world, id, 0, 0);
            } else {
                player.openGui(Pixelmon.INSTANCE, EnumGui.NPCTraderGui.getIndex(), player.world, id, 0, 0);
            }
        }
        return true;
    }

    public boolean hasTrade() {
        return !this.getExchange().equals("null") && !this.getOffer().equals("null");
    }

    public void updateTradePair() {
        if (!this.hasTrade()) {
            this.setNewTrade();
        }
        this.tradePair = new TradePair(EnumSpecies.getFromName(this.getOffer()).get(), EnumSpecies.getFromName(this.getExchange()).get());
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        this.updateTradePair();
        nbt.setString("Offer", this.getOffer());
        nbt.setString("Exchange", this.getExchange());
        nbt.setShort("ModelIndex", (short)((Integer)this.dataManager.get(dwModel)).intValue());
        if (this.getBaseTrainer().textures.size() > 1) {
            nbt.setInteger("TextureIndex", (Integer)this.dataManager.get(dwTextureIndex));
        }
        nbt.setInteger("Shiny", this.getIsShiny() ? 1 : 0);
        nbt.setInteger("Lvl", this.getLevel());
        nbt.setInteger("Variant", this.getForm());
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        if (nbt.hasKey("ModelIndex")) {
            this.dataManager.set(dwModel, Integer.valueOf(nbt.getShort("ModelIndex")));
            if (this.getBaseTrainer().textures.size() > 1) {
                this.dataManager.set(dwTextureIndex, nbt.getInteger("TextureIndex"));
            }
        }
        this.setExchange(nbt.getString("Exchange"));
        this.setOffer(nbt.getString("Offer"));
        this.setIsShiny(nbt.getInteger("Shiny") == 1);
        this.setLevel(nbt.getInteger("Lvl"));
        this.setForm(nbt.getInteger("Variant"));
    }

    public void updateTrade(String exchange, String offer, int level, boolean isShiny, int form) {
        this.setExchange(exchange);
        this.setOffer(offer);
        this.setLevel(level);
        this.setIsShiny(isShiny);
        this.setForm(form);
    }

    @Override
    public String getDisplayText() {
        return I18n.translateToLocal("pixelmon.npc.tradername");
    }

    public String getExchange() {
        return this.dataManager.get(dwExchange);
    }

    public String getOffer() {
        return this.dataManager.get(dwOffer);
    }

    public void setOffer(String offer) {
        this.dataManager.set(dwOffer, offer);
    }

    public void setExchange(String exchange) {
        this.dataManager.set(dwExchange, exchange);
    }

    public boolean getIsShiny() {
        return this.dataManager.get(dwShiny);
    }

    public void setIsShiny(boolean isShiny) {
        this.dataManager.set(dwShiny, isShiny);
    }

    public int getForm() {
        return this.dataManager.get(dwForm);
    }

    public void setForm(int form) {
        this.dataManager.set(dwForm, form);
    }

    public int getLevel() {
        return this.dataManager.get(dwLevel);
    }

    public void setLevel(int level) {
        this.dataManager.set(dwLevel, level);
    }

    @Override
    public void initAI() {
        this.tasks.taskEntries.clear();
        this.tasks.addTask(0, new EntityAISwimming(this));
        switch (this.getAIMode()) {
            case StandStill: {
                this.tasks.addTask(1, new EntityAIWatchClosest(this, EntityPlayer.class, 10.0f));
                this.tasks.addTask(2, new EntityAIWatchClosest(this, EntityPixelmon.class, 6.0f));
                break;
            }
            case Wander: {
                this.tasks.addTask(1, new EntityAIWander(this, SharedMonsterAttributes.MOVEMENT_SPEED.getDefaultValue()));
                break;
            }
        }
    }

    @Override
    public void addVelocity(double par1, double par3, double par5) {
        if (this.canBePushed()) {
            super.addVelocity(par1, par3, par5);
        }
    }

    @Override
    public boolean canBePushed() {
        return this.getAIMode() != EnumTrainerAI.StandStill;
    }

    @Override
    protected boolean canDespawn() {
        return this.isSetup() && this.getAIMode() == EnumTrainerAI.Wander;
    }

    public class TradePair {
        public EnumSpecies offer;
        public EnumSpecies exchangefor;

        public TradePair(EnumSpecies offer, EnumSpecies exchangefor) {
            this.offer = offer;
            this.exchangefor = exchangefor;
        }
    }
}

