/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.npcs;

import com.pixelmongenerations.common.block.tileEntities.TileEntityHealer;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class NPCNurseJoy
extends EntityNPC {
    public NPCNurseJoy(World world) {
        super(world);
        this.npcLocation = SpawnLocation.LandVillager;
    }

    @Override
    public String getDisplayText() {
        return "";
    }

    @Override
    public String getTexture() {
        return "pixelmon:textures/steve/doctor.png";
    }

    @Override
    public boolean interactWithNPC(EntityPlayer player, EnumHand hand) {
        if (player instanceof EntityPlayerMP) {
            TileEntityHealer healer = BlockHelper.findClosestTileEntity(TileEntityHealer.class, this, 8.0, h -> !h.beingUsed);
            if (healer != null) {
                ChatHandler.sendChat(player, "gui.nursejoy.healing", new Object[0]);
                healer.use(this, player);
            } else {
                ChatHandler.sendChat(player, "gui.nursejoy.full", new Object[0]);
            }
        }
        return true;
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        this.initDefaultAI();
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
    public void addVelocity(double par1, double par3, double par5) {
        if (this.canBePushed()) {
            super.addVelocity(par1, par3, par5);
        }
    }
}

