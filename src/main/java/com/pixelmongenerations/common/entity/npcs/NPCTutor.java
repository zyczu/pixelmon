/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.common.entity.npcs;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.registry.BaseTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.enums.EnumTrainerAI;
import com.pixelmongenerations.core.network.packetHandlers.npc.InteractNPC;
import com.pixelmongenerations.core.network.packetHandlers.npc.LoadTutorData;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class NPCTutor
extends EntityNPC {
    public ArrayList<Attack> attackList;
    public ArrayList<ArrayList<ItemStack>> costs;
    public boolean isTransfer;

    public NPCTutor(World world) {
        super(world);
        this.init("Tutor");
    }

    @Override
    public void init(String name) {
        if (name.equals("Tutor")) {
            BaseTrainer trainer = ServerNPCRegistry.trainers.getRandomBase();
            name = trainer.name;
            this.setBaseTrainer(trainer);
            if (trainer.textures.size() > 1) {
                this.dataManager.set(dwTextureIndex, this.world.rand.nextInt(trainer.textures.size()));
            }
            this.isTransfer = this.world.rand.nextBoolean();
        }
        super.init(name);
        this.npcLocation = SpawnLocation.LandNPC;
        if (this.attackList == null || this.costs == null) {
            int i;
            this.attackList = new ArrayList();
            for (i = 0; i < 10; ++i) {
                Attack randomAttack;
                while (this.attackList.contains(randomAttack = this.getRandomAttack())) {
                }
                this.attackList.add(randomAttack);
            }
            this.costs = new ArrayList();
            for (i = 0; i < 10; ++i) {
                ArrayList<ItemStack> initialCost = new ArrayList<ItemStack>();
                initialCost.add(this.getRandomCost());
                this.costs.add(initialCost);
            }
        }
        if (this.getAIMode() != EnumTrainerAI.StandStill) {
            this.setAIMode(EnumTrainerAI.Wander);
        }
        this.initAI();
    }

    @Override
    public String getDisplayText() {
        return I18n.translateToLocal("pixelmon.npc.tutorname");
    }

    @Override
    public void addVelocity(double par1, double par3, double par5) {
        if (this.canBePushed()) {
            super.addVelocity(par1, par3, par5);
        }
    }

    @Override
    public boolean canBePushed() {
        return this.getAIMode() != EnumTrainerAI.StandStill;
    }

    @Override
    protected boolean canDespawn() {
        return this.isSetup() && this.getAIMode() == EnumTrainerAI.Wander;
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        if (this.attackList == null) {
            nbt.setInteger("numAttacks", 0);
            return;
        }
        nbt.setInteger("numAttacks", this.attackList.size());
        for (int i = 0; i < this.attackList.size(); ++i) {
            nbt.setString("attack" + i, this.attackList.get(i).getAttackBase().getUnlocalizedName());
            nbt.setInteger("attack" + i + "costNum", this.costs.get(i).size());
            for (int j = 0; j < this.costs.get(i).size(); ++j) {
                ItemStack current = this.costs.get(i).get(j);
                nbt.setInteger("attack" + i + "cost" + j, Item.getIdFromItem(current.getItem()));
                nbt.setInteger("attack" + i + "cost" + j + "Num", current.getCount());
                nbt.setInteger("attack" + i + "cost" + j + "Damage", current.getItemDamage());
            }
        }
        nbt.setBoolean("transfer", this.isTransfer);
    }

    @Override
    public void initAI() {
        this.tasks.taskEntries.clear();
        this.tasks.addTask(0, new EntityAISwimming(this));
        switch (this.getAIMode()) {
            case StandStill: {
                this.tasks.addTask(1, new EntityAIWatchClosest(this, EntityPlayer.class, 10.0f));
                this.tasks.addTask(2, new EntityAIWatchClosest(this, EntityPixelmon.class, 6.0f));
                break;
            }
            case Wander: {
                this.tasks.addTask(1, new EntityAIWander(this, SharedMonsterAttributes.MOVEMENT_SPEED.getDefaultValue()));
                break;
            }
        }
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        int numAttacks = nbt.getInteger("numAttacks");
        if (numAttacks > 0) {
            this.attackList.clear();
            this.costs.clear();
        }
        for (int i = 0; i < numAttacks; ++i) {
            this.attackList.add(new Attack(nbt.getString("attack" + i)));
            int numCosts = nbt.getInteger("attack" + i + "costNum");
            ArrayList<ItemStack> cost = new ArrayList<ItemStack>(numCosts);
            for (int j = 0; j < numCosts; ++j) {
                ItemStack current = new ItemStack(Item.getItemById(nbt.getInteger("attack" + i + "cost" + j)), nbt.getInteger("attack" + i + "cost" + j + "Num"));
                current.setItemDamage(nbt.getInteger("attack" + i + "cost" + j + "Damage"));
                cost.add(current);
            }
            this.costs.add(cost);
        }
        this.isTransfer = nbt.getBoolean("transfer");
    }

    @Override
    public boolean interactWithNPC(EntityPlayer player, EnumHand hand) {
        if (player instanceof EntityPlayerMP) {
            if (this.attackList == null) {
                this.init("");
            }
            ItemStack itemStack = player.getHeldItem(hand);
            Pixelmon.NETWORK.sendTo(new LoadTutorData(this), (EntityPlayerMP)player);
            if (player.capabilities.isCreativeMode && itemStack.getItem() instanceof ItemNPCEditor) {
                this.setAIMode(EnumTrainerAI.StandStill);
                this.initAI();
                player.openGui(Pixelmon.INSTANCE, EnumGui.TutorEditor.getIndex(), player.world, this.getId(), 0, 0);
            } else {
                Pixelmon.NETWORK.sendTo(new InteractNPC(this.getId(), EnumNPCType.Tutor), (EntityPlayerMP)player);
                player.openGui(Pixelmon.INSTANCE, EnumGui.ChooseTutor.getIndex(), player.world, 0, 0, 0);
            }
        } else {
            this.tasks.taskEntries.clear();
        }
        return true;
    }

    @Override
    protected boolean processInteract(EntityPlayer player, EnumHand hand) {
        if (player instanceof EntityPlayerMP && hand == EnumHand.MAIN_HAND) {
            if (this.attackList == null) {
                this.init("");
            }
            ItemStack itemStack = player.getHeldItem(hand);
            Pixelmon.NETWORK.sendTo(new LoadTutorData(this), (EntityPlayerMP)player);
            if (player.capabilities.isCreativeMode && itemStack.getItem() instanceof ItemNPCEditor) {
                this.setAIMode(EnumTrainerAI.StandStill);
                this.initAI();
                player.openGui(Pixelmon.INSTANCE, EnumGui.TutorEditor.getIndex(), player.world, this.getId(), 0, 0);
            } else {
                Pixelmon.NETWORK.sendTo(new InteractNPC(this.getId(), EnumNPCType.Tutor), (EntityPlayerMP)player);
                player.openGui(Pixelmon.INSTANCE, EnumGui.ChooseTutor.getIndex(), player.world, 0, 0, 0);
            }
        } else {
            this.tasks.taskEntries.clear();
        }
        return super.processInteract(player, hand);
    }

    private Attack getRandomAttack() {
        List<Attack> allTutorAttacks = this.isTransfer ? DatabaseMoves.getAllTransferAttacks() : DatabaseMoves.getAllTutorAttacks(PixelmonConfig.allowEventMoveTutors);
        int randomAttack = RandomHelper.getRandomNumberBetween(0, allTutorAttacks.size() - 1);
        return allTutorAttacks.get(randomAttack);
    }

    private ItemStack getRandomCost() {
        return new ItemStack(PixelmonItems.heartScale, RandomHelper.getRandomNumberBetween(1, 3), 0);
    }

    public static void encode(ByteBuf buf, ArrayList<Attack> attackList, ArrayList<ArrayList<ItemStack>> costs) {
        if (attackList != null) {
            buf.writeInt(attackList.size());
            for (int i = 0; i < attackList.size(); ++i) {
                ByteBufUtils.writeUTF8String(buf, attackList.get(i).getAttackBase().getUnlocalizedName());
                buf.writeInt(costs.get(i).size());
                for (int j = 0; j < costs.get(i).size(); ++j) {
                    ItemStack current = costs.get(i).get(j);
                    buf.writeInt(Item.getIdFromItem(current.getItem()));
                    buf.writeInt(current.getCount());
                    buf.writeInt(current.getItemDamage());
                }
            }
        } else {
            buf.writeInt(0);
        }
    }

    public static void decode(ByteBuf buf, ArrayList<Attack> attackList, ArrayList<ArrayList<ItemStack>> costs) {
        int numAttacks = buf.readInt();
        for (int i = 0; i < numAttacks; ++i) {
            attackList.add(new Attack(ByteBufUtils.readUTF8String(buf)));
            int numCosts = buf.readInt();
            ArrayList<ItemStack> cost = new ArrayList<ItemStack>(numCosts);
            for (int j = 0; j < numCosts; ++j) {
                ItemStack current = new ItemStack(Item.getItemById(buf.readInt()), buf.readInt());
                current.setItemDamage(buf.readInt());
                cost.add(current);
            }
            costs.add(cost);
        }
    }
}

