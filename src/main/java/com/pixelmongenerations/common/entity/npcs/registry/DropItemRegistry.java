/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Streams
 *  com.google.common.collect.Table
 *  com.google.common.collect.TreeBasedTable
 *  com.google.gson.Gson
 *  com.google.gson.JsonArray
 *  com.google.gson.JsonElement
 *  com.google.gson.JsonObject
 *  com.google.gson.JsonParser
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.google.common.collect.Streams;
import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pixelmongenerations.common.entity.npcs.registry.BaseShopItem;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryShopkeepers;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonDropInformation;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.entity.pixelmon.Entity8HoldsItems;
import com.pixelmongenerations.common.entity.pixelmon.drops.AlphaDrops;
import com.pixelmongenerations.common.entity.pixelmon.drops.AlphaInfo;
import com.pixelmongenerations.common.entity.pixelmon.drops.BossInfo;
import com.pixelmongenerations.common.entity.pixelmon.drops.TotemDrops;
import com.pixelmongenerations.common.entity.pixelmon.stats.FlyingOptions;
import com.pixelmongenerations.common.entity.pixelmon.stats.SwimOptions;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumMegaPokemon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;

public class DropItemRegistry {
    private static ArrayList<ItemStack> tier1 = new ArrayList();
    private static ArrayList<ItemStack> tier2 = new ArrayList();
    private static ArrayList<ItemStack> tier3 = new ArrayList();
    private static ArrayList<ItemStack> tier4 = new ArrayList();
    private static ArrayList<ItemStack> pokeStopDrops = new ArrayList();
    private static ArrayList<ItemStack> rocketDrops = new ArrayList();
    private static Table<EnumSpecies, Integer, PokemonDropInformation> pokemonDrops = TreeBasedTable.create();
    private static ArrayList<ItemStack> megaDrops = new ArrayList();
    private static ArrayList<ItemStack> shinyMegaDrops = new ArrayList();
    private static ArrayList<BossInfo> bossMap = new ArrayList();
    private static HashMap<EnumSpecies, BossInfo> pokemonToBossInfo = new HashMap();
    private static Set<String> futureDrops = new HashSet<String>();
    private static TotemDrops totemDrops;
    private static AlphaDrops alphaDrops;

    public static void registerDropItems() throws FileNotFoundException {
        Pixelmon.LOGGER.info("Registering drops.");
        String path = Pixelmon.modDirectory + "/pixelmon/drops/";
        File dropsDir = new File(path);
        if (PixelmonConfig.useExternalJSONFilesDrops && !dropsDir.isDirectory()) {
            File baseDir = new File(Pixelmon.modDirectory + "/pixelmon");
            if (!baseDir.isDirectory()) {
                baseDir.mkdir();
            }
            Pixelmon.LOGGER.info("Creating drops directory.");
            dropsDir.mkdir();
            DropItemRegistry.extractDropsDir(dropsDir);
        }
        InputStream iStream = !PixelmonConfig.useExternalJSONFilesDrops ? ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/drops/pokechestdrops.json") : new FileInputStream(new File(dropsDir, "pokechestdrops.json"));
        JsonObject json = new JsonParser().parse((Reader)new InputStreamReader(iStream, StandardCharsets.UTF_8)).getAsJsonObject();
        DropItemRegistry.registerTierDrops(json, "tier1", tier1);
        DropItemRegistry.registerTierDrops(json, "tier2", tier2);
        DropItemRegistry.registerTierDrops(json, "tier3", tier3);
        DropItemRegistry.registerTierDrops(json, "tier4", tier4);
        DropItemRegistry.registerTierDrops(json, "pokeStopDrops", pokeStopDrops);
        DropItemRegistry.registerTierDrops(json, "rocketDrops", rocketDrops);
        DropItemRegistry.registerPokemonDrops();
        iStream = !PixelmonConfig.useExternalJSONFilesDrops ? ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/drops/bossdrops.json") : new FileInputStream(new File(dropsDir, "bossdrops.json"));
        JsonObject jsonObject = new JsonParser().parse((Reader)new InputStreamReader(iStream, StandardCharsets.UTF_8)).getAsJsonObject();
        DropItemRegistry.registerBossDrops(jsonObject);
        iStream = !PixelmonConfig.useExternalJSONFilesDrops ? ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/drops/totemdrops.json") : new FileInputStream(new File(dropsDir, "totemdrops.json"));
        totemDrops = (TotemDrops)new Gson().fromJson((Reader)new InputStreamReader(iStream, StandardCharsets.UTF_8), TotemDrops.class);
        iStream = !PixelmonConfig.useExternalJSONFilesDrops ? ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/drops/alphadrops.json") : new FileInputStream(new File(dropsDir, "alphadrops.json"));
        alphaDrops = (AlphaDrops)new Gson().fromJson((Reader)new InputStreamReader(iStream, StandardCharsets.UTF_8), AlphaDrops.class);
        if (DropItemRegistry.alphaDrops.alphaPokes.removeIf(alphaInfo -> alphaInfo.pokemon == null)) {
            Pixelmon.LOGGER.error("[Alpha Config] A Pokemon in the config has a misspelled species!");
        }
    }

    private static void registerPokemonDrops() throws FileNotFoundException {
        Pixelmon.LOGGER.info("Registering Pok\u00e9mon drops.");
        String path = Pixelmon.modDirectory + "/pixelmon/drops/";
        File dropsDir = new File(path);
        InputStream iStream = !PixelmonConfig.useExternalJSONFilesDrops ? ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/drops/pokedrops.json") : new FileInputStream(new File(dropsDir, "pokedrops.json"));
        JsonArray json = new JsonParser().parse((Reader)new InputStreamReader(iStream, StandardCharsets.UTF_8)).getAsJsonArray();
        DropItemRegistry.registerPokemonDrops(json);
    }

    private static void registerBossDrops(JsonObject jsonObject) {
        DropItemRegistry.registerBossDropArray(jsonObject, "megadrops", megaDrops);
        DropItemRegistry.registerBossDropArray(jsonObject, "shinymegadrops", shinyMegaDrops);
        if (jsonObject.has("bosses")) {
            JsonArray bossArray = jsonObject.get("bosses").getAsJsonArray();
            for (int i = 0; i < bossArray.size(); ++i) {
                JsonObject bossObject = bossArray.get(i).getAsJsonObject();
                EnumSpecies pokemon = EnumSpecies.getFromNameAnyCase(bossObject.get("name").getAsString());
                if (pokemon == null) {
                    Pixelmon.LOGGER.warn("Error in boss information for Pok\u00e9mon " + bossObject.get("name"));
                    continue;
                }
                SpawnLocation spawnLocation = SpawnLocation.getSpawnLocation(bossObject.get("spawnlocation").getAsString());
                BossInfo bossInfo = new BossInfo(pokemon, spawnLocation);
                if (bossObject.has("flying")) {
                    bossInfo.withFlyParams(new FlyingOptions(bossObject.get("flying").getAsJsonObject()));
                } else if (bossObject.has("swimming")) {
                    bossInfo.withSwimParams(new SwimOptions(bossObject.get("swimming").getAsJsonObject()));
                }
                if (bossObject.has("dimensions")) {
                    bossInfo.withDimensions(Streams.stream(bossObject.get("dimensions").getAsJsonArray().iterator()).mapToInt(JsonElement::getAsInt).boxed().collect(Collectors.toList()));
                }
                pokemonToBossInfo.put(pokemon, bossInfo);
                bossMap.add(bossInfo);
            }
        }
    }

    private static void registerBossDropArray(JsonObject jsonObject, String key, List<ItemStack> dropArray) {
        if (jsonObject.has(key)) {
            JsonArray megaDropsArray = jsonObject.get(key).getAsJsonArray();
            for (int i = 0; i < megaDropsArray.size(); ++i) {
                String itemString = megaDropsArray.get(i).getAsString();
                ItemStack itemStack = DropItemRegistry.parseItem(itemString, "bossdrops.json");
                if (itemStack == null) {
                    if (futureDrops.contains(itemString)) continue;
                    Pixelmon.LOGGER.error("Boss drop item not found: " + itemString);
                    continue;
                }
                dropArray.add(itemStack);
            }
        }
    }

    private static void registerPokemonDrops(JsonArray jsonArray) {
        for (int i = 0; i < jsonArray.size(); ++i) {
            JsonObject pokemonDropObject = jsonArray.get(i).getAsJsonObject();
            String pokemonName = pokemonDropObject.get("pokemon").getAsString();
            int form = pokemonDropObject.has("form") ? pokemonDropObject.getAsJsonPrimitive("form").getAsInt() : -1;
            Optional<EnumSpecies> pokemon = EnumSpecies.getFromName(pokemonName);
            if (!pokemon.isPresent()) continue;
            PokemonDropInformation drop = new PokemonDropInformation(pokemon.get(), form, pokemonDropObject);
            if (pokemon.get() == EnumSpecies.Pikachu) continue;
            pokemonDrops.put(pokemon.get(), form, drop);
        }
        DropItemRegistry.dealWithSpecialCases();
    }

    private static void dealWithSpecialCases() {
        PokemonDropInformation drop = new PokemonDropInformation();
        drop.pokemon = EnumSpecies.Pikachu;
        drop.mainDrop = new ItemStack(PixelmonItems.libreCostume);
        drop.optDrop1 = new ItemStack(PixelmonItems.belleCostume);
        drop.optDrop2 = new ItemStack(PixelmonItems.popStarCostume);
        drop.rareDrop = new ItemStack(PixelmonItems.phdCostume);
        drop.optDrop2Min = 0;
        drop.optDrop1Min = 0;
        drop.rareDropMin = 0;
        drop.mainDropMin = 0;
        drop.optDrop2Max = 1;
        drop.optDrop1Max = 1;
        drop.rareDropMax = 1;
        drop.mainDropMax = 1;
        pokemonDrops.put(EnumSpecies.Pikachu, -1, drop);
    }

    private static void registerTierDrops(JsonObject json, String tierName, List<ItemStack> tierList) {
        if (json.has(tierName)) {
            JsonArray jsonArray = JsonUtils.getJsonArray(json, tierName);
            for (int i = 0; i < jsonArray.size(); ++i) {
                String itemName = jsonArray.get(i).getAsString();
                Item item = null;
                ResourceLocation loc = new ResourceLocation(itemName);
                item = Item.REGISTRY.getObject(loc);
                if (item == null) {
                    Pixelmon.LOGGER.info("Item not found: " + itemName + " in pokechestdrops.json.");
                    continue;
                }
                if (NPCRegistryShopkeepers.shopItems.containsKey(itemName)) {
                    BaseShopItem bItem = NPCRegistryShopkeepers.shopItems.get(itemName);
                    tierList.add(bItem.itemStack);
                    continue;
                }
                tierList.add(new ItemStack(item));
            }
        }
    }

    private static void extractDropsDir(File dropsDir) {
        ServerNPCRegistry.extractFile("/assets/pixelmon/drops/pokechestdrops.json", dropsDir, "pokechestdrops.json");
        ServerNPCRegistry.extractFile("/assets/pixelmon/drops/pokedrops.json", dropsDir, "pokedrops.json");
        ServerNPCRegistry.extractFile("/assets/pixelmon/drops/bossdrops.json", dropsDir, "bossdrops.json");
        ServerNPCRegistry.extractFile("/assets/pixelmon/drops/totemdrops.json", dropsDir, "totemdrops.json");
        ServerNPCRegistry.extractFile("/assets/pixelmon/drops/alphadrops.json", dropsDir, "alphadrops.json");
    }

    public static ItemStack getTier1Drop() {
        return RandomHelper.getRandomElementFromList(tier1);
    }

    public static ItemStack getTier2Drop() {
        return RandomHelper.getRandomElementFromList(tier2);
    }

    public static ItemStack getTier3Drop() {
        return RandomHelper.getRandomElementFromList(tier3);
    }

    public static ItemStack getTier4Drop() {
        return RandomHelper.getRandomElementFromList(tier4);
    }

    public static ArrayList<ItemStack> getPokeStopDrops() {
        return RandomHelper.getThreeRandomItemsFromList(pokeStopDrops);
    }

    public static ArrayList<ItemStack> getRocketDrops() {
        return RandomHelper.getThreeRandomItemsFromList(rocketDrops);
    }

    public static ArrayList<ItemStack> getDropsForPokemon(Entity8HoldsItems pixelmon) {
        PokemonDropInformation dropInfo = (PokemonDropInformation)pokemonDrops.get(pixelmon.baseStats.pokemon, pixelmon.getForm());
        if (dropInfo == null && (dropInfo = (PokemonDropInformation)pokemonDrops.get(pixelmon.baseStats.pokemon, -1)) == null) {
            return new ArrayList<ItemStack>(0);
        }
        return dropInfo.getDrops();
    }

    public static ArrayList<ItemStack> getDropsForPokemon(EnumSpecies species, int form) {
        PokemonDropInformation dropInfo = (PokemonDropInformation)pokemonDrops.get(species, form);
        if (dropInfo == null && (dropInfo = (PokemonDropInformation)pokemonDrops.get(species, -1)) == null) {
            return new ArrayList<ItemStack>(0);
        }
        return dropInfo.getDrops();
    }

    public static ItemStack parseItem(String name, String filename) {
        String[] splits = name.split(":");
        int data = 0;
        if (splits.length > 2) {
            data = Integer.parseInt(splits[2]);
            name = splits[0] + ":" + splits[1];
        }
        ResourceLocation location = new ResourceLocation(name);
        Item item = null;
        item = Item.REGISTRY.getObject(location);
        if (item == null) {
            if (!futureDrops.contains(name)) {
                Pixelmon.LOGGER.info("Item not found: " + name + " in " + filename);
            }
            return null;
        }
        return new ItemStack(item, 1, data);
    }

    public static ArrayList<ItemStack> getBossDrops(Entity8HoldsItems pixelmon, EntityPlayerMP player) {
        EnumSpecies pokemon;
        ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
        EnumBossMode bossMode = pixelmon.getBossMode();
        if (bossMode.extraLevels <= EnumBossMode.Rare.extraLevels) {
            DropItemRegistry.addRandomBossDrops(drops, megaDrops, 0.333f);
        } else if (bossMode.extraLevels >= EnumBossMode.Legendary.extraLevels) {
            DropItemRegistry.addRandomBossDrops(drops, shinyMegaDrops, 0.5f);
        }
        Optional<PlayerStorage> storage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (storage.isPresent() && (pokemon = pixelmon.baseStats.pokemon).hasMega()) {
            int form = pixelmon.getForm();
            if (!storage.get().megaData.isMegaItemObtained(pokemon, form)) {
                drops.add(new ItemStack(EnumMegaPokemon.getMega(pixelmon.getSpecies()).getMegaEvoItems()[form - 1]));
                storage.get().megaData.obtainedItem(pokemon, form, player);
            } else if (RandomHelper.getRandomChance(0.025f)) {
                drops.add(new ItemStack(EnumMegaPokemon.getMega(pixelmon.getSpecies()).getMegaEvoItems()[form - 1]));
            }
        }
        return drops;
    }

    private static void addRandomBossDrops(List<ItemStack> drops, List<ItemStack> bossDropStore, float candyChance) {
        ItemStack megaDrop;
        if (RandomHelper.getRandomChance(candyChance)) {
            drops.add(new ItemStack(PixelmonItems.rareCandy));
        }
        if ((megaDrop = RandomHelper.getRandomElementFromList(bossDropStore)) == null) {
            return;
        }
        megaDrop = megaDrop.copy();
        megaDrop.setCount(1);
        drops.add(megaDrop);
    }

    public static ArrayList<AlphaInfo> getAlphaPokemon() {
        return DropItemRegistry.alphaDrops.alphaPokes;
    }

    public static ArrayList<ItemStack> getTotemDrops(Entity8HoldsItems pixelmon) {
        return totemDrops.getDropsFor(pixelmon);
    }

    public static ArrayList<ItemStack> getAlphaDrops(Entity8HoldsItems pixelmon) {
        return alphaDrops.getDropsFor(pixelmon);
    }

    public static ArrayList<BossInfo> getBossPokemon() {
        return bossMap;
    }

    public static BossInfo getBossPokemon(EnumSpecies pokemon) {
        return pokemonToBossInfo.get(pokemon);
    }

    static {
        futureDrops.addAll(Arrays.asList("minecraft:iron_nugget", "minecraft:shulker_shell", "minecraft:totem_of_undying", "pixelmon:cleanse_tag", "pixelmon:clever_wing", "pixelmon:fluffy_tail", "pixelmon:genius_wing", "pixelmon:honey"));
    }
}

