/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.npcs;

import com.pixelmongenerations.common.entity.npcs.EntityIndexedNPC;
import com.pixelmongenerations.common.entity.npcs.registry.BaseShopItem;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.entity.npcs.registry.ShopItem;
import com.pixelmongenerations.common.entity.npcs.registry.ShopItemWithVariation;
import com.pixelmongenerations.common.entity.npcs.registry.ShopkeeperChat;
import com.pixelmongenerations.common.entity.npcs.registry.ShopkeeperData;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.packetHandlers.npc.SetNPCData;
import com.pixelmongenerations.core.network.packetHandlers.npc.SetNPCEditData;
import com.pixelmongenerations.core.network.packetHandlers.npc.SetShopkeeperClient;
import java.util.ArrayList;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;

public class NPCShopkeeper
extends EntityIndexedNPC {
    private long lastUpdatedTime = 0L;
    private ArrayList<ShopItemWithVariation> itemList;
    ArrayList<UUID> playerList = new ArrayList();

    public NPCShopkeeper(World world) {
        super(world);
    }

    public void init(ShopkeeperData data) {
        this.npcIndex = data.id;
        this.nameIndex = data.getRandomNameIndex();
        this.chatIndex = data.getRandomChatIndex();
        this.setCustomSteveTexture(data.getRandomTexture());
        if (this.getId() == -1) {
            this.setId(idIndex++);
        }
    }

    @Override
    public void init(String name) {
        super.init(name);
        if (this.getCustomSteveTexture().equals("")) {
            this.setCustomSteveTexture("npcchat1.png");
        }
    }

    public void initRandom(String biomeID) {
        ShopkeeperData data = ServerNPCRegistry.shopkeepers.getRandomSpawning(biomeID);
        if (data == null) {
            this.setDead();
        } else {
            this.init(data);
        }
        this.npcLocation = SpawnLocation.LandNPC;
    }

    @Override
    public String getTexture() {
        return "pixelmon:textures/steve/" + this.getCustomSteveTexture();
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        this.initDefaultAI();
        if (nbt.hasKey("ShopItems")) {
            NBTTagList list = nbt.getTagList("ShopItems", 10);
            this.itemList = new ArrayList();
            for (int i = 0; i < list.tagCount(); ++i) {
                ShopItemWithVariation item = ShopItemWithVariation.getFromNBT(this.npcIndex, list.getCompoundTagAt(i));
                if (item == null) continue;
                this.getItemList().add(item);
            }
        }
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        NBTTagList list = new NBTTagList();
        if (this.getItemList() != null) {
            for (ShopItemWithVariation item : this.getItemList()) {
                item.writeToNBT(list);
            }
            nbt.setTag("ShopItems", list);
        }
    }

    @Override
    public boolean interactWithNPC(EntityPlayer player, EnumHand hand) {
        ItemStack itemStack = player.getHeldItem(hand);
        if (!(player instanceof EntityPlayerMP)) {
            this.tasks.taskEntries.clear();
            return true;
        }
        if (itemStack.getItem() instanceof ItemNPCEditor) {
            if (!this.checkOP(player)) {
                return false;
            }
            EntityPlayerMP playerMP = (EntityPlayerMP)player;
            String loc = playerMP.language;
            Pixelmon.NETWORK.sendTo(new SetShopkeeperClient(loc), playerMP);
            Pixelmon.NETWORK.sendTo(new SetNPCEditData(ServerNPCRegistry.shopkeepers.getJsonName(this.npcIndex), this.getShopkeeperName(loc), this.getCustomSteveTexture()), playerMP);
            player.openGui(Pixelmon.INSTANCE, EnumGui.ShopkeeperEditor.getIndex(), player.world, this.getId(), 0, 0);
            return true;
        }
        if (this.getItemList() == null || this.getItemList().isEmpty()) {
            this.loadItems();
        }
        this.sendItemsToPlayer(player);
        player.openGui(Pixelmon.INSTANCE, EnumGui.Shopkeeper.getIndex(), player.world, this.getId(), 0, 0);
        return true;
    }

    public void sendItemsToPlayer(EntityPlayer player) {
        ArrayList<ShopItemWithVariation> sellList = this.getSellList(player);
        String loc = ((EntityPlayerMP)player).language;
        this.playerList.add(player.getUniqueID());
        Pixelmon.NETWORK.sendTo(new SetNPCData(this.getShopkeeperName(loc), this.getShopkeeperChat(loc), this.getItemList(), sellList), (EntityPlayerMP)player);
    }

    public ArrayList<ShopItemWithVariation> getSellList(EntityPlayer player) {
        ArrayList<ShopItemWithVariation> sellList = new ArrayList<ShopItemWithVariation>();
        for (int i = 0; i < player.inventory.mainInventory.size(); ++i) {
            BaseShopItem baseItem;
            ItemStack item = player.inventory.mainInventory.get(i);
            ShopItemWithVariation shopItem = this.getExistingItem(item);
            if (shopItem == null && (baseItem = ServerNPCRegistry.shopkeepers.getItem(item)) != null) {
                shopItem = new ShopItemWithVariation(new ShopItem(baseItem, 1.0f, 1.0f, false), 1.0f);
            }
            if (shopItem == null || !shopItem.canSell() || this.alreadyListed(sellList, shopItem)) continue;
            sellList.add(shopItem);
        }
        return sellList;
    }

    private boolean alreadyListed(ArrayList<ShopItemWithVariation> sellList, ShopItemWithVariation shopItem) {
        for (ShopItemWithVariation item : sellList) {
            if (item.getItem() != shopItem.getItem()) continue;
            return true;
        }
        return false;
    }

    private ShopItemWithVariation getExistingItem(ItemStack item) {
        for (ShopItemWithVariation si : this.getItemList()) {
            if (!ItemStack.areItemStacksEqual(si.getItem(), item)) continue;
            return si;
        }
        return null;
    }

    private ShopkeeperChat getShopkeeperChat(String langCode) {
        int index = this.chatIndex;
        return ServerNPCRegistry.shopkeepers.getTranslatedChat(langCode, this.npcIndex, index);
    }

    public String getShopkeeperName(String langCode) {
        return ServerNPCRegistry.shopkeepers.getTranslatedName(langCode, this.npcIndex, this.nameIndex);
    }

    public void loadItems() {
        this.lastUpdatedTime = this.world.getTotalWorldTime();
        ShopkeeperData skd = ServerNPCRegistry.shopkeepers.getById(this.npcIndex);
        this.itemList = skd != null ? skd.getItemList() : new ArrayList();
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        if (!this.world.isRemote && this.playerList.size() > 0) {
            for (int i = 0; i < this.playerList.size(); ++i) {
                EntityPlayer player = this.world.getPlayerEntityByUUID(this.playerList.get(i));
                if (player != null && player.openContainer instanceof ContainerEmpty) continue;
                this.playerList.remove(i);
                --i;
            }
        }
        if (!this.world.isRemote && this.world.getTotalWorldTime() > this.lastUpdatedTime + 24000L && this.playerList.size() == 0) {
            this.loadItems();
        }
    }

    public ArrayList<ShopItemWithVariation> getItemList() {
        return this.itemList;
    }

    public void cycleJson(EntityPlayerMP p, String newJSON) {
        ShopkeeperData id = ServerNPCRegistry.shopkeepers.getById(newJSON);
        if (id == null) {
            return;
        }
        this.init(id);
        String loc = p.language;
        Pixelmon.NETWORK.sendTo(new SetNPCEditData(ServerNPCRegistry.shopkeepers.getJsonName(this.npcIndex), this.getShopkeeperName(loc), this.getCustomSteveTexture()), p);
    }

    public void cycleName(EntityPlayerMP p, int nameIndex) {
        this.nameIndex = nameIndex;
        String loc = p.language;
        Pixelmon.NETWORK.sendTo(new SetNPCEditData(ServerNPCRegistry.shopkeepers.getJsonName(this.npcIndex), this.getShopkeeperName(loc), this.getCustomSteveTexture()), p);
    }

    @Override
    public String getDisplayText() {
        return I18n.translateToLocal("gui.shopkeeper.name");
    }

    @Override
    public String getSubTitleText() {
        return null;
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
    public void addVelocity(double par1, double par3, double par5) {
        if (this.canBePushed()) {
            super.addVelocity(par1, par3, par5);
        }
    }
}

