/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.npcs;

import com.pixelmongenerations.api.events.npc.NPCChatEvent;
import com.pixelmongenerations.common.entity.npcs.EntityIndexedNPC;
import com.pixelmongenerations.common.entity.npcs.registry.BaseTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.ClientNPCData;
import com.pixelmongenerations.common.entity.npcs.registry.GeneralNPCData;
import com.pixelmongenerations.common.entity.npcs.registry.GymNPCData;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryTrainers;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.packetHandlers.npc.SetChattingNPCTextures;
import com.pixelmongenerations.core.network.packetHandlers.npc.SetNPCData;
import com.pixelmongenerations.core.network.packetHandlers.npc.SetNPCEditData;
import java.util.ArrayList;
import java.util.Arrays;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class NPCChatting
extends EntityIndexedNPC {
    private ArrayList<String> chatPages = new ArrayList();
    boolean usingDefaultName = true;
    boolean usingDefaultChat = true;

    public NPCChatting(World world) {
        super(world);
    }

    public void init(GeneralNPCData data) {
        this.npcIndex = data.id;
        this.nameIndex = data.getRandomNameIndex();
        this.chatIndex = data.getRandomChatIndex();
        if (this.getId() == -1) {
            this.setId(idIndex++);
        }
        this.npcLocation = SpawnLocation.LandVillager;
    }

    public void init(GymNPCData data) {
        this.npcIndex = "_gym_" + data.id;
        this.nameIndex = data.getRandomNameIndex();
        this.chatIndex = data.getRandomChatIndex();
        if (this.getId() == -1) {
            this.setId(idIndex++);
        }
        this.npcLocation = SpawnLocation.LandVillager;
    }

    @Override
    public void init(String name) {
        super.init(name);
        this.setCustomSteveTexture("npcchat1.png");
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public String getTexture() {
        String pathName;
        BaseTrainer t = this.getBaseTrainer();
        String string = pathName = t == NPCRegistryTrainers.Alex ? "alex" : "steve";
        if (!t.textures.isEmpty() && this.getTextureIndex() > -1) {
            if (this.getTextureIndex() >= t.textures.size()) {
                this.setTextureIndex(t.textures.size() - 1);
            }
            return t.textures.get(this.getTextureIndex()).equals("Custom_RP") || this.getProfession() == 0 ? "pixelmon:textures/" + pathName + "/" + this.getCustomSteveTexture() + ".png" : "pixelmon:textures/" + pathName + "/" + t.textures.get(this.getTextureIndex()) + ".png";
        }
        return this.getTextureIndex() == -1 ? "pixelmon:textures/" + pathName + "/" + this.getCustomSteveTexture() : (t != NPCRegistryTrainers.Steve ? "pixelmon:textures/" + pathName + "/" + t.name.toLowerCase() + ".png" : "pixelmon:textures/" + pathName + "/" + this.getCustomSteveTexture().toLowerCase());
    }

    @Override
    public String getDisplayText() {
        return "";
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        nbt.setBoolean("DefaultName", this.usingDefaultName);
        nbt.setBoolean("DefaultGreet", this.usingDefaultChat);
        nbt.setInteger("chatNum", this.chatPages.size());
        int i = 0;
        for (String page : this.chatPages) {
            nbt.setString("chat" + i, page);
            ++i;
        }
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        this.usingDefaultName = nbt.getBoolean("DefaultName");
        this.usingDefaultChat = nbt.getBoolean("DefaultGreet");
        int numPages = nbt.getInteger("chatNum");
        for (int i = 0; i < numPages; ++i) {
            this.chatPages.add(nbt.getString("chat" + i));
        }
        if (this.getProfession() != 0) {
            this.initDefaultAI();
        }
    }

    @Override
    public boolean interactWithNPC(EntityPlayer player, EnumHand hand) {
        ItemStack itemstack = player.getHeldItem(hand);
        if (player instanceof EntityPlayerMP) {
            EntityPlayerMP playerMP = (EntityPlayerMP)player;
            String loc = playerMP.language;
            if (itemstack.getItem() instanceof ItemNPCEditor) {
                if (!this.checkOP(player)) {
                    return false;
                }
                Pixelmon.NETWORK.sendTo(new SetNPCEditData(this.getName(loc), this.getChat(loc)), playerMP);
                Pixelmon.NETWORK.sendTo(new SetChattingNPCTextures(), playerMP);
                player.openGui(Pixelmon.INSTANCE, EnumGui.NPCChatEditor.getIndex(), player.world, this.getId(), 0, 0);
            } else if (!this.getChat(loc).isEmpty()) {
                NPCChatEvent event = new NPCChatEvent(this, player, this.getChat(loc));
                if (MinecraftForge.EVENT_BUS.post(event)) {
                    return false;
                }
                Pixelmon.NETWORK.sendTo(new SetNPCData(this.getName(loc), event.getChat()), (EntityPlayerMP)player);
                player.openGui(Pixelmon.INSTANCE, EnumGui.NPCChat.getIndex(), player.world, this.getId(), 0, 0);
            }
        } else {
            this.tasks.taskEntries.clear();
        }
        return true;
    }

    @Override
    public ArrayList<String> getChat(String langCode) {
        if (this.usingDefaultChat) {
            int index = this.chatIndex;
            return this.npcIndex.startsWith("_gym_") ? new ArrayList<String>(Arrays.asList(ServerNPCRegistry.getTranslatedGymMemberChat(langCode, this.npcIndex.substring(5), index))) : new ArrayList<String>(Arrays.asList(ServerNPCRegistry.villagers.getTranslatedChat(langCode, this.npcIndex, index)));
        }
        return this.chatPages;
    }

    @Override
    public String getName(String langCode) {
        if (this.usingDefaultName) {
            int index = this.nameIndex;
            return this.npcIndex.startsWith("_gym_") ? ServerNPCRegistry.getTranslatedGymMemberName(langCode, this.npcIndex.substring(5), index) : ServerNPCRegistry.villagers.getTranslatedName(langCode, this.npcIndex, index);
        }
        return this.getName();
    }

    public void setChat(ArrayList<String> pages) {
        this.chatPages = pages;
        this.usingDefaultChat = false;
    }

    @Override
    public void setName(String name) {
        super.setName(name);
        this.usingDefaultName = false;
    }

    public void cycleTexture(EntityPlayerMP p, ClientNPCData newData) {
        GeneralNPCData data = ServerNPCRegistry.villagers.getData(newData.getID());
        if (data != null) {
            this.init(data);
            this.setCustomSteveTexture(newData.getTexture());
            Pixelmon.NETWORK.sendTo(new SetNPCEditData(this.getName(p.language), this.getChat(p.language)), p);
        }
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
    public void addVelocity(double par1, double par3, double par5) {
        if (this.canBePushed()) {
            super.addVelocity(par1, par3, par5);
        }
    }
}

