/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonArray
 *  com.google.gson.JsonObject
 *  com.google.gson.JsonParser
 */
package com.pixelmongenerations.common.entity.npcs.registry;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pixelmongenerations.common.entity.npcs.registry.BaseTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.LanguageNotFoundException;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryData;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.entity.npcs.registry.TrainerChat;
import com.pixelmongenerations.common.entity.npcs.registry.TrainerData;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.util.JsonUtils;

public class NPCRegistryTrainers {
    public static BaseTrainer Steve;
    public static BaseTrainer Alex;

    void loadTrainer(NPCRegistryData thisData, String name, String langCode) throws Exception {
        try {
            List<TrainerData> list;
            JsonObject jsonelement1;
            JsonArray jsonarray;
            String path = Pixelmon.modDirectory + "/pixelmon/npcs/trainers/";
            File tDir = new File(path);
            InputStream istream = null;
            if (!PixelmonConfig.useExternalJSONFilesNPCs) {
                istream = ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/npcs/trainers/" + name + "_" + langCode.toLowerCase() + ".json");
            } else {
                File file = new File(tDir, name + "_" + langCode.toLowerCase() + ".json");
                if (file.exists()) {
                    istream = new FileInputStream(file);
                }
            }
            if (istream == null && !langCode.equals(ServerNPCRegistry.en_us)) {
                throw new LanguageNotFoundException();
            }
            if (istream == null) {
                return;
            }
            TrainerData data = new TrainerData(name);
            JsonObject json = new JsonParser().parse((Reader)new InputStreamReader(istream, StandardCharsets.UTF_8)).getAsJsonObject();
            if (json.has("data")) {
                JsonObject object = JsonUtils.getJsonObject(json.get("data"), "data");
                String trainerType = object.get("trainerType").getAsString();
                data.trainerType = this.get(trainerType);
                data.minLevel = JsonUtils.getInt(object, "minLevel");
                data.maxLevel = JsonUtils.getInt(object, "maxLevel");
                data.minPartyPokemon = JsonUtils.getInt(object, "minPartyPokemon");
                data.maxPartyPokemon = JsonUtils.getInt(object, "maxPartyPokemon");
                data.winnings = JsonUtils.getInt(object, "winnings");
            }
            if (json.has("names")) {
                jsonarray = JsonUtils.getJsonArray(json, "names");
                for (int i = 0; i < jsonarray.size(); ++i) {
                    jsonelement1 = jsonarray.get(i).getAsJsonObject();
                    String npcname = jsonelement1.get("name").getAsString();
                    data.addName(npcname);
                }
            }
            if (json.has("pokemon")) {
                jsonarray = JsonUtils.getJsonArray(json, "pokemon");
                for (int i = 0; i < jsonarray.size(); ++i) {
                    jsonelement1 = jsonarray.get(i).getAsJsonObject();
                    String pokename = jsonelement1.get("name").getAsString();
                    EnumSpecies poke = EnumSpecies.getFromNameAnyCase(pokename);
                    if (poke == null) continue;
                    PokemonForm pokemonForm = new PokemonForm(poke);
                    String formKey = "form";
                    if (jsonelement1.has(formKey)) {
                        pokemonForm.form = jsonelement1.get(formKey).getAsInt();
                    }
                    data.addPokemon(pokemonForm);
                }
            }
            if (json.has("chat")) {
                jsonarray = JsonUtils.getJsonArray(json, "chat");
                for (int i = 0; i < jsonarray.size(); ++i) {
                    jsonelement1 = jsonarray.get(i).getAsJsonObject();
                    String opening = jsonelement1.get("opening").getAsString();
                    String win = jsonelement1.get("win").getAsString();
                    String lose = jsonelement1.get("lose").getAsString();
                    data.addChat(opening, win, lose);
                }
            }
            if ((list = thisData.trainers.get(data.trainerType)) == null) {
                list = new ArrayList<TrainerData>();
            }
            list.add(data);
            thisData.trainers.put(data.trainerType, list);
        }
        catch (LanguageNotFoundException e) {
            throw e;
        }
        catch (Exception e) {
            throw new Exception("Error in trainer " + name + "_" + langCode.toLowerCase(), e);
        }
    }

    public boolean has(String trainerName) {
        NPCRegistryData trainerData = ServerNPCRegistry.data.get(ServerNPCRegistry.en_us);
        if (trainerData != null) {
            for (BaseTrainer trainer : ServerNPCRegistry.data.get((Object)ServerNPCRegistry.en_us).trainerTypes) {
                if (!trainer.name.equalsIgnoreCase(trainerName)) continue;
                return true;
            }
        }
        return false;
    }

    public BaseTrainer get(String trainerName) {
        for (BaseTrainer trainer : ServerNPCRegistry.data.get((Object)ServerNPCRegistry.en_us).trainerTypes) {
            if (!trainer.name.equalsIgnoreCase(trainerName)) continue;
            return trainer;
        }
        return null;
    }

    public BaseTrainer getById(int id) {
        NPCRegistryData englishData = ServerNPCRegistry.data.get(ServerNPCRegistry.en_us);
        if (englishData != null) {
            for (BaseTrainer trainer : englishData.trainerTypes) {
                if (trainer.id != id) continue;
                return trainer;
            }
        }
        return Steve;
    }

    public BaseTrainer getNextBase(BaseTrainer model) {
        int index = model.id + 1;
        if (index >= BaseTrainer._index) {
            index = 0;
        }
        return this.getById(index);
    }

    public BaseTrainer getRandomBase() {
        return this.getById(RandomHelper.getRandomNumberBetween(0, BaseTrainer._index - 1));
    }

    public TrainerData getRandomData(BaseTrainer trainer) {
        List<TrainerData> list = ServerNPCRegistry.data.get((Object)ServerNPCRegistry.en_us).trainers.get(trainer);
        if (list == null) {
            return null;
        }
        return RandomHelper.getRandomElementFromList(list);
    }

    public TrainerData getRandomData(String name) {
        BaseTrainer trainer = this.get(name);
        return this.getRandomData(trainer);
    }

    public BaseTrainer getRandomBaseWithData() {
        TrainerData data = null;
        BaseTrainer t = null;
        while (data == null) {
            t = this.getRandomBase();
            data = this.getRandomData(t.name);
        }
        return t;
    }

    public static BaseTrainer getByName(String name) {
        NPCRegistryData data = ServerNPCRegistry.data.get(ServerNPCRegistry.en_us);
        if (data != null) {
            for (BaseTrainer trainer : data.trainerTypes) {
                if (!trainer.name.equalsIgnoreCase(name)) continue;
                return trainer;
            }
        }
        return Steve;
    }

    public TrainerData getTranslatedData(String langCode, BaseTrainer baseTrainer, String id) {
        TrainerData trainer;
        String lowerLangCode = langCode.toLowerCase();
        if (!ServerNPCRegistry.data.containsKey(lowerLangCode)) {
            try {
                ServerNPCRegistry.registerNPCS(lowerLangCode);
            }
            catch (LanguageNotFoundException e) {
                ServerNPCRegistry.data.put(lowerLangCode, ServerNPCRegistry.data.get(ServerNPCRegistry.en_us));
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        if ((trainer = this.findTrainerData(lowerLangCode, baseTrainer, id)) != null) {
            return trainer;
        }
        return this.findTrainerData(ServerNPCRegistry.en_us, baseTrainer, id);
    }

    private TrainerData findTrainerData(String langCode, BaseTrainer baseTrainer, String id) {
        List<TrainerData> trainerData;
        NPCRegistryData npcData = ServerNPCRegistry.data.get(langCode);
        if (npcData != null && (trainerData = npcData.trainers.get(baseTrainer)) != null) {
            for (TrainerData trainer : trainerData) {
                if (!trainer.id.equals(id)) continue;
                return trainer;
            }
        }
        return null;
    }

    public String getTranslatedRandomName(String langCode, BaseTrainer baseTrainer, String id) {
        try {
            ArrayList<String> names = this.getTranslatedData((String)langCode.toLowerCase(), (BaseTrainer)baseTrainer, (String)id).names;
            return RandomHelper.getRandomElementFromList(names);
        }
        catch (NullPointerException e) {
            return "Steve";
        }
    }

    public String getTranslatedName(String langCode, BaseTrainer baseTrainer, String id, int index) {
        ArrayList<String> names = this.getTranslatedData((String)langCode.toLowerCase(), (BaseTrainer)baseTrainer, (String)id).names;
        if (index >= names.size()) {
            index = 0;
        }
        return names.get(index);
    }

    public String getTranslatedGreeting(String langCode, BaseTrainer baseTrainer, String id, int index) {
        TrainerData trainerData = this.getTranslatedData(langCode.toLowerCase(), baseTrainer, id);
        if (trainerData == null) {
            return "";
        }
        ArrayList<TrainerChat> chat = trainerData.chat;
        if (index >= chat.size()) {
            index = 0;
        }
        return chat.get((int)index).opening;
    }

    public String getTranslatedWin(String langCode, BaseTrainer baseTrainer, String id, int index) {
        TrainerData trainerData = this.getTranslatedData(langCode.toLowerCase(), baseTrainer, id);
        if (trainerData == null) {
            return "";
        }
        ArrayList<TrainerChat> chat = trainerData.chat;
        if (index >= chat.size()) {
            index = 0;
        }
        return chat.get((int)index).win;
    }

    public String getTranslatedLose(String langCode, BaseTrainer baseTrainer, String id, int index) {
        TrainerData trainerData = this.getTranslatedData(langCode.toLowerCase(), baseTrainer, id);
        if (trainerData == null) {
            return "";
        }
        ArrayList<TrainerChat> chat = trainerData.chat;
        if (index >= chat.size()) {
            index = 0;
        }
        return chat.get((int)index).lose;
    }

    public ArrayList<BaseTrainer> getTypes() {
        return ServerNPCRegistry.data == null || ServerNPCRegistry.data.get(ServerNPCRegistry.en_us) == null ? new ArrayList<BaseTrainer>() : ServerNPCRegistry.data.get((Object)ServerNPCRegistry.en_us).trainerTypes;
    }
}

