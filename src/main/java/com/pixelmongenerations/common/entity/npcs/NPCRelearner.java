/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.npcs;

import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.enums.EnumTrainerAI;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.packetHandlers.npc.InteractNPC;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;

public class NPCRelearner
extends EntityNPC {
    private static final DataParameter<ItemStack> COST = EntityDataManager.createKey(NPCRelearner.class, DataSerializers.ITEM_STACK);

    public NPCRelearner(World world) {
        super(world);
        this.init(I18n.translateToLocal("pixelmon.npc.relearnername"));
        this.dataManager.register(COST, ItemStack.EMPTY);
    }

    @Override
    public String getTexture() {
        return "pixelmon:textures/steve/npcchat1.png";
    }

    @Override
    public String getDisplayText() {
        return I18n.translateToLocal("pixelmon.npc.relearnername");
    }

    public ItemStack getCost() {
        ItemStack cost = this.dataManager.get(COST);
        if (cost.isEmpty() || cost.getItem() instanceof ItemNPCEditor || cost.getItem() == Items.AIR) {
            return ItemStack.EMPTY;
        }
        return cost;
    }

    public void setCost(ItemStack item) {
        this.dataManager.set(COST, item);
    }

    @Override
    public boolean interactWithNPC(EntityPlayer player, EnumHand hand) {
        if (player instanceof EntityPlayerMP) {
            ItemStack cost = this.getCost();
            ItemStack itemstack = player.getHeldItem(hand);
            if (player.capabilities.isCreativeMode) {
                if (!itemstack.isEmpty()) {
                    if (itemstack.getItem() instanceof ItemNPCEditor) {
                        this.openRelearnerGui(player);
                    } else if (cost.isEmpty() || cost.getItem() != itemstack.getItem() || cost.getItemDamage() != itemstack.getItemDamage()) {
                        cost = new ItemStack(itemstack.getItem());
                        cost.setItemDamage(itemstack.getItemDamage());
                        this.setCost(cost);
                        ChatHandler.sendChat(player, I18n.translateToLocal("pixelmon.npc.relearnersetitem"), cost.getDisplayName(), cost.getCount());
                    } else if (cost.getCount() != cost.getMaxStackSize()) {
                        cost.grow(1);
                        this.setCost(cost);
                        ChatHandler.sendChat(player, I18n.translateToLocal("pixelmon.npc.relearnersetitem"), cost.getDisplayName(), cost.getCount());
                    } else {
                        ChatHandler.sendChat(player, I18n.translateToLocal("pixelmon.npc.relearnerfull"), new Object[0]);
                    }
                } else if (!cost.isEmpty()) {
                    cost.shrink(1);
                    if (cost.getCount() <= 0) {
                        cost = ItemStack.EMPTY;
                        ChatHandler.sendChat(player, I18n.translateToLocal("pixelmon.npc.relearnernoitem"), new Object[0]);
                        this.setCost(cost);
                    } else {
                        this.setCost(cost);
                        ChatHandler.sendChat(player, I18n.translateToLocal("pixelmon.npc.relearnersetitem"), cost.getDisplayName(), cost.getCount());
                    }
                }
            } else if (!cost.isEmpty()) {
                if (!itemstack.isEmpty() && itemstack.getItem() == cost.getItem() && itemstack.getCount() >= cost.getCount() && itemstack.getItemDamage() == cost.getItemDamage()) {
                    this.openRelearnerGui(player);
                } else {
                    ChatHandler.sendChat(player, I18n.translateToLocal("pixelmon.npc.relearnercost"), cost.getDisplayName(), cost.getCount());
                }
            } else {
                this.openRelearnerGui(player);
            }
        } else {
            this.tasks.taskEntries.clear();
        }
        return true;
    }

    public void openRelearnerGui(EntityPlayer player) {
        Pixelmon.NETWORK.sendTo(new InteractNPC(this.getId(), EnumNPCType.Relearner), (EntityPlayerMP)player);
        player.openGui(Pixelmon.INSTANCE, EnumGui.ChooseRelearnMove.getIndex(), player.world, 0, 0, 0);
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        ItemStack cost = this.getCost();
        if (!cost.isEmpty()) {
            nbt.setInteger("Cost", Item.getIdFromItem(!this.dataManager.get(COST).isEmpty() ? this.dataManager.get(COST).getItem() : Items.AIR));
            nbt.setInteger("costNum", cost.getCount());
            nbt.setInteger("CostDamage", cost.getItemDamage());
        }
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        if (nbt.hasKey("Cost")) {
            ItemStack cost = new ItemStack(Item.getItemById(nbt.getInteger("Cost")), nbt.getInteger("costNum"));
            cost.setItemDamage(nbt.getInteger("CostDamage"));
            this.setCost(cost);
        }
    }

    @Override
    public void initAI() {
        this.tasks.taskEntries.clear();
        this.tasks.addTask(0, new EntityAISwimming(this));
        switch (this.getAIMode()) {
            case StandStill: {
                this.tasks.addTask(1, new EntityAIWatchClosest(this, EntityPlayer.class, 10.0f));
                this.tasks.addTask(2, new EntityAIWatchClosest(this, EntityPixelmon.class, 6.0f));
                break;
            }
            case Wander: {
                this.tasks.addTask(1, new EntityAIWander(this, SharedMonsterAttributes.MOVEMENT_SPEED.getDefaultValue()));
                break;
            }
        }
    }

    @Override
    public void addVelocity(double par1, double par3, double par5) {
        if (this.canBePushed()) {
            super.addVelocity(par1, par3, par5);
        }
    }

    @Override
    public boolean canBePushed() {
        return this.getAIMode() != EnumTrainerAI.StandStill;
    }

    @Override
    protected boolean canDespawn() {
        return this.isSetup() && this.getAIMode() == EnumTrainerAI.Wander;
    }
}

