/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.apache.commons.lang3.ArrayUtils
 */
package com.pixelmongenerations.common.entity.npcs;

import com.pixelmongenerations.api.events.npc.BeatTrainerEvent;
import com.pixelmongenerations.api.events.npc.LostToTrainerEvent;
import com.pixelmongenerations.api.events.npc.TrainerAISetupEvent;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.basic.HiddenPower;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.rules.BattleRules;
import com.pixelmongenerations.common.battle.rules.clauses.BattleClauseRegistry;
import com.pixelmongenerations.common.entity.ai.AIExecuteAction;
import com.pixelmongenerations.common.entity.ai.AIMoveTowardsTarget;
import com.pixelmongenerations.common.entity.ai.AITargetNearest;
import com.pixelmongenerations.common.entity.ai.AITrainerInBattle;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.common.entity.npcs.registry.BaseShopItem;
import com.pixelmongenerations.common.entity.npcs.registry.BaseTrainer;
import com.pixelmongenerations.common.entity.npcs.registry.GymNPCData;
import com.pixelmongenerations.common.entity.npcs.registry.ITrainerData;
import com.pixelmongenerations.common.entity.npcs.registry.NPCRegistryTrainers;
import com.pixelmongenerations.common.entity.npcs.registry.PokemonForm;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.entity.npcs.registry.TrainerChat;
import com.pixelmongenerations.common.entity.npcs.registry.TrainerData;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemQueryList;
import com.pixelmongenerations.common.entity.pixelmon.drops.DroppedItem;
import com.pixelmongenerations.common.entity.pixelmon.stats.FriendShip;
import com.pixelmongenerations.common.entity.pixelmon.stats.Level;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.item.ItemNPCEditor;
import com.pixelmongenerations.common.world.gen.structure.gyms.GymInfo;
import com.pixelmongenerations.common.world.gen.structure.gyms.MovesetDefinition;
import com.pixelmongenerations.common.world.gen.structure.gyms.PokemonDefinition;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumEncounterMode;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumTrainerAI;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.EnumBattleAIMode;
import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.SetTrainerData;
import com.pixelmongenerations.core.network.packetHandlers.ClearTrainerPokemon;
import com.pixelmongenerations.core.network.packetHandlers.battles.rules.UpdateClientRules;
import com.pixelmongenerations.core.network.packetHandlers.npc.SetNPCEditData;
import com.pixelmongenerations.core.network.packetHandlers.npc.StoreTrainerPokemon;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import org.apache.commons.lang3.ArrayUtils;

public class NPCTrainer
extends EntityNPC {
    private static final DataParameter<Integer> dwEncounterMode = EntityDataManager.createKey(NPCTrainer.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> dwBossMode = EntityDataManager.createKey(NPCTrainer.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> dwTrainerLevel = EntityDataManager.createKey(NPCTrainer.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> dwBattleAI = EntityDataManager.createKey(NPCTrainer.class, DataSerializers.VARINT);
    private static final DataParameter<Integer> dwAggroRange = EntityDataManager.createKey(NPCTrainer.class, DataSerializers.VARINT);
    private PlayerStorage pokemonStorage = new PlayerStorage(this);
    boolean usingDefaultName = true;
    boolean usingDefaultGreeting = true;
    boolean usingDefaultWin = true;
    boolean usingDefaultLose = true;
    private String greeting = "";
    private String winMessage = "";
    private String loseMessage = "";
    private int winMoney;
    private int level;
    private String trainerId = "";
    private ItemStack[] winnings = new ItemStack[0];
    private boolean startRotationSet = false;
    private float startRotationYaw;
    public boolean isGymLeader = false;
    public HashMap<UUID, Long> playerEncounters = new HashMap();
    public ArrayList<String> winCommands = new ArrayList();
    public ArrayList<String> loseCommands = new ArrayList();
    public ArrayList<String> forfeitCommands = new ArrayList();
    public ArrayList<String> preBattleCommands = new ArrayList();
    public BattleRules battleRules = new BattleRules();
    public BattleControllerBase battleController;
    public int aggroRange;

    public NPCTrainer(World par1World) {
        super(par1World);
        this.dataManager.register(dwEncounterMode, 0);
        this.dataManager.register(dwBossMode, 0);
        this.dataManager.register(dwTrainerLevel, 0);
        this.dataManager.register(dwBattleAI, 0);
        this.dataManager.register(dwAggroRange, -1);
    }

    public void init(BaseTrainer trainer) {
        super.init(trainer.name);
        this.pokemonStorage = new PlayerStorage(this);
        TrainerData info = ServerNPCRegistry.trainers.getRandomData(trainer);
        if (info == null) {
            BaseTrainer b = ServerNPCRegistry.trainers.getRandomBaseWithData();
            info = ServerNPCRegistry.trainers.getRandomData(b.name);
        }
        this.chatIndex = info.getRandomChat();
        if (this.usingDefaultName) {
            this.setName("" + info.getRandomName());
        }
        this.trainerId = info.id;
        this.winMoney = info.winnings;
        this.level = info.getRandomLevel();
        if (this.level == 0) {
            this.level = 1;
        }
        this.dataManager.set(dwTrainerLevel, this.level);
        this.setBaseTrainer(info.trainerType);
        if (info.trainerType.textures.size() > 1) {
            this.setTextureIndex(this.world.rand.nextInt(info.trainerType.textures.size()));
        }
        this.loadPokemon(info.getRandomParty());
    }

    public void init(GymNPCData trainer, GymInfo info, int tier) {
        super.init(trainer.id);
        this.pokemonStorage = new PlayerStorage(this);
        this.chatIndex = trainer.getRandomTrainerChatIndex();
        this.setName("" + trainer.getRandomNameIndex());
        this.trainerId = trainer.id;
        this.winMoney = trainer.winnings;
        this.level = info.level;
        if (this.level > 0) {
            this.level = Math.min(PixelmonServerConfig.maxLevel, Math.max(1, this.level - tier));
        } else {
            this.setBossMode(EnumBossMode.Equal);
            this.level = 100;
        }
        this.dataManager.set(dwTrainerLevel, this.level);
        this.setBaseTrainer(NPCRegistryTrainers.Steve);
        if (trainer.textures.size() > 0) {
            this.setCustomSteveTexture(trainer.getRandomTexture().replaceAll(".png", ""));
            this.setTextureIndex(4);
        }
        int numPokemon = 6;
        if (tier > 0) {
            numPokemon = 1 + this.world.rand.nextInt(numPokemon - tier);
            this.setBattleAIMode(PixelmonConfig.battleAITrainer);
        } else {
            this.setBattleAIMode(EnumBattleAIMode.Advanced);
            this.isGymLeader = true;
        }
        int tries = 0;
        while (this.pokemonStorage.count() < numPokemon && tries++ < 100) {
            PokemonDefinition p = RandomHelper.getRandomElementFromList(info.pokemon);
            this.initializePokemon(p, this.level, false);
        }
        if (this.pokemonStorage.count() == 0) {
            this.setDead();
        }
    }

    public String getName(String langCode) {
        if (this.usingDefaultName) {
            try {
                int index = Integer.parseInt(this.getName());
                return this.getTranslatedData(langCode).getName(index);
            }
            catch (NumberFormatException e) {
                return this.getName();
            }
        }
        return this.getName();
    }

    public String getGreeting(String langCode) {
        if (this.usingDefaultGreeting) {
            return this.getChat((String)langCode).opening;
        }
        return this.greeting;
    }

    public String getWinMessage(String langCode) {
        if (this.usingDefaultWin) {
            return this.getChat((String)langCode).win;
        }
        return this.winMessage;
    }

    public String getLoseMessage(String langCode) {
        if (this.usingDefaultLose) {
            return this.getChat((String)langCode).lose;
        }
        return this.loseMessage;
    }

    private ITrainerData getTranslatedData(String langCode) {
        return ServerNPCRegistry.getTranslatedData(langCode, this.getBaseTrainer(), this.trainerId);
    }

    private TrainerChat getChat(String langCode) {
        ITrainerData data = this.getTranslatedData(langCode);
        if (data == null) {
            return new TrainerChat("", "", "");
        }
        return data.getChat(this.chatIndex);
    }

    public int getWinMoney() {
        return this.winMoney;
    }

    public void setEncounterMode(EnumEncounterMode mode) {
        this.dataManager.set(dwEncounterMode, mode.ordinal());
    }

    public EnumEncounterMode getEncounterMode() {
        return EnumEncounterMode.getFromIndex(this.dataManager.get(dwEncounterMode));
    }

    public void setTrainerType(BaseTrainer model, EntityPlayer player) {
        this.init(model);
        this.setTextureIndex(0);
        this.setBaseTrainer(model);
        EntityPlayerMP playerMP = (EntityPlayerMP)player;
        String loc = playerMP.language;
        this.setName(ServerNPCRegistry.trainers.getTranslatedRandomName(loc, model, this.trainerId));
        SetTrainerData p = new SetTrainerData(this, loc);
        Pixelmon.NETWORK.sendTo(new SetNPCEditData(p), playerMP);
    }

    @Override
    public void updateLeashedState() {
        if (this.getLeashed()) {
            this.clearLeashed(true, true);
        }
    }

    @Override
    public void addVelocity(double par1, double par3, double par5) {
        if (this.canBePushed()) {
            super.addVelocity(par1, par3, par5);
        }
    }

    @Override
    public boolean canBeCollidedWith() {
        return true;
    }

    @Override
    public boolean canBePushed() {
        EnumTrainerAI ai = this.getAIMode();
        return ai != EnumTrainerAI.StandStill && ai != EnumTrainerAI.StillAndEngage;
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        if (this.startRotationSet && this.getAIMode() == EnumTrainerAI.StillAndEngage) {
            this.rotationYaw = this.startRotationYaw;
            this.rotationYawHead = this.startRotationYaw;
        }
    }

    @Override
    protected void checkForRarityDespawn() {
        if (this.battleController != null) {
            return;
        }
        super.checkForRarityDespawn();
    }

    public EntityPixelmon releasePokemon(int[] newPokemonID) {
        Optional<EntityPixelmon> optional = this.pokemonStorage.getAlreadyExists(newPokemonID, this.world);
        EntityPixelmon p = optional.orElseGet(() -> this.pokemonStorage.sendOut(newPokemonID, this.world));
        if (p != null && !p.isEgg) {
            if (this.battleController == null || !this.battleController.simulateMode) {
                p.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw, 0.0f);
            } else {
                p.setPosition(0.0, 0.0, 0.0);
            }
            this.motionX = 0.0;
            this.motionY = 0.0;
            this.motionZ = 0.0;
            if (!optional.isPresent()) {
                p.releaseFromPokeball();
            }
        }
        return p;
    }

    public EntityPixelmon[] releasePokemon(int numPokemon) {
        EntityPixelmon[] pokemon;
        for (EntityPixelmon p : pokemon = this.pokemonStorage.getAmountAblePokemon(this.world, numPokemon)) {
            if (p == null || p.isEgg || this.pokemonStorage.entityAlreadyExists(p)) continue;
            p.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw, 0.0f);
            this.motionX = 0.0;
            this.motionY = 0.0;
            this.motionZ = 0.0;
            p.releaseFromPokeball();
        }
        return pokemon;
    }

    public void loadPokemon(ArrayList<PokemonForm> pokemonList) {
        for (int i = 0; i < 6; ++i) {
            this.pokemonStorage.getList()[i] = null;
        }
        if (pokemonList == null || pokemonList.isEmpty()) {
            this.initializePokemon(EnumSpecies.Rattata.name, 5);
        } else {
            for (PokemonForm pokemon : pokemonList) {
                this.initializePokemon(pokemon.pokemon.name, this.level, pokemon.form);
            }
        }
    }

    private void initializePokemon(String name, int level) {
        this.initializePokemon(name, level, 0);
    }

    private void initializePokemon(String name, int level, int form) {
        EntityPixelmon p = (EntityPixelmon)PixelmonEntityList.createEntityByName(name, this.world);
        if (p != null) {
            p.getLvl().setLevel(Math.max(1, RandomHelper.getRandomNumberBetween(level - 1, level + 1)));
            p.setHealth(p.stats.HP);
            p.setTrainer(this);
            p.setNickname(Entity1Base.getLocalizedName(name));
            p.setForm(form, false);
            this.pokemonStorage.addToParty(p);
        }
    }

    private void initializePokemon(PokemonDefinition definition, int level, boolean isDouble) {
        MovesetDefinition moves = RandomHelper.getRandomElementFromList(definition.movesets);
        if (level < moves.minLevel || level < definition.minLevel || level > definition.maxLevel) {
            return;
        }
        if (moves.doubleOnly && !isDouble) {
            return;
        }
        EntityPixelmon p = (EntityPixelmon)PixelmonEntityList.createEntityByName(definition.pokemon.name, this.world);
        if (p != null) {
            BaseShopItem item;
            Level pLevel = p.getLvl();
            pLevel.setLevel(Math.max(1, Math.min(PixelmonServerConfig.maxLevel, RandomHelper.getRandomNumberBetween(level - 1, level + 1))));
            p.setTrainer(this);
            if (moves.form > -1) {
                p.setForm(moves.form);
            }
            if (this.isGymLeader) {
                p.stats.EVs.Attack = moves.evAtk;
                p.stats.EVs.Defence = moves.evDef;
                p.stats.EVs.HP = moves.evHP;
                p.stats.EVs.SpecialAttack = moves.evSpAtk;
                p.stats.EVs.SpecialDefence = moves.evSpDef;
                p.stats.EVs.Speed = moves.evSpeed;
                p.stats.IVs.Attack = moves.ivAtk;
                p.stats.IVs.Defence = moves.ivDef;
                p.stats.IVs.HP = moves.ivHP;
                p.stats.IVs.SpAtt = moves.ivSpAtk;
                p.stats.IVs.SpDef = moves.ivSpDef;
                p.stats.IVs.Speed = moves.ivSpeed;
                if (moves.nature != null && moves.nature.length > 0) {
                    p.setNature(RandomHelper.getRandomElementFromArray(moves.nature));
                }
            }
            p.stats.setLevelStats(p.getNature(), p.getNature(), p.baseStats, pLevel.getLevel());
            p.setHealth(p.stats.HP);
            if (moves.ability != null && moves.ability.length > 0) {
                p.setAbility(RandomHelper.getRandomElementFromArray(moves.ability));
            } else {
                p.setRandomAbilityUniform();
            }
            if (moves.heldItem != null && moves.heldItem.length > 0 && (item = ServerNPCRegistry.shopkeepers.getItem(RandomHelper.getRandomElementFromArray(moves.heldItem))) != null) {
                item.getItem().setCount(1);
                p.setHeldItem(item.getItem().copy());
            }
            p.friendship.setFriendship(FriendShip.getMaxFriendship());
            Moveset moveset = p.getMoveset();
            moveset.clear();
            this.addGymTrainerMove(moves.move1, p, moves.ivsDefined);
            this.addGymTrainerMove(moves.move2, p, moves.ivsDefined);
            this.addGymTrainerMove(moves.move3, p, moves.ivsDefined);
            this.addGymTrainerMove(moves.move4, p, moves.ivsDefined);
            this.pokemonStorage.addToParty(p);
            int pos = this.pokemonStorage.getPosition(p.getPokemonId());
            if (moves.lead && pos != 0) {
                NBTTagCompound other = this.pokemonStorage.partyPokemon[0];
                this.pokemonStorage.partyPokemon[0] = this.pokemonStorage.partyPokemon[pos];
                this.pokemonStorage.partyPokemon[pos] = other;
            }
        }
    }

    private void addGymTrainerMove(String[] possibleMoves, EntityPixelmon p, boolean ivsDefined) {
        if (possibleMoves != null && possibleMoves.length > 0) {
            Attack attack;
            Moveset moveset = p.getMoveset();
            int randomIndex = this.world.rand.nextInt(possibleMoves.length);
            String selectedMove = possibleMoves[randomIndex];
            EnumType hiddenPowerType = null;
            if (selectedMove.contains("Hidden Power")) {
                if (!ivsDefined) {
                    hiddenPowerType = EnumType.parseType(selectedMove.replace("Hidden Power ", ""));
                }
                selectedMove = "Hidden Power";
            }
            if ((attack = DatabaseMoves.getAttack(selectedMove)) != null && !moveset.contains(attack)) {
                moveset.add(attack);
                if (hiddenPowerType != null) {
                    p.stats.IVs.copyIVs(HiddenPower.getOptimalIVs(hiddenPowerType));
                }
            } else {
                String[] reducedPossible = (String[])ArrayUtils.remove((Object[])possibleMoves, (int)randomIndex);
                this.addGymTrainerMove(reducedPossible, p, ivsDefined);
            }
        }
    }

    public void startBattle(BattleParticipant battleParticipant) {
        if (battleParticipant instanceof PlayerParticipant) {
            PlayerParticipant player = (PlayerParticipant)battleParticipant;
            EnumEncounterMode encounterMode = this.getEncounterMode();
            if (encounterMode == EnumEncounterMode.OncePerMCDay) {
                this.playerEncounters.put(player.player.getUniqueID(), this.world.getTotalWorldTime());
            } else if (encounterMode == EnumEncounterMode.OncePerDay) {
                this.playerEncounters.put(player.player.getUniqueID(), System.currentTimeMillis());
            }
            String loc = ((EntityPlayerMP)player.getEntity()).language;
            if (this.getGreeting(loc) != null) {
                ChatHandler.sendBattleMessage(player.player, this.getName(loc) + ": " + this.getGreeting(loc), new Object[0]);
            }
        }
        this.healAllPokemon();
    }

    public void loseBattle(ArrayList<BattleParticipant> opponents) {
        String langCode;
        if (opponents.get(0) instanceof PlayerParticipant && this.getLoseMessage(langCode = ((EntityPlayerMP)opponents.get((int)0).getEntity()).language) != null) {
            ChatHandler.sendBattleMessage(opponents, this.getLoseMessage(langCode), new Object[0]);
        }
        if (opponents.size() == 1 && opponents.get(0) instanceof PlayerParticipant) {
            EntityPlayerMP player = (EntityPlayerMP)opponents.get(0).getEntity();
            BeatTrainerEvent event = new BeatTrainerEvent(player, this);
            MinecraftForge.EVENT_BUS.post(event);
            if (this.getEncounterMode() == EnumEncounterMode.Once) {
                this.despawnEntity();
                this.setDead();
            } else {
                this.healAllPokemon();
                if (this.getEncounterMode() == EnumEncounterMode.OncePerPlayer) {
                    this.playerEncounters.put(player.getUniqueID(), this.world.getTotalWorldTime());
                }
            }
            int money = event.getWinnings();
            if (money > 0) {
                int calculatedWinMoney = money * this.pokemonStorage.getAveragePartyLevel();
                if (!this.canDespawn()) {
                    calculatedWinMoney = money;
                }
                PlayerParticipant playerParticipant = (PlayerParticipant)opponents.get(0);
                calculatedWinMoney *= playerParticipant.getPrizeMoneyMultiplier();
                Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
                if (optstorage.isPresent()) {
                    optstorage.get().addCurrency(calculatedWinMoney);
                }
                if (playerParticipant.bc == null) {
                    ChatHandler.sendFormattedChat(player, TextFormatting.GREEN, "pixelmon.entitytrainer.winnings", "" + calculatedWinMoney, this.getName(player.language));
                } else {
                    ChatHandler.sendBattleMessage(playerParticipant.getEntity(), "pixelmon.entitytrainer.winnings", "" + calculatedWinMoney, this.getName(player.language));
                }
            }
            if (this.winnings.length > 0) {
                ArrayList<DroppedItem> drops = new ArrayList<DroppedItem>();
                int id = 0;
                for (ItemStack item : this.winnings) {
                    if (item.getCount() == 0) {
                        item.setCount(1);
                    }
                    drops.add(new DroppedItem(item.copy(), id++));
                }
                DropItemQueryList.register(this, drops, player);
            }
            if (this.getBaseTrainer() != null && this.getBaseTrainer().name.equals("Fisherman")) {
                int number = RandomHelper.getRandomNumberBetween(1, 100);
                int number2 = RandomHelper.getRandomNumberBetween(1, 1000);
                if (number == 43) {
                    DropItemHelper.giveItemStackToPlayer(player, new ItemStack(PixelmonItems.goodRod));
                    ChatHandler.sendFormattedChat(player, TextFormatting.GREEN, "pixelmon.entitytrainer.goodrod", new Object[0]);
                } else if (number2 == 564) {
                    DropItemHelper.giveItemStackToPlayer(player, new ItemStack(PixelmonItems.superRod));
                    ChatHandler.sendFormattedChat(player, TextFormatting.GREEN, "pixelmon.entitytrainer.superrod", new Object[0]);
                }
            }
        }
    }

    public void winBattle(ArrayList<BattleParticipant> opponents) {
        String langCode;
        if (opponents.get(0) instanceof PlayerParticipant && this.getWinMessage(langCode = ((EntityPlayerMP)opponents.get((int)0).getEntity()).language) != null) {
            ChatHandler.sendBattleMessage(opponents, this.getWinMessage(langCode), new Object[0]);
        }
        if (opponents.size() == 1 && opponents.get(0) instanceof PlayerParticipant) {
            EntityPlayerMP player = (EntityPlayerMP)opponents.get(0).getEntity();
            MinecraftForge.EVENT_BUS.post(new LostToTrainerEvent(player, this));
        }
    }

    public void retrievePokemon(EntityPixelmon releasedPokemon) {
        if (releasedPokemon != null) {
            releasedPokemon.unloadEntity();
        }
    }

    public void healAllPokemon() {
        this.pokemonStorage.healAllPokemon(this.world);
    }

    public int[] getNextPokemonID() {
        int[] arrn;
        EntityPixelmon p = this.pokemonStorage.getFirstAblePokemon(this.world);
        if (p == null) {
            int[] arrn2 = new int[2];
            arrn2[0] = -1;
            arrn = arrn2;
            arrn2[1] = -1;
        } else {
            arrn = p.getPokemonId();
        }
        return arrn;
    }

    public int getLvl() {
        return this.dataManager.get(dwTrainerLevel);
    }

    @Override
    public boolean interactWithNPC(EntityPlayer player, EnumHand hand) {
        return false;
    }

    @Override
    protected boolean processInteract(EntityPlayer player, EnumHand hand) {
        ItemStack stack = player.getHeldItem(hand);
        if (player instanceof EntityPlayerMP) {
            if (stack.getItem() instanceof ItemNPCEditor) {
                if (!this.checkOP(player)) {
                    return false;
                }
                this.ignoreDespawnCounter = true;
                this.tasks.taskEntries.clear();
                EntityPlayerMP playerMP = (EntityPlayerMP)player;
                Pixelmon.NETWORK.sendTo(new ClearTrainerPokemon(), playerMP);
                for (int i = 0; i < this.pokemonStorage.count(); ++i) {
                    Pixelmon.NETWORK.sendTo(new StoreTrainerPokemon(new PixelmonData(this.pokemonStorage.getList()[i])), playerMP);
                }
                String loc = playerMP.language;
                SetTrainerData p = new SetTrainerData(this, loc);
                Pixelmon.NETWORK.sendTo(new SetNPCEditData(p), playerMP);
                if (BattleClauseRegistry.getClauseVersion() > 0) {
                    Pixelmon.NETWORK.sendTo(new UpdateClientRules(), playerMP);
                }
                player.openGui(Pixelmon.INSTANCE, EnumGui.TrainerEditor.getIndex(), player.world, this.getId(), 0, 0);
            }
        } else {
            this.tasks.taskEntries.clear();
        }
        return super.processInteract(player, hand);
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        NBTTagCompound pokemonNbt = new NBTTagCompound();
        this.pokemonStorage.writeToNBT(pokemonNbt);
        nbt.setTag("pokeStore", pokemonNbt);
        nbt.setShort("BossMode", (short)this.getBossMode().index);
        nbt.setString("Greeting", this.greeting);
        nbt.setString("WinMessage", this.winMessage);
        nbt.setString("LoseMessage", this.loseMessage);
        NBTTagCompound tmpWinnings = new NBTTagCompound();
        for (int i = 0; i < this.winnings.length; ++i) {
            NBTTagCompound itemTag = new NBTTagCompound();
            if (this.winnings[i] == null) continue;
            this.winnings[i].writeToNBT(itemTag);
            tmpWinnings.setTag("item" + i, itemTag);
        }
        nbt.setInteger("NPCLevel", this.level);
        nbt.setInteger("WinMoney", this.winMoney);
        nbt.setTag("WinningsTag", tmpWinnings);
        EnumEncounterMode mode = this.getEncounterMode();
        nbt.setShort("EncMode", (short)mode.ordinal());
        if (mode != EnumEncounterMode.Once && mode != EnumEncounterMode.Unlimited) {
            NBTTagList list = new NBTTagList();
            for (Map.Entry<UUID, Long> entry : this.playerEncounters.entrySet()) {
                NBTTagCompound nBTTagCompound = new NBTTagCompound();
                nBTTagCompound.setUniqueId("UUID", entry.getKey());
                if (mode != EnumEncounterMode.OncePerPlayer) {
                    nBTTagCompound.setLong("time", entry.getValue());
                }
                list.appendTag(nBTTagCompound);
            }
            nbt.setTag("Encounters", list);
        }
        nbt.setShort("BattleAIMode", (short)this.getBattleAIMode().ordinal());
        nbt.setBoolean("DefaultName", this.usingDefaultName);
        nbt.setBoolean("DefaultWin", this.usingDefaultWin);
        nbt.setBoolean("DefaultLose", this.usingDefaultLose);
        nbt.setBoolean("DefaultGreet", this.usingDefaultGreeting);
        nbt.setString("TrainerIndex", this.trainerId);
        nbt.setInteger("ChatIndex", this.chatIndex);
        if (this.getAIMode() == EnumTrainerAI.StillAndEngage) {
            nbt.setFloat("TrainerRotation", this.startRotationYaw);
        }
        nbt.setBoolean("GymLeader", this.isGymLeader);
        this.battleRules.writeToNBT(nbt);
        NBTTagCompound commandsNbt = nbt.getCompoundTag("Commands");
        NBTTagList winList = new NBTTagList();
        for (String string : this.winCommands) {
            winList.appendTag(new NBTTagString(string));
        }
        commandsNbt.setTag("winCommands", winList);
        NBTTagList nBTTagList = new NBTTagList();
        for (String string : this.loseCommands) {
            nBTTagList.appendTag(new NBTTagString(string));
        }
        commandsNbt.setTag("loseCommands", nBTTagList);
        NBTTagList nBTTagList2 = new NBTTagList();
        for (String forfeitCommand : this.forfeitCommands) {
            nBTTagList2.appendTag(new NBTTagString(forfeitCommand));
        }
        commandsNbt.setTag("forfeitCommands", nBTTagList2);
        NBTTagList nBTTagList3 = new NBTTagList();
        for (String preBattleCommand : this.preBattleCommands) {
            nBTTagList3.appendTag(new NBTTagString(preBattleCommand));
        }
        commandsNbt.setTag("preBattleCommands", nBTTagList3);
        nbt.setTag("Commands", commandsNbt);
        nbt.setInteger("AggroRange", this.aggroRange == 0 ? PixelmonServerConfig.defaultNPCTrainerAggroRange : this.aggroRange);
    }

    public boolean canStartBattle(EntityPlayer opponent, boolean printMessages) {
        Long lastEncounter = this.playerEncounters.get(opponent.getUniqueID());
        if (lastEncounter != null) {
            EnumEncounterMode mode = this.getEncounterMode();
            if (mode == EnumEncounterMode.OncePerDay) {
                long curTime = System.currentTimeMillis();
                long curDay = curTime / 86400000L;
                if (curDay <= lastEncounter / 86400000L) {
                    if (printMessages) {
                        ChatHandler.sendChat(opponent, "pixelmon.entitytrainer.onceday", new Object[0]);
                    }
                    return false;
                }
            } else if (mode == EnumEncounterMode.OncePerMCDay) {
                long curTime = this.world.getTotalWorldTime();
                long curDay = curTime / 24000L;
                if (curDay <= lastEncounter / 24000L) {
                    if (printMessages) {
                        ChatHandler.sendChat(opponent, "pixelmon.entitytrainer.oncemcday", new Object[0]);
                    }
                    return false;
                }
            } else if (mode == EnumEncounterMode.OncePerPlayer) {
                if (printMessages) {
                    ChatHandler.sendChat(opponent, "pixelmon.entitytrainer.onceplayer", new Object[0]);
                }
                return false;
            }
        }
        return true;
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        if (nbt.hasKey("DefaultName")) {
            this.usingDefaultName = nbt.getBoolean("DefaultName");
            this.usingDefaultWin = nbt.getBoolean("DefaultWin");
            this.usingDefaultLose = nbt.getBoolean("DefaultLose");
            this.usingDefaultGreeting = nbt.getBoolean("DefaultGreet");
            this.trainerId = nbt.getString("TrainerIndex");
            this.chatIndex = nbt.getInteger("ChatIndex");
        }
        super.readEntityFromNBT(nbt);
        if (nbt.hasKey("BossMode")) {
            this.setBossMode(EnumBossMode.getMode(nbt.getShort("BossMode")));
        }
        this.pokemonStorage.readFromNBT(nbt.getCompoundTag("pokeStore"));
        if (nbt.hasKey("Greeting")) {
            this.greeting = nbt.getString("Greeting");
            this.winMessage = nbt.getString("WinMessage");
            this.loseMessage = nbt.getString("LoseMessage");
        }
        if (nbt.hasKey("WinningsTag")) {
            NBTTagCompound tag = nbt.getCompoundTag("WinningsTag");
            this.winnings = new ItemStack[tag.getKeySet().size()];
            int index = 0;
            for (String key : tag.getKeySet()) {
                this.winnings[index++] = new ItemStack(tag.getCompoundTag(key));
            }
        } else if (nbt.hasKey("Winnings")) {
            int[] testArray = nbt.getIntArray("Winnings");
            ArrayList<Item> array = new ArrayList<Item>();
            for (int aTestArray : testArray) {
                array.add(Item.getItemById(aTestArray));
            }
            this.winnings = new ItemStack[array.size()];
            for (int i = 0; i < array.size(); ++i) {
                this.winnings[i] = new ItemStack((Item)array.get(i));
            }
        }
        if (nbt.hasKey("TrainerRotation")) {
            this.setStartRotationYaw(nbt.getFloat("TrainerRotation"));
        }
        if (nbt.hasKey("EncMode")) {
            EnumEncounterMode mode = EnumEncounterMode.getFromIndex(nbt.getShort("EncMode"));
            this.setEncounterMode(mode);
            this.playerEncounters.clear();
            if (mode != EnumEncounterMode.Once && mode != EnumEncounterMode.Unlimited && nbt.hasKey("numEncounters")) {
                int numEncounters = nbt.getInteger("numEncounters");
                for (int i = 0; i < numEncounters; ++i) {
                    String uuid = nbt.getString("encPl" + i);
                    if (mode != EnumEncounterMode.OncePerPlayer) {
                        long curDay;
                        long time = nbt.getLong("encTi" + i);
                        if (mode == EnumEncounterMode.OncePerDay) {
                            curDay = System.currentTimeMillis() / 86400000L;
                            if (curDay > time / 86400000L) continue;
                            this.playerEncounters.put(UUID.fromString(uuid), time);
                            continue;
                        }
                        if (mode != EnumEncounterMode.OncePerMCDay || (curDay = this.world.getTotalWorldTime() / 24000L) > time / 24000L) continue;
                        this.playerEncounters.put(UUID.fromString(uuid), time);
                        continue;
                    }
                    this.playerEncounters.put(UUID.fromString(uuid), 0L);
                }
            }
            if (mode != EnumEncounterMode.Once && mode != EnumEncounterMode.Unlimited && nbt.hasKey("Encounters")) {
                NBTTagList list = nbt.getTagList("Encounters", 10);
                for (int i = 0; i < list.tagCount(); ++i) {
                    long time;
                    NBTTagCompound compound = list.getCompoundTagAt(i);
                    UUID uuid = compound.getUniqueId("UUID");
                    long l = time = mode != EnumEncounterMode.OncePerPlayer ? compound.getLong("time") : 0L;
                    if (mode == EnumEncounterMode.OncePerDay) {
                        long curDay = System.currentTimeMillis() / 86400000L;
                        if (curDay > time / 86400000L) continue;
                        this.playerEncounters.put(uuid, time);
                        continue;
                    }
                    if (mode == EnumEncounterMode.OncePerMCDay) {
                        long curDay = this.world.getTotalWorldTime() / 24000L;
                        if (curDay > time / 24000L) continue;
                        this.playerEncounters.put(uuid, time);
                        continue;
                    }
                    this.playerEncounters.put(uuid, 0L);
                }
            }
        } else {
            this.setEncounterMode(EnumEncounterMode.Once);
        }
        if (nbt.hasKey("BattleAIMode")) {
            this.setBattleAIMode(EnumBattleAIMode.getFromIndex(nbt.getShort("BattleAIMode")));
        }
        if (nbt.hasKey("NPCLevel")) {
            this.level = nbt.getInteger("NPCLevel");
            this.dataManager.set(dwTrainerLevel, this.level);
            this.updateLvl();
        }
        if (nbt.hasKey("Commands")) {
            NBTTagCompound cmdNbt = nbt.getCompoundTag("Commands");
            if (cmdNbt.hasKey("winCommands")) {
                NBTTagList wins = cmdNbt.getTagList("winCommands", 8);
                for (int i = 0; i < wins.tagCount(); ++i) {
                    this.winCommands.add(wins.getStringTagAt(i));
                }
            }
            if (cmdNbt.hasKey("loseCommands")) {
                NBTTagList loss = cmdNbt.getTagList("loseCommands", 8);
                for (int i = 0; i < loss.tagCount(); ++i) {
                    this.loseCommands.add(loss.getStringTagAt(i));
                }
            }
            if (cmdNbt.hasKey("forfeitCommands")) {
                NBTTagList forfeit = cmdNbt.getTagList("forfeitCommands", 8);
                for (int i = 0; i < forfeit.tagCount(); ++i) {
                    this.forfeitCommands.add(forfeit.getStringTagAt(i));
                }
            }
            if (cmdNbt.hasKey("preBattleCommands")) {
                NBTTagList preBattle = cmdNbt.getTagList("preBattleCommands", 8);
                for (int i = 0; i < preBattle.tagCount(); ++i) {
                    this.preBattleCommands.add(preBattle.getStringTagAt(i));
                }
            }
        }
        if (nbt.hasKey("WinMoney")) {
            this.winMoney = nbt.getInteger("WinMoney");
        }
        if (nbt.hasKey("GymLeader")) {
            this.isGymLeader = nbt.getBoolean("GymLeader");
        }
        this.battleRules.readFromNBT(nbt);
        if (nbt.hasKey("BattleType")) {
            this.battleRules.battleType = EnumBattleType.values()[nbt.getShort("BattleType")];
            nbt.removeTag("BattleType");
        }
        this.aggroRange = nbt.hasKey("AggroRange") ? nbt.getInteger("AggroRange") : PixelmonServerConfig.defaultNPCTrainerAggroRange;
    }

    public void randomisePokemon(EntityPlayer player) {
        ArrayList<PokemonForm> randomParty;
        BaseTrainer base = this.getBaseTrainer();
        if (base.name.equals("Steve")) {
            int partySize = RandomHelper.getRandomNumberBetween(1, 6);
            randomParty = new ArrayList(partySize);
            for (int i = 0; i < partySize; ++i) {
                randomParty.add(new PokemonForm(EnumSpecies.randomPoke()));
            }
            this.level = RandomHelper.getRandomNumberBetween(2, 99);
        } else {
            TrainerData data = ServerNPCRegistry.trainers.getRandomData(base);
            randomParty = data.getRandomParty();
        }
        this.loadPokemon(randomParty);
        this.updateLvl();
        Pixelmon.NETWORK.sendTo(new ClearTrainerPokemon(), (EntityPlayerMP)player);
        for (int i = 0; i < this.pokemonStorage.count(); ++i) {
            Pixelmon.NETWORK.sendTo(new StoreTrainerPokemon(new PixelmonData(this.pokemonStorage.getList()[i])), (EntityPlayerMP)player);
        }
    }

    @Override
    public EnumBossMode getBossMode() {
        return EnumBossMode.getMode(this.dataManager.get(dwBossMode));
    }

    public void setBossMode(EnumBossMode mode) {
        this.dataManager.set(dwBossMode, mode.index);
    }

    public EnumBattleAIMode getBattleAIMode() {
        return EnumBattleAIMode.getFromIndex(this.dataManager.get(dwBattleAI));
    }

    public void setBattleAIMode(EnumBattleAIMode mode) {
        if (mode != null) {
            this.dataManager.set(dwBattleAI, mode.ordinal());
        }
    }

    public int getAggroRange() {
        return this.aggroRange;
    }

    public EnumBattleType getBattleType() {
        return this.battleRules.battleType;
    }

    @Deprecated
    public void setBattleType(EnumBattleType type) {
        this.battleRules.battleType = type;
    }

    public void updateLvl() {
        int lvlTotal = 0;
        for (int i = 0; i < this.pokemonStorage.count(); ++i) {
            lvlTotal += this.pokemonStorage.getList()[i].getInteger("Level");
        }
        float lvl = (float)lvlTotal / (float)this.pokemonStorage.count();
        this.dataManager.set(dwTrainerLevel, (int)lvl);
    }

    @Override
    public void initAI() {
        this.tasks.taskEntries.clear();
        TrainerAISetupEvent event = new TrainerAISetupEvent(this);
        MinecraftForge.EVENT_BUS.post(event);
        switch (this.getAIMode()) {
            case StandStill: {
                this.tasks.addTask(0, new EntityAISwimming(this));
                this.tasks.addTask(1, new AITrainerInBattle(this));
                this.tasks.addTask(2, new EntityAIWatchClosest(this, EntityPlayer.class, 10.0f));
                this.tasks.addTask(3, new EntityAIWatchClosest(this, EntityPixelmon.class, 6.0f));
                break;
            }
            case Wander: {
                this.tasks.addTask(0, new EntityAISwimming(this));
                this.tasks.addTask(1, new AITrainerInBattle(this));
                this.tasks.addTask(2, new EntityAIWander(this, SharedMonsterAttributes.MOVEMENT_SPEED.getDefaultValue()));
                break;
            }
            case StillAndEngage: {
                int range = this.aggroRange == 0 ? PixelmonServerConfig.defaultNPCTrainerAggroRange : this.aggroRange;
                this.tasks.addTask(0, new EntityAISwimming(this));
                this.tasks.addTask(1, new AITrainerInBattle(this));
                this.tasks.addTask(2, new AIExecuteAction(this));
                this.tasks.addTask(3, new AITargetNearest(this, (float)range, true));
                this.tasks.addTask(4, new AIMoveTowardsTarget(this, 10.0f));
                break;
            }
            case WanderAndEngage: {
                int range = this.aggroRange == 0 ? PixelmonServerConfig.defaultNPCTrainerAggroRange : this.aggroRange;
                this.tasks.addTask(0, new EntityAISwimming(this));
                this.tasks.addTask(1, new AITrainerInBattle(this));
                this.tasks.addTask(2, new EntityAIWander(this, this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getAttributeValue()));
                this.tasks.addTask(3, new EntityAIWatchClosest(this, EntityPlayer.class, 10.0f));
                this.tasks.addTask(4, new EntityAIWatchClosest(this, EntityPixelmon.class, 6.0f));
                this.tasks.addTask(5, new AIExecuteAction(this));
                this.tasks.addTask(6, new AIMoveTowardsTarget(this, 10.0f));
                this.tasks.addTask(7, new AITargetNearest(this, (float)range, true));
                break;
            }
        }
    }

    public void update(SetTrainerData p) {
        if (!p.greeting.equals("")) {
            this.greeting = p.greeting;
            this.usingDefaultGreeting = false;
        }
        if (!p.lose.equals("")) {
            this.loseMessage = p.lose;
            this.usingDefaultLose = false;
        }
        if (!p.win.equals("")) {
            this.winMessage = p.win;
            this.usingDefaultWin = false;
        }
        this.winMoney = p.winMoney;
        if (p.rules != null) {
            this.battleRules = p.rules;
        }
    }

    public void setStartRotationYaw(float f) {
        this.startRotationSet = true;
        this.startRotationYaw = f;
        this.rotationYaw = f;
        this.rotationYawHead = f;
    }

    @Override
    public String getDisplayText() {
        String s = "boss";
        if (this.getBossMode() == EnumBossMode.NotBoss) {
            s = "" + this.getLvl();
        }
        return s;
    }

    @Override
    public String getSubTitleText() {
        return I18n.translateToLocal("gui.screenpokechecker.lvl");
    }

    public void setBattleController(BattleControllerBase battleController) {
        this.battleController = battleController;
    }

    public BattleControllerBase getBattleController() {
        return this.battleController;
    }

    public PlayerStorage getPokemonStorage() {
        return this.pokemonStorage;
    }

    public EntityLiving getEntity() {
        return this;
    }

    public void setAttackTargetPix(EntityLivingBase entity) {
        this.setAttackTarget(entity);
    }

    public void updateDrops(ItemStack[] drops) {
        this.winnings = drops;
    }

    public ItemStack[] getWinnings() {
        return this.winnings;
    }

    public void setLevel(int level) {
        this.level = level;
        this.dataManager.set(dwTrainerLevel, level);
        BaseTrainer base = this.getBaseTrainer();
        TrainerData data = ServerNPCRegistry.trainers.getRandomData(base);
        this.loadPokemon(data.getRandomParty());
    }

    public void setWinMoney(int money) {
        this.winMoney = money;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public void setWinMessage(String message) {
        this.winMessage = message;
    }

    public void setLoseMessage(String message) {
        this.loseMessage = message;
    }

    public void removePlayerStatus(EntityPlayerMP player) {
        this.playerEncounters.remove(player.getUniqueID());
    }
}

