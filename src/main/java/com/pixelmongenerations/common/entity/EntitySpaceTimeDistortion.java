/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity;

import com.pixelmongenerations.api.events.SpaceTimeDistortionEvent;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import org.jetbrains.annotations.NotNull;

public class EntitySpaceTimeDistortion
extends EntityLiving {
    private int ticks = 0;
    private boolean isExpanding = true;
    private boolean eventAttempted = false;
    public boolean maxSize = false;
    public BlockPos spawnPos = this.getPosition();

    public EntitySpaceTimeDistortion(World worldIn) {
        super(worldIn);
        this.setSize(1.0f, 1.0f);
    }

    @Override
    protected boolean processInteract(@NotNull EntityPlayer player, @NotNull EnumHand hand) {
        return true;
    }

    @Override
    public boolean attackEntityFrom(@NotNull DamageSource source, float par2) {
        return false;
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
    public void addVelocity(double x, double y, double z) {
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        this.rotationYaw += 1.0f;
        if (this.isExpanding) {
            if (this.width <= 5.0f && this.height <= 5.0f) {
                ++this.ticks;
                SpaceTimeDistortionEvent.ExpandingEvent expandingEvent = new SpaceTimeDistortionEvent.ExpandingEvent(this, this.ticks);
                MinecraftForge.EVENT_BUS.post(expandingEvent);
                this.setSize(this.width + 0.01f, this.height + 0.01f);
            } else {
                if (!this.maxSize) {
                    this.maxSize = true;
                }
                if (!this.eventAttempted) {
                    this.ticks = 0;
                    this.eventAttempted = true;
                    SpaceTimeDistortionEvent.StartEvent startEvent = new SpaceTimeDistortionEvent.StartEvent(this, Pixelmon.spaceTimeDistortionSpawns.getDefaultItems(), Pixelmon.spaceTimeDistortionSpawns.getDefaultPokemon());
                    MinecraftForge.EVENT_BUS.post(startEvent);
                    if (startEvent.isCanceled()) {
                        this.setDead();
                    }
                }
                if (this.ticks >= PixelmonServerConfig.spaceTimeDistortionLifeTimer * 20) {
                    this.isExpanding = false;
                }
                ++this.ticks;
            }
        } else {
            if ((double)this.width >= 5.0 && (double)this.height >= 5.0) {
                this.ticks = 0;
            }
            if (this.maxSize) {
                this.maxSize = false;
            }
            ++this.ticks;
            SpaceTimeDistortionEvent.ShrinkingEvent shrinkingEvent = new SpaceTimeDistortionEvent.ShrinkingEvent(this, this.ticks);
            MinecraftForge.EVENT_BUS.post(shrinkingEvent);
            if (!shrinkingEvent.isCanceled()) {
                this.setSize(this.width - 0.01f, this.height - 0.01f);
                if (this.width <= 0.0f && this.height <= 0.0f) {
                    this.setDead();
                    SpaceTimeDistortionEvent.EndEvent endEvent = new SpaceTimeDistortionEvent.EndEvent(this);
                    MinecraftForge.EVENT_BUS.post(endEvent);
                }
            }
        }
    }

    @Override
    protected void collideWithEntity(@NotNull Entity entityIn) {
    }
}

