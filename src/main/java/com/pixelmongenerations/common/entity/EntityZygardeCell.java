/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity;

import com.pixelmongenerations.common.item.ItemZygardeCube;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.PixelSounds;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import org.jetbrains.annotations.NotNull;

public class EntityZygardeCell
extends EntityLiving {
    public EntityZygardeCell(World world) {
        super(world);
        this.setSize(1.0f, 1.0f);
    }

    @Override
    protected boolean processInteract(EntityPlayer player, @NotNull EnumHand hand) {
        if (player.getHeldItem(hand).getItem() == PixelmonItems.zygardeCube) {
            ItemStack stack = player.getHeldItem(hand);
            if (stack.getItemDamage() != ItemZygardeCube.full) {
                stack.setItemDamage(stack.getItemDamage() + 1);
                ChatHandler.sendChat(player, "pixelmon.zygardecube.cellAdd", new Object[0]);
                this.world.playSound(null, this.getPosition(), PixelSounds.zygardeCell, SoundCategory.BLOCKS, 0.5f, 1.0f);
                this.setDead();
                return true;
            }
            ChatHandler.sendChat(player, "pixelmon.zygardecube.cellFull", new Object[0]);
            return false;
        }
        return false;
    }

    @Override
    public boolean attackEntityFrom(@NotNull DamageSource source, float par2) {
        return false;
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
    public void addVelocity(double par1, double par3, double par5) {
        if (this.canBePushed()) {
            super.addVelocity(par1, par3, par5);
        }
    }
}

