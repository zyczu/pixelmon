/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity;

import com.pixelmongenerations.core.Pixelmon;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;
import org.jetbrains.annotations.NotNull;

public class EntityWishingStar
extends EntityLiving {
    private int ticks = 0;

    public EntityWishingStar(World world) {
        super(world);
        this.setSize(1.0f, 1.0f);
    }

    @Override
    protected boolean processInteract(@NotNull EntityPlayer player, @NotNull EnumHand hand) {
        this.setDead();
        return true;
    }

    @Override
    public boolean attackEntityFrom(@NotNull DamageSource source, float par2) {
        return false;
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        this.setRotation(0.0f, 0.0f);
        if (this.world.isRemote) {
            Pixelmon.PROXY.spawnParticle(EnumParticleTypes.SPELL_WITCH.getParticleID(), this.posX, this.posY, this.posZ, 1.0, 0.0, 0.0, 0, 0, 0);
        }
    }
}

