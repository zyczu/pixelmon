/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonObject
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

import com.google.gson.JsonObject;
import net.minecraft.block.material.Material;

public class FlyingOptions {
    public int flyHeightMin = 0;
    public int flyHeightMax = 10;
    public float flySpeedModifier = 1.0f;
    public int flyRefreshRateY = 100;
    public int flyRefreshRateXZ = 100;
    public int flyRefreshRateSpeed = 100;
    public int flightTimeMin = 0;
    public int flightTimeMax = 20;
    public int flapRate = 20;
    public String landingMaterial;
    public transient Material[] landingMat = null;

    public FlyingOptions(int heightMin, int heightMax, float speedMod, int rateY, int rateXZ, int rateSpeed, int flightTimeMin, int flightTimeMax, int flapRate, String landingMaterial) {
        Material[] arrmaterial;
        this.flyHeightMin = heightMin;
        this.flyHeightMax = heightMax;
        this.flySpeedModifier = speedMod;
        this.flyRefreshRateY = rateY;
        this.flyRefreshRateXZ = rateXZ;
        this.flyRefreshRateSpeed = rateSpeed;
        this.flightTimeMin = flightTimeMin;
        this.flightTimeMax = flightTimeMax;
        this.flapRate = flapRate / 4;
        this.landingMaterial = landingMaterial;
        if (landingMaterial.equalsIgnoreCase("grassandleaves")) {
            Material[] arrmaterial2 = new Material[2];
            arrmaterial2[0] = Material.LEAVES;
            arrmaterial = arrmaterial2;
            arrmaterial2[1] = Material.GRASS;
        } else if (landingMaterial.equalsIgnoreCase("leaves")) {
            Material[] arrmaterial3 = new Material[1];
            arrmaterial = arrmaterial3;
            arrmaterial3[0] = Material.LEAVES;
        } else {
            arrmaterial = null;
        }
        this.landingMat = arrmaterial;
    }

    public FlyingOptions(JsonObject jsonObject) {
        if (jsonObject.has("heightmin")) {
            this.flyHeightMin = jsonObject.get("heightmin").getAsInt();
        }
        if (jsonObject.has("heightmax")) {
            this.flyHeightMax = jsonObject.get("heightmax").getAsInt();
        }
        if (jsonObject.has("speed")) {
            this.flySpeedModifier = jsonObject.get("speed").getAsFloat();
        }
        if (jsonObject.has("refreshratey")) {
            this.flyRefreshRateY = jsonObject.get("refreshratey").getAsInt();
        }
        if (jsonObject.has("refreshratexz")) {
            this.flyRefreshRateXZ = jsonObject.get("refreshratexz").getAsInt();
        }
        if (jsonObject.has("refreshratespeed")) {
            this.flyRefreshRateSpeed = jsonObject.get("refreshratespeed").getAsInt();
        }
        if (jsonObject.has("flaprate")) {
            this.flapRate = jsonObject.get("flaprate").getAsInt() / 4;
        }
    }

    public boolean willLandInMaterial(Material m) {
        if (this.landingMaterial == null) {
            return false;
        }
        if (this.landingMat == null) {
            Material[] arrmaterial;
            if (this.landingMaterial.equalsIgnoreCase("grassandleaves")) {
                Material[] arrmaterial2 = new Material[2];
                arrmaterial2[0] = Material.LEAVES;
                arrmaterial = arrmaterial2;
                arrmaterial2[1] = Material.GRASS;
            } else if (this.landingMaterial.equalsIgnoreCase("leaves")) {
                Material[] arrmaterial3 = new Material[1];
                arrmaterial = arrmaterial3;
                arrmaterial3[0] = Material.LEAVES;
            } else {
                arrmaterial = null;
            }
            this.landingMat = arrmaterial;
        }
        for (Material mat : this.landingMat) {
            if (m != mat) continue;
            return true;
        }
        return false;
    }
}

