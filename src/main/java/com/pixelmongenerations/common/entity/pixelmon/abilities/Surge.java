/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Terrain;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.item.heldItems.ItemTerrainExtender;

public class Surge
extends AbilityBase {
    public Terrain terrainType;

    public Surge(Terrain terrainType) {
        this.terrainType = terrainType;
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.bc.globalStatusController.getTerrain().type == this.terrainType.type && this.terrainType.turnsToGo != 0) {
            return;
        }
        this.terrainType.turnsToGo = newPokemon.getHeldItem() instanceof ItemTerrainExtender ? 8 : 5;
        newPokemon.bc.globalStatusController.removeGlobalStatus(newPokemon.bc.globalStatusController.getTerrain());
        newPokemon.bc.globalStatusController.addGlobalStatus(this.terrainType);
        this.sendActivatedMessage(newPokemon);
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper oldPokemon) {
    }

    @Override
    public boolean needNewInstance() {
        return true;
    }

    @Override
    public AbilityBase getNewInstance() {
        return new Surge(this.terrainType.getNewInstance());
    }
}

