/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class CuriousMedicine
extends AbilityBase {
    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        for (PixelmonWrapper allies : newPokemon.bc.getTeamPokemon(newPokemon.getParticipant())) {
            allies.getBattleStats().clearBattleStats();
            newPokemon.bc.sendToAll("pixelmon.effect.modifierscleared", new Object[0]);
        }
    }
}

