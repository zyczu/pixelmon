/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumSpecies;

public class Pressure
extends AbilityBase {
    @Override
    public void preProcessAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (a.pp > 1 && !user.bc.simulateMode) {
            if (user.inMultipleHit) {
                return;
            }
            --a.pp;
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.getSpecies() == EnumSpecies.Eternatus && newPokemon.getParticipant() instanceof WildPixelmonParticipant) {
            newPokemon.bc.sendToAll("pixelmon.eternatuswarning", newPokemon.getNickname());
        }
        newPokemon.bc.sendToAll("pixelmon.abilities.pressure", newPokemon.getNickname());
    }
}

