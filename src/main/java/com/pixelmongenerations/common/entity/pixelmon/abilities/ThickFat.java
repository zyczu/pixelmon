/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;

public class ThickFat
extends AbilityBase {
    @Override
    public int modifyDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper pokemon, Attack a) {
        if (a.getAttackBase().attackType == EnumType.Fire || a.getAttackBase().attackType == EnumType.Ice) {
            damage /= 2;
        }
        return damage;
    }
}

