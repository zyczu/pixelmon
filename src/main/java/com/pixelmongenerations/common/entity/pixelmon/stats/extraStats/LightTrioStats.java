/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.extraStats;

import com.pixelmongenerations.common.entity.pixelmon.stats.ExtraStats;
import com.pixelmongenerations.core.config.PixelmonConfig;
import net.minecraft.nbt.NBTTagCompound;

public class LightTrioStats
extends ExtraStats {
    public int numWormholes = 0;
    public static final int MAX_WORMHOLES = PixelmonConfig.lightTrioMaxWormholes;

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        nbt.setByte("NumWormholes", (byte)this.numWormholes);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        this.numWormholes = nbt.getInteger("NumWormholes");
    }
}

