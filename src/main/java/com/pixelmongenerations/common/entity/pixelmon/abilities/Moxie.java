/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Mummy;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class Moxie
extends AbilityBase {
    @Override
    public void tookDamageUser(int damage, PixelmonWrapper user, PixelmonWrapper pokemon, Attack a) {
        if (!(!pokemon.isFainted() || a.getAttackBase().getMakesContact() && pokemon.getBattleAbility() instanceof Mummy)) {
            this.sendActivatedMessage(user);
            user.getBattleStats().modifyStat(1, StatsType.Attack);
        }
    }
}

