/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sleep;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class EarlyBird
extends AbilityBase {
    @Override
    public void onStatusAdded(StatusBase status, PixelmonWrapper user, PixelmonWrapper opponent) {
        if (status instanceof Sleep) {
            Sleep sleepStatus = (Sleep)status;
            sleepStatus.effectTurns /= 2;
        }
    }
}

