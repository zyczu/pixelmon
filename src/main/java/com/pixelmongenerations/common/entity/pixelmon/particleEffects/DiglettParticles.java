/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.particleEffects;

import com.pixelmongenerations.common.entity.pixelmon.Entity4Textures;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.ParticleEffects;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;

public class DiglettParticles
extends ParticleEffects {
    public DiglettParticles(Entity4Textures pixelmon) {
        super(pixelmon);
    }

    @Override
    public void onUpdate() {
        IBlockState state;
        if ((this.pixelmon.lastTickPosX - this.pixelmon.posX != 0.0 || this.pixelmon.lastTickPosZ - this.pixelmon.posZ != 0.0) && (state = this.pixelmon.world.getBlockState(new BlockPos(MathHelper.floor(this.pixelmon.posX), MathHelper.floor(this.pixelmon.posY - (double)0.2f - this.pixelmon.getYOffset()), MathHelper.floor(this.pixelmon.posZ)))).getMaterial() != Material.AIR) {
            this.pixelmon.world.spawnParticle(EnumParticleTypes.BLOCK_CRACK, this.pixelmon.posX + ((double)this.rand.nextFloat() - 0.5) * (double)this.pixelmon.width, this.pixelmon.getCollisionBoundingBox().minY + 0.1, this.pixelmon.posZ + ((double)this.rand.nextFloat() - 0.5) * (double)this.pixelmon.width, -this.pixelmon.motionX * 4.0, 1.5, -this.pixelmon.motionZ * 4.0, Block.getStateId(state));
        }
    }
}

