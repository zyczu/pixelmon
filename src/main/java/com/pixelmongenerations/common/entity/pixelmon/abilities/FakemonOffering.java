/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.network.EnumUpdateType;

public class FakemonOffering
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return new int[]{(int)((double)power * (double)(user.getFriendship().getFriendship() / 100)), accuracy};
    }

    @Override
    public void tookDamageUser(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        user.getFriendship().decreaseFriendship(Math.round(user.damageTakenThisTurn / 2));
        user.update(EnumUpdateType.Friendship);
    }
}

