/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.network.PixelmonData;
import java.util.Arrays;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class Dig
extends ExternalMoveBase {
    private List<Block> allowedBlocks = Arrays.asList(Blocks.GRASS, Blocks.SAND, Blocks.SANDSTONE, Blocks.STONE, Blocks.GRAVEL, Blocks.MYCELIUM, Blocks.DIRT);

    public Dig() {
        super("dig", "Dig");
    }

    @Override
    public boolean execute(EntityPixelmon user, RayTraceResult target, int moveIndex) {
        BlockPos pos;
        int depth = 74 - (int)(((float)user.stats.Attack - 20.0f) / 180.0f * 74.0f);
        if (depth < 2) {
            depth = 2;
        }
        int maxOff = 1;
        if (user.stats.Attack > 150) {
            maxOff = 2;
        }
        if (user.stats.Attack > 300) {
            maxOff = 3;
        }
        if ((pos = target.getBlockPos()).getY() < depth) {
            user.world.playSound(null, pos, SoundEvents.BLOCK_ANVIL_LAND, SoundCategory.NEUTRAL, 1.0f, 0.1f);
            return true;
        }
        if (target.sideHit == EnumFacing.UP) {
            for (int xOff = -1 * maxOff; xOff <= maxOff; ++xOff) {
                for (int zOff = -1 * maxOff; zOff <= maxOff; ++zOff) {
                    this.doHarvest(user, pos.add(xOff, 0, zOff));
                }
            }
        } else if (target.sideHit == EnumFacing.DOWN) {
            for (int xOff = -1 * maxOff; xOff <= maxOff; ++xOff) {
                for (int zOff = -1 * maxOff; zOff <= maxOff; ++zOff) {
                    this.doHarvest(user, pos.add(xOff, 0, zOff));
                }
            }
        } else if (target.sideHit == EnumFacing.SOUTH) {
            for (int xOff = -1 * maxOff; xOff <= maxOff; ++xOff) {
                for (int yOff = -1 * maxOff; yOff <= maxOff; ++yOff) {
                    this.doHarvest(user, pos.add(xOff, yOff, 0));
                }
            }
        } else if (target.sideHit == EnumFacing.NORTH) {
            for (int xOff = -1 * maxOff; xOff <= maxOff; ++xOff) {
                for (int yOff = -1 * maxOff; yOff <= maxOff; ++yOff) {
                    this.doHarvest(user, pos.add(xOff, yOff, 0));
                }
            }
        } else if (target.sideHit == EnumFacing.EAST) {
            for (int yOff = -1 * maxOff; yOff <= maxOff; ++yOff) {
                for (int zOff = -1 * maxOff; zOff <= maxOff; ++zOff) {
                    this.doHarvest(user, pos.add(0, yOff, zOff));
                }
            }
        } else if (target.sideHit == EnumFacing.WEST) {
            for (int yOff = -1 * maxOff; yOff <= maxOff; ++yOff) {
                for (int zOff = -1 * maxOff; zOff <= maxOff; ++zOff) {
                    this.doHarvest(user, pos.add(0, yOff, zOff));
                }
            }
        }
        user.world.playSound(null, pos, SoundEvents.BLOCK_GRASS_BREAK, SoundCategory.NEUTRAL, 1.0f, 0.1f);
        return true;
    }

    private void doHarvest(EntityPixelmon user, BlockPos pos) {
        IBlockState state = user.world.getBlockState(pos);
        Block block = state.getBlock();
        if (this.allowedBlocks.contains(block)) {
            EntityPlayerMP player = (EntityPlayerMP)user.getOwner();
            boolean flag = block.canHarvestBlock(user.world, pos, player);
            boolean flag1 = this.removeBlock(pos, user.world, player, flag);
            if (flag && flag1) {
                block.dropBlockAsItem(user.world, pos, state, 0);
            }
        }
    }

    private boolean removeBlock(BlockPos pos, World theWorld, EntityPlayerMP playerMP, boolean canHarvest) {
        IBlockState iblockstate = theWorld.getBlockState(pos);
        Block block = iblockstate.getBlock();
        block.onBlockHarvested(theWorld, pos, iblockstate, playerMP);
        boolean flag = block.removedByPlayer(iblockstate, theWorld, pos, playerMP, canHarvest);
        if (flag) {
            block.onPlayerDestroy(theWorld, pos, iblockstate);
        }
        return flag;
    }

    @Override
    public int getCooldown(EntityPixelmon user) {
        return (int)Math.max(0.0f, (200.0f - (float)user.stats.Speed) / 30.0f * 20.0f);
    }

    @Override
    public int getCooldown(PixelmonData user) {
        return (int)Math.max(0.0f, (200.0f - (float)user.Speed) / 30.0f * 20.0f);
    }

    @Override
    public boolean isDestructive() {
        return true;
    }
}

