/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.network.EnumUpdateType;

public class Regenerator
extends AbilityBase {
    @Override
    public void applySwitchOutEffect(PixelmonWrapper pw) {
        pw.animateHP = false;
        pw.healByPercent(33.333332f);
        pw.update(EnumUpdateType.HP);
        pw.animateHP = true;
    }
}

