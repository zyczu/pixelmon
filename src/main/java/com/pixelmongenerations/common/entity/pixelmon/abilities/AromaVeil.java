/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class AromaVeil
extends AbilityBase {
    @Override
    public boolean allowsStatusTeammate(StatusType status, PixelmonWrapper pokemon, PixelmonWrapper target, PixelmonWrapper user) {
        if (status.isStatus(StatusType.Disable, StatusType.Encore, StatusType.HealBlock, StatusType.Infatuated, StatusType.Taunt, StatusType.Torment)) {
            user.bc.sendToAll("pixelmon.abilities.aromaveil", pokemon.getNickname(), target.getNickname());
            return false;
        }
        return true;
    }
}

