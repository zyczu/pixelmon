/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon;

import com.pixelmongenerations.api.Tuple;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.HashSet;

public enum EnumCreators {
    Deltric(Tuple.of(EnumSpecies.Krabby, "spookster"));

    private static HashSet<EnumSpecies> creatorSpecies;
    private Tuple<EnumSpecies, String>[] voices;

    @SafeVarargs
    private EnumCreators(Tuple<EnumSpecies, String> ... voices) {
        this.voices = voices;
    }

    public Tuple<EnumSpecies, String>[] getVoices() {
        return this.voices;
    }

    public static EnumCreators getCreatorVoiceFor(EnumSpecies species, String nickName) {
        if (!creatorSpecies.contains((Object)species)) {
            return null;
        }
        for (EnumCreators creator : EnumCreators.values()) {
            for (Tuple<EnumSpecies, String> voice : creator.getVoices()) {
                if (!voice.getFirst().equals((Object)species) || !voice.getSecond().equalsIgnoreCase(nickName)) continue;
                return creator;
            }
        }
        return null;
    }

    static {
        creatorSpecies = new HashSet();
        for (EnumCreators creator : EnumCreators.values()) {
            for (Tuple<EnumSpecies, String> voice : creator.getVoices()) {
                creatorSpecies.add(voice.getFirst());
            }
        }
    }
}

