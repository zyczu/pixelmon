/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class AngerPoint
extends AbilityBase {
    public boolean wasCrit = false;

    @Override
    public void tookDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (this.wasCrit && target.isAlive()) {
            target.bc.sendToAll("pixelmon.abilities.angerpoint", target.getNickname());
            target.getBattleStats().modifyStat(12, StatsType.Attack);
        }
        this.wasCrit = false;
    }
}

