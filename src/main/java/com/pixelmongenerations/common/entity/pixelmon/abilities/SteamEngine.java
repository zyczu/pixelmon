/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;

public class SteamEngine
extends AbilityBase {
    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (a.getAttackBase().attackType == EnumType.Fire || a.getAttackBase().attackType == EnumType.Water) {
            pokemon.getBattleStats().modifyStat(6, StatsType.Speed);
            pokemon.bc.sendToAll("pixelmon.abilities.steamengine", pokemon.getNickname());
        }
        return true;
    }
}

