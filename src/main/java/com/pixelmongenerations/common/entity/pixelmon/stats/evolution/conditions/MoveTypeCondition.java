/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import com.pixelmongenerations.core.enums.EnumType;

public class MoveTypeCondition
extends EvoCondition {
    EnumType type = EnumType.Dragon;

    public MoveTypeCondition() {
    }

    public MoveTypeCondition(EnumType type) {
        this.type = type;
    }

    @Override
    public boolean passes(EntityPixelmon pokemon) {
        Moveset moveset = pokemon.getMoveset();
        for (Attack a : moveset.attacks) {
            if (a == null || a.getAttackBase().attackType != this.type) continue;
            return true;
        }
        return false;
    }
}

