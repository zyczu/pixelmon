/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQuery;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumGreninja;

public class BattleBond
extends AbilityBase {
    @Override
    public void tookDamageUser(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (user.getParticipant().ashNinja == null && user.getForm() != 2 && user.getSpecies().equals((Object)EnumSpecies.Greninja) && user.getParticipant().getType() != ParticipantType.WildPokemon && target.isFainted() && target.getParticipant().hasMorePokemon()) {
            if (!user.hasStatus(StatusType.Transformed)) {
                user.getParticipant().ashNinja = user.getPokemonID();
                user.bc.sendToAll("pixelmon.battletext.ashgreninja.react", user.getParticipant().getDisplayName());
                user.evolution = new EvolutionQuery(user.pokemon, 2);
                user.setFormNoEntity(2);
            }
        }
    }

    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (user.getSpecies().equals((Object)EnumSpecies.Greninja) && user.pokemon.getForm() == 2) {
            if (a.isAttack("Water Shuriken")) {
                user.attack.getAttackBase().basePower = 20;
                return new int[]{20, accuracy};
            }
        }
        if (user.getSpecies().equals((Object)EnumSpecies.Greninja) && user.pokemon.getForm() != 2) {
            if (a.isAttack("Water Shuriken")) {
                user.attack.getAttackBase().basePower = 15;
                return new int[]{15, accuracy};
            }
        }
        return new int[]{power, accuracy};
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper user) {
        if (user.getParticipant().ashNinja != null && user.getForm() == 2 && user.getSpecies().equals((Object)EnumSpecies.Greninja)) {
            user.setForm(EnumGreninja.ASH.getForm());
            user.bc.modifyStats();
        }
    }

    @Override
    public boolean canBeNeutralized() {
        return true;
    }
}

