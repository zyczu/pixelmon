/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.vecmath.Vector3f
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

import javax.vecmath.Vector3f;

public class RidingOptions {
    public Vector3f standing;
    public Vector3f moving;

    public void setStandingOffsets(double x, double y, double z) {
        this.standing = new Vector3f();
        this.standing.x = (float)x;
        this.standing.y = (float)y;
        this.standing.z = (float)z;
    }

    public void setMovingOffsets(double x, double y, double z) {
        this.moving = new Vector3f();
        this.moving.x = (float)x;
        this.moving.y = (float)y;
        this.moving.z = (float)z;
    }
}

