/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.ArrayList;
import net.minecraft.nbt.NBTTagCompound;

public class PartyCondition
extends EvoCondition {
    public ArrayList<EnumSpecies> withPokemon = new ArrayList();
    public ArrayList<EnumType> withTypes = new ArrayList();

    public PartyCondition() {
    }

    public PartyCondition(EnumSpecies ... with) {
        for (EnumSpecies pokemon : with) {
            if (pokemon == null) continue;
            this.withPokemon.add(pokemon);
        }
    }

    public PartyCondition(EnumType ... types) {
        for (EnumType type : types) {
            if (types == null) continue;
            this.withTypes.add(type);
        }
    }

    @Override
    public boolean passes(EntityPixelmon pokemon) {
        PlayerStorage storage = pokemon.getStorage().orElse(null);
        if (storage != null) {
            for (NBTTagCompound nbt : storage.partyPokemon) {
                if (nbt == null) continue;
                EnumSpecies partyPokemon = EnumSpecies.getFromNameAnyCase(nbt.getString("Name"));
                if (partyPokemon != null && this.withPokemon.contains((Object)partyPokemon)) {
                    return true;
                }
                BaseStats bs = EntityPixelmon.getBaseStats(partyPokemon).get();
                if (partyPokemon == null || !this.withTypes.contains((Object)bs.type1) && (bs.type2 == null || !this.withTypes.contains((Object)bs.type2))) continue;
                return true;
            }
        }
        return false;
    }
}

