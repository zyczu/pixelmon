/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

public class Rarity {
    public int day;
    public int dawndusk;
    public int night;

    public Rarity(int day, int dawndusk, int night) {
        this.day = day;
        this.dawndusk = dawndusk;
        this.night = night;
    }
}

