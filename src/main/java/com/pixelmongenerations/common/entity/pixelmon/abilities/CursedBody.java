/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Disable;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import net.minecraft.util.text.TextComponentTranslation;

public class CursedBody
extends AbilityBase {
    @Override
    public void tookDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (!user.hasStatus(StatusType.Disable)) {
            if (!target.hasStatus(StatusType.Substitute) && RandomHelper.getRandomChance(0.3f) && !user.isDynamaxed()) {
                TextComponentTranslation message = ChatHandler.getMessage("pixelmon.abilities.cursedbody", target.getNickname(), user.getNickname(), a.getAttackBase().getLocalizedName());
                user.addStatus(new Disable(a), target, message);
            }
        }
    }
}

