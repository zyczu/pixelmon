/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;

public class FriendshipCondition
extends EvoCondition {
    int friendship = -1;

    public FriendshipCondition() {
    }

    public FriendshipCondition(int friendship) {
        this.friendship = friendship;
    }

    @Override
    public boolean passes(EntityPixelmon pokemon) {
        return this.friendship == -1 ? pokemon.friendship.isFriendshipHighEnoughToEvolve() : pokemon.friendship.getFriendship() >= this.friendship;
    }
}

