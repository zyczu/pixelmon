/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PreventStatus;

public class WaterVeil
extends PreventStatus {
    public WaterVeil() {
        super("pixelmon.abilities.waterveil", "pixelmon.abilities.waterveilcure", StatusType.Burn);
    }
}

