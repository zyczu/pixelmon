/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon;

import com.pixelmongenerations.api.events.BreedEvent;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.block.ranch.BreedingConditions;
import com.pixelmongenerations.common.block.ranch.RanchBounds;
import com.pixelmongenerations.common.block.tileEntities.TileEntityRanchBase;
import com.pixelmongenerations.common.block.tileEntities.TileEntityRanchBlock;
import com.pixelmongenerations.common.entity.pixelmon.Entity6CanBattle;
import com.pixelmongenerations.common.entity.pixelmon.Entity9HasSounds;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.BreedingParticles;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.ParticleEffects;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.IVStore;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.item.EnumIsisHourglassType;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.ItemIsisHourglass;
import com.pixelmongenerations.common.item.heldItems.NoItem;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.database.DatabaseStats;
import com.pixelmongenerations.core.enums.EnumBreedingStrength;
import com.pixelmongenerations.core.enums.EnumEggGroup;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.forms.EnumRotom;
import com.pixelmongenerations.core.enums.forms.EnumSinistea;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.util.RegexPatterns;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class Entity10CanBreed
extends Entity9HasSounds {
    public boolean isEgg = false;
    public boolean isRandomEgg = false;
    public EnumEggGroup group = EnumEggGroup.Undiscovered;
    public Integer eggCycles = 21;
    public int steps;
    public boolean isInRanchBlock = false;
    public ParticleEffects[] breedingParticleEffects = null;
    private int lastBreedingLevels = -1;
    public EnumBreedingStrength breedingStrength = EnumBreedingStrength.NONE;
    public long lastBreedingTime;
    public int[] eggMoves = new int[0];
    int breedingCheckCount = 0;
    List<EnumSpecies> galarian = Arrays.asList(new EnumSpecies[]{EnumSpecies.Runerigus, EnumSpecies.Sirfetchd, EnumSpecies.Obstagoon, EnumSpecies.Perrserker, EnumSpecies.Cursola, EnumSpecies.MrRime});
    List<EnumSpecies> hisuian = Arrays.asList(new EnumSpecies[]{EnumSpecies.Basculegion, EnumSpecies.Kleavor, EnumSpecies.Overqwil, EnumSpecies.Sneasler, EnumSpecies.Ursaluna, EnumSpecies.Wyrdeer});

    public Entity10CanBreed(World par1World) {
        super(par1World);
        this.dataManager.register(EntityPixelmon.dwNumBreedingLevels, -1);
    }

    @Override
    public void init(String name) {
        super.init(name);
    }

    public int getNumBreedingLevels() {
        return this.dataManager.get(EntityPixelmon.dwNumBreedingLevels);
    }

    public void setNumBreedingLevels(int newValue) {
        this.dataManager.set(EntityPixelmon.dwNumBreedingLevels, newValue);
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void onUpdate() {
        super.onUpdate();
        if (this.world.isRemote) {
            if (this.getNumBreedingLevels() != this.lastBreedingLevels) {
                this.breedingParticleEffects = this.getBreedingParticleEffects();
                this.lastBreedingLevels = this.getNumBreedingLevels();
            }
            if (this.breedingParticleEffects != null) {
                for (ParticleEffects breedingParticleEffect : this.breedingParticleEffects) {
                    breedingParticleEffect.onUpdate();
                }
            }
        }
    }

    @Override
    public boolean canDespawn() {
        return this.blockOwner == null && super.canDespawn();
    }

    public void setRanchBlockOwner(TileEntityRanchBase tileEntityRanchBase) {
        this.blockOwner = tileEntityRanchBase;
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        if (this.isInRanchBlock) {
            nbt.setLong("lastBreedingTime", this.lastBreedingTime);
        }
        nbt.setBoolean("isEgg", this.isEgg);
        nbt.setBoolean("isRandomEgg", this.isRandomEgg);
        nbt.setInteger("eggGroupID", this.group.getIndex());
        nbt.setInteger("eggCycles", this.eggCycles);
        nbt.setInteger("steps", this.steps);
        nbt.setFloat("lastEggXCoord", (float)this.posX);
        nbt.setFloat("lastEggZCoord", (float)this.posZ);
        nbt.setBoolean("isInRanch", this.isInRanchBlock);
        nbt.setShort("BreedingInteractions", (short)this.getNumBreedingLevels());
        nbt.setIntArray("EggMoves", this.eggMoves);
    }

    @Override
    public void getNBTTags(HashMap<String, Class> tags) {
        super.getNBTTags(tags);
        tags.put("lastBreedingTime", Long.class);
        tags.put("isEgg", Boolean.class);
        tags.put("isRandomEgg", Boolean.class);
        tags.put("eggGroupID", Integer.class);
        tags.put("eggCycles", Integer.class);
        tags.put("steps", Integer.class);
        tags.put("lastEggXCoord", Float.class);
        tags.put("lastEggZCoord", Float.class);
        tags.put("isInRanch", Boolean.class);
        tags.put("BreedingInteractions", Short.class);
        tags.put("EggMoves", Integer[].class);
    }

    @Override
    public boolean processInteract(EntityPlayer player, EnumHand hand) {
        if (player instanceof EntityPlayerMP) {
            if (!this.isInRanchBlock) {
                return super.processInteract(player, hand);
            }
            ItemStack itemStack = player.getHeldItem(hand);
            if (this.getOwner() == player && hand != EnumHand.OFF_HAND) {
                if (super.processInteract(player, hand)) {
                    return true;
                }
                this.blockOwner.updateStatus();
                if (this.blockOwner instanceof TileEntityRanchBlock) {
                    TileEntityRanchBlock ranch = (TileEntityRanchBlock)this.blockOwner;
                    if (this.blockOwner.getEntityCount() > 1) {
                        Item item = itemStack.getItem();
                        if (item instanceof ItemIsisHourglass && ((ItemIsisHourglass)item).type == EnumIsisHourglassType.Silver) {
                            if (this.breedingStrength == EnumBreedingStrength.NONE) {
                                return false;
                            }
                            if (ranch.getFirstBreedingPartner((EntityPixelmon)this) == null) {
                                return false;
                            }
                            int numBreedingLevels = this.getNumBreedingLevels();
                            if (numBreedingLevels >= PixelmonConfig.numBreedingLevels) {
                                return false;
                            }
                            player.sendMessage(new TextComponentTranslation("ranch.hourglass.upgrade", this.getNickname()));
                            this.setNumBreedingLevels(numBreedingLevels + 1);
                            if (!player.capabilities.isCreativeMode) {
                                player.inventory.clearMatchingItems(item, itemStack.getMetadata(), 1, itemStack.getTagCompound());
                            }
                            return super.processInteract(player, hand);
                        }
                        EntityPixelmon otherPixelmon = ranch.getFirstBreedingPartner((EntityPixelmon)this);
                        if (otherPixelmon != null) {
                            if (this.breedingStrength == EnumBreedingStrength.NONE) {
                                this.refreshBreedingStrength();
                            }
                            if (this.getNumBreedingLevels() >= PixelmonConfig.numBreedingLevels) {
                                player.sendMessage(new TextComponentTranslation("pixelmon.ranch.maxaffection", this.getNickname(), otherPixelmon.getNickname()));
                            } else {
                                player.sendMessage(new TextComponentTranslation("pixelmon.ranch.level" + this.breedingStrength.ordinal(), this.getNickname(), otherPixelmon.getNickname()));
                            }
                        } else {
                            player.sendMessage(new TextComponentTranslation("pixelmon.ranch.notcompatible", this.getNickname()));
                        }
                    }
                } else {
                    player.sendMessage(new TextComponentTranslation("pixelmon.ranch.nopartner", this.getNickname()));
                }
            }
        }
        return super.processInteract(player, hand);
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        try {
            if (nbt.hasKey("isEgg")) {
                this.isEgg = nbt.getBoolean("isEgg");
                this.isRandomEgg = nbt.getBoolean("isRandomEgg");
                this.group = EnumEggGroup.getEggGroupFromIndex(nbt.getInteger("eggGroupID"));
                this.eggCycles = nbt.getInteger("eggCycles");
                this.steps = nbt.getInteger("steps");
            } else {
                this.isEgg = false;
                this.isRandomEgg = false;
                this.group = EnumEggGroup.getRandomEggGroup(this.getSpecies());
                this.eggCycles = 21;
                this.steps = 0;
            }
            if (nbt.hasKey("isInRanch")) {
                this.isInRanchBlock = nbt.getBoolean("isInRanch");
            }
            if (nbt.hasKey("lastBreedingTime")) {
                this.lastBreedingTime = nbt.getLong("lastBreedingTime");
            }
            if (nbt.hasKey("BreedingInteractions")) {
                this.setNumBreedingLevels(nbt.getShort("BreedingInteractions"));
            }
            this.eggMoves = nbt.getIntArray("EggMoves");
        }
        catch (Exception var3) {
            var3.printStackTrace();
        }
    }

    public void makeEntityIntoRandomEgg(EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        EnumSpecies pokemon = EnumSpecies.randomPoke(PixelmonConfig.allowRandomBreedingEggsToBeLegendary);
        while (pokemon == EnumSpecies.MissingNo) {
            pokemon = EnumSpecies.randomPoke(PixelmonConfig.allowRandomBreedingEggsToBeLegendary);
        }
        this.baseStats = null;
        super.init(pokemon.name);
        this.isRandomEgg = true;
        this.isEgg = true;
        this.getLvl().setLevel(1);
        this.setAbilitySlot(this.baseStats.abilities[1] != null ? RandomHelper.getRandomNumberBetween(0, 1) : 0);
        this.setAbility(this.baseStats.abilities[this.getAbilitySlot()]);
        this.stats.IVs = Entity10CanBreed.getIVsForEgg(pixelmon1, pixelmon2);
        this.setNature(Entity10CanBreed.getNatureForEgg(pixelmon1, pixelmon2));
        this.setGrowth(Entity10CanBreed.getEggGrowth(pixelmon1, pixelmon2));
        this.setShiny(this.getIfEggIsShiny(pixelmon1, pixelmon2));
        this.group = Entity10CanBreed.getRandomEggGroupForPokemon(pokemon);
        Optional<BaseStats> optional = Entity10CanBreed.getBaseStats(pokemon);
        Integer tempCycles = optional.map(value -> value.eggCycles).orElse(null);
        this.eggCycles = tempCycles != null && tempCycles != 0 ? tempCycles : Integer.valueOf(21);
    }

    public void makeEntityIntoEgg() {
        super.init(this.getPokemonName());
        this.isEgg = true;
        this.group = this.getRandomEggGroupForPokemon();
        this.getLvl().setLevel(1);
        Integer tempCycles = this.baseStats.eggCycles;
        this.eggCycles = tempCycles != null && tempCycles != 0 ? tempCycles : Integer.valueOf(21);
    }

    public void makeEntityIntoEgg(EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        block105: {
            block98: {
                block100: {
                    EnumSpecies pokemon;
                    block104: {
                        block103: {
                            block101: {
                                block102: {
                                    block99: {
                                        if (!Entity10CanBreed.canBreed(pixelmon1, pixelmon2)) break block98;
                                        if (!pixelmon1.isPokemon(EnumSpecies.Ditto)) break block99;
                                        if (!pixelmon2.isPokemon(EnumSpecies.Ditto) || !PixelmonConfig.allowDittoDittoBreeding) break block99;
                                        this.makeEntityIntoRandomEgg(pixelmon1, pixelmon2);
                                        break block100;
                                    }
                                    boolean shouldBeAlolanEgg = false;
                                    int alolanChance = -1;
                                    boolean shouldBeGalarianEgg = this.galarian.contains((Object)pixelmon1.getSpecies()) || this.galarian.contains((Object)pixelmon2.getSpecies());
                                    int GalarianChance = -1;
                                    boolean shouldBeHisuianEgg = this.hisuian.contains((Object)pixelmon1.getSpecies()) || this.hisuian.contains((Object)pixelmon2.getSpecies());
                                    int hisuianChance = -1;
                                    if (pixelmon1.hasForms() || pixelmon2.hasForms()) {
                                        if (pixelmon1.getFormEnum() == EnumForms.Alolan) {
                                            if (pixelmon2.getFormEnum() == EnumForms.Alolan) {
                                                shouldBeAlolanEgg = true;
                                            } else if (pixelmon1.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                if (pixelmon2.getItemHeld() != PixelmonItemsHeld.everStone) {
                                                    shouldBeAlolanEgg = true;
                                                }
                                            } else {
                                                alolanChance = 20;
                                            }
                                        }
                                        if (pixelmon1.getFormEnum() == EnumForms.Galarian) {
                                            if (pixelmon2.getFormEnum() == EnumForms.Galarian) {
                                                shouldBeGalarianEgg = true;
                                            } else if (pixelmon1.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                if (pixelmon2.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                    if (pixelmon1.getGender() == Gender.Female) {
                                                        shouldBeGalarianEgg = true;
                                                    } else if (pixelmon2.getGender() == Gender.Female) {
                                                        shouldBeGalarianEgg = false;
                                                    }
                                                } else {
                                                    shouldBeGalarianEgg = true;
                                                }
                                            } else {
                                                GalarianChance = 20;
                                            }
                                        }
                                        if (pixelmon1.getFormEnum() == EnumForms.Hisuian) {
                                            if (pixelmon2.getFormEnum() == EnumForms.Hisuian) {
                                                shouldBeHisuianEgg = true;
                                            } else if (pixelmon1.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                if (pixelmon2.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                    if (pixelmon1.getGender() == Gender.Female) {
                                                        shouldBeHisuianEgg = true;
                                                    } else if (pixelmon2.getGender() == Gender.Female) {
                                                        shouldBeHisuianEgg = false;
                                                    }
                                                } else {
                                                    shouldBeHisuianEgg = true;
                                                }
                                            } else {
                                                hisuianChance = 20;
                                            }
                                        }
                                        if (!shouldBeAlolanEgg && pixelmon2.getFormEnum() == EnumForms.Alolan) {
                                            if (pixelmon1.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                if (pixelmon2.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                    if (pixelmon1.getGender() == Gender.Female) {
                                                        shouldBeAlolanEgg = false;
                                                        alolanChance = -1;
                                                    } else if (pixelmon2.getGender() == Gender.Female) {
                                                        shouldBeAlolanEgg = true;
                                                    }
                                                }
                                            } else if (pixelmon2.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                if (pixelmon2.getGender() == Gender.Female) {
                                                    shouldBeAlolanEgg = true;
                                                } else {
                                                    shouldBeAlolanEgg = false;
                                                    alolanChance = -1;
                                                }
                                            } else {
                                                alolanChance = 20;
                                            }
                                        }
                                        if (!shouldBeGalarianEgg && pixelmon2.getFormEnum() == EnumForms.Galarian) {
                                            if (pixelmon1.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                if (pixelmon2.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                    if (pixelmon1.getGender() == Gender.Female) {
                                                        shouldBeGalarianEgg = false;
                                                        GalarianChance = -1;
                                                    } else if (pixelmon2.getGender() == Gender.Female) {
                                                        shouldBeGalarianEgg = true;
                                                    }
                                                }
                                            } else if (pixelmon2.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                if (pixelmon2.getGender() == Gender.Female) {
                                                    shouldBeGalarianEgg = true;
                                                } else {
                                                    shouldBeGalarianEgg = false;
                                                    GalarianChance = -1;
                                                }
                                            } else {
                                                GalarianChance = 20;
                                            }
                                        }
                                        if (!shouldBeHisuianEgg && pixelmon2.getFormEnum() == EnumForms.Hisuian) {
                                            if (pixelmon1.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                if (pixelmon2.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                    if (pixelmon1.getGender() == Gender.Female) {
                                                        shouldBeHisuianEgg = false;
                                                        hisuianChance = -1;
                                                    } else if (pixelmon2.getGender() == Gender.Female) {
                                                        shouldBeHisuianEgg = true;
                                                    }
                                                }
                                            } else if (pixelmon2.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                if (pixelmon2.getGender() == Gender.Female) {
                                                    shouldBeHisuianEgg = true;
                                                } else {
                                                    shouldBeHisuianEgg = false;
                                                    hisuianChance = -1;
                                                }
                                            } else {
                                                hisuianChance = 20;
                                            }
                                        }
                                        if (!(shouldBeAlolanEgg && shouldBeGalarianEgg && shouldBeHisuianEgg)) {
                                            if (pixelmon1.isPokemon(EnumSpecies.Ditto)) {
                                                if (pixelmon2.getFormEnum() == EnumForms.Alolan && pixelmon2.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                    shouldBeAlolanEgg = true;
                                                } else if (pixelmon2.getFormEnum() == EnumForms.Galarian && pixelmon2.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                    shouldBeGalarianEgg = true;
                                                }
                                            } else if (pixelmon2.isPokemon(EnumSpecies.Ditto) && pixelmon1.getFormEnum() == EnumForms.Alolan && pixelmon1.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                shouldBeAlolanEgg = true;
                                            } else if (pixelmon2.isPokemon(EnumSpecies.Ditto) && pixelmon1.getFormEnum() == EnumForms.Galarian && pixelmon1.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                shouldBeGalarianEgg = true;
                                            } else if (pixelmon2.isPokemon(EnumSpecies.Ditto) && pixelmon1.getFormEnum() == EnumForms.Hisuian && pixelmon1.getItemHeld() == PixelmonItemsHeld.everStone) {
                                                shouldBeHisuianEgg = true;
                                            }
                                        }
                                    }
                                    pokemon = Entity10CanBreed.getPokemonInEggName(pixelmon1, pixelmon2);
                                    this.baseStats = null;
                                    super.init(pokemon.name);
                                    this.isEgg = true;
                                    this.group = Entity10CanBreed.getEggGroupForPair(pixelmon1, pixelmon2);
                                    this.getLvl().setLevel(1);
                                    Integer tempCycles = this.baseStats.eggCycles;
                                    Integer n = this.eggCycles = tempCycles != null && tempCycles != 0 ? tempCycles : Integer.valueOf(21);
                                    if (shouldBeAlolanEgg) {
                                        this.setForm(EnumForms.Alolan.getForm());
                                    } else if (alolanChance != -1 && RandomHelper.getRandomChance(alolanChance)) {
                                        this.setForm(EnumForms.Alolan.getForm());
                                    }
                                    if (shouldBeGalarianEgg) {
                                        this.setForm(EnumForms.Galarian.getForm());
                                    } else if (GalarianChance != -1 && RandomHelper.getRandomChance(GalarianChance)) {
                                        this.setForm(EnumForms.Galarian.getForm());
                                    }
                                    if (shouldBeHisuianEgg) {
                                        this.setForm(EnumForms.Hisuian.getForm());
                                    } else if (hisuianChance != -1 && RandomHelper.getRandomChance(hisuianChance)) {
                                        this.setForm(EnumForms.Hisuian.getForm());
                                    }
                                    if (pixelmon1.isPokemon(EnumSpecies.Ditto) && pixelmon2.getFormEnum() == EnumForms.Alolan && !shouldBeAlolanEgg) {
                                        this.setAbilitySlot(1);
                                    }
                                    if (pixelmon1.isPokemon(EnumSpecies.Ditto) && pixelmon2.getFormEnum() == EnumForms.Galarian && !shouldBeGalarianEgg) {
                                        this.setAbilitySlot(1);
                                    }
                                    if (pokemon != EnumSpecies.Rotom) break block101;
                                    if (pixelmon1.isPokemon(EnumSpecies.Ditto)) break block102;
                                    if (!pixelmon2.isPokemon(EnumSpecies.Ditto)) break block101;
                                }
                                this.setForm(EnumRotom.NORMAL.getForm());
                            }
                            if (pixelmon1.isPokemon(EnumSpecies.Sinistea)) break block103;
                            if (!pixelmon2.isPokemon(EnumSpecies.Sinistea)) break block104;
                        }
                        this.setForm(EnumSinistea.Phony.getForm());
                    }
                    if (this.baseStats.malePercent < 0) {
                        this.setGender(Gender.None);
                    } else if (this.baseStats.malePercent == 0) {
                        this.setGender(Gender.Female);
                    } else if (this.baseStats.malePercent == 100) {
                        this.setGender(Gender.Male);
                    } else if (this.getGender() == Gender.None) {
                        this.chooseRandomGender();
                    }
                    this.stats.IVs = Entity10CanBreed.getIVsForEgg(pixelmon1, pixelmon2);
                    this.setNature(Entity10CanBreed.getNatureForEgg(pixelmon1, pixelmon2));
                    this.caughtBall = Entity10CanBreed.getInheritedPokeball(pixelmon1, pixelmon2);
                    this.setGrowth(Entity10CanBreed.getEggGrowth(pixelmon1, pixelmon2));
                    this.setAbilitySlot(Entity10CanBreed.getEggAbilitySlot(this.baseStats.abilities, pixelmon1, pixelmon2));
                    this.setAbility(this.baseStats.abilities[this.getAbilitySlot()]);
                    this.setShiny(this.getIfEggIsShiny(pixelmon1, pixelmon2));
                    if (pixelmon1.isAlpha() && pixelmon2.isAlpha()) {
                        this.setAlpha(true);
                    }
                    Moveset moveset = this.getEggMoveset(this, pokemon, pixelmon1, pixelmon2);
                    Moveset thisMoveset = this.getMoveset();
                    thisMoveset.set(0, moveset.get(0));
                    thisMoveset.set(1, moveset.get(1));
                    thisMoveset.set(2, moveset.get(2));
                    thisMoveset.set(3, moveset.get(3));
                }
                this.updateStats();
                break block105;
            }
            Pixelmon.LOGGER.info("Error occurred in breeding; incompatible pair passed to Egg initialization.");
        }
    }

    public static EnumNature getNatureForEgg(EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        boolean isEverstone1;
        boolean isEverstone2 = pixelmon2.getItemHeld() == PixelmonItemsHeld.everStone;
        boolean bl = isEverstone1 = pixelmon1.getItemHeld() == PixelmonItemsHeld.everStone;
        return isEverstone1 && isEverstone2 ? (RandomHelper.getRandomChance() ? pixelmon1.getNature() : pixelmon2.getNature()) : (isEverstone1 ? pixelmon1.getNature() : (isEverstone2 ? pixelmon2.getNature() : EnumNature.getRandomNature()));
    }

    public static boolean canLearnVoltTackle(EnumSpecies pokemon, EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        if (pokemon != EnumSpecies.Pichu) {
            return false;
        }
        ItemStack heldItem1 = pixelmon1.getHeldItemMainhand();
        ItemStack heldItem2 = pixelmon2.getHeldItemMainhand();
        return heldItem1 != null && heldItem1.getItem() == PixelmonItemsHeld.lightBall || heldItem2 != null && heldItem2.getItem() == PixelmonItemsHeld.lightBall;
    }

    public boolean getIfEggIsShiny(EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        float intDifferentTrainerFactor = 1.0f;
        if (!pixelmon1.originalTrainerUUID.equalsIgnoreCase(pixelmon2.originalTrainerUUID)) {
            intDifferentTrainerFactor = 2.0f;
        }
        return PixelmonConfig.shinyRate != 0.0f && this.rand.nextFloat() < intDifferentTrainerFactor / PixelmonConfig.shinyRate;
    }

    @Deprecated
    public static EnumPokeball getMotherPokeball(EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        return pixelmon1.gender == Gender.Female && pixelmon1.caughtBall != EnumPokeball.MasterBall && pixelmon1.caughtBall != EnumPokeball.CherishBall ? pixelmon1.caughtBall : (pixelmon2.gender == Gender.Female && pixelmon2.caughtBall != EnumPokeball.MasterBall && pixelmon2.caughtBall != EnumPokeball.CherishBall ? pixelmon2.caughtBall : EnumPokeball.PokeBall);
    }

    public static EnumPokeball getInheritedPokeball(EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        if (pixelmon1.getGender() == Gender.Male || pixelmon2.getGender() == Gender.Male) {
            EnumPokeball fatherBall;
            EnumPokeball enumPokeball = fatherBall = pixelmon1.getGender() == Gender.Male ? pixelmon1.caughtBall : pixelmon2.caughtBall;
            if (pixelmon1.getSpecies() == pixelmon2.getSpecies() && Math.random() >= 0.5) {
                return fatherBall != EnumPokeball.MasterBall && fatherBall != EnumPokeball.CherishBall ? fatherBall : EnumPokeball.PokeBall;
            }
            if (pixelmon1.getGender() == Gender.None || pixelmon2.getGender() == Gender.None) {
                return fatherBall != EnumPokeball.MasterBall && fatherBall != EnumPokeball.CherishBall ? fatherBall : EnumPokeball.PokeBall;
            }
        }
        return pixelmon1.gender == Gender.Female && pixelmon1.caughtBall != EnumPokeball.MasterBall && pixelmon1.caughtBall != EnumPokeball.CherishBall ? pixelmon1.caughtBall : (pixelmon2.gender == Gender.Female && pixelmon2.caughtBall != EnumPokeball.MasterBall && pixelmon2.caughtBall != EnumPokeball.CherishBall ? pixelmon2.caughtBall : EnumPokeball.PokeBall);
    }

    public static Integer getEggAbilitySlot(String[] abilities, EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        EntityPixelmon inheritParent;
        boolean is1Ditto = pixelmon1.isPokemon(EnumSpecies.Ditto);
        boolean is2Ditto = pixelmon2.isPokemon(EnumSpecies.Ditto);
        int intPercent = 80;
        if (!is1Ditto && !is2Ditto) {
            inheritParent = pixelmon1.gender == Gender.Female ? pixelmon1 : pixelmon2;
        } else {
            if (is1Ditto && is2Ditto) {
                return Entity10CanBreed.getRandomNormalAbilitySlot(abilities);
            }
            EntityPixelmon entityPixelmon = inheritParent = is1Ditto ? pixelmon2 : pixelmon1;
            if (inheritParent.gender == Gender.Male) {
                intPercent = 60;
            }
        }
        if (RandomHelper.getRandomChance(intPercent)) {
            int currentSlot = inheritParent.getAbilitySlot();
            return abilities[currentSlot] != null ? currentSlot : 0;
        }
        return Entity10CanBreed.getRandomNormalAbilitySlot(abilities);
    }

    public static IVStore getIVsForEgg(EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        ArrayList<String> masterStatsList = new ArrayList<String>(Arrays.asList("HP1", "ATK1", "DEF1", "SPATK1", "SPDEF1", "SPD1", "HP2", "ATK2", "DEF2", "SPATK2", "SPDEF2", "SPD2"));
        ArrayList<String> strStatsToInherit = new ArrayList<String>();
        IVStore newIVs = IVStore.createNewIVs();
        int intNumberToInherit = 3;
        ItemHeld heldItem1 = pixelmon1.getItemHeld();
        ItemHeld heldItem2 = pixelmon2.getItemHeld();
        if (heldItem1 == PixelmonItemsHeld.destinyKnot || heldItem2 == PixelmonItemsHeld.destinyKnot) {
            intNumberToInherit = 5;
        }
        boolean skipSecondHeldItem = false;
        if (heldItem1 != NoItem.noItem) {
            if (heldItem1 == PixelmonItemsHeld.powerWeight) {
                strStatsToInherit.add("HP1");
                --intNumberToInherit;
            } else if (heldItem1 == PixelmonItemsHeld.powerBracer) {
                strStatsToInherit.add("ATK1");
                --intNumberToInherit;
            } else if (heldItem1 == PixelmonItemsHeld.powerBelt) {
                strStatsToInherit.add("DEF1");
                --intNumberToInherit;
            } else if (heldItem1 == PixelmonItemsHeld.powerLens) {
                strStatsToInherit.add("SPATK1");
                --intNumberToInherit;
            } else if (heldItem1 == PixelmonItemsHeld.powerBand) {
                strStatsToInherit.add("SPDEF1");
                --intNumberToInherit;
            } else if (heldItem1 == PixelmonItemsHeld.powerAnklet) {
                strStatsToInherit.add("SPD1");
                --intNumberToInherit;
            }
            if (heldItem2 != NoItem.noItem && heldItem1 == heldItem2) {
                if (RandomHelper.getRandomChance()) {
                    skipSecondHeldItem = true;
                } else {
                    strStatsToInherit = new ArrayList();
                }
            }
        }
        if (!strStatsToInherit.isEmpty()) {
            for (String strStat : strStatsToInherit) {
                masterStatsList.remove(strStat);
                if (strStat.contains("1")) {
                    masterStatsList.remove(RegexPatterns.NUMBER_ONE.matcher(strStat).replaceAll("2"));
                    continue;
                }
                masterStatsList.remove(RegexPatterns.NUMBER_TWO.matcher(strStat).replaceAll("1"));
            }
        }
        if (!skipSecondHeldItem && heldItem2 != NoItem.noItem) {
            if (heldItem2 == PixelmonItemsHeld.powerWeight) {
                strStatsToInherit.add("HP2");
                --intNumberToInherit;
            } else if (heldItem2 == PixelmonItemsHeld.powerBracer) {
                strStatsToInherit.add("ATK2");
                --intNumberToInherit;
            } else if (heldItem2 == PixelmonItemsHeld.powerBelt) {
                strStatsToInherit.add("DEF2");
                --intNumberToInherit;
            } else if (heldItem2 == PixelmonItemsHeld.powerLens) {
                strStatsToInherit.add("SPATK2");
                --intNumberToInherit;
            } else if (heldItem2 == PixelmonItemsHeld.powerBand) {
                strStatsToInherit.add("SPDEF2");
                --intNumberToInherit;
            } else if (heldItem2 == PixelmonItemsHeld.powerAnklet) {
                strStatsToInherit.add("SPD2");
                --intNumberToInherit;
            }
        }
        if (!strStatsToInherit.isEmpty()) {
            for (String strStat : strStatsToInherit) {
                masterStatsList.remove(strStat);
                if (strStat.contains("1")) {
                    masterStatsList.remove(RegexPatterns.NUMBER_ONE.matcher(strStat).replaceAll("2"));
                    continue;
                }
                masterStatsList.remove(RegexPatterns.NUMBER_TWO.matcher(strStat).replaceAll("1"));
            }
        }
        for (int i = 0; i < intNumberToInherit; ++i) {
            String strStat;
            strStat = RandomHelper.getRandomElementFromList(masterStatsList);
            strStatsToInherit.add(strStat);
            masterStatsList.remove(strStat);
            if (strStat.contains("1")) {
                masterStatsList.remove(RegexPatterns.NUMBER_ONE.matcher(strStat).replaceAll("2"));
                continue;
            }
            masterStatsList.remove(RegexPatterns.NUMBER_TWO.matcher(strStat).replaceAll("1"));
        }
        for (String strStat : strStatsToInherit) {
            if (strStat.equalsIgnoreCase("HP1")) {
                newIVs.HP = pixelmon1.stats.IVs.HP;
                continue;
            }
            if (strStat.equalsIgnoreCase("ATK1")) {
                newIVs.Attack = pixelmon1.stats.IVs.Attack;
                continue;
            }
            if (strStat.equalsIgnoreCase("DEF1")) {
                newIVs.Defence = pixelmon1.stats.IVs.Defence;
                continue;
            }
            if (strStat.equalsIgnoreCase("SPATK1")) {
                newIVs.SpAtt = pixelmon1.stats.IVs.SpAtt;
                continue;
            }
            if (strStat.equalsIgnoreCase("SPDEF1")) {
                newIVs.SpDef = pixelmon1.stats.IVs.SpDef;
                continue;
            }
            if (strStat.equalsIgnoreCase("SPD1")) {
                newIVs.Speed = pixelmon1.stats.IVs.Speed;
                continue;
            }
            if (strStat.equalsIgnoreCase("HP2")) {
                newIVs.HP = pixelmon2.stats.IVs.HP;
                continue;
            }
            if (strStat.equalsIgnoreCase("ATK2")) {
                newIVs.Attack = pixelmon2.stats.IVs.Attack;
                continue;
            }
            if (strStat.equalsIgnoreCase("DEF2")) {
                newIVs.Defence = pixelmon2.stats.IVs.Defence;
                continue;
            }
            if (strStat.equalsIgnoreCase("SPATK2")) {
                newIVs.SpAtt = pixelmon2.stats.IVs.SpAtt;
                continue;
            }
            if (strStat.equalsIgnoreCase("SPDEF2")) {
                newIVs.SpDef = pixelmon2.stats.IVs.SpDef;
                continue;
            }
            if (!strStat.equalsIgnoreCase("SPD2")) continue;
            newIVs.Speed = pixelmon2.stats.IVs.Speed;
        }
        return newIVs;
    }

    public Moveset getEggMoveset(Entity10CanBreed eggPokemon, EnumSpecies pokemonSpecies, EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        ArrayList<Attack> possibleEggMoves = DatabaseMoves.getAllEggAttacks(eggPokemon);
        ArrayList<Attack> lvl0Moves = DatabaseMoves.getAttacksAtLevel(eggPokemon, 0);
        ArrayList<Attack> levelupMoves = Entity10CanBreed.getLevelupMoves(eggPokemon, pixelmon1, pixelmon2);
        EntityPixelmon father = Entity10CanBreed.returnFather(pixelmon1, pixelmon2);
        ArrayList<Attack> fathersTMHMTutorMoves = Entity10CanBreed.getFathersTMHMTutorMoves(eggPokemon, father);
        ArrayList<Attack> fathersEggMoves = Entity10CanBreed.getEggMoves(eggPokemon, father, possibleEggMoves);
        ArrayList<Attack> mothersEggMoves = Entity10CanBreed.getEggMoves(eggPokemon, Entity10CanBreed.returnMother(pixelmon1, pixelmon2), possibleEggMoves);
        boolean canLearnVoltTackle = Entity10CanBreed.canLearnVoltTackle(pokemonSpecies, pixelmon1, pixelmon2);
        ArrayList<Attack> masterAttackList = new ArrayList<Attack>();
        if (canLearnVoltTackle) {
            Attack voltTackle = new Attack("Volt Tackle");
            masterAttackList.add(voltTackle);
            possibleEggMoves.add(voltTackle);
        }
        Entity10CanBreed.addAttacksToList(masterAttackList, mothersEggMoves);
        Entity10CanBreed.addAttacksToList(masterAttackList, fathersEggMoves);
        Entity10CanBreed.addAttacksToList(masterAttackList, fathersTMHMTutorMoves);
        Entity10CanBreed.addAttacksToList(masterAttackList, levelupMoves);
        Entity10CanBreed.addAttacksToList(masterAttackList, lvl0Moves);
        if (pixelmon1.isAlpha() && pixelmon2.isAlpha()) {
            ArrayList<Attack> alphaMoves = DatabaseMoves.getAllAlphaAttacks(eggPokemon.getBaseStats().id);
            Entity10CanBreed.addAttacksToList(masterAttackList, alphaMoves);
        }
        Moveset moveset = Entity10CanBreed.getFirstFourMoves(eggPokemon, masterAttackList);
        ArrayList<Attack> knownEggMoves = new ArrayList<Attack>(4);
        for (Attack move : moveset) {
            if (move == null || !possibleEggMoves.contains(move)) continue;
            knownEggMoves.add(move);
        }
        int numEggMoves = knownEggMoves.size();
        this.eggMoves = new int[numEggMoves];
        for (int i = 0; i < numEggMoves; ++i) {
            this.eggMoves[i] = ((Attack)knownEggMoves.get((int)i)).getAttackBase().attackIndex;
        }
        return moveset;
    }

    public static ArrayList<Attack> getEggMoves(Entity6CanBattle pokemon, EntityPixelmon pixelmon1, ArrayList<Attack> allEggMoves) {
        ArrayList<Attack> eggMoves = new ArrayList<Attack>();
        if (pixelmon1 != null) {
            ArrayList<Attack> allBabyEggMoves = DatabaseMoves.getAllEggAttacks(pokemon);
            ArrayList<Attack> allParentsMoves = new ArrayList<Attack>(Arrays.asList(pixelmon1.getMoveset().attacks));
            eggMoves.addAll(allParentsMoves.stream().filter(attack -> Entity10CanBreed.listContains(allBabyEggMoves, attack)).collect(Collectors.toList()));
        }
        return eggMoves;
    }

    private static void addAttacksToList(ArrayList<Attack> masterList, ArrayList<Attack> addList) {
        for (Attack attack : addList) {
            if (masterList.contains(attack)) continue;
            masterList.add(attack);
        }
    }

    private static boolean listContains(ArrayList<Attack> list, Attack attack) {
        return list.contains(attack);
    }

    public static ArrayList<Attack> getFathersTMHMTutorMoves(Entity6CanBattle pokemon, EntityPixelmon pixelmon1) {
        ArrayList<Attack> tmhmTutorMoves = new ArrayList<Attack>();
        if (pixelmon1 != null) {
            ArrayList<Attack> allBabyTMHMTutorMoves = DatabaseMoves.getAllTMHMTutorAttacks(pokemon);
            ArrayList<Attack> allFathersMoves = new ArrayList<Attack>(Arrays.asList(pixelmon1.getMoveset().attacks));
            tmhmTutorMoves.addAll(allFathersMoves.stream().filter(attack -> Entity10CanBreed.listContains(allBabyTMHMTutorMoves, attack)).collect(Collectors.toList()));
        }
        return tmhmTutorMoves;
    }

    public static EntityPixelmon returnFather(EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        boolean is1Ditto = pixelmon1.isPokemon(EnumSpecies.Ditto);
        boolean is2Ditto = pixelmon2.isPokemon(EnumSpecies.Ditto);
        return !is1Ditto && !is2Ditto ? (pixelmon1.gender == Gender.Male ? pixelmon1 : pixelmon2) : (is1Ditto && pixelmon2.gender != Gender.Female ? pixelmon2 : (is2Ditto && pixelmon1.gender != Gender.Female ? pixelmon1 : null));
    }

    public static EntityPixelmon returnMother(EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        boolean is1Ditto = pixelmon1.isPokemon(EnumSpecies.Ditto);
        boolean is2Ditto = pixelmon1.isPokemon(EnumSpecies.Ditto);
        return !is1Ditto && !is2Ditto ? (pixelmon1.gender == Gender.Female ? pixelmon1 : pixelmon2) : (is1Ditto && pixelmon2.gender != Gender.Male ? pixelmon2 : (is2Ditto && pixelmon1.gender != Gender.Male ? pixelmon1 : null));
    }

    public static ArrayList<Attack> getLevelupMoves(Entity6CanBattle pokemon, EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        ArrayList<Attack> allBabyAttacks = DatabaseMoves.getAllAttacks(pokemon);
        ArrayList<Attack> pixelmon1Attacks = new ArrayList<Attack>(Arrays.asList(pixelmon1.getMoveset().attacks));
        ArrayList<Attack> pixelmon2Attacks = new ArrayList<Attack>(Arrays.asList(pixelmon2.getMoveset().attacks));
        return (ArrayList)allBabyAttacks.stream().filter(attack -> Entity10CanBreed.listContains(pixelmon1Attacks, attack) && Entity10CanBreed.listContains(pixelmon2Attacks, attack)).collect(Collectors.toList());
    }

    public static Moveset getFirstFourMoves(Entity6CanBattle pokemon, ArrayList<Attack> masterAttackList) {
        Moveset moveset = new Moveset(pokemon);
        moveset.set(0, DatabaseMoves.getAttack("Tackle"));
        moveset.set(1, null);
        moveset.set(2, null);
        moveset.set(3, null);
        if (!masterAttackList.isEmpty()) {
            for (int i = 0; i < Math.min(4, masterAttackList.size()); ++i) {
                if (masterAttackList.get(i) == null) continue;
                moveset.set(i, masterAttackList.get(i));
            }
        }
        return moveset;
    }

    public static boolean canBreed(EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        EnumEggGroup[] groupsFather;
        boolean is1Ditto = pixelmon1.isPokemon(EnumSpecies.Ditto);
        boolean is2Ditto = pixelmon2.isPokemon(EnumSpecies.Ditto);
        EnumSpecies species1 = pixelmon1.getSpecies();
        EnumSpecies species2 = pixelmon2.getSpecies();
        if (!is1Ditto && !is2Ditto) {
            if (pixelmon1.gender == pixelmon2.gender) {
                return false;
            }
            if (pixelmon1.gender != Gender.None && pixelmon2.gender != Gender.None) {
                EnumEggGroup[] groupsMother;
                EnumEggGroup[] groupsFather2;
                if (pixelmon1.gender == Gender.Male) {
                    groupsFather2 = EnumEggGroup.getEggGroups(species1);
                    groupsMother = EnumEggGroup.getEggGroups(species2);
                } else {
                    groupsFather2 = EnumEggGroup.getEggGroups(species2);
                    groupsMother = EnumEggGroup.getEggGroups(species1);
                }
                if (groupsFather2 != null && groupsMother != null) {
                    for (EnumEggGroup groupFather : groupsFather2) {
                        for (EnumEggGroup groupMother : groupsMother) {
                            if (groupFather != groupMother || groupMother == EnumEggGroup.Undiscovered) continue;
                            return true;
                        }
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        if (is1Ditto && is2Ditto) {
            return PixelmonConfig.allowDittoDittoBreeding;
        }
        for (EnumEggGroup groupNonDitto : groupsFather = is1Ditto ? EnumEggGroup.getEggGroups(species2) : EnumEggGroup.getEggGroups(species1)) {
            if (groupNonDitto != EnumEggGroup.Undiscovered) continue;
            return false;
        }
        return true;
    }

    public static EnumSpecies getPokemonInEggName(EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        boolean inherit1;
        boolean is1Ditto = pixelmon1.isPokemon(EnumSpecies.Ditto);
        boolean is2Ditto = pixelmon2.isPokemon(EnumSpecies.Ditto);
        ItemStack item1 = pixelmon1.getHeldItemMainhand();
        ItemStack item2 = pixelmon2.getHeldItemMainhand();
        if (!is1Ditto && !is2Ditto) {
            inherit1 = pixelmon2.gender == Gender.Male;
        } else {
            if (is1Ditto && is2Ditto) {
                return EnumSpecies.randomPoke(PixelmonConfig.allowRandomBreedingEggsToBeLegendary);
            }
            inherit1 = is2Ditto;
        }
        return inherit1 ? Entity10CanBreed.getEggForm(pixelmon1, item1, item2) : Entity10CanBreed.getEggForm(pixelmon2, item2, item1);
    }

    public static EnumSpecies getEggForm(EntityPixelmon parentForEggLine, ItemStack itemParentForEggLine, ItemStack itemOtherParent) {
        if (parentForEggLine.isPokemon(EnumSpecies.Nidoranfemale, EnumSpecies.Nidoranmale, EnumSpecies.Nidorino, EnumSpecies.Nidoking)) {
            EnumSpecies[] eggForms = new EnumSpecies[]{EnumSpecies.Nidoranfemale, EnumSpecies.Nidoranmale};
            return RandomHelper.getRandomElementFromArray(eggForms);
        }
        if (parentForEggLine.isPokemon(EnumSpecies.Illumise, EnumSpecies.Volbeat)) {
            EnumSpecies[] eggForms = new EnumSpecies[]{EnumSpecies.Illumise, EnumSpecies.Volbeat};
            return RandomHelper.getRandomElementFromArray(eggForms);
        }
        if (parentForEggLine.isPokemon(EnumSpecies.Manaphy)) {
            return EnumSpecies.Phione;
        }
        Item itemTypeParent = null;
        if (itemParentForEggLine != null) {
            itemTypeParent = itemParentForEggLine.getItem();
        }
        Item itemTypeOther = null;
        if (itemOtherParent != null) {
            itemTypeOther = itemOtherParent.getItem();
        }
        return Entity10CanBreed.checkIncense(itemTypeParent, itemTypeOther, parentForEggLine, PixelmonItemsHeld.seaIncense, EnumSpecies.Azurill, EnumSpecies.Marill, EnumSpecies.Azumarill) ? EnumSpecies.Marill : (Entity10CanBreed.checkIncense(itemTypeParent, itemTypeOther, parentForEggLine, PixelmonItemsHeld.laxIncense, EnumSpecies.Wynaut, EnumSpecies.Wobbuffet) ? EnumSpecies.Wobbuffet : (Entity10CanBreed.checkIncense(itemTypeParent, itemTypeOther, parentForEggLine, PixelmonItemsHeld.roseIncense, EnumSpecies.Budew, EnumSpecies.Roselia, EnumSpecies.Roserade) ? EnumSpecies.Roselia : (Entity10CanBreed.checkIncense(itemTypeParent, itemTypeOther, parentForEggLine, PixelmonItemsHeld.pureIncense, EnumSpecies.Chingling, EnumSpecies.Chimecho) ? EnumSpecies.Chimecho : (Entity10CanBreed.checkIncense(itemTypeParent, itemTypeOther, parentForEggLine, PixelmonItemsHeld.rockIncense, EnumSpecies.Bonsly, EnumSpecies.Sudowoodo) ? EnumSpecies.Sudowoodo : (Entity10CanBreed.checkIncense(itemTypeParent, itemTypeOther, parentForEggLine, PixelmonItemsHeld.oddIncense, EnumSpecies.MimeJr, EnumSpecies.MrMime, EnumSpecies.MrRime) ? EnumSpecies.MrMime : (Entity10CanBreed.checkIncense(itemTypeParent, itemTypeOther, parentForEggLine, PixelmonItemsHeld.luckIncense, EnumSpecies.Happiny, EnumSpecies.Chansey, EnumSpecies.Blissey) ? EnumSpecies.Chansey : (Entity10CanBreed.checkIncense(itemTypeParent, itemTypeOther, parentForEggLine, PixelmonItemsHeld.waveIncense, EnumSpecies.Mantyke, EnumSpecies.Mantine) ? EnumSpecies.Mantine : (Entity10CanBreed.checkIncense(itemTypeParent, itemTypeOther, parentForEggLine, PixelmonItemsHeld.fullIncense, EnumSpecies.Munchlax, EnumSpecies.Snorlax) ? EnumSpecies.Snorlax : Entity10CanBreed.getBasicPokemonForm(parentForEggLine.baseStats.pokemon)))))))));
    }

    private static boolean checkIncense(Item itemTypeParent, Item itemTypeOther, EntityPixelmon parentForEggLine, Item neededItem, EnumSpecies baby, EnumSpecies ... parents) {
        return parentForEggLine.isPokemon(parents) && (itemTypeParent != neededItem && itemTypeOther != neededItem || parentForEggLine.isAvailableGeneration() && !Entity10CanBreed.isAvailableGeneration(baby));
    }

    public static EnumSpecies getBasicPokemonForm(EnumSpecies pokemon) {
        try {
            Optional<BaseStats> optional = DatabaseStats.getBaseStats(pokemon.name);
            if (optional.isPresent()) {
                BaseStats store = optional.get();
                if (store.preEvolutions != null && store.preEvolutions.length > 0) {
                    EnumSpecies preEvolution = store.preEvolutions[0];
                    return !Entity10CanBreed.isAvailableGeneration(preEvolution) && Entity10CanBreed.isAvailableGeneration(pokemon) ? pokemon : Entity10CanBreed.getBasicPokemonForm(preEvolution);
                }
                return pokemon;
            }
            throw new IllegalStateException("Could not load stats from database!");
        }
        catch (Exception var4) {
            Pixelmon.LOGGER.error("Error getting basic Pok\u00c3\u00a9mon form for " + pokemon.name + ".");
            var4.printStackTrace();
            return pokemon;
        }
    }

    public String getEggDescripion() {
        return Entity10CanBreed.getEggDescription(this.eggCycles);
    }

    public static String getEggDescription(Integer cycles) {
        return cycles < 5 ? I18n.translateToLocal("pixelmon.egg.stage1") : (cycles < 10 ? I18n.translateToLocal("pixelmon.egg.stage2") : (cycles < 40 ? I18n.translateToLocal("pixelmon.egg.stage3") : I18n.translateToLocal("pixelmon.egg.stage4")));
    }

    public EnumEggGroup getRandomEggGroupForPokemon() {
        return Entity10CanBreed.getRandomEggGroupForPokemon(this.getSpecies());
    }

    public static EnumEggGroup getRandomEggGroupForPokemon(EnumSpecies pokemon) {
        EnumEggGroup[] eggGroups = EnumEggGroup.getEggGroups(pokemon);
        try {
            return eggGroups[RandomHelper.rand.nextInt(eggGroups.length)];
        }
        catch (Exception var3) {
            Pixelmon.LOGGER.error("Error finding Egg Group for Pok\u00c3\u00a9mon: " + pokemon.name + ".");
            var3.printStackTrace();
            return null;
        }
    }

    public static EnumEggGroup getEggGroupForPair(EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        EnumEggGroup[] groups1 = EnumEggGroup.getEggGroups(pixelmon1.getSpecies());
        EnumEggGroup[] groups2 = EnumEggGroup.getEggGroups(pixelmon2.getSpecies());
        ArrayList<EnumEggGroup> groupsEgg = new ArrayList<EnumEggGroup>();
        for (EnumEggGroup group1 : groups1) {
            for (EnumEggGroup group2 : groups2) {
                if (group1 != group2 || group2 == EnumEggGroup.Undiscovered) continue;
                groupsEgg.add(group2);
            }
        }
        if (groupsEgg.isEmpty()) {
            return EnumEggGroup.Undiscovered;
        }
        return (EnumEggGroup)((Object)RandomHelper.getRandomElementFromList(groupsEgg));
    }

    public static EnumGrowth getEggGrowth(EntityPixelmon pixelmon1, EntityPixelmon pixelmon2) {
        double averageOrdinal = (pixelmon1.getGrowth().scaleOrdinal + pixelmon2.getGrowth().scaleOrdinal) / 2;
        int ordinal = (int)averageOrdinal;
        if (ordinal <= 0) {
            ordinal = 1;
        }
        if (ordinal >= 8) {
            ordinal = 7;
        }
        int rand = RandomHelper.getRandomNumberBetween(ordinal - 1, ordinal + 1);
        return EnumGrowth.getGrowthFromScaleOrdinal(rand);
    }

    @Override
    public boolean attackEntityFrom(DamageSource source, float amount) {
        return !this.isInRanchBlock && super.attackEntityFrom(source, amount);
    }

    public void resetBreedingLevel() {
        this.setNumBreedingLevels(0);
        this.lastBreedingTime = this.world.getTotalWorldTime();
    }

    public boolean readyToMakeEgg() {
        return this.getNumBreedingLevels() >= PixelmonConfig.numBreedingLevels;
    }

    public ParticleEffects[] getBreedingParticleEffects() {
        ArrayList<BreedingParticles> effects = new ArrayList<BreedingParticles>();
        if (this.getNumBreedingLevels() > 0) {
            effects.add(new BreedingParticles(this));
        }
        return !effects.isEmpty() ? effects.toArray(new ParticleEffects[effects.size()]) : null;
    }

    public void updateBreeding() {
        if (this.getNumBreedingLevels() >= PixelmonConfig.numBreedingLevels) {
            this.setNumBreedingLevels(PixelmonConfig.numBreedingLevels);
        } else {
            this.refreshBreedingStrength();
            TileEntityRanchBase ranch = ((RanchBounds)this.blockOwner.getBounds()).ranch;
            if (this.breedingStrength == EnumBreedingStrength.NONE) {
                this.lastBreedingTime = this.world.getTotalWorldTime();
                if (this.getNumBreedingLevels() > 0) {
                    BreedEvent.BreedingLevelChangedEvent event = new BreedEvent.BreedingLevelChangedEvent(this.getOwnerId(), ranch, (EntityPixelmon)this, this.getNumBreedingLevels(), this.getNumBreedingLevels() - 1);
                    MinecraftForge.EVENT_BUS.post(event);
                    this.setNumBreedingLevels(event.getNewLevel());
                }
            } else {
                long currentTime = this.world.getTotalWorldTime();
                int breedingTicks = PixelmonConfig.breedingTicks;
                BreedEvent.BreedingTickEvent ticksEvent = new BreedEvent.BreedingTickEvent(this.getOwnerId(), ranch, (EntityPixelmon)this, breedingTicks);
                MinecraftForge.EVENT_BUS.post(ticksEvent);
                breedingTicks = ticksEvent.getBreedingTicks();
                while ((float)(currentTime - this.lastBreedingTime) - (float)breedingTicks / this.breedingStrength.value >= 0.0f) {
                    BreedEvent.BreedingLevelChangedEvent levelChangedEvent = new BreedEvent.BreedingLevelChangedEvent(this.getOwnerId(), ranch, (EntityPixelmon)this, this.getNumBreedingLevels(), this.getNumBreedingLevels() + 1);
                    MinecraftForge.EVENT_BUS.post(levelChangedEvent);
                    this.setNumBreedingLevels(levelChangedEvent.getNewLevel());
                    this.lastBreedingTime = (long)((float)this.lastBreedingTime + (float)breedingTicks / this.breedingStrength.value);
                }
                if (this.getNumBreedingLevels() == PixelmonConfig.numBreedingLevels && this.blockOwner != null) {
                    this.blockOwner.updateStatus();
                }
            }
        }
    }

    public void refreshBreedingStrength() {
        float breedingStrengthFloat;
        if (PixelmonConfig.useBreedingEnvironment) {
            BreedingConditions conditions = ((RanchBounds)this.blockOwner.getBounds()).getContainingBreedingConditions(this.world);
            breedingStrengthFloat = conditions.getBreedingStrength(this.type);
        } else {
            breedingStrengthFloat = 2.0f;
        }
        RanchBounds bounds = (RanchBounds)this.blockOwner.getBounds();
        TileEntityRanchBase ranch = bounds.ranch;
        BreedEvent.EnvironmentStrengthEvent strengthEvent = new BreedEvent.EnvironmentStrengthEvent(this.getOwnerId(), ranch, (EntityPixelmon)this, bounds, breedingStrengthFloat);
        MinecraftForge.EVENT_BUS.post(strengthEvent);
        this.breedingStrength = EnumBreedingStrength.of(strengthEvent.getBreedingStrength());
    }
}

