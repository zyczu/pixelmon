/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.helpers;

import com.pixelmongenerations.api.enums.ReceiveType;
import com.pixelmongenerations.api.events.EvolveEvent;
import com.pixelmongenerations.api.events.PixelmonReceivedEvent;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQueryList;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.Level;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.Evolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.LevelingEvolution;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.config.PixelmonItemsPokeballs;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.packetHandlers.OpenReplaceMoveScreen;
import com.pixelmongenerations.core.network.packetHandlers.evolution.EvolutionStage;
import com.pixelmongenerations.core.network.packetHandlers.evolution.EvolvePokemon;
import com.pixelmongenerations.core.network.packetHandlers.evolution.OpenEvolutionGUI;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.AirSaver;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.NetworkRegistry;

public class EvolutionQuery {
    EntityPixelmon pixelmon;
    Evolution evolution = null;
    PokemonSpec newPokemon;
    public int[] pokemonID;
    boolean fromLevelUp;
    int level;
    int newForm;
    EntityPlayerMP player;
    boolean normalEvolution = true;
    EvolutionStage stage;
    protected AirSaver airSaver;
    int ticks = 0;

    public EvolutionQuery(EntityPixelmon pixelmon, Evolution evolution) {
        this.pixelmon = pixelmon;
        this.newPokemon = evolution.to;
        this.pokemonID = pixelmon.getPokemonId();
        this.evolution = evolution;
        this.fromLevelUp = evolution instanceof LevelingEvolution;
        if (pixelmon.getOwner() != null) {
            this.player = (EntityPlayerMP)pixelmon.getOwner();
            this.airSaver = new AirSaver(this.player);
        }
        this.sendQuery();
        this.level = pixelmon.getLvl().getLevel();
        pixelmon.tasks.taskEntries.clear();
        this.setStage(EvolutionStage.PreChoice);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public EvolutionQuery(EntityPixelmon pixelmon, int form) {
        List<EvolutionQuery> list;
        this.normalEvolution = false;
        this.pixelmon = pixelmon;
        this.pokemonID = pixelmon.getPokemonId();
        this.fromLevelUp = false;
        this.newForm = form;
        if (pixelmon.getOwner() != null) {
            this.player = (EntityPlayerMP)pixelmon.getOwner();
            this.airSaver = new AirSaver(this.player);
        }
        this.level = pixelmon.getLvl().getLevel();
        pixelmon.tasks.taskEntries.clear();
        this.setStage(EvolutionStage.PreAnimation);
        this.updateAllAround(this.stage);
        List<EvolutionQuery> list2 = list = EvolutionQueryList.queryList;
        synchronized (list2) {
            EvolutionQueryList.queryList.add(this);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void tick(World world) {
        if (this.pixelmon.world != world) {
            return;
        }
        if (this.airSaver != null) {
            this.airSaver.tick();
        }
        if (this.stage == EvolutionStage.Choice) {
            return;
        }
        ++this.ticks;
        if (this.stage == EvolutionStage.PreChoice) {
            if (this.ticks >= this.stage.ticks) {
                this.ticks = 0;
                this.setStage(EvolutionStage.Choice);
            }
        } else if (this.stage == EvolutionStage.PreAnimation) {
            if (this.ticks >= this.stage.ticks) {
                this.ticks = 0;
                this.setStage(EvolutionStage.PostAnimation);
                this.updateAllAround(this.stage);
                this.doEvoSwitch();
            }
        } else if (this.stage == EvolutionStage.PostAnimation && this.ticks >= this.stage.ticks) {
            List<EvolutionQuery> list;
            this.ticks = 0;
            this.setStage(EvolutionStage.End);
            if (this.evolution != null) {
                this.evolution.finishedEvolving(this.pixelmon);
            }
            this.updateAllAround(this.stage);
            List<EvolutionQuery> list2 = list = EvolutionQueryList.queryList;
            synchronized (list2) {
                EvolutionQueryList.queryList.remove(this);
            }
        }
    }

    protected void setStage(EvolutionStage stage) {
        this.stage = stage;
        this.pixelmon.evoStage = stage;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void sendQuery() {
        if (this.pixelmon.hasOwner()) {
            List<EvolutionQuery> list;
            Pixelmon.NETWORK.sendTo(new OpenEvolutionGUI(this.pokemonID, this.newPokemon.name), this.player);
            this.updateAllAround(EvolutionStage.Choice);
            this.removeExisting();
            List<EvolutionQuery> list2 = list = EvolutionQueryList.queryList;
            synchronized (list2) {
                EvolutionQueryList.queryList.add(this);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void removeExisting() {
        List<EvolutionQuery> list;
        List<EvolutionQuery> list2 = list = EvolutionQueryList.queryList;
        synchronized (list2) {
            for (int i = 0; i < EvolutionQueryList.queryList.size(); ++i) {
                EvolutionQuery query = EvolutionQueryList.queryList.get(i);
                if (!PixelmonMethods.isIDSame(query.pokemonID, this.pokemonID)) continue;
                EvolutionQueryList.queryList.remove(i);
                break;
            }
        }
    }

    void accept() {
        this.setStage(EvolutionStage.PreAnimation);
        this.ticks = 0;
        this.updateAllAround(this.stage);
    }

    void doEvoSwitch() {
        if (this.normalEvolution) {
            EntityPixelmon pre = (EntityPixelmon)PixelmonEntityList.createEntityByName(this.pixelmon.getPokemonName(), this.pixelmon.world);
            this.pixelmon.evolve(this.newPokemon);
            this.checkShedinja();
            this.checkForLearnMoves();
            this.evolution.finishedEvolving(this.pixelmon);
            MinecraftForge.EVENT_BUS.post(new EvolveEvent.PostEvolve(this.player, pre, this.evolution, this.pixelmon));
            if (!this.pixelmon.getPokemonName().equals(EnumSpecies.Shedinja.name)) {
                MinecraftForge.EVENT_BUS.post(new PixelmonReceivedEvent(this.player, ReceiveType.Evolution, this.pixelmon));
            }
        } else {
            this.pixelmon.setForm(this.newForm);
            this.pixelmon.playSound(SoundEvents.ENTITY_GENERIC_EXPLODE, 1.0f, 1.0f);
            if (this.pixelmon.battleController == null) {
                this.pixelmon.resetAI();
                this.pixelmon.setBlockTarget((int)this.pixelmon.posX, (int)this.pixelmon.posY, (int)this.pixelmon.posZ, EnumFacing.SOUTH, -1);
            }
        }
    }

    private void removeEntity() {
        this.pixelmon.unloadEntity();
    }

    void decline() {
        this.removeEntity();
    }

    private void checkShedinja() {
        EntityPlayerMP player;
        PlayerStorage storage;
        Optional<PlayerStorage> optstorage;
        if (this.pixelmon.baseStats.pokemon == EnumSpecies.Ninjask && (optstorage = this.pixelmon.getStorage()).isPresent() && (storage = optstorage.get()).hasSpace() && (player = storage.getPlayer()) != null && player.inventory.clearMatchingItems(PixelmonItemsPokeballs.pokeBall, 0, 1, null) == 1) {
            EntityPixelmon shedinja = new EntityPixelmon(player.world);
            shedinja.init(EnumSpecies.Shedinja.name);
            Level shedinjaLevel = shedinja.getLvl();
            Level ninjaskLevel = this.pixelmon.getLvl();
            shedinjaLevel.setLevel(ninjaskLevel.getLevel());
            shedinjaLevel.setExp(ninjaskLevel.getExp());
            Moveset moveset = shedinja.getMoveset();
            moveset.clear();
            moveset.addAll(this.pixelmon.getMoveset());
            shedinja.status = (StatusPersist)this.pixelmon.status.copy();
            shedinja.setShiny(this.pixelmon.isShiny());
            shedinja.setGrowth(this.pixelmon.getGrowth());
            shedinja.friendship.setFriendship(this.pixelmon.friendship.getFriendship());
            shedinja.setNature(this.pixelmon.getNature());
            shedinja.stats.EVs = this.pixelmon.stats.EVs.cloneEVs();
            shedinja.stats.IVs.copyIVs(this.pixelmon.stats.IVs);
            shedinja.originalTrainer = this.pixelmon.originalTrainer;
            shedinja.originalTrainerUUID = this.pixelmon.originalTrainerUUID;
            MinecraftForge.EVENT_BUS.post(new PixelmonReceivedEvent(this.player, ReceiveType.Evolution, shedinja));
            storage.addToParty(shedinja);
        }
    }

    private void checkForLearnMoves() {
        String name = this.pixelmon.getPokemonName();
        Optional<BaseStats> optional = Entity3HasStats.getBaseStats(name, this.pixelmon.getForm());
        if (!optional.isPresent()) {
            return;
        }
        BaseStats baseStats = optional.get();
        this.pixelmon.baseStats.id = baseStats.id;
        this.pixelmon.baseStats.baseFormID = baseStats.baseFormID;
        int level = this.pixelmon.getLvl().getLevel();
        if (level == 1) {
            level = 0;
        }
        if (DatabaseMoves.learnsAttackAtLevel(this.pixelmon, level) || DatabaseMoves.hasEvolutionAttacks(this.pixelmon)) {
            ArrayList<Attack> newAttacks = DatabaseMoves.getAttacksAtLevel(this.pixelmon, level);
            newAttacks.addAll(DatabaseMoves.getAllEvolutionAttacks(this.pixelmon));
            Moveset moveset = this.pixelmon.getMoveset();
            newAttacks.stream().filter(a -> !moveset.hasAttack((Attack)a)).forEach(a -> {
                if (moveset.size() >= 4) {
                    EntityPlayerMP player = (EntityPlayerMP)this.pixelmon.getOwner();
                    Pixelmon.NETWORK.sendTo(new OpenReplaceMoveScreen(player.getUniqueID(), this.pixelmon.getPokemonId(), a.getAttackBase().attackIndex, 0, this.pixelmon.getLvl().getLevel()), player);
                } else {
                    moveset.add((Attack)a);
                    this.pixelmon.update(EnumUpdateType.Moveset);
                    if (BattleRegistry.getBattle((EntityPlayer)this.pixelmon.getOwner()) != null) {
                        ChatHandler.sendBattleMessage(this.pixelmon.getOwner(), "pixelmon.stats.learnedmove", this.pixelmon.getNickname(), a.getAttackBase().getLocalizedName());
                    } else {
                        ChatHandler.sendChat(this.pixelmon.getOwner(), "pixelmon.stats.learnedmove", this.pixelmon.getNickname(), a.getAttackBase().getLocalizedName());
                    }
                }
            });
        }
    }

    public boolean isEnded() {
        return this.stage == EvolutionStage.End;
    }

    protected void updateAllAround(EvolutionStage currentStage) {
        EntityLivingBase owner = this.pixelmon.getOwner();
        if (owner == null) {
            owner = this.pixelmon;
        }
        NetworkRegistry.TargetPoint point = new NetworkRegistry.TargetPoint(owner.dimension, owner.posX, owner.posY, owner.posZ, 60.0);
        Pixelmon.NETWORK.sendToAllAround(new EvolvePokemon(this.pokemonID, currentStage), point);
    }
}

