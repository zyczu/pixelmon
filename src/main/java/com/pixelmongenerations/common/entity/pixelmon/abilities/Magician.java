/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.item.ItemHeld;

public class Magician
extends AbilityBase {
    @Override
    public void tookDamageUser(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (!user.hasHeldItem() && target.hasHeldItem() && target.isItemRemovable(user)) {
            ItemHeld targetItem = target.getHeldItem();
            if (user.bc != null) {
                user.bc.sendToAll("pixelmon.abilities.magician", user.getNickname(), target.getNickname(), targetItem.getLocalizedName());
            }
            user.setNewHeldItem(targetItem);
            target.removeHeldItem();
            user.bc.enableReturnHeldItems(user, target);
        }
    }
}

