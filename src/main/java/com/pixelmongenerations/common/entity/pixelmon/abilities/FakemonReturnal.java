/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class FakemonReturnal
extends AbilityBase {
    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        PixelmonWrapper target = user.bc.getOppositePokemon(user);
        if (target.lastAttack != null && user.bc.isLastMover() && !user.attack.equals(user.choiceLocked)) {
            if (!user.hasStatus(StatusType.Taunt) && user.getHeldItem().getHeldItemType() != EnumHeldItems.assaultVest) {
                user.bc.sendToAll("pixelmon.abilities.returnal", user.getNickname());
                int targetBase = target.attack.baseAttack.basePower;
                int userBase = user.attack.baseAttack.basePower;
                user.attack.baseAttack.basePower = (int)((double)user.attack.baseAttack.basePower * 0.5);
                target.attack.baseAttack.basePower = (int)((double)target.attack.baseAttack.basePower * 0.5);
                user.useTempAttack(target.attack, false);
                target.attack.baseAttack.basePower = targetBase;
                user.attack.baseAttack.basePower = userBase;
                target.lastAttack = null;
            }
        }
        return stats;
    }
}

