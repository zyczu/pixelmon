/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

import com.pixelmongenerations.api.events.pokemon.MaxFriendshipEvent;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.network.EnumUpdateType;
import java.util.HashMap;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.MinecraftForge;

public class FriendShip {
    private static final int maxFriendship = 255;
    private static final int minFriendship = 0;
    private int friendship = 0;
    private PokemonLink pixelmon;
    boolean luxuryBall = false;
    private boolean hasMaxBefore = false;
    private int tickCounter = 0;

    public FriendShip(EntityPixelmon pixelmon) {
        this.pixelmon = new EntityLink(pixelmon);
    }

    public FriendShip(PixelmonWrapper pixelmon) {
        this.pixelmon = new WrapperLink(pixelmon);
    }

    public FriendShip(PokemonLink pixelmon) {
        this.pixelmon = pixelmon;
    }

    public void initFromCapture() {
        this.friendship = this.pixelmon.getBaseStats().baseFriendship;
    }

    public void increaseFriendship(int amount) {
        this.friendship += this.pixelmon.getHeldItem().getHeldItemType() == EnumHeldItems.sootheBell ? (int)((double)amount * 1.5) : amount;
        this.friendship = Math.min(this.friendship, 255);
        if (!this.hasMaxBefore && this.friendship == 255) {
            if (this.pixelmon.getEntity() != null && this.pixelmon.getPlayerOwner() != null) {
                MinecraftForge.EVENT_BUS.post(new MaxFriendshipEvent(this.pixelmon.getPlayerOwner(), this.pixelmon.getEntity()));
            }
            this.hasMaxBefore = true;
        }
        this.pixelmon.update(EnumUpdateType.Friendship);
    }

    public void decreaseFriendship(int amount) {
        this.friendship -= amount;
        this.friendship = Math.max(this.friendship, 0);
        this.pixelmon.update(EnumUpdateType.Friendship);
    }

    public int getFriendship() {
        return this.friendship;
    }

    public void initFromEgg() {
        this.friendship = 120;
    }

    public void resetFromTrade() {
        this.friendship = 70;
    }

    public boolean isFriendshipHighEnoughToEvolve() {
        return this.friendship >= 220;
    }

    public void writeToNBT(NBTTagCompound nbt) {
        nbt.setInteger("Friendship", this.friendship);
        nbt.setBoolean("LuxuryBall", this.luxuryBall);
        nbt.setBoolean("HasMaxBefore", this.hasMaxBefore);
    }

    public void readFromNBT(NBTTagCompound nbt) {
        this.friendship = nbt.getInteger("Friendship");
        this.luxuryBall = nbt.getBoolean("LuxuryBall");
        this.hasMaxBefore = nbt.getBoolean("HasMaxBefore");
    }

    private int luxuryBonus() {
        return this.luxuryBall ? 1 : 0;
    }

    public void tick() {
        ++this.tickCounter;
        if (this.tickCounter >= 800) {
            int amount = this.friendship < 200 ? 2 : 1;
            int n = amount;
            if (this.pixelmon.getHeldItem().getHeldItemType() == EnumHeldItems.sootheBell) {
                amount = (int)((double)amount * 1.5);
            }
            this.increaseFriendship(amount);
            this.tickCounter = 0;
        }
    }

    public void hurtByOwner() {
        this.decreaseFriendship(20);
    }

    public void onLevelUp() {
        int amount = this.friendship < 100 ? 5 : (this.friendship < 200 ? 3 : 2);
        this.increaseFriendship(amount + this.luxuryBonus());
    }

    public void onFaint() {
        this.decreaseFriendship(1);
    }

    public void captureLuxuryBall() {
        this.luxuryBall = true;
    }

    public void setFriendship(int i) {
        this.friendship = i;
    }

    public boolean berryFriendship() {
        if (this.friendship < 100) {
            this.increaseFriendship(10);
        } else if (this.friendship >= 100 && this.friendship < 200) {
            this.increaseFriendship(5);
        } else if (this.friendship >= 200 && this.friendship < 255) {
            this.increaseFriendship(2);
        } else {
            return false;
        }
        return true;
    }

    public void vitaminFriendship() {
        if (this.friendship < 100) {
            this.increaseFriendship(5);
        } else if (this.friendship >= 100 && this.friendship < 200) {
            this.increaseFriendship(3);
        } else if (this.friendship >= 200 && this.friendship < 255) {
            this.increaseFriendship(2);
        }
    }

    public boolean hasMaxBefore() {
        return this.hasMaxBefore;
    }

    public void getNBTTags(HashMap<String, Class> tags) {
        tags.put("Friendship", Integer.class);
        tags.put("LuxuryBall", Boolean.class);
        tags.put("HasMaxBefore", Boolean.class);
    }

    public static int getMaxFriendship() {
        return 255;
    }
}

