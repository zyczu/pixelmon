/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class FriendGuard
extends AbilityBase {
    @Override
    public int modifyDamageTeammate(int damage, PixelmonWrapper user, PixelmonWrapper opponent, Attack a) {
        return (int)((double)damage * 0.75);
    }
}

