/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Maps
 */
package com.pixelmongenerations.common.entity.pixelmon.tickHandlers;

import com.google.common.collect.Maps;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.tickHandlers.CastformTickHandler;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.HashMap;
import net.minecraft.world.World;

public abstract class TickHandlerBase {
    protected EntityPixelmon pixelmon;
    private int refreshRate;
    private int ticks = 0;
    private static HashMap<EnumSpecies, Class<? extends TickHandlerBase>> tickHandlers = Maps.newHashMap();

    public TickHandlerBase(EntityPixelmon pixelmon, int refreshRate) {
        this.pixelmon = pixelmon;
        this.refreshRate = refreshRate;
    }

    public TickHandlerBase(EntityPixelmon pixelmon) {
        this(pixelmon, 0);
    }

    protected abstract void onTick(World var1);

    public void tick(World world) {
        if (++this.ticks > this.refreshRate) {
            this.ticks = 0;
            this.onTick(world);
        }
    }

    public static TickHandlerBase getTickHandler(EntityPixelmon pixelmon) {
        if (!tickHandlers.containsKey((Object)pixelmon.getSpecies())) {
            return null;
        }
        Class<? extends TickHandlerBase> c = tickHandlers.get((Object)pixelmon.baseStats.pokemon);
        try {
            return c.getConstructor(EntityPixelmon.class).newInstance(pixelmon);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static {
        tickHandlers.put(EnumSpecies.Castform, CastformTickHandler.class);
    }
}

