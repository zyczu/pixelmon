/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class ComingSoon
extends AbilityBase {
    private final String truAbility;
    public static ComingSoon noAbility = new ComingSoon("");

    public ComingSoon(String truAbility) {
        this.truAbility = truAbility;
    }

    public String getTrueAbility() {
        return this.truAbility;
    }
}

