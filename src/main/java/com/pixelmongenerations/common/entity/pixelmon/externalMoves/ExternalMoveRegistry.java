/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.Cut;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.Dig;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.Fly;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.Forage;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.HealOther;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.HealingWish;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.Hyperspace;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.LightFire;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.Lightning;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.MegaEvolution;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.RockSmash;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.Teleport;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.Wish;
import com.pixelmongenerations.core.config.PixelmonConfig;
import java.util.ArrayList;

public class ExternalMoveRegistry {
    public static Forage forage = new Forage();
    public static MegaEvolution megaEvolution = new MegaEvolution();
    private static ArrayList<ExternalMoveBase> externalMoveList = new ArrayList();

    public static ExternalMoveBase getExternalMove(Attack a) {
        return ExternalMoveRegistry.getExternalMove(a.getAttackBase().attackIndex);
    }

    public static ExternalMoveBase getExternalMove(int attackIndex) {
        if (!PixelmonConfig.allowExternalMoves) {
            return null;
        }
        for (ExternalMoveBase e : externalMoveList) {
            if (!e.matches(attackIndex) || e.isDestructive() && !PixelmonConfig.allowDestructiveExternalMoves) continue;
            return e;
        }
        return null;
    }

    static {
        externalMoveList.add(new Wish());
        externalMoveList.add(new HealingWish());
        externalMoveList.add(new Cut());
        externalMoveList.add(new Dig());
        externalMoveList.add(new Fly());
        externalMoveList.add(new LightFire());
        externalMoveList.add(new Teleport());
        externalMoveList.add(new Lightning());
        externalMoveList.add(new RockSmash());
        externalMoveList.add(new HealOther());
        externalMoveList.add(new Hyperspace());
    }
}

