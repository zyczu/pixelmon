/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

import com.pixelmongenerations.api.events.pokemon.MovesetEvent;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.Entity6CanBattle;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.NBTLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.RandomAccess;
import java.util.stream.Collectors;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.MinecraftForge;

public class Moveset
extends AbstractList<Attack>
implements RandomAccess,
Cloneable {
    public PokemonLink pokemonLink = null;
    public Attack[] attacks = new Attack[4];

    public Moveset() {
        this.pokemonLink = null;
    }

    public Moveset(PokemonLink owner) {
        this.pokemonLink = owner;
    }

    public Moveset(Entity6CanBattle owner) {
        this.pokemonLink = owner.getPixelmonWrapper() != null ? owner.getPixelmonWrapper().getInnerLink() : new EntityLink(owner);
    }

    public Moveset(NBTTagCompound owner) {
        this.pokemonLink = new NBTLink(owner);
    }

    public Moveset(Attack[] attacks) {
        this.attacks = attacks;
    }

    public Moveset(int index, Attack a) {
        this.attacks[0] = a;
    }

    @Override
    public Attack get(int index) {
        if (index < 0 || index > 3) {
            return null;
        }
        return this.attacks[index];
    }

    @Override
    public boolean add(Attack a) {
        if (this.size() >= 4) {
            return false;
        }
        this.set(this.size(), a);
        return true;
    }

    @Override
    public Attack set(int index, Attack a) {
        Attack previousAttack = this.attacks[index];
        this.attacks[index] = a;
        if (this.pokemonLink == null || this.pokemonLink.getPlayerOwner() == null) {
            return previousAttack;
        }
        if (previousAttack != null) {
            MinecraftForge.EVENT_BUS.post(new MovesetEvent.ForgotMoveEvent(this.pokemonLink, this, previousAttack));
        }
        if (a != null) {
            MinecraftForge.EVENT_BUS.post(new MovesetEvent.LearntMoveEvent(this.pokemonLink, this, previousAttack, a));
        }
        return previousAttack;
    }

    public void swap(int index, int index2) {
        Attack a = this.attacks[index];
        this.attacks[index] = this.attacks[index2];
        this.attacks[index2] = a;
    }

    @Override
    public Attack remove(int index) {
        Attack a = this.get(index);
        int oldSize = this.size();
        for (int i = index + 1; i < this.size(); ++i) {
            this.set(i - 1, this.get(i));
        }
        this.set(oldSize - 1, null);
        return a;
    }

    @Override
    public boolean remove(Object o) {
        if (!(o instanceof Attack)) {
            return false;
        }
        for (int i = 0; i < this.size(); ++i) {
            if (this.attacks[i] != o) continue;
            this.set(i, null);
            return true;
        }
        return false;
    }

    @Override
    public int size() {
        int count = 0;
        for (int i = 0; i < 4; ++i) {
            if (this.attacks[i] == null) continue;
            ++count;
        }
        return count;
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    public boolean isAt(int index, Attack a) {
        if (this.isEmpty()) {
            return false;
        }
        if (!this.contains(a)) {
            return false;
        }
        return this.attacks[index] == a;
    }

    @Override
    public boolean contains(Object o) {
        if (this.isEmpty()) {
            return false;
        }
        if (o instanceof Attack) {
            for (int i = 0; i < this.size(); ++i) {
                if (!this.attacks[i].equals(o)) continue;
                return true;
            }
        }
        return false;
    }

    @Override
    public void clear() {
        this.attacks = new Attack[4];
    }

    public void writeToNBT(NBTTagCompound var1) {
        this.writeToNBT(var1, true);
    }

    public void writeToNBT(NBTTagCompound var1, boolean changePP) {
        int size = this.size();
        var1.setInteger("PixelmonNumberMoves", size);
        for (int i = 0; i < size; ++i) {
            Attack attack = this.get(i);
            if (attack != null) {
                String moveKey = "PixelmonMoveID" + i;
                boolean savePP = true;
                if (!changePP && var1.hasKey(moveKey)) {
                    savePP = var1.getInteger(moveKey) != attack.getAttackBase().attackIndex;
                }
                var1.setInteger(moveKey, attack.getAttackBase().attackIndex);
                if (!savePP) continue;
                var1.setInteger("PixelmonMovePP" + i, attack.pp);
                var1.setInteger("PixelmonMovePPBase" + i, attack.ppBase);
                var1.setInteger("PixelmonMovePPMax" + i, attack.getAttackBase().ppMax);
                continue;
            }
            if (size >= 4) continue;
            ++size;
        }
    }

    public void readFromNBT(NBTTagCompound var1) {
        this.clear();
        int numMoves = var1.getInteger("PixelmonNumberMoves");
        for (int i = 0; i < numMoves; ++i) {
            Attack a;
            if (var1.hasKey("PixelmonMoveID" + i)) {
                if (var1.getInteger("PixelmonMoveID" + i) == 691) {
                    var1.setInteger("PixelmonMoveID" + i, 468);
                }
                if (var1.getInteger("PixelmonMoveID" + i) == 695) {
                    var1.setInteger("PixelmonMoveID" + i, 545);
                }
            }
            Attack attack = a = !var1.hasKey("PixelmonMoveID" + i) && var1.hasKey("PixelmonMoveName" + i) ? DatabaseMoves.getAttack(var1.getString("PixelmonMoveName" + i)) : DatabaseMoves.getAttack(var1.getInteger("PixelmonMoveID" + i));
            if (a == null) continue;
            if (var1.hasKey("PixelmonMovePP" + i)) {
                a.pp = var1.getInteger("PixelmonMovePP" + i);
            }
            if (var1.hasKey("PixelmonMovePPBase" + i)) {
                a.ppBase = var1.getInteger("PixelmonMovePPBase" + i);
            }
            this.attacks[i] = a;
        }
    }

    public boolean hasAttack(Attack a) {
        for (Attack attack : this.attacks) {
            if (attack == null || attack.getAttackBase().attackIndex != a.getAttackBase().attackIndex) continue;
            return true;
        }
        return false;
    }

    public boolean hasAttack(String ... attackNames) {
        for (Attack attack : this.attacks) {
            if (attack == null || !attack.isAttack(attackNames)) continue;
            return true;
        }
        return false;
    }

    public boolean hasAttackCategory(AttackCategory attackCategory) {
        for (Attack attack : this.attacks) {
            if (attack == null || attack.getAttackCategory() != attackCategory) continue;
            return true;
        }
        return false;
    }

    public boolean hasOffensiveAttackType(EnumType ... types) {
        for (Attack attack : this.attacks) {
            for (EnumType type : types) {
                if (attack == null || attack.getAttackCategory() == AttackCategory.Status || attack.getAttackBase().attackType != type) continue;
                return true;
            }
        }
        return false;
    }

    public void replaceWith(ArrayList<Integer> attackIds) {
        this.clear();
        this.addAll(attackIds.stream().map(DatabaseMoves::getAttack).collect(Collectors.toList()));
    }

    public boolean hasFullPP() {
        for (Attack a : this) {
            if (a.pp >= a.ppBase) continue;
            return false;
        }
        return true;
    }

    public boolean hasMaxBasePP() {
        for (Attack a : this) {
            if (a.ppBase >= a.getAttackBase().ppMax) continue;
            return false;
        }
        return true;
    }

    public Moveset copy() {
        Attack[] attacks = new Attack[this.attacks.length];
        for (int i = 0; i < this.attacks.length; ++i) {
            if (this.attacks[i] == null) continue;
            attacks[i] = this.attacks[i].copy();
        }
        return new Moveset(attacks);
    }

    public void getNBTTags(HashMap<String, Class> tags) {
        tags.put("PixelmonNumberMoves", Integer.class);
        for (int i = 0; i < 4; ++i) {
            tags.put("PixelmonMoveID" + i, Integer.class);
            tags.put("PixelmonMovePP" + i, Integer.class);
            tags.put("PixelmonMovePPBase" + i, Integer.class);
            tags.put("PixelmonMovePPMax" + i, Integer.class);
        }
    }

    public boolean replaceMove(String oldMove, Attack newMove) {
        for (int i = 0; i < this.size(); ++i) {
            if (!this.attacks[i].isAttack(oldMove)) continue;
            this.attacks[i] = newMove;
            return true;
        }
        return false;
    }

    public void healAllPP() {
        for (int i = 0; i < this.size(); ++i) {
            Attack attack = this.attacks[i];
            attack.pp = attack.ppBase;
        }
    }

    public static Moveset loadMoveset(PokemonLink pokemon) {
        return DatabaseMoves.getInitialMoves(pokemon, pokemon.getLevel());
    }
}

