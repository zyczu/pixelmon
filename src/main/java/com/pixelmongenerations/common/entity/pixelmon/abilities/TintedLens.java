/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class TintedLens
extends AbilityBase {
    @Override
    public int modifyDamageUser(int damage, PixelmonWrapper user, PixelmonWrapper pokemon, Attack a) {
        double effectiveness = a.getTypeEffectiveness(user, pokemon);
        if (effectiveness == 0.5 || effectiveness == 0.25) {
            damage *= 2;
        }
        return damage;
    }
}

