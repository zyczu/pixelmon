/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.HashMap;
import net.minecraft.nbt.NBTTagCompound;

public class EVsStore {
    public int HP = 0;
    public int Attack = 0;
    public int Defence = 0;
    public int SpecialAttack = 0;
    public int SpecialDefence = 0;
    public int Speed = 0;
    public static int MAX_EVS = 252;
    public static int MAX_TOTAL_EVS = 510;

    public EVsStore() {
    }

    public EVsStore(int[] evs) {
        this.HP = evs[0];
        this.Attack = evs[1];
        this.Defence = evs[2];
        this.SpecialAttack = evs[3];
        this.SpecialDefence = evs[4];
        this.Speed = evs[5];
    }

    public void gainEV(EVsStore evGain) {
        int remainingEVs = this.getRemainingEVs();
        this.HP = Math.min(MAX_EVS, this.HP + Math.min(remainingEVs, evGain.HP));
        remainingEVs = this.getRemainingEVs();
        this.Attack = Math.min(MAX_EVS, this.Attack + Math.min(remainingEVs, evGain.Attack));
        remainingEVs = this.getRemainingEVs();
        this.Defence = Math.min(MAX_EVS, this.Defence + Math.min(remainingEVs, evGain.Defence));
        remainingEVs = this.getRemainingEVs();
        this.SpecialAttack = Math.min(MAX_EVS, this.SpecialAttack + Math.min(remainingEVs, evGain.SpecialAttack));
        remainingEVs = this.getRemainingEVs();
        this.SpecialDefence = Math.min(MAX_EVS, this.SpecialDefence + Math.min(remainingEVs, evGain.SpecialDefence));
        remainingEVs = this.getRemainingEVs();
        this.Speed = Math.min(MAX_EVS, this.Speed + Math.min(remainingEVs, evGain.Speed));
    }

    private int getRemainingEVs() {
        return Math.max(0, MAX_TOTAL_EVS - this.HP - this.Attack - this.Defence - this.SpecialAttack - this.SpecialDefence - this.Speed);
    }

    public void writeToNBT(NBTTagCompound nbt) {
        nbt.setInteger("EVHP", this.HP);
        nbt.setInteger("EVAttack", this.Attack);
        nbt.setInteger("EVDefence", this.Defence);
        nbt.setInteger("EVSpecialAttack", this.SpecialAttack);
        nbt.setInteger("EVSpecialDefence", this.SpecialDefence);
        nbt.setInteger("EVSpeed", this.Speed);
    }

    public void readFromNBT(NBTTagCompound nbt) {
        this.HP = nbt.getInteger("EVHP");
        this.Attack = nbt.getInteger("EVAttack");
        this.Defence = nbt.getInteger("EVDefence");
        this.SpecialAttack = nbt.getInteger("EVSpecialAttack");
        this.SpecialDefence = nbt.getInteger("EVSpecialDefence");
        this.Speed = nbt.getInteger("EVSpeed");
    }

    public int get(StatsType stat) {
        switch (stat) {
            case Attack: {
                return this.Attack;
            }
            case Defence: {
                return this.Defence;
            }
            case HP: {
                return this.HP;
            }
            case SpecialAttack: {
                return this.SpecialAttack;
            }
            case SpecialDefence: {
                return this.SpecialDefence;
            }
            case Speed: {
                return this.Speed;
            }
        }
        return -1;
    }

    public EVsStore cloneEVs() {
        EVsStore s = new EVsStore();
        s.HP = this.HP;
        s.Attack = this.Attack;
        s.Defence = this.Defence;
        s.SpecialAttack = this.SpecialAttack;
        s.SpecialDefence = this.SpecialDefence;
        s.Speed = this.Speed;
        return s;
    }

    public void doubleValues() {
        this.Attack *= 2;
        this.Defence *= 2;
        this.HP *= 2;
        this.SpecialAttack *= 2;
        this.SpecialDefence *= 2;
        this.Speed *= 2;
    }

    public EVsStore addEVs(StatsType stat, int amount) {
        switch (stat) {
            case Attack: {
                this.Attack += amount;
                break;
            }
            case Defence: {
                this.Defence += amount;
                break;
            }
            case HP: {
                this.HP += amount;
                break;
            }
            case SpecialAttack: {
                this.SpecialAttack += amount;
                break;
            }
            case SpecialDefence: {
                this.SpecialDefence += amount;
                break;
            }
            case Speed: {
                this.Speed += amount;
                break;
            }
        }
        return this;
    }

    public boolean berryEVs(StatsType stat) {
        int statValue = stat.getEV(this);
        if (stat.isPermanent() && statValue > 0) {
            stat.setEV(this, Math.max(statValue - 10, 0));
            return true;
        }
        return false;
    }

    public boolean featherEVs(StatsType stat) {
        if (stat.isPermanent()) {
            int value = stat.getEV(this);
            if (this.getRemainingEVs() > 0 && value < MAX_EVS) {
                stat.setEV(this, Math.min(value + 1, MAX_EVS));
                return true;
            }
        }
        return false;
    }

    public boolean vitaminEVs(StatsType stat) {
        int remainingEVs;
        if (stat.isPermanent() && (remainingEVs = this.getRemainingEVs()) > 0) {
            int evIncrease = Math.min(10, remainingEVs);
            int value = stat.getEV(this);
            if (value < MAX_EVS) {
                stat.setEV(this, Math.min(value + evIncrease, MAX_EVS));
                return true;
            }
        }
        return false;
    }

    public boolean juiceEVs(StatsType stat) {
        int remainingEVs;
        if (stat.isPermanent() && (remainingEVs = this.getRemainingEVs()) > 0) {
            int evIncrease = Math.min(5, remainingEVs);
            int value = stat.getEV(this);
            if (value < MAX_EVS) {
                stat.setEV(this, Math.min(value + evIncrease, MAX_EVS));
                return true;
            }
        }
        return false;
    }

    public static void getNBTTags(HashMap<String, Class> tags) {
        tags.put("EVHP", Integer.class);
        tags.put("EVAttack", Integer.class);
        tags.put("EVDefence", Integer.class);
        tags.put("EVSpecialAttack", Integer.class);
        tags.put("EVSpecialDefence", Integer.class);
        tags.put("EVSpeed", Integer.class);
    }

    public void randomizeMaxEVs() {
        int remainingEVs = MAX_TOTAL_EVS;
        int[] evs = new int[6];
        while (remainingEVs > 0) {
            int index = RandomHelper.getRandomNumberBetween(0, evs.length - 1);
            if (evs[index] >= MAX_EVS) continue;
            int n = index;
            evs[n] = evs[n] + 1;
            --remainingEVs;
        }
        this.HP = evs[0];
        this.Attack = evs[1];
        this.Defence = evs[2];
        this.SpecialAttack = evs[3];
        this.SpecialDefence = evs[4];
        this.Speed = evs[5];
    }

    public int[] getArray() {
        return new int[]{this.HP, this.Attack, this.Defence, this.SpecialAttack, this.SpecialDefence, this.Speed};
    }
}

