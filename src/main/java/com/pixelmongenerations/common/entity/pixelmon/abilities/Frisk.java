/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import java.util.ArrayList;

public class Frisk
extends AbilityBase {
    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        ArrayList<PixelmonWrapper> opponents = newPokemon.bc.getOpponentPokemon(newPokemon.getParticipant());
        for (PixelmonWrapper opponent : opponents) {
            if (!opponent.hasHeldItem()) continue;
            newPokemon.bc.sendToAll("pixelmon.abilities.frisk", newPokemon.getNickname(), opponent.getNickname(), opponent.getHeldItem().getLocalizedName());
        }
    }
}

