/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Hail;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.WeatherTrio;

public class SnowWarning
extends AbilityBase {
    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.bc.getActivePokemon().stream().map(PixelmonWrapper::getBattleAbility).filter(WeatherTrio.class::isInstance).map(WeatherTrio.class::cast).map(a -> a.weather).noneMatch(Weather::isWeatherTrioStatus) && !(newPokemon.bc.globalStatusController.getWeatherIgnoreAbility() instanceof Hail)) {
            Hail hail = new Hail();
            hail.setStartTurns(newPokemon);
            newPokemon.bc.globalStatusController.addGlobalStatus(hail);
            newPokemon.bc.sendToAll("pixelmon.abilities.snowwarning", newPokemon.getNickname());
        }
    }
}

