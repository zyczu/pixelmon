/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon;

import com.pixelmongenerations.common.entity.pixelmon.Entity8HoldsItems;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;

public abstract class Entity9HasSounds
extends Entity8HoldsItems {
    public Entity9HasSounds(World par1World) {
        super(par1World);
    }

    @Override
    protected SoundEvent getAmbientSound() {
        return this.baseStats.getSoundForGender(this.getGender(), this.hasNickname() ? this.getNickname() : null);
    }

    @Override
    protected float getSoundVolume() {
        return 0.4f;
    }
}

