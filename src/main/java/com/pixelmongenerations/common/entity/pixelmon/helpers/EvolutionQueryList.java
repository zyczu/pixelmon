/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.helpers;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQuery;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.evolution.EvolutionStage;
import com.pixelmongenerations.core.network.packetHandlers.evolution.EvolvePokemon;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.NetworkRegistry;

public class EvolutionQueryList {
    public static List<EvolutionQuery> queryList = Collections.synchronizedList(new ArrayList());

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void declineQuery(EntityPlayerMP player, int[] pokemonID) {
        List<EvolutionQuery> list;
        List<EvolutionQuery> list2 = list = queryList;
        synchronized (list2) {
            for (int i = 0; i < queryList.size(); ++i) {
                EvolutionQuery query = queryList.get(i);
                if (!PixelmonMethods.isIDSame(query.pokemonID, pokemonID)) continue;
                if (query.player == player) {
                    query.decline();
                    queryList.remove(i);
                }
                return;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void acceptQuery(EntityPlayerMP player, int[] pokemonID) {
        List<EvolutionQuery> list;
        List<EvolutionQuery> list2 = list = queryList;
        synchronized (list2) {
            for (EvolutionQuery query : queryList) {
                if (!PixelmonMethods.isIDSame(query.pokemonID, pokemonID)) continue;
                if (query.player == player) {
                    query.accept();
                }
                return;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static EvolutionQuery get(EntityPlayer player) {
        List<EvolutionQuery> list;
        List<EvolutionQuery> list2 = list = queryList;
        synchronized (list2) {
            for (EvolutionQuery q : queryList) {
                if (q.player != player) continue;
                return q;
            }
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void tick(World world) {
        try {
            List<EvolutionQuery> list = queryList;
            synchronized (list) {
                for (EvolutionQuery q : queryList) {
                    q.tick(world);
                }
            }
        } catch (NullPointerException | ConcurrentModificationException runtimeException) {
            // empty catch block
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void spawnPokemon(EntityPlayerMP player, int[] pokemonID) {
        List<EvolutionQuery> list = queryList;
        synchronized (list) {
            for (EvolutionQuery query : queryList) {
                if (!PixelmonMethods.isIDSame(query.pokemonID, pokemonID)) continue;
                Optional<PlayerStorage> playerStorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
                if (playerStorage.isPresent()) {
                    EntityPixelmon pixelmon = playerStorage.get().sendOut(pokemonID, player.world);
                    if (pixelmon == null) {
                        return;
                    }
                    pixelmon.motionZ = 0.0;
                    pixelmon.motionY = 0.0;
                    pixelmon.motionX = 0.0;
                    pixelmon.setLocationAndAngles(player.posX, player.posY, player.posZ, player.rotationYaw, 0.0f);
                    pixelmon.releaseFromPokeball();
                    player.world.spawnEntity(pixelmon);
                    Pixelmon.NETWORK.sendToAllAround(new EvolvePokemon(pokemonID, EvolutionStage.Choice), new NetworkRegistry.TargetPoint(pixelmon.dimension, pixelmon.posX, pixelmon.posY, pixelmon.posZ, 60.0));
                    query.pixelmon = pixelmon;
                    query.pixelmon.tasks.taskEntries.clear();
                }
                return;
            }
        }
    }
}

