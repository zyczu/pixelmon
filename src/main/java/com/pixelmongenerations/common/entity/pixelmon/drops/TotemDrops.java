/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.drops;

import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.common.entity.pixelmon.Entity8HoldsItems;
import com.pixelmongenerations.common.entity.pixelmon.drops.TotemInfo;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import net.minecraft.item.ItemStack;

public class TotemDrops {
    public ArrayList<TotemInfo> totemPokes;
    public ArrayList<String> anyDropPool;
    public HashMap<EnumType, ArrayList<String>> typeDropPool;

    public ArrayList<ItemStack> getDropsFor(Entity8HoldsItems pixelmon) {
        ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
        drops.add(DropItemRegistry.parseItem(Objects.requireNonNull(RandomHelper.getRandomElementFromList(this.anyDropPool)), "totemdrops.json {pool=any}"));
        if (this.typeDropPool.containsKey(pixelmon.type.get(0)) && RandomHelper.getRandomChance(0.45f)) {
            drops.add(DropItemRegistry.parseItem((String)Objects.requireNonNull(RandomHelper.getRandomElementFromList((List)this.typeDropPool.get(pixelmon.type.get(0)))), String.format("totemdrops.json {pool=type, type=%s}", ((EnumType)((Object)pixelmon.type.get(0))).getLocalizedName())));
        }
        return drops;
    }
}

