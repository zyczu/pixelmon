/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon;

import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.ParticleEffects;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.data.particles.ParticleRegistry;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumMegaPokemon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.packetHandlers.Transform;
import com.pixelmongenerations.core.network.packetHandlers.evolution.EvolutionStage;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.HashMap;
import java.util.Random;
import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class Entity4Textures
extends Entity3HasStats {
    public float maxScale = 1.25f;
    public float hoverTimer;
    protected ParticleEffects[] particleEffects;
    private static final String textureFileType = ".png";
    ResourceLocation lastTexture;
    boolean exists = true;
    boolean recheckTexture = false;
    public String transformedTexture = null;
    public int evolvingVal = 0;
    public int evoAnimTicks = 0;
    public EvolutionStage evoStage = null;
    public int fadeCount = 0;
    private boolean fadeDirection = true;
    public float heightDiff;
    public float widthDiff;
    public float lengthDiff;

    public Entity4Textures(World par1World) {
        super(par1World);
        this.dataManager.register(EntityPixelmon.dwTextures, 0);
        this.dataManager.register(EntityPixelmon.dwCustomTexture, "");
        this.dataManager.register(EntityPixelmon.dwShiny, false);
        this.dataManager.register(EntityPixelmon.dwParticle, "");
        this.dataManager.register(EntityPixelmon.dwColorTint, "");
        this.dataManager.register(EntityPixelmon.dwColorStrobe, false);
        this.dataManager.register(EntityPixelmon.dwParticleTint, -1);
        this.dataManager.register(EntityPixelmon.dwParticleTintAlpha, 255);
        this.dataManager.register(EntityPixelmon.dwParticleColorStrobe, false);
    }

    @Override
    protected void init(String name) {
        super.init(name);
        if (this.world != null) {
            if (!this.world.isRemote && PixelmonConfig.shinyRate > 0.0f && this.rand.nextFloat() < 1.0f / PixelmonConfig.shinyRate) {
                this.setShiny(true);
            }
            if (this.world.isRemote) {
                this.particleEffects = ParticleEffects.getParticleEffects(this);
            }
        }
    }

    @Override
    public void onStruckByLightning(EntityLightningBolt lightningBolt) {
        if (this.baseStats.pokemon == EnumSpecies.Magikarp) {
            this.setSpecialTexture(1);
            this.update(EnumUpdateType.Texture);
        } else {
            super.onStruckByLightning(lightningBolt);
        }
    }

    public ResourceLocation getTexture() {
        if (this.transformedTexture != null) {
            return new ResourceLocation(this.transformedTexture);
        }
        ResourceLocation location = this.getRealTexture();
        if (this.recheckTexture || this.lastTexture == null || !this.lastTexture.equals(location)) {
            this.lastTexture = location;
            this.exists = Pixelmon.PROXY.resourceLocationExists(location);
            this.recheckTexture = !this.exists && !this.getCustomTexture().isEmpty();
        }
        int form = this.getSpecies() == EnumSpecies.Deerling ? 0 : this.getForm();
        return this.exists ? location : Entity4Textures.getOGTexture(this.getSpecies(), this.getSpecies().getFormEnum(form), this.getGender(), this.isShiny());
    }

    public ResourceLocation getTextureNoCheck() {
        return this.transformedTexture != null ? new ResourceLocation(this.transformedTexture) : this.getRealTexture();
    }

    public ResourceLocation getRealTexture() {
        EnumSpecies pokemon = this.getSpecies();
        return Entity4Textures.getTextureFor(pokemon, pokemon.getFormEnum(this.getForm()), this.getGender(), this.getSpecialTexture(), this.isShiny());
    }

    public String getRealTextureNoCheck() {
        return this.getRealTexture().toString();
    }

    public void transformTexture(String transformedTexture) {
        this.transformedTexture = transformedTexture;
    }

    public boolean isShiny() {
        return this.dataManager.get(EntityPixelmon.dwShiny);
    }

    public void setShiny(boolean isShiny) {
        this.dataManager.set(EntityPixelmon.dwShiny, isShiny);
        if (isShiny) {
            if (this.getParticleId().isEmpty()) {
                this.setParticleId("shiny");
            }
        } else if (this.getParticleId().equals("shiny") || this.getParticleId().equals("ultrashiny")) {
            this.setParticleId("");
        }
    }

    public String getParticleId() {
        return this.dataManager.get(EntityPixelmon.dwParticle);
    }

    public void setParticleId(String particle) {
        if (!particle.isEmpty() && !ParticleRegistry.getInstance().particleExists(particle)) {
            Pixelmon.LOGGER.warn(String.format("Failed to set particle %s: Does not exist with ParticleRegistry", particle));
            return;
        }
        this.dataManager.set(EntityPixelmon.dwParticle, particle);
    }

    public boolean getIsRed() {
        return false;
    }

    public int getSpecialTextureIndex() {
        return this.dataManager.get(EntityPixelmon.dwTextures);
    }

    public String getSpecialTexture() {
        IEnumSpecialTexture enumm = this.baseStats.pokemon.getSpecialTexture(this.getFormEnum(), this.getSpecialTextureIndex());
        String texturCustom = this.dataManager.get(EntityPixelmon.dwCustomTexture);
        return !enumm.hasTexutre() ? (texturCustom.isEmpty() ? "" : "-" + texturCustom) : enumm.getTexture();
    }

    public IEnumSpecialTexture getSpecialTextureEnum() {
        return this.baseStats.pokemon.getSpecialTexture(this.getFormEnum(), this.getSpecialTextureIndex());
    }

    public void setCustomSpecialTexture(String texture) {
        this.dataManager.set(EntityPixelmon.dwCustomTexture, texture);
    }

    @Deprecated
    public String getCustomSpecialTexture() {
        return this.dataManager.get(EntityPixelmon.dwCustomTexture);
    }

    public String getCustomTexture() {
        return this.dataManager.get(EntityPixelmon.dwCustomTexture);
    }

    public void setSpecialTexture(int specTex) {
        this.dataManager.set(EntityPixelmon.dwTextures, specTex);
    }

    public boolean hasColorTint() {
        return !this.getColorTint().isEmpty();
    }

    public String getColorTint() {
        return this.dataManager.get(EntityPixelmon.dwColorTint);
    }

    public void removeColorTint() {
        this.dataManager.set(EntityPixelmon.dwColorTint, "");
        this.dataManager.setDirty(EntityPixelmon.dwColorTint);
    }

    public void setColorTint(String hexcode) {
        this.setColorTint(hexcode, false);
    }

    public void setColorTint(String hexcode, boolean isStrobe) {
        if (!isStrobe) {
            this.setColorStrobe(false);
        }
        this.dataManager.set(EntityPixelmon.dwColorTint, hexcode);
    }

    public boolean hasColorStrobe() {
        return this.dataManager.get(EntityPixelmon.dwColorStrobe);
    }

    public void setColorStrobe(boolean strobe) {
        this.dataManager.set(EntityPixelmon.dwColorStrobe, strobe);
        if (!strobe) {
            this.removeColorTint();
        }
    }

    public boolean hasParticleColorStrobe() {
        return this.dataManager.get(EntityPixelmon.dwParticleColorStrobe);
    }

    public void setParticleColorStrobe(boolean strobe) {
        this.dataManager.set(EntityPixelmon.dwParticleColorStrobe, strobe);
        if (!strobe) {
            this.removeParticleTint();
        }
    }

    public int getParticleTint() {
        return this.dataManager.get(EntityPixelmon.dwParticleTint);
    }

    public void setParticleTint(int color) {
        this.dataManager.set(EntityPixelmon.dwParticleTint, color);
    }

    public void removeParticleTint() {
        this.dataManager.set(EntityPixelmon.dwParticleTint, -1);
    }

    public int getParticleTintAlpha() {
        return this.dataManager.get(EntityPixelmon.dwParticleTintAlpha);
    }

    public void setParticleAlpha(int alpha) {
        this.dataManager.set(EntityPixelmon.dwParticleTintAlpha, alpha);
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public ModelBase getModel() {
        ModelBase model;
        if (this.isDynamaxed() && this.hasGmaxFactor() && this.getSpecies().hasGmaxForm()) {
            int size = PixelmonModelRegistry.gmaxModels.get((Object)this.getSpecies()).size();
            if (size == 1) {
                return PixelmonModelRegistry.gmaxModels.get((Object)this.getSpecies()).get(null).getModel();
            }
            if (size > 1) {
                return PixelmonModelRegistry.gmaxModels.get((Object)this.getSpecies()).get(this.getFormEnum()).getModel();
            }
        }
        String specialTexture = this.getSpecialTexture();
        if (this.transformed && this.transformedTexture.contains("-")) {
            specialTexture = "-" + this.transformedTexture.split("-")[1].replaceAll(textureFileType, "");
        }
        if (!specialTexture.isEmpty() && (model = PixelmonModelRegistry.getSpecialTextureModel(this.transformed ? this.transformedModel : this.getSpecies(), specialTexture)) != null) {
            return model;
        }
        return super.getModel();
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        if (this.world.isRemote) {
            if (this.particleEffects != null) {
                for (ParticleEffects particleEffect : this.particleEffects) {
                    particleEffect.onUpdate();
                }
            }
            this.tickEvolveAnimation();
        }
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        nbt.setBoolean("IsShiny", this.isShiny());
        nbt.setShort("specialTexture", (short)this.getSpecialTextureIndex());
        if (!this.dataManager.get(EntityPixelmon.dwCustomTexture).isEmpty()) {
            nbt.setString("CustomTexture", this.dataManager.get(EntityPixelmon.dwCustomTexture));
        } else {
            nbt.removeTag("CustomTexture");
        }
        nbt.setBoolean("HasStrobe", this.hasColorStrobe());
        nbt.setString("ColorTint", this.getColorTint());
        nbt.setBoolean("HasParticleStrobe", this.hasParticleColorStrobe());
        nbt.setInteger("ParticleTint", this.getParticleTint());
        nbt.setInteger("ParticleAlpha", this.getParticleTintAlpha());
        nbt.setString("ParticleId", this.getParticleId());
    }

    @Override
    public void getNBTTags(HashMap<String, Class> tags) {
        super.getNBTTags(tags);
        tags.put("IsShiny", Boolean.class);
        tags.put("specialTexture", Short.class);
        tags.put("CustomTexture", String.class);
        tags.put("HasStrobe", Boolean.class);
        tags.put("ColorTint", String.class);
        tags.put("HasParticleStrobe", Boolean.class);
        tags.put("ParticleTint", Integer.class);
        tags.put("ParticleAlpha", Integer.class);
        tags.put("ParticleId", String.class);
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        this.setShiny(nbt.getBoolean("IsShiny"));
        short specialTexture = nbt.getShort("specialTexture");
        this.setSpecialTexture(specialTexture);
        this.dataManager.set(EntityPixelmon.dwCustomTexture, nbt.getString("CustomTexture"));
        this.dataManager.set(EntityPixelmon.dwColorStrobe, nbt.getBoolean("HasStrobe"));
        this.dataManager.set(EntityPixelmon.dwColorTint, nbt.getString("ColorTint"));
        this.dataManager.set(EntityPixelmon.dwParticleColorStrobe, nbt.getBoolean("HasParticleStrobe"));
        int particleTint = nbt.getInteger("ParticleTint");
        this.dataManager.set(EntityPixelmon.dwParticleTint, particleTint != 0 ? particleTint : -1);
        int particleTintAlpha = nbt.getInteger("ParticleAlpha");
        this.dataManager.set(EntityPixelmon.dwParticleTintAlpha, particleTintAlpha != 0 ? particleTintAlpha : -1);
        this.dataManager.set(EntityPixelmon.dwParticle, nbt.getString("ParticleId"));
    }

    public void transformServer(EnumSpecies transformedModel, int form, String texture) {
        this.transformServer(transformedModel, form);
        this.transformTexture(texture);
        this.updateTransformed();
    }

    @Override
    public void cancelTransform() {
        super.cancelTransform();
        this.transformedTexture = null;
        this.updateTransformed();
    }

    public void updateTransformed() {
        Transform transform = this.transformed ? new Transform(this.getEntityId(), this.transformedModel, this.transformedTexture, this.transformedForm) : new Transform(this.getEntityId(), this.baseStats.pokemon, this.getRealTextureNoCheck(), this.getForm());
        Pixelmon.NETWORK.sendToAll(transform);
    }

    public boolean isEvolving() {
        return this.evoStage != null && this.evoStage != EvolutionStage.End;
    }

    public void setEvolutionAnimationStage(EvolutionStage stage) {
        if (stage == EvolutionStage.End) {
            this.evoStage = null;
            this.setSize(this.baseStats.width, this.baseStats.height + this.baseStats.hoverHeight);
        } else {
            this.evoStage = stage;
            this.evoAnimTicks = 0;
            if (stage == EvolutionStage.PreChoice) {
                this.fadeCount = 20;
            }
        }
    }

    public void setDynamaxSize() {
        this.setPixelmonScale(PixelmonConfig.dynamaxScale);
    }

    public void revertDynamaxSize() {
        this.setPixelmonScale(1.0f);
    }

    @SideOnly(value=Side.CLIENT)
    private void tickEvolveAnimation() {
        ++this.evoAnimTicks;
        if (this.evoStage != null) {
            ++this.evoAnimTicks;
            if (this.evoStage == EvolutionStage.Choice) {
                if (this.fadeDirection) {
                    ++this.fadeCount;
                    if (this.fadeCount >= 20) {
                        this.fadeDirection = false;
                    }
                } else {
                    --this.fadeCount;
                    if (this.fadeCount <= 0) {
                        this.fadeDirection = true;
                    }
                }
            } else if (this.evoStage == EvolutionStage.PreAnimation && this.fadeDirection && this.fadeCount < 20) {
                ++this.fadeCount;
            }
            if (this.evoStage == EvolutionStage.Choice || this.evoStage == EvolutionStage.PreAnimation) {
                Random random = this.getRNG();
                int numEffects = random.nextInt(10);
                for (int i = 0; i < numEffects; ++i) {
                    this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX + (double)((random.nextFloat() * 2.2f - 1.0f) * this.baseStats.width), this.posY + (double)((random.nextFloat() * 2.2f - 1.0f) * this.baseStats.height), this.posZ + (double)((random.nextFloat() * 2.2f - 1.0f) * this.baseStats.length), 255.0, 255.0, 255.0, new int[0]);
                }
            }
            if (this.evoStage == EvolutionStage.PreAnimation || this.evoStage == EvolutionStage.PostAnimation) {
                if (this.evoStage == EvolutionStage.PreAnimation) {
                    if (this.evoAnimTicks > EvolutionStage.PreAnimation.ticks) {
                        --this.evoAnimTicks;
                    }
                } else {
                    if (this.evoAnimTicks > EvolutionStage.PostAnimation.ticks) {
                        --this.evoAnimTicks;
                    }
                    if (this.evoAnimTicks > EvolutionStage.PostAnimation.ticks - 21 && this.fadeCount > 0) {
                        --this.fadeCount;
                    }
                }
                int ticks = this.evoAnimTicks;
                if (this.evoStage == EvolutionStage.PostAnimation) {
                    ticks += EvolutionStage.PreAnimation.ticks;
                }
                float length = this.baseStats.length + (float)ticks / 200.0f * this.lengthDiff;
                float height = this.baseStats.height + (float)ticks / 200.0f * this.heightDiff;
                float width = this.baseStats.width + (float)ticks / 200.0f * this.widthDiff;
                Random random = this.getRNG();
                int numEffects = random.nextInt(50);
                for (int i = 0; i < numEffects; ++i) {
                    this.world.spawnParticle(EnumParticleTypes.REDSTONE, this.posX + (double)((random.nextFloat() * 2.2f - 1.0f) * width), this.posY + (double)((random.nextFloat() * 2.2f - 1.0f) * height), this.posZ + (double)((random.nextFloat() * 2.2f - 1.0f) * length), 255.0, 255.0, 255.0, new int[0]);
                }
            }
        }
    }

    @Override
    public void setBoss(EnumBossMode mode) {
        super.setBoss(mode);
        if (this.serverBossMode.isBossPokemon() && this.baseStats.pokemon.hasMega()) {
            int numMegas = EnumMegaPokemon.getMega((EnumSpecies)this.baseStats.pokemon).numMegaForms;
            int form = 1;
            if (numMegas > 1) {
                form = RandomHelper.getRandomNumberBetween(1, numMegas);
            }
            this.isMega = true;
            this.setForm(form);
            this.setShiny(mode.extraLevels >= EnumBossMode.Legendary.extraLevels);
        }
    }

    public static ResourceLocation getTextureFor(EnumSpecies pokemon, IEnumForm form, Gender gender, String specialTexture, boolean shiny) {
        String path = "textures/pokemon/";
        String folder = "";
        String prefix = "";
        if (shiny) {
            folder = "pokemon-shiny/";
            prefix = "shiny";
        }
        String mf = EnumSpecies.mfTextured.contains((Object)pokemon) ? (gender == Gender.Male ? "male" : "female") : "";
        if (pokemon == EnumSpecies.Magikarp && specialTexture.equals("-classic")) {
            mf = "";
        }
        ResourceLocation location = new ResourceLocation("pixelmon", path + folder + prefix + pokemon.name.toLowerCase() + form.getFormSuffix() + mf + specialTexture + textureFileType);
        if (specialTexture.equals("-detective")) {
            return new ResourceLocation("pixelmon", path + pokemon.name.toLowerCase() + "-detective.png");
        }
        if (pokemon == EnumSpecies.Sawsbuck && specialTexture.equals("-halloween")) {
            return new ResourceLocation("pixelmon", path + pokemon.name.toLowerCase() + "-halloween.png");
        }
        if (shiny && !Pixelmon.PROXY.resourceLocationExists(location)) {
            return new ResourceLocation("pixelmon", path + pokemon.name.toLowerCase() + form.getFormSuffix() + mf + specialTexture + textureFileType);
        }
        return location;
    }

    public static ResourceLocation getOGTexture(EnumSpecies pokemon, IEnumForm form, Gender gender, boolean shiny) {
        String mf = EnumSpecies.mfTextured.contains((Object)pokemon) ? (gender == Gender.Male ? "male" : "female") : "";
        return new ResourceLocation("pixelmon", "textures/pokemon/" + (shiny ? "pokemon-shiny/shiny" : "") + pokemon.name.toLowerCase() + form.getFormSuffix() + mf + textureFileType);
    }

    public static String getOGTexturePath(EnumSpecies pokemon, IEnumForm form, Gender gender, boolean shiny) {
        String mf = EnumSpecies.mfTextured.contains((Object)pokemon) ? (gender == Gender.Male ? "male" : "female") : "";
        return "pixelmon:textures/pokemon/" + (shiny ? "pokemon-shiny/shiny" : "") + pokemon.name.toLowerCase() + form.getFormSuffix() + mf + textureFileType;
    }
}

