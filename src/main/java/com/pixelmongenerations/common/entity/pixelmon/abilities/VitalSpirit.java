/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.PreventSleep;

public class VitalSpirit
extends PreventSleep {
    public VitalSpirit() {
        super("pixelmon.abilities.vitalspirit", "pixelmon.abilities.vitalspiritcure");
    }
}

