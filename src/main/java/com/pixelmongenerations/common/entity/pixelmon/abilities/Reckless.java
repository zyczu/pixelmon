/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import java.util.Arrays;

public class Reckless
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        String[] recoilMoves = new String[]{"Brave Bird", "Double-Edge", "Flare Blitz", "Head Charge", "Head Smash", "Hi Jump Kick", "Jump Kick", "Submission", "Take Down", "Volt Tackle", "Wild Charge", "Wood Hammer"};
        if (Arrays.asList(recoilMoves).contains(a.getAttackBase().getUnlocalizedName())) {
            power = (int)((double)power * 1.2);
        }
        return new int[]{power, accuracy};
    }
}

