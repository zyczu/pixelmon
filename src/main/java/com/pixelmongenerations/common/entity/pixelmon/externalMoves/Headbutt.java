/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.api.spawning.archetypes.spawners.TriggerSpawner;
import com.pixelmongenerations.common.block.spawning.BlockSpawningHandler;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.enums.battle.EnumBattleStartTypes;
import com.pixelmongenerations.core.network.PixelmonData;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class Headbutt
extends ExternalMoveBase {
    public static TriggerSpawner HEADBUTT_SPAWNER = null;

    public Headbutt() {
        super("headbutt", "Headbutt");
    }

    @Override
    public boolean execute(EntityPixelmon user, RayTraceResult target, int moveIndex) {
        if (target.typeOfHit == RayTraceResult.Type.BLOCK) {
            World world = user.world;
            BlockPos pos = target.getBlockPos();
            IBlockState state = world.getBlockState(pos);
            if (state.getMaterial() != Material.WOOD) {
                return false;
            }
            BlockSpawningHandler.getInstance().performBattleStartCheck(world, pos, user.getOwner(), user, "", EnumBattleStartTypes.HEADBUTT);
            return true;
        }
        return false;
    }

    @Override
    public int getCooldown(EntityPixelmon user) {
        return 800 - user.stats.Speed;
    }

    @Override
    public int getCooldown(PixelmonData user) {
        return 800 - user.Speed;
    }

    @Override
    public boolean isDestructive() {
        return false;
    }
}

