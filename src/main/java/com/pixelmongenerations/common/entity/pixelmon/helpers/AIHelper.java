/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.entity.pixelmon.helpers;

import com.google.common.collect.Lists;
import com.pixelmongenerations.common.block.enums.EnumSpawnerAggression;
import com.pixelmongenerations.common.entity.ai.AIExecuteAction;
import com.pixelmongenerations.common.entity.ai.AIFlying;
import com.pixelmongenerations.common.entity.ai.AIFlyingPersistent;
import com.pixelmongenerations.common.entity.ai.AIIsInBattle;
import com.pixelmongenerations.common.entity.ai.AIMoveTowardsBlock;
import com.pixelmongenerations.common.entity.ai.AIMoveTowardsTarget;
import com.pixelmongenerations.common.entity.ai.AISwimming;
import com.pixelmongenerations.common.entity.ai.AITargetNearest;
import com.pixelmongenerations.common.entity.ai.AITeleportAway;
import com.pixelmongenerations.common.entity.ai.AITempt;
import com.pixelmongenerations.common.entity.ai.AIWander;
import com.pixelmongenerations.common.entity.pixelmon.Entity7HasAI;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.EnumAggression;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.List;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.ai.EntityAIFollowOwner;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAITasks;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.pathfinding.PathNavigateGround;

public class AIHelper {
    private static List<EnumSpecies> teleportingPokemon = Lists.newArrayList(EnumSpecies.Abra, EnumSpecies.Hattrem, EnumSpecies.Hatterene, EnumSpecies.Hatenna);
    private int i = 0;

    public AIHelper(Entity7HasAI entity, EntityAITasks tasks) {
        EntityPixelmon pix = (EntityPixelmon)entity;
        if (!tasks.taskEntries.isEmpty()) {
            tasks.taskEntries.clear();
        }
        this.initBaseAI(pix, tasks);
        if (pix.getSpawnLocation() == SpawnLocation.Land && !entity.baseStats.canFly && (!entity.baseStats.isRideable || entity.baseStats.type1 != EnumType.Water && entity.baseStats.type2 != EnumType.Water)) {
            this.initGroundAI(pix, tasks);
        } else if (entity.baseStats.canFly && entity.getFlyingParameters() != null) {
            if (pix.getSpawnLocation() == SpawnLocation.AirPersistent) {
                this.initFlyingPersistentAI(pix, tasks);
            } else {
                this.initFlyingAI(pix, tasks);
            }
        } else if (pix.getSpawnLocation() == SpawnLocation.Water) {
            this.initSwimmingAI(pix, tasks);
        } else {
            this.initGroundAI(pix, tasks);
        }
    }

    private void initFlyingPersistentAI(EntityPixelmon entity, EntityAITasks tasks) {
        if (!entity.isInRanchBlock) {
            tasks.addTask(this.i++, new EntityAIFollowOwner(entity, 1.0, 10.0f, 4.0f));
            tasks.addTask(this.i++, new AITempt(entity, PixelmonItems.rareCandy, false));
        }
        tasks.addTask(this.i++, new AIFlyingPersistent(entity));
    }

    private void initSwimmingAI(EntityPixelmon entity, EntityAITasks tasks) {
        if (!entity.isInRanchBlock) {
            tasks.addTask(this.i++, new EntityAIFollowOwner(entity, 1.0, 10.0f, 4.0f));
            tasks.addTask(this.i++, new AITempt(entity, PixelmonItems.rareCandy, false));
        }
        tasks.addTask(this.i++, new AISwimming(entity));
    }

    private void initFlyingAI(EntityPixelmon entity, EntityAITasks tasks) {
        tasks.addTask(this.i++, new EntityAISwimming(entity));
        if (!entity.isInRanchBlock) {
            tasks.addTask(this.i++, new EntityAIFollowOwner(entity, 1.0, 10.0f, 4.0f));
            tasks.addTask(this.i++, new AITempt(entity, PixelmonItems.rareCandy, false));
        }
        tasks.addTask(this.i++, new EntityAIWatchClosest(entity, EntityPixelmon.class, 8.0f));
        tasks.addTask(this.i++, new AIFlying(entity));
    }

    private void initBaseAI(EntityPixelmon entity, EntityAITasks tasks) {
        if (!entity.isInRanchBlock) {
            tasks.addTask(this.i++, new AIIsInBattle(entity));
            tasks.addTask(this.i++, new AIMoveTowardsTarget(entity, 15.0f));
            tasks.addTask(this.i++, new AIExecuteAction(entity));
            if (entity.getSpawnLocation() != SpawnLocation.Water && (!entity.baseStats.isRideable || entity.baseStats.type1 != EnumType.Water && entity.baseStats.type2 != EnumType.Water)) {
                if ((PixelmonConfig.isAggressionAllowed || entity.spawner != null && entity.spawner.aggression == EnumSpawnerAggression.Aggressive) && entity.aggression == EnumAggression.aggressive) {
                    tasks.addTask(this.i++, new AITargetNearest((EntityCreature)entity, 10.0f, true));
                } else if (entity.aggression == EnumAggression.timid) {
                    tasks.addTask(this.i++, new EntityAIAvoidEntity<EntityPlayer>(entity, EntityPlayer.class, 16.0f, 0.23f, 0.4f));
                }
            }
            if (entity.getOwner() != null) {
                tasks.addTask(this.i++, new AIMoveTowardsBlock(entity));
            }
        }
    }

    private void initGroundAI(EntityPixelmon entity, EntityAITasks tasks) {
        if (entity.baseStats.type1 != EnumType.Fire && entity.baseStats.type2 != EnumType.Fire) {
            ((PathNavigateGround)entity.getNavigator()).setCanSwim(true);
        }
        if (teleportingPokemon.contains((Object)entity.getSpecies()) && !entity.isInRanchBlock && !PixelmonConfig.stopTeleportingPokemon) {
            tasks.addTask(this.i++, new AITeleportAway(entity));
        }
        tasks.addTask(this.i++, new EntityAISwimming(entity));
        if (!entity.isInRanchBlock) {
            tasks.addTask(this.i++, new EntityAIFollowOwner(entity, 1.0, 10.0f, 4.0f));
            tasks.addTask(this.i++, new AITempt(entity, PixelmonItems.rareCandy, false));
        }
        tasks.addTask(this.i++, new AIWander(entity));
        tasks.addTask(this.i++, new EntityAIWatchClosest(entity, EntityPixelmon.class, 8.0f));
        tasks.addTask(this.i++, new EntityAILookIdle(entity));
    }

    public void setWanderGroundAI(EntityPixelmon entity, EntityAITasks tasks) {
        tasks.taskEntries.clear();
        ((PathNavigateGround)entity.getNavigator()).setCanSwim(true);
        tasks.addTask(this.i++, new EntityAISwimming(entity));
        tasks.addTask(this.i++, new AIWander(entity));
        tasks.addTask(this.i++, new EntityAIWatchClosest(entity, EntityPixelmon.class, 8.0f));
        tasks.addTask(this.i++, new EntityAILookIdle(entity));
    }
}

