/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;
import net.minecraft.util.text.TextComponentTranslation;

public class LiquidVoice
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (this.isSoundMove(a)) {
            user.bc.sendToAll(new TextComponentTranslation("pixelmon.effect.liquidvoice", user.getNickname(), user.attack.getAttackBase().getLocalizedName()));
            a.overrideType(EnumType.Water);
        }
        return new int[]{power, accuracy};
    }

    public boolean isSoundMove(Attack attack) {
        return attack.isAttack("Boomburst", "Bug Buzz", "Chatter", "Clanging Scales", "Clangorous Soulblaze", "Confide", "Disarming Voice", "Echoed Voice", "Grass Whistle", "Growl", "Heal Bell", "Hyper Voice", "Metal Sound", "Noble Roar", "Parting Shot", "Perish Song", "Relic Song", "Roar", "Round", "Screech", "Sing", "Snarl", "Snore", "Sparkling Aria", "Supersonic", "Uproar", "Overdrive");
    }
}

