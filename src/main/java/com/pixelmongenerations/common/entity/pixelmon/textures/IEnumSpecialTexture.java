/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.textures;

public interface IEnumSpecialTexture {
    public boolean hasTexutre();

    public String getTexture();

    public String name();

    public String getProperName();

    default public boolean hasGlow() {
        return false;
    }

    default public boolean hasGlowRainbow() {
        return false;
    }

    default public boolean hasShinyVariant() {
        return false;
    }

    default public String format(String contents) {
        String properName = this.getProperName();
        if (!properName.isEmpty()) {
            return contents;
        }
        return this.getProperName() + " " + contents;
    }

    public int getId();
}

