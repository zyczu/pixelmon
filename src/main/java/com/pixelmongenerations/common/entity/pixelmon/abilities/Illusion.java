/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.clientStorage.Add;
import com.pixelmongenerations.core.util.PixelmonMethods;
import net.minecraft.entity.player.EntityPlayerMP;

public class Illusion
extends AbilityBase {
    public EnumSpecies disguisedPokemon = null;
    public String disguisedNickname = null;
    public String disguisedTexture = null;
    public Gender disguisedGender = null;
    public int disguisedForm = -1;

    @Override
    public void beforeSwitch(PixelmonWrapper newPokemon) {
        if (newPokemon.bc.simulateMode) {
            return;
        }
        BattleParticipant participant = newPokemon.getParticipant();
        if (participant instanceof WildPixelmonParticipant) {
            return;
        }
        PixelmonWrapper disguised = null;
        for (int i = participant.allPokemon.length - 1; i >= 0; --i) {
            PixelmonWrapper pw = participant.allPokemon[i];
            if (pw.isFainted()) continue;
            if (!(PixelmonMethods.isIDSame(pw, newPokemon) || newPokemon.bc.rules.battleType == EnumBattleType.Double && pw.bc.isInBattle(pw))) {
                disguised = pw;
                this.disguisedTexture = disguised.getRealTextureNoCheck();
                if (this.disguisedTexture.equals(newPokemon.getRealTextureNoCheck())) {
                    this.disguisedTexture = null;
                    return;
                }
                this.disguisedPokemon = disguised.getSpecies();
                this.disguisedNickname = disguised.getNickname();
                this.disguisedGender = disguised.getGender();
                this.disguisedForm = disguised.getForm();
                break;
            }
            return;
        }
        if (disguised == null) {
            return;
        }
        newPokemon.pokemon.transformServer(disguised.getSpecies(), this.disguisedForm, this.disguisedTexture);
        if (newPokemon.getPlayerOwner() != null) {
            PixelmonData data = new PixelmonData(new WrapperLink(newPokemon));
            data.inBattle = true;
            this.updateOwner(data, newPokemon);
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.pokemon.transformedTexture == null && this.disguisedPokemon != null) {
            newPokemon.pokemon.transformServer(this.disguisedPokemon, this.disguisedForm, this.disguisedTexture);
        }
    }

    @Override
    public void tookDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        this.fade(target);
    }

    @Override
    public void onAbilityLost(PixelmonWrapper pokemon) {
        this.fade(pokemon);
    }

    private void fade(PixelmonWrapper target) {
        if (this.disguisedPokemon != null && !target.bc.simulateMode) {
            this.disguisedPokemon = null;
            this.disguisedNickname = null;
            this.disguisedTexture = null;
            this.disguisedGender = null;
            target.bc.sendToAll("pixelmon.abilities.illusion", target.getNickname());
            target.pokemon.cancelTransform();
            target.bc.participants.stream().filter(participant -> participant instanceof PlayerParticipant).forEach(participant -> ((PlayerParticipant)participant).updateOpponentPokemon());
            if (target.getPlayerOwner() != null) {
                PixelmonData data = new PixelmonData(new WrapperLink(target));
                data.inBattle = true;
                this.updateOwner(data, target);
            }
        }
    }

    private void updateOwner(PixelmonData data, PixelmonWrapper pokemon) {
        Pixelmon.NETWORK.sendTo(new Add(data, true), pokemon.getPlayerOwner());
        pokemon.bc.spectators.forEach(spectator -> spectator.sendMessage(new Add(data, true, true)));
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper oldPokemon) {
        EntityPlayerMP player = oldPokemon.getPlayerOwner();
        if (!oldPokemon.bc.simulateMode && player != null) {
            PixelmonData data = new PixelmonData(oldPokemon.pokemon);
            if (data.order != -1) {
                data.inBattle = true;
                Pixelmon.NETWORK.sendTo(new Add(data, false), player);
            }
        }
    }

    @Override
    public void applyEndOfBattleEffect(PixelmonWrapper pokemon) {
        EntityPlayerMP player = pokemon.getPlayerOwner();
        if (player != null) {
            PixelmonData data = new PixelmonData(new WrapperLink(pokemon));
            if (data.order != -1) {
                data.inBattle = true;
                Pixelmon.NETWORK.sendTo(new Add(data, true), player);
            }
        }
    }

    @Override
    public boolean needNewInstance() {
        return true;
    }
}

