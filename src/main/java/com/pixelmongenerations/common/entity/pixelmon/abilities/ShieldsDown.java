/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.forms.EnumMinior;

public class ShieldsDown
extends AbilityBase {
    @Override
    public boolean allowsStatChange(PixelmonWrapper pokemon, PixelmonWrapper user, StatsEffect e) {
        if (e.getUser() || e.value > 0) {
            return true;
        }
        if (!Attack.dealsDamage(user.attack)) {
            user.getForm();
            user.bc.sendToAll("pixelmon.abilities.shieldsdown", pokemon.getNickname());
            user.bc.modifyStats();
        }
        return false;
    }

    @Override
    public int[] modifyStats(PixelmonWrapper pokemon, int[] stats) {
        switch (pokemon.getForm()) {
            case 0: 
            case 7: {
                if (pokemon.getHealthPercent() <= 50.0f) {
                    pokemon.setForm(EnumMinior.Red.getForm());
                    break;
                }
                if (!(pokemon.getHealthPercent() > 50.0f)) break;
                pokemon.setForm(EnumMinior.MeteorRed.getForm());
                break;
            }
            case 1: 
            case 8: {
                if (pokemon.getHealthPercent() <= 50.0f) {
                    pokemon.setForm(EnumMinior.Orange.getForm());
                    break;
                }
                if (!(pokemon.getHealthPercent() > 50.0f)) break;
                pokemon.setForm(EnumMinior.MeteorOrange.getForm());
                break;
            }
            case 2: 
            case 9: {
                if (pokemon.getHealthPercent() <= 50.0f) {
                    pokemon.setForm(EnumMinior.Yellow.getForm());
                    break;
                }
                if (!(pokemon.getHealthPercent() > 50.0f)) break;
                pokemon.setForm(EnumMinior.MeteorYellow.getForm());
                break;
            }
            case 3: 
            case 10: {
                if (pokemon.getHealthPercent() <= 50.0f) {
                    pokemon.setForm(EnumMinior.Green.getForm());
                    break;
                }
                if (!(pokemon.getHealthPercent() > 50.0f)) break;
                pokemon.setForm(EnumMinior.MeteorGreen.getForm());
                break;
            }
            case 4: 
            case 11: {
                if (pokemon.getHealthPercent() <= 50.0f) {
                    pokemon.setForm(EnumMinior.Blue.getForm());
                    break;
                }
                if (!(pokemon.getHealthPercent() > 50.0f)) break;
                pokemon.setForm(EnumMinior.MeteorBlue.getForm());
                break;
            }
            case 5: 
            case 12: {
                if (pokemon.getHealthPercent() <= 50.0f) {
                    pokemon.setForm(EnumMinior.Indigo.getForm());
                    break;
                }
                if (!(pokemon.getHealthPercent() > 50.0f)) break;
                pokemon.setForm(EnumMinior.MeteorIndigo.getForm());
                break;
            }
            case 6: 
            case 13: {
                if (pokemon.getHealthPercent() <= 50.0f) {
                    pokemon.setForm(EnumMinior.Violet.getForm());
                    break;
                }
                if (!(pokemon.getHealthPercent() > 50.0f)) break;
                pokemon.setForm(EnumMinior.MeteorViolet.getForm());
            }
        }
        return stats;
    }

    @Override
    public boolean allowsStatus(StatusType status, PixelmonWrapper pokemon, PixelmonWrapper user) {
        if (pokemon.getHealthPercent() > 50.0f && status.isPrimaryStatus()) {
            pokemon.bc.sendToAll("pixelmon.abilities.meteorshield", pokemon.getNickname());
            return false;
        }
        return true;
    }

    @Override
    public boolean canBeNeutralized() {
        return true;
    }
}

