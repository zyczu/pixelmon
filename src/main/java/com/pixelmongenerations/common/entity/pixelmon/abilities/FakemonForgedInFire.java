/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;

public class FakemonForgedInFire
extends AbilityBase {
    @Override
    public void startMove(PixelmonWrapper user) {
        if (user.attack.getAttackBase().attackType == EnumType.Fire) {
            user.getBattleStats().modifyStat(-2, StatsType.Attack);
            user.getBattleStats().modifyStat(-2, StatsType.SpecialAttack);
            user.getBattleStats().modifyStat(2, StatsType.Defence);
            user.getBattleStats().modifyStat(2, StatsType.SpecialDefence);
            user.bc.sendToAll("pixelmon.abilities.forgedinfire", user.getNickname());
        } else if (user.attack.getAttackBase().attackType == EnumType.Steel) {
            user.getBattleStats().modifyStat(2, StatsType.Attack);
            user.getBattleStats().modifyStat(2, StatsType.SpecialAttack);
            user.getBattleStats().modifyStat(-2, StatsType.Defence);
            user.getBattleStats().modifyStat(-2, StatsType.SpecialDefence);
            user.bc.sendToAll("pixelmon.abilities.forgedinfire", user.getNickname());
        }
    }
}

