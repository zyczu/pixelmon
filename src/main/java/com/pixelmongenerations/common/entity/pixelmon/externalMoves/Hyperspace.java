/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.api.events.ExternalMoveEvent;
import com.pixelmongenerations.common.entity.EntityMysteriousRing;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.network.PixelmonData;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class Hyperspace
extends ExternalMoveBase {
    public Hyperspace() {
        super("ring", "Hyperspace Hole", "Hyperspace Fury");
    }

    @Override
    public boolean execute(EntityPixelmon pokemon, RayTraceResult rtr, int moveIndex) {
        ExternalMoveEvent.Hyperspace event = new ExternalMoveEvent.Hyperspace((EntityPlayerMP)pokemon.getOwner(), pokemon, this, rtr);
        MinecraftForge.EVENT_BUS.post(event);
        if (!event.isCanceled() && PixelmonConfig.allowMysteriousRings) {
            World world = pokemon.world;
            EntityMysteriousRing ring = new EntityMysteriousRing(world);
            ring.setPositionAndUpdate(pokemon.posX, pokemon.posY, pokemon.posZ);
            FMLCommonHandler.instance().getMinecraftServerInstance().getServer().addScheduledTask(() -> {
                ring.life = 0;
                world.spawnEntity(ring);
            });
            return true;
        }
        return false;
    }

    @Override
    public int getCooldown(EntityPixelmon pokemon) {
        return 800;
    }

    @Override
    public int getCooldown(PixelmonData pokemon) {
        return 800;
    }

    @Override
    public boolean isDestructive() {
        return false;
    }
}

