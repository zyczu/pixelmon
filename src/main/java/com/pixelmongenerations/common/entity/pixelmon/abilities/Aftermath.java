/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Damp;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LongReach;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.item.heldItems.ItemProtectivePads;

public class Aftermath
extends AbilityBase {
    @Override
    public void applyEffectOnContactTarget(PixelmonWrapper user, PixelmonWrapper target) {
        boolean hasDamp = false;
        for (PixelmonWrapper pw : user.bc.getActiveUnfaintedPokemon()) {
            if (!(pw.getBattleAbility() instanceof Damp)) continue;
            hasDamp = true;
        }
        if (!hasDamp && target.isFainted() && !(user.getBattleAbility() instanceof MagicGuard)) {
            if (user.hasHeldItem() && user.getHeldItem() instanceof ItemProtectivePads) {
                user.bc.sendToAll("pixelmon.effect.protectivepads", user.getNickname());
                return;
            }
            if (user.getBattleAbility() instanceof LongReach) {
                return;
            }
            user.doBattleDamage(target, user.getPercentMaxHealth(25.0f), DamageTypeEnum.ABILITY);
            user.bc.sendToAll("pixelmon.abilities.aftermath", target.getNickname(), user.getNickname());
        }
    }
}

