/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.config.PixelmonPotions;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.RayTraceResult;

public class Wish
extends ExternalMoveBase {
    public Wish() {
        super("wish", "Wish");
    }

    @Override
    public boolean execute(EntityPixelmon user, RayTraceResult target, int moveIndex) {
        if (user.getSpecies() == EnumSpecies.Jirachi && user.getOwner() instanceof EntityPlayerMP) {
            for (ItemStack itemStack : ((EntityPlayerMP)user.getOwner()).inventory.mainInventory) {
                if (itemStack.getItem() != Items.DIAMOND && itemStack.getItem() != Items.EMERALD) continue;
                itemStack.setCount(itemStack.getCount() - 1);
                user.getOwner().addPotionEffect(RandomHelper.getRandomChance() ? PixelmonPotions.attract.getEffect(900) : PixelmonPotions.luck.getEffect(900));
                return true;
            }
        }
        return false;
    }

    @Override
    public int getCooldown(EntityPixelmon user) {
        return 4800 - (int)((double)user.stats.Speed * 1.25);
    }

    @Override
    public int getCooldown(PixelmonData user) {
        return 4800 - (int)((double)user.Speed * 1.25);
    }

    @Override
    public boolean isDestructive() {
        return false;
    }
}

