/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableMap
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.entity.pixelmon;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.client.models.PixelmonModelHolder;
import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.client.models.PixelmonModelSmd;
import com.pixelmongenerations.client.models.smd.AnimationType;
import com.pixelmongenerations.client.models.smd.SmdAnimation;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.common.entity.SpawningEntity;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.Entity4Textures;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.FriendShip;
import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;
import com.pixelmongenerations.common.item.ItemStatueMaker;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumStatueTextureType;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.packetHandlers.statueEditor.StatuePacketClient;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.SpawnColors;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.IntStream;
import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityStatue extends Entity3HasStats {

    private static ImmutableMap<String, List<Color>> colors = ImmutableMap.of(
            "Suicune", Lists.newArrayList(SpawnColors.LIGHT_BLUE, SpawnColors.WHITE, SpawnColors.PURPLE),
            "Raikou", Lists.newArrayList(SpawnColors.YELLOW, SpawnColors.PURPLE, SpawnColors.LIGHT_BLUE),
            "Entei", Lists.newArrayList(SpawnColors.BROWN, SpawnColors.WHITE, SpawnColors.RED));

    public static final DataParameter<Float> dwRotation = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.FLOAT);
    public static final DataParameter<String> dwLabel = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.STRING);
    public static final DataParameter<String> dwAnimation = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.STRING);
    public static final DataParameter<Integer> dwAnimationFrame = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<Boolean> dwIsFlying = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.BOOLEAN);
    public static final DataParameter<Integer> dwSpecial = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.VARINT);
    public static final DataParameter<String> dwSpec = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.STRING);
    public static final DataParameter<Boolean> dwAdminPlaced = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.BOOLEAN);
    public static final DataParameter<Boolean> dwAnimated = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.BOOLEAN);
    public static final DataParameter<Boolean> dwGmaxModel = EntityDataManager.createKey(EntityPixelmon.class, DataSerializers.BOOLEAN);
    private String lastAnimation = null;
    private int lastFrame = -1;

    public EntityStatue(World par1World) {
        super(par1World);
        this.dataManager.register(dwRotation, Float.valueOf(0.0f));
        this.dataManager.register(EntityPixelmon.dwTextures, -1);
        this.dataManager.register(EntityPixelmon.dwCustomTexture, "");
        this.dataManager.register(dwLabel, "");
        this.dataManager.register(dwAnimation, "");
        this.dataManager.register(dwAnimationFrame, 0);
        this.dataManager.register(dwIsFlying, false);
        this.dataManager.register(dwSpecial, 0);
        this.dataManager.register(dwSpec, "");
        this.dataManager.register(dwAdminPlaced, false);
        this.dataManager.register(dwAnimated, false);
        this.dataManager.register(dwGmaxModel, false);
    }

    @Override
    public void init(String name) {
        if (this.baseStats != null && !name.equals(this.baseStats.pixelmonName)) {
            this.baseStats = null;
        }
        super.init(name);
        if (this.getTextureType() == null) {
            this.setTextureType(EnumStatueTextureType.OriginalTexture);
        }
        this.initAnimation();
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        nbt.setFloat("StartingYaw", this.rotationYaw);
        nbt.setString("statueLabel", this.getLabel());
        nbt.setShort("statueTexture", (short)this.getTextureType().ordinal());
        nbt.setString("statueAnimation", this.getAnimation());
        nbt.setInteger("statueFrame", this.getAnimationFrame());
        nbt.setBoolean("statueModelType", !this.getIsFlying());
        if (!this.dataManager.get(EntityPixelmon.dwCustomTexture).isEmpty()) {
            nbt.setString("CustomTexture", this.dataManager.get(EntityPixelmon.dwCustomTexture));
        } else {
            nbt.removeTag("CustomTexture");
        }
        nbt.setInteger("statueSpecial", this.getSpecialTextureId());
        nbt.setBoolean("wasPlacedByAdmin", this.isAdminPlaced());
        nbt.setString("spec", this.dataManager.get(dwSpec));
        nbt.setBoolean("animated", this.dataManager.get(dwAnimated));
        nbt.setBoolean("usegmaxmodel", this.isGmaxModel());
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        if (nbt.hasKey("StartingYaw")) {
            this.setRotation(nbt.getFloat("StartingYaw"));
        }
        if (nbt.hasKey("statueLabel")) {
            this.setLabel(nbt.getString("statueLabel"));
            this.setTextureType(EnumStatueTextureType.getFromOrdinal(nbt.getShort("statueTexture")));
        }
        if (nbt.hasKey("statueAnimation")) {
            this.setAnimation(nbt.getString("statueAnimation"));
            this.setAnimationFrame(nbt.getInteger("statueFrame"));
            this.setIsFlying(!nbt.getBoolean("statueModelType"));
        }
        this.dataManager.set(EntityPixelmon.dwCustomTexture, nbt.getString("CustomTexture"));
        this.setSpecialTextureId(nbt.getInteger("statueSpecial"));
        this.dataManager.set(dwAdminPlaced, nbt.getBoolean("wasPlacedByAdmin"));
        this.dataManager.set(dwSpec, nbt.getString("spec"));
        this.dataManager.set(dwAnimated, nbt.getBoolean("animated"));
        this.setGmaxModel(nbt.getBoolean("usegmaxmodel"));
    }

    @Override
    public void onUpdate() {
        this.motionZ = 0.0;
        this.motionY = 0.0;
        this.motionX = 0.0;
        this.rotationYaw = this.rotationYawHead = this.getRotation();
        super.onUpdate();
        if (this.world.isRemote && (this.lastAnimation == null || this.lastFrame == -1 || this.lastFrame != this.getAnimationFrame() || !this.lastAnimation.equals(this.getAnimation())) && this.isSmd()) {
            this.lastAnimation = this.getAnimation();
            this.lastFrame = this.getAnimationFrame();
            ValveStudioModel model = ((PixelmonModelSmd)this.getModel()).theModel;
            model.setAnimation(this.getAnimation());
            SmdAnimation theAnim = model.currentAnimation;
            if (theAnim == null || theAnim.totalFrames == 0) {
                return;
            }
            if (this.getAnimationFrame() >= theAnim.totalFrames) {
                this.setAnimationFrame(theAnim.totalFrames - 1);
            }
            theAnim.setCurrentFrame(this.getAnimationFrame());
            model.animate();
        }
    }

    @Override
    public boolean processInteract(EntityPlayer player, EnumHand hand) {
        ItemStack stack = player.getHeldItem(hand);
        if (player instanceof EntityPlayerMP && !stack.isEmpty()) {
            if (stack.getItem() == PixelmonItems.chisel && ItemStatueMaker.checkCanPlayerDeleteStatue(this, (EntityPlayerMP)player)) {
                Pixelmon.NETWORK.sendTo(new StatuePacketClient(this.getPokemonId()), (EntityPlayerMP)player);
                player.openGui(Pixelmon.INSTANCE, EnumGui.StatueEditor.getIndex(), this.world, this.getEntityId(), this.isAdminPlaced() ? 1 : 0, 0);
            } else if (stack.getItem() == PixelmonItems.rainbowWing) {
                Optional<EntityPixelmon> optional;
                if (PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get().countAblePokemon() < 1) {
                    player.sendMessage(new TextComponentString("Unable to activate shrine. Your party is fainted."));
                    return false;
                }
                if (!this.dataManager.get(dwSpec).isEmpty() && hand.equals(EnumHand.MAIN_HAND) && (optional = this.checkForHooh(player)).isPresent()) {
                    if (PixelmonConfig.limitHoOh) {
                        PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
                        for (int i = 0; i < 6; ++i) {
                            String name;
                            if (storage.getNBT(storage.getIDFromPosition(i)) == null || !(name = storage.getNBT(storage.getIDFromPosition(i)).getString("Name")).equals("Ho-Oh")) continue;
                            NBTTagCompound compound = storage.getNBT(storage.getIDFromPosition(i));
                            String key = this.getKeyFromSpecies(this.getSpecies());
                            if (key == null) {
                                return false;
                            }
                            if (compound.getBoolean(key)) {
                                player.sendMessage(new TextComponentString((TextFormatting.RED) + "Your Ho-Oh has already summoned a " + this.getSpecies().name + "!"));
                                return false;
                            }
                            compound.setBoolean(key, true);
                        }
                    }
                    player.getHeldItem(hand).shrink(1);
                    PokemonSpec spec = this.getSpec();
                    optional.ifPresent(a -> {
                        a.friendship.setFriendship(0);
                        a.update(EnumUpdateType.Friendship);
                    });
                    this.world.spawnEntity(SpawningEntity.builder().pixelmon(spec).location(this).maxTick(100).colors(colors.getOrDefault(spec.name, Lists.newArrayList(SpawnColors.WHITE))).build(this.world, (EntityPlayerMP)player));
                }
            }
        }
        return false;
    }

    public String getKeyFromSpecies(EnumSpecies species) {
        switch (species) {
            case Raikou: {
                return "Raikou";
            }
            case Entei: {
                return "Entei";
            }
            case Suicune: {
                return "Suicune";
            }
        }
        return null;
    }

    private Optional<EntityPixelmon> checkForHooh(EntityPlayer player) {
        PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
        return IntStream.range(0, 6).mapToObj(storage::getIDFromPosition).map(id -> storage.getPokemon((int[])id, player.world)).filter(Objects::nonNull).filter(a -> a.getSpecies() == EnumSpecies.Hooh).filter(a -> a.friendship.getFriendship() >= FriendShip.getMaxFriendship()).findFirst();
    }

    @Override
    public EntityAgeable createChild(EntityAgeable p_90011_1_) {
        return null;
    }

    @Override
    protected boolean canDespawn() {
        return false;
    }

    @Override
    public void fall(float distance, float damageMultiplier) {
    }

    @Override
    public boolean canBreatheUnderwater() {
        return true;
    }

    public EnumStatueTextureType getTextureType() {
        return EnumStatueTextureType.getFromOrdinal(this.dataManager.get(EntityPixelmon.dwTextures));
    }

    public void setTextureType(EnumStatueTextureType type) {
        this.setBoss(type.bossMode);
        this.dataManager.set(EntityPixelmon.dwTextures, type.ordinal());
    }

    public String getLabel() {
        return this.dataManager.get(dwLabel);
    }

    public void setLabel(String label) {
        this.dataManager.set(dwLabel, label);
    }

    @SideOnly(value=Side.CLIENT)
    public ResourceLocation getTexture() {
        EnumStatueTextureType type = this.getTextureType();
        if (type == EnumStatueTextureType.Stone) {
            return new ResourceLocation("pixelmon:textures/pokemon/statue.png");
        }
        if (type == EnumStatueTextureType.Prismarine) {
            return new ResourceLocation("pixelmon:textures/pokemon/statue_prismarine.png");
        }
        if (type == EnumStatueTextureType.Gold) {
            return new ResourceLocation("pixelmon:textures/pokemon/gold.png");
        }
        if (type == EnumStatueTextureType.Silver) {
            return new ResourceLocation("pixelmon:textures/pokemon/silver.png");
        }
        if (type == EnumStatueTextureType.Bronze) {
            return new ResourceLocation("pixelmon:textures/pokemon/bronze.png");
        }
        EnumSpecies pokemon = this.getSpecies();
        String specialTexture = this.baseStats.pokemon.getSpecialTexture(this.getFormEnum(), this.getSpecialTextureId()).getTexture();
        return Entity4Textures.getTextureFor(pokemon, pokemon.getFormEnum(this.getForm()), this.getGender(), !specialTexture.isEmpty() ? specialTexture : (this.dataManager.get(EntityPixelmon.dwCustomTexture).isEmpty() ? "" : "-" + this.dataManager.get(EntityPixelmon.dwCustomTexture)), type == EnumStatueTextureType.Shiny);
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    public void setRotation(float rotation) {
        this.dataManager.set(dwRotation, Float.valueOf(rotation));
    }

    public float getRotation() {
        return this.dataManager.get(dwRotation).floatValue();
    }

    @Override
    public AnimationType getCurrentAnimation() {
        return AnimationType.getTypeFor(this.getAnimation());
    }

    public String getAnimation() {
        String animationType = this.dataManager.get(dwAnimation);
        if (animationType.equals("")) {
            animationType = "walk";
            this.setAnimation(animationType);
        }
        return animationType;
    }

    public void setAnimation(String animationName) {
        this.dataManager.set(dwAnimation, animationName);
    }

    public int getAnimationFrame() {
        return this.dataManager.get(dwAnimationFrame);
    }

    public void setAnimationFrame(int animationFrame) {
        if (animationFrame < 0) {
            animationFrame = 0;
        }
        this.dataManager.set(dwAnimationFrame, animationFrame);
    }

    @SideOnly(value=Side.CLIENT)
    public boolean isSmd() {
        return this.getModel() instanceof PixelmonModelSmd;
    }

    @SideOnly(value=Side.CLIENT)
    public int getFrameCount() {
        if (this.isSmd()) {
            ValveStudioModel model = ((PixelmonModelSmd)this.getModel()).theModel;
            model.setAnimation(this.getAnimation());
            SmdAnimation theAnim = model.currentAnimation;
            return theAnim == null ? 0 : theAnim.totalFrames;
        }
        return 0;
    }

    @SideOnly(value=Side.CLIENT)
    public String nextAnimation() {
        if (this.isSmd()) {
            String animation = this.getAnimation();
            Iterator<Map.Entry<String, SmdAnimation>> it = ((PixelmonModelSmd)this.getModel()).theModel.anims.entrySet().iterator();
            String first = null;
            boolean takeNext = false;
            while (it.hasNext()) {
                Map.Entry<String, SmdAnimation> entry = it.next();
                String key = entry.getKey();
                if (first == null) {
                    first = key;
                }
                if (takeNext) {
                    return key;
                }
                if (!key.equals(animation)) continue;
                takeNext = true;
            }
            return first;
        }
        return null;
    }

    @SideOnly(value=Side.CLIENT)
    public List<String> getAllAnimations() {
        ArrayList<String> animations = new ArrayList<String>();
        if (this.isSmd()) {
            animations.addAll(((PixelmonModelSmd)this.getModel()).theModel.anims.keySet());
        }
        return animations;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public ModelBase getModel() {
        ModelBase model;
        String specialTexture;
        if (this.isGmaxModel()) {
            try {
                int size = PixelmonModelRegistry.gmaxModels.get((Object)this.getSpecies()).size();
                if (size == 1) {
                    PixelmonModelHolder<?> holder = PixelmonModelRegistry.gmaxModels.get((Object)this.getSpecies()).get(null);
                    return holder != null ? (ModelBase)holder.getModel() : null;
                }
                if (size > 1) {
                    PixelmonModelHolder<?> holder = PixelmonModelRegistry.gmaxModels.get((Object)this.getSpecies()).get(this.getFormEnum());
                    return holder != null ? (ModelBase)holder.getModel() : null;
                }
            }
            catch (Exception e) {
                return null;
            }
        }
        if (!(specialTexture = this.getSpecialTexture()).isEmpty() && (model = PixelmonModelRegistry.getSpecialTextureModel(this.getSpecies(), specialTexture)) != null) {
            return model;
        }
        try {
            if (this.getIsFlying() && this.hasFlyingModel()) {
                return PixelmonModelRegistry.getFlyingModel(this.getSpecies(), this.getFormEnum());
            }
            return PixelmonModelRegistry.getModel(this.getSpecies(), this.getFormEnum());
        }
        catch (NullPointerException e) {
            return null;
        }
    }

    public boolean getIsFlying() {
        return this.dataManager.get(dwIsFlying);
    }

    public void setIsFlying(boolean isFlying) {
        this.dataManager.set(dwIsFlying, isFlying);
    }

    @SideOnly(value=Side.CLIENT)
    public boolean hasFlyingModel() {
        return PixelmonModelRegistry.hasFlyingModel(this.getSpecies(), this.getSpecies().getFormEnum(this.getForm()));
    }

    public int getSpecialTextureId() {
        return this.dataManager.get(dwSpecial);
    }

    public void setSpecialTextureId(int special) {
        this.dataManager.set(dwSpecial, special);
    }

    public void setSpec(PokemonSpec spec) {
        this.setSpec(spec.toString());
    }

    public void setSpec(String spec) {
        this.dataManager.set(dwSpec, spec);
    }

    public PokemonSpec getSpec() {
        return new PokemonSpec(this.dataManager.get(dwSpec));
    }

    public boolean isAdminPlaced() {
        return this.dataManager.get(dwAdminPlaced);
    }

    public void setAdminPlaced(Boolean adminPlaced) {
        this.dataManager.set(dwAdminPlaced, adminPlaced);
    }

    public String getSpecText() {
        return this.dataManager.get(dwSpec);
    }

    public String getCustomSpecial() {
        return this.dataManager.get(EntityPixelmon.dwCustomTexture);
    }

    public void setCustomSpecial(String special) {
        this.dataManager.set(EntityPixelmon.dwCustomTexture, special);
    }

    public String getSpecialTexture() {
        IEnumSpecialTexture enumm = this.baseStats.pokemon.getSpecialTexture(this.getFormEnum(), this.getSpecialTextureId());
        String texturCustom = this.dataManager.get(EntityPixelmon.dwCustomTexture);
        return !enumm.hasTexutre() ? (texturCustom.isEmpty() ? "" : "-" + texturCustom) : enumm.getTexture();
    }

    public boolean isAnimated() {
        return this.dataManager.get(dwAnimated);
    }

    public void setAnimated(boolean animated) {
        this.dataManager.set(dwAnimated, animated);
    }

    public boolean isGmaxModel() {
        return this.dataManager.get(dwGmaxModel);
    }

    public void setGmaxModel(boolean useGmaxModel) {
        this.dataManager.set(dwGmaxModel, useGmaxModel);
    }
}

