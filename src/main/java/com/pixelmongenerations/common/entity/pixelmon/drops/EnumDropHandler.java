/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.drops;

import com.pixelmongenerations.common.entity.pixelmon.Entity8HoldsItems;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.core.enums.EnumBossMode;
import java.util.function.BiConsumer;
import net.minecraft.entity.player.EntityPlayerMP;

public enum EnumDropHandler {
    BOSS((dropHelper, player) -> dropHelper.dropBossItems((EntityPlayerMP)player)),
    TOTEM((dropHelper, player) -> dropHelper.dropTotemItems((EntityPlayerMP)player)),
    ALPHA((dropHelper, player) -> dropHelper.dropAlphaItems((EntityPlayerMP)player)),
    NORMAL((dropHelper, player) -> dropHelper.dropNormalItems((EntityPlayerMP)player));

    private BiConsumer<DropItemHelper, EntityPlayerMP> handler;

    private EnumDropHandler(BiConsumer<DropItemHelper, EntityPlayerMP> handler) {
        this.handler = handler;
    }

    public static void dropFrom(Entity8HoldsItems pokemon, EntityPlayerMP player) {
        if (pokemon.getBossMode() != EnumBossMode.NotBoss) {
            EnumDropHandler.BOSS.handler.accept(pokemon.getDropHelper(), player);
        } else if (pokemon.isTotem()) {
            EnumDropHandler.TOTEM.handler.accept(pokemon.getDropHelper(), player);
        } else if (pokemon.isAlpha()) {
            EnumDropHandler.ALPHA.handler.accept(pokemon.getDropHelper(), player);
        } else {
            EnumDropHandler.NORMAL.handler.accept(pokemon.getDropHelper(), player);
        }
    }
}

