/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.PixelmonData;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLog;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class Cut
extends ExternalMoveBase {
    ArrayList<EnumFacing[]> checkDirections;

    public Cut() {
        super("cut", "Cut");
    }

    @Override
    public boolean execute(EntityPixelmon user, RayTraceResult target, int moveIndex) {
        if (target.typeOfHit == RayTraceResult.Type.BLOCK) {
            World world = user.world;
            BlockPos pos = target.getBlockPos();
            Block block = world.getBlockState(pos).getBlock();
            if (block != Blocks.LOG && block != Blocks.LOG2) {
                return false;
            }
            if (world.getBlockState(pos).getValue(BlockLog.LOG_AXIS) != BlockLog.EnumAxis.Y) {
                return false;
            }
            if (this.checkDirections == null) {
                this.setCheckDirections();
            }
            List<BlockPos> logs = this.findTreeLogs(world, pos);
            if (user.stats.Attack < 300 && logs.size() > user.stats.Attack / 30 * 5 + 5) {
                ChatHandler.sendChat(user.getOwner(), "externalMove.Cut.fail", user.getNickname());
                return false;
            }
            for (BlockPos log : logs) {
                IBlockState state = world.getBlockState(log);
                if (!((EntityPlayerMP)user.getOwner()).interactionManager.tryHarvestBlock(log)) continue;
                block.dropBlockAsItem(world, log, state, 2);
            }
            return true;
        }
        return false;
    }

    private List<BlockPos> findTreeLogs(World world, BlockPos pos) {
        ArrayList<BlockPos> logs = new ArrayList<BlockPos>();
        ArrayList<BlockPos> newLogs = new ArrayList<BlockPos>();
        newLogs.add(pos);
        while (!newLogs.isEmpty()) {
            this.addAdjacentLogs(world, (BlockPos)newLogs.get(0), newLogs, logs);
            logs.add(newLogs.get(0));
            newLogs.remove(0);
        }
        return logs;
    }

    private void addAdjacentLogs(World world, BlockPos pos, List<BlockPos> newLogs, List<BlockPos> logs) {
        for (EnumFacing[] facings : this.checkDirections) {
            for (EnumFacing f : facings) {
                pos = pos.offset(f);
            }
            Block block = world.getBlockState(pos).getBlock();
            if (block != Blocks.LOG && block != Blocks.LOG2 || this.hasLog(logs, pos) || this.hasLog(newLogs, pos)) continue;
            newLogs.add(pos);
        }
    }

    private boolean hasLog(List<BlockPos> logs, BlockPos loc) {
        for (BlockPos log : logs) {
            if (!log.equals(loc)) continue;
            return true;
        }
        return false;
    }

    @Override
    public int getCooldown(EntityPixelmon user) {
        return 800 - (int)((double)user.stats.Speed * 1.5);
    }

    @Override
    public int getCooldown(PixelmonData user) {
        return 800 - (int)((double)user.Speed * 1.5);
    }

    @Override
    public boolean isDestructive() {
        return true;
    }

    private void setCheckDirections() {
        this.checkDirections = new ArrayList();
        this.checkDirections.add(new EnumFacing[]{EnumFacing.NORTH});
        this.checkDirections.add(new EnumFacing[]{EnumFacing.SOUTH});
        this.checkDirections.add(new EnumFacing[]{EnumFacing.EAST});
        this.checkDirections.add(new EnumFacing[]{EnumFacing.WEST});
        this.checkDirections.add(new EnumFacing[]{EnumFacing.UP});
        this.checkDirections.add(new EnumFacing[]{EnumFacing.UP, EnumFacing.NORTH});
        this.checkDirections.add(new EnumFacing[]{EnumFacing.UP, EnumFacing.SOUTH});
        this.checkDirections.add(new EnumFacing[]{EnumFacing.UP, EnumFacing.EAST});
        this.checkDirections.add(new EnumFacing[]{EnumFacing.UP, EnumFacing.WEST});
    }
}

