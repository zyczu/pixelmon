/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class Rush
extends AbilityBase {
    public StatusType stormType;

    public Rush(StatusType stormType) {
        this.stormType = stormType;
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        if (user.bc.globalStatusController.hasStatus(this.stormType)) {
            int n = StatsType.Speed.getStatIndex();
            stats[n] = stats[n] * 2;
        }
        return stats;
    }
}

