/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.EVsStore;
import com.pixelmongenerations.common.entity.pixelmon.stats.IVStore;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.network.PixelmonData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.NoSuchElementException;
import net.minecraft.nbt.NBTTagCompound;

public class Stats {
    public int HP;
    public int Attack;
    public int Defence;
    public int SpecialAttack;
    public int SpecialDefence;
    public int Speed;
    public IVStore IVs = new IVStore();
    public ArrayList<StatsType> bottleCapIVs = new ArrayList();
    public EVsStore EVs = new EVsStore();
    public boolean inBattle = false;

    public Stats setLevelStats(EnumNature nature, EnumNature pseudoNature, BaseStats baseStats, int level) {
        this.HP = this.calculateHP(baseStats, level);
        this.Attack = this.calculateStat(StatsType.Attack, nature, pseudoNature, baseStats, level);
        this.Defence = this.calculateStat(StatsType.Defence, nature, pseudoNature, baseStats, level);
        this.SpecialAttack = this.calculateStat(StatsType.SpecialAttack, nature, pseudoNature, baseStats, level);
        this.SpecialDefence = this.calculateStat(StatsType.SpecialDefence, nature, pseudoNature, baseStats, level);
        this.Speed = this.calculateStat(StatsType.Speed, nature, pseudoNature, baseStats, level);
        return this;
    }

    public int calculateHP(BaseStats baseStats, int level) {
        if (baseStats.hp == 1) {
            return 1;
        }
        int hpIV = !this.bottleCapIVs.contains((Object)StatsType.HP) ? this.IVs.HP : 31;
        return (int)(((float)hpIV + 2.0f * (float)baseStats.hp + (float)this.EVs.HP / 4.0f + 100.0f) * (float)level / 100.0f + 10.0f);
    }

    public int calculateStat(StatsType stat, EnumNature nature, EnumNature pseudoNature, BaseStats baseStats, int level) {
        int iv = !this.bottleCapIVs.contains((Object)stat) ? this.IVs.get(stat) : 31;
        float val = ((float)iv + 2.0f * (float)baseStats.get(stat) + (float)this.EVs.get(stat) / 4.0f) * (float)level;
        val /= 100.0f;
        val += 5.0f;
        val = (float)Math.floor(val);
        if (pseudoNature == null) {
            if (stat == nature.increasedStat) {
                val *= 1.1f;
            } else if (stat == nature.decreasedStat) {
                val *= 0.9f;
            }
        } else if (stat == pseudoNature.increasedStat) {
            val *= 1.1f;
        } else if (stat == pseudoNature.decreasedStat) {
            val *= 0.9f;
        }
        return (int)val;
    }

    public boolean[] getBottleCapIVArray() {
        boolean[] bcArray = new boolean[6];
        int i = 0;
        for (StatsType type : StatsType.getMainTypes()) {
            bcArray[i] = this.bottleCapIVs.contains((Object)type);
            ++i;
        }
        return bcArray;
    }

    public void writeToNBT(NBTTagCompound var1) {
        var1.setInteger("StatsHP", this.HP);
        var1.setInteger("StatsAttack", this.Attack);
        var1.setInteger("StatsDefence", this.Defence);
        var1.setInteger("StatsSpecialAttack", this.SpecialAttack);
        var1.setInteger("StatsSpecialDefence", this.SpecialDefence);
        var1.setInteger("StatsSpeed", this.Speed);
        this.IVs.writeToNBT(var1);
        this.EVs.writeToNBT(var1);
        for (StatsType statType : this.bottleCapIVs) {
            var1.setBoolean("BottleCap" + statType.name(), true);
        }
    }

    public void readFromNBT(NBTTagCompound var1) {
        this.HP = var1.getInteger("StatsHP");
        this.Attack = var1.getInteger("StatsAttack");
        this.Defence = var1.getInteger("StatsDefence");
        this.SpecialAttack = var1.getInteger("StatsSpecialAttack");
        this.SpecialDefence = var1.getInteger("StatsSpecialDefence");
        this.Speed = var1.getInteger("StatsSpeed");
        this.IVs.readFromNBT(var1);
        this.EVs.readFromNBT(var1);
        this.bottleCapIVs = new ArrayList();
        for (StatsType statType : StatsType.getMainTypes()) {
            if (!var1.getBoolean("BottleCap" + statType.name())) continue;
            this.bottleCapIVs.add(statType);
        }
    }

    public void getNBTTags(HashMap<String, Class> tags) {
        tags.put("StatsHP", Integer.class);
        tags.put("StatsAttack", Integer.class);
        tags.put("StatsDefence", Integer.class);
        tags.put("StatsSpecialAttack", Integer.class);
        tags.put("StatsSpecialDefence", Integer.class);
        tags.put("StatsSpeed", Integer.class);
        IVStore.getNBTTags(tags);
        EVsStore.getNBTTags(tags);
    }

    public static void calculateStatsForData(PixelmonData data) {
        BaseStats baseStats;
        Stats stats = new Stats();
        stats.EVs = new EVsStore(data.evs);
        stats.IVs = new IVStore(data.ivs);
        if (data.caps[0]) {
            stats.addBottleCapIV(StatsType.HP);
        }
        if (data.caps[1]) {
            stats.addBottleCapIV(StatsType.Attack);
        }
        if (data.caps[2]) {
            stats.addBottleCapIV(StatsType.Defence);
        }
        if (data.caps[3]) {
            stats.addBottleCapIV(StatsType.SpecialAttack);
        }
        if (data.caps[4]) {
            stats.addBottleCapIV(StatsType.SpecialDefence);
        }
        if (data.caps[5]) {
            stats.addBottleCapIV(StatsType.Speed);
        }
        try {
            baseStats = Entity3HasStats.getBaseStats(data.name, (int)data.form).get();
        }
        catch (NoSuchElementException e) {
            return;
        }
        stats.setLevelStats(data.nature, data.pseudoNature, baseStats, data.lvl);
        data.health = stats.HP;
        data.hp = stats.HP;
        data.HP = stats.HP;
        data.Attack = stats.Attack;
        data.Defence = stats.Defence;
        data.SpecialAttack = stats.SpecialAttack;
        data.SpecialDefence = stats.SpecialDefence;
        data.Speed = stats.Speed;
    }

    public int get(StatsType stat) {
        switch (stat) {
            case HP: {
                return this.HP;
            }
            case Attack: {
                return this.Attack;
            }
            case Defence: {
                return this.Defence;
            }
            case SpecialAttack: {
                return this.SpecialAttack;
            }
            case SpecialDefence: {
                return this.SpecialDefence;
            }
            case Speed: {
                return this.Speed;
            }
        }
        return -1;
    }

    public boolean isBottleCapIV(StatsType stat) {
        return this.bottleCapIVs.contains((Object)stat);
    }

    public void addBottleCapIV(StatsType stat) {
        this.bottleCapIVs.add(stat);
    }

    public boolean isBottleCapMaxed() {
        return this.bottleCapIVs.size() == 6;
    }
}

