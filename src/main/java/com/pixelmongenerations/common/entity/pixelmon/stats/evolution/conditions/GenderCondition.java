/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions;

import com.google.common.collect.Lists;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import java.util.ArrayList;

public class GenderCondition
extends EvoCondition {
    public ArrayList<Gender> genders = Lists.newArrayList();

    public GenderCondition() {
    }

    public GenderCondition(Gender ... genders) {
        this.genders = Lists.newArrayList(genders);
    }

    @Override
    public boolean passes(EntityPixelmon pokemon) {
        return this.genders.contains(pokemon.getGender());
    }
}

