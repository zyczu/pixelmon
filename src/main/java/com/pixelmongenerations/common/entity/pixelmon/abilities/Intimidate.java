/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.CalcPriority;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Contrary;
import com.pixelmongenerations.common.entity.pixelmon.abilities.InnerFocus;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Oblivious;
import com.pixelmongenerations.common.entity.pixelmon.abilities.OwnTempo;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Rattled;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Scrappy;
import com.pixelmongenerations.common.entity.pixelmon.stats.BattleStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;

public class Intimidate
extends AbilityBase {
    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        boolean sentMessage = false;
        boolean firstPokemon = true;
        ArrayList<PixelmonWrapper> opponents = newPokemon.bc.getOpponentPokemon(newPokemon);
        for (PixelmonWrapper pw : CalcPriority.getTurnOrder(opponents)) {
            AbilityBase abil;
            if (!sentMessage) {
                this.sendActivatedMessage(newPokemon);
                sentMessage = true;
            }
            if ((abil = pw.getBattleAbility()) instanceof InnerFocus || abil instanceof Oblivious || abil instanceof OwnTempo || abil instanceof Rattled || abil instanceof Scrappy) {
                if (firstPokemon) {
                    pw.bc.sendToAll("pixelmon.abilities.intimidate.unaffected", pw.getNickname());
                }
                firstPokemon = false;
                continue;
            }
            pw.getBattleStats().modifyStat(-1, StatsType.Attack, newPokemon, false);
            firstPokemon = false;
            if (pw.getHeldItem().getHeldItemType() != EnumHeldItems.adrenalineOrb) continue;
            BattleStats bs = pw.getBattleStats();
            if (pw.getBattleAbility() instanceof Contrary && bs.getSpeedStage() > -6 || bs.getSpeedStage() < 6) {
                if (pw.getBattleAbility() instanceof Contrary && bs.getAttackStage() > -6 || bs.getAttackStage() < 6) {
                    pw.bc.sendToAll("pixelmon.helditem.adrenalineorb.activate", pw.getNickname());
                    if (pw.getBattleStats().modifyStat(1, StatsType.Speed)) {
                        pw.consumeItem();
                        continue;
                    }
                    pw.bc.sendToAll("pixelmon.helditem.adrenalineorb.failed", pw.getNickname());
                    continue;
                }
                pw.bc.sendToAll("pixelmon.helditem.adrenalineorb.failed", pw.getNickname());
                continue;
            }
            pw.bc.sendToAll("pixelmon.helditem.adrenalineorb.failed", pw.getNickname());
        }
    }
}

