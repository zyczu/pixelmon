/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ComingSoon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Defeatist;
import com.pixelmongenerations.common.entity.pixelmon.abilities.DesolateLand;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MoldBreaker;
import com.pixelmongenerations.common.entity.pixelmon.abilities.NeutralizingGas;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PrimordialSea;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SlowStart;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Teravolt;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Truant;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Turboblaze;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.util.RegexPatterns;
import com.pixelmongenerations.core.util.helper.ArrayHelper;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import net.minecraft.util.text.translation.I18n;

public abstract class AbilityBase {
    public final String getName() {
        return this.getClass().getSimpleName();
    }

    public String getLocalizedName() {
        return I18n.translateToLocal("ability." + this.getName() + ".name");
    }

    public void startMove(PixelmonWrapper user) {
    }

    public boolean canAttackThisTurn(PixelmonWrapper user, Attack a) {
        return true;
    }

    public int modifyDamageUser(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return damage;
    }

    public int modifyDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return damage;
    }

    public int modifyDamageIncludeFixed(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return damage;
    }

    public int modifyDamageTeammate(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return damage;
    }

    public void tookDamageUser(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
    }

    public void tookDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
    }

    public void tookDamageTargetAfterMove(PixelmonWrapper user, PixelmonWrapper target, Attack a) {
    }

    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return new int[]{power, accuracy};
    }

    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return new int[]{power, accuracy};
    }

    public int[] modifyPowerAndAccuracyTeammate(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return new int[]{power, accuracy};
    }

    public void beforeSwitch(PixelmonWrapper newPokemon) {
    }

    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
    }

    public void applySwitchOutEffect(PixelmonWrapper oldPokemon) {
    }

    public void applyEndOfBattleEffect(PixelmonWrapper pokemon) {
    }

    public void applyEffectOnContactUser(PixelmonWrapper user, PixelmonWrapper target) {
    }

    public void applyEffectOnContactTarget(PixelmonWrapper user, PixelmonWrapper target) {
    }

    public void applyEffectOnContactTargetLate(PixelmonWrapper user, PixelmonWrapper target) {
    }

    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        return stats;
    }

    public int[] modifyStatsTeammate(PixelmonWrapper pokemon, int[] stats) {
        return stats;
    }

    public int[] modifyStatsCancellable(PixelmonWrapper user, int[] stats) {
        return stats;
    }

    public int[] modifyStatsCancellableTeammate(PixelmonWrapper pokemon, int[] stats) {
        return stats;
    }

    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
    }

    public boolean allowsStatChange(PixelmonWrapper pokemon, PixelmonWrapper user, StatsEffect e) {
        return true;
    }

    public boolean allowsStatChangeTeammate(PixelmonWrapper pokemon, PixelmonWrapper target, PixelmonWrapper user, StatsEffect e) {
        return true;
    }

    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        return true;
    }

    public void allowsIncomingAttackMessage(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
    }

    public void preProcessAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
    }

    public boolean preventsCriticalHits(PixelmonWrapper opponent) {
        return false;
    }

    public boolean allowsStatus(StatusType status, PixelmonWrapper pokemon, PixelmonWrapper user) {
        return true;
    }

    public boolean allowsStatusTeammate(StatusType status, PixelmonWrapper pokemon, PixelmonWrapper target, PixelmonWrapper user) {
        return true;
    }

    public double modifyStab(double stab) {
        return stab;
    }

    public float modifyPriority(PixelmonWrapper pokemon, float priority) {
        return priority;
    }

    public boolean stopsSwitching(PixelmonWrapper user, PixelmonWrapper opponent) {
        return false;
    }

    public float modifyWeight(float initWeight) {
        return initWeight;
    }

    public List<EnumType> getEffectiveTypes(PixelmonWrapper user, PixelmonWrapper target) {
        return target.type;
    }

    public void onStatusAdded(StatusBase status, PixelmonWrapper user, PixelmonWrapper opponent) {
    }

    public boolean ignoreWeather() {
        return false;
    }

    public boolean redirectAttack(PixelmonWrapper user, PixelmonWrapper targetAlly, Attack attack) {
        return false;
    }

    public void onItemConsumed(PixelmonWrapper pokemon, PixelmonWrapper consumer, ItemHeld heldItem) {
    }

    public void onAbilityLost(PixelmonWrapper pokemon) {
    }

    public boolean needNewInstance() {
        return false;
    }

    public boolean isNegativeAbility() {
        return this instanceof Defeatist || this instanceof SlowStart || this instanceof Truant;
    }

    public void onWeatherChange(PixelmonWrapper pw, Weather weather) {
    }

    public static boolean ignoreAbility(PixelmonWrapper pokemon) {
        if (pokemon == null) {
            return false;
        }
        return pokemon.hasStatus(StatusType.GastroAcid);
    }

    public static boolean ignoreAbility(PixelmonWrapper user, PixelmonWrapper opponent) {
        AbilityBase userAbility;
        if (user != null && user.attack != null && (user.attack.getAttackBase().getUnlocalizedName().equals("G-Max Fireball") || user.attack.getAttackBase().getUnlocalizedName().equals("G-Max Drum Solo") || user.attack.getAttackBase().getUnlocalizedName().equals("G-Max Hydrosnipe") || user.attack.getAttackBase().getUnlocalizedName().equals("Sunsteel Strike") || user.attack.getAttackBase().getUnlocalizedName().equals("Photon Geyser") || user.attack.getAttackBase().getUnlocalizedName().equals("Moongeist Beam"))) {
            return true;
        }
        AbilityBase abilityBase = userAbility = user == null ? null : user.getBattleAbility();
        if (AbilityBase.checkNeturalizingGas(opponent) && userAbility != null && userAbility.canBeNeutralized()) {
            return true;
        }
        if (opponent.getAbility() instanceof PrimordialSea) {
            return false;
        }
        if (opponent.getAbility() instanceof DesolateLand) {
            return false;
        }
        return AbilityBase.ignoreAbility(opponent) || userAbility instanceof MoldBreaker || userAbility instanceof Teravolt || userAbility instanceof Turboblaze;
    }

    public boolean canBeNeutralized() {
        return false;
    }

    public static boolean checkNeturalizingGas(PixelmonWrapper pokemon) {
        BattleControllerBase bc;
        EntityPixelmon pixelmon = pokemon.pokemon;
        if (pixelmon != null && (bc = pixelmon.battleController) != null) {
            return bc.getActivePokemon().stream().map(a -> a.getBattleAbility(false)).filter(Objects::nonNull).anyMatch(NeutralizingGas.class::isInstance);
        }
        return false;
    }

    public boolean equals(Object o) {
        return o instanceof AbilityBase && ((AbilityBase)o).getName().equals(this.getName());
    }

    public int hashCode() {
        return this.getName().hashCode();
    }

    @SafeVarargs
    public final boolean isAbility(Class<? extends AbilityBase> ... abilities) {
        return ArrayHelper.contains(abilities, this.getClass());
    }

    public static Optional<AbilityBase> getAbility(String name) {
        name = RegexPatterns.SPACE_SYMBOL.matcher(name).replaceAll("");
        try {
            Class<?> abilityClass = Class.forName("com.pixelmongenerations.common.entity.pixelmon.abilities." + name);
            AbilityBase ability = (AbilityBase)abilityClass.getConstructor(new Class[0]).newInstance(new Object[0]);
            return Optional.of(ability);
        }
        catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            return Optional.empty();
        }
    }

    public AbilityBase getNewInstance() {
        return AbilityBase.getNewInstance(this.getClass());
    }

    public void sendActivatedMessage(PixelmonWrapper pw) {
        pw.bc.sendToAll("pixelmon.abilities.activated", pw.getNickname(), this.getLocalizedName());
    }

    public static AbilityBase getNewInstance(Class<? extends AbilityBase> abilityClass) {
        try {
            return abilityClass.getConstructor(new Class[0]).newInstance(new Object[0]);
        }
        catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            return ComingSoon.noAbility;
        }
    }

    public void onPokemonFaintSelf(PixelmonWrapper pokemon) {
    }

    public void onPokemonFaintOther(PixelmonWrapper user, PixelmonWrapper pokemon) {
    }

    public boolean allowsAttack(PixelmonWrapper user, PixelmonWrapper target, Attack attack) {
        return true;
    }

    public void onAttackUsed(PixelmonWrapper pixelmonWrapper, Attack attack) {
    }

    public int[] modifyStatsItem(PixelmonWrapper user, int[] stats) {
        return stats;
    }

    public void applyRepeatedEffectItem(PixelmonWrapper pokemon) {
    }
}

