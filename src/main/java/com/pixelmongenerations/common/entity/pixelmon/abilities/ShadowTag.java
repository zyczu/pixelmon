/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class ShadowTag
extends AbilityBase {
    @Override
    public boolean stopsSwitching(PixelmonWrapper user, PixelmonWrapper opponent) {
        return !(opponent.getBattleAbility() instanceof ShadowTag);
    }
}

