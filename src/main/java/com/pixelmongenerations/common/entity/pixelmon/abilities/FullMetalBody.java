/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.PreventStatDrop;

public class FullMetalBody
extends PreventStatDrop {
    public FullMetalBody() {
        super("pixelmon.abilities.fullmetalbody");
    }
}

