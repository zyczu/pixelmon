/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.textures;

import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;

public enum EnumLugiaTextures implements IEnumSpecialTexture
{
    Shadow(1);

    private int id;

    private EnumLugiaTextures(int id) {
        this.id = id;
    }

    @Override
    public boolean hasTexutre() {
        return true;
    }

    @Override
    public String getTexture() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public String getProperName() {
        return this.name();
    }

    @Override
    public boolean hasShinyVariant() {
        return true;
    }

    @Override
    public int getId() {
        return this.id;
    }
}

