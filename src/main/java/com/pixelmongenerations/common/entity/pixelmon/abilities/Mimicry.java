/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.ElectricTerrain;
import com.pixelmongenerations.common.battle.status.GrassyTerrain;
import com.pixelmongenerations.common.battle.status.MistyTerrain;
import com.pixelmongenerations.common.battle.status.NoTerrain;
import com.pixelmongenerations.common.battle.status.PsychicTerrain;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;

public class Mimicry
extends AbilityBase {
    @Override
    public int[] modifyStats(PixelmonWrapper pokemon, int[] stats) {
        if (pokemon.bc.globalStatusController.getTerrain() instanceof ElectricTerrain && !pokemon.isSingleType()) {
            pokemon.setTempType(EnumType.Electric);
            pokemon.bc.sendToAll("pixelmon.abilities.mimicry", pokemon.getNickname(), EnumType.Electric.toString());
            return stats;
        }
        if (pokemon.bc.globalStatusController.getTerrain() instanceof GrassyTerrain && !pokemon.isSingleType()) {
            pokemon.setTempType(EnumType.Grass);
            pokemon.bc.sendToAll("pixelmon.abilities.mimicry", pokemon.getNickname(), EnumType.Grass.toString());
            return stats;
        }
        if (pokemon.bc.globalStatusController.getTerrain() instanceof PsychicTerrain && !pokemon.isSingleType()) {
            pokemon.setTempType(EnumType.Psychic);
            pokemon.bc.sendToAll("pixelmon.abilities.mimicry", pokemon.getNickname(), EnumType.Psychic.toString());
            return stats;
        }
        if (pokemon.bc.globalStatusController.getTerrain() instanceof MistyTerrain && !pokemon.isSingleType()) {
            pokemon.setTempType(EnumType.Fairy);
            pokemon.bc.sendToAll("pixelmon.abilities.mimicry", pokemon.getNickname(), EnumType.Fairy.toString());
            return stats;
        }
        if (pokemon.bc.globalStatusController.getTerrain() instanceof NoTerrain) {
            if (!pokemon.hasType(EnumType.Ground)) {
                pokemon.setTempType(pokemon.getInitialType());
                pokemon.bc.sendToAll("pixelmon.abilities.mimicryrevert", pokemon.getNickname(), EnumType.Normal.toString());
                return stats;
            }
        }
        return stats;
    }
}

