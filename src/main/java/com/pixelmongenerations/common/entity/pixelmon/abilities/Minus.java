/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Plus;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class Minus
extends AbilityBase {
    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        for (PixelmonWrapper pw : user.bc.getTeamPokemon(user.getParticipant())) {
            if (pw == user || !(pw.getBattleAbility() instanceof Plus) && !(pw.getBattleAbility() instanceof Minus)) continue;
            int n = StatsType.SpecialAttack.getStatIndex();
            stats[n] = (int)((double)stats[n] * 1.5);
            return stats;
        }
        return stats;
    }
}

