/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class QuickFeet
extends AbilityBase {
    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        if (user.hasPrimaryStatus()) {
            int n = StatsType.Speed.getStatIndex();
            stats[n] = (int)((double)stats[n] * 1.5);
        }
        return stats;
    }
}

