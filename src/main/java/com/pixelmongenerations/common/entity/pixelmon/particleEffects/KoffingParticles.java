/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.particleEffects;

import com.pixelmongenerations.common.entity.pixelmon.Entity4Textures;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.ParticleEffects;
import com.pixelmongenerations.core.enums.EnumPixelmonParticles;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import net.minecraft.util.math.MathHelper;

public class KoffingParticles
extends ParticleEffects {
    int count = 0;
    boolean particlesOn = true;

    public KoffingParticles(Entity4Textures pixelmon) {
        super(pixelmon);
    }

    @Override
    public void onUpdate() {
        float var2 = this.pixelmon.baseStats.width * this.pixelmon.baseStats.giScale * this.pixelmon.getPixelmonScale();
        float var4 = this.rand.nextFloat() * (float)Math.PI * 2.0f;
        float var5 = this.rand.nextFloat() * 4.0f + 0.5f;
        float var6 = MathHelper.sin(var4) * var2 * 0.5f * var5;
        float var7 = MathHelper.cos(var4) * var2 * 0.5f * var5;
        if (this.count <= 0) {
            this.particlesOn = !this.particlesOn;
            this.count = this.particlesOn ? RandomHelper.rand.nextInt(3) : RandomHelper.rand.nextInt(27);
        }
        --this.count;
        if (this.particlesOn) {
            ClientProxy.spawnParticle(EnumPixelmonParticles.Koffing, this.pixelmon.world, this.pixelmon.posX + (double)var6, this.pixelmon.posY + (double)this.pixelmon.hoverTimer + 1.5, this.pixelmon.posZ + (double)var7, this.pixelmon.isShiny());
        }
    }
}

