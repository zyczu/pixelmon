/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class Download
extends AbilityBase {
    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        int totalDef = 0;
        int totalSpDef = 0;
        for (PixelmonWrapper opponent : newPokemon.bc.getOpponentPokemon(newPokemon.getParticipant())) {
            totalDef += opponent.getBattleStats().defenceStat;
            totalSpDef += opponent.getBattleStats().specialDefenceStat;
        }
        this.sendActivatedMessage(newPokemon);
        if (totalDef < totalSpDef) {
            newPokemon.getBattleStats().modifyStat(1, StatsType.Attack);
        } else {
            newPokemon.getBattleStats().modifyStat(1, StatsType.SpecialAttack);
        }
    }
}

