/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.forms.EnumMorpeko;

public class HungerSwitch
extends AbilityBase {
    @Override
    public void applySwitchInEffect(PixelmonWrapper pokemon) {
        pokemon.setForm(EnumMorpeko.FullBellyMode.getForm());
        this.sendActivatedMessage(pokemon);
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        if (!pokemon.switchedThisTurn) {
            this.sendActivatedMessage(pokemon);
            if (pokemon.getForm() == EnumMorpeko.FullBellyMode.getForm()) {
                pokemon.setForm(EnumMorpeko.HangryMode.getForm());
            } else {
                pokemon.setForm(EnumMorpeko.FullBellyMode.getForm());
            }
        }
    }
}

