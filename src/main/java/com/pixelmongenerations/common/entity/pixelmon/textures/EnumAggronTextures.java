/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.textures;

import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;

public enum EnumAggronTextures implements IEnumSpecialTexture
{
    AgedCopper(1, "Aged Copper"),
    BrassForge(1, "Brass Forge"),
    CarbonSteel(1, "Carbon Steel"),
    CrystalForge(1, "Crystal Forge");

    private int id;
    private String name;

    private EnumAggronTextures(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean hasTexutre() {
        return true;
    }

    @Override
    public String getTexture() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public String getProperName() {
        return this.name;
    }

    @Override
    public int getId() {
        return this.id;
    }
}

