/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumCastform;

public class Forecast
extends AbilityBase {
    @Override
    public void onWeatherChange(PixelmonWrapper pw, Weather weather) {
        if (pw.getSpecies() == EnumSpecies.Castform) {
            if (pw.bc.simulateMode) {
                return;
            }
            StatusType weatherType = weather == null ? StatusType.None : weather.type;
            EnumCastform form = EnumCastform.Normal;
            switch (weatherType) {
                case Rainy: {
                    form = EnumCastform.Rain;
                    break;
                }
                case Sunny: {
                    form = EnumCastform.Sun;
                    break;
                }
                case Hail: {
                    form = EnumCastform.Ice;
                    break;
                }
            }
            int formIndex = form.ordinal();
            if (pw.getForm() != formIndex) {
                pw.setForm(formIndex);
                pw.bc.sendToAll("pixelmon.abilities.changeform", pw.getNickname());
            }
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        this.resetForm(newPokemon);
        this.onWeatherChange(newPokemon, newPokemon.bc.globalStatusController.getWeather());
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper pw) {
        this.resetForm(pw);
    }

    @Override
    public void onAbilityLost(PixelmonWrapper pokemon) {
        this.onWeatherChange(pokemon, null);
    }

    private void resetForm(PixelmonWrapper pw) {
        if (pw.bc.simulateMode) {
            return;
        }
        int normalIndex = EnumCastform.Normal.ordinal();
        if (pw.getForm() != normalIndex) {
            pw.setForm(normalIndex);
        }
    }
}

