/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class MegaLauncher
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (a.isAttack("Aura Sphere", "Dark Pulse", "Dragon Pulse", "Origin Pulse", "Water Pulse")) {
            power = (int)((double)power * 1.5);
        }
        return new int[]{power, accuracy};
    }
}

