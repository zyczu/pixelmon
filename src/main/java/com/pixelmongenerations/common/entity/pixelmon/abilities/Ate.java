/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.ZAttackBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Aerilate;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Galvanize;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Pixilate;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Refrigerate;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public abstract class Ate
extends AbilityBase {
    private EnumType changeType;

    public Ate(EnumType changeType) {
        this.changeType = changeType;
    }

    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        int newPower = power;
        AbilityBase userAbility = user.getAbility();
        if (!(a.getAttackBase() instanceof ZAttackBase) || a.getAttackCategory() == AttackCategory.Status) {
            if (a.getAttackBase().attackType == EnumType.Normal && userAbility instanceof Pixilate) {
                a.overrideType(this.changeType);
                newPower = (int)((double)newPower * 1.2);
            }
            if (a.getAttackBase().attackType == EnumType.Normal && userAbility instanceof Aerilate) {
                a.overrideType(this.changeType);
                newPower = (int)((double)newPower * 1.2);
            }
            if (a.getAttackBase().attackType == EnumType.Normal && userAbility instanceof Galvanize) {
                a.overrideType(this.changeType);
                newPower = (int)((double)newPower * 1.2);
            }
            if (a.getAttackBase().attackType == EnumType.Normal && userAbility instanceof Refrigerate) {
                a.overrideType(this.changeType);
                newPower = (int)((double)newPower * 1.2);
            }
        }
        return new int[]{newPower, accuracy};
    }
}

