/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.PreventStatDrop;

public class MirrorArmor
extends PreventStatDrop {
    public MirrorArmor() {
        super("pixelmon.abilities.mirrorarmor");
    }
}

