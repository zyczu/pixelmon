/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.drops;

import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.common.entity.pixelmon.Entity8HoldsItems;
import com.pixelmongenerations.common.entity.pixelmon.drops.AlphaInfo;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import net.minecraft.item.ItemStack;

public class AlphaDrops {
    public ArrayList<AlphaInfo> alphaPokes;
    public ArrayList<String> anyDropPool;

    public ArrayList<ItemStack> getDropsFor(Entity8HoldsItems pixelmon) {
        ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
        int rng = RandomHelper.getRandomNumberBetween(1, 3);
        for (int i = 0; i < rng; ++i) {
            String item = RandomHelper.getRandomElementFromList(this.anyDropPool);
            drops.add(DropItemRegistry.parseItem(item, "alphadrops.json {pool=any}"));
        }
        ArrayList<ItemStack> defaultDrops = DropItemRegistry.getDropsForPokemon(pixelmon);
        for (int i = 0; i < 2; ++i) {
            drops.add(defaultDrops.get(RandomHelper.getRandomNumberBetween(0, defaultDrops.size() - 1)));
        }
        return drops;
    }
}

