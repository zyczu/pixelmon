/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.EnumType;

public class Rattled
extends AbilityBase {
    @Override
    public void tookDamageTarget(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (target.isAlive() && (a.getAttackBase().attackType.equals((Object)EnumType.Bug) || a.getAttackBase().attackType.equals((Object)EnumType.Dark) || a.getAttackBase().attackType.equals((Object)EnumType.Ghost))) {
            this.sendActivatedMessage(target);
            target.getBattleStats().modifyStat(1, StatsType.Speed);
        }
    }
}

