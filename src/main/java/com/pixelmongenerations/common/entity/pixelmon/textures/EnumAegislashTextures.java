/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.textures;

import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;

public enum EnumAegislashTextures implements IEnumSpecialTexture
{
    Zelda(1, "Zelda"),
    FourSword(1, "Four Sword"),
    GanonBlade(1, "Ganon Blade"),
    CaptainAmerica(2, "Captain America"),
    Valentines(3, "Valentines");

    private int id;
    private String name;

    private EnumAegislashTextures(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean hasTexutre() {
        return true;
    }

    @Override
    public String getTexture() {
        return "-" + this.name().toLowerCase();
    }

    @Override
    public String getProperName() {
        return this.name;
    }

    @Override
    public int getId() {
        return this.id;
    }
}

