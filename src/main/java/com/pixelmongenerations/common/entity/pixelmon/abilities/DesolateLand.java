/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.HarshSunlight;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AirLock;
import com.pixelmongenerations.common.entity.pixelmon.abilities.CloudNine;
import com.pixelmongenerations.common.entity.pixelmon.abilities.WeatherTrio;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.EnumType;
import net.minecraft.util.text.TextComponentTranslation;

public class DesolateLand
extends WeatherTrio {
    public DesolateLand() {
        super(new HarshSunlight());
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.getHeldItem() != PixelmonItemsHeld.redOrb) {
            newPokemon.setForm(0);
        } else if (!(newPokemon.bc.globalStatusController.getWeather() instanceof HarshSunlight) && newPokemon.getHeldItem() == PixelmonItemsHeld.redOrb) {
            HarshSunlight harshsunlight = new HarshSunlight();
            harshsunlight.setStartTurns(newPokemon);
            newPokemon.bc.globalStatusController.addGlobalStatus(harshsunlight);
        }
    }

    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        Weather weather = user.bc.globalStatusController.getWeather();
        if (weather != null && weather instanceof HarshSunlight && a.getAttackBase().attackType == EnumType.Water && Attack.dealsDamage(a)) {
            for (BattleParticipant participant : pokemon.bc.participants) {
                for (PixelmonWrapper pixelmonWrapper : participant.controlledPokemon) {
                    if (!(pixelmonWrapper.getAbility() instanceof AirLock) && !(pixelmonWrapper.getAbility() instanceof CloudNine)) continue;
                    return true;
                }
            }
            pokemon.bc.sendToAll(new TextComponentTranslation("The Water-type attack evaporated in the harsh sunlight!", new Object[0]));
            return false;
        }
        return true;
    }
}

