/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class Hustle
extends AbilityBase {
    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        int n = StatsType.Attack.getStatIndex();
        stats[n] = (int)((double)stats[n] * 1.5);
        return stats;
    }

    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (accuracy > 0) {
            if (a.getAttackCategory() == AttackCategory.Physical) {
                accuracy -= 20;
            }
            if (accuracy < 0) {
                accuracy = 0;
            }
        }
        return new int[]{power, accuracy};
    }
}

