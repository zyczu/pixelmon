/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.client.models.PixelmonModelSmd;
import com.pixelmongenerations.client.models.smd.AnimationType;
import com.pixelmongenerations.client.models.smd.ValveStudioModel;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.EntityStatue;
import com.pixelmongenerations.common.entity.pixelmon.helpers.animation.AnimationVariables;
import com.pixelmongenerations.common.entity.pixelmon.helpers.animation.IncrementingVariable;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.EnumUpdateType;
import java.util.HashMap;
import net.minecraft.client.model.ModelBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class Entity2HasModel
extends Entity1Base {
    static int animationFlyingDelayLimit = 10;
    static int animationDelayLimit = 3;
    static int flyingDelayLimit = 10;
    public boolean animationFlyingCounting = false;
    public boolean animationFlyingSwap = false;
    public boolean animationCounting = false;
    public boolean animationSwap = false;
    public boolean transformed;
    public EnumSpecies transformedModel;
    public int transformedForm;
    int animationFlyingDelayCounter = 0;
    int animationDelayCounter = 0;
    int flyingDelayCounter = 0;
    private AnimationVariables animationVariables;
    private AnimationType animationType;

    public Entity2HasModel(World par1World) {
        super(par1World);
        this.dataManager.register(EntityPixelmon.dwTransformation, 0);
    }

    @Override
    protected void init(String name) {
        super.init(name);
    }

    public void evolve(PokemonSpec evolveTo) {
        if (!EnumSpecies.hasPokemon(evolveTo.name)) {
            return;
        }
        if (this.getNickname().equals(this.getLocalizedName())) {
            this.setNickname(Entity2HasModel.getLocalizedName(evolveTo.name));
        }
        this.setName(evolveTo.name);
        this.update(EnumUpdateType.Name, EnumUpdateType.Nickname);
    }

    @SideOnly(value=Side.CLIENT)
    public ModelBase getModel() {
        if (this.transformed) {
            return PixelmonModelRegistry.getModel(this.transformedModel, this.transformedModel.getFormEnum(this.transformedForm));
        }
        Entity3HasStats entityStats = (Entity3HasStats)this;
        if (PixelmonModelRegistry.hasFlyingModel(this.getSpecies(), entityStats.getFormEnum()) && this.flyingDelayCounter >= flyingDelayLimit) {
            return PixelmonModelRegistry.getFlyingModel(this.getSpecies(), entityStats.getFormEnum());
        }
        return PixelmonModelRegistry.getModel(this.getSpecies(), entityStats.getFormEnum());
    }

    @SideOnly(value=Side.CLIENT)
    public void transform(EnumSpecies transformedModel, int transformedForm) {
        this.transformed = true;
        this.transformedModel = transformedModel;
        this.transformedForm = transformedForm;
    }

    public void transformServer(EnumSpecies transformedModel, int transformedForm) {
        this.transformed = true;
        this.transformedModel = transformedModel;
        this.transformedForm = transformedForm;
    }

    public void cancelTransform() {
        this.transformed = false;
        this.transformedModel = null;
        this.transformedForm = -1;
    }

    public int getTransformed() {
        return this.dataManager.get(EntityPixelmon.dwTransformation);
    }

    public float getScaleFactor() {
        return (float)Math.pow(this.getGrowth().scaleValue, PixelmonConfig.getGrowthModifier()) * this.getBossMode().scaleFactor;
    }

    public AnimationVariables getAnimationVariables() {
        if (this.animationVariables == null) {
            this.animationVariables = new AnimationVariables();
        }
        return this.animationVariables;
    }

    public void initAnimation() {
        ModelBase base;
        if (this.world.isRemote && (base = this.getModel()) != null && base instanceof PixelmonModelSmd) {
            ValveStudioModel model = ((PixelmonModelSmd)base).theModel;
            model.animate();
        }
    }

    protected void checkAnimation() {
        IncrementingVariable inc;
        if (!(this.getModel() instanceof PixelmonModelSmd)) {
            return;
        }
        PixelmonModelSmd smdModel = (PixelmonModelSmd)this.getModel();
        Entity3HasStats pixelmon = (Entity3HasStats)this;
        float f1 = pixelmon.prevLimbSwingAmount + (pixelmon.limbSwingAmount - pixelmon.prevLimbSwingAmount);
        if (f1 > 1.0f) {
            f1 = 1.0f;
        }
        if ((inc = this.getAnimationVariables().getCounter(-1)) == null) {
            this.getAnimationVariables().setCounter(-1, 2.14748365E9f, smdModel.animationIncrement);
        } else {
            inc.increment = smdModel.animationIncrement;
        }
        if (!pixelmon.animationCounting) {
            this.setAnimation(AnimationType.IDLE);
            pixelmon.animationCounting = true;
        }
        if (pixelmon.isInWater()) {
            if (f1 > smdModel.movementThreshold) {
                if (pixelmon.animationSwap) {
                    this.setAnimation(AnimationType.SWIM);
                }
            } else if (pixelmon.animationSwap) {
                this.setAnimation(AnimationType.IDLE_SWIM);
            }
        } else if (!pixelmon.onGround || pixelmon.baseStats.doesHover && f1 > smdModel.movementThreshold) {
            if (!pixelmon.animationFlyingSwap) {
                pixelmon.animationFlyingCounting = true;
            }
            if (pixelmon.animationFlyingSwap) {
                this.setAnimation(AnimationType.FLY);
            }
        } else if (f1 > smdModel.movementThreshold) {
            if (pixelmon.animationSwap) {
                this.setAnimation(AnimationType.WALK);
            }
        } else if (pixelmon.animationSwap) {
            this.setAnimation(AnimationType.IDLE);
        }
        if (smdModel.theModel.currentAnimation == null) {
            this.setAnimation(AnimationType.IDLE);
        }
    }

    public void setAnimation(AnimationType animation) {
        this.animationType = animation;
    }

    public AnimationType getCurrentAnimation() {
        if (this.animationType == null) {
            return AnimationType.IDLE;
        }
        return this.animationType;
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        if (this instanceof EntityStatue && !((EntityStatue)this).isAnimated()) {
            return;
        }
        if (!this.onGround && !this.inWater) {
            if (this.flyingDelayCounter < flyingDelayLimit) {
                ++this.flyingDelayCounter;
            }
        } else {
            this.flyingDelayCounter = 0;
        }
        if (this.world.isRemote) {
            if (this.animationVariables != null) {
                this.animationVariables.tick();
            }
            if (this.animationFlyingCounting) {
                if (this.animationFlyingDelayCounter < animationFlyingDelayLimit) {
                    ++this.animationFlyingDelayCounter;
                    this.animationFlyingSwap = false;
                }
                if (this.animationFlyingDelayCounter >= animationFlyingDelayLimit) {
                    this.animationFlyingSwap = true;
                    this.animationFlyingDelayCounter = 0;
                }
            } else {
                this.animationFlyingDelayCounter = 0;
                this.animationFlyingSwap = false;
            }
            if (this.animationCounting) {
                if (this.animationDelayCounter < animationDelayLimit) {
                    ++this.animationDelayCounter;
                    this.animationSwap = false;
                }
                if (this.animationDelayCounter >= animationDelayLimit) {
                    this.animationSwap = true;
                    this.animationDelayCounter = 0;
                }
            } else {
                this.animationDelayCounter = 0;
                this.animationSwap = false;
            }
            if (this instanceof EntityPixelmon) {
                try {
                    this.checkAnimation();
                }
                catch (NullPointerException nullPointerException) {
                    // empty catch block
                }
            }
        }
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        nbt.setShort("transform", (short)this.getTransformed());
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        this.dataManager.set(EntityPixelmon.dwTransformation, Integer.valueOf(nbt.getShort("transform")));
    }

    @Override
    public void getNBTTags(HashMap<String, Class> tags) {
        super.getNBTTags(tags);
    }
}

