/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumCherrim;

public class FlowerGift
extends AbilityBase {
    @Override
    public int[] modifyStatsTeammate(PixelmonWrapper pokemon, int[] stats) {
        if (pokemon.bc.globalStatusController.hasStatus(StatusType.Sunny)) {
            int n = StatsType.Attack.getStatIndex();
            stats[n] = (int)((double)stats[n] * 1.5);
        }
        return stats;
    }

    @Override
    public int[] modifyStatsCancellableTeammate(PixelmonWrapper pokemon, int[] stats) {
        if (pokemon.bc.globalStatusController.hasStatus(StatusType.Sunny)) {
            int n = StatsType.SpecialDefence.getStatIndex();
            stats[n] = (int)((double)stats[n] * 1.5);
        }
        return stats;
    }

    @Override
    public void onWeatherChange(PixelmonWrapper pw, Weather weather) {
        if (pw.getSpecies() != EnumSpecies.Cherrim || pw.bc.simulateMode) {
            return;
        }
        StatusType weatherType = weather == null ? StatusType.None : weather.type;
        StatusType statusType = weatherType;
        if (weatherType == StatusType.Sunny) {
            if (pw.getForm() == EnumCherrim.OVERCAST.getForm() && pw.getHeldItem() != PixelmonItemsHeld.utilityUmbrella) {
                pw.setForm(EnumCherrim.SUNSHINE.getForm());
                pw.bc.sendToAll("pixelmon.abilities.changeform", pw.getNickname());
            }
        } else if (pw.getForm() == EnumCherrim.SUNSHINE.getForm()) {
            pw.setForm(EnumCherrim.OVERCAST.getForm());
            pw.bc.sendToAll("pixelmon.abilities.changeform", pw.getNickname());
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        this.resetForm(newPokemon);
        this.onWeatherChange(newPokemon, newPokemon.bc.globalStatusController.getWeather());
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper pw) {
        this.resetForm(pw);
    }

    @Override
    public void onAbilityLost(PixelmonWrapper pokemon) {
        this.onWeatherChange(pokemon, null);
    }

    private void resetForm(PixelmonWrapper pw) {
        if (pw.bc.simulateMode) {
            return;
        }
        if (pw.getForm() != EnumCherrim.OVERCAST.getForm()) {
            pw.setForm(EnumCherrim.OVERCAST.getForm());
        }
    }
}

