/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class WonderSkin
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyTarget(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (a.getAttackCategory() == AttackCategory.Status && accuracy > 50 && !AbilityBase.ignoreAbility(user, target)) {
            accuracy = 50;
        }
        return new int[]{power, accuracy};
    }
}

