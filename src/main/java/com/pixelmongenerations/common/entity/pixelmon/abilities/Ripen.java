/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.item.ItemHeld;

public class Ripen
extends AbilityBase {
    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        ItemHeld consumedItem = pokemon.getConsumedItem();
        if (consumedItem.isBerry() && !pokemon.hasHeldItem()) {
            ItemHeld tempItem = pokemon.getHeldItem();
            ItemHeld tempConsumedItem = pokemon.getConsumedItem();
            pokemon.getHeldItem().eatBerry(pokemon);
            pokemon.setHeldItem(tempItem);
            pokemon.setConsumedItem(tempConsumedItem);
            pokemon.bc.sendToAll("pixelmon.abilities.ripen", pokemon.getNickname(), consumedItem.getLocalizedName());
        }
    }
}

