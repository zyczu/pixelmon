/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.item.ItemHeld;

public class Pickpocket
extends AbilityBase {
    @Override
    public void applyEffectOnContactTargetLate(PixelmonWrapper user, PixelmonWrapper target) {
        if (!user.inMultipleHit && !target.hasHeldItem() && user.hasHeldItem() && user.isItemRemovable(target) && !target.isFainted()) {
            ItemHeld userItem = user.getHeldItem();
            user.bc.sendToAll("pixelmon.abilities.pickpocket", target.getNickname(), user.getNickname(), userItem.getLocalizedName());
            target.setNewHeldItem(userItem);
            user.removeHeldItem();
            user.bc.enableReturnHeldItems(target, user);
        }
    }
}

