/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.common.block.spawning.BlockSpawningHandler;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.enums.battle.EnumBattleStartTypes;
import com.pixelmongenerations.core.network.PixelmonData;
import net.minecraft.util.math.RayTraceResult;

public class SweetScent
extends ExternalMoveBase {
    public SweetScent() {
        super("sweetscent", "Sweet Scent");
    }

    @Override
    public boolean execute(EntityPixelmon user, RayTraceResult target, int moveIndex) {
        BlockSpawningHandler.getInstance().performBattleStartCheck(user.world, target.getBlockPos(), user.getOwner(), user, "", EnumBattleStartTypes.SWEETSCENT);
        return true;
    }

    @Override
    public int getCooldown(EntityPixelmon user) {
        return 800 - user.stats.Speed;
    }

    @Override
    public int getCooldown(PixelmonData user) {
        return 800 - user.Speed;
    }

    @Override
    public boolean isDestructive() {
        return false;
    }
}

