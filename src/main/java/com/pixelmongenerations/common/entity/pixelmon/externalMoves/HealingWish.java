/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.RayTraceResult;

public class HealingWish
extends ExternalMoveBase {
    public HealingWish() {
        super("heal", "Healing Wish");
    }

    @Override
    public boolean execute(EntityPixelmon user, RayTraceResult target, int moveIndex) {
        PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)user.getOwner()).ifPresent(storage -> {
            for (int i = 0; i < 6; ++i) {
                Optional.ofNullable(storage.sendOutFromPosition(i, user.world)).ifPresent(p -> p.setHealth(p.getMaxHealth()));
            }
        });
        return true;
    }

    @Override
    public int getCooldown(EntityPixelmon user) {
        return 500;
    }

    @Override
    public int getCooldown(PixelmonData pixelmonData) {
        return 500;
    }

    @Override
    public boolean isDestructive() {
        return false;
    }

    @Override
    public boolean targetsBlocks() {
        return false;
    }
}

