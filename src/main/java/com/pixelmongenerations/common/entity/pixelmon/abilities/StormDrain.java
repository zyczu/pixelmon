/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.Redirect;
import com.pixelmongenerations.core.enums.EnumType;

public class StormDrain
extends Redirect {
    public StormDrain() {
        super(EnumType.Water, "pixelmon.abilities.stormdrain", "pixelmon.abilities.stormdrainredirect");
    }
}

