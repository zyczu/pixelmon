/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.entity.pixelmon.abilities.LowHPTypeBoost;
import com.pixelmongenerations.core.enums.EnumType;

public class Blaze
extends LowHPTypeBoost {
    public Blaze() {
        super(EnumType.Fire);
    }
}

