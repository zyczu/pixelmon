/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

import com.google.common.collect.Lists;
import com.pixelmongenerations.common.entity.pixelmon.stats.EVsStore;
import com.pixelmongenerations.common.entity.pixelmon.stats.IVStore;
import java.util.ArrayList;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Stream;
import net.minecraft.util.text.translation.I18n;

public enum StatsType {
    None(-1),
    HP(-1, 0, true, store -> store.HP, (store, val) -> {
        store.HP = val;
    }, store -> store.HP, (store, val) -> {
        store.HP = val;
    }),
    Attack(2, 1, true, store -> store.Attack, (store, val) -> {
        store.Attack = val;
    }, store -> store.Attack, (store, val) -> {
        store.Attack = val;
    }),
    Defence(3, 2, true, store -> store.Defence, (store, val) -> {
        store.Defence = val;
    }, store -> store.Defence, (store, val) -> {
        store.Defence = val;
    }),
    SpecialAttack(4, 3, true, store -> store.SpecialAttack, (store, val) -> {
        store.SpecialAttack = val;
    }, store -> store.SpAtt, (store, val) -> {
        store.SpAtt = val;
    }),
    SpecialDefence(5, 4, true, store -> store.SpecialDefence, (store, val) -> {
        store.SpecialDefence = val;
    }, store -> store.SpDef, (store, val) -> {
        store.SpDef = val;
    }),
    Speed(6, 5, true, store -> store.Speed, (store, val) -> {
        store.Speed = val;
    }, store -> store.Speed, (store, val) -> {
        store.Speed = val;
    }),
    Accuracy(0),
    Evasion(1);

    private final Function<EVsStore, Integer> evGetter;
    private final BiConsumer<EVsStore, Integer> evSetter;
    private final Function<IVStore, Integer> ivGetter;
    private final BiConsumer<IVStore, Integer> ivSetter;
    private final int index;
    private boolean isPermanent;
    private int permaIndex = -1;

    private StatsType(int index) {
        this(index, -1, false, ev -> -1, (ev, value) -> {}, ivStore -> -1, (ivStore, integer) -> {});
    }

    private StatsType(int index, int permaIndex, boolean isPermanent, Function<EVsStore, Integer> evGetter, BiConsumer<EVsStore, Integer> evSetter, Function<IVStore, Integer> ivGetter, BiConsumer<IVStore, Integer> ivSetter) {
        this.index = index;
        this.evGetter = evGetter;
        this.evSetter = evSetter;
        this.ivGetter = ivGetter;
        this.ivSetter = ivSetter;
        this.isPermanent = isPermanent;
        this.permaIndex = permaIndex;
    }

    public static ArrayList<StatsType> getMainTypes() {
        return Lists.newArrayList(HP, Attack, Defence, SpecialAttack, SpecialDefence, Speed);
    }

    public static boolean isStatsEffect(String effectTypeString) {
        for (StatsType t : StatsType.values()) {
            if (!effectTypeString.equalsIgnoreCase(t.toString())) continue;
            return true;
        }
        return false;
    }

    public static StatsType getStatsEffect(String effectTypeString) {
        for (StatsType t : StatsType.values()) {
            if (!effectTypeString.equalsIgnoreCase(t.toString())) continue;
            return t;
        }
        return null;
    }

    public static StatsType fromPermaIndex(int index) {
        return Stream.of(StatsType.values()).filter(a -> a.permaIndex == index).findFirst().orElse(None);
    }

    public int getStatIndex() {
        return this.index;
    }

    public boolean isPermanent() {
        return this.isPermanent;
    }

    public String getLocalizedName() {
        if (this == None) {
            return "";
        }
        return I18n.translateToLocal("enum.stat." + this.toString().toLowerCase());
    }

    public int getEV(EVsStore store) {
        return this.evGetter.apply(store);
    }

    public void setEV(EVsStore store, int value) {
        this.evSetter.accept(store, value);
    }

    public int getIV(IVStore store) {
        return this.ivGetter.apply(store);
    }

    public void setIV(IVStore store, int value) {
        this.ivSetter.accept(store, value);
    }

    public int permaIndex() {
        return this.permaIndex;
    }
}

