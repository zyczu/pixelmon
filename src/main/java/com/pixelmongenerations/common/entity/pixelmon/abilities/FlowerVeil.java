/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class FlowerVeil
extends AbilityBase {
    @Override
    public boolean allowsStatusTeammate(StatusType status, PixelmonWrapper pokemon, PixelmonWrapper target, PixelmonWrapper user) {
        if (user != target && status.isPrimaryStatus()) {
            if (target.hasType(EnumType.Grass)) {
                if (user != target && user.attack != null && user.attack.getAttackCategory() == AttackCategory.Status) {
                    user.bc.sendToAll("pixelmon.abilities.flowerveil", pokemon.getNickname(), target.getNickname());
                }
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean allowsStatChangeTeammate(PixelmonWrapper pokemon, PixelmonWrapper target, PixelmonWrapper user, StatsEffect e) {
        if (e.value < 0 && !e.getUser()) {
            if (target.hasType(EnumType.Grass)) {
                if (!Attack.dealsDamage(user.attack)) {
                    user.bc.sendToAll("pixelmon.abilities.flowerveil", pokemon.getNickname(), target.getNickname());
                }
                return false;
            }
        }
        return true;
    }
}

