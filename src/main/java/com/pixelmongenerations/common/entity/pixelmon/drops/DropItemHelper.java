/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.drops;

import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.common.entity.pixelmon.Entity8HoldsItems;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemQueryList;
import com.pixelmongenerations.common.entity.pixelmon.drops.DroppedItem;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.network.ChatHandler;
import java.util.ArrayList;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

public class DropItemHelper {
    Entity8HoldsItems pixelmon;

    public DropItemHelper(Entity8HoldsItems entity8HoldsItems) {
        this.pixelmon = entity8HoldsItems;
    }

    public Item getDropItem() {
        return null;
    }

    public static boolean giveItemStackToPlayer(EntityPlayer player, ItemStack itemstack) {
        return DropItemHelper.giveItemStackToPlayer(player, (Integer)1, itemstack);
    }

    public static boolean giveItemStackToPlayer(EntityPlayer player, Integer count, ItemStack itemstack) {
        if (player.world.isRemote) {
            boolean boolAddedToInventory = true;
            for (int i = 0; i < count; ++i) {
                boolAddedToInventory = player.inventory.addItemStackToInventory(itemstack);
                if (boolAddedToInventory || itemstack.getItemDamage() != 0) continue;
                player.dropItem(itemstack.getItem(), 1);
                ChatHandler.sendFormattedChat(player, TextFormatting.RED, "pixelmon.drops.fullinventory", itemstack.getItem().getItemStackDisplayName(itemstack));
            }
            return boolAddedToInventory;
        }
        return DropItemHelper.giveItemStackToPlayer((EntityPlayerMP)player, count, itemstack);
    }

    public static boolean giveItemStackToPlayer(EntityPlayerMP player, ItemStack itemstack) {
        return DropItemHelper.giveItemStackToPlayer(player, (Integer)itemstack.getCount(), itemstack);
    }

    public static boolean giveItemStackToPlayer(EntityPlayerMP player, Integer count, ItemStack itemstack) {
        return DropItemHelper.giveItemStackToPlayer(player, count, itemstack, false);
    }

    public static boolean giveItemStackToPlayer(EntityPlayerMP player, ItemStack itemstack, boolean itemsOwnedByPlayer) {
        return DropItemHelper.giveItemStackToPlayer(player, 1, itemstack, itemsOwnedByPlayer);
    }

    public static boolean giveItemStackToPlayer(EntityPlayerMP player, Integer count, ItemStack itemstack, boolean itemsOwnedByPlayer) {
        boolean boolAddedToInventory = true;
        for (int i = 0; i < count; ++i) {
            itemstack.setCount(1);
            boolAddedToInventory = player.inventory.addItemStackToInventory(itemstack);
            if (!boolAddedToInventory && itemstack.getItemDamage() == 0) {
                DropItemHelper.dropItemOnGround(player, itemstack, itemsOwnedByPlayer, true);
                continue;
            }
            player.sendContainerToPlayer(player.inventoryContainer);
        }
        return boolAddedToInventory;
    }

    public static void dropItemOnGround(EntityPlayerMP player, ItemStack itemstack, boolean itemsOwnedByPlayer, boolean announce) {
        EntityItem entityitem = DropItemHelper.createItemEntity(player.getPositionVector(), itemstack, player.world);
        if (itemsOwnedByPlayer && entityitem != null) {
            entityitem.setOwner(player.getName());
        }
        if (announce) {
            ChatHandler.sendFormattedChat(player, TextFormatting.RED, "pixelmon.drops.fullinventory", itemstack.getItem().getItemStackDisplayName(itemstack));
        }
    }

    public static void dropItemOnGround(Vec3d position, EntityPlayerMP player, ItemStack itemstack, boolean itemsOwnedByPlayer, boolean announce) {
        EntityItem entityitem = DropItemHelper.createItemEntity(position, itemstack, player.world);
        if (itemsOwnedByPlayer && entityitem != null) {
            entityitem.setOwner(player.getName());
        }
        if (announce) {
            ChatHandler.sendFormattedChat(player, TextFormatting.RED, "pixelmon.drops.fullinventory", itemstack.getItem().getItemStackDisplayName(itemstack));
        }
    }

    public static EntityItem createItemEntity(Vec3d position, ItemStack stack, World world) {
        if (stack.getCount() != 0 && stack.getItem() != null) {
            EntityItem entityitem = new EntityItem(world, position.x, position.y + 0.5, position.z, stack);
            entityitem.setDefaultPickupDelay();
            world.spawnEntity(entityitem);
            return entityitem;
        }
        return null;
    }

    public void dropNormalItems(EntityPlayerMP player) {
        if (!PixelmonConfig.pokemonDropsEnabled || this.pixelmon.hasOwner() || this.pixelmon.getTrainer() != null) {
            return;
        }
        if (this.pixelmon.isBossPokemon()) {
            return;
        }
        ArrayList<ItemStack> items = DropItemRegistry.getDropsForPokemon(this.pixelmon);
        int id = 0;
        ArrayList<DroppedItem> givenDrops = new ArrayList<DroppedItem>();
        for (ItemStack item : items) {
            if (item == null) continue;
            givenDrops.add(new DroppedItem(item, id++));
        }
        if (givenDrops.size() > 0) {
            DropItemQueryList.register(this.pixelmon, givenDrops, player);
        }
    }

    public void dropBossItems(EntityPlayerMP player) {
        ArrayList<ItemStack> items = DropItemRegistry.getDropsForPokemon(this.pixelmon);
        int id = 0;
        ArrayList<DroppedItem> givenDrops = new ArrayList<DroppedItem>();
        for (ItemStack item : items) {
            if (item == null) continue;
            givenDrops.add(new DroppedItem(item, id++));
        }
        ArrayList<ItemStack> bossDrops = DropItemRegistry.getBossDrops(this.pixelmon, player);
        for (ItemStack item : bossDrops) {
            givenDrops.add(new DroppedItem(item, id++));
        }
        if (givenDrops.size() > 0) {
            DropItemQueryList.register(this.pixelmon, givenDrops, player);
        }
    }

    public void dropTotemItems(EntityPlayerMP player) {
        ArrayList<ItemStack> items = DropItemRegistry.getTotemDrops(this.pixelmon);
        int id = 0;
        ArrayList<DroppedItem> givenDrops = new ArrayList<DroppedItem>();
        for (ItemStack item : items) {
            if (item == null) continue;
            givenDrops.add(new DroppedItem(item, id++));
        }
        if (givenDrops.size() > 0) {
            DropItemQueryList.register(this.pixelmon, givenDrops, player);
        }
    }

    public void dropAlphaItems(EntityPlayerMP player) {
        ArrayList<ItemStack> items = DropItemRegistry.getAlphaDrops(this.pixelmon);
        int id = 0;
        ArrayList<DroppedItem> givenDrops = new ArrayList<DroppedItem>();
        for (ItemStack item : items) {
            if (item == null) continue;
            givenDrops.add(new DroppedItem(item, id++));
        }
        if (givenDrops.size() > 0) {
            DropItemQueryList.register(this.pixelmon, givenDrops, player);
        }
    }
}

