/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.links;

import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusPersist;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.ExtraStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.FriendShip;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.Level;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.Stats;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumMark;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.PixelmonStatsData;
import com.pixelmongenerations.core.network.packetHandlers.LevelUp;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.List;
import java.util.Optional;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class EntityLink
extends PokemonLink {
    private EntityPixelmon pixelmon;

    public EntityLink(EntityPixelmon pixelmon) {
        this.pixelmon = pixelmon;
    }

    public EntityLink(Entity1Base pixelmon) {
        try {
            this.pixelmon = (EntityPixelmon)pixelmon;
        }
        catch (ClassCastException e) {
            throw new IllegalArgumentException("Cannot initialize link to a non-Pixelmon entity!");
        }
    }

    @Override
    public BaseStats getBaseStats() {
        return this.pixelmon.baseStats;
    }

    @Override
    public Stats getStats() {
        return this.pixelmon.stats;
    }

    @Override
    public ItemHeld getHeldItem() {
        return this.pixelmon.getItemHeld();
    }

    @Override
    public void setHeldItem(ItemStack item) {
        this.pixelmon.setHeldItem(item);
    }

    @Override
    public int getHealth() {
        return (int)this.pixelmon.getHealth();
    }

    @Override
    public int getMaxHealth() {
        return (int)this.pixelmon.getMaxHealth();
    }

    @Override
    public void setHealth(int health) {
        this.pixelmon.setHealth(health);
    }

    @Override
    public int getLevel() {
        return this.pixelmon.getDataManager().get(EntityPixelmon.dwLevel);
    }

    @Override
    public void setLevel(int level) {
        this.pixelmon.getDataManager().set(EntityPixelmon.dwLevel, level);
    }

    @Override
    public int getDynamaxLevel() {
        return this.pixelmon.getDataManager().get(EntityPixelmon.dwDynamaxLevel);
    }

    @Override
    public void setDynamaxLevel(int level) {
        this.pixelmon.getDataManager().set(EntityPixelmon.dwDynamaxLevel, level);
    }

    @Override
    public int getExp() {
        return this.pixelmon.getDataManager().get(EntityPixelmon.dwExp);
    }

    @Override
    public void setExp(int experience) {
        this.pixelmon.getDataManager().set(EntityPixelmon.dwExp, experience);
    }

    @Override
    public FriendShip getFriendship() {
        return this.pixelmon.friendship;
    }

    @Override
    public boolean doesLevel() {
        return this.pixelmon.doesLevel;
    }

    @Override
    public EntityPlayerMP getPlayerOwner() {
        EntityLivingBase owner = this.pixelmon.getOwner();
        if (owner instanceof EntityPlayerMP) {
            return (EntityPlayerMP)owner;
        }
        return null;
    }

    @Override
    public String getRealNickname() {
        return this.pixelmon.getNickname();
    }

    @Override
    public BattleControllerBase getBattleController() {
        return this.pixelmon.battleController;
    }

    @Override
    public Moveset getMoveset() {
        Moveset moveset = this.pixelmon.getMoveset();
        if (moveset.isEmpty()) {
            this.pixelmon.loadMoveset();
            moveset = this.pixelmon.getMoveset();
        }
        return moveset;
    }

    @Override
    public int[] getPokemonID() {
        return this.pixelmon.getPokemonId();
    }

    @Override
    public EntityPixelmon getEntity() {
        return this.pixelmon;
    }

    @Override
    public void setScale(float scale) {
        if (scale > this.pixelmon.maxScale) {
            scale = this.pixelmon.maxScale;
        }
        this.pixelmon.setPixelmonScale(scale);
    }

    @Override
    public World getWorld() {
        return this.pixelmon.world;
    }

    @Override
    public Gender getGender() {
        return this.pixelmon.getGender();
    }

    @Override
    public BlockPos getPos() {
        return this.pixelmon.getPosition();
    }

    @Override
    public Optional<PlayerStorage> getStorage() {
        return this.pixelmon.getStorage();
    }

    @Override
    public void update(EnumUpdateType ... updateTypes) {
        if (this.pixelmon.getOwner() != null) {
            this.pixelmon.update(updateTypes);
        }
    }

    @Override
    public void updateStats() {
        this.pixelmon.updateStats();
    }

    @Override
    public void updateLevelUp(PixelmonStatsData stats) {
        EntityLivingBase owner = this.pixelmon.getOwner();
        if (owner != null && owner instanceof EntityPlayerMP) {
            PixelmonStatsData stats2 = PixelmonStatsData.createPacket(this);
            Pixelmon.NETWORK.sendTo(new LevelUp(this.pixelmon, this.getLevel(), stats, stats2), (EntityPlayerMP)owner);
            this.pixelmon.update(EnumUpdateType.Stats);
        }
    }

    @Override
    public void sendMessage(String langKey, Object ... data) {
        EntityLivingBase owner = this.pixelmon.getOwner();
        if (owner instanceof EntityPlayer) {
            ChatHandler.sendChat((EntityPlayer)owner, langKey, data);
        }
    }

    @Override
    public String getOriginalTrainer() {
        return this.pixelmon.originalTrainer;
    }

    @Override
    public String getOriginalTrainerUUID() {
        return this.pixelmon.originalTrainerUUID;
    }

    @Override
    public String getRealTextureNoCheck(PixelmonWrapper pw) {
        return this.pixelmon.getRealTextureNoCheck();
    }

    @Override
    public String getNickname() {
        return this.pixelmon.getNickname();
    }

    @Override
    public boolean removeStatuses(StatusType ... statuses) {
        return this.pixelmon.removeStatuses(statuses);
    }

    @Override
    public EnumNature getNature() {
        return this.pixelmon.getNature();
    }

    @Override
    public EnumNature getPseudoNature() {
        return this.pixelmon.getPseudoNature();
    }

    @Override
    public int getExpToNextLevel() {
        return this.pixelmon.getLvl().expToNextLevel;
    }

    @Override
    public StatusPersist getPrimaryStatus() {
        return this.pixelmon.status;
    }

    @Override
    public AbilityBase getAbility() {
        return this.pixelmon.getAbility();
    }

    @Override
    public List<EnumType> getType() {
        return this.pixelmon.type;
    }

    @Override
    public int getForm() {
        return this.pixelmon.getForm();
    }

    @Override
    public boolean isShiny() {
        return this.pixelmon.isShiny();
    }

    @Override
    public boolean isEgg() {
        return this.pixelmon.isEgg;
    }

    @Override
    public int getEggCycles() {
        return this.pixelmon.eggCycles;
    }

    @Override
    public EnumGrowth getGrowth() {
        return this.pixelmon.getGrowth();
    }

    @Override
    public EnumPokeball getCaughtBall() {
        return this.pixelmon.caughtBall;
    }

    @Override
    public int getPartyPosition() {
        return this.pixelmon.getPartyPosition();
    }

    @Override
    public ItemStack getHeldItemStack() {
        return this.pixelmon.getHeldItemMainhand();
    }

    @Override
    public boolean hasOwner() {
        return this.pixelmon.hasOwner() || this.pixelmon.hasNPCTrainer;
    }

    @Override
    public EnumBossMode getBossMode() {
        return this.pixelmon.getBossMode();
    }

    @Override
    public int getSpecialTexture() {
        return this.pixelmon.getSpecialTextureIndex();
    }

    @Override
    public boolean isInRanch() {
        return this.pixelmon.isInRanchBlock;
    }

    @Override
    public int[] getEggMoves() {
        return this.pixelmon.eggMoves;
    }

    @Override
    public Level getLevelContainer() {
        return this.pixelmon.getLvl();
    }

    @Override
    public NBTTagCompound getNBT() {
        return this.pixelmon.getEntityData();
    }

    @Override
    public boolean isTotem() {
        return this.pixelmon.isTotem();
    }

    @Override
    public boolean isAlpha() {
        return this.pixelmon.isAlpha();
    }

    @Override
    public String getCustomTexture() {
        return this.pixelmon.getCustomTexture();
    }

    @Override
    public int getPokeRus() {
        return this.pixelmon.getPokeRus();
    }

    @Override
    public boolean hasGmaxFactor() {
        return this.pixelmon.hasGmaxFactor();
    }

    @Override
    public void setShiny(boolean shiny) {
        this.pixelmon.setShiny(shiny);
    }

    @Override
    public EnumMark getMark() {
        return this.pixelmon.getMark();
    }

    @Override
    public void setMark(EnumMark mark) {
        this.pixelmon.setMark(mark);
    }

    @Override
    public ExtraStats getExtraStats() {
        return this.pixelmon.extraStats;
    }

    @Override
    public IEnumForm getFormEnum() {
        return this.pixelmon.getFormEnum();
    }

    @Override
    public void setForm(int form) {
        this.pixelmon.setForm(form);
    }

    @Override
    public void setForm(IEnumForm form) {
        this.pixelmon.setForm(form.getForm());
    }

    @Override
    public void setNature(EnumNature nature) {
        this.pixelmon.setNature(nature);
    }

    @Override
    public void setPseudoNature(EnumNature nature) {
        this.pixelmon.setPseudoNature(nature);
    }

    @Override
    public void setGender(Gender gender) {
        this.pixelmon.setGender(gender);
    }

    @Override
    public void setAbility(String ability) {
        this.pixelmon.setAbility(ability);
        this.pixelmon.giveAbilitySlot();
    }

    @Override
    public void setCustomTexture(String customTexture) {
        this.pixelmon.setCustomSpecialTexture(customTexture);
    }

    @Override
    public void setGrowth(EnumGrowth growth) {
        this.pixelmon.setGrowth(growth);
    }

    @Override
    public void setCaughtBall(EnumPokeball ball) {
        this.pixelmon.caughtBall = ball;
    }

    @Override
    public void setBossMode(EnumBossMode mode) {
        this.pixelmon.setBoss(mode);
    }

    @Override
    public void setSpecialTexture(int special) {
        this.pixelmon.setSpecialTexture(special);
    }

    @Override
    public void setTotem(boolean totem) {
        this.pixelmon.setTotem(totem);
    }

    @Override
    public boolean isNoble() {
        return this.pixelmon.isNoble();
    }

    @Override
    public void setNoble(boolean noble) {
        this.pixelmon.setNoble(noble);
    }

    @Override
    public void setAlpha(boolean alpha) {
        this.pixelmon.setAlpha(alpha);
    }

    @Override
    public void setCatchable(boolean catchable) {
        this.pixelmon.setCatchable(catchable);
    }

    @Override
    public void setBreedable(boolean breedable) {
        this.pixelmon.setBreedable(breedable);
    }

    @Override
    public void setParticleId(String particleId) {
        this.pixelmon.setParticleId(particleId);
    }

    @Override
    public void setStats(Stats stats) {
        this.pixelmon.stats = stats;
    }
}

