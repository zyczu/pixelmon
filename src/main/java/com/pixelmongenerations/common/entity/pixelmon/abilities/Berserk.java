/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class Berserk
extends AbilityBase {
    @Override
    public void tookDamageTargetAfterMove(PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (target.isAlive() && target.getHealthPercent() <= 50.0f && target.getHealth() + a.moveResult.damage > target.getPercentMaxHealth(50.0f) && !this.isAffectedBySheerForce(a)) {
            this.sendActivatedMessage(user);
            target.getBattleStats().modifyStat(1, StatsType.SpecialAttack);
        }
    }

    public boolean isAffectedBySheerForce(Attack a) {
        return a.isAttack("Acid", "Acid Spray", "Anchor Shot", "Aurora Beam", "Blaze Kick", "Blue Flare", "Bolt Strike", "Bone Club", "Bounce", "Bug Buzz", "Chatter", "Clangorous Soulblaze", "Constrict", "Cross Poison", "Diamond Storm", "Discharge", "Dizzy Punch", "Electroweb", "Energy Ball", "Fiery Dance", "Fire Lash", "Freeze-Dry", "Freeze Shock", "Genesis Supernova", "Glaciate", "Heart Stamp", "Hurricane", "Hyper Fang", "Ice Burn", "Icicle Crash", "Inferno", "Leaf Tornado", "Lick", "Lunge", "Luster Purge", "Meteor Mash", "Mirror Shot", "Mist Ball", "Moonblast", "Muddy Water", "Mystical Fire", "Needle Arm", "Night Daze", "Nuzzle", "Octazooka", "Ominous Wind", "Powder Snow", "Psybeam", "Razor Shell", "Relic Song", "Rolling Kick", "Sacred Fire", "Searing Shot", "Seed Flare", "Shadow Bone", "Silver Wind", "Sludge", "Smog", "Spark", "Spirit Shackle", "Steam Eruption", "Steamroller", "Stoked Sparksurfer", "Thunder Shock", "Tri Attack", "Trop Kick", "Twineedle", "Volt Tackle", "Zing Zap", "Air Slash", "Ancient Power", "Astonish", "Bite", "Blizzard", "Body Slam", "Bubble", "Bulldoze", "Charge Beam", "Confusion", "Crunch", "Crush Claw", "Dark Pulse", "Dragon Rush", "Dragon Breath", "DynamicPunch", "Earth Power", "Ember", "Extrasensory", "Fake Out", "Fire Blast", "Fire Fang", "Fire Punch", "Flame Charge", "Flame Wheel", "Flamethrower", "Flare Blitz", "Flash Cannon", "Focus Blast", "Force Palm", "Gunk Shot", "Headbutt", "Heat Wave", "Ice Beam", "Ice Fang", "Ice Punch", "Icy Wind", "Iron Head", "Iron Tail", "Lava Plume", "Liquidation", "Low Sweep", "Metal Claw", "Mud Bomb", "Mud Shot", "Mud-Slap", "Play Rough", "Poison Fang", "Poison Jab", "Poison Sting", "Poison Tail", "Power-Up Punch", "Psychic", "Rock Climb", "Rock Slide", "Rock Smash", "Rock Tomb", "Scald", "Secret Power", "Shadow Ball", "Signal Beam", "Sky Attack", "Sludge Bomb", "Sludge Wave", "Snarl", "Steel Wing", "Stomp", "Struggle Bug", "Throat Chop", "Thunder", "Thunder Fang", "Thunderbolt", "Thunder Punch", "Twister", "Water Pulse", "Waterfall", "Zap Cannon", "Zen Headbutt");
    }
}

