/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Rainy;
import com.pixelmongenerations.common.battle.status.Weather;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.WeatherTrio;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class Drizzle
extends AbilityBase {
    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.getSpecies() == EnumSpecies.Kyogre && newPokemon.getHeldItem().getHeldItemType() == EnumHeldItems.blueOrb) {
            return;
        }
        if (newPokemon.bc.getActivePokemon().stream().map(PixelmonWrapper::getBattleAbility).filter(WeatherTrio.class::isInstance).map(WeatherTrio.class::cast).map(a -> a.weather).noneMatch(Weather::isWeatherTrioStatus) && !(newPokemon.bc.globalStatusController.getWeatherIgnoreAbility() instanceof Rainy)) {
            Rainy rainy = new Rainy();
            rainy.setStartTurns(newPokemon);
            newPokemon.bc.globalStatusController.addGlobalStatus(rainy);
            newPokemon.bc.sendToAll("pixelmon.abilities.drizzle", newPokemon.getNickname());
        }
    }
}

