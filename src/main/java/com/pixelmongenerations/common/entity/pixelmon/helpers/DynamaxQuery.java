/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.helpers;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQuery;
import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQueryList;
import com.pixelmongenerations.core.network.packetHandlers.evolution.EvolutionStage;
import java.util.List;
import net.minecraft.world.World;

public class DynamaxQuery
extends EvolutionQuery {
    public DynamaxQuery(EntityPixelmon pixelmon) {
        super(pixelmon, 0);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void tick(World world) {
        if (this.pixelmon.world != world) {
            return;
        }
        if (this.airSaver != null) {
            this.airSaver.tick();
        }
        ++this.ticks;
        if (this.stage == EvolutionStage.PreAnimation) {
            if (this.ticks >= this.stage.ticks) {
                this.ticks = 0;
                this.setStage(EvolutionStage.PostAnimation);
                this.updateAllAround(this.stage);
            }
        } else if (this.stage == EvolutionStage.PostAnimation && this.ticks >= this.stage.ticks) {
            List<EvolutionQuery> list;
            this.ticks = 0;
            this.setStage(EvolutionStage.End);
            this.updateAllAround(this.stage);
            this.pixelmon.setDynamaxSize();
            List<EvolutionQuery> list2 = list = EvolutionQueryList.queryList;
            synchronized (list2) {
                EvolutionQueryList.queryList.remove(this);
            }
        }
    }
}

