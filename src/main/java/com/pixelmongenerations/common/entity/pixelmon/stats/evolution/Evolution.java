/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution;

import com.pixelmongenerations.api.events.EvolveEvent;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.HeldItemCondition;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.EnumUpdateType;
import java.util.ArrayList;
import java.util.Arrays;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;

public abstract class Evolution {
    public EnumSpecies from;
    public PokemonSpec to;
    public ArrayList<EvoCondition> conditions = new ArrayList();
    private int form = -1;

    public Evolution(EnumSpecies from, PokemonSpec to, EvoCondition ... conditions) {
        this.from = from;
        this.to = to;
        this.conditions.addAll(Arrays.asList(conditions));
    }

    public void setForm(int form) {
        this.form = form;
    }

    public EnumSpecies getSpecies() {
        return this.from;
    }

    protected boolean canEvolve(EntityPixelmon pokemon) {
        for (EvoCondition evoCondition : this.conditions) {
            if (evoCondition.passes(pokemon)) continue;
            return false;
        }
        return true;
    }

    public boolean doEvolution(EntityPixelmon pokemon) {
        if (!MinecraftForge.EVENT_BUS.post(new EvolveEvent.PreEvolve((EntityPlayerMP)pokemon.getOwner(), pokemon, this))) {
            pokemon.startEvolution(this);
            return true;
        }
        return false;
    }

    public boolean doEvolution(ItemStack itemUsed, EntityPixelmon pokemon) {
        if (!MinecraftForge.EVENT_BUS.post(new EvolveEvent.PreEvolve((EntityPlayerMP)pokemon.getOwner(), pokemon, this))) {
            pokemon.startEvolution(this);
            return true;
        }
        return false;
    }

    public void finishedEvolving(EntityPixelmon pokemon) {
        if (this.consumesHeldItem()) {
            pokemon.setHeldItem(ItemStack.EMPTY);
            pokemon.update(EnumUpdateType.HeldItem);
        }
        if (this.form != -1) {
            pokemon.setForm(this.form, true);
        }
    }

    public boolean consumesHeldItem() {
        for (EvoCondition evoCondition : this.conditions) {
            if (!(evoCondition instanceof HeldItemCondition)) continue;
            return true;
        }
        return false;
    }
}

