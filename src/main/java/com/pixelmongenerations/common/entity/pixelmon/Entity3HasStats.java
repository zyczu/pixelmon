/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon;

import com.pixelmongenerations.api.events.PokedexEvent;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.common.entity.pixelmon.Entity10CanBreed;
import com.pixelmongenerations.common.entity.pixelmon.Entity2HasModel;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.EntityStatue;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.ComingSoon;
import com.pixelmongenerations.common.entity.pixelmon.drops.BossInfo;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.ExtraStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.FlyingOptions;
import com.pixelmongenerations.common.entity.pixelmon.stats.FriendShip;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.IVStore;
import com.pixelmongenerations.common.entity.pixelmon.stats.Level;
import com.pixelmongenerations.common.entity.pixelmon.stats.Stats;
import com.pixelmongenerations.common.entity.pixelmon.stats.SwimOptions;
import com.pixelmongenerations.common.entity.pixelmon.tickHandlers.TickHandlerBase;
import com.pixelmongenerations.common.pokedex.EnumPokedexRegisterStatus;
import com.pixelmongenerations.common.pokedex.Pokedex;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.DatabaseAbilities;
import com.pixelmongenerations.core.database.DatabaseStats;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumMark;
import com.pixelmongenerations.core.enums.EnumMegaPokemon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public abstract class Entity3HasStats
extends Entity2HasModel {
    public static final EnumMap<EnumSpecies, BaseStats> baseStatsStore1 = new EnumMap(EnumSpecies.class);
    public static final Map<String, BaseStats> baseStatsFormStore = new HashMap<String, BaseStats>();
    public Stats stats;
    public BaseStats baseStats;
    public FriendShip friendship;
    public ArrayList<EnumType> type = new ArrayList();
    public float length;
    public boolean doesLevel = true;
    public ExtraStats extraStats = null;
    public boolean isMega;
    public boolean isPrimal;
    public boolean isAsh;
    public boolean isUltra;
    public Level level;
    public String oldName;
    public int oldForm = -1;
    public AbilityBase ability;
    public Integer abilitySlot;
    public TickHandlerBase tickHandler;
    public FlyingOptions bossFlyingParameters = null;
    public SwimOptions bossSwimmingParameters = null;

    public Entity3HasStats(World par1World) {
        super(par1World);
        this.dataManager.register(EntityPixelmon.dwScale, 1000);
        this.dataManager.register(EntityPixelmon.dwForm, -1);
        this.dataManager.register(EntityPixelmon.dwGender, 2);
        this.dataManager.register(EntityPixelmon.dwTotem, false);
        this.dataManager.register(EntityPixelmon.dwAlpha, false);
        this.dataManager.register(EntityPixelmon.dwNoble, false);
        this.dataManager.register(EntityPixelmon.dwCatchable, true);
        this.dataManager.register(EntityPixelmon.dwBreedable, true);
        this.dataManager.register(EntityPixelmon.dwPokeRus, 0);
        this.dataManager.register(EntityPixelmon.dwPokeRusTimer, 0);
        this.dataManager.register(EntityPixelmon.dwGmaxFactor, false);
        this.stats = new Stats();
        if (this instanceof EntityPixelmon) {
            this.level = new Level((EntityPixelmon)this);
            this.friendship = new FriendShip((EntityPixelmon)this);
        }
        this.dataManager.register(EntityPixelmon.dwMaxHP, 10);
        this.dataManager.register(EntityPixelmon.dwDynamaxed, false);
        this.dataManager.register(EntityPixelmon.dwMark, EnumMark.None.ordinal());
    }

    @Override
    protected void init(String name) {
        super.init(name);
        this.oldName = name;
        this.oldForm = this.getForm();
        this.baseStats = this.getBaseStats();
        if (this.baseStats == null && !this.world.isRemote) {
            this.setDead();
            return;
        }
        this.extraStats = this.getExtraStats(name);
        if (!this.stats.IVs.hasIVS()) {
            this.stats.IVs = EnumSpecies.legendaries.contains(name) || EnumSpecies.ultrabeasts.contains(name) ? IVStore.createNewIVs3Perfect() : IVStore.createNewIVs();
        }
        this.setType();
        this.length = this.baseStats.length;
        if (!this.world.isRemote && (this.gender == null || this.getGender() == Gender.Male && this.baseStats.malePercent <= 0 || this.getGender() == Gender.Female && (this.baseStats.malePercent == 100 || this.baseStats.malePercent < 0) || this.getGender() == Gender.None && this.baseStats.malePercent >= 0)) {
            this.chooseRandomGender();
        }
        this.isImmuneToFire = this.type.contains((Object)EnumType.Fire);
        if (!(this instanceof EntityStatue) && this.level.getLevel() == -1) {
            int spawnLevelRange = this.baseStats.spawnLevelRange;
            int spawnLevel = this.baseStats.spawnLevel;
            if (spawnLevelRange <= 0) {
                this.level.setLevel(spawnLevel);
            } else {
                this.level.setLevel(spawnLevel + this.rand.nextInt(spawnLevelRange));
            }
            this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.stats.HP);
            this.setHealth(this.stats.HP);
        }
        if (this.getAbility() == null) {
            this.giveAbility();
        }
        if (this.getForm() == -1 && this.hasForms() && PixelmonModelRegistry.getModel(this.getSpecies(), this.getFormEnum()) == null && this.getOwner() == null) {
            this.update(EnumUpdateType.Name, EnumUpdateType.Stats, EnumUpdateType.Texture);
            this.setForm(Entity3HasStats.getRandomForm(this.getSpecies()));
        }
        this.setSize(this.baseStats.width, this.baseStats.height + this.baseStats.hoverHeight);
        this.setPosition(this.posX, this.posY, this.posZ);
        if (this instanceof EntityPixelmon) {
            this.tickHandler = TickHandlerBase.getTickHandler((EntityPixelmon)this);
        }
    }

    public BaseStats getBaseStats() {
        if (this.baseStats == null) {
            Optional<BaseStats> optBs = Entity3HasStats.getBaseStats(this.oldName, this.getForm());
            if (!optBs.isPresent()) {
                Pixelmon.LOGGER.error("Cannot find base stats for " + this.oldName + " with form " + this.getForm());
            }
            this.baseStats = optBs.orElse(null);
        }
        return this.baseStats;
    }

    public int getCatchRate() {
        float c = this.baseStats.catchRate;
        return (int)c;
    }

    public Level getLvl() {
        return this.level;
    }

    private ExtraStats getExtraStats(String name) {
        return ExtraStats.getExtraStats(name);
    }

    float getMoveSpeed() {
        return 0.3f + (1.0f - (200.0f - (float)this.stats.Speed) / 200.0f) * 0.3f;
    }

    private void setType() {
        this.type.clear();
        this.type.add(this.baseStats.type1);
        if (this.baseStats.type2 != EnumType.Mystery) {
            this.type.add(this.baseStats.type2);
        }
    }

    @Override
    public void evolve(PokemonSpec evolveTo) {
        super.evolve(evolveTo);
        int form = this.getForm();
        if (evolveTo.form != null) {
            form = evolveTo.form;
        }
        float oldHp = this.stats.HP;
        float oldHealth = this.getHealth();
        if (!Entity3HasStats.getBaseStats(evolveTo.name, form).isPresent()) {
            return;
        }
        this.baseStats = Entity3HasStats.getBaseStats(evolveTo.name, form).get();
        if (evolveTo.form != null) {
            this.setForm(evolveTo.form, false);
        } else if (this.getForm() == -1) {
            this.setForm(0, false);
        }
        this.setType();
        String[] oldAbilities = DatabaseAbilities.getAbilities(this.getPokemonName());
        String[] newAbilities = this.baseStats.abilities;
        Integer newSlot = this.getAbilitySlot();
        if (this.getAbilitySlot() == 0 && oldAbilities[1] == null && newAbilities[1] != null) {
            newSlot = RandomHelper.getRandomNumberBetween(0, 1);
        }
        if (this.getAbilitySlot() == 1 && newAbilities[1] == null) {
            newSlot = 0;
        }
        if (this.getAbilitySlot() == 3) {
            newSlot = 2;
        }
        if (evolveTo.getSpecies() == EnumSpecies.Lycanroc && this.getAbilitySlot() == 2) {
            newSlot = 1;
        }
        this.setAbilitySlot(newSlot);
        String chosenAbility = newAbilities[newSlot];
        this.setAbility(chosenAbility);
        if (!this.hasForms() && this.getForm() > -1) {
            this.setForm(-1);
        }
        this.updateStats();
        float newHealth = this.stats.HP;
        if (oldHp != 0.0f) {
            newHealth = oldHealth / oldHp * (float)this.stats.HP;
        }
        this.setHealth((int)Math.ceil(newHealth));
        Optional<PlayerStorage> storage = this.getStorage();
        if (storage.isPresent()) {
            Pokedex pokedex = storage.get().pokedex;
            EntityPlayerMP player = (EntityPlayerMP) pokedex.owner;
            if (!MinecraftForge.EVENT_BUS.post(new PokedexEvent(player, pokedex, this.getSpecies(), EnumPokedexRegisterStatus.caught))) {
                pokedex.set(this.getSpecies(), EnumPokedexRegisterStatus.caught);
                pokedex.sendToPlayer(player);
            }
        }
        if (this.getOwner() != null) {
            this.update(EnumUpdateType.Name, EnumUpdateType.Stats);
        }
    }

    @Override
    public void setHealth(float par1) {
        super.setHealth(par1);
        this.updateHealth();
    }

    public void updateHealth() {
        if (this.stats != null && this.getHealth() > this.getMaxHealth()) {
            super.setHealth(this.getMaxHealth());
        }
        if (this.getHealth() < 0.0f) {
            super.setHealth(0.0f);
        }
        if (this.getOwner() != null && !this.world.isRemote) {
            this.update(EnumUpdateType.HP);
        }
    }

    public float getPixelmonScale() {
        return (float)this.dataManager.get(EntityPixelmon.dwScale).intValue() / 1000.0f;
    }

    public void setPixelmonScale(float scale) {
        this.dataManager.set(EntityPixelmon.dwScale, (int)(scale * 1000.0f));
    }

    public void updateStats() {
        if (this.level != null) {
            this.stats.setLevelStats(this.getNature(), this.getPseudoNature(), this.baseStats, this.level.getLevel());
            this.dataManager.set(EntityPixelmon.dwMaxHP, this.stats.HP);
            this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(this.stats.HP);
            this.updateHealth();
        }
    }

    public AbilityBase getAbility() {
        return this.ability;
    }

    public void setAbility(AbilityBase newAbility) {
        this.ability = newAbility;
    }

    public void setAbility(String abilityName) {
        try {
            Optional<AbilityBase> abilityOptional = AbilityBase.getAbility(abilityName);
            this.ability = abilityOptional.orElseGet(() -> new ComingSoon(abilityName));
        }
        catch (Exception exception) {
            Pixelmon.LOGGER.info("Entity3HasStats.setAbility() is erroring and it is weird.");
            if (!PixelmonConfig.printErrors) {
                return;
            }
            exception.printStackTrace();
        }
    }

    private void giveAbility() {
        String abilityName = "";
        if (this.getSpecies() == EnumSpecies.Greninja || this.oldName.equalsIgnoreCase("Greninja")) {
            this.abilitySlot = 0;
            abilityName = this.baseStats.abilities[this.abilitySlot];
            this.setAbility(abilityName);
            return;
        }
        if (this.baseStats.abilities != null && (PixelmonConfig.hiddenAbilityRate <= 0 || this.baseStats.abilities[2] == null || RandomHelper.getRandomNumberBetween(1, PixelmonConfig.hiddenAbilityRate) != 1)) {
            this.abilitySlot = this.baseStats.abilities[1] != null ? Integer.valueOf(RandomHelper.getRandomNumberBetween(0, 1)) : Integer.valueOf(0);
            abilityName = this.baseStats.abilities[this.abilitySlot];
            this.setAbility(abilityName);
        } else {
            this.abilitySlot = 2;
            if (this.baseStats.abilities != null) {
                if (this.baseStats.abilities[this.abilitySlot] != null) {
                    abilityName = this.baseStats.abilities[this.abilitySlot];
                    this.setAbility(abilityName);
                } else {
                    this.ability = new ComingSoon(abilityName);
                }
            }
        }
    }

    public Integer getAbilitySlot() {
        return this.abilitySlot;
    }

    public void setAbilitySlot(Integer slot) {
        this.abilitySlot = Math.max(0, Math.min(2, slot));
    }

    public void giveAbilitySlot() {
        Integer slot = DatabaseAbilities.getSlotForAbility(this.baseStats.id, this.ability.getName());
        this.abilitySlot = slot == null ? Integer.valueOf(0) : slot;
    }

    public void setRandomAbilityUniform() {
        ArrayList<String> randomAbilities = new ArrayList<String>();
        for (String ability : this.baseStats.abilities) {
            if (ability == null) continue;
            randomAbilities.add(ability);
        }
        if (!randomAbilities.isEmpty()) {
            this.setAbility((String)RandomHelper.getRandomElementFromList(randomAbilities));
        }
    }

    public static String getAbility(EnumSpecies species, int form, int slot) {
        Optional<BaseStats> optBs = EntityPixelmon.getBaseStats(species, form);
        if (optBs.isPresent()) {
            BaseStats bs = optBs.get();
            if (species == EnumSpecies.Lycanroc && slot == 3) {
                slot = 2;
            }
            return bs.abilities[slot] != null ? bs.abilities[slot] : bs.abilities[0];
        }
        return "SwiftSwim";
    }

    public int getForm() {
        return this.dataManager.get(EntityPixelmon.dwForm);
    }

    public IEnumForm getFormEnum() {
        return Gender.mfModels.contains((Object)this.getSpecies()) ? this.getGender() : this.getSpecies().getFormEnum(this.getForm());
    }

    public void setForm(int form) {
        this.setForm(form, true);
    }

    public void setForm(int form, boolean update) {
        if (this.baseStats != null) {
            boolean formChange = false;
            if (!(this.hasForms() || this.isMega || this.isPrimal || this.isUltra || this.isAsh)) {
                if (this.baseStats.pokemon.hasMega() && this instanceof EntityStatue) {
                    int numForms = EnumMegaPokemon.getMega((EnumSpecies)this.baseStats.pokemon).numMegaForms;
                    if (form > numForms || form == 0 || form < -1) {
                        form = -1;
                    }
                    formChange = true;
                } else {
                    form = -1;
                }
            } else {
                formChange = true;
            }
            if (formChange) {
                Optional<BaseStats> baseStatsOpt = Entity3HasStats.getBaseStats(this.baseStats.pokemon, form);
                if (baseStatsOpt.isPresent()) {
                    this.baseStats = baseStatsOpt.get();
                    this.setType();
                    if (this.isMega) {
                        this.setAbility(this.baseStats.abilities[form > -1 ? 0 : this.getAbilitySlot()]);
                    } else if (this.isPrimal) {
                        this.setAbility(this.baseStats.abilities[form > -1 ? 0 : this.getAbilitySlot()]);
                    } else {
                        this.setAbility(EntityPixelmon.getAbility(this.getSpecies(), form, this.getAbilitySlot()));
                    }
                } else {
                    return;
                }
            }
        }
        this.dataManager.set(EntityPixelmon.dwForm, form);
        if (this instanceof EntityPixelmon && update) {
            this.updateStats();
            this.update(EnumUpdateType.Stats);
            EntityPixelmon pokemon = (EntityPixelmon)this;
            if (pokemon.battleController != null) {
                pokemon.battleController.updateFormChange(pokemon);
            }
        }
    }

    public boolean hasForms() {
        return this.baseStats != null && Entity3HasStats.hasForms(this.baseStats.pokemon);
    }

    public int getNumForms() {
        return this.hasForms() ? Entity3HasStats.getNumForms(this.baseStats.pokemon) : 0;
    }

    public int getFormIncludeTransformed() {
        return this.transformed ? this.transformedForm : this.getForm();
    }

    public int getPartyPosition() {
        Optional<PlayerStorage> storage = this.getStorage();
        return storage.map(playerStorage -> playerStorage.getPosition(this.getPokemonId())).orElse(-1);
    }

    public boolean isAvailableGeneration() {
        return Entity3HasStats.isAvailableGeneration(this.getPokemonName());
    }

    public void chooseRandomGender() {
        this.setGender(Entity3HasStats.getRandomGender(this.baseStats));
    }

    public SpawnLocation getDefaultSpawnLocation() {
        return this.baseStats != null && this.baseStats.spawnLocations != null && this.baseStats.spawnLocations.length > 0 ? this.baseStats.spawnLocations[0] : SpawnLocation.Land;
    }

    public FlyingOptions getFlyingParameters() {
        BossInfo info;
        if (this.bossFlyingParameters != null) {
            return this.bossFlyingParameters;
        }
        if (this.isBossPokemon() && this.baseStats.pokemon.hasMega() && (info = DropItemRegistry.getBossPokemon(this.baseStats.pokemon)) != null) {
            this.bossFlyingParameters = info.flyingParameters;
            return this.bossFlyingParameters;
        }
        return this.baseStats.flyingParameters;
    }

    public SwimOptions getSwimmingParameters() {
        BossInfo info;
        if (this.bossSwimmingParameters != null) {
            return this.bossSwimmingParameters;
        }
        if (this.isBossPokemon() && this.baseStats.pokemon.hasMega() && (info = DropItemRegistry.getBossPokemon(this.baseStats.pokemon)) != null) {
            this.bossSwimmingParameters = info.swimmingParameters;
            return this.bossSwimmingParameters;
        }
        return this.baseStats.swimmingParameters;
    }

    public BaseStats initializeBaseStatsIfNull() {
        if (this.baseStats == null) {
            String name = this.getName();
            this.baseStats = Entity3HasStats.getBaseStats(name, this.getForm()).orElse(null);
            if (this.baseStats == null) {
                this.baseStats = Entity3HasStats.getBaseStats(name).orElse(null);
            }
            if (this.baseStats == null) {
                this.baseStats = Entity3HasStats.getBaseStats(EnumSpecies.Bulbasaur).orElse(null);
            }
        }
        return this.baseStats;
    }

    @Override
    public void onDeath(DamageSource cause) {
        boolean showDeathMessages = this.world.getGameRules().getBoolean("showDeathMessages");
        this.world.getGameRules().setOrCreateGameRule("showDeathMessages", "false");
        super.onDeath(cause);
        this.world.getGameRules().setOrCreateGameRule("showDeathMessages", showDeathMessages ? "true" : "false");
    }

    @Override
    public boolean attackEntityFrom(DamageSource source, float amount) {
        if (this instanceof EntityStatue) {
            return false;
        }
        if (source.getImmediateSource() == this.getOwner()) {
            this.friendship.hurtByOwner();
        }
        return super.attackEntityFrom(source, amount);
    }

    @Override
    public void fall(float distance, float damageMultiplier) {
        if (this.baseStats != null) {
            if (this.getSpawnLocation() == SpawnLocation.Water) {
                return;
            }
            if (this.baseStats.canFly) {
                return;
            }
        }
        super.fall(distance, damageMultiplier);
    }

    @Override
    public boolean canBreatheUnderwater() {
        return this.baseStats == null || this.getSpawnLocation() == SpawnLocation.Water || this.baseStats.type1 == EnumType.Water || this.baseStats.type2 == EnumType.Water;
    }

    @Override
    public void setPosition(double x, double y, double z) {
        float scaleFactor = PixelmonConfig.scaleModelsUp ? 1.3f : 1.0f;
        this.posX = x;
        this.posY = y;
        this.posZ = z;
        float scale = 1.0f;
        if (this.isInitialised) {
            scale = this.getPixelmonScale() * scaleFactor * this.getScaleFactor();
        }
        if (this.baseStats != null) {
            float halfWidth = this.baseStats.width * scale / 2.0f;
            this.setEntityBoundingBox(new AxisAlignedBB(x - (double)halfWidth, y, z - (double)halfWidth, x + (double)halfWidth, y + (double)this.baseStats.height * (double)scale + (double)this.baseStats.hoverHeight, z + (double)halfWidth));
        } else {
            float halfWidth = this.width * scale / 2.0f;
            this.setEntityBoundingBox(new AxisAlignedBB(x - (double)halfWidth, y, z - (double)halfWidth, x + (double)halfWidth, y - this.getYOffset() + (double)this.height * (double)scale, z + (double)halfWidth));
        }
    }

    @Override
    public int getMaxSpawnedInChunk() {
        return this.rand.nextInt(this.baseStats.maxGroupSize - this.baseStats.minGroupSize + 1) + this.baseStats.minGroupSize;
    }

    @Override
    public void onUpdate() {
        if (!(this instanceof EntityStatue)) {
            if (!((Entity10CanBreed)this).isInRanchBlock && this.getOwner() != null && !this.world.isRemote) {
                this.friendship.tick();
            }
            if (this.hasOwner() && this.getOwner() == null && !this.world.isRemote) {
                this.setDead();
            }
        } else {
            this.rotationYawHead = this.rotationYaw;
        }
        if (this.tickHandler != null) {
            this.tickHandler.tick(this.world);
        }
        super.onUpdate();
        if (this.world.isRemote) {
            if (this.getForm() != this.oldForm) {
                this.baseStats = Entity3HasStats.getBaseStats(this.baseStats.pokemon, this.getForm()).get();
                this.oldForm = this.getForm();
            }
            if (!this.oldName.equals(this.getPokemonName())) {
                this.isInitialised = false;
                this.baseStats = Entity3HasStats.getBaseStats(this.getPokemonName()).get();
                this.oldName = this.getPokemonName();
            }
        }
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        if (!(this instanceof EntityStatue)) {
            if (!(this.isMega || this.isPrimal || this.isUltra || this.isAsh)) {
                this.stats.writeToNBT(nbt);
            }
            this.level.writeToNBT(nbt);
            this.friendship.writeToNBT(nbt);
            nbt.setBoolean("DoesLevel", this.doesLevel);
            if (this.extraStats != null) {
                this.extraStats.writeToNBT(nbt);
            }
            if (!(this.ability == null || this.isMega || this.isPrimal || this.isUltra || this.isAsh)) {
                String abilityName = this.ability.getName();
                if (abilityName.equals("Coming Soon")) {
                    nbt.setString("Ability", ((ComingSoon)this.ability).getTrueAbility());
                } else {
                    nbt.setString("Ability", abilityName);
                }
            }
            if (this.abilitySlot != null) {
                nbt.setInteger("AbilitySlot", this.abilitySlot);
            }
        }
        if (!(this.isMega || this.isPrimal || this.isUltra || this.isAsh)) {
            int form = this.getForm();
            nbt.setInteger("Variant", form);
        }
        nbt.setBoolean("Totem", this.isTotem());
        nbt.setBoolean("gmaxfactor", this.hasGmaxFactor());
        nbt.setBoolean("Alpha", this.isAlpha());
        nbt.setBoolean("Noble", this.isNoble());
        nbt.setInteger("Mark", this.getMark().ordinal());
        nbt.setBoolean("Breedable", this.isBreedable());
        nbt.setBoolean("Catchable", this.isCatchable());
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound nbt) {
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(nbt.getInteger("StatsHP"));
        super.readEntityFromNBT(nbt);
        if (!(this instanceof EntityStatue)) {
            block28: {
                this.stats.readFromNBT(nbt);
                this.friendship.readFromNBT(nbt);
                if (this.extraStats != null) {
                    this.extraStats.readFromNBT(nbt);
                }
                try {
                    if (!nbt.hasKey("Ability")) break block28;
                    String abilityName = nbt.getString("Ability");
                    try {
                        this.ability = AbilityBase.getAbility(abilityName).get();
                    }
                    catch (Exception exception) {
                        if (abilityName.equals("ComingSoon")) {
                            if (nbt.hasKey("AbilitySlot")) {
                                int tempAbilitySlot = nbt.getInteger("AbilitySlot");
                                if (this.baseStats.abilities != null && this.baseStats.abilities[tempAbilitySlot] != null) {
                                    abilityName = this.baseStats.abilities[tempAbilitySlot];
                                    this.setAbility(abilityName);
                                }
                            }
                            break block28;
                        }
                        this.giveAbility();
                    }
                }
                catch (Exception exception) {
                    System.out.println("Didn't have an Ability; giving it one.");
                    this.giveAbility();
                }
            }
            try {
                if (nbt.hasKey("AbilitySlot")) {
                    this.abilitySlot = nbt.getInteger("AbilitySlot");
                } else {
                    this.giveAbilitySlot();
                }
            }
            catch (Exception exception) {
                System.out.println("Didn't have an Ability slot; giving it one.");
                this.giveAbilitySlot();
            }
            if (nbt.hasKey("primaryType")) {
                this.type.clear();
                this.type.add(EnumType.parseOrNull(nbt.getShort("primaryType")));
                if (nbt.hasKey("secondaryType") && nbt.getShort("secondaryType") != -1) {
                    this.type.add(EnumType.parseOrNull(nbt.getShort("secondaryType")));
                }
            }
        }
        if (nbt.hasKey("Variant")) {
            this.setForm(nbt.getInteger("Variant"), false);
        }
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.55);
        if (nbt.hasKey("gmaxfactor")) {
            if (this.getSpecies().hasGmaxForm()) {
                this.setGmaxFactor(nbt.getBoolean("gmaxfactor"));
            } else {
                this.setGmaxFactor(false);
            }
        } else {
            this.setGmaxFactor(false);
        }
        if (nbt.hasKey("Alpha")) {
            this.setAlphaFactor(nbt.getBoolean("Alpha"));
        } else {
            this.setAlphaFactor(false);
        }
        if (nbt.hasKey("Noble")) {
            this.setNoble(nbt.getBoolean("Noble"));
        } else {
            this.setNoble(false);
        }
        if (nbt.hasKey("Catchable")) {
            this.setCatchable(nbt.getBoolean("Catchable"));
        } else {
            this.setCatchable(true);
        }
        if (nbt.hasKey("Breedable")) {
            this.setBreedable(nbt.getBoolean("Breedable"));
        } else {
            this.setBreedable(true);
        }
        this.setMark(nbt.hasKey("Mark") ? EnumMark.values()[nbt.getInteger("Mark")] : EnumMark.None);
    }

    @Override
    public void getNBTTags(HashMap<String, Class> tags) {
        super.getNBTTags(tags);
        this.stats.getNBTTags(tags);
        this.level.getNBTTags(tags);
        this.friendship.getNBTTags(tags);
        ExtraStats.getNBTTags(tags);
        tags.put("DoesLevel", Boolean.class);
        tags.put("Ability", String.class);
        tags.put("AbilitySlot", Integer.class);
        tags.put("Health", Float.class);
        tags.put("Variant", Integer.class);
        tags.put("Totem", Boolean.class);
        tags.put("Mark", Integer.class);
    }

    public static BaseStats getBaseStats(int index) {
        Optional<EnumSpecies> pokemon = EnumSpecies.getFromDex(index);
        return pokemon.flatMap(Entity3HasStats::getBaseStats).orElse(null);
    }

    public static Optional<BaseStats> getBaseStats(String name) {
        return Entity3HasStats.getBaseStats(name, -1);
    }

    public static Optional<BaseStats> getBaseStats(EnumSpecies pokemon) {
        return Entity3HasStats.getBaseStats(pokemon, -1);
    }

    public static Optional<BaseStats> getBaseStats(String name, int form) {
        EnumSpecies pokemon = EnumSpecies.getFromNameAnyCase(name);
        return Entity3HasStats.getBaseStats(pokemon, form);
    }

    public static Optional<BaseStats> getBaseStats(EnumSpecies pokemon, int form) {
        Optional<BaseStats> optional;
        if (pokemon == null) {
            return Optional.empty();
        }
        BaseStats baseStats = Entity3HasStats.getBaseStatsFromStore(pokemon, form);
        BaseStats noForm = null;
        if (baseStats != null) {
            if (baseStats.form == form || form == -1) {
                return Optional.of(baseStats);
            }
            noForm = baseStats;
        }
        if (!(optional = DatabaseStats.getBaseStats(pokemon.name, form)).isPresent()) {
            if (noForm != null) {
                return Optional.of(noForm);
            }
            if (form > -1) {
                optional = DatabaseStats.getBaseStats(pokemon.name);
            }
            return optional;
        }
        baseStats = optional.get();
        if (form == -1 && !baseStatsStore1.containsKey((Object)pokemon)) {
            baseStatsStore1.put(pokemon, baseStats);
        } else {
            String storename = pokemon.name + pokemon.getFormEnum(form).getForm();
            if (!baseStatsFormStore.containsKey(storename)) {
                baseStatsFormStore.put(storename, baseStats);
            }
        }
        return Optional.of(baseStats);
    }

    private static BaseStats getBaseStatsFromStore(EnumSpecies pokemon, int form) {
        return form == -1 ? baseStatsStore1.get((Object)pokemon) : baseStatsFormStore.get(pokemon.name + pokemon.getFormEnum(form).getForm());
    }

    public static boolean hasForms(EnumSpecies pokemon) {
        return pokemon.getNumForms(false) > 0;
    }

    public static boolean hasForms(String name) {
        Optional<EnumSpecies> pokemon = EnumSpecies.getFromName(name);
        return pokemon.isPresent() && Entity3HasStats.hasForms(pokemon.get());
    }

    public static int getNumForms(String name) {
        Optional<EnumSpecies> pokemon = EnumSpecies.getFromName(name);
        return pokemon.map(Entity3HasStats::getNumForms).orElse(0);
    }

    public static int getNumForms(EnumSpecies pokemon) {
        return Entity3HasStats.hasForms(pokemon) ? pokemon.getNumForms(false) : 0;
    }

    public static int getRandomForm(EnumSpecies pokemon) {
        return Entity3HasStats.hasForms(pokemon) ? (pokemon.getFormEnum(0) instanceof EnumForms ? (int)EnumForms.NoForm.getForm() : RandomHelper.rand.nextInt(Entity3HasStats.getNumForms(pokemon))) : -1;
    }

    public static boolean isAvailableGeneration(String name) {
        int dexNumber = Entity3HasStats.getPokedexNumber(name);
        return dexNumber != -1 && Entity3HasStats.isAvailableGeneration(dexNumber);
    }

    public static boolean isAvailableGeneration(EnumSpecies pokemon) {
        return Entity3HasStats.isAvailableGeneration(pokemon.getNationalPokedexInteger());
    }

    public static boolean isAvailableGeneration(int dexNumber) {
        int genFrom = Entity3HasStats.getGenerationFrom(dexNumber);
        switch (genFrom) {
            case 1: {
                return PixelmonConfig.Gen1;
            }
            case 2: {
                return PixelmonConfig.Gen2;
            }
            case 3: {
                return PixelmonConfig.Gen3;
            }
            case 4: {
                return PixelmonConfig.Gen4;
            }
            case 5: {
                return PixelmonConfig.Gen5;
            }
            case 6: {
                return PixelmonConfig.Gen6;
            }
            case 7: {
                return PixelmonConfig.Gen7;
            }
            case 8: {
                return PixelmonConfig.Gen8;
            }
        }
        return true;
    }

    public static int getGenerationFrom(EnumSpecies pokemon) {
        return Entity3HasStats.getGenerationFrom(pokemon.getNationalPokedexInteger());
    }

    public static int getGenerationFrom(int dexNumber) {
        if (dexNumber <= 151) {
            return 1;
        }
        if (dexNumber <= 251) {
            return 2;
        }
        if (dexNumber <= 386) {
            return 3;
        }
        if (dexNumber <= 493) {
            return 4;
        }
        if (dexNumber <= 649) {
            return 5;
        }
        if (dexNumber <= 721) {
            return 6;
        }
        if (dexNumber <= 809) {
            return 7;
        }
        if (dexNumber <= 898) {
            return 8;
        }
        return 404;
    }

    public static Gender getRandomGender(BaseStats baseStats) {
        return baseStats.malePercent < 0 ? Gender.None : (RandomHelper.rand.nextInt(100) < baseStats.malePercent ? Gender.Male : Gender.Female);
    }

    protected static int getRandomNormalAbilitySlot(String[] abilities) {
        return abilities[1] != null ? RandomHelper.getRandomNumberBetween(0, 1) : 0;
    }

    public static int getPokedexNumber(String name) {
        Optional<EnumSpecies> pokemon = EnumSpecies.getFromName(name);
        return pokemon.map(EnumSpecies::getNationalPokedexInteger).orElse(-1);
    }

    public boolean isTotem() {
        return this.dataManager.get(EntityPixelmon.dwTotem);
    }

    public boolean isAlpha() {
        return this.dataManager.get(EntityPixelmon.dwAlpha);
    }

    public void setAlpha(boolean alpha) {
        this.dataManager.set(EntityPixelmon.dwAlpha, alpha);
    }

    public void setAlphaFactor(boolean isAlpha) {
        this.dataManager.set(EntityPixelmon.dwAlpha, isAlpha);
    }

    public boolean isNoble() {
        return this.dataManager.get(EntityPixelmon.dwNoble);
    }

    public void setNoble(boolean isNoble) {
        this.dataManager.set(EntityPixelmon.dwNoble, isNoble);
    }

    public boolean isCatchable() {
        return this.dataManager.get(EntityPixelmon.dwCatchable);
    }

    public void setCatchable(boolean catchable) {
        this.dataManager.set(EntityPixelmon.dwCatchable, catchable);
    }

    public boolean isBreedable() {
        return this.dataManager.get(EntityPixelmon.dwBreedable);
    }

    public void setBreedable(boolean breedable) {
        this.dataManager.set(EntityPixelmon.dwBreedable, breedable);
    }

    public boolean isDynamaxed() {
        return this.dataManager.get(EntityPixelmon.dwDynamaxed);
    }

    public void setDynamaxed(boolean dynamaxed) {
        this.dataManager.set(EntityPixelmon.dwDynamaxed, dynamaxed);
    }

    public boolean hasGmaxFactor() {
        return this.dataManager.get(EntityPixelmon.dwGmaxFactor);
    }

    public void setGmaxFactor(boolean hasGmaxFactor) {
        this.dataManager.set(EntityPixelmon.dwGmaxFactor, hasGmaxFactor);
    }

    public EnumMark getMark() {
        return EnumMark.values()[this.dataManager.get(EntityPixelmon.dwMark)];
    }

    public void setMark(EnumMark mark) {
        this.dataManager.set(EntityPixelmon.dwMark, mark.ordinal());
    }
}

