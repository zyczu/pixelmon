/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Sunny;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;

public class SolarPower
extends AbilityBase {
    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        if (pokemon.bc.globalStatusController.getWeather() instanceof Sunny && pokemon.getHeldItem() != PixelmonItemsHeld.utilityUmbrella) {
            pokemon.bc.sendToAll("pixelmon.abilities.solarpower", pokemon.getNickname());
            pokemon.doBattleDamage(pokemon, pokemon.getPercentMaxHealth(12.5f), DamageTypeEnum.ABILITY);
        }
    }

    @Override
    public int[] modifyStats(PixelmonWrapper pokemon, int[] stats) {
        if (pokemon.bc.globalStatusController.getWeather() instanceof Sunny && pokemon.getHeldItem() != PixelmonItemsHeld.utilityUmbrella) {
            int n = StatsType.SpecialAttack.getStatIndex();
            stats[n] = (int)((double)stats[n] * 1.5);
        }
        return stats;
    }
}

