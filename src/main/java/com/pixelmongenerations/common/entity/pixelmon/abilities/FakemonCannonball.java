/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import java.util.ArrayList;

public class FakemonCannonball
extends AbilityBase {
    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        ArrayList<PixelmonWrapper> opponents = newPokemon.bc.getOpponentPokemon(newPokemon);
        for (PixelmonWrapper opponent : opponents) {
            newPokemon.bc.sendToAll("pixelmon.abilities.cannonball", newPokemon.getNickname(), opponent.getNickname());
            if (!newPokemon.removeTeamStatus(StatusType.Spikes, StatusType.StealthRock, StatusType.ToxicSpikes)) continue;
            opponent.useTempAttack(new Attack("Splash"));
            if (opponent.hasMoved()) continue;
            opponent.bc.removeFromTurnList(opponent);
        }
    }
}

