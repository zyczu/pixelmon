/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumZygarde;

public class PowerConstruct
extends AbilityBase {
    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        if (pokemon.getSpecies() == EnumSpecies.Zygarde) {
            int oldmax = pokemon.getMaxHealth();
            if (pokemon.getHealthPercent() <= 50.0f) {
                pokemon.setForm(((EnumZygarde)EnumSpecies.Zygarde.getFormEnum(pokemon.getForm())).getComplete().getForm());
                pokemon.bc.modifyStats();
                int currenthealth = pokemon.getHealth();
                int newmax = pokemon.getMaxHealth();
                pokemon.setHealth(newmax - oldmax + currenthealth);
                pokemon.updateHPIncrease();
            }
        }
    }

    @Override
    public boolean canBeNeutralized() {
        return true;
    }
}

