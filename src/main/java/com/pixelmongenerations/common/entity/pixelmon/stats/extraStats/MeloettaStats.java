/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.extraStats;

import com.pixelmongenerations.common.entity.pixelmon.stats.ExtraStats;
import com.pixelmongenerations.core.config.PixelmonConfig;
import net.minecraft.nbt.NBTTagCompound;

public class MeloettaStats
extends ExtraStats {
    public int abundantActivations = 0;
    public static final int MAX_ACTIVATIONS = PixelmonConfig.lightTrioMaxWormholes;

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        nbt.setByte("NumAbundantActivations", (byte)this.abundantActivations);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        this.abundantActivations = nbt.getInteger("NumAbundantActivations");
    }
}

