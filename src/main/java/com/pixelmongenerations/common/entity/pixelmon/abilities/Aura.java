/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.AuraStatus;
import com.pixelmongenerations.common.battle.status.GlobalStatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;

public abstract class Aura
extends AbilityBase {
    private EnumType boostType;
    private StatusType auraStatus;

    public Aura(EnumType boostType, StatusType auraStatus) {
        this.boostType = boostType;
        this.auraStatus = auraStatus;
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        GlobalStatusBase current = newPokemon.bc.globalStatusController.getGlobalStatus(this.auraStatus);
        if (current == null) {
            newPokemon.bc.globalStatusController.addGlobalStatus(new AuraStatus(this.boostType, this.auraStatus));
            newPokemon.bc.sendToAll("pixelmon.abilities." + this.auraStatus.toString().toLowerCase(), newPokemon.getNickname());
        }
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper oldPokemon) {
        this.removeAuraStatus(oldPokemon);
    }

    @Override
    public void onAbilityLost(PixelmonWrapper pokemon) {
        this.removeAuraStatus(pokemon);
    }

    private void removeAuraStatus(PixelmonWrapper pokemon) {
        for (PixelmonWrapper pw : pokemon.bc.getActivePokemon()) {
            if (pw == pokemon || !pw.getBattleAbility().getClass().isAssignableFrom(this.getClass())) continue;
            return;
        }
        pokemon.bc.globalStatusController.removeGlobalStatus(this.auraStatus);
    }
}

