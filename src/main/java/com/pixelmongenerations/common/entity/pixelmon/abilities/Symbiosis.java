/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.item.ItemHeld;

public class Symbiosis
extends AbilityBase {
    @Override
    public void onItemConsumed(PixelmonWrapper pokemon, PixelmonWrapper consumer, ItemHeld heldItem) {
        if (!pokemon.hasHeldItem() && pokemon.getTeamPokemon().contains(consumer) && pokemon.isItemRemovable(pokemon)) {
            consumer.setNewHeldItem(pokemon.getHeldItem());
            pokemon.removeHeldItem();
        }
    }
}

