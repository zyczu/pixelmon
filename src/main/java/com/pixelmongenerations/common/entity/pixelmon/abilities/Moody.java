/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class Moody
extends AbilityBase {
    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        int randomStatToIncrease = -1;
        int randomStatToDecrease = -1;
        this.sendActivatedMessage(pokemon);
        int[] statStages = pokemon.getBattleStats().getStages();
        if (pokemon.getBattleStats().statCanBeRaised()) {
            while (randomStatToIncrease == -1) {
                randomStatToIncrease = RandomHelper.getRandomNumberBetween(2, 6);
                if (statStages[randomStatToIncrease] < 6) {
                    pokemon.getBattleStats().modifyStat(2, pokemon.getBattleStats().getStageEnum(randomStatToIncrease));
                    continue;
                }
                randomStatToIncrease = -1;
            }
        }
        boolean canDecrease = false;
        for (int i = 0; i < statStages.length; ++i) {
            if (i == randomStatToIncrease || statStages[i] >= 6) continue;
            canDecrease = true;
            break;
        }
        if (canDecrease) {
            while (randomStatToDecrease == -1) {
                while (randomStatToIncrease == (randomStatToDecrease = RandomHelper.getRandomNumberBetween(2, 6))) {
                }
                if (statStages[randomStatToDecrease] > -6) {
                    pokemon.getBattleStats().modifyStat(-1, pokemon.getBattleStats().getStageEnum(randomStatToDecrease));
                    continue;
                }
                randomStatToDecrease = -1;
            }
        }
    }
}

