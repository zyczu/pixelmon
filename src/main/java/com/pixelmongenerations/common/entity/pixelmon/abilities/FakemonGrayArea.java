/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.EnumType;
import java.util.List;

public class FakemonGrayArea
extends AbilityBase {
    @Override
    public List<EnumType> getEffectiveTypes(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.attack != null && user.attack.getAttackBase().attackType == EnumType.Psychic) {
            return EnumType.ignoreType(target.type, EnumType.Dark);
        }
        return target.type;
    }
}

