/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class InnerFocus
extends AbilityBase {
    @Override
    public boolean allowsStatus(StatusType status, PixelmonWrapper pokemon, PixelmonWrapper user) {
        return status != StatusType.Flinch;
    }
}

