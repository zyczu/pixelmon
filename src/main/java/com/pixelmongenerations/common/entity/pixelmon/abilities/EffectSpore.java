/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Paralysis;
import com.pixelmongenerations.common.battle.status.Poison;
import com.pixelmongenerations.common.battle.status.Sleep;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LongReach;
import com.pixelmongenerations.common.item.heldItems.ItemProtectivePads;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class EffectSpore
extends AbilityBase {
    @Override
    public void applyEffectOnContactTarget(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.isImmuneToPowder()) {
            return;
        }
        if (user.hasHeldItem() && user.getHeldItem() instanceof ItemProtectivePads) {
            user.bc.sendToAll("pixelmon.effect.protectivepads", user.getNickname());
            return;
        }
        if (user.getBattleAbility() instanceof LongReach) {
            return;
        }
        int r = RandomHelper.getRandomNumberBetween(1, 100);
        if (r <= 9) {
            if (Poison.poison(target, user, null, false)) {
                user.bc.sendToAll("pixelmon.abilities.effectsporepois", target.getNickname(), user.getNickname());
            }
        } else if (r <= 19) {
            if (Paralysis.paralyze(target, user, null, false)) {
                user.bc.sendToAll("pixelmon.abilities.effectsporeparal", target.getNickname(), user.getNickname());
            }
        } else if (r <= 30 && Sleep.sleep(target, user, null, false)) {
            user.bc.sendToAll("pixelmon.abilities.effectsporesleep", target.getNickname(), user.getNickname());
        }
    }
}

