/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;

public class Rivalry
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (user.getGender() == target.getGender()) {
            power = (int)((double)power * 1.25);
            return new int[]{power, accuracy};
        }
        if (user.getGender() != Gender.None && target.getGender() != Gender.None) {
            power = (int)((double)power * 0.75);
        }
        return new int[]{power, accuracy};
    }
}

