/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class BattleArmour
extends AbilityBase {
    @Override
    public boolean preventsCriticalHits(PixelmonWrapper opponent) {
        return true;
    }
}

