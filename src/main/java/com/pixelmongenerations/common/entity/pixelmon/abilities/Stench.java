/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Flinch;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class Stench
extends AbilityBase {
    @Override
    public void tookDamageUser(int damage, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        EnumHeldItems heldItem = user.getUsableHeldItem().getHeldItemType();
        if (heldItem != EnumHeldItems.kingsRock && heldItem != EnumHeldItems.razorFang && RandomHelper.getRandomChance(10)) {
            Flinch.flinch(user, target);
        }
    }
}

