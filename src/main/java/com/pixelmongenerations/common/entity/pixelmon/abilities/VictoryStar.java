/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class VictoryStar
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyTeammate(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return new int[]{power, (int)((double)accuracy * 1.1)};
    }
}

