/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats;

import com.pixelmongenerations.api.events.ExperienceGainEvent;
import com.pixelmongenerations.api.events.LevelUpEvent;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQuery;
import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQueryList;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.Stats;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types.LevelingEvolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.enums.EnumExpSource;
import com.pixelmongenerations.core.enums.EnumExperienceGroup;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.PixelmonStatsData;
import com.pixelmongenerations.core.network.packetHandlers.OpenReplaceMoveScreen;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.common.MinecraftForge;

public class Level {
    private PokemonLink pixelmon;
    public int expToNextLevel = 0;
    int oldLevel = -1;

    public Level(EntityPixelmon p) {
        this(new EntityLink(p));
        EntityDataManager dataManager = p.getDataManager();
        dataManager.register(EntityPixelmon.dwLevel, -1);
        dataManager.register(EntityPixelmon.dwExp, 0);
        dataManager.register(EntityPixelmon.dwDynamaxLevel, 0);
        this.setScale();
    }

    public Level(PixelmonWrapper p) {
        this(new WrapperLink(p));
    }

    public Level(PokemonLink p) {
        this.pixelmon = p;
    }

    protected void updateStats() {
        this.pixelmon.updateStats();
    }

    public void writeToNBT(NBTTagCompound compound) {
        compound.setInteger("DynamaxLevel", this.getDynamaxLevel());
        compound.setInteger("Level", this.getLevel());
        compound.setInteger("EXP", this.getExp());
        compound.setInteger("EXPToNextLevel", this.canLevelUp() ? this.getExpForLevel(this.getLevel() + 1) - this.getExpForLevel(this.getLevel()) : 0);
    }

    public void readFromNBT(NBTTagCompound compound) {
        this.setDynamaxLevel(compound.getInteger("DynamaxLevel"));
        this.setExp(compound.getInteger("EXP"));
        this.setLevel(compound.getInteger("Level"));
    }

    public int getLevel() {
        return this.pixelmon.getLevel();
    }

    public void setLevel(int level) {
        this.pixelmon.setLevel(level);
        this.setScale();
        this.updateExpToNextLevel();
        Stats stats = this.pixelmon.getStats();
        if (this.pixelmon.getHealth() == stats.HP) {
            this.updateStats();
            this.pixelmon.setHealthDirect(stats.HP);
        } else {
            float oldHP = stats.HP;
            float oldHealth = this.pixelmon.getHealth();
            this.updateStats();
            float newHealth = stats.HP;
            if (oldHP != 0.0f) {
                newHealth = oldHealth / oldHP * (float)stats.HP;
            }
            this.pixelmon.setHealthDirect((int)Math.ceil(newHealth));
        }
    }

    public int getDynamaxLevel() {
        return this.pixelmon.getDynamaxLevel();
    }

    public void setDynamaxLevel(int level) {
        this.pixelmon.setDynamaxLevel(level);
    }

    public void updateExpToNextLevel() {
        this.expToNextLevel = this.getLevel() == 1 ? this.getExpForLevel(this.getLevel() + 1) : (this.canLevelUp() ? this.getExpForLevel(this.getLevel() + 1) - this.getExpForLevel(this.getLevel()) : 0);
    }

    private int getExpForLevel(int level) {
        EnumExperienceGroup ex = this.pixelmon.getBaseStats().experienceGroup;
        return ex.getExpForLevel(level);
    }

    public int getExp() {
        return this.pixelmon.getExp();
    }

    public void setExp(int exp) {
        this.pixelmon.setExp(exp);
    }

    public boolean canLevelUp() {
        return this.getLevel() < PixelmonServerConfig.maxLevel;
    }

    protected void onLevelUp(PixelmonStatsData stats) {
        this.updateStats();
        this.pixelmon.updateLevelUp(stats);
        this.pixelmon.getFriendship().onLevelUp();
        this.setScale();
    }

    public void awardEXP(int exp) {
        this.awardEXP(exp, true, EnumExpSource.Default);
    }

    public void awardEXP(int exp, boolean shouldTriggerLevelEvent, EnumExpSource source) {
        if (this.pixelmon.doesLevel()) {
            ExperienceGainEvent expGainEvent = new ExperienceGainEvent(this.pixelmon, exp);
            MinecraftForge.EVENT_BUS.post(expGainEvent);
            exp = expGainEvent.getExperience();
            this.setExp(this.getExp() + exp);
            EntityPlayerMP owner = this.pixelmon.getPlayerOwner();
            BattleControllerBase bc = this.pixelmon.getBattleController();
            if (owner != null && this.canLevelUp() && exp > 0) {
                TextComponentTranslation message = ChatHandler.getMessage("pixelmon.stats.gainexp", this.pixelmon.getRealNickname(), exp);
                if (bc == null) {
                    ChatHandler.sendChat(owner, message);
                } else {
                    bc.sendToPlayer(owner, message);
                }
            }
            if (this.canLevelUp() && this.expToNextLevel != -1) {
                if (owner != null) {
                    boolean isReplacingMove = false;
                    boolean didLevel = false;
                    while (this.getExp() >= this.expToNextLevel) {
                        this.pixelmon.update(EnumUpdateType.Name);
                        int newExp = this.getExp() - this.expToNextLevel;
                        if (!this.canLevelUp() || shouldTriggerLevelEvent && MinecraftForge.EVENT_BUS.post(new LevelUpEvent(owner, this.pixelmon, this.getLevel() + 1, source))) break;
                        didLevel = true;
                        PixelmonStatsData stats = PixelmonStatsData.createPacket(this.pixelmon);
                        this.setLevel(this.getLevel() + 1);
                        this.onLevelUp(stats);
                        this.setExp(newExp);
                        int newLevel = this.getLevel();
                        ArrayList<Attack> newAttacks = DatabaseMoves.getAttacksAtLevel(this.pixelmon.getEntity(), newLevel);
                        for (Attack a : newAttacks) {
                            Moveset moveset = this.pixelmon.getMoveset();
                            if (moveset.hasAttack(a)) continue;
                            if (moveset.size() >= 4) {
                                isReplacingMove = true;
                                Pixelmon.NETWORK.sendTo(new OpenReplaceMoveScreen(owner.getUniqueID(), this.pixelmon.getPokemonID(), a.getAttackBase().attackIndex, 0, newLevel, true), owner);
                                continue;
                            }
                            moveset.add(a);
                            this.pixelmon.update(EnumUpdateType.Moveset);
                            TextComponentTranslation message = ChatHandler.getMessage("pixelmon.stats.learnedmove", this.pixelmon.getRealNickname(), a.getAttackBase().getLocalizedName());
                            if (bc != null) {
                                ChatHandler.sendBattleMessage(owner, message);
                                continue;
                            }
                            ChatHandler.sendChat(owner, message);
                        }
                        if (bc == null || bc.rules.levelCap >= PixelmonServerConfig.maxLevel || bc.rules.levelCap < newLevel) continue;
                        this.setExp(0);
                        break;
                    }
                    this.pixelmon.update(EnumUpdateType.Stats);
                    if (didLevel && !isReplacingMove) {
                        this.tryEvolution();
                    }
                }
            } else {
                this.setExp(0);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean checkForExistingEvolutionQuery() {
        List<EvolutionQuery> list;
        List<EvolutionQuery> list2 = list = EvolutionQueryList.queryList;
        synchronized (list2) {
            for (int i = 0; i < EvolutionQueryList.queryList.size(); ++i) {
                if (!PixelmonMethods.isIDSame(EvolutionQueryList.queryList.get((int)i).pokemonID, this.pixelmon)) continue;
                return true;
            }
        }
        return false;
    }

    public void tryEvolution() {
        boolean hasEvolved = this.checkForExistingEvolutionQuery();
        if (this.pixelmon.getHeldItem().getHeldItemType() != EnumHeldItems.everStone && !hasEvolved) {
            BattleControllerBase bc = this.pixelmon.getBattleController();
            if (bc != null && !bc.battleEnded) {
                bc.addCheckEvolve(this.pixelmon);
            } else if (this.pixelmon.getEntity() != null) {
                if (this.pixelmon.getEntity().getSpecies() != EnumSpecies.Basculin) {
                    this.pixelmon.getEntity().testLevelEvolution(this.pixelmon.getLevel());
                    this.pixelmon.getEntity().testNatureEvolution(this.pixelmon.getLevel());
                } else if (this.pixelmon.getEntity().getForm() == 2) {
                    PokemonSpec spec = PokemonSpec.from("Basculegion");
                    this.pixelmon.getEntity().startEvolution(new LevelingEvolution(EnumSpecies.Petilil, spec, this.pixelmon.getLevel(), new EvoCondition[0]));
                }
            }
        }
    }

    private void setScale() {
        float percent = 1.0f;
        percent = 0.8f + 0.4f * (float)this.getLevel() / (float)PixelmonServerConfig.maxLevel;
        this.pixelmon.setScale(percent);
    }

    public void recalculateXP() {
        this.setExp(0);
        this.expToNextLevel = this.getExpForLevel(this.getLevel() + 1) - this.getExpForLevel(this.getLevel());
    }

    public int getExpForNextLevelClient() {
        if (this.oldLevel != this.getLevel()) {
            this.expToNextLevel = this.getExpForLevel(this.getLevel() + 1) - this.getExpForLevel(this.getLevel());
            this.oldLevel = this.getLevel();
        }
        return this.expToNextLevel;
    }

    public float getExpFraction() {
        return Level.getExpFraction(this.getExp(), this.expToNextLevel);
    }

    public static float getExpFraction(int exp, int expToNextLevel) {
        return expToNextLevel == 0 ? 0.0f : (float)exp / (float)expToNextLevel;
    }

    public void getNBTTags(HashMap<String, Class> tags) {
        tags.put("Level", Integer.class);
        tags.put("EXP", Integer.class);
        tags.put("EXPToNextLevel", Integer.class);
        tags.put("DynamaxLevel", Integer.class);
    }
}

