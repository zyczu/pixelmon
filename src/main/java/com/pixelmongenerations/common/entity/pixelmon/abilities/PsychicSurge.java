/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.status.PsychicTerrain;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Surge;

public class PsychicSurge
extends Surge {
    public PsychicSurge() {
        super(new PsychicTerrain());
    }
}

