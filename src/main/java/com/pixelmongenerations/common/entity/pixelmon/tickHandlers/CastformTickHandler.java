/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.tickHandlers;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.tickHandlers.TickHandlerBase;
import com.pixelmongenerations.common.spawning.spawners.EnumWorldState;
import com.pixelmongenerations.common.spawning.spawners.SpawnerBase;
import com.pixelmongenerations.core.enums.forms.EnumCastform;
import net.minecraft.world.World;

public class CastformTickHandler
extends TickHandlerBase {
    int formIndex = -1;

    public CastformTickHandler(EntityPixelmon pixelmon) {
        super(pixelmon, 100);
    }

    @Override
    public void onTick(World world) {
        if (world.isRemote) {
            return;
        }
        if (this.pixelmon.battleController != null || this.pixelmon.isInRanchBlock) {
            return;
        }
        if (world.isRaining()) {
            int j2 = world.getPrecipitationHeight(this.pixelmon.getPosition()).getY();
            float f2 = world.getBiome(this.pixelmon.getPosition()).getTemperature(this.pixelmon.getPosition());
            if (world.getBiomeProvider().getTemperatureAtHeight(f2, j2) >= 0.15f) {
                this.formIndex = EnumCastform.Rain.ordinal();
                if (this.pixelmon.getForm() != this.formIndex) {
                    this.pixelmon.setForm(EnumCastform.Rain.ordinal(), true);
                }
            } else {
                this.formIndex = EnumCastform.Ice.ordinal();
                if (this.pixelmon.getForm() != this.formIndex) {
                    this.pixelmon.setForm(EnumCastform.Ice.ordinal(), true);
                }
            }
        } else if (SpawnerBase.getWorldState(world) == EnumWorldState.day) {
            this.formIndex = EnumCastform.Sun.ordinal();
            if (this.pixelmon.getForm() != this.formIndex) {
                this.pixelmon.setForm(EnumCastform.Sun.ordinal(), true);
            }
        } else {
            this.formIndex = EnumCastform.Normal.ordinal();
            if (this.pixelmon.getForm() != this.formIndex) {
                this.pixelmon.setForm(EnumCastform.Normal.ordinal(), true);
            }
        }
    }
}

