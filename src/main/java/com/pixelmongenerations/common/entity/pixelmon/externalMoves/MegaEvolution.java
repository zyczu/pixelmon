/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.common.entity.pixelmon.helpers.EvolutionQuery;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.ItemMegaStone;
import com.pixelmongenerations.common.item.heldItems.ZCrystal;
import com.pixelmongenerations.core.config.PixelmonItemsHeld;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumGreninja;
import com.pixelmongenerations.core.enums.forms.EnumGroudon;
import com.pixelmongenerations.core.enums.forms.EnumKyogre;
import com.pixelmongenerations.core.enums.forms.EnumNecrozma;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.storage.PlayerStorage;
import net.minecraft.util.math.RayTraceResult;

public class MegaEvolution
extends ExternalMoveBase {
    public MegaEvolution() {
        super("mega", null);
        this.targetted = false;
    }

    @Override
    public boolean execute(EntityPixelmon user, RayTraceResult target, int moveIndex) {
        if (!(user.getOwner() == null || user.isMega || user.isPrimal || user.isUltra || user.isAsh)) {
            PlayerStorage storage = user.getStorage().orElse(null);
            if (storage == null) {
                return false;
            }
            if (user.getSpecies() == EnumSpecies.Kyogre && user.getItemHeld() == PixelmonItemsHeld.blueOrb) {
                new EvolutionQuery(user, EnumKyogre.Primal.getForm());
                user.isPrimal = true;
            } else if (user.getSpecies() == EnumSpecies.Groudon && user.getItemHeld() == PixelmonItemsHeld.redOrb) {
                new EvolutionQuery(user, EnumGroudon.Primal.getForm());
                user.isPrimal = true;
            } else if (user.getSpecies() == EnumSpecies.Greninja && user.getFormEnum() == EnumGreninja.BATTLE_BOND) {
                new EvolutionQuery(user, EnumGreninja.ASH.getForm());
                user.isAsh = true;
            } else if (user.getSpecies() == EnumSpecies.Necrozma && user.getFormEnum() == EnumNecrozma.Dawn && user.getItemHeld() == PixelmonItemsHeld.ultranecroziumZ) {
                new EvolutionQuery(user, EnumNecrozma.UltraDawn.getForm());
                user.isUltra = true;
            } else if (user.getSpecies() == EnumSpecies.Necrozma && user.getFormEnum() == EnumNecrozma.Dusk && user.getItemHeld() == PixelmonItemsHeld.ultranecroziumZ) {
                new EvolutionQuery(user, EnumNecrozma.UltraDusk.getForm());
                user.isUltra = true;
            } else if (user.getSpecies() == EnumSpecies.Rayquaza && EnumSpecies.Rayquaza.hasMega()) {
                if (user.getMoveset().hasAttack("Dragon Ascent", "attack.dragon ascent.name") && !(user.getItemHeld() instanceof ZCrystal)) {
                    new EvolutionQuery(user, 1);
                    user.isMega = true;
                }
            } else {
                ItemHeld held = user.getItemHeld();
                if (!(held instanceof ItemMegaStone) || ((ItemMegaStone)held).pokemon != user.getSpecies()) {
                    return false;
                }
                ItemMegaStone stone = (ItemMegaStone)held;
                if (!storage.megaData.getMegaItem().canEvolve()) {
                    return false;
                }
                new EvolutionQuery(user, stone.form);
                user.isMega = true;
            }
        }
        return true;
    }

    @Override
    public int getCooldown(EntityPixelmon user) {
        return 20;
    }

    @Override
    public int getCooldown(PixelmonData pixelmonData) {
        return 20;
    }

    @Override
    public boolean isDestructive() {
        return false;
    }
}

