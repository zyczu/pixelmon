/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class GrassPelt
extends AbilityBase {
    @Override
    public int[] modifyStatsCancellable(PixelmonWrapper user, int[] stats) {
        if (user.bc.globalStatusController.hasStatus(StatusType.GrassyTerrain)) {
            int n = StatsType.Defence.getStatIndex();
            stats[n] = (int)((double)stats[n] * 1.5);
        }
        return stats;
    }
}

