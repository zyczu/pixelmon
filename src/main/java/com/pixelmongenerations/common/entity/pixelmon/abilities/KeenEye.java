/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.specialAttacks.StatsEffect;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class KeenEye
extends AbilityBase {
    @Override
    public boolean allowsStatChange(PixelmonWrapper pokemon, PixelmonWrapper user, StatsEffect e) {
        if (e.getStatsType() == StatsType.Accuracy) {
            if (!Attack.dealsDamage(user.attack)) {
                pokemon.bc.sendToAll("pixelmon.abilities.keeneye", pokemon.getNickname());
            }
            return false;
        }
        return true;
    }
}

