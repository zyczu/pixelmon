/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import java.util.Arrays;

public class FakemonSteelToe
extends AbilityBase {
    @Override
    public int[] modifyPowerAndAccuracyUser(int power, int accuracy, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        String[] punchMoves = new String[]{"Blaze Kick", "Double Kick", "High Jump Kick", "Hi Jump Kick", "Jump Kick", "Low Kick", "Mega Kick", "Rolling Kick", "Stomp", "Thunderous Kick", "Triple Axel", "Triple Kick"};
        if (Arrays.asList(punchMoves).contains(a.getAttackBase().getUnlocalizedName())) {
            power = (int)((double)power * 1.2);
        }
        return new int[]{power, accuracy};
    }
}

