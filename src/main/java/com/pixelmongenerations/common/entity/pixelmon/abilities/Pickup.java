/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.NoItem;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import net.minecraft.item.ItemStack;

public class Pickup
extends AbilityBase {
    private ItemHeld consumedItem = NoItem.noItem;
    private PixelmonWrapper consumer;

    @Override
    public void onItemConsumed(PixelmonWrapper pokemon, PixelmonWrapper consumer, ItemHeld heldItem) {
        if (pokemon != consumer && !pokemon.bc.simulateMode) {
            this.consumedItem = heldItem;
            this.consumer = consumer;
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        if (pokemon != this.consumer && !pokemon.bc.simulateMode) {
            if (!pokemon.hasHeldItem() && this.consumedItem != NoItem.noItem && this.consumer != null && this.consumer.getConsumedItem() != NoItem.noItem && pokemon.bc.getActivePokemon().contains(this.consumer)) {
                pokemon.setHeldItem(this.consumedItem);
                this.consumer.setConsumedItem(null);
                pokemon.bc.sendToAll("pixelmon.abilities.pickup", pokemon.getNickname(), this.consumedItem.getLocalizedName());
                pokemon.enableReturnHeldItem();
            }
            this.consumedItem = null;
            this.consumer = null;
        }
    }

    @Override
    public boolean needNewInstance() {
        return true;
    }

    public static void pickupItem(PlayerParticipant player) {
        if (PixelmonConfig.pickupRate > 0) {
            for (PixelmonWrapper pw : player.allPokemon) {
                int rand;
                if (!(pw.getAbility() instanceof Pickup) || pw.hasHeldItem() || !RandomHelper.getRandomChance(1.0f / (float)PixelmonConfig.pickupRate)) continue;
                ItemStack foundItem = null;
                int level = pw.getLevelNum();
                ItemStack itemStack = level <= 50 ? DropItemRegistry.getTier1Drop() : (level <= 80 ? ((rand = RandomHelper.getRandomNumberBetween(1, 100)) < 91 ? DropItemRegistry.getTier1Drop() : DropItemRegistry.getTier2Drop()) : ((rand = RandomHelper.getRandomNumberBetween(1, 100)) < 80 ? DropItemRegistry.getTier1Drop() : (foundItem = rand < 100 ? DropItemRegistry.getTier2Drop() : DropItemRegistry.getTier3Drop())));
                if (foundItem == null) continue;
                ItemStack itemCopy = foundItem.copy();
                itemCopy.setCount(1);
                player.player.inventory.addItemStackToInventory(itemCopy);
            }
        }
    }
}

