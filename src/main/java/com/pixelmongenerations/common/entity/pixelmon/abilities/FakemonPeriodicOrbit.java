/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Gravity;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;

public class FakemonPeriodicOrbit
extends AbilityBase {
    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        newPokemon.bc.sendToAll("pixelmon.status.gravity", new Object[0]);
        newPokemon.bc.globalStatusController.addGlobalStatus(new Gravity());
    }
}

