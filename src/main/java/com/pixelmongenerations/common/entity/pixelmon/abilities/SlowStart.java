/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class SlowStart
extends AbilityBase {
    private int turnsRemaining = 5;

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (AbilityBase.checkNeturalizingGas(newPokemon)) {
            this.turnsRemaining = 0;
        } else {
            this.turnsRemaining = 5;
            newPokemon.bc.sendToAll("pixelmon.abilities.slowstart", newPokemon.getNickname());
        }
    }

    @Override
    public boolean canBeNeutralized() {
        return true;
    }

    @Override
    public void applySwitchOutEffect(PixelmonWrapper oldPokemon) {
        this.turnsRemaining = 5;
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        if (this.turnsRemaining > 0) {
            int n = StatsType.Attack.getStatIndex();
            stats[n] = stats[n] / 2;
            int n2 = StatsType.Speed.getStatIndex();
            stats[n2] = stats[n2] / 2;
        }
        return stats;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        if (--this.turnsRemaining == 0) {
            pokemon.bc.sendToAll("pixelmon.abilities.slowstartend", pokemon.getNickname());
        }
    }

    @Override
    public boolean needNewInstance() {
        return true;
    }
}

