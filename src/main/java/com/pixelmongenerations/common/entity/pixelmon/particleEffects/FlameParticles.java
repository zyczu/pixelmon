/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.particleEffects;

import com.pixelmongenerations.common.battle.animations.particles.ParticlePixelmonFlame;
import com.pixelmongenerations.common.battle.animations.particles.ParticleSmoke;
import com.pixelmongenerations.common.entity.pixelmon.Entity4Textures;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.ParticleEffects;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.client.Minecraft;

public class FlameParticles
extends ParticleEffects {
    public float radius;
    public float yOffset;
    private byte count = 0;
    private int size = 0;

    public FlameParticles(Entity4Textures pixelmon, float r, float y, int s) {
        super(pixelmon);
        this.radius = r;
        this.yOffset = y;
        this.size = s;
    }

    private Double random(double sc, boolean np) {
        if (!np) {
            return Math.random() * sc;
        }
        double d = Math.random();
        if (d > 0.5) {
            return Math.random() * sc;
        }
        return -Math.random() * sc;
    }

    private Double random(double sc) {
        return this.random(sc, true);
    }

    @Override
    public void onUpdate() {
        double x = this.pixelmon.posX;
        double y = this.pixelmon.posY + (double)this.pixelmon.hoverTimer + (double)(this.yOffset * this.pixelmon.getScaleFactor());
        double z = this.pixelmon.posZ;
        float f = 180.0f - this.pixelmon.renderYawOffset;
        x += Math.sin(Math.toRadians(f)) * (double)this.radius * (double)this.pixelmon.getScaleFactor();
        z += Math.cos(Math.toRadians(f)) * (double)this.radius * (double)this.pixelmon.getScaleFactor();
        byte countmax = 3;
        byte by = this.count;
        this.count = (byte)(by + 1);
        if (by == countmax) {
            if (this.pixelmon.isWet()) {
                for (int i = 0; i < this.size * 2; ++i) {
                    boolean movedMuch;
                    ParticleSmoke esp = new ParticleSmoke(this.pixelmon.world, x, y, z, this.random(0.001 * (double)this.size), this.random(0.001 * (double)this.size, false), this.random(0.001 * (double)this.size));
                    boolean bl = movedMuch = Math.abs(this.pixelmon.posX - this.pixelmon.lastTickPosX) > 0.3 || Math.abs(this.pixelmon.posY - this.pixelmon.lastTickPosY) > 0.1 || Math.abs(this.pixelmon.posZ - this.pixelmon.lastTickPosZ) > 0.1;
                    if (movedMuch) {
                        esp.setMaxAge(esp.maxAge() / this.size);
                    } else {
                        esp.setMaxAge(esp.maxAge() * this.size / 10);
                    }
                    Minecraft.getMinecraft().effectRenderer.addEffect(esp);
                }
            } else {
                for (int i = 0; i < this.size * 2; ++i) {
                    boolean movedMuch;
                    ParticlePixelmonFlame efp = new ParticlePixelmonFlame(this.pixelmon.world, x, y, z, this.random(0.005 * (double)this.size), this.random(0.015 * (double)this.size, false), this.random(0.005 * (double)this.size));
                    boolean bl = movedMuch = Math.abs(this.pixelmon.posX - this.pixelmon.lastTickPosX) > 0.1 || Math.abs(this.pixelmon.posY - this.pixelmon.lastTickPosY) > 0.1 || Math.abs(this.pixelmon.posZ - this.pixelmon.lastTickPosZ) > 0.1;
                    if (this.pixelmon.baseStats.pokemon == EnumSpecies.Charizard && this.pixelmon.getForm() == 1) {
                        efp.setRBGColorF(0.43f, 0.77f, 0.95f);
                        efp.setParticleTextureIndex(3);
                    }
                    if (movedMuch) {
                        efp.setMaxAge(efp.maxAge() / 4);
                    } else {
                        efp.setMaxAge(efp.maxAge() * this.size / 10);
                    }
                    Minecraft.getMinecraft().effectRenderer.addEffect(efp);
                }
            }
            this.count = 0;
        }
    }
}

