/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.core.enums.battle.AttackCategory;

public class Telepathy
extends AbilityBase {
    @Override
    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        if (a != null && a.getAttackCategory() != AttackCategory.Status && user.bc.getTeamPokemon(user.getParticipant()).contains(pokemon)) {
            user.bc.sendToAll("pixelmon.abilities.telepathy", pokemon.getNickname());
            return false;
        }
        return true;
    }
}

