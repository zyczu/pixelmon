/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.abilities;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;

public class Defeatist
extends AbilityBase {
    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        if (user.getHealthPercent() <= 50.0f) {
            int n = StatsType.Attack.getStatIndex();
            stats[n] = stats[n] / 2;
            int n2 = StatsType.SpecialAttack.getStatIndex();
            stats[n2] = stats[n2] / 2;
        }
        return stats;
    }
}

