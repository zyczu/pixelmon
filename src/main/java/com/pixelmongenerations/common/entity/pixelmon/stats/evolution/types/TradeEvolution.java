/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.stats.evolution.types;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.Evolution;
import com.pixelmongenerations.common.entity.pixelmon.stats.evolution.conditions.EvoCondition;
import com.pixelmongenerations.core.enums.EnumSpecies;

public class TradeEvolution
extends Evolution {
    public EnumSpecies with = null;

    public TradeEvolution(EnumSpecies from, PokemonSpec to, EnumSpecies with, EvoCondition ... conditions) {
        super(from, to, conditions);
        this.with = with;
    }

    public boolean canEvolve(EntityPixelmon pokemon, EnumSpecies with) {
        return (this.with == null || this.with == with) && super.canEvolve(pokemon);
    }

    @Override
    public boolean canEvolve(EntityPixelmon pokemon) {
        boolean evolves = false;
        if (pokemon.getSpecies() == EnumSpecies.Poliwhirl || pokemon.getSpecies() == EnumSpecies.Slowpoke) {
            if (pokemon.heldItem.getItem().getRegistryName() != null && pokemon.heldItem.getItem().getRegistryName().toString().equalsIgnoreCase("pixelmon:kings_rock")) {
                evolves = true;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Scyther || pokemon.getSpecies() == EnumSpecies.Onix) {
            if (pokemon.heldItem.getItem().getRegistryName() != null && pokemon.heldItem.getItem().getRegistryName().toString().equalsIgnoreCase("pixelmon:metal_coat")) {
                evolves = true;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Rhydon) {
            if (pokemon.heldItem.getItem().getRegistryName() != null && pokemon.heldItem.getItem().getRegistryName().toString().equalsIgnoreCase("pixelmon:protector")) {
                evolves = true;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Seadra) {
            if (pokemon.heldItem.getItem().getRegistryName() != null && pokemon.heldItem.getItem().getRegistryName().toString().equalsIgnoreCase("pixelmon:dragon_scale")) {
                evolves = true;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Electabuzz) {
            if (pokemon.heldItem.getItem().getRegistryName() != null && pokemon.heldItem.getItem().getRegistryName().toString().equalsIgnoreCase("pixelmon:electirizer")) {
                evolves = true;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Magmar) {
            if (pokemon.heldItem.getItem().getRegistryName() != null && pokemon.heldItem.getItem().getRegistryName().toString().equalsIgnoreCase("pixelmon:magmarizer")) {
                evolves = true;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Porygon) {
            if (pokemon.heldItem.getItem().getRegistryName() != null && pokemon.heldItem.getItem().getRegistryName().toString().equalsIgnoreCase("pixelmon:upgrade")) {
                evolves = true;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Porygon2) {
            if (pokemon.heldItem.getItem().getRegistryName() != null && pokemon.heldItem.getItem().getRegistryName().toString().equalsIgnoreCase("pixelmon:dubious_disc")) {
                evolves = true;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Feebas) {
            if (pokemon.heldItem.getItem().getRegistryName() != null && pokemon.heldItem.getItem().getRegistryName().toString().equalsIgnoreCase("pixelmon:prism_scale")) {
                evolves = true;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Duskull) {
            if (pokemon.heldItem.getItem().getRegistryName() != null && pokemon.heldItem.getItem().getRegistryName().toString().equalsIgnoreCase("pixelmon:reaper_cloth")) {
                evolves = true;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Clamperl) {
            if (pokemon.heldItem.getItem().getRegistryName() != null && pokemon.heldItem.getItem().getRegistryName().toString().equalsIgnoreCase("pixelmon:deep_sea_tooth") || pokemon.heldItem.getItem().getRegistryName() != null && pokemon.heldItem.getItem().getRegistryName().toString().equalsIgnoreCase("pixelmon:deep_sea_scale")) {
                evolves = true;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Spritzee) {
            if (pokemon.heldItem.getItem().getRegistryName() != null && pokemon.heldItem.getItem().getRegistryName().toString().equalsIgnoreCase("pixelmon:sachet")) {
                evolves = true;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Swirlix) {
            if (pokemon.heldItem.getItem().getRegistryName() != null && pokemon.heldItem.getItem().getRegistryName().toString().equalsIgnoreCase("pixelmon:whipped_dream")) {
                evolves = true;
            }
        } else if (pokemon.getSpecies() == EnumSpecies.Kadabra || pokemon.getSpecies() == EnumSpecies.Machoke || pokemon.getSpecies() == EnumSpecies.Graveler || pokemon.getSpecies() == EnumSpecies.Haunter || pokemon.getSpecies() == EnumSpecies.Boldore || pokemon.getSpecies() == EnumSpecies.Gurdurr || pokemon.getSpecies() == EnumSpecies.Karrablast || pokemon.getSpecies() == EnumSpecies.Shelmet || pokemon.getSpecies() == EnumSpecies.Phantump || pokemon.getSpecies() == EnumSpecies.Pumpkaboo) {
            evolves = true;
        }
        return evolves;
    }
}

