/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.particleEffects;

import com.pixelmongenerations.common.entity.pixelmon.Entity10CanBreed;
import com.pixelmongenerations.common.entity.pixelmon.particleEffects.ParticleEffects;
import com.pixelmongenerations.core.enums.EnumBreedingParticles;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.helper.RandomHelper;

public class BreedingParticles
extends ParticleEffects {
    public int breedingLevel = 0;
    int count = 0;
    boolean particlesOn = false;

    public BreedingParticles(Entity10CanBreed pixelmon) {
        super(pixelmon);
        this.breedingLevel = pixelmon.getNumBreedingLevels();
    }

    @Override
    public void onUpdate() {
        if (this.count <= 0) {
            this.particlesOn = !this.particlesOn;
            this.count = this.particlesOn ? RandomHelper.getRandomNumberBetween(0, 1) : RandomHelper.getRandomNumberBetween(30, 60);
        }
        --this.count;
        if (this.breedingLevel > 0 && this.particlesOn) {
            ClientProxy.spawnParticle(EnumBreedingParticles.getFromIndex(this.breedingLevel), this.pixelmon.world, this.pixelmon.posX, this.pixelmon.posY + (double)(this.pixelmon.height * this.pixelmon.getPixelmonScale() * this.pixelmon.getScaleFactor()), this.pixelmon.posZ);
        }
    }
}

