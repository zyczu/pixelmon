/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pixelmon.externalMoves;

import com.pixelmongenerations.api.events.ExternalMoveEvent;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.network.PixelmonData;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.GameType;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class LightFire
extends ExternalMoveBase {
    public LightFire() {
        super("fire", "Ember", "Fire Blast", "Flamethrower", "Incinerate", "Blast Burn", "Fire Spin", "Flame Burst", "Flame Wheel", "Heat Wave", "Inferno", "Sacred Fire", "Will-O-Wisp");
    }

    @Override
    public boolean execute(EntityPixelmon user, RayTraceResult target, int moveIndex) {
        ExternalMoveEvent.LightFireMoveEvent lightFireEvent;
        if (((EntityPlayerMP)user.getOwner()).interactionManager.getGameType() == GameType.ADVENTURE) {
            return true;
        }
        String attack = user.getMoveset().get(moveIndex).getAttackBase().getUnlocalizedName();
        int width = 1;
        int length = 1;
        boolean cross = false;
        if (attack.equals("Flamethrower")) {
            length = 3;
        }
        if (attack.equals("Fire Blast")) {
            cross = true;
            width = 3;
            length = 3;
        }
        if (attack.equals("Inferno") || attack.equals("Sacred Fire") || attack.equals("Incinerate")) {
            width = 3;
            length = 3;
        }
        if (MinecraftForge.EVENT_BUS.post(lightFireEvent = new ExternalMoveEvent.LightFireMoveEvent((EntityPlayerMP)user.getOwner(), user, this, target, length, width, cross))) {
            return false;
        }
        length = lightFireEvent.getLength();
        width = lightFireEvent.getWidth();
        cross = lightFireEvent.isCross();
        BlockPos pos = target.getBlockPos();
        int initPosX = pos.getX();
        int initPosY = pos.getY();
        int initPosZ = pos.getZ();
        if (!cross) {
            for (int x = 0; x < width; ++x) {
                int posX = x + initPosX;
                for (int z = 0; z < length; ++z) {
                    int posZ = z + initPosZ;
                    this.placeFire(user.world, posX, initPosY, posZ);
                }
            }
        } else {
            for (int x = -1; x < 2; ++x) {
                int posX = x + initPosX;
                for (int z = -1; z < 2; ++z) {
                    int posZ = z + initPosZ;
                    if (x != z && x != -z) continue;
                    this.placeFire(user.world, posX, initPosY, posZ);
                }
            }
        }
        return true;
    }

    private void placeFire(World world, int x, int y, int z) {
        int origy = y;
        while (world.getBlockState(new BlockPos(x, y, z)).getBlock() != Blocks.AIR && y < origy + 3) {
            ++y;
        }
        BlockPos pos = new BlockPos(x, y, z);
        if (world.getBlockState(pos).getBlock() == Blocks.AIR && Blocks.FIRE.canPlaceBlockAt(world, pos)) {
            world.setBlockState(pos, Blocks.FIRE.getDefaultState());
        }
    }

    @Override
    public int getCooldown(EntityPixelmon user) {
        return 800;
    }

    @Override
    public int getCooldown(PixelmonData pixelmonData) {
        return 800;
    }

    @Override
    public boolean isDestructive() {
        return true;
    }

    @Override
    public double getTargetDistance() {
        return 4.0;
    }
}

