/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.entity;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.api.pokemon.SpawnPokemon;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.SpawnColors;
import com.pixelmongenerations.core.util.SpawnMethodCooldowns;
import java.awt.Color;
import java.lang.invoke.LambdaMetafactory;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.jetbrains.annotations.NotNull;

public class SpawningEntity
extends Entity {
    private EntityPlayerMP player;
    private Supplier<Vec3d> location = () -> Vec3d.ZERO;
    private Function<World, EntityPixelmon> pixelmonFunction = w -> new PokemonSpec(EnumSpecies.Magikarp.name).create((World)w);
    private List<Color> colors = Lists.newArrayList(SpawnColors.WHITE);
    private int tick = 0;
    private int maxTick = 200;

    private SpawningEntity(World worldIn) {
        super(worldIn);
    }

    private SpawningEntity(World world, EntityPlayerMP player, Supplier<Vec3d> location, Function<World, EntityPixelmon> pixelmonFunction, List<Color> colors, int maxTick) {
        this(world);
        this.player = player;
        this.location = location;
        this.pixelmonFunction = pixelmonFunction;
        this.colors = colors;
        this.maxTick = maxTick;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    @NotNull
    public AxisAlignedBB getRenderBoundingBox() {
        return TileEntity.INFINITE_EXTENT_AABB;
    }

    @Override
    public boolean isInRangeToRenderDist(double distance) {
        return true;
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        if (!this.world.isRemote) {
            Vec3d loc = this.location.get();
            if (this.tick >= this.maxTick) {
                if (this.player != null) {
                    SpawnPokemon.builder().pixelmon(this.pixelmonFunction).pos(loc.x, loc.y, loc.z).build().spawn(this.player, this.world);
                    SpawnMethodCooldowns.clearCooldown(this.player);
                }
                this.setDead();
            }
            this.reposition();
        }
        ++this.tick;
    }

    @Override
    protected void readEntityFromNBT(NBTTagCompound compound) {
        if (compound.hasKey("colors")) {
            this.colors = IntStream.of(compound.getIntArray("colors")).mapToObj(Color::new).collect(Collectors.toList());
        }
        if (compound.hasKey("tick")) {
            this.tick = compound.getInteger("tick");
        }
        if (compound.hasKey("maxTick")) {
            this.maxTick = compound.getInteger("maxTick");
        }
    }

    @Override
    protected void writeEntityToNBT(NBTTagCompound compound) {
        compound.setIntArray("colors", this.colors.stream().mapToInt(Color::getRGB).toArray());
        compound.setInteger("tick", this.tick);
        compound.setInteger("maxTick", this.maxTick);
    }

    public int getTick() {
        return this.tick;
    }

    public int getMaxTick() {
        return this.maxTick;
    }

    @Override
    protected void entityInit() {
    }

    public List<Color> getColors() {
        return this.colors;
    }

    public SpawningEntity reposition() {
        if (this.location != null) {
            Vec3d loc = this.location.get();
            this.setPositionAndUpdate(loc.x, loc.y, loc.z);
        }
        return this;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Supplier<Vec3d> location = () -> Vec3d.ZERO;
        private Function<World, EntityPixelmon> pixelmonFunction = w -> new PokemonSpec(EnumSpecies.Magikarp.name).create((World)w);
        private List<Color> colors = Lists.newArrayList(SpawnColors.WHITE);
        private int maxTick = 100;

        public Builder pixelmon(EnumSpecies species) {
            return this.pixelmon(new PokemonSpec(species.name));
        }

        public Builder pixelmon(PokemonSpec spec) {
            return this.pixelmon(spec::create);
        }

        public Builder pixelmon(Function<World, EntityPixelmon> pixelmonFunction) {
            this.pixelmonFunction = pixelmonFunction;
            return this;
        }

        public Builder colors(Color ... colors) {
            return this.colors(Lists.newArrayList(colors));
        }

        public Builder colors(List<Color> colors) {
            this.colors = colors;
            return this;
        }

        public Builder maxTick(int maxTick) {
            this.maxTick = maxTick;
            return this;
        }

        public Builder location(Supplier<Vec3d> location) {
            this.location = location;
            return this;
        }

        public Builder location(double x, double y, double z) {
            return this.location(new Vec3d(x, y, z));
        }

        public Builder location(Vec3d vec3d) {
            return this.location(() -> vec3d);
        }

        public Builder location(Entity entity) {
            return this.location(() -> entity.getPositionVector());
        }

        public SpawningEntity build(World world, EntityPlayerMP player) {
            return new SpawningEntity(world, player, this.location, this.pixelmonFunction, this.colors, this.maxTick).reposition();
        }
    }
}

