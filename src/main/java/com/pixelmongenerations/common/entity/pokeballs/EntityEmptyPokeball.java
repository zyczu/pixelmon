/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pokeballs;

import com.pixelmongenerations.api.DropQueryFactory;
import com.pixelmongenerations.api.enums.ReceiveType;
import com.pixelmongenerations.api.events.CaptureEvent;
import com.pixelmongenerations.api.events.PixelmonReceivedEvent;
import com.pixelmongenerations.api.events.PokeballImpactEvent;
import com.pixelmongenerations.common.achievement.PixelmonAchievements;
import com.pixelmongenerations.common.achievement.PixelmonAdvancements;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.Experience;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.drops.DroppedItem;
import com.pixelmongenerations.common.entity.pokeballs.EntityPokeBall;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.common.entity.pokeballs.PokeballTypeHelper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.NoItem;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsPokeballs;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.battle.EnumBattleEndCause;
import com.pixelmongenerations.core.enums.forms.EnumValentine;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class EntityEmptyPokeball
extends EntityPokeBall {
    private int breakChance;
    private int waitTime;
    private BattleControllerBase battleController;
    private boolean isBattleThrown;
    private int totalTime;
    private boolean captureFinished;
    private double pokemonPosX;
    private double pokemonPosY;
    private double pokemonPosZ;
    boolean capturedPokemon;

    public EntityEmptyPokeball(World world) {
        super(world);
        this.breakChance = this.rand.nextInt(30);
        this.isBattleThrown = false;
        this.capturedPokemon = false;
    }

    public EntityEmptyPokeball(World world, EntityLivingBase entityliving, EnumPokeball type, boolean dropItem) {
        super(type, world, entityliving, EnumPokeBallMode.EMPTY);
        this.breakChance = this.rand.nextInt(30);
        this.isBattleThrown = false;
        this.capturedPokemon = false;
        this.thrower = entityliving;
        this.dropItem = dropItem;
        this.shoot(entityliving, entityliving.rotationPitch, entityliving.rotationYaw, 0.0f, 1.0f, 0.5f);
    }

    public EntityEmptyPokeball(World world, EntityLivingBase thrower, EntityPixelmon target, EnumPokeball type, BattleControllerBase battleController) {
        super(type, world, thrower, EnumPokeBallMode.BATTLE);
        this.breakChance = this.rand.nextInt(30);
        this.capturedPokemon = false;
        this.thrower = thrower;
        this.dropItem = false;
        this.endRotationYaw = thrower.rotationYawHead;
        this.pixelmon = target;
        this.isBattleThrown = true;
        this.battleController = battleController;
        this.world = thrower.world;
        battleController.pauseBattle();
        this.setLocationAndAngles(thrower.posX, thrower.posY + (double)thrower.getEyeHeight(), thrower.posZ, thrower.rotationYaw, thrower.rotationPitch);
        this.posX -= (double)(MathHelper.cos(this.rotationYaw / 180.0f * (float)Math.PI) * 0.16f);
        this.posY -= (double)0.1f;
        this.posZ -= (double)(MathHelper.sin(this.rotationYaw / 180.0f * (float)Math.PI) * 0.16f);
        this.setPosition(this.posX, this.posY, this.posZ);
        this.motionX = (double)(-MathHelper.sin(this.rotationYaw / 180.0f * (float)Math.PI) * MathHelper.cos(this.rotationPitch / 180.0f * (float)Math.PI)) * 0.8;
        this.motionZ = (double)(MathHelper.cos(this.rotationYaw / 180.0f * (float)Math.PI) * MathHelper.cos(this.rotationPitch / 180.0f * (float)Math.PI)) * 0.8;
        this.motionY = (double)(-MathHelper.sin(0.0f)) * 0.8;
    }

    @Override
    protected void onImpact(RayTraceResult movingobjectposition) {
        if (this.world.isRemote) {
            return;
        }
        if (MinecraftForge.EVENT_BUS.post(new PokeballImpactEvent(this, movingobjectposition)) && !this.isBattleThrown) {
            return;
        }
        if (this.dropItem && this.breakChance == 1 && this.getType() != EnumPokeball.MasterBall) {
            this.world.playSound(null, this.posX, this.posY, this.posZ, SoundEvents.ENTITY_ITEM_BREAK, SoundCategory.NEUTRAL, 1.0f, 1.0f);
            this.entityDropItem(new ItemStack(Blocks.STONE_BUTTON), 0.0f);
            this.entityDropItem(new ItemStack(PixelmonItemsPokeballs.ironBase), 0.0f);
            this.entityDropItem(new ItemStack(this.breakBall()), 0.0f);
            this.setDead();
            return;
        }
        if (this.isBattleThrown && !this.world.isRemote) {
            if (!this.getIsWaiting()) {
                this.startBattleCapture();
            } else {
                this.startShake();
            }
        } else {
            if (movingobjectposition.typeOfHit == RayTraceResult.Type.BLOCK) {
                IBlockState state = this.world.getBlockState(movingobjectposition.getBlockPos());
                Material mat = state.getMaterial();
                if (!this.getIsWaiting() && mat.isSolid()) {
                    List<EntityPixelmon> pokemon = this.world.getEntities(EntityPixelmon.class, entity -> entity.getDistance(this) / 2.0f <= PixelmonConfig.ballSlackRadius);
                    if (PixelmonConfig.ballSlackRadius < 0.0f || !pokemon.isEmpty()) {
                        this.pixelmon = pokemon.get(0);
                        this.pokemonPosX = this.pixelmon.posX;
                        this.pokemonPosY = this.pixelmon.posY;
                        this.pokemonPosZ = this.pixelmon.posZ;
                        if (this.pixelmon.battleController != null) {
                            boolean inBattle = false;
                            if (this.pixelmon.battleController.checkValid()) {
                                ChatHandler.sendChat(this.thrower, "pixelmon.pokeballs.inbattle", new Object[0]);
                                inBattle = true;
                            } else if (this.pixelmon.hasNPCTrainer) {
                                this.pixelmon.setDead();
                                inBattle = true;
                            }
                            if (inBattle) {
                                ChatHandler.sendChat(this.thrower, "pixelmon.pokeballs.pokeinbattle", new Object[0]);
                                if (this.dropItem) {
                                    this.entityDropItem(new ItemStack(this.getType().getItem()), 0.0f);
                                }
                                this.setDead();
                                return;
                            }
                        }
                        if (this.pixelmon.getBossMode() != EnumBossMode.NotBoss) {
                            ChatHandler.sendChat(this.thrower, "pixelmon.pokeballs.boss", new Object[0]);
                            this.setDead();
                            EntityItem item = new EntityItem(this.world, this.posX, this.posY, this.posZ, new ItemStack(this.getType().getItem()));
                            this.world.spawnEntity(item);
                            return;
                        }
                        if (this.pixelmon.isTotem()) {
                            ChatHandler.sendChat(this.thrower, "pixelmon.pokeballs.totem", new Object[0]);
                            this.setDead();
                            EntityItem item = new EntityItem(this.world, this.posX, this.posY, this.posZ, new ItemStack(this.getType().getItem()));
                            this.world.spawnEntity(item);
                            return;
                        }
                        if (this.pixelmon.hasOwner() || this.pixelmon.getTrainer() != null) {
                            if (this.pixelmon.getOwner() == this.thrower) {
                                ChatHandler.sendChat(this.thrower, "pixelmon.pokeballs.alreadyown", new Object[0]);
                            } else {
                                ChatHandler.sendChat(this.thrower, "pixelmon.pokeballs.otherspokemon", new Object[0]);
                            }
                            if (this.dropItem) {
                                this.entityDropItem(new ItemStack(this.getType().getItem()), 0.0f);
                            }
                            this.setDead();
                            return;
                        }
                        if (this.pixelmon.hitByPokeball != null) {
                            return;
                        }
                        this.pixelmon.hitByPokeball = this;
                        if (!this.getIsWaiting()) {
                            this.startCapture();
                        }
                    } else {
                        if (this.dropItem) {
                            this.entityDropItem(new ItemStack(this.getType().getItem()), 0.0f);
                        }
                        this.setDead();
                        return;
                    }
                }
            }
            if (movingobjectposition.entityHit instanceof EntityPixelmon) {
                EntityItem item;
                this.pixelmon = (EntityPixelmon)movingobjectposition.entityHit;
                this.pokemonPosX = this.pixelmon.posX;
                this.pokemonPosY = this.pixelmon.posY;
                this.pokemonPosZ = this.pixelmon.posZ;
                if (this.pixelmon.battleController != null) {
                    boolean inBattle = false;
                    if (this.pixelmon.battleController.checkValid()) {
                        ChatHandler.sendChat(this.thrower, "pixelmon.pokeballs.inbattle", new Object[0]);
                        inBattle = true;
                    } else if (this.pixelmon.hasNPCTrainer) {
                        this.pixelmon.setDead();
                        inBattle = true;
                    }
                    if (inBattle) {
                        ChatHandler.sendChat(this.thrower, "pixelmon.pokeballs.pokeinbattle", new Object[0]);
                        if (this.dropItem) {
                            this.entityDropItem(new ItemStack(this.getType().getItem()), 0.0f);
                        }
                        this.setDead();
                        return;
                    }
                }
                if (this.pixelmon.getBossMode() != EnumBossMode.NotBoss) {
                    ChatHandler.sendChat(this.thrower, "pixelmon.pokeballs.boss", new Object[0]);
                    this.setDead();
                    item = new EntityItem(this.world, this.posX, this.posY, this.posZ, new ItemStack(this.getType().getItem()));
                    this.world.spawnEntity(item);
                    return;
                }
                if (this.pixelmon.isTotem()) {
                    ChatHandler.sendChat(this.thrower, "pixelmon.pokeballs.totem", new Object[0]);
                    this.setDead();
                    item = new EntityItem(this.world, this.posX, this.posY, this.posZ, new ItemStack(this.getType().getItem()));
                    this.world.spawnEntity(item);
                    return;
                }
                if (this.pixelmon.hasOwner() || this.pixelmon.getTrainer() != null) {
                    if (this.pixelmon.getOwner() == this.thrower) {
                        ChatHandler.sendChat(this.thrower, "pixelmon.pokeballs.alreadyown", new Object[0]);
                    } else {
                        ChatHandler.sendChat(this.thrower, "pixelmon.pokeballs.otherspokemon", new Object[0]);
                    }
                    if (this.dropItem) {
                        this.entityDropItem(new ItemStack(this.getType().getItem()), 0.0f);
                    }
                    this.setDead();
                    return;
                }
                if (this.pixelmon.hitByPokeball != null) {
                    return;
                }
                this.pixelmon.hitByPokeball = this;
                if (!this.getIsWaiting()) {
                    this.startCapture();
                }
            } else if (this.getIsWaiting()) {
                this.startShake();
            } else {
                if (this.dropItem) {
                    this.entityDropItem(new ItemStack(this.getType().getItem()), 0.0f);
                }
                this.setDead();
                return;
            }
        }
        super.onImpact(movingobjectposition);
    }

    private void startCapture() {
        this.doCaptureCalc(this.pixelmon);
        this.pixelmon.motionZ = 0.0;
        this.pixelmon.motionY = 0.0;
        this.pixelmon.motionX = 0.0;
        this.initialScale = this.pixelmon.getPixelmonScale();
        this.setAnimation("bounceOpen");
        this.setIsWaiting(true);
        this.setId(this.canCatch ? this.numShakes : -1 * this.numShakes);
        this.motionY = 0.0;
        this.motionX = 0.0;
        this.motionZ = 0.0;
        this.rotationPitch = 0.0f;
        int i = -2;
        while (!this.world.isAirBlock(new BlockPos((int)this.posX, (int)Math.ceil(this.posY) + i, (int)this.posZ))) {
            ++i;
        }
        this.posY = Math.ceil(this.posY) + (double)i + (double)0.1f;
    }

    private void startBattleCapture() {
        this.pokemonPosX = this.pixelmon.posX;
        this.pokemonPosY = this.pixelmon.posY;
        this.pokemonPosZ = this.pixelmon.posZ;
        this.pixelmon.hitByPokeball = this;
        this.startCapture();
        this.posX = (int)this.posX;
        this.posZ = (int)this.posZ;
    }

    private void forceBattleCapture() {
        this.startBattleCapture();
        this.pixelmon.unloadEntity();
        this.setIsOnGround(true);
    }

    private void startShake() {
        this.motionY = 0.0;
        this.motionX = 0.0;
        this.motionZ = 0.0;
        this.setIsOnGround(true);
    }

    @Override
    public void onUpdate() {
        if (this.world.isRemote) {
            super.onUpdate();
            return;
        }
        ++this.totalTime;
        if (this.getIsOnGround()) {
            ++this.waitTime;
        }
        if (this.capturedPokemon) {
            if (this.waitTime > 20) {
                this.storeCapture();
                this.setDead();
            }
        } else {
            this.getClass();
            if (this.waitTime > this.numShakes * 25) {
                this.catchPokemon();
                this.waitTime = 0;
            } else if (this.totalTime > 100 && this.isBattleThrown && !this.getIsWaiting() && !this.captureFinished) {
                this.forceBattleCapture();
            }
        }
        super.onUpdate();
    }

    private void storeCapture() {
        Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)this.thrower);
        if (optstorage.isPresent()) {
            PixelmonWrapper pw = this.pixelmon.getPixelmonWrapper();
            if (pw != null && this.getType() != EnumPokeball.HealBall) {
                this.pixelmon.setHealth(pw.getHealth());
            }
            this.pixelmon.setTamed(true);
            this.pixelmon.setOwnerId(this.thrower.getUniqueID());
            this.pixelmon.caughtBall = this.getType();
            this.pixelmon.clearAttackTarget();
            this.pixelmon.friendship.initFromCapture();
            PokeballTypeHelper.doAfterEffect(this.getType(), this.pixelmon);
            MinecraftForge.EVENT_BUS.post(new PixelmonReceivedEvent((EntityPlayerMP)this.thrower, ReceiveType.PokeBall, this.pixelmon));
            optstorage.get().addToParty(this.pixelmon);
            this.pixelmon.catchInPokeball();
            if (this.getMode() == EnumPokeBallMode.BATTLE) {
                PlayerParticipant p = (PlayerParticipant)this.battleController.getParticipantForEntity(this.thrower);
                Experience.awardExp(this.battleController.participants, p, this.pixelmon.getPixelmonWrapper());
                this.battleController.endBattle(EnumBattleEndCause.FORCE);
            }
        }
        this.setIsWaiting(false);
    }

    protected void catchPokemon() {
        this.captureFinished = true;
        if (this.canCatch) {
            CaptureEvent.SuccessfulCaptureEvent capEvent;
            if (this.getType() == EnumPokeball.LoveBall) {
                Calendar currentCalendar = Calendar.getInstance();
                int month = currentCalendar.get(2);
                int day = currentCalendar.get(5);
                if (month == 1 && day >= 14 && day <= 20 && this.pixelmon.hasForms()) {
                    for (IEnumForm form : EnumSpecies.formList.get(this.pixelmon.getSpecies())) {
                        if (form != EnumValentine.LOVED) continue;
                        this.pixelmon.setForm(form.getForm(), true);
                        break;
                    }
                }
            }
            if (MinecraftForge.EVENT_BUS.post(capEvent = new CaptureEvent.SuccessfulCaptureEvent((EntityPlayerMP)this.thrower, this.pixelmon, this))) {
                this.failCapture();
                return;
            }
            this.pixelmon = capEvent.getPokemon();
            this.pixelmon.originalTrainer = this.thrower.getName();
            this.pixelmon.originalTrainerUUID = this.thrower.getUniqueID().toString();
            TextComponentTranslation message = ChatHandler.getMessage("pixelmon.pokeballs.capture", this.pixelmon.getLocalizedName());
            if (this.pixelmon.battleController == null) {
                ChatHandler.sendChat(this.thrower, "pixelmon.pokeballs.capture", this.pixelmon.getLocalizedName());
            } else {
                PixelmonWrapper pw = this.pixelmon.getPixelmonWrapper();
                if (pw != null) {
                    pw.resetOnSwitch();
                    ItemHeld currentHeldItem = pw.getHeldItem();
                    if (currentHeldItem != NoItem.noItem && currentHeldItem != pw.getInitialHeldItem()) {
                        pw.enableReturnHeldItem();
                    }
                }
                this.pixelmon.battleController.sendToAll(message);
                Experience.awardExp(this.pixelmon.battleController.participants, this.pixelmon.getParticipant(), pw);
            }
            this.capturedPokemon = true;
            this.waitTime = 0;
            EntityPlayerMP player = (EntityPlayerMP)this.thrower;
            PixelmonAdvancements.throwCaptureTriggers(player, this.getType(), this.pixelmon);
            PixelmonAchievements.captureChieves(player, this.pixelmon);
            if (PixelmonConfig.catchingCharmMaxChance == 0 && RandomHelper.getRandomNumberBetween(1, PixelmonConfig.catchingCharmMaxChance) == 1) {
                DropQueryFactory.newInstance().customTitle(new TextComponentTranslation("pixelmon.pokeballs.catchcharm", new Object[0])).addDrop(DroppedItem.of(new ItemStack(PixelmonItems.catchingCharm))).addTarget(player).send();
                if (player.getServer() != null) {
                    ChatHandler.sendMessageToAllPlayers(player.getServer(), "[" + (Object)((Object)TextFormatting.DARK_PURPLE) + "Pixelmon" + (Object)((Object)TextFormatting.WHITE) + "] " + (Object)((Object)TextFormatting.GREEN) + player.getName() + " has found the rare " + (Object)((Object)TextFormatting.BLUE) + "Catching Charm!");
                }
            }
        } else {
            this.failCapture();
        }
    }

    private void failCapture() {
        MinecraftForge.EVENT_BUS.post(new CaptureEvent.FailedCaptureEvent((EntityPlayerMP)this.thrower, this.pixelmon, this));
        this.openAngle = -1.5707964f;
        this.waitTime = 0;
        this.setIsWaiting(false);
        this.pixelmon.setPosition(this.pokemonPosX, this.pokemonPosY, this.pokemonPosZ);
        this.pixelmon.hitByPokeball = null;
        if (this.world.getEntityByID(this.pixelmon.getEntityId()) != null) {
            this.pixelmon.unloadEntity();
        }
        this.pixelmon.isDead = false;
        try {
            this.world.spawnEntity(this.pixelmon);
        }
        catch (IllegalStateException illegalStateException) {
            // empty catch block
        }
        this.pixelmon.setPosition(this.pokemonPosX, this.pokemonPosY, this.pokemonPosZ);
        this.pixelmon.setPixelmonScale(this.initialScale);
        this.pixelmon.isDead = false;
        TextComponentTranslation message = ChatHandler.getMessage("pixelmon.pokeballs.brokefree", this.pixelmon.getLocalizedName());
        if (this.getMode() == EnumPokeBallMode.BATTLE && !this.battleController.battleEnded) {
            this.pixelmon.battleController = this.battleController;
            if (this.pixelmon.transformed) {
                this.pixelmon.updateTransformed();
            }
            this.battleController.sendToAll(message);
            this.battleController.endPause();
        } else {
            ChatHandler.sendChat(this.thrower, message);
        }
        this.setDead();
    }

    @Override
    public void setDead() {
        if (this.isBattleThrown && !this.captureFinished) {
            if (!this.getIsWaiting()) {
                this.forceBattleCapture();
            }
            this.catchPokemon();
            return;
        }
        if (this.capturedPokemon && this.getIsWaiting()) {
            this.storeCapture();
        }
        if (this.pixelmon != null && this.pixelmon.hitByPokeball == this) {
            this.pixelmon.hitByPokeball = null;
        }
        super.setDead();
    }
}

