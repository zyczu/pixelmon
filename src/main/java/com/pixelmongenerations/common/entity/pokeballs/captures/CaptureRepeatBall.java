/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pokeballs.captures;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureBase;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;

public class CaptureRepeatBall
extends CaptureBase {
    public CaptureRepeatBall() {
        super(EnumPokeball.RepeatBall);
    }

    @Override
    public double getBallBonus(EnumPokeball type, EntityPlayer thrower, PokemonLink px, EnumPokeBallMode mode) {
        Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)thrower);
        if (optstorage.isPresent()) {
            PlayerStorage storage = optstorage.get();
            return storage.pokedex.hasCaught(px.getBaseStats().nationalPokedexNumber) ? 3.0 : 1.0;
        }
        return 1.0;
    }
}

