/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pokeballs.captures;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureBase;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.biome.Biome;

public class CaptureSafariBall
extends CaptureBase {
    public CaptureSafariBall() {
        super(EnumPokeball.SafariBall);
    }

    @Override
    public double getBallBonus(EnumPokeball type, EntityPlayer thrower, PokemonLink px, EnumPokeBallMode mode) {
        Biome biome = thrower.world.getBiome(new BlockPos(MathHelper.floor(thrower.posX), 0, MathHelper.floor(thrower.posZ)));
        if (biome.getRegistryName().getPath().equals("plains") || biome.getRegistryName().getPath().equalsIgnoreCase("savanna")) {
            return 1.5;
        }
        return 1.0;
    }
}

