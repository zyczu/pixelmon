/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pokeballs.captures;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureBase;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import net.minecraft.entity.player.EntityPlayer;

public class CaptureLuxuryBall
extends CaptureBase {
    public CaptureLuxuryBall() {
        super(EnumPokeball.LuxuryBall);
    }

    @Override
    public double getBallBonus(EnumPokeball type, EntityPlayer thrower, PokemonLink px, EnumPokeBallMode mode) {
        return type.getBallBonus();
    }

    @Override
    public void doAfterEffect(EnumPokeball type, EntityPixelmon p) {
        p.friendship.captureLuxuryBall();
    }
}

