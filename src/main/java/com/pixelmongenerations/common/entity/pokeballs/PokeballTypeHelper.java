/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableList
 *  com.google.common.collect.ImmutableList$Builder
 */
package com.pixelmongenerations.common.entity.pokeballs;

import com.google.common.collect.ImmutableList;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureBase;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureBeastBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureDiveBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureDreamBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureDuskBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureFastBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureFriendBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureHealBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureHeavyBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureLevelBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureLoveBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureLureBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureLuxuryBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureMoonBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureNestBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureNetBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureQuickBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureRepeatBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureSafariBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureSportBall;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureTimerBall;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import java.util.List;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;

public class PokeballTypeHelper {
    public static final List<CaptureBase> captureList;

    public static double getBallBonus(EnumPokeball type, EntityLivingBase thrower, PokemonLink link, EnumPokeBallMode mode) {
        for (CaptureBase c : captureList) {
            if (c.pokeball != type) continue;
            return c.getBallBonus(type, (EntityPlayer)thrower, link, mode);
        }
        return type.getBallBonus();
    }

    public static void doAfterEffect(EnumPokeball type, EntityPixelmon p2) {
        captureList.stream().filter(c -> c.pokeball == type).forEach(c -> c.doAfterEffect(type, p2));
    }

    public static int modifyCaptureRate(EnumPokeball type, EnumSpecies pokemon, int captureRate) {
        for (CaptureBase c : captureList) {
            if (c.pokeball != type) continue;
            return c.modifyCaptureRate(pokemon, captureRate);
        }
        return captureRate;
    }

    static {
        ImmutableList.Builder builder = ImmutableList.builder();
        builder.add((Object)new CaptureLoveBall());
        builder.add((Object)new CaptureLevelBall());
        builder.add((Object)new CaptureMoonBall());
        builder.add((Object)new CaptureFriendBall());
        builder.add((Object)new CaptureSafariBall());
        builder.add((Object)new CaptureDiveBall());
        builder.add((Object)new CaptureDuskBall());
        builder.add((Object)new CaptureHealBall());
        builder.add((Object)new CaptureLuxuryBall());
        builder.add((Object)new CaptureNetBall());
        builder.add((Object)new CaptureNestBall());
        builder.add((Object)new CaptureHeavyBall());
        builder.add((Object)new CaptureSportBall());
        builder.add((Object)new CaptureQuickBall());
        builder.add((Object)new CaptureLureBall());
        builder.add((Object)new CaptureFastBall());
        builder.add((Object)new CaptureTimerBall());
        builder.add((Object)new CaptureRepeatBall());
        builder.add((Object)new CaptureBeastBall());
        builder.add((Object)new CaptureDreamBall());
        captureList = builder.build();
    }
}

