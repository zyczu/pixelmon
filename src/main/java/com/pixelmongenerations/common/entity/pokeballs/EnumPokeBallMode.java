/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pokeballs;

public enum EnumPokeBallMode {
    EMPTY,
    FULL,
    BATTLE;


    public static EnumPokeBallMode getFromOrdinal(Integer integer) {
        for (EnumPokeBallMode m : EnumPokeBallMode.values()) {
            if (m.ordinal() != integer.intValue()) continue;
            return m;
        }
        return null;
    }
}

