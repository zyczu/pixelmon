/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pokeballs.captures;

import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.common.entity.pokeballs.captures.CaptureBase;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import net.minecraft.entity.player.EntityPlayer;

public class CaptureHeavyBall
extends CaptureBase {
    public CaptureHeavyBall() {
        super(EnumPokeball.HeavyBall);
    }

    @Override
    public double getBallBonus(EnumPokeball type, EntityPlayer thrower, PokemonLink px, EnumPokeBallMode mode) {
        return type.getBallBonus();
    }

    @Override
    public int modifyCaptureRate(EnumSpecies pokemon, int captureRate) {
        float weight = Entity3HasStats.getBaseStats((EnumSpecies)pokemon).get().weight;
        captureRate = weight < 205.0f ? captureRate - 20 : (weight < 307.0f ? captureRate + 20 : ((double)weight < 409.5 ? captureRate + 30 : captureRate + 40));
        return captureRate;
    }
}

