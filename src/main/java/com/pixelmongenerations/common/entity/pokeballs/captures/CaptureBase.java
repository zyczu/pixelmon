/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pokeballs.captures;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import net.minecraft.entity.player.EntityPlayer;

public abstract class CaptureBase {
    public EnumPokeball pokeball;

    public CaptureBase(EnumPokeball pokeball) {
        this.pokeball = pokeball;
    }

    public abstract double getBallBonus(EnumPokeball var1, EntityPlayer var2, PokemonLink var3, EnumPokeBallMode var4);

    public void doAfterEffect(EnumPokeball type, EntityPixelmon p) {
    }

    public int modifyCaptureRate(EnumSpecies pokemon, int captureRate) {
        return captureRate;
    }
}

