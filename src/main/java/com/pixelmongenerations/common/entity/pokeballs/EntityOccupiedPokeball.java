/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity.pokeballs;

import com.pixelmongenerations.api.events.PokeballImpactEvent;
import com.pixelmongenerations.api.events.battles.BattleRequestEvent;
import com.pixelmongenerations.api.events.battles.PokeballSpeedEvent;
import com.pixelmongenerations.common.battle.BattleFactory;
import com.pixelmongenerations.common.battle.BattleQuery;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.TrainerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.battle.rules.teamselection.TeamSelectionList;
import com.pixelmongenerations.common.entity.npcs.NPCTrainer;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pokeballs.EntityPokeBall;
import com.pixelmongenerations.common.entity.pokeballs.EnumPokeBallMode;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class EntityOccupiedPokeball
extends EntityPokeBall {
    private static final int MAX_LIFETIME = 400;
    String pokeName = null;

    public EntityOccupiedPokeball(World world) {
        super(world);
        this.dataManager.set(dwMode, EnumPokeBallMode.FULL.ordinal());
    }

    public EntityOccupiedPokeball(World world, EntityLivingBase entityliving, int[] pokemonId, EnumPokeball type) {
        super(type, world, entityliving, EnumPokeBallMode.FULL);
        this.thrower = entityliving;
        this.setOwnerId(this.thrower.getUniqueID());
        this.endRotationYaw = entityliving.rotationYawHead;
        this.setPokeId(pokemonId);
        this.setLocationAndAngles(entityliving.posX, entityliving.posY + (double)entityliving.getEyeHeight(), entityliving.posZ, entityliving.rotationYaw, entityliving.rotationPitch);
        this.posX -= (double)(MathHelper.cos(this.rotationYaw / 180.0f * (float)Math.PI) * 0.16f);
        this.posY -= (double)0.1f;
        this.posZ -= (double)(MathHelper.sin(this.rotationYaw / 180.0f * (float)Math.PI) * 0.16f);
        this.setPosition(this.posX, this.posY, this.posZ);
        PokeballSpeedEvent event = new PokeballSpeedEvent(entityliving, type, 0.8f);
        MinecraftForge.EVENT_BUS.post(event);
        this.motionX = (double)(-MathHelper.sin(this.rotationYaw / 180.0f * (float)Math.PI) * MathHelper.cos(this.rotationPitch / 180.0f * (float)Math.PI)) * (double)event.getSpeed();
        this.motionZ = (double)(MathHelper.cos(this.rotationYaw / 180.0f * (float)Math.PI) * MathHelper.cos(this.rotationPitch / 180.0f * (float)Math.PI)) * (double)event.getSpeed();
        this.motionY = (double)(-MathHelper.sin(this.rotationPitch / 180.0f * (float)Math.PI)) * (double)event.getSpeed();
        this.setInitialYaw(this.thrower.rotationYaw);
        this.setInitialPitch(this.thrower.rotationPitch);
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        if (!(this.world.isRemote || this.ticksExisted <= 400 && this.posY >= 0.0)) {
            this.setDead();
        }
    }

    @Override
    public String getName() {
        Optional<PlayerStorage> optstorage;
        if (this.pokeName == null && this.thrower instanceof EntityPlayerMP && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)this.thrower)).isPresent()) {
            this.pokeName = I18n.translateToLocal("pixelmon." + optstorage.get().getNBT(this.getPokeId()).getString("Name").toLowerCase() + ".name");
        }
        return this.pokeName + " " + super.getName();
    }

    @Override
    protected void onImpact(RayTraceResult traceResult) {
        if (!this.world.isRemote) {
            BlockPos pos;
            IBlockState state;
            if (traceResult.typeOfHit == RayTraceResult.Type.BLOCK && !(state = this.world.getBlockState(pos = traceResult.getBlockPos())).isSideSolid(this.world, pos, traceResult.sideHit) && state.getCollisionBoundingBox(this.world, pos) == null) {
                return;
            }
            if (MinecraftForge.EVENT_BUS.post(new PokeballImpactEvent(this, traceResult))) {
                return;
            }
            if (traceResult.typeOfHit != RayTraceResult.Type.BLOCK) {
                PlayerStorage storage;
                EntityPixelmon pokemon;
                EntityPixelmon pokemonHit = null;
                if (traceResult.entityHit instanceof EntityPixelmon) {
                    pokemonHit = (EntityPixelmon)traceResult.entityHit;
                }
                if (pokemonHit != null && pokemonHit.getOwner() == this.thrower && pokemonHit.isBeingRidden() || traceResult.entityHit == this.thrower) {
                    return;
                }
                Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)this.thrower);
                if (optstorage.isPresent() && (pokemon = (storage = optstorage.get()).sendOut(this.getPokeId(), this.thrower.world)) != null) {
                    pokemon.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw, 0.0f);
                    if (pokemonHit != null && !storage.isIn(pokemonHit)) {
                        if (pokemonHit.battleController != null) {
                            if (pokemonHit.battleController.checkValid()) {
                                ChatHandler.sendChat(this.thrower, "pixelmon.pokeballs.inbattle", new Object[0]);
                                this.setDead();
                                return;
                            }
                            if (pokemonHit.hasNPCTrainer) {
                                pokemonHit.setDead();
                                this.setDead();
                                return;
                            }
                        }
                        if (pokemonHit.hitByPokeball != null) {
                            this.setDead();
                            return;
                        }
                        if (pokemonHit.hasOwner()) {
                            Optional<PlayerStorage> opttargetstorage = pokemonHit.getStorage();
                            PlayerStorage targetStorage = opttargetstorage.get();
                            if (pokemonHit.getOwner() == null) {
                                this.setDead();
                                return;
                            }
                            if (!targetStorage.battleEnabled || pokemonHit.blockOwner != null) {
                                this.setDead();
                                return;
                            }
                            BattleRequestEvent event = new BattleRequestEvent((EntityPlayerMP)this.thrower, pokemon, (EntityPlayerMP)pokemonHit.getOwner(), pokemonHit);
                            if (MinecraftForge.EVENT_BUS.post(event)) {
                                return;
                            }
                            if (targetStorage.guiOpened) {
                                ChatHandler.sendChat(this.thrower, "pixelmon.general.playerbusy", new Object[0]);
                                this.setDead();
                                return;
                            }
                            new BattleQuery((EntityPlayerMP)this.thrower, pokemon, (EntityPlayerMP)pokemonHit.getOwner(), pokemonHit);
                        } else {
                            if (pokemonHit.isInRanchBlock) {
                                this.setDead();
                                return;
                            }
                            if (BattleRegistry.getBattle((EntityPlayer)this.thrower) != null) {
                                this.setDead();
                                return;
                            }
                            WildPixelmonParticipant wildPokemon = new WildPixelmonParticipant(pokemonHit);
                            BattleFactory.createBattle().team1(new PlayerParticipant((EntityPlayerMP)this.thrower, pokemon)).team2(wildPokemon).startBattle();
                            this.setDead();
                        }
                    } else {
                        if (traceResult.entityHit instanceof NPCTrainer) {
                            EntityPlayerMP throwerPlayer;
                            NPCTrainer trainerEntity = (NPCTrainer)traceResult.entityHit;
                            BattleControllerBase bc = trainerEntity.getBattleController();
                            if (bc != null) {
                                if (bc.battleEnded) {
                                    bc.endBattleWithoutXP();
                                } else {
                                    this.setDead();
                                    return;
                                }
                            }
                            if (!trainerEntity.canStartBattle(throwerPlayer = (EntityPlayerMP)this.thrower, true)) {
                                return;
                            }
                            EnumBattleType battleType = EnumBattleType.Single;
                            if (traceResult.entityHit instanceof NPCTrainer) {
                                battleType = trainerEntity.getBattleType();
                            }
                            if (trainerEntity.battleRules.isDefault()) {
                                TrainerParticipant trainer = new TrainerParticipant(trainerEntity, (EntityPlayer)this.thrower, battleType.numPokemon);
                                PlayerParticipant playerPart = battleType == EnumBattleType.Single ? new PlayerParticipant(throwerPlayer, pokemon) : new PlayerParticipant(throwerPlayer, storage.getAmountAblePokemon(throwerPlayer.world, battleType.numPokemon));
                                BattleFactory.createBattle().team1(playerPart).team2(trainer).rules(trainerEntity.battleRules).startBattle();
                            } else {
                                TeamSelectionList.addTeamSelection(trainerEntity.battleRules, true, trainerEntity.getPokemonStorage(), PixelmonStorage.pokeBallManager.getPlayerStorage(throwerPlayer).get());
                            }
                            this.setDead();
                            return;
                        }
                        if (PixelmonConfig.pokeBallPlayerEngage && traceResult.entityHit instanceof EntityPlayerMP) {
                            if (traceResult.entityHit != this.thrower) {
                                EntityPlayerMP thrower = (EntityPlayerMP)this.getThrower();
                                EntityPlayerMP enemy = (EntityPlayerMP)traceResult.entityHit;
                                EntityPixelmon player1FirstPokemon = storage.getFirstAblePokemon(thrower.world);
                                Optional<PlayerStorage> optEnemyStorage = PixelmonStorage.pokeBallManager.getPlayerStorage(enemy);
                                if (optEnemyStorage.isPresent()) {
                                    PlayerStorage enemyStorage = optEnemyStorage.get();
                                    EntityPixelmon player2FirstPokemon = enemyStorage.getFirstAblePokemon(enemy.world);
                                    if (player2FirstPokemon == null) {
                                        ChatHandler.sendChat(thrower, "pixelmon.command.battle.nopokemon", enemy.getDisplayNameString());
                                        return;
                                    }
                                    BattleRequestEvent event = new BattleRequestEvent(thrower, player1FirstPokemon, enemy, player2FirstPokemon);
                                    if (MinecraftForge.EVENT_BUS.post(event)) {
                                        return;
                                    }
                                    if (enemyStorage.guiOpened) {
                                        ChatHandler.sendChat(thrower, "pixelmon.general.playerbusy", new Object[0]);
                                        return;
                                    }
                                    new BattleQuery(thrower, player1FirstPokemon, enemy, player2FirstPokemon);
                                }
                            }
                        } else {
                            pokemon.clearAttackTarget();
                        }
                    }
                }
            }
            if (this.getIsWaiting()) {
                this.motionY = 0.0;
                this.motionX = 0.0;
                this.motionZ = 0.0;
                this.setDead();
                this.setIsOnGround(true);
            } else {
                this.setAnimation("bounceOpen");
                this.setIsWaiting(true);
                this.motionY = 0.0;
                this.motionX = 0.0;
                this.motionZ = 0.0;
                this.rotationPitch = 0.0f;
            }
        }
    }
}

