/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.entity;

import com.pixelmongenerations.common.entity.SpawningEntity;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.SpawnColors;
import com.pixelmongenerations.core.util.SpawnMethodCooldowns;
import java.util.List;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.event.ForgeEventFactory;

public class EntityMagmaCrystal
extends EntityThrowable {
    private boolean alreadyDropped;

    public EntityMagmaCrystal(World world) {
        super(world);
    }

    public EntityMagmaCrystal(World worldIn, EntityPlayer playerIn) {
        super(worldIn, playerIn);
    }

    @Override
    public void onUpdate() {
        this.lastTickPosX = this.posX;
        this.lastTickPosY = this.posY;
        this.lastTickPosZ = this.posZ;
        if (this.throwableShake > 0) {
            --this.throwableShake;
        }
        if (this.inGround) {
            if (this.world.getBlockState(new BlockPos(this.xTile, this.yTile, this.zTile)).getBlock() == this.inTile) {
                ++this.ticksInGround;
                if (this.ticksInGround == 1200) {
                    this.setDead();
                }
                return;
            }
            this.inGround = false;
            this.motionX *= (double)(this.rand.nextFloat() * 0.2f);
            this.motionY *= (double)(this.rand.nextFloat() * 0.2f);
            this.motionZ *= (double)(this.rand.nextFloat() * 0.2f);
            this.ticksInGround = 0;
            this.ticksInAir = 0;
        } else {
            ++this.ticksInAir;
        }
        Vec3d vec3d = new Vec3d(this.posX, this.posY, this.posZ);
        Vec3d vec3d1 = new Vec3d(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
        RayTraceResult raytraceresult = this.world.rayTraceBlocks(vec3d, vec3d1, true);
        vec3d = new Vec3d(this.posX, this.posY, this.posZ);
        vec3d1 = new Vec3d(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
        if (raytraceresult != null) {
            vec3d1 = new Vec3d(raytraceresult.hitVec.x, raytraceresult.hitVec.y, raytraceresult.hitVec.z);
        }
        Entity entity = null;
        List<Entity> list = this.world.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(this.motionX, this.motionY, this.motionZ).grow(1.0));
        double d0 = 0.0;
        boolean flag = false;
        for (Entity entity1 : list) {
            double d1;
            if (!entity1.canBeCollidedWith()) continue;
            if (entity1 == this.ignoreEntity) {
                flag = true;
                continue;
            }
            if (this.thrower != null && this.ticksExisted < 2 && this.ignoreEntity == null) {
                this.ignoreEntity = entity1;
                flag = true;
                continue;
            }
            flag = false;
            AxisAlignedBB axisalignedbb = entity1.getEntityBoundingBox().grow(0.3f);
            RayTraceResult raytraceresult1 = axisalignedbb.calculateIntercept(vec3d, vec3d1);
            if (raytraceresult1 == null || !((d1 = vec3d.squareDistanceTo(raytraceresult1.hitVec)) < d0) && d0 != 0.0) continue;
            entity = entity1;
            d0 = d1;
        }
        if (this.ignoreEntity != null) {
            if (flag) {
                this.ignoreTime = 2;
            } else if (this.ignoreTime-- <= 0) {
                this.ignoreEntity = null;
            }
        }
        if (entity != null) {
            raytraceresult = new RayTraceResult(entity);
        }
        if (raytraceresult != null) {
            if (raytraceresult.typeOfHit == RayTraceResult.Type.BLOCK && this.world.getBlockState(raytraceresult.getBlockPos()).getBlock() == Blocks.PORTAL) {
                this.setPortal(raytraceresult.getBlockPos());
            } else if (!ForgeEventFactory.onProjectileImpact(this, raytraceresult) && !this.world.isRemote) {
                this.onImpact(raytraceresult);
            }
        }
        this.posX += this.motionX;
        this.posY += this.motionY;
        this.posZ += this.motionZ;
        this.rotationYaw = (float)(MathHelper.atan2(this.motionX, this.motionZ) * 57.29577951308232);
        while (this.rotationPitch - this.prevRotationPitch >= 180.0f) {
            this.prevRotationPitch += 360.0f;
        }
        while (this.rotationYaw - this.prevRotationYaw < -180.0f) {
            this.prevRotationYaw -= 360.0f;
        }
        while (this.rotationYaw - this.prevRotationYaw >= 180.0f) {
            this.prevRotationYaw += 360.0f;
        }
        this.rotationPitch = this.prevRotationPitch + (this.rotationPitch - this.prevRotationPitch) * 0.2f;
        this.rotationYaw = this.prevRotationYaw + (this.rotationYaw - this.prevRotationYaw) * 0.2f;
        float f1 = 0.99f;
        float f2 = this.getGravityVelocity();
        if (this.isInWater()) {
            for (int j = 0; j < 4; ++j) {
                this.world.spawnParticle(EnumParticleTypes.WATER_BUBBLE, this.posX - this.motionX * 0.25, this.posY - this.motionY * 0.25, this.posZ - this.motionZ * 0.25, this.motionX, this.motionY, this.motionZ, new int[0]);
            }
            f1 = 0.8f;
        }
        this.motionX *= (double)f1;
        this.motionY *= (double)f1;
        this.motionZ *= (double)f1;
        if (!this.hasNoGravity()) {
            this.motionY -= (double)f2;
        }
        this.setPosition(this.posX, this.posY, this.posZ);
    }

    @Override
    protected void onImpact(RayTraceResult result) {
        if (result.typeOfHit == RayTraceResult.Type.BLOCK && !this.world.isRemote) {
            IBlockState state = this.world.getBlockState(result.getBlockPos());
            Material mat = state.getMaterial();
            if (this.getThrower() instanceof EntityPlayerMP) {
                EntityPlayerMP player = (EntityPlayerMP)this.getThrower();
                SpawnMethodCooldowns.clearCooldown(player);
                if (mat == Material.LAVA || this.world.getBlockState(result.getBlockPos().add(0, 1, 0)).getMaterial() == Material.LAVA) {
                    this.world.spawnEntity(SpawningEntity.builder().location(result.hitVec).colors(SpawnColors.RED, SpawnColors.ORANGE, SpawnColors.GRAY).maxTick(100).pixelmon(EnumSpecies.Heatran).build(this.world, (EntityPlayerMP)this.getThrower()));
                    SpawnMethodCooldowns.addCooldown(player, 120);
                    this.setDead();
                } else if (!this.world.isRemote && mat != Material.AIR) {
                    player.addItemStackToInventory(new ItemStack(PixelmonItems.magmaCrystal));
                    this.setDead();
                }
            }
        }
    }
}

