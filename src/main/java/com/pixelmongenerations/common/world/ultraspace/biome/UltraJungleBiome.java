/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.ultraspace.biome;

import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.util.Random;
import net.minecraft.block.BlockLeaves;
import net.minecraft.block.BlockOldLeaf;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.BlockTallGrass;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.EntityOcelot;
import net.minecraft.entity.passive.EntityParrot;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.feature.WorldGenAbstractTree;
import net.minecraft.world.gen.feature.WorldGenMegaJungle;
import net.minecraft.world.gen.feature.WorldGenMelon;
import net.minecraft.world.gen.feature.WorldGenShrub;
import net.minecraft.world.gen.feature.WorldGenTallGrass;
import net.minecraft.world.gen.feature.WorldGenTrees;
import net.minecraft.world.gen.feature.WorldGenVines;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.event.terraingen.DecorateBiomeEvent;
import net.minecraftforge.event.terraingen.TerrainGen;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class UltraJungleBiome
extends Biome {
    private static int[] foliageBuffer = new int[65536];
    private final boolean isEdge;
    private static final IBlockState JUNGLE_LOG = PixelmonBlocks.ultraJungleLog.getDefaultState();
    private static final IBlockState JUNGLE_LEAF = Blocks.LEAVES.getDefaultState().withProperty(BlockOldLeaf.VARIANT, BlockPlanks.EnumType.JUNGLE).withProperty(BlockLeaves.CHECK_DECAY, false);
    private static final IBlockState OAK_LEAF = Blocks.LEAVES.getDefaultState().withProperty(BlockOldLeaf.VARIANT, BlockPlanks.EnumType.OAK).withProperty(BlockLeaves.CHECK_DECAY, false);

    public UltraJungleBiome(boolean isEdgeIn, Biome.BiomeProperties properties) {
        super(properties);
        this.isEdge = isEdgeIn;
        this.decorator.treesPerChunk = isEdgeIn ? 2 : 50;
        this.decorator.grassPerChunk = 25;
        this.decorator.flowersPerChunk = 4;
        if (!isEdgeIn) {
            this.spawnableMonsterList.add(new Biome.SpawnListEntry(EntityOcelot.class, 2, 1, 1));
        }
        this.spawnableCreatureList.add(new Biome.SpawnListEntry(EntityParrot.class, 40, 1, 2));
        this.spawnableCreatureList.add(new Biome.SpawnListEntry(EntityChicken.class, 10, 4, 4));
    }

    @Override
    public WorldGenAbstractTree getRandomTreeFeature(Random rand) {
        if (rand.nextInt(10) == 0) {
            return BIG_TREE_FEATURE;
        }
        if (rand.nextInt(2) == 0) {
            return new WorldGenShrub(JUNGLE_LOG, OAK_LEAF);
        }
        return !this.isEdge && rand.nextInt(3) == 0 ? new WorldGenMegaJungle(false, 10, 20, JUNGLE_LOG, JUNGLE_LEAF) : new WorldGenTrees(false, 4 + rand.nextInt(7), JUNGLE_LOG, JUNGLE_LEAF, true);
    }

    @Override
    public WorldGenerator getRandomWorldGenForGrass(Random rand) {
        return rand.nextInt(4) == 0 ? new WorldGenTallGrass(BlockTallGrass.EnumType.FERN) : new WorldGenTallGrass(BlockTallGrass.EnumType.GRASS);
    }

    @Override
    public void decorate(World worldIn, Random rand, BlockPos pos) {
        int k;
        super.decorate(worldIn, rand, pos);
        int i = rand.nextInt(16) + 8;
        int j = rand.nextInt(16) + 8;
        int height = worldIn.getHeight(pos.add(i, 0, j)).getY() * 2;
        if (height < 1) {
            height = 1;
        }
        if (TerrainGen.decorate(worldIn, rand, new ChunkPos(pos), pos.add(i, k = rand.nextInt(height), j), DecorateBiomeEvent.Decorate.EventType.PUMPKIN)) {
            new WorldGenMelon().generate(worldIn, rand, pos.add(i, k, j));
        }
        WorldGenVines worldgenvines = new WorldGenVines();
        if (TerrainGen.decorate(worldIn, rand, new ChunkPos(pos), DecorateBiomeEvent.Decorate.EventType.GRASS)) {
            for (int j1 = 0; j1 < 50; ++j1) {
                k = rand.nextInt(16) + 8;
                int l = 128;
                int i1 = rand.nextInt(16) + 8;
                worldgenvines.generate(worldIn, rand, pos.add(k, 128, i1));
            }
        }
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public int getFoliageColorAtPos(BlockPos pos) {
        double d0 = 0.3f;
        double d1 = 0.9f;
        return this.getModdedBiomeFoliageColor(16711780);
    }

    @SideOnly(value=Side.CLIENT)
    public static int getFoliageColor(double temperature, double humidity) {
        humidity *= temperature;
        int i = 255;
        int j = 0;
        return foliageBuffer[j << 8 | i];
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public int getGrassColorAtPos(BlockPos pos) {
        return this.getModdedBiomeGrassColor(0xFF6699);
    }
}

