/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.ultraspace;

import com.pixelmongenerations.common.world.ModWorldGen;
import com.pixelmongenerations.common.world.ultraspace.UltraSpaceBiomeProvider;
import com.pixelmongenerations.common.world.ultraspace.UltraSpaceChunkGenerator;
import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.BiomeProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class UltraSpaceWorldType
extends WorldType {
    public UltraSpaceWorldType() {
        super(ModWorldGen.ULTRA_SPACE_NAME);
    }

    @Override
    public BiomeProvider getBiomeProvider(World world) {
        return new UltraSpaceBiomeProvider(world.getWorldInfo());
    }

    @Override
    public IChunkGenerator getChunkGenerator(World world, String generatorOptions) {
        return new UltraSpaceChunkGenerator(world, generatorOptions);
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public boolean canBeCreated() {
        return false;
    }
}

