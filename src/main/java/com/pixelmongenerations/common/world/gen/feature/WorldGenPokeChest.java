/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.world.gen.feature;

import com.google.common.collect.Lists;
import com.pixelmongenerations.common.block.enums.EnumPokechestVisibility;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPokeChest;
import com.pixelmongenerations.common.world.ModBiomes;
import com.pixelmongenerations.core.config.BetterSpawnerConfig;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.util.helper.WorldHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenPokeChest
implements IWorldGenerator {
    private static int lastChunk = 0;
    private static int lastX;
    private static int lastZ;
    private static int totalPlaced;
    private static int normalPlaced;
    private static int ultraPlaced;
    Block underBlock;
    private static List<Biome> goodBiomes;
    private static List<Biome> goodBiomesUltraSpace;

    public static void init() {
        if (PixelmonConfig.pokeChestBiomeWhiteList.contains("all")) {
            Biome.REGISTRY.iterator().forEachRemaining(goodBiomes::add);
            goodBiomes.removeIf(goodBiomesUltraSpace::contains);
        } else {
            goodBiomes = PixelmonConfig.pokeChestBiomeWhiteList.stream().flatMap(BetterSpawnerConfig::processBiomeIdToStream).map(ResourceLocation::new).map(WorldHelper::demandBiome).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
        }
        PixelmonConfig.pokeChestBiomeBlackList.stream().flatMap(BetterSpawnerConfig::processBiomeIdToStream).map(ResourceLocation::new).map(WorldHelper::demandBiome).filter(Optional::isPresent).map(Optional::get).forEach(goodBiomes::remove);
    }

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        if (PixelmonConfig.spawnNormal || PixelmonConfig.spawnHidden) {
            this.genBlocks(random, chunkX, chunkZ, world);
        }
    }

    private void genBlocks(Random random, int chunkX, int chunkZ, World world) {
        int dimensionID = world.provider.getDimension();
        if (dimensionID == -1 || dimensionID == 1) {
            return;
        }
        int x = random.nextInt(16) + chunkX * 16;
        int z = random.nextInt(16) + chunkZ * 16;
        int y = world.getHeight(new BlockPos(x, 0, z)).getY();
        int distance = (int)Math.sqrt(Math.pow(lastX - x, 2.0) + Math.pow(lastZ - z, 2.0));
        if (distance >= PixelmonConfig.spawnRate.getMinDistance() && lastChunk % PixelmonConfig.spawnRate.getMinChunk() == 0) {
            IBlockState underBlockState = world.getBlockState(new BlockPos(x, y - 1, z));
            this.underBlock = underBlockState.getBlock();
            Biome biome = world.getBiome(new BlockPos(chunkX * 16, 0, chunkZ * 16));
            while (this.underBlock == Blocks.LEAVES || this.underBlock == Blocks.LOG || underBlockState.getMaterial() == Material.AIR) {
                if (--y <= 0) {
                    return;
                }
                try {
                    underBlockState = world.getBlockState(new BlockPos(x, y - 1, z));
                    this.underBlock = underBlockState.getBlock();
                }
                catch (Exception e) {
                    return;
                }
            }
            if (WorldGenPokeChest.isGoodBiome(biome) && underBlockState.getMaterial() != Material.LAVA && underBlockState.getMaterial() != Material.WATER) {
                Block block = PixelmonBlocks.pokeChest;
                if (normalPlaced > 4) {
                    if (ultraPlaced > 4) {
                        block = PixelmonBlocks.masterChest;
                        ultraPlaced = 0;
                    } else {
                        block = PixelmonBlocks.ultraChest;
                        ++ultraPlaced;
                    }
                    normalPlaced = 0;
                } else {
                    ++normalPlaced;
                }
                BlockPos currentPos = new BlockPos(x, y, z);
                if (PixelmonConfig.spawnHidden && totalPlaced % PixelmonConfig.spawnRate.getHidden_frequency() == 0 && totalPlaced != 0 && block == PixelmonBlocks.pokeChest) {
                    world.setBlockState(currentPos, PixelmonBlocks.pokeChest.getDefaultState());
                    TileEntity chestEntity = world.getTileEntity(currentPos);
                    if (chestEntity == null) {
                        return;
                    }
                    ((TileEntityPokeChest)chestEntity).setVisibility(EnumPokechestVisibility.Hidden);
                } else if (PixelmonConfig.spawnNormal) {
                    world.setBlockState(currentPos, block.getDefaultState());
                } else if (PixelmonConfig.spawnHidden) {
                    world.setBlockState(currentPos, PixelmonBlocks.pokeChest.getDefaultState());
                    ((TileEntityPokeChest)world.getTileEntity(currentPos)).setVisibility(EnumPokechestVisibility.Hidden);
                }
                TileEntity t0 = world.getTileEntity(currentPos);
                if (t0 instanceof TileEntityPokeChest) {
                    TileEntityPokeChest t = (TileEntityPokeChest)t0;
                    t.setChestOneTime(PixelmonConfig.spawnMode.isOneTimeUse());
                    t.setDropOneTime(PixelmonConfig.spawnMode.isOncePerPlayer());
                    t.setTimeEnabled(PixelmonConfig.spawnMode.isTimeEnabled());
                    lastX = x;
                    lastZ = z;
                    ++totalPlaced;
                } else {
                    world.setBlockToAir(currentPos);
                }
            } else if (WorldGenPokeChest.isGoodBiomeUltraSpace(biome) && underBlockState.getMaterial() != Material.LAVA && underBlockState.getMaterial() != Material.WATER) {
                BlockPos currentPos = new BlockPos(x, y, z);
                if (PixelmonConfig.spawnHidden) {
                    world.setBlockState(currentPos, PixelmonBlocks.beastChest.getDefaultState());
                    world.getTileEntity(currentPos);
                } else if (PixelmonConfig.spawnNormal) {
                    world.setBlockState(currentPos, PixelmonBlocks.beastChest.getDefaultState());
                }
                TileEntity t0 = world.getTileEntity(currentPos);
                if (t0 instanceof TileEntityPokeChest) {
                    TileEntityPokeChest t = (TileEntityPokeChest)t0;
                    t.setChestOneTime(PixelmonConfig.spawnMode.isOneTimeUse());
                    t.setDropOneTime(PixelmonConfig.spawnMode.isOncePerPlayer());
                    t.setTimeEnabled(PixelmonConfig.spawnMode.isTimeEnabled());
                    lastX = x;
                    lastZ = z;
                    ++totalPlaced;
                } else {
                    world.setBlockToAir(currentPos);
                }
            } else {
                --lastChunk;
            }
        }
        ++lastChunk;
    }

    public static boolean isGoodBiome(Biome biome) {
        return goodBiomes.contains(biome);
    }

    public static boolean isGoodBiomeUltraSpace(Biome biome) {
        return goodBiomesUltraSpace.contains(biome);
    }

    static {
        goodBiomes = new ArrayList<Biome>();
        goodBiomesUltraSpace = Lists.newArrayList(ModBiomes.ultraburst, ModBiomes.ultradarkforest, ModBiomes.ultradeepsea, ModBiomes.ultradesert, ModBiomes.ultraforest, ModBiomes.ultrajungle, ModBiomes.ultraplant, ModBiomes.ultraruin, ModBiomes.ultraswamp);
        lastZ = 0;
        totalPlaced = 0;
        normalPlaced = 0;
        ultraPlaced = 0;
    }
}

