/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.structure.towns;

import com.pixelmongenerations.common.world.gen.structure.StructureInfo;
import com.pixelmongenerations.common.world.gen.structure.towns.ComponentTownPart;
import java.util.List;
import net.minecraft.world.gen.structure.StructureComponent;

public class TownStructureInfo
extends StructureInfo {
    int weighting = 0;
    int maxNum = 0;

    public void setWeighting(int weighting) {
        this.weighting = weighting;
    }

    public int getWeight() {
        return this.weighting;
    }

    public void setMaxNum(int maxNum) {
        this.maxNum = maxNum;
    }

    public boolean canPlaceMore(List<StructureComponent> pieces) {
        return this.getCount(pieces) < this.maxNum;
    }

    private int getCount(List<StructureComponent> pieces) {
        return (int)pieces.stream().filter(obj -> obj instanceof ComponentTownPart && this.id.equals(((ComponentTownPart)obj).info.id)).count();
    }
}

