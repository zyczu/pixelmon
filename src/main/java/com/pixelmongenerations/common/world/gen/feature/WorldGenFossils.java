/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.feature;

import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.util.Random;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenFossils
implements IWorldGenerator {
    public boolean generate(World world, int x, int y, int z) {
        BlockPos pos = new BlockPos(x, y, z);
        if (world.getBlockState(pos).getBlock() == Blocks.GRAVEL) {
            world.setBlockState(pos, PixelmonBlocks.fossil.getDefaultState());
            return true;
        }
        return false;
    }

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        for (int i = 0; i < 6; ++i) {
            int xPos = random.nextInt(16) + chunkX * 16;
            int zPos = random.nextInt(16) + chunkZ * 16;
            int yPos = random.nextInt(30) + 20;
            if (this.generate(world, xPos, yPos - random.nextInt(9), zPos)) break;
        }
    }
}

