/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.feature;

import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenLeafStoneOre
implements IWorldGenerator {
    public boolean generate(World world, int x, int y, int z) {
        world.setBlockState(new BlockPos(x, y, z), PixelmonBlocks.leafStoneOre.getDefaultState());
        return true;
    }

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        if (!world.provider.isSurfaceWorld()) {
            return;
        }
        if (BiomeDictionary.getTypes(world.getBiome(new BlockPos(chunkX * 16, 0, chunkZ * 16))).contains(BiomeDictionary.Type.FOREST)) {
            for (int i = 0; i < random.nextInt(3) + 1; ++i) {
                int z;
                int y;
                int x = random.nextInt(16) + chunkX * 16;
                IBlockState block = world.getBlockState(new BlockPos(x, (y = world.getHeight(new BlockPos(x, 0, z = random.nextInt(16) + chunkZ * 16)).getY()) - 1, z));
                Material blockMaterial = block.getMaterial();
                if (blockMaterial != Material.LEAVES) continue;
                while (blockMaterial != Material.ROCK && y > 0) {
                    block = world.getBlockState(new BlockPos(x, --y - 1, z));
                    blockMaterial = block.getMaterial();
                }
                if (y <= 0) continue;
                this.generate(world, x, y - 1, z);
            }
        }
    }
}

