/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.BiMap
 *  com.google.common.collect.HashBiMap
 *  com.google.common.collect.Lists
 *  com.google.common.collect.Maps
 */
package com.pixelmongenerations.common.world.gen.structure.util;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.pixelmongenerations.common.world.Schematic;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.util.PixelBlockSnapshot;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;

public class StructureSnapshot {
    private final int width;
    private final int hight;
    private final int lenght;
    private List<NBTTagCompound> statues = Lists.newArrayList();
    private Map<Vec3i, NBTTagCompound> tileEntityData = Maps.newHashMap();
    private BiMap<BlockState, Integer> idToBlock = HashBiMap.create((int)32);
    private int[][][] ids;

    public StructureSnapshot(int width, int hight, int lenght) {
        this.width = width;
        this.hight = hight;
        this.lenght = lenght;
        this.ids = new int[width][hight][lenght];
    }

    public int getWidth() {
        return this.width;
    }

    public int getHight() {
        return this.hight;
    }

    public int getLenght() {
        return this.lenght;
    }

    public PixelBlockSnapshot getBlockAt(int x, int y, int z) {
        int id = this.ids[x][y][z];
        BlockPos pos = new BlockPos(x, y, z);
        BlockState state = (BlockState)this.idToBlock.inverse().get((Object)id);
        NBTTagCompound nbt = this.tileEntityData.getOrDefault(pos, null);
        return new PixelBlockSnapshot(new BlockPos(x, y, z), state.getBlockState(), nbt);
    }

    public List<NBTTagCompound> getStatues() {
        return this.statues;
    }

    public static StructureSnapshot readFromNBT(NBTTagCompound compound) {
        if (compound.hasKey("formatVersion") && compound.getByte("formatVersion") == 0) {
            return StructureSnapshot.loadOldFormat(compound);
        }
        if (compound.getInteger("Version") == 1) {
            return StructureSnapshot.loadSchematic(compound);
        }
        return StructureSnapshot.loadOldFormat(compound);
    }

    private static StructureSnapshot loadSchematic(NBTTagCompound compound) {
        Schematic schematic = Schematic.loadFromNBT(compound);
        StructureSnapshot snapshot = new StructureSnapshot(schematic.width, schematic.height, schematic.length);
        schematic.tileEntities.forEach(tileEntityNBT -> snapshot.tileEntityData.put(StructureSnapshot.getPosition(tileEntityNBT), (NBTTagCompound)tileEntityNBT));
        int air_id = -1;
        for (int i = 0; i < schematic.palette.size(); ++i) {
            IBlockState state = schematic.palette.get(i);
            if (state.getBlock() == Blocks.AIR) {
                air_id = i;
            }
            snapshot.idToBlock.put(new BlockState(state.getBlock(), state.getBlock().getMetaFromState(state)), i);
        }
        for (int y = 0; y < schematic.height; ++y) {
            for (int x = 0; x < schematic.width; ++x) {
                for (int z = 0; z < schematic.length; ++z) {
                    if (schematic.blockData[x][y][z] == air_id) continue;
                    snapshot.ids[x][y][z] = schematic.blockData[x][y][z];
                }
            }
        }
        snapshot.statues = schematic.entities;
        return snapshot;
    }

    private static Vec3i getPosition(NBTTagCompound tileEntityNBT) {
        return new Vec3i(tileEntityNBT.getInteger("x"), tileEntityNBT.getInteger("y"), tileEntityNBT.getInteger("z"));
    }

    private static StructureSnapshot loadOldFormat(NBTTagCompound compound) {
        int width = compound.getInteger("Width");
        int height = compound.getInteger("Height");
        int length = compound.getInteger("Length");
        StructureSnapshot snapshot = new StructureSnapshot(width, height, length);
        int nextId = 1;
        snapshot.idToBlock.put(new BlockState(Blocks.AIR, 0), 0);
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                for (int z = 0; z < length; ++z) {
                    NBTTagCompound blockTag = (NBTTagCompound)compound.getTag("X" + x + "Y" + y + "Z" + z);
                    if (blockTag.getBoolean("hasTE")) {
                        snapshot.tileEntityData.put(new Vec3i(x, y, z), blockTag.getCompoundTag("tileEntity"));
                    }
                    String domain = blockTag.hasKey("blockMod") ? blockTag.getString("blockMod") : "minecraft";
                    String resource = blockTag.getString("blockName");
                    int meta = blockTag.getInteger("metadata");
                    Block block = Block.REGISTRY.getObject(new ResourceLocation(domain, resource));
                    if (block == Blocks.AIR) continue;
                    BlockState state = new BlockState(block, meta);
                    if (!snapshot.idToBlock.containsKey(state)) {
                        snapshot.idToBlock.put(state, nextId++);
                    }
                    snapshot.ids[x][y][z] = snapshot.idToBlock.get(state);
                }
            }
        }
        int numStatues = compound.getInteger("numStatues");
        if (numStatues > 0) {
            float baseX = compound.getInteger("baseX");
            float baseY = compound.getInteger("baseY");
            float baseZ = compound.getInteger("baseZ");
            for (int i = 0; i < numStatues; ++i) {
                NBTTagCompound statue = compound.getCompoundTag("statue" + i);
                NBTTagList pos = statue.getTagList("Pos", 6);
                double posX = pos.getDoubleAt(0);
                double posY = pos.getDoubleAt(1);
                double posZ = pos.getDoubleAt(2);
                pos.set(0, new NBTTagDouble(posX - (double)baseX));
                pos.set(1, new NBTTagDouble(posY - (double)baseY));
                pos.set(2, new NBTTagDouble(posZ - (double)baseZ));
                snapshot.statues.add(statue);
            }
        }
        return snapshot;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static StructureSnapshot load(String type, String id) {
        String schematicJarDirectory = String.format("/assets/pixelmon/structures/%s/", type);
        InputStream schematicStream = Pixelmon.class.getResourceAsStream(schematicJarDirectory + id + ".schem");
        DataInputStream schematicDataStream = null;
        boolean streamOpened = false;
        if (schematicStream != null) {
            schematicDataStream = new DataInputStream(schematicStream);
            streamOpened = true;
        } else {
            Pixelmon.LOGGER.warn("Schematic '" + id + "' was not found in the jar or config directory, neither with the .schem extension, nor with the .schematic extension. ");
        }
        StructureSnapshot schematic = null;
        if (streamOpened) {
            try {
                NBTTagCompound schematicNBT = CompressedStreamTools.readCompressed(schematicDataStream);
                schematic = StructureSnapshot.readFromNBT(schematicNBT);
                schematicDataStream.close();
            }
            catch (IOException ex) {
                ex.printStackTrace();
                Pixelmon.LOGGER.error("Schematic file for " + id + " could not be read as a valid schematic NBT file.", (Throwable)ex);
            }
            finally {
                try {
                    schematicDataStream.close();
                }
                catch (IOException ex) {
                    Pixelmon.LOGGER.error("Error occured while closing schematicDataStream", (Throwable)ex);
                }
            }
        }
        return schematic;
    }

    private static class BlockState {
        Block block;
        int meta;

        public BlockState(Block block, int meta) {
            this.block = block;
            this.meta = meta;
        }

        public BlockState(IBlockState state) {
            this.block = state.getBlock();
            this.meta = this.block.getMetaFromState(state);
        }

        public IBlockState getBlockState() {
            return this.block.getStateFromMeta(this.meta);
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || this.getClass() != o.getClass()) {
                return false;
            }
            BlockState state = (BlockState)o;
            return this.meta == state.meta && Objects.equals(this.block, state.block);
        }

        public int hashCode() {
            return Objects.hash(this.block, this.meta);
        }
    }
}

