/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 *  org.apache.logging.log4j.Level
 */
package com.pixelmongenerations.common.world.gen.structure.util;

import com.pixelmongenerations.api.events.PixelmonStructurePlacedEvent;
import com.pixelmongenerations.common.entity.pixelmon.EntityStatue;
import com.pixelmongenerations.common.world.Schematic;
import com.pixelmongenerations.common.world.gen.structure.StructureInfo;
import com.pixelmongenerations.common.world.gen.structure.util.StructureScattered;
import com.pixelmongenerations.core.Pixelmon;
import java.lang.invoke.LambdaMetafactory;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import javax.annotation.Nullable;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.structure.StructureBoundingBox;
import net.minecraft.world.gen.structure.template.TemplateManager;
import net.minecraftforge.common.MinecraftForge;
import org.apache.logging.log4j.Level;

public class GeneralScattered
extends StructureScattered {
    protected StructureInfo si;
    private boolean forceGeneration;
    protected Schematic schematic;

    public GeneralScattered(Random par1Random, BlockPos pos, Schematic snapshot, StructureInfo structureData, boolean doRotation, boolean forceGeneration) {
        this(par1Random, pos, snapshot, structureData, doRotation, forceGeneration, null);
    }

    public GeneralScattered(Random par1Random, BlockPos pos, Schematic snapshot, StructureInfo structureData, boolean doRotation, boolean forceGeneration, EnumFacing facing) {
        super(par1Random, pos, snapshot.width, snapshot.height, snapshot.length, doRotation);
        this.schematic = snapshot;
        this.forceGeneration = forceGeneration;
        this.si = structureData;
        if (facing == null) {
            this.setCoordBaseMode(EnumFacing.random(par1Random));
            while (this.getCoordBaseMode().getAxis() == EnumFacing.Axis.Y) {
                this.setCoordBaseMode(EnumFacing.random(par1Random));
            }
        } else {
            this.setCoordBaseMode(facing);
        }
        short width = snapshot.width;
        short height = snapshot.height;
        short length = snapshot.length;
        switch (this.getCoordBaseMode()) {
            case EAST: 
            case WEST: {
                this.boundingBox = new StructureBoundingBox(pos.getX(), pos.getY(), pos.getZ(), pos.getX() + width - 1, pos.getY() + height - 1, pos.getZ() + length - 1);
                break;
            }
            default: {
                this.boundingBox = new StructureBoundingBox(pos.getX(), pos.getY(), pos.getZ(), pos.getX() + length - 1, pos.getY() + height - 1, pos.getZ() + width - 1);
            }
        }
    }

    @Override
    public boolean addComponentParts(World world, Random par2Random, StructureBoundingBox bb) {
        if (!this.forceGeneration && !this.canStructureFitAtCoords(world)) {
            return false;
        }
        int width = this.schematic.width;
        int height = this.schematic.height;
        int length = this.schematic.length;
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                for (int z = 0; z < length; ++z) {
                    IBlockState state = this.schematic.getBlockState(x, y, z);
                    if (Schematic.shouldPlace(state)) continue;
                    this.setBlockState(world, state, x, y, z, this.boundingBox);
                }
            }
        }
        for (NBTTagCompound statueTag : this.schematic.entities) {
            EntityStatue statue = new EntityStatue(world);
            statue.readFromNBT(statueTag);
            double posX = statue.posX;
            double posY = statue.posY;
            double posZ = statue.posZ;
            double x2 = this.getXWithOffsetDouble(posX, posZ);
            double y2 = this.getYWithOffsetDouble(posY);
            double z2 = this.getZWithOffsetDouble(posX, posZ);
            if (this.getCoordBaseMode() != EnumFacing.EAST) {
                if (this.getCoordBaseMode() == EnumFacing.WEST) {
                    statue.setRotation(statue.getRotation() + 180.0f);
                } else if (this.getCoordBaseMode() == EnumFacing.NORTH) {
                    statue.setRotation(statue.getRotation() - 90.0f);
                } else if (this.getCoordBaseMode() == EnumFacing.SOUTH) {
                    statue.setRotation(statue.getRotation() + 90.0f);
                }
                statue.prevRotationYaw = statue.rotationYaw;
            }
            statue.setPosition(x2, y2, z2);
            statue.setUniqueId(UUID.randomUUID());
            world.spawnEntity(statue);
        }
        Pixelmon.LOGGER.log(Level.INFO, String.format("%s has generated at X: %s Y: %s Z: %s", this.si.id, this.boundingBox.minX, this.boundingBox.minY, this.boundingBox.minZ));
        MinecraftForge.EVENT_BUS.post(new PixelmonStructurePlacedEvent(this, world, this.boundingBox));
        return true;
    }

    @Override
    public int getX(int x, int z) {
        return this.getXWithOffset(x, z);
    }

    @Override
    public int getY(int y) {
        return this.getYWithOffset(y);
    }

    @Override
    public int getZ(int x, int z) {
        return this.getZWithOffset(x, z);
    }

    @Override
    protected int getXWithOffset(int x, int z) {
        if (this.getCoordBaseMode() == null) {
            return x;
        }
        switch (this.getCoordBaseMode()) {
            case EAST: {
                return this.boundingBox.minX + x;
            }
            case WEST: {
                return this.boundingBox.maxX - x;
            }
            case NORTH: {
                return this.boundingBox.minX + z;
            }
            case SOUTH: {
                return this.boundingBox.maxX - z;
            }
        }
        return x;
    }

    @Override
    protected int getZWithOffset(int x, int z) {
        if (this.getCoordBaseMode() == null) {
            return z;
        }
        switch (this.getCoordBaseMode()) {
            case EAST: {
                return this.boundingBox.minZ + z;
            }
            case WEST: {
                return this.boundingBox.maxZ - z;
            }
            case NORTH: {
                return this.boundingBox.maxZ - x;
            }
            case SOUTH: {
                return this.boundingBox.minZ + x;
            }
        }
        return z;
    }

    protected double getXWithOffsetDouble(double x, double z) {
        if (this.getCoordBaseMode() == null) {
            return x;
        }
        switch (this.getCoordBaseMode()) {
            case EAST: {
                return (double)this.boundingBox.minX + x;
            }
            case WEST: {
                return (double)this.boundingBox.maxX - x + x % 1.0 * 2.0;
            }
            case NORTH: {
                return (double)this.boundingBox.minX + z;
            }
            case SOUTH: {
                return (double)this.boundingBox.maxX - z + z % 1.0 * 2.0;
            }
        }
        return x;
    }

    protected double getYWithOffsetDouble(double y) {
        return this.getCoordBaseMode() == null ? y : y + (double)this.boundingBox.minY;
    }

    protected double getZWithOffsetDouble(double x, double z) {
        if (this.getCoordBaseMode() == null) {
            return z;
        }
        switch (this.getCoordBaseMode()) {
            case EAST: {
                return (double)this.boundingBox.minZ + z;
            }
            case WEST: {
                return (double)this.boundingBox.maxZ - z + z % 1.0 * 2.0;
            }
            case NORTH: {
                return (double)this.boundingBox.maxZ - x + x % 1.0 * 2.0;
            }
            case SOUTH: {
                return (double)this.boundingBox.minZ + x;
            }
        }
        return z;
    }

    @Override
    public int getNPCXWithOffset(int x, int z) {
        if (this.getCoordBaseMode() == null) {
            return x;
        }
        switch (this.getCoordBaseMode()) {
            case SOUTH: {
                return this.boundingBox.minX + x;
            }
            case NORTH: {
                return this.boundingBox.maxX - x;
            }
            case EAST: {
                return this.boundingBox.minX + z;
            }
            case WEST: {
                return this.boundingBox.maxX - z;
            }
        }
        return x;
    }

    @Override
    public int getNPCZWithOffset(int x, int z) {
        if (this.getCoordBaseMode() == null) {
            return z;
        }
        switch (this.getCoordBaseMode()) {
            case SOUTH: {
                return this.boundingBox.minZ + z;
            }
            case NORTH: {
                return this.boundingBox.maxZ - z;
            }
            case EAST: {
                return this.boundingBox.maxZ - x;
            }
            case WEST: {
                return this.boundingBox.minZ + x;
            }
        }
        return z;
    }

    protected int getAverageGroundLevel(World worldIn) {
        int i = 0;
        int j = 0;
        int min = -1;
        int max = -1;
        BlockPos.MutableBlockPos blockpos = new BlockPos.MutableBlockPos();
        for (int k = this.boundingBox.minZ; k <= this.boundingBox.maxZ; ++k) {
            for (int l = this.boundingBox.minX; l <= this.boundingBox.maxX; ++l) {
                BlockPos pos;
                blockpos.setPos(l, 64, k);
                int value = Math.max(worldIn.getTopSolidOrLiquidBlock(blockpos).getY(), worldIn.provider.getAverageGroundLevel());
                if (min == -1 || value < min) {
                    min = value;
                }
                if (max == -1 || value > max) {
                    max = value;
                }
                if (this.anyWaterUnder(worldIn, pos = new BlockPos(l, value, k))) {
                    return -1;
                }
                i += value;
                ++j;
            }
        }
        if (j == 0 || max - min > 5) {
            return -1;
        }
        return i / j;
    }

    protected boolean anyWaterUnder(World world, BlockPos pos) {
        return IntStream.range(0, 4)
                .mapToObj(i -> pos.down(i))
                .map(p -> world.getBlockState(p))
                .map(bs -> bs.getMaterial())
                .anyMatch(m -> m.isLiquid());
    }

    protected int getAverageSeaFloorLevel(World world) {
        int i = 0;
        int j = 0;
        int height = this.boundingBox.getYSize();
        for (int k = this.boundingBox.minZ; k <= this.boundingBox.maxZ; ++k) {
            for (int l = this.boundingBox.minX; l <= this.boundingBox.maxX; ++l) {
                int value = this.getSeaFloorY(height, world, l, k);
                if (value == -1) continue;
                i += value;
                ++j;
            }
        }
        if (j == 0) {
            return -1;
        }
        int average = i / j;
        return world.getSeaLevel() - average > height ? average : -1;
    }

    protected int getSeaFloorY(int height, World world, int x, int z) {
        BlockPos pos = new BlockPos(x, world.getSeaLevel() - 1, z);
        while (pos.getY() >= 0 && this.isWaterBLock(world, pos)) {
            pos = pos.down();
        }
        int y = pos.getY();
        return world.getSeaLevel() - y > height ? y : -1;
    }

    protected boolean isWaterBLock(World world, BlockPos pos) {
        Block block = world.getBlockState(pos).getBlock();
        return block == Blocks.WATER || block == Blocks.FLOWING_WATER;
    }

    @Override
    protected boolean canStructureFitAtCoords(World world) {
        int height;
        int n = height = this.si.isUnderwater() ? this.getAverageSeaFloorLevel(world) : this.getAverageGroundLevel(world);
        if (this.si.isUnderwater() && Math.random() > 0.1) {
            return false;
        }
        if (height == -1) {
            return false;
        }
        this.boundingBox.offset(0, height - this.si.getDepth() - this.boundingBox.minY + 1, 0);
        return true;
    }

    @Override
    public boolean generateImpl(World world, Random random) {
        return this.addComponentParts(world, random, this.boundingBox);
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    protected void writeStructureToNBT(NBTTagCompound tagCompound) {
    }

    @Override
    protected void readStructureFromNBT(NBTTagCompound tagCompound, TemplateManager templateManager) {
    }

    @Override
    protected void setBlockState(World world, IBlockState blockstateIn, int x, int y, int z, StructureBoundingBox boundingboxIn) {
        BlockPos blockpos = new BlockPos(this.getXWithOffset(x, z), this.getYWithOffset(y), this.getZWithOffset(x, z));
        Optional<NBTTagCompound> optional = this.schematic.tileEntities.stream().filter(a -> a.getInteger("x") == x && a.getInteger("y") == y && a.getInteger("z") == z).findFirst();
        if (boundingboxIn.isVecInside(blockpos)) {
            blockstateIn = blockstateIn.withRotation(this.rotation.add(Rotation.CLOCKWISE_90));
            world.setBlockState(blockpos, blockstateIn, 2);
            optional.ifPresent(tag -> {
                String blockTileEntityId;
                String schematicTileEntityId;
                TileEntity tileEntity = world.getTileEntity(blockpos);
                if (tileEntity != null && (schematicTileEntityId = tag.getString("id")).equals(blockTileEntityId = TileEntity.getKey(tileEntity.getClass()).toString())) {
                    tileEntity.readFromNBT((NBTTagCompound)tag);
                    tileEntity.setWorld(world);
                    tileEntity.setPos(blockpos);
                    tileEntity.markDirty();
                }
            });
        }
    }

    @Override
    public void setCoordBaseMode(@Nullable EnumFacing facing) {
        super.setCoordBaseMode(facing);
        if (facing == null) {
            this.rotation = Rotation.NONE;
        } else {
            switch (facing) {
                case NORTH: {
                    this.rotation = Rotation.CLOCKWISE_180;
                    break;
                }
                case EAST: {
                    this.rotation = Rotation.COUNTERCLOCKWISE_90;
                    break;
                }
                case SOUTH: {
                    this.rotation = Rotation.NONE;
                    break;
                }
                case WEST: {
                    this.rotation = Rotation.CLOCKWISE_90;
                    break;
                }
            }
        }
    }

    public StructureInfo getStructureInfo() {
        return this.si;
    }

    public Schematic getSchematic() {
        return this.schematic;
    }
}

