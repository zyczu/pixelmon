/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.feature;

import com.pixelmongenerations.common.block.apricornTrees.BlockApricornTree;
import com.pixelmongenerations.core.config.BetterSpawnerConfig;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonConfig;
import java.util.Random;
import java.util.function.Predicate;
import net.minecraft.block.Block;
import net.minecraft.block.BlockHardenedClay;
import net.minecraft.block.BlockSand;
import net.minecraft.block.BlockStainedHardenedClay;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenPokemonGrass
implements IWorldGenerator {
    @Override
    public void generate(Random r, int chunkX, int chunkZ, World world, IChunkGenerator chunkGen, IChunkProvider chunkProvider) {
        Predicate<IBlockState> suitableSpawnBlock;
        IBlockState pixelmonGrassState;
        if (!world.provider.isSurfaceWorld() || !PixelmonConfig.generatePokeGass) {
            return;
        }
        Biome biome = world.getBiome(new BlockPos(chunkX * 16, 0, chunkZ * 16));
        String biomeId = biome.getRegistryName().toString();
        if (biome.isSnowyBiome()) {
            pixelmonGrassState = PixelmonBlocks.pixelmonSnowGrassBlock.getDefaultState();
            suitableSpawnBlock = b -> BlockApricornTree.isSuitableSoil(b.getBlock());
        } else if (BetterSpawnerConfig.INSTANCE.biomeCategories.get("dry").contains(biomeId)) {
            pixelmonGrassState = PixelmonBlocks.pixelmonDesertGrassBlock.getDefaultState();
            suitableSpawnBlock = b -> b.getBlock() == Blocks.SAND;
        } else if (BetterSpawnerConfig.INSTANCE.biomeCategories.get("mesas").contains(biomeId)) {
            pixelmonGrassState = PixelmonBlocks.pixelmonMesaGrassBlock.getDefaultState();
            suitableSpawnBlock = b -> {
                Block block = b.getBlock();
                return block == Blocks.SAND && b.getValue(BlockSand.VARIANT) == BlockSand.EnumType.RED_SAND || block instanceof BlockStainedHardenedClay || block instanceof BlockHardenedClay;
            };
        } else {
            pixelmonGrassState = PixelmonBlocks.pixelmonGrassBlock.getDefaultState();
            suitableSpawnBlock = b -> BlockApricornTree.isSuitableSoil(b.getBlock());
        }
        int maxInChunk = r.nextInt(10);
        for (int i = 0; i < maxInChunk - 1; ++i) {
            int z;
            int y;
            int x = r.nextInt(16) + chunkX * 16;
            if (!suitableSpawnBlock.test(world.getBlockState(new BlockPos(x, (y = world.getHeight(new BlockPos(x, 0, z = r.nextInt(16) + chunkZ * 16)).getY()) - 1, z)))) continue;
            world.setBlockState(new BlockPos(x, y, z), pixelmonGrassState, 1);
        }
    }
}

