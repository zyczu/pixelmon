/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.structure.towns;

import com.pixelmongenerations.core.enums.EnumNPCType;
import java.util.ArrayList;
import net.minecraft.item.ItemStack;

public class NPCPlacementInfo {
    public int x;
    public int y;
    public int z;
    public EnumNPCType npcType;
    public String extraData;
    public String npcName;
    public int rotation;
    public boolean hasRotation = false;
    public int tier;
    public ArrayList<ItemStack> drops;

    public NPCPlacementInfo(EnumNPCType type, int x, int y, int z) {
        this.npcType = type;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public NPCPlacementInfo(String npcname, EnumNPCType type, int x, int y, int z) {
        this(type, x, y, z);
        this.npcName = npcname;
    }

    public NPCPlacementInfo(String npcname, EnumNPCType type, int x, int y, int z, int rotation, int tier) {
        this(npcname, type, x, y, z);
        this.hasRotation = true;
        this.rotation = rotation;
        this.tier = tier;
    }

    public void setData(String data) {
        this.extraData = data;
    }

    public void addDrop(ItemStack item) {
        if (this.drops == null) {
            this.drops = new ArrayList();
        }
        this.drops.add(item);
    }
}

