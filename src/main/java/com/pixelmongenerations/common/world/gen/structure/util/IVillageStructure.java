/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.structure.util;

import net.minecraft.util.math.BlockPos;

public interface IVillageStructure {
    public int getNPCXWithOffset(int var1, int var2);

    public int getNPCYWithOffset(int var1);

    public int getNPCZWithOffset(int var1, int var2);

    default public BlockPos getAdjustedPosition(int x, int y, int z) {
        return new BlockPos(this.getNPCXWithOffset(x, z), this.getNPCYWithOffset(y), this.getNPCZWithOffset(x, z));
    }
}

