/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.feature;

import com.pixelmongenerations.common.block.apricornTrees.BlockApricornTree;
import com.pixelmongenerations.common.block.enums.EnumBlockPos;
import com.pixelmongenerations.common.block.tileEntities.TileEntityApricornTree;
import com.pixelmongenerations.core.config.PixelmonBlocksApricornTrees;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenApricornTrees
implements IWorldGenerator {
    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        if (!world.provider.isSurfaceWorld()) {
            return;
        }
        if (BiomeDictionary.getTypes(world.getBiome(new BlockPos(chunkX * 16, 0, chunkZ * 16))).contains(BiomeDictionary.Type.FOREST)) {
            for (int i = 0; i < random.nextInt(3) - 1; ++i) {
                TileEntityApricornTree tree;
                TileEntity tile;
                int z;
                int y;
                int x = random.nextInt(16) + chunkX * 16;
                Block block = world.getBlockState(new BlockPos(x, (y = world.getHeight(new BlockPos(x, 0, z = random.nextInt(16) + chunkZ * 16)).getY()) - 1, z)).getBlock();
                if (block != Blocks.GRASS && block != Blocks.DIRT) continue;
                Block newBlock = null;
                switch (random.nextInt(7)) {
                    case 0: {
                        newBlock = PixelmonBlocksApricornTrees.apricornTreeBlack;
                        break;
                    }
                    case 1: {
                        newBlock = PixelmonBlocksApricornTrees.apricornTreeWhite;
                        break;
                    }
                    case 2: {
                        newBlock = PixelmonBlocksApricornTrees.apricornTreePink;
                        break;
                    }
                    case 3: {
                        newBlock = PixelmonBlocksApricornTrees.apricornTreeGreen;
                        break;
                    }
                    case 4: {
                        newBlock = PixelmonBlocksApricornTrees.apricornTreeBlue;
                        break;
                    }
                    case 5: {
                        newBlock = PixelmonBlocksApricornTrees.apricornTreeYellow;
                        break;
                    }
                    case 6: {
                        newBlock = PixelmonBlocksApricornTrees.apricornTreeRed;
                    }
                }
                if (newBlock != null) {
                    world.setBlockState(new BlockPos(x, y, z), newBlock.getDefaultState().withProperty(BlockApricornTree.BLOCKPOS, EnumBlockPos.BOTTOM), 2);
                    world.setBlockState(new BlockPos(x, y + 1, z), newBlock.getDefaultState().withProperty(BlockApricornTree.BLOCKPOS, EnumBlockPos.TOP), 2);
                }
                if (!((tile = world.getTileEntity(new BlockPos(x, y, z))) instanceof TileEntityApricornTree) || (tree = (TileEntityApricornTree)world.getTileEntity(new BlockPos(x, y, z))) == null) continue;
                tree.setStage(5);
            }
        }
    }
}

