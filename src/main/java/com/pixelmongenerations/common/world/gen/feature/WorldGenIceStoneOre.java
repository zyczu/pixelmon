/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.feature;

import com.pixelmongenerations.core.config.PixelmonBlocks;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenIceStoneOre
implements IWorldGenerator {
    public boolean generate(World world, int x, int y, int z) {
        BlockPos pos = new BlockPos(x, y, z);
        world.setBlockState(pos, PixelmonBlocks.iceStoneOre.getDefaultState());
        return true;
    }

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        if (!world.provider.isSurfaceWorld()) {
            return;
        }
        if (BiomeDictionary.hasType(world.getBiome(new BlockPos(chunkX * 16, 0, chunkZ * 16)), BiomeDictionary.Type.SNOWY)) {
            for (int i = 0; i < random.nextInt(3) + 1; ++i) {
                int x = random.nextInt(16) + chunkX * 16;
                int z = random.nextInt(16) + chunkZ * 16;
                int y = world.getHeight(new BlockPos(x, 0, z)).getY();
                IBlockState block = world.getBlockState(new BlockPos(x, y - 1, z));
                Material blockMaterial = block.getMaterial();
                while (blockMaterial != Material.ROCK && y > 0) {
                    blockMaterial = world.getBlockState(new BlockPos(x, --y - 1, z)).getMaterial();
                }
                if (y <= 0) continue;
                this.generate(world, x, y - 1, z);
            }
        }
    }
}

