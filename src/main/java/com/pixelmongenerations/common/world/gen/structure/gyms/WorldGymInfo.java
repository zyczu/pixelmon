/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.structure.gyms;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;

public class WorldGymInfo {
    public String name;
    public ItemStack badge;
    public int level;

    public WorldGymInfo(String name, ItemStack badge, int level) {
        this.name = name;
        this.badge = badge;
        this.level = level;
    }

    public WorldGymInfo(NBTTagCompound gymTag) {
        this.name = gymTag.getString("name");
        this.level = gymTag.getInteger("level");
        this.badge = new ItemStack((NBTTagCompound)gymTag.getTag("badge"));
    }

    public NBTBase getTag() {
        NBTTagCompound gymTag = new NBTTagCompound();
        gymTag.setString("name", this.name);
        gymTag.setInteger("level", this.level);
        NBTTagCompound itemTag = new NBTTagCompound();
        this.badge.writeToNBT(itemTag);
        gymTag.setTag("badge", itemTag);
        return gymTag;
    }
}

