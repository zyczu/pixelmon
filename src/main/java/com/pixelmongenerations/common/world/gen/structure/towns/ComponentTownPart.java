/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.structure.towns;

import com.pixelmongenerations.common.entity.npcs.NPCNurseJoy;
import com.pixelmongenerations.common.entity.npcs.NPCShopkeeper;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.entity.npcs.registry.ShopkeeperData;
import com.pixelmongenerations.common.world.gen.structure.StructureInfo;
import com.pixelmongenerations.common.world.gen.structure.StructureRegistry;
import com.pixelmongenerations.common.world.gen.structure.towns.NPCPlacementInfo;
import com.pixelmongenerations.common.world.gen.structure.util.IVillageStructure;
import com.pixelmongenerations.common.world.gen.structure.util.StructureScattered;
import com.pixelmongenerations.common.world.gen.structure.worldGen.WorldGenGym;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.enums.EnumShopKeeperType;
import java.util.List;
import java.util.Random;
import net.minecraft.entity.EntityLiving;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.structure.StructureBoundingBox;
import net.minecraft.world.gen.structure.StructureComponent;
import net.minecraft.world.gen.structure.StructureVillagePieces;
import net.minecraft.world.gen.structure.template.TemplateManager;

public class ComponentTownPart
extends StructureVillagePieces.House1
implements IVillageStructure {
    StructureInfo info;

    public ComponentTownPart() {
        this.info = null;
    }

    public ComponentTownPart(StructureInfo info, StructureVillagePieces.Start villagePiece, int par2, StructureBoundingBox structureBoundingBox, EnumFacing facing) {
        this.info = info;
        this.setCoordBaseMode(facing);
        this.boundingBox = structureBoundingBox;
    }

    public static ComponentTownPart buildComponent(StructureVillagePieces.Start villagePiece, List<StructureComponent> pieces, Random random, int p1, int p2, int p3, EnumFacing facing, int p5, StructureInfo info) {
        StructureScattered structureGenerated = info.createStructure(random, new BlockPos(p1, p2, p3), true, true, facing = facing.rotateY());
        StructureBoundingBox structureBoundingBox = structureGenerated.getBoundingBox();
        return ComponentTownPart.canVillageGoDeeper(structureBoundingBox) && StructureComponent.findIntersecting(pieces, structureBoundingBox) == null ? new ComponentTownPart(info, villagePiece, p5, structureBoundingBox, facing) : null;
    }

    @Override
    public boolean addComponentParts(World worldIn, Random randomIn, StructureBoundingBox structureBoundingBoxIn) {
        if (this.averageGroundLvl < 0) {
            this.averageGroundLvl = this.getAverageGroundLevel(worldIn, structureBoundingBoxIn);
            if (this.averageGroundLvl < 0 || this.boundingBox == null || this.info == null) {
                return true;
            }
            this.boundingBox.offset(0, this.averageGroundLvl - this.boundingBox.minY - this.info.getDepth(), 0);
        }
        if (this.info == null || this.boundingBox == null) {
            return true;
        }
        this.info.createStructure(randomIn, new BlockPos(this.boundingBox.minX, this.boundingBox.minY, this.boundingBox.minZ), true, true, this.getCoordBaseMode()).addComponentParts(worldIn, randomIn, structureBoundingBoxIn);
        ComponentTownPart.spawnVillagers(this, this.info, worldIn, structureBoundingBoxIn);
        if (WorldGenGym.lastTownBB == null || !this.closeTo(structureBoundingBoxIn)) {
            WorldGenGym.lastTownBB = structureBoundingBoxIn;
            WorldGenGym.hasGenerated = false;
        }
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean closeTo(StructureBoundingBox structureBoundingBoxIn) {
        List<StructureBoundingBox> list;
        BlockPos center = ComponentTownPart.getCenter(structureBoundingBoxIn);
        if (ComponentTownPart.getCenter(WorldGenGym.lastTownBB).distanceSq(center) < 6400.0) {
            return true;
        }
        List<StructureBoundingBox> list2 = list = WorldGenGym.usedTownsList;
        synchronized (list2) {
            for (int i = 0; i < WorldGenGym.usedTownsList.size(); ++i) {
                if (ComponentTownPart.getCenter(WorldGenGym.usedTownsList.get(i)).distanceSq(center) >= 6400.0) continue;
                return true;
            }
        }
        return false;
    }

    public static void spawnVillagers(IVillageStructure structure, StructureInfo info, World worldIn, StructureBoundingBox structureBoundingBoxIn) {
        for (NPCPlacementInfo npcInfo : info.getNPCS()) {
            if (npcInfo.npcType == EnumNPCType.Shopkeeper) {
                ShopkeeperData shopkeeper = null;
                if (npcInfo.extraData.equalsIgnoreCase("Main")) {
                    shopkeeper = ServerNPCRegistry.shopkeepers.getRandom(EnumShopKeeperType.PokemartMain);
                } else if (npcInfo.extraData.equalsIgnoreCase("Secondary")) {
                    shopkeeper = ServerNPCRegistry.shopkeepers.getRandom(EnumShopKeeperType.PokemartSecond);
                }
                if (shopkeeper == null) continue;
                NPCShopkeeper entity = new NPCShopkeeper(worldIn);
                entity.init(shopkeeper);
                entity.initDefaultAI();
                entity.npcLocation = SpawnLocation.LandVillager;
                entity.ignoreDespawnCounter = true;
                ComponentTownPart.spawnVillager(structure, worldIn, structureBoundingBoxIn, npcInfo.x, npcInfo.y, npcInfo.z, entity);
                continue;
            }
            if (npcInfo.npcType != EnumNPCType.NurseJoy) continue;
            NPCNurseJoy entity = new NPCNurseJoy(worldIn);
            entity.initDefaultAI();
            entity.ignoreDespawnCounter = true;
            ComponentTownPart.spawnVillager(structure, worldIn, structureBoundingBoxIn, npcInfo.x, npcInfo.y, npcInfo.z, entity);
        }
    }

    protected static void spawnVillager(IVillageStructure structure, World worldIn, StructureBoundingBox bbox, int x, int y, int z, EntityLiving entity) {
        BlockPos pos = structure.getAdjustedPosition(x, y, z);
        if (bbox.isVecInside(pos)) {
            entity.setLocationAndAngles((double)pos.getX() + 0.5, pos.getY(), (double)pos.getZ() + 0.5, 0.0f, 0.0f);
            worldIn.spawnEntity(entity);
        }
    }

    @Override
    protected void readStructureFromNBT(NBTTagCompound tagCompound, TemplateManager p_143011_2_) {
        if (tagCompound.hasKey("strucID")) {
            this.info = StructureRegistry.getByID(tagCompound.getString("strucID"));
        }
        super.readStructureFromNBT(tagCompound, p_143011_2_);
    }

    @Override
    protected void writeStructureToNBT(NBTTagCompound tagCompound) {
        if (this.info != null) {
            tagCompound.setString("strucID", this.info.id);
        }
        super.writeStructureToNBT(tagCompound);
    }

    @Override
    public int getNPCXWithOffset(int x, int z) {
        return super.getXWithOffset(x, z);
    }

    @Override
    public int getNPCYWithOffset(int y) {
        return super.getYWithOffset(y);
    }

    @Override
    public int getNPCZWithOffset(int x, int z) {
        return super.getZWithOffset(x, z);
    }

    public static BlockPos getCenter(StructureBoundingBox boundingBox) {
        return new BlockPos(boundingBox.minX + (boundingBox.maxX - boundingBox.minX + 1) / 2, boundingBox.minY + (boundingBox.maxY - boundingBox.minY + 1) / 2, boundingBox.minZ + (boundingBox.maxZ - boundingBox.minZ + 1) / 2);
    }
}

