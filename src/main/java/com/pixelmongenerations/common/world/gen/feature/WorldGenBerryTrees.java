/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.feature;

import com.pixelmongenerations.common.block.BlockBerryTree;
import com.pixelmongenerations.common.block.enums.EnumBlockPos;
import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityBerryTree;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenBerryTrees
implements IWorldGenerator {
    private EnumFacing[] horiz = new EnumFacing[]{EnumFacing.EAST, EnumFacing.WEST, EnumFacing.NORTH, EnumFacing.SOUTH};

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        if (!world.provider.isSurfaceWorld()) {
            return;
        }
        if (BiomeDictionary.getTypes(world.getBiome(new BlockPos(chunkX * 16, 0, chunkZ * 16))).contains(BiomeDictionary.Type.FOREST)) {
            for (int i = 0; i < random.nextInt(4) - 1; ++i) {
                TileEntityBerryTree tree;
                int z;
                int y;
                int x = random.nextInt(16) + chunkX * 16;
                Block block = world.getBlockState(new BlockPos(x, (y = world.getHeight(new BlockPos(x, 0, z = random.nextInt(16) + chunkZ * 16)).getY()) - 1, z)).getBlock();
                if (block != Blocks.GRASS && block != Blocks.DIRT) continue;
                EnumBerry berry = EnumBerry.getImplementedBerry();
                Block newBlock = berry.getTreeBlock();
                if (newBlock != null) {
                    EnumFacing facing = this.getRandomHorizontalFacing();
                    world.setBlockState(new BlockPos(x, y, z), newBlock.getDefaultState().withProperty(BlockBerryTree.BLOCKPOS, EnumBlockPos.BOTTOM).withProperty(GenericRotatableModelBlock.FACING, facing), 2);
                    if (berry.height > 1) {
                        world.setBlockState(new BlockPos(x, y + 1, z), newBlock.getDefaultState().withProperty(BlockBerryTree.BLOCKPOS, EnumBlockPos.TOP).withProperty(GenericRotatableModelBlock.FACING, facing), 2);
                    }
                }
                if ((tree = (TileEntityBerryTree)world.getTileEntity(new BlockPos(x, y, z))) == null) continue;
                tree.setGenerated();
            }
        }
    }

    private EnumFacing getRandomHorizontalFacing() {
        return RandomHelper.getRandomElementFromArray(this.horiz);
    }
}

