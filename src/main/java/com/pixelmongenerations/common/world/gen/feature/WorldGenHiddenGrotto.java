/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.world.gen.feature;

import com.pixelmongenerations.common.spawning.EnumBiomeType;
import com.pixelmongenerations.common.spawning.PixelmonBiomeDictionary;
import com.pixelmongenerations.common.world.gen.structure.HiddenGrotto;
import com.pixelmongenerations.core.config.PixelmonConfig;
import java.util.ArrayList;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.structure.MapGenScatteredFeature;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenHiddenGrotto
extends MapGenScatteredFeature
implements IWorldGenerator {
    private static int lastChunk = 0;
    private static int lastX;
    private static int lastZ;
    private static int MIN_DISTANCE;
    private static int MIN_CHUNK;
    private static HiddenGrotto groto;

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        int z;
        if (!PixelmonConfig.spawnGrotto) {
            return;
        }
        int x = random.nextInt(16) + chunkX * 16;
        if (this.canGenerate(world, x, z = random.nextInt(16) + chunkZ * 16)) {
            int y0 = this.lowerToGround(world, x, world.getHeight(new BlockPos(x, 0, z)).getY(), z);
            int y1 = this.lowerToGround(world, x + 8, world.getHeight(new BlockPos(x + 8, 0, z)).getY(), z);
            int y2 = this.lowerToGround(world, x, world.getHeight(new BlockPos(x, 0, z + 8)).getY(), z + 8);
            int y3 = this.lowerToGround(world, x + 8, world.getHeight(new BlockPos(x + 8, 0, z + 8)).getY(), z + 8);
            if (!(this.waterCheck(world, x, y0, z) && this.waterCheck(world, x + 8, y1, z) && this.waterCheck(world, x, y2, z + 8) && this.waterCheck(world, x + 8, y3, z + 8))) {
                return;
            }
            int y = Math.min(Math.min(y0, y1), Math.min(y2, y3));
            groto.generate(world, new BlockPos(x, y, z));
            lastX = x;
            lastZ = y;
        }
    }

    private boolean canGenerate(World world, int x, int z) {
        if (world.provider.getDimension() == -1 || world.provider.getDimension() == 1) {
            return false;
        }
        if ((int)Math.sqrt(Math.pow(lastX - x, 2.0) + Math.pow(lastZ - z, 2.0)) >= MIN_DISTANCE && lastChunk % MIN_CHUNK == 0) {
            if (this.biomeOK(world.getBiome(new BlockPos(x, 0, z)))) {
                ++lastChunk;
                return true;
            }
            ++lastChunk;
            return false;
        }
        ++lastChunk;
        return false;
    }

    private boolean biomeOK(Biome b) {
        ArrayList<EnumBiomeType> validTypes = new ArrayList<EnumBiomeType>();
        validTypes.add(EnumBiomeType.FOREST);
        validTypes.add(EnumBiomeType.TAIGA);
        validTypes.add(EnumBiomeType.JUNGLE);
        return PixelmonBiomeDictionary.getBiomeIDsMatchingAnyTag(validTypes).contains(Biome.getIdForBiome(b));
    }

    private boolean waterCheck(World world, int x, int y, int z) {
        Block underBlock = world.getBlockState(new BlockPos(x, y - 1, z)).getBlock();
        return underBlock != Blocks.FLOWING_WATER && underBlock != Blocks.WATER && underBlock != Blocks.WATERLILY;
    }

    private int lowerToGround(World world, int x, int y, int z) {
        int Y = y;
        Block underBlock = world.getBlockState(new BlockPos(x, Y - 1, z)).getBlock();
        while (underBlock == Blocks.LEAVES || underBlock == Blocks.LOG || world.isAirBlock(new BlockPos(x, Y - 1, z))) {
            underBlock = world.getBlockState(new BlockPos(x, --Y - 1, z)).getBlock();
        }
        return Y;
    }

    static {
        lastZ = 0;
        MIN_DISTANCE = 512;
        MIN_CHUNK = 128;
        groto = new HiddenGrotto();
    }
}

