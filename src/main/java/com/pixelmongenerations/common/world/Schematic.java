/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.base.Predicate
 */
package com.pixelmongenerations.common.world;

import com.google.common.base.Predicate;
import com.pixelmongenerations.common.entity.pixelmon.EntityStatue;
import com.pixelmongenerations.core.Pixelmon;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import net.minecraft.block.Block;
import net.minecraft.block.BlockStructureVoid;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.storage.ExtendedBlockStorage;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class Schematic {
    private static final boolean cubicChunks = Loader.isModLoaded("cubicchunks");
    public int version = 1;
    public String author = null;
    public String name = null;
    public long creationDate;
    public String[] requiredMods = new String[0];
    public short width;
    public short height;
    public short length;
    public int[] offset = new int[]{0, 0, 0};
    public int paletteMax = -1;
    public List<IBlockState> palette = new ArrayList<IBlockState>();
    public short[][][] blockData;
    public List<NBTTagCompound> tileEntities = new ArrayList<NBTTagCompound>();
    public List<NBTTagCompound> entities = new ArrayList<NBTTagCompound>();

    public Schematic() {
    }

    public Schematic(short width, short height, short length) {
        this();
        this.width = width;
        this.height = height;
        this.length = length;
        this.blockData = new short[width][height][length];
        this.palette.add(Blocks.AIR.getDefaultState());
        ++this.paletteMax;
        this.creationDate = System.currentTimeMillis();
    }

    public Schematic(String name, String author, short width, short height, short length) {
        this(width, height, length);
        this.name = name;
        this.author = author;
    }

    public static Schematic loadFromNBT(NBTTagCompound nbt) {
        Schematic schematic = new Schematic();
        schematic.version = nbt.getInteger("Version");
        schematic.creationDate = System.currentTimeMillis();
        if (nbt.hasKey("Metadata")) {
            NBTTagCompound metadataCompound = nbt.getCompoundTag("Metadata").getCompoundTag(".");
            if (nbt.hasKey("Author")) {
                schematic.author = metadataCompound.getString("Author");
            }
            schematic.name = metadataCompound.getString("Name");
            schematic.creationDate = nbt.hasKey("Date") ? metadataCompound.getLong("Date") : -1L;
            if (nbt.hasKey("RequiredMods")) {
                NBTTagList requiredModsTagList = (NBTTagList)metadataCompound.getTag("RequiredMods");
                schematic.requiredMods = new String[requiredModsTagList.tagCount()];
                for (int i2 = 0; i2 < requiredModsTagList.tagCount(); ++i2) {
                    schematic.requiredMods[i2] = requiredModsTagList.getStringTagAt(i2);
                }
            }
        }
        schematic.width = nbt.getShort("Width");
        schematic.height = nbt.getShort("Height");
        schematic.length = nbt.getShort("Length");
        if (nbt.hasKey("Offset")) {
            schematic.offset = nbt.getIntArray("Offset");
        }
        NBTTagCompound paletteNBT = nbt.getCompoundTag("Palette");
        HashMap<Integer, String> paletteMap = new HashMap<Integer, String>();
        for (String key : paletteNBT.getKeySet()) {
            int paletteID = paletteNBT.getInteger(key);
            paletteMap.put(paletteID, key);
        }
        for (int i3 = 0; i3 < paletteMap.size(); ++i3) {
            String stateString;
            String blockString;
            String blockStateString = (String)paletteMap.get(i3);
            if (blockStateString == null) {
                schematic.palette.add(Blocks.AIR.getDefaultState());
                continue;
            }
            char lastBlockStateStringChar = blockStateString.charAt(blockStateString.length() - 1);
            if (lastBlockStateStringChar == ']') {
                String[] blockAndStateStrings = blockStateString.split("\\[");
                blockString = blockAndStateStrings[0];
                stateString = blockAndStateStrings[1];
                stateString = stateString.substring(0, stateString.length() - 1);
            } else {
                blockString = blockStateString;
                stateString = "";
            }
            Block block = ForgeRegistries.BLOCKS.getValue(new ResourceLocation(blockString));
            IBlockState blockstate = block.getDefaultState();
            if (!stateString.equals("")) {
                String[] properties = stateString.split(",");
                blockstate = Schematic.getBlockStateWithProperties(block, properties);
            }
            schematic.palette.add(blockstate);
        }
        schematic.paletteMax = nbt.hasKey("PaletteMax") ? nbt.getInteger("PaletteMax") : schematic.palette.size() - 1;
        if (nbt.getInteger("SchemFormat") > 0) {
            int[] blockDataIntArray = nbt.getIntArray("BlockData");
            schematic.blockData = new short[schematic.width][schematic.height][schematic.length];
            for (int x = 0; x < schematic.width; ++x) {
                for (int y = 0; y < schematic.height; ++y) {
                    for (int z = 0; z < schematic.length; ++z) {
                        schematic.blockData[x][y][z] = (short)blockDataIntArray[x + z * schematic.width + y * schematic.width * schematic.length];
                    }
                }
            }
        } else {
            byte[] blockDataIntArray = nbt.getByteArray("BlockData");
            schematic.blockData = new short[schematic.width][schematic.height][schematic.length];
            for (int x = 0; x < schematic.width; ++x) {
                for (int y = 0; y < schematic.height; ++y) {
                    for (int z = 0; z < schematic.length; ++z) {
                        schematic.blockData[x][y][z] = (short)blockDataIntArray[x + z * schematic.width + y * schematic.width * schematic.length];
                    }
                }
            }
        }
        if (nbt.hasKey("TileEntities")) {
            NBTTagList tileEntitiesTagList = (NBTTagList)nbt.getTag("TileEntities");
            for (int i = 0; i < tileEntitiesTagList.tagCount(); ++i) {
                NBTTagCompound tileEntityTagCompound = tileEntitiesTagList.getCompoundTagAt(i);
                schematic.tileEntities.add(tileEntityTagCompound);
            }
        }
        if (nbt.hasKey("Entities")) {
            NBTTagList entitiesTagList = (NBTTagList)nbt.getTag("Entities");
            for (int i = 0; i < entitiesTagList.tagCount(); ++i) {
                NBTTagCompound entityTagCompound = entitiesTagList.getCompoundTagAt(i);
                schematic.entities.add(entityTagCompound);
            }
        }
        return schematic;
    }

    public NBTTagCompound saveToNBT() {
        NBTTagCompound nbt = new NBTTagCompound();
        nbt.setInteger("Version", this.version);
        NBTTagCompound metadataCompound = new NBTTagCompound();
        if (this.author != null) {
            metadataCompound.setString("Author", this.author);
        }
        metadataCompound.setString("Name", this.name);
        if (this.creationDate != -1L) {
            metadataCompound.setLong("Date", this.creationDate);
        }
        NBTTagList requiredModsTagList = new NBTTagList();
        for (String requiredMod : this.requiredMods) {
            requiredModsTagList.appendTag(new NBTTagString(requiredMod));
        }
        metadataCompound.setTag("RequiredMods", requiredModsTagList);
        nbt.setTag("Metadata", metadataCompound);
        nbt.setShort("Width", this.width);
        nbt.setShort("Height", this.height);
        nbt.setShort("Length", this.length);
        nbt.setIntArray("Offset", this.offset);
        nbt.setInteger("PaletteMax", this.paletteMax);
        NBTTagCompound paletteNBT = new NBTTagCompound();
        for (int i = 0; i < this.palette.size(); ++i) {
            IBlockState state = this.palette.get(i);
            String blockStateString = state.toString();
            paletteNBT.setInteger(blockStateString, i);
        }
        nbt.setTag("Palette", paletteNBT);
        nbt.setInteger("SchemFormat", 1);
        int[] blockDataIntArray = new int[this.width * this.height * this.length];
        for (int x = 0; x < this.width; ++x) {
            for (int y = 0; y < this.height; ++y) {
                for (int z = 0; z < this.length; ++z) {
                    blockDataIntArray[x + z * this.width + y * this.width * this.length] = this.blockData[x][y][z];
                }
            }
        }
        nbt.setIntArray("BlockData", blockDataIntArray);
        NBTTagList tileEntitiesTagList = new NBTTagList();
        for (NBTTagCompound tileEntityTagCompound : this.tileEntities) {
            tileEntitiesTagList.appendTag(tileEntityTagCompound);
        }
        nbt.setTag("TileEntities", tileEntitiesTagList);
        NBTTagList entitiesTagList = new NBTTagList();
        for (NBTTagCompound entityTagCompound : this.entities) {
            entitiesTagList.appendTag(entityTagCompound);
        }
        nbt.setTag("Entities", entitiesTagList);
        return nbt;
    }

    static IBlockState getBlockStateWithProperties(Block block, String[] properties) {
        HashMap<String, String> propertyAndBlockStringsMap = new HashMap<String, String>();
        for (String property : properties) {
            String[] propertyAndBlockStrings = property.split("=");
            propertyAndBlockStringsMap.put(propertyAndBlockStrings[0], propertyAndBlockStrings[1]);
        }
        BlockStateContainer container = block.getBlockState();
        IBlockState chosenState = block.getDefaultState();
        for (Map.Entry entry : propertyAndBlockStringsMap.entrySet()) {
            IProperty<?> property = container.getProperty((String)entry.getKey());
            if (property == null) continue;
            Comparable value = null;
            for (Comparable object : property.getAllowedValues()) {
                if (!object.toString().equals(entry.getValue())) continue;
                value = object;
                break;
            }
            if (value == null) continue;
            chosenState = chosenState.withProperty((IProperty)property, value);
        }
        return chosenState;
    }

    private static String getBlockStateStringFromState(IBlockState state) {
        String totalString;
        Block block = state.getBlock();
        String blockNameString = String.valueOf(Block.REGISTRY.getNameForObject(block));
        StringBuilder blockStateString = new StringBuilder();
        IBlockState defaultState = block.getDefaultState();
        if (state == defaultState) {
            totalString = blockNameString;
        } else {
            BlockStateContainer container = block.getBlockState();
            for (IProperty<?> property : container.getProperties()) {
                String thisPropertyValue;
                String defaultPropertyValue = ((Comparable)defaultState.getProperties().get(property)).toString();
                if (defaultPropertyValue.equals(thisPropertyValue = ((Comparable)state.getProperties().get(property)).toString())) continue;
                String firstHalf = property.getName();
                String secondHalf = ((Comparable)state.getProperties().get(property)).toString();
                String propertyString = firstHalf + "=" + secondHalf;
                blockStateString.append(propertyString).append(",");
            }
            blockStateString = new StringBuilder(blockStateString.substring(0, blockStateString.length() - 1));
            totalString = blockNameString + "[" + blockStateString + "]";
        }
        return totalString;
    }

    public static Schematic createFromWorld(World world, BlockPos from, BlockPos to) {
        Vec3d min = new Vec3d(Math.min(from.getX(), to.getX()), Math.min(from.getY(), to.getY()), Math.min(from.getZ(), to.getZ()));
        Vec3d max = new Vec3d(Math.max(from.getX(), to.getX()), Math.max(from.getY(), to.getY()), Math.max(from.getZ(), to.getZ()));
        BlockPos dimensions = new BlockPos(max.subtract(min).add(1.0, 1.0, 1.0));
        Schematic schematic = new Schematic((short)dimensions.getX(), (short)dimensions.getY(), (short)dimensions.getZ());
        HashSet<String> mods = new HashSet<String>();
        for (int x = 0; x < dimensions.getX(); ++x) {
            for (int y = 0; y < dimensions.getY(); ++y) {
                for (int z = 0; z < dimensions.getZ(); ++z) {
                    BlockPos pos = new BlockPos(min.x + (double)x, min.y + (double)y, min.z + (double)z);
                    IBlockState state = world.getBlockState(pos);
                    String id = state.toString();
                    if (id.contains(":")) {
                        mods.add(id.split(":")[0]);
                    }
                    schematic.setBlockState(x, y, z, state);
                    TileEntity tileEntity = world.getChunk(pos).getTileEntity(pos, Chunk.EnumCreateEntityType.IMMEDIATE);
                    if (tileEntity == null) continue;
                    NBTTagCompound tileEntityNBT = tileEntity.serializeNBT();
                    tileEntityNBT.setInteger("x", x);
                    tileEntityNBT.setInteger("y", y);
                    tileEntityNBT.setInteger("z", z);
                    schematic.tileEntities.add(tileEntityNBT);
                }
            }
        }
        for (Entity entity : world.getEntitiesInAABBexcluding(null, Schematic.getBoundingBox(from, to), (Predicate<? super Entity>)((Predicate)EntityStatue.class::isInstance))) {
            NBTTagCompound entityNBT = entity.serializeNBT();
            NBTTagList posNBT = (NBTTagList)entityNBT.getTag("Pos");
            NBTTagList newPosNBT = new NBTTagList();
            newPosNBT.appendTag(new NBTTagDouble(posNBT.getDoubleAt(0) - min.x));
            newPosNBT.appendTag(new NBTTagDouble(posNBT.getDoubleAt(1) - min.y));
            newPosNBT.appendTag(new NBTTagDouble(posNBT.getDoubleAt(2) - min.z));
            entityNBT.setTag("Pos", newPosNBT);
            schematic.entities.add(entityNBT);
        }
        schematic.requiredMods = mods.toArray(new String[0]);
        schematic.creationDate = System.currentTimeMillis();
        return schematic;
    }

    private static AxisAlignedBB getBoundingBox(Vec3i from, Vec3i to) {
        return new AxisAlignedBB(new BlockPos(from.getX(), from.getY(), from.getZ()), new BlockPos(to.getX(), to.getY(), to.getZ()));
    }

    public void place(World world, int xBase, int yBase, int zBase, boolean visiblePlacement) {
        if (visiblePlacement) {
            this.setBlocksVisibly(world, xBase, yBase, zBase);
        } else {
            this.setBlocks(world, xBase, yBase, zBase);
        }
        for (NBTTagCompound tileEntityNBT : this.tileEntities) {
            BlockPos schematicPos = new BlockPos(tileEntityNBT.getInteger("x"), tileEntityNBT.getInteger("y"), tileEntityNBT.getInteger("z"));
            BlockPos pos = new BlockPos(xBase, yBase, zBase).add(schematicPos);
            TileEntity tileEntity = world.getTileEntity(pos);
            if (tileEntity != null) {
                String blockTileEntityId;
                String schematicTileEntityId = tileEntityNBT.getString("id");
                if (schematicTileEntityId.equals(blockTileEntityId = TileEntity.getKey(tileEntity.getClass()).toString())) {
                    tileEntity.readFromNBT(tileEntityNBT);
                    tileEntity.setWorld(world);
                    tileEntity.setPos(pos);
                    tileEntity.markDirty();
                    continue;
                }
                throw new RuntimeException("Schematic contained TileEntity " + schematicTileEntityId + " at " + pos + " but the TileEntity of that block (" + world.getBlockState(pos) + ") must be " + blockTileEntityId);
            }
            throw new RuntimeException("Schematic contained TileEntity info at " + pos + " but the block there (" + world.getBlockState(pos) + ") has no TileEntity.");
        }
        for (NBTTagCompound entityNBT : this.entities) {
            NBTTagList posNBT = (NBTTagList)entityNBT.getTag("Pos");
            NBTTagList newPosNBT = new NBTTagList();
            newPosNBT.appendTag(new NBTTagDouble(posNBT.getDoubleAt(0) + (double)xBase));
            newPosNBT.appendTag(new NBTTagDouble(posNBT.getDoubleAt(1) + (double)yBase));
            newPosNBT.appendTag(new NBTTagDouble(posNBT.getDoubleAt(2) + (double)zBase));
            NBTTagCompound adjustedEntityNBT = entityNBT.copy();
            adjustedEntityNBT.setTag("Pos", newPosNBT);
            adjustedEntityNBT.setUniqueId("UUID", UUID.randomUUID());
            Entity entity = EntityList.createEntityFromNBT(adjustedEntityNBT, world);
            world.spawnEntity(entity);
        }
    }

    public IBlockState getBlockState(int x, int y, int z) {
        return this.palette.get(this.blockData[x][y][z]);
    }

    public void setBlockState(int x, int y, int z, IBlockState state) {
        if (this.palette.contains(state)) {
            this.blockData[x][y][z] = (short)this.palette.indexOf(state);
        } else {
            this.palette.add(state);
            this.blockData[x][y][z] = (short)(++this.paletteMax);
        }
    }

    private void setBlocksVisibly(World world, int xBase, int yBase, int zBase) {
        for (int x = 0; x < this.width; ++x) {
            for (int y = 0; y < this.height; ++y) {
                for (int z = 0; z < this.length; ++z) {
                    IBlockState state = this.getBlockState(x, y, z);
                    if (Schematic.shouldPlace(state)) continue;
                    world.setBlockState(new BlockPos(x + xBase, y + yBase, z + zBase), state);
                }
            }
        }
    }

    private void setBlocks(World world, int xBase, int yBase, int zBase) {
        long setTime = 0L;
        long relightTime = 0L;
        Pixelmon.LOGGER.info("Setting chunk blockstates");
        for (int chunkX = 0; chunkX <= (this.width >> 4) + 1; ++chunkX) {
            for (int chunkZ = 0; chunkZ <= (this.length >> 4) + 1; ++chunkZ) {
                long setStart = System.nanoTime();
                Chunk chunk = world.getChunk((xBase >> 4) + chunkX, (zBase >> 4) + chunkZ);
                ExtendedBlockStorage[] storageArray = chunk.getBlockStorageArray();
                for (int storageY = 0; storageY <= (this.height >> 4) + 1; ++storageY) {
                    ExtendedBlockStorage storage = storageArray[(yBase >> 4) + storageY];
                    boolean setAir = storage != null;
                    for (int x = 0; x < 16; ++x) {
                        for (int y = 0; y < 16; ++y) {
                            for (int z = 0; z < 16; ++z) {
                                IBlockState state;
                                int sx = (chunkX << 4) + x - (xBase & 0xF);
                                int sy = (storageY << 4) + y - (yBase & 0xF);
                                int sz = (chunkZ << 4) + z - (zBase & 0xF);
                                if (sx < 0 || sy < 0 || sz < 0 || sx >= this.width || sy >= this.height || sz >= this.length || Schematic.shouldPlace(state = this.palette.get(this.blockData[sx][sy][sz]))) continue;
                                if (!state.getBlock().equals(Blocks.AIR)) {
                                    if (storage == null) {
                                        storageArray[(yBase >> 4) + storageY] = storage = new ExtendedBlockStorage(storageY << 4, world.provider.hasSkyLight());
                                    }
                                    storage.set(x, y, z, state);
                                    continue;
                                }
                                if (!setAir) continue;
                                storage.set(x, y, z, state);
                            }
                        }
                    }
                }
                setTime += System.nanoTime() - setStart;
                long relightStart = System.nanoTime();
                chunk.setLightPopulated(false);
                chunk.setTerrainPopulated(true);
                chunk.resetRelightChecks();
                chunk.checkLight();
                relightTime += System.nanoTime() - relightStart;
                chunk.markDirty();
            }
        }
        world.markBlockRangeForRenderUpdate(xBase, yBase, zBase, xBase + this.width, yBase + this.height, zBase + this.length);
        Pixelmon.LOGGER.info("Set block states in " + setTime / 1000000L + " ms and relit chunks/cubes in " + relightTime / 1000000L);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Schematic load(String type, String id) {
        String schematicJarDirectory = String.format("/assets/pixelmon/structures/%s/", type);
        InputStream schematicStream = Pixelmon.class.getResourceAsStream(schematicJarDirectory + id + ".schem");
        DataInputStream schematicDataStream = null;
        boolean streamOpened = false;
        if (schematicStream != null) {
            schematicDataStream = new DataInputStream(schematicStream);
            streamOpened = true;
        } else {
            Pixelmon.LOGGER.warn("Schematic '" + id + "' was not found in the jar or config directory, neither with the .schem extension, nor with the .schematic extension. ");
        }
        Schematic schematic = null;
        if (streamOpened) {
            try {
                NBTTagCompound schematicNBT = CompressedStreamTools.readCompressed(schematicDataStream);
                schematic = Schematic.loadFromNBT(schematicNBT);
                schematicDataStream.close();
            }
            catch (IOException ex) {
                ex.printStackTrace();
                Pixelmon.LOGGER.error("Schematic file for " + id + " could not be read as a valid schematic NBT file.", (Throwable)ex);
            }
            finally {
                try {
                    schematicDataStream.close();
                }
                catch (IOException ex) {
                    Pixelmon.LOGGER.error("Error occured while closing schematicDataStream", (Throwable)ex);
                }
            }
        }
        return schematic;
    }

    public static boolean shouldPlace(IBlockState state) {
        return state.getBlock() instanceof BlockStructureVoid;
    }
}

