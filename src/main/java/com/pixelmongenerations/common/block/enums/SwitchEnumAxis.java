/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.enums;

import com.pixelmongenerations.common.block.enums.EnumAxis;

public final class SwitchEnumAxis {
    static final int[] AXIS_LOOKUP = new int[EnumAxis.values().length];

    static {
        try {
            SwitchEnumAxis.AXIS_LOOKUP[EnumAxis.X.ordinal()] = 1;
        }
        catch (NoSuchFieldError noSuchFieldError) {
            // empty catch block
        }
        try {
            SwitchEnumAxis.AXIS_LOOKUP[EnumAxis.Z.ordinal()] = 2;
        }
        catch (NoSuchFieldError noSuchFieldError) {
            // empty catch block
        }
        try {
            SwitchEnumAxis.AXIS_LOOKUP[EnumAxis.NONE.ordinal()] = 3;
        }
        catch (NoSuchFieldError noSuchFieldError) {
            // empty catch block
        }
    }
}

