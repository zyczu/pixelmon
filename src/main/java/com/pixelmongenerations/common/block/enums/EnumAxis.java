/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.enums;

import com.pixelmongenerations.common.block.enums.SwitchEnumAxis;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IStringSerializable;

public enum EnumAxis implements IStringSerializable
{
    X,
    Y,
    Z,
    NONE;


    @Override
    public String getName() {
        return this.toString().toLowerCase();
    }

    public int toMeta() {
        return this.ordinal();
    }

    public static EnumAxis fromMeta(int i) {
        return EnumAxis.values()[i];
    }

    public static EnumAxis fromFacingAxis(EnumFacing.Axis axis) {
        switch (SwitchEnumAxis.AXIS_LOOKUP[axis.ordinal()]) {
            case 1: {
                return X;
            }
            case 2: {
                return Z;
            }
            case 3: {
                return Y;
            }
        }
        return NONE;
    }
}

