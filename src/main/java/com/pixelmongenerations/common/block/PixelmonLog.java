/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLog;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

public class PixelmonLog
extends BlockLog {
    public PixelmonLog(String name) {
        this.setTranslationKey(name);
        this.setSoundType(SoundType.WOOD);
        this.setDefaultState(this.blockState.getBaseState().withProperty(LOG_AXIS, BlockLog.EnumAxis.Y));
        this.setCreativeTab(PixelmonCreativeTabs.buildingBlocks);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        IBlockState state = this.getDefaultState();
        switch (meta & 9) {
            case 0: {
                state = state.withProperty(LOG_AXIS, BlockLog.EnumAxis.Y);
                break;
            }
            case 2: {
                state = state.withProperty(LOG_AXIS, BlockLog.EnumAxis.X);
                break;
            }
            case 4: {
                state = state.withProperty(LOG_AXIS, BlockLog.EnumAxis.Z);
                break;
            }
            default: {
                state = state.withProperty(LOG_AXIS, BlockLog.EnumAxis.NONE);
            }
        }
        return state;
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        int i = 0;
        switch ((BlockLog.EnumAxis)state.getValue(LOG_AXIS)) {
            case X: {
                i |= 2;
                break;
            }
            case Y: {
                i |= 4;
                break;
            }
            case Z: {
                i |= 6;
            }
        }
        return i;
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, LOG_AXIS);
    }

    @Override
    public int getFlammability(IBlockAccess world, BlockPos pos, EnumFacing face) {
        return Blocks.LOG.getFlammability(world, pos, face);
    }

    @Override
    public int getFireSpreadSpeed(IBlockAccess world, BlockPos pos, EnumFacing face) {
        return Blocks.LOG.getFireSpreadSpeed(world, pos, face);
    }
}

