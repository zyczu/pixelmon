/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.common.block.multiBlocks.BlockGenericModelMultiblock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityRotomPC;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.storage.PCServer;
import com.pixelmongenerations.core.util.PixelSounds;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.ContainerPlayer;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockRotomPC
extends BlockGenericModelMultiblock {
    public BlockRotomPC() {
        super(Material.ROCK, 1, 2.0, 1);
        this.setHardness(2.5f);
        this.setTranslationKey("rotompc");
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World var1, IBlockState var2) {
        return Optional.of(new TileEntityRotomPC());
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!world.isRemote && hand == EnumHand.MAIN_HAND) {
            try {
                if (!(player.openContainer instanceof ContainerPlayer) && !(player.openContainer instanceof ContainerEmpty)) {
                    return false;
                }
                CompletionStage future = CompletableFuture.runAsync(() -> PCServer.sendContentsToPlayer((EntityPlayerMP)player)).thenAccept(v -> {
                    player.openGui(Pixelmon.INSTANCE, EnumGui.PC.getIndex(), world, 0, 0, 0);
                    world.playSound(null, player.posX, player.posY, player.posZ, PixelSounds.pc, SoundCategory.BLOCKS, 0.7f, 1.0f);
                });
                return true;
            }
            catch (Exception exception) {
                // empty catch block
            }
        }
        return true;
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return Item.getItemFromBlock(this);
    }
}

