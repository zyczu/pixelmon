/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.machines;

import com.pixelmongenerations.api.enums.ReceiveType;
import com.pixelmongenerations.api.events.FossilMachineEvent;
import com.pixelmongenerations.api.events.PixelmonReceivedEvent;
import com.pixelmongenerations.api.events.player.PlayerShinyChanceEvent;
import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.enums.EnumMultiPos;
import com.pixelmongenerations.common.block.tileEntities.TileEntityFossilMachine;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.entity.pokeballs.PokeballTypeHelper;
import com.pixelmongenerations.common.item.ItemFossil;
import com.pixelmongenerations.common.item.ItemPokeball;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Optional;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.management.PlayerChunkMap;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;

public class BlockFossilMachine
extends MultiBlock {
    public BlockFossilMachine() {
        super(Material.IRON, 1, 2.0, 1);
        this.setHardness(1.0f);
        this.setTranslationKey("fossilmachine");
    }

    @Override
    public void randomDisplayTick(IBlockState state, World world, BlockPos pos, Random rand) {
        TileEntityFossilMachine machine;
        if (state.getValue(MULTIPOS) == EnumMultiPos.BASE && (machine = BlockHelper.getTileEntity(TileEntityFossilMachine.class, world, pos)) != null && machine.fossilProgress > 1.0f && machine.pokemonProgress < 3200.0f) {
            world.playSound(pos.getX(), (double)pos.getY(), (double)pos.getZ(), SoundEvents.BLOCK_PORTAL_TRAVEL, SoundCategory.BLOCKS, 0.01f, rand.nextFloat() * 0.4f + 0.8f, true);
        }
    }

    public void capturePokemonInMachine(World world, BlockPos pos, EntityPlayer player) {
        TileEntityFossilMachine tile = (TileEntityFossilMachine)world.getTileEntity(pos);
        if (tile != null) {
            EntityPixelmon pixelmon = (EntityPixelmon)PixelmonEntityList.createEntityByName(tile.currentPokemon, world);
            pixelmon.getLvl().setLevel(1);
            pixelmon.setTamed(true);
            pixelmon.setOwnerId(player.getUniqueID());
            pixelmon.caughtBall = tile.pokeball;
            pixelmon.clearAttackTarget();
            boolean isShiny = tile.isShiny;
            if (!isShiny) {
                float chance = PixelmonConfig.shinyRate;
                if (PixelmonConfig.enableCatchCombos) {
                    if (PixelmonConfig.enableCatchComboShinyLock && PixelmonMethods.isCatchComboSpecies((EntityPlayerMP)player, EnumSpecies.getFromNameAnyCase(tile.currentPokemon))) {
                        chance = PixelmonMethods.getCatchComboChance((EntityPlayerMP)player);
                    } else if (!PixelmonConfig.enableCatchComboShinyLock) {
                        chance = PixelmonMethods.getCatchComboChance((EntityPlayerMP)player);
                    }
                } else if (PixelmonMethods.isWearingShinyCharm((EntityPlayerMP)player)) {
                    chance = PixelmonConfig.shinyCharmRate;
                }
                PlayerShinyChanceEvent event = new PlayerShinyChanceEvent((EntityPlayerMP)player, chance);
                MinecraftForge.EVENT_BUS.post(event);
                chance = event.isCanceled() ? 0.0f : event.getChance();
                isShiny = chance > 0.0f && RandomHelper.getRandomChance(1.0f / chance);
            }
            pixelmon.setShiny(isShiny);
            PokeballTypeHelper.doAfterEffect(tile.pokeball, pixelmon);
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
            if (optstorage.isPresent()) {
                MinecraftForge.EVENT_BUS.post(new PixelmonReceivedEvent((EntityPlayerMP)player, ReceiveType.Fossil, pixelmon));
                optstorage.get().addToParty(pixelmon);
            }
            pixelmon.catchInPokeball();
            pixelmon.friendship.initFromCapture();
            ((WorldServer)world).getPlayerChunkMap().markBlockForUpdate(pos);
        }
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        boolean obtainedPokemon;
        if (world.isRemote || hand == EnumHand.OFF_HAND) {
            return true;
        }
        ItemStack heldItem = player.getHeldItem(hand);
        pos = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), state);
        TileEntityFossilMachine tile = BlockHelper.getTileEntity(TileEntityFossilMachine.class, world, pos);
        PlayerChunkMap playerChunkMap = ((WorldServer)world).getPlayerChunkMap();
        Item heldItemType = null;
        if (!heldItem.isEmpty()) {
            heldItemType = heldItem.getItem();
        }
        boolean heldItemIsFossil = heldItemType instanceof ItemFossil && ((ItemFossil)heldItemType).fossil.isMachineUsable();
        boolean heldItemIsPokeBall = heldItemType instanceof ItemPokeball;
        boolean bl = obtainedPokemon = tile.pokeball != null && tile.pokemonOccupied && tile.pokemonProgress == 3200.0f;
        if (!obtainedPokemon) {
            FossilMachineEvent event;
            FossilMachineEvent.Pokeball.Take event2;
            if (tile.pokeball != null && !heldItemIsFossil && !MinecraftForge.EVENT_BUS.post(event2 = new FossilMachineEvent.Pokeball.Take(world, pos, player, tile.pokeball))) {
                ItemPokeball item = event2.getPokeball().getItem();
                DropItemHelper.giveItemStackToPlayer(player, new ItemStack(item));
                tile.pokeball = null;
            }
            ItemFossil prevFossil = tile.currentFossil;
            if (tile.currentFossil != null && !heldItemIsPokeBall && !MinecraftForge.EVENT_BUS.post(event = new FossilMachineEvent.Fossil.Take(world, pos, player, prevFossil))) {
                DropItemHelper.giveItemStackToPlayer(player, new ItemStack(((FossilMachineEvent.Fossil)event).getFossil()));
                tile.currentFossil = null;
                tile.fossilProgress = 0.0f;
                tile.pokemonProgress = 0.0f;
            }
            if (!heldItem.isEmpty()) {
                if (heldItemIsPokeBall && !MinecraftForge.EVENT_BUS.post(event = new FossilMachineEvent.Pokeball.Insert(world, pos, player, ((ItemPokeball)player.getHeldItemMainhand().getItem()).type))) {
                    tile.pokeball = ((FossilMachineEvent.Pokeball)event).getPokeball();
                    if (!player.capabilities.isCreativeMode) {
                        heldItem.shrink(1);
                    }
                    playerChunkMap.markBlockForUpdate(pos);
                    return true;
                }
                if (heldItemIsFossil && !tile.pokemonOccupied && prevFossil != heldItemType && !MinecraftForge.EVENT_BUS.post(event = new FossilMachineEvent.Fossil.Insert(world, pos, player, (ItemFossil)heldItemType))) {
                    tile.currentFossil = (ItemFossil)heldItemType;
                    if (!player.capabilities.isCreativeMode) {
                        heldItem.shrink(1);
                    }
                    playerChunkMap.markBlockForUpdate(pos);
                    return true;
                }
            }
        }
        if (obtainedPokemon) {
            if (!MinecraftForge.EVENT_BUS.post(new FossilMachineEvent.TakePokemon(world, pos, player, tile.currentFossil, tile.pokeball))) {
                this.capturePokemonInMachine(world, pos, player);
            } else {
                obtainedPokemon = false;
            }
        }
        if (obtainedPokemon || !tile.pokemonOccupied && !heldItemIsFossil && !heldItemIsPokeBall) {
            tile.pokemonOccupied = false;
            tile.fossilProgress = 0.0f;
            tile.pokemonProgress = 0.0f;
            tile.currentFossil = null;
            tile.currentPokemon = "";
            tile.pokeball = null;
            tile.completionRate = 0;
            playerChunkMap.markBlockForUpdate(pos);
            return true;
        }
        playerChunkMap.markBlockForUpdate(pos);
        return false;
    }

    @Override
    public int quantityDropped(Random random) {
        return 1;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.INVISIBLE;
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World world, IBlockState state) {
        return Optional.of(new TileEntityFossilMachine());
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return Item.getItemFromBlock(this);
    }
}

