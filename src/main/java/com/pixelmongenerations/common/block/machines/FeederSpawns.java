/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.reflect.TypeToken
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 *  com.google.gson.JsonDeserializationContext
 *  com.google.gson.JsonDeserializer
 *  com.google.gson.JsonElement
 *  com.google.gson.JsonParseException
 *  com.google.gson.JsonPrimitive
 *  com.google.gson.JsonSerializationContext
 *  com.google.gson.JsonSerializer
 */
package com.pixelmongenerations.common.block.machines;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class FeederSpawns {
    public static Gson gsonInstance = new GsonBuilder().registerTypeAdapter(PokemonSpec.class, (Object)new PokemonSpecAdapter()).setPrettyPrinting().create();
    private static FeederSpawns spawns = new FeederSpawns();
    private List<EntityPixelmon> nobles;
    private List<PokemonSpec> commonSpawns = Stream.of(new EnumSpecies[]{EnumSpecies.Sandshrew, EnumSpecies.Gligar, EnumSpecies.Trapinch, EnumSpecies.Hoppip, EnumSpecies.Phanpy, EnumSpecies.Surskit, EnumSpecies.Zubat, EnumSpecies.Aron, EnumSpecies.Wooper, EnumSpecies.Pikachu, EnumSpecies.Oddish, EnumSpecies.Gloom, EnumSpecies.Doduo, EnumSpecies.Natu, EnumSpecies.Girafarig, EnumSpecies.Wobbuffet, EnumSpecies.Dodrio, EnumSpecies.Rhyhorn, EnumSpecies.Pinsir, EnumSpecies.Xatu, EnumSpecies.Heracross, EnumSpecies.Hoothoot, EnumSpecies.Spinarak, EnumSpecies.Mareep, EnumSpecies.Aipom, EnumSpecies.Sunkern, EnumSpecies.Snubbull, EnumSpecies.Stantler, EnumSpecies.Ledyba, EnumSpecies.Pineco, EnumSpecies.Teddiursa, EnumSpecies.Houndour, EnumSpecies.Miltank, EnumSpecies.Shuckle, EnumSpecies.Geodude, EnumSpecies.Scyther, EnumSpecies.Growlithe, EnumSpecies.Petilil, EnumSpecies.Voltorb, EnumSpecies.Bergmite, EnumSpecies.Rufflet, EnumSpecies.Sneasel}).map(EnumSpecies::toSpec).collect(Collectors.toList());

    private void setDefaultNobleList() {
        this.nobles = new ArrayList<EntityPixelmon>();
        World w = FMLCommonHandler.instance().getMinecraftServerInstance().getEntityWorld();
        PokemonSpec kleavor = PokemonSpec.from("Kleavor");
        this.nobles.add(kleavor.create(w));
        PokemonSpec lilligant = PokemonSpec.from("Lilligant", "f:11");
        this.nobles.add(lilligant.create(w));
        PokemonSpec arcanine = PokemonSpec.from("Arcanine", "f:11");
        this.nobles.add(arcanine.create(w));
        PokemonSpec electrode = PokemonSpec.from("Electrode", "f:11");
        this.nobles.add(electrode.create(w));
        PokemonSpec avalugg = PokemonSpec.from("Avalugg", "f:11");
        this.nobles.add(avalugg.create(w));
    }

    public static void registerFeederSpawns() {
        Pixelmon.LOGGER.info("Registering Feeder Spawns.");
        spawns = PixelmonConfig.useExternalJSONFilesFeederSpawns ? FeederSpawns.importSetsFrom("./config/pixelmon/feeder_pokemon.json") : FeederSpawns.retrieveFeederSpawnsFromAssets();
    }

    public static FeederSpawns importSetsFrom(String dir) {
        File file = new File(dir);
        if (!file.exists()) {
            FeederSpawns.retrieveFeederSpawnsFromAssets();
        }
        try {
            return (FeederSpawns)gsonInstance.fromJson((Reader)new FileReader(file), TypeToken.of(FeederSpawns.class).getType());
        }
        catch (Exception var6) {
            var6.printStackTrace();
            return new FeederSpawns();
        }
    }

    public static FeederSpawns retrieveFeederSpawnsFromAssets() {
        FeederSpawns map = new FeederSpawns();
        try {
            String primaryPath;
            File file;
            if (PixelmonConfig.useExternalJSONFilesFeederSpawns && !(file = new File(primaryPath = "./config/pixelmon/feeder_pokemon.json")).exists()) {
                file.getParentFile().mkdirs();
                PrintWriter pw = new PrintWriter(file);
                pw.write(gsonInstance.toJson((Object)map));
                pw.flush();
                pw.close();
            }
            return map;
        }
        catch (Exception e) {
            e.printStackTrace();
            return map;
        }
    }

    public static FeederSpawns getSpawns() {
        return spawns;
    }

    public List<PokemonSpec> getCommonSpawns() {
        return this.commonSpawns;
    }

    public EntityPixelmon getRandomNoble() {
        this.setDefaultNobleList();
        return this.nobles.get(new Random().nextInt(this.nobles.size()));
    }

    public static class PokemonSpecAdapter
    implements JsonDeserializer<PokemonSpec>,
    JsonSerializer<PokemonSpec> {
        public JsonElement serialize(PokemonSpec src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.toString());
        }

        public PokemonSpec deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return new PokemonSpec(json.getAsString());
        }
    }
}

