/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.machines;

import com.pixelmongenerations.api.events.AnvilEvent;
import com.pixelmongenerations.common.block.tileEntities.TileEntityAnvil;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.item.ItemPokeballDisc;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonItemsPokeballs;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.BlockHorizontal;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;

public class BlockAnvil
extends BlockContainer {
    public static final PropertyDirection FACING = BlockHorizontal.FACING;
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.8f, 1.0);

    public BlockAnvil() {
        super(Material.ROCK);
        this.setHardness(1.0f);
        this.setResistance(2000.0f);
        this.setSoundType(SoundType.METAL);
        this.setTranslationKey("anvil");
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        return AABB;
    }

    @Override
    public boolean removedByPlayer(IBlockState state, World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {
        this.onPlayerDestroy(world, pos, world.getBlockState(pos));
        return super.removedByPlayer(state, world, pos, player, willHarvest);
    }

    public void onPlayerDestroy(World world, BlockPos pos, IBlockState state) {
        TileEntityAnvil anvil;
        if (!world.isRemote && (anvil = BlockHelper.getTileEntity(TileEntityAnvil.class, world, pos)) != null && anvil.itemOnAnvil != null) {
            Item item = anvil.itemOnAnvil;
            EntityItem var3 = new EntityItem(world, pos.getX(), (double)pos.getY() + BlockAnvil.AABB.maxY, pos.getZ(), new ItemStack(item));
            var3.setPickupDelay(10);
            world.spawnEntity(var3);
        }
        super.onPlayerDestroy(world, pos, state);
    }

    @Override
    public int quantityDropped(Random random) {
        return 1;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        Item heldItem;
        if (world.isRemote) {
            return false;
        }
        ItemStack playerItemStack = player.getHeldItem(hand);
        TileEntityAnvil anvil = BlockHelper.getTileEntity(TileEntityAnvil.class, world, pos);
        if (anvil == null) {
            return false;
        }
        if (anvil.itemOnAnvil != null && hand == EnumHand.MAIN_HAND) {
            AnvilEvent.MaterialChangedEvent materialChangedEvent = new AnvilEvent.MaterialChangedEvent((EntityPlayerMP)player, anvil, new ItemStack(anvil.itemOnAnvil), true);
            if (MinecraftForge.EVENT_BUS.post(materialChangedEvent)) {
                return false;
            }
            anvil.itemOnAnvil = null;
            anvil.state = 0;
            DropItemHelper.giveItemStackToPlayer(player, materialChangedEvent.getMaterial());
            ((WorldServer)player.world).getPlayerChunkMap().markBlockForUpdate(pos);
            return true;
        }
        if (anvil.itemOnAnvil == null && !playerItemStack.isEmpty() && ((heldItem = playerItemStack.getItem()) instanceof ItemPokeballDisc || heldItem == PixelmonItemsPokeballs.ironDisc || heldItem == PixelmonItemsPokeballs.aluDisc || heldItem == PixelmonItems.aluminiumIngot)) {
            AnvilEvent.MaterialChangedEvent materialChangedEvent = new AnvilEvent.MaterialChangedEvent((EntityPlayerMP)player, anvil, playerItemStack, false);
            if (MinecraftForge.EVENT_BUS.post(materialChangedEvent)) {
                return false;
            }
            if (playerItemStack.getCount() > 1) {
                playerItemStack.shrink(1);
            } else {
                player.setHeldItem(hand, ItemStack.EMPTY);
            }
            anvil.itemOnAnvil = heldItem;
            world.playSound(null, pos, SoundEvents.BLOCK_ANVIL_LAND, SoundCategory.BLOCKS, 0.8f, 1.0f);
            ((WorldServer)player.world).getPlayerChunkMap().markBlockForUpdate(pos);
            return true;
        }
        return false;
    }

    @Override
    public TileEntity createNewTileEntity(World var1, int var2) {
        return new TileEntityAnvil();
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return Item.getItemFromBlock(PixelmonBlocks.anvil);
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(Item.getItemFromBlock(PixelmonBlocks.anvil));
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, FACING);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(FACING, EnumFacing.byHorizontalIndex((int)meta));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(FACING).getHorizontalIndex();
    }

    @Override
    public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
        IBlockState iblockstate = this.getDefaultState();
        if (facing.getAxis().isHorizontal()) {
            iblockstate = iblockstate.withProperty(FACING, facing);
        }
        return iblockstate;
    }
}

