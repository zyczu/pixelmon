/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.machines;

import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityFossilCleaner;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.item.ItemCoveredFossil;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.Optional;
import java.util.Random;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockFossilCleaner
extends MultiBlock {
    public BlockFossilCleaner() {
        super(Material.IRON, 1, 1.0, 1);
        this.setHardness(1.0f);
        this.setSoundType(SoundType.STONE);
        this.setTranslationKey("fossilcleaner");
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return Item.getItemFromBlock(this);
    }

    @Override
    public int quantityDropped(Random random) {
        return 1;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return face == EnumFacing.DOWN ? BlockFaceShape.SOLID : BlockFaceShape.UNDEFINED;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.INVISIBLE;
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World world, IBlockState state) {
        return Optional.of(new TileEntityFossilCleaner());
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!world.isRemote && hand == EnumHand.MAIN_HAND) {
            ItemStack heldItem = player.getHeldItem(hand);
            TileEntityFossilCleaner fossilCleaner = BlockHelper.getTileEntity(TileEntityFossilCleaner.class, world, pos);
            if (fossilCleaner != null) {
                if (!fossilCleaner.isEmpty()) {
                    Item item = fossilCleaner.getItemInCleaner();
                    fossilCleaner.setItemInCleaner(null);
                    DropItemHelper.giveItemStackToPlayer(player, new ItemStack(item));
                } else if (!heldItem.isEmpty() && heldItem.getItem() instanceof ItemCoveredFossil) {
                    fossilCleaner.setItemInCleaner(heldItem.getItem());
                    heldItem.shrink(1);
                    if (heldItem.getCount() <= 0) {
                        player.setHeldItem(hand, ItemStack.EMPTY);
                    }
                }
            }
        }
        return true;
    }

    @Override
    public boolean removedByPlayer(IBlockState state, World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {
        if (!player.capabilities.isCreativeMode) {
            this.onPlayerDestroy(world, pos, world.getBlockState(pos));
        }
        return super.removedByPlayer(state, world, pos, player, willHarvest);
    }

    public void onPlayerDestroy(World world, BlockPos pos, IBlockState state) {
        TileEntityFossilCleaner fossilCleaner;
        if (!world.isRemote && (fossilCleaner = BlockHelper.getTileEntity(TileEntityFossilCleaner.class, world, pos)) != null && fossilCleaner.getItemInCleaner() != null) {
            EntityItem entityItem = new EntityItem(world, pos.getX(), (double)pos.getY() + 1.3, pos.getZ(), new ItemStack(fossilCleaner.getItemInCleaner()));
            entityItem.setPickupDelay(10);
            world.spawnEntity(entityItem);
        }
        super.onPlayerDestroy(world, pos, state);
    }

    @Override
    public int tickRate(World world) {
        return 2;
    }

    @Override
    public int getWeakPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side) {
        TileEntityFossilCleaner tile = BlockHelper.getTileEntity(TileEntityFossilCleaner.class, blockAccess, pos);
        if (tile != null && tile.isOn()) {
            if (tile.timer == 360) {
                return 15;
            }
            return tile.timer / 24 + 1;
        }
        return 0;
    }

    @Override
    public boolean canProvidePower(IBlockState state) {
        return true;
    }
}

