/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.machines;

import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityZygardeMachine;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockZygardeMachine
extends GenericRotatableModelBlock {
    public BlockZygardeMachine() {
        super(Material.ROCK);
        this.setHardness(2.5f);
        this.setTranslationKey("zygardemachine");
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        ItemStack heldItem = player.getHeldItem(hand);
        if (world.isRemote || hand == EnumHand.OFF_HAND) {
            return true;
        }
        TileEntityZygardeMachine tile = BlockHelper.getTileEntity(TileEntityZygardeMachine.class, world, pos);
        if (tile != null) {
            tile.activate(player, hand, heldItem);
        }
        return true;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityZygardeMachine();
    }

    public Item getDroppedItem(World world, BlockPos pos) {
        return Item.getItemFromBlock(this);
    }
}

