/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.base.Enums
 */
package com.pixelmongenerations.common.block.machines;

import com.google.common.base.Enums;
import com.pixelmongenerations.common.block.IBlockHasOwner;
import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPC;
import com.pixelmongenerations.common.gui.ContainerEmpty;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PCServer;
import com.pixelmongenerations.core.util.PixelSounds;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.inventory.ContainerPlayer;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockPC
extends MultiBlock
implements IBlockHasOwner {
    public BlockPC() {
        super(Material.ROCK, 1, 2.0, 1);
        this.setHardness(2.5f);
        this.setTranslationKey("pc");
    }

    @Override
    public int quantityDropped(Random random) {
        return 1;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!world.isRemote && hand == EnumHand.MAIN_HAND) {
            ItemStack heldItem = player.getHeldItem(hand);
            pos = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), state);
            if (!heldItem.isEmpty() && heldItem.getItem() instanceof ItemDye) {
                TileEntityPC tileEntityPC = BlockHelper.getTileEntity(TileEntityPC.class, world, pos);
                if (tileEntityPC != null) {
                    if (player.getUniqueID().equals(tileEntityPC.getOwnerUUID())) {
                        EnumDyeColor color;
                        if (!tileEntityPC.getRave() && !tileEntityPC.getColour().isEmpty() && (color = (EnumDyeColor)Enums.getIfPresent(EnumDyeColor.class, (String)tileEntityPC.getColour().toUpperCase()).orNull()) != null) {
                            ItemStack stack = new ItemStack(Items.DYE);
                            stack.setItemDamage(color.getDyeDamage());
                            world.spawnEntity(new EntityItem(world, (double)pos.getX() + 0.5, (double)pos.getY() + 0.5, (double)pos.getZ() + 0.5, stack));
                        }
                        EnumDyeColor dyeColor = EnumDyeColor.byDyeDamage(heldItem.getItemDamage());
                        tileEntityPC.setColour(dyeColor.getName());
                        if (!player.capabilities.isCreativeMode) {
                            heldItem.shrink(1);
                        }
                    } else {
                        ChatHandler.sendChat(player, "pixelmon.blocks.pc.ownership", new Object[0]);
                    }
                }
                return true;
            }
            try {
                if (!(player.openContainer instanceof ContainerPlayer) && !(player.openContainer instanceof ContainerEmpty)) {
                    return false;
                }
                CompletableFuture.runAsync(() -> PCServer.sendContentsToPlayer((EntityPlayerMP)player)).thenAccept(v -> {
                    player.openGui(Pixelmon.INSTANCE, EnumGui.PC.getIndex(), world, 0, 0, 0);
                    world.playSound(null, player.posX, player.posY, player.posZ, PixelSounds.pc, SoundCategory.BLOCKS, 0.7f, 1.0f);
                });
                return true;
            }
            catch (Exception exception) {
                // empty catch block
            }
        }
        return true;
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World world, IBlockState state) {
        return Optional.of(new TileEntityPC());
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return Item.getItemFromBlock(this);
    }

    @Override
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
        TileEntityPC tileEntity = BlockHelper.getTileEntity(TileEntityPC.class, worldIn, pos);
        if (tileEntity != null && !tileEntity.getRave() && !tileEntity.getColour().isEmpty()) {
            EnumDyeColor color = (EnumDyeColor)Enums.getIfPresent(EnumDyeColor.class, (String)tileEntity.getColour().toUpperCase()).orNull();
            if (!tileEntity.getRave() && color != null) {
                ItemStack stack = new ItemStack(Items.DYE);
                stack.setItemDamage(color.getDyeDamage());
                InventoryHelper.spawnItemStack(worldIn, (double)pos.getX() + 0.5, (double)pos.getY() + 0.5, (double)pos.getZ() + 0.5, stack);
            }
        }
        super.breakBlock(worldIn, pos, state);
    }

    @Override
    public void setOwner(BlockPos pos, EntityPlayer playerIn) {
        UUID playerID = playerIn.getUniqueID();
        TileEntityPC tileEntityPC = BlockHelper.getTileEntity(TileEntityPC.class, playerIn.getEntityWorld(), pos);
        tileEntityPC.setOwner(playerID);
    }
}

