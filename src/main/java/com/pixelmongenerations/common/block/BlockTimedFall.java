/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.common.block.generic.GenericBlock;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockTimedFall
extends GenericBlock {
    private static int ticksToFall = 300;
    public HashMap<UUID, Integer> fallCounters = new HashMap();

    public BlockTimedFall() {
        super(Material.WOOD);
        this.setHardness(1.0f);
        this.setCreativeTab(PixelmonCreativeTabs.utilityBlocks);
    }

    @Override
    public void addCollisionBoxToList(IBlockState state, World worldIn, BlockPos pos, AxisAlignedBB mask, List<AxisAlignedBB> list, Entity collidingEntity, boolean b) {
        if (!(collidingEntity instanceof EntityPlayer)) {
            super.addCollisionBoxToList(state, worldIn, pos, mask, list, collidingEntity, b);
            return;
        }
        EntityPlayer player = (EntityPlayer)collidingEntity;
        if (!this.fallCounters.containsKey(player.getUniqueID())) {
            this.fallCounters.put(player.getUniqueID(), ticksToFall);
        }
        int currentTickForEntity = this.fallCounters.get(player.getUniqueID());
        this.fallCounters.put(player.getUniqueID(), currentTickForEntity - 1);
        if (currentTickForEntity >= 0) {
            super.addCollisionBoxToList(state, worldIn, pos, mask, list, collidingEntity, b);
            return;
        }
        if (currentTickForEntity <= -100) {
            this.fallCounters.remove(player.getUniqueID());
        }
    }
}

