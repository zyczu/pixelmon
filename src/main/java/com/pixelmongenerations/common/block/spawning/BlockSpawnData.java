/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawning;

import com.pixelmongenerations.common.entity.pixelmon.stats.Rarity;
import com.pixelmongenerations.common.spawning.spawners.EnumWorldState;

public class BlockSpawnData {
    public final String name;
    public int minLvl;
    public int maxLvl;
    public Rarity rarity;

    public BlockSpawnData(String name, int minLvl, int maxLvl, Rarity rarity) {
        this.name = name;
        this.maxLvl = maxLvl;
        this.minLvl = minLvl;
        this.rarity = rarity;
    }

    public int getRarity(EnumWorldState state) {
        return state.equals(EnumWorldState.day) ? this.rarity.day : (state.equals(EnumWorldState.dawn) || state.equals(EnumWorldState.dusk) ? this.rarity.dawndusk : this.rarity.night);
    }
}

