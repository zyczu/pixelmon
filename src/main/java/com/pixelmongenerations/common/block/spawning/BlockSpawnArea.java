/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawning;

import com.pixelmongenerations.common.block.spawning.BlockSpawnData;
import com.pixelmongenerations.common.spawning.spawners.EnumWorldState;
import com.pixelmongenerations.core.enums.battle.EnumBattleStartTypes;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.HashMap;
import net.minecraft.world.World;

public class BlockSpawnArea {
    public String name;
    public EnumBattleStartTypes type;
    public HashMap<String, BlockSpawnData> blockSpawns = new HashMap();

    public BlockSpawnArea(String name, EnumBattleStartTypes type, HashMap<String, BlockSpawnData> blockSpawns) {
        this.name = name;
        this.type = type;
        this.blockSpawns = blockSpawns;
    }

    public String getRandomPokemon(World world) {
        return this.getPokemonFromList(world);
    }

    public String getPokemonFromList(World world) {
        EnumWorldState state = this.getWorldState(world);
        int totalRarity = 0;
        for (BlockSpawnData spawnList : this.blockSpawns.values()) {
            totalRarity += spawnList.getRarity(state);
        }
        if (totalRarity > 0) {
            int num = RandomHelper.getRandomNumberBetween(0, totalRarity - 1);
            int sum = 0;
            for (BlockSpawnData spawnList : this.blockSpawns.values()) {
                int rarity = 0;
                if (num < sum + (rarity += spawnList.getRarity(state))) {
                    return spawnList.name;
                }
                sum += rarity;
            }
        }
        return null;
    }

    public EnumWorldState getWorldState(World world) {
        long time = world.getWorldTime() % 24000L;
        return time >= 22500L || time < 1000L ? EnumWorldState.dawn : (time < 11000L ? EnumWorldState.day : (time < 13500L ? EnumWorldState.dusk : EnumWorldState.night));
    }
}

