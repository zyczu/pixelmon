/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawning;

import com.pixelmongenerations.common.block.enums.EnumSpawnerAggression;
import com.pixelmongenerations.common.block.machines.PokemonRarity;
import com.pixelmongenerations.common.spawning.spawners.SpawnerAir;
import com.pixelmongenerations.common.spawning.spawners.SpawnerLand;
import com.pixelmongenerations.common.spawning.spawners.SpawnerUnderWater;
import com.pixelmongenerations.common.spawning.spawners.SpawnerUnderground;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.ArrayList;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;

public class TileEntityPixelmonGrass
extends TileEntity
implements ITickable {
    public ArrayList<PokemonRarity> pokemonList = new ArrayList();
    public int levelMin = 5;
    public int levelMax = 10;
    public boolean spawnRandom = false;
    public EnumSpawnerAggression aggression = EnumSpawnerAggression.Default;
    public int bossRatio = 100;
    public SpawnLocation spawnLocation = SpawnLocation.Land;
    public static SpawnerLand spawnerLand = new SpawnerLand();
    public static SpawnerAir spawnerAir = new SpawnerAir();
    public static SpawnerUnderground spawnerUnderground = new SpawnerUnderground();
    public static SpawnerUnderWater spawnerUnderwater = new SpawnerUnderWater();
    private boolean editing = false;
    private int tick = -1;

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setShort("levelMin", (short)this.levelMin);
        nbt.setShort("levelMax", (short)this.levelMax);
        nbt.setBoolean("spawnRandom", this.spawnRandom);
        nbt.setShort("aggression", (short)this.aggression.ordinal());
        nbt.setShort("bossRatio", (short)this.bossRatio);
        nbt.setShort("numPokemon", (short)this.pokemonList.size());
        nbt.setShort("spawnLocation", (short)this.spawnLocation.ordinal());
        for (int i = 0; i < this.pokemonList.size(); ++i) {
            nbt.setString("pokemonName" + i, this.pokemonList.get((int)i).pokemon.name);
            nbt.setShort("rarity" + i, (short)this.pokemonList.get((int)i).rarity);
            nbt.setShort("form" + i, (short)this.pokemonList.get((int)i).form);
            nbt.setShort("texture" + i, (short)this.pokemonList.get((int)i).texture);
            short chance = (short)this.pokemonList.get((int)i).shinyChance;
            nbt.setShort("shinyChance" + i, chance == 0 || chance == 1 ? (short)4096 : (short)this.pokemonList.get((int)i).shinyChance);
        }
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        if (nbt.hasKey("levelMin")) {
            this.levelMin = nbt.getShort("levelMin");
        }
        if (nbt.hasKey("levelMax")) {
            this.levelMax = nbt.getShort("levelMax");
        }
        if (nbt.hasKey("spawnRandom")) {
            this.spawnRandom = nbt.getBoolean("spawnRandom");
        }
        if (nbt.hasKey("aggression")) {
            this.aggression = EnumSpawnerAggression.getFromOrdinal(nbt.getShort("aggression"));
        }
        if (nbt.hasKey("bossRatio")) {
            this.bossRatio = nbt.getShort("bossRatio");
        }
        if (nbt.hasKey("spawnLocation")) {
            this.spawnLocation = SpawnLocation.getFromIndex(nbt.getShort("spawnLocation"));
        }
        int numPokemon = nbt.getShort("numPokemon");
        this.pokemonList.clear();
        for (int i = 0; i < numPokemon; ++i) {
            if (!nbt.hasKey("pokemonName" + i)) continue;
            int shinyChance = 4096;
            if (nbt.hasKey("shinyChance" + i)) {
                shinyChance = nbt.getShort("shinyChance" + i);
            }
            if (shinyChance < 2) {
                shinyChance = 4096;
            }
            this.pokemonList.add(new PokemonRarity(EnumSpecies.getFromName(nbt.getString("pokemonName" + i)).get(), nbt.getShort("rarity" + i), nbt.getShort("form" + i), nbt.getShort("texture" + i), shinyChance));
        }
    }

    public PokemonRarity selectPokemonForSpawn() {
        int total = 0;
        for (PokemonRarity aPokemonList1 : this.pokemonList) {
            total += aPokemonList1.rarity;
        }
        if (this.spawnRandom) {
            EnumSpecies pokemon = EnumSpecies.randomPoke();
            return new PokemonRarity(pokemon, 0, 0, 0, 4096);
        }
        if (total <= 0) {
            return null;
        }
        int rand = this.world.rand.nextInt(total);
        total = 0;
        for (PokemonRarity aPokemonList : this.pokemonList) {
            if (rand >= (total += aPokemonList.rarity)) continue;
            return aPokemonList;
        }
        return null;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
    }

    public void onActivate() {
        this.editing = true;
    }

    public void finishEdit() {
        this.editing = false;
    }

    @Override
    public void update() {
    }
}

