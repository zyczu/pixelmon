/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawning;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.block.apricornTrees.BlockApricornTree;
import com.pixelmongenerations.common.block.spawning.BlockSpawnArea;
import com.pixelmongenerations.common.block.spawning.BlockSpawnRegistry;
import com.pixelmongenerations.common.block.spawning.BlockSpawningHandler;
import com.pixelmongenerations.common.block.spawning.SpawningBlock;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.battle.EnumBattleStartTypes;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDirt;
import net.minecraft.block.BlockFarmland;
import net.minecraft.block.BlockGrass;
import net.minecraft.block.BlockHardenedClay;
import net.minecraft.block.BlockSand;
import net.minecraft.block.BlockSnow;
import net.minecraft.block.BlockSnowBlock;
import net.minecraft.block.BlockStainedHardenedClay;
import net.minecraft.block.IGrowable;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.IShearable;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockPixelmonGrass
extends SpawningBlock
implements IGrowable,
IShearable,
IPlantable {
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.1, 0.0, 0.1, 0.9, 0.8, 0.9);

    public BlockPixelmonGrass() {
        super(Material.PLANTS);
        this.setTickRandomly(true);
        this.setSoundType(SoundType.PLANT);
        this.setHardness(3.0f);
    }

    @Override
    public EnumPlantType getPlantType(IBlockAccess world, BlockPos pos) {
        return EnumPlantType.Plains;
    }

    protected boolean canPlaceBlockOn(Block ground) {
        return BlockApricornTree.isSuitableSoil(ground) || ground == Blocks.SAND;
    }

    @Override
    public void onBlockHarvested(World worldIn, BlockPos pos, IBlockState state, EntityPlayer player) {
        ItemStack item = player.getHeldItem(EnumHand.MAIN_HAND);
        if (player.capabilities.isCreativeMode) {
            return;
        }
        if (worldIn.isRemote) {
            return;
        }
        if (item.isItemEnchanted()) {
            NBTTagList tagList = item.getEnchantmentTagList();
            for (int i = 0; i < tagList.tagCount(); ++i) {
                ItemStack itemStack;
                NBTTagCompound nbttagcompound = tagList.getCompoundTagAt(i);
                short id = nbttagcompound.getShort("id");
                Enchantment enchantment = Enchantment.getEnchantmentByID(id);
                if (!enchantment.getName().replaceAll(" ", "").contains("untouching") || (itemStack = new ItemStack(this, 1, 0)) == null) continue;
                worldIn.spawnEntity(new EntityItem(worldIn, pos.getX(), pos.getY(), pos.getZ(), itemStack));
                break;
            }
        }
    }

    @Override
    public void onEntityWalk(World worldIn, BlockPos pos, Entity entityIn) {
        if (entityIn instanceof EntityPlayerMP && entityIn.posY == (double)pos.getY()) {
            BlockSpawningHandler.getInstance().performBattleStartCheck(worldIn, pos, entityIn, null, this.areaName, EnumBattleStartTypes.PUGRASSSINGLE);
        }
    }

    public void onEntityCollision(World worldIn, BlockPos pos, IBlockState state, Entity entityIn) {
        if (entityIn instanceof EntityPlayerMP) {
            if (BattleRegistry.getBattle((EntityPlayerMP)entityIn) != null) {
                return;
            }
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)entityIn);
            if (optstorage.isPresent()) {
                PlayerStorage storage = optstorage.get();
                if (storage.getTicksTillEncounter() <= 1 && entityIn.posY == (double)pos.getY()) {
                    BlockSpawningHandler.getInstance().performBattleStartCheck(worldIn, pos, entityIn, null, this.areaName, EnumBattleStartTypes.PUGRASSSINGLE);
                }
                storage.updateTicksTillEncounter();
            }
        }
    }

    @Override
    public boolean isReplaceable(IBlockAccess worldIn, BlockPos pos) {
        return false;
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return null;
    }

    @Override
    public int quantityDroppedWithBonus(int fortune, Random random) {
        return 1 + random.nextInt(fortune * 2 + 1);
    }

    @Override
    public int damageDropped(IBlockState state) {
        return state.getBlock().getMetaFromState(state);
    }

    @Override
    public boolean canGrow(World worldIn, BlockPos pos, IBlockState state, boolean isClient) {
        return true;
    }

    @Override
    public boolean canUseBonemeal(World worldIn, Random rand, BlockPos pos, IBlockState state) {
        return true;
    }

    @Override
    public void grow(World worldIn, Random rand, BlockPos pos, IBlockState state) {
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return 0;
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState();
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, new IProperty[0]);
    }

    @Override
    public List<ItemStack> onSheared(ItemStack item, IBlockAccess world, BlockPos pos, int fortune) {
        ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
        ret.add(new ItemStack(this, 1, 0));
        return ret;
    }

    @Override
    public boolean isShearable(ItemStack item, IBlockAccess world, BlockPos pos) {
        return true;
    }

    @Override
    public boolean canPlaceBlockAt(World worldIn, BlockPos pos) {
        if (!super.canPlaceBlockAt(worldIn, pos)) {
            return false;
        }
        Block block = worldIn.getBlockState(pos.down()).getBlock();
        if (this == PixelmonBlocks.pixelmonDesertGrassBlock) {
            return block instanceof BlockSand;
        }
        if (this == PixelmonBlocks.pixelmonMesaGrassBlock) {
            return block instanceof BlockSand || block instanceof BlockStainedHardenedClay || block instanceof BlockHardenedClay;
        }
        if (this == PixelmonBlocks.pixelmonSnowGrassBlock) {
            return block instanceof BlockGrass || block instanceof BlockDirt || block instanceof BlockFarmland || block instanceof BlockSnowBlock || block instanceof BlockSnow;
        }
        return block.canSustainPlant(worldIn.getBlockState(pos.down()), worldIn, pos.down(), EnumFacing.UP, this);
    }

    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
        super.neighborChanged(state, worldIn, pos, blockIn, fromPos);
        this.checkAndDropBlock(worldIn, pos, state);
    }

    @Override
    public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand) {
        this.checkAndDropBlock(worldIn, pos, state);
    }

    protected void checkAndDropBlock(World worldIn, BlockPos pos, IBlockState state) {
        if (!this.canBlockStay(worldIn, pos, state)) {
            this.dropBlockAsItem(worldIn, pos, state, 0);
            worldIn.setBlockState(pos, Blocks.AIR.getDefaultState(), 3);
        }
    }

    public boolean canBlockStay(World worldIn, BlockPos pos, IBlockState state) {
        BlockPos down = pos.down();
        Block soil = worldIn.getBlockState(down).getBlock();
        Block block = worldIn.getBlockState(pos.down()).getBlock();
        if (this == PixelmonBlocks.pixelmonDesertGrassBlock) {
            return block instanceof BlockSand;
        }
        if (this == PixelmonBlocks.pixelmonMesaGrassBlock) {
            return block instanceof BlockSand || block instanceof BlockStainedHardenedClay || block instanceof BlockHardenedClay;
        }
        if (this == PixelmonBlocks.pixelmonSnowGrassBlock) {
            return BlockApricornTree.isSuitableSoil(block);
        }
        return state.getBlock() != this ? this.canPlaceBlockOn(soil) : soil.canSustainPlant(state, worldIn, down, EnumFacing.UP, this);
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
        return null;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @SideOnly(value=Side.CLIENT)
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.CUTOUT;
    }

    @Override
    public IBlockState getPlant(IBlockAccess world, BlockPos pos) {
        IBlockState state = world.getBlockState(pos);
        return state.getBlock() != this ? this.getDefaultState() : state;
    }

    @Override
    public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
        BlockSpawnArea area;
        if (PixelmonConfig.useExternalBlockSpawnFiles && (area = BlockSpawnRegistry.getRandomAreaForBlockType(EnumBattleStartTypes.PUGRASSSINGLE)) != null) {
            this.areaName = area.name;
        }
        return super.getStateForPlacement(worldIn, pos, facing, hitX, hitY, hitZ, meta, placer);
    }
}

