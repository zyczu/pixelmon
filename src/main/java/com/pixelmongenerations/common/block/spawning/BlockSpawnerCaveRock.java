/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawning;

import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.block.spawning.BlockSpawningHandler;
import com.pixelmongenerations.common.block.spawning.SpawningBlock;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.enums.battle.EnumBattleStartTypes;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockSpawnerCaveRock
extends SpawningBlock {
    public BlockSpawnerCaveRock() {
        super(Material.ROCK);
        this.setTickRandomly(true);
        this.setSoundType(SoundType.STONE);
        this.setHardness(0.5f);
    }

    @Override
    public void onEntityWalk(World worldIn, BlockPos pos, Entity entityIn) {
        if (entityIn instanceof EntityPlayerMP) {
            if (BattleRegistry.getBattle((EntityPlayerMP)entityIn) != null) {
                return;
            }
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)entityIn);
            if (optstorage.isPresent()) {
                PlayerStorage storage = optstorage.get();
                if (storage.getTicksTillEncounter() <= 1) {
                    BlockSpawningHandler.getInstance().performBattleStartCheck(worldIn, pos, entityIn, null, this.areaName, EnumBattleStartTypes.PUGRASSSINGLE);
                }
                storage.updateTicksTillEncounter();
            }
        }
        super.onEntityWalk(worldIn, pos, entityIn);
    }

    @Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
        drops.add(new ItemStack(PixelmonBlocks.caveRockBlock));
        return drops;
    }
}

