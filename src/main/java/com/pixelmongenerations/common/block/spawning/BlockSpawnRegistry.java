/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonArray
 *  com.google.gson.JsonObject
 *  com.google.gson.JsonParser
 */
package com.pixelmongenerations.common.block.spawning;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pixelmongenerations.common.block.spawning.BlockSpawnArea;
import com.pixelmongenerations.common.block.spawning.BlockSpawnData;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.entity.pixelmon.stats.Rarity;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.battle.EnumBattleStartTypes;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import net.minecraft.util.JsonUtils;

public class BlockSpawnRegistry {
    public static HashMap<String, BlockSpawnArea> areaSpawns = new HashMap();

    public static void registerBlockSpawns() throws Exception {
        Pixelmon.LOGGER.info("Registering block spawns.");
        String path = Pixelmon.modDirectory + "/pixelmon/spawning/";
        File spawningDir = new File(path);
        if (PixelmonConfig.useExternalBlockSpawnFiles && !spawningDir.isDirectory()) {
            File baseDir = new File(Pixelmon.modDirectory + "/pixelmon");
            if (!baseDir.isDirectory()) {
                baseDir.mkdir();
            }
            Pixelmon.LOGGER.info("Creating Spawning directory.");
            spawningDir.mkdir();
            BlockSpawnRegistry.extractSpawningDir(spawningDir);
        }
        InputStream istream = !PixelmonConfig.useExternalBlockSpawnFiles ? ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/spawning/spawns.json") : new FileInputStream(new File(spawningDir, "spawns.json"));
        JsonObject json = new JsonParser().parse((Reader)new InputStreamReader(istream, StandardCharsets.UTF_8)).getAsJsonObject();
        JsonArray areArray = JsonUtils.getJsonArray(json, "areas");
        for (int i = 0; i < areArray.size(); ++i) {
            JsonObject areaElement = areArray.get(i).getAsJsonObject();
            String name = areaElement.get("name").getAsString();
            JsonArray spawnsArray = JsonUtils.getJsonArray(areaElement, "spawns");
            HashMap<String, BlockSpawnData> blockSpawns = new HashMap<String, BlockSpawnData>();
            for (int j = 0; j < spawnsArray.size(); ++j) {
                JsonObject spawnsElement = spawnsArray.get(j).getAsJsonObject();
                String pixelmonName = spawnsElement.get("pixelmon").getAsString();
                String time = spawnsElement.get("time").getAsString();
                int spawnRarity = spawnsElement.get("rarity").getAsInt();
                Rarity rarity = BlockSpawnRegistry.getRarity(time, spawnRarity);
                int minLvl = spawnsElement.get("minlevel").getAsInt();
                int maxLvl = spawnsElement.get("maxlevel").getAsInt();
                BlockSpawnData bsd = new BlockSpawnData(pixelmonName, minLvl, maxLvl, rarity);
                blockSpawns.put(pixelmonName, bsd);
            }
            String type = areaElement.get("type").getAsString();
            EnumBattleStartTypes typeEnum = null;
            switch (type) {
                case "grass": {
                    typeEnum = EnumBattleStartTypes.PUGRASSSINGLE;
                    break;
                }
                case "tallgrass": {
                    typeEnum = EnumBattleStartTypes.PUGRASSDOUBLE;
                    break;
                }
                case "seaweed": {
                    typeEnum = EnumBattleStartTypes.SEAWEED;
                    break;
                }
                case "headbutt": {
                    typeEnum = EnumBattleStartTypes.HEADBUTT;
                    break;
                }
                case "rocksmash": {
                    typeEnum = EnumBattleStartTypes.ROCKSMASH;
                }
            }
            if (typeEnum == null) {
                throw new NullPointerException("Invalid block type for area:" + name);
            }
            BlockSpawnArea bsa = new BlockSpawnArea(name, typeEnum, blockSpawns);
            areaSpawns.put(name, bsa);
        }
    }

    private static void extractSpawningDir(File spawningDir) {
        ServerNPCRegistry.class.getResourceAsStream("/assets/pixelmon/spawning/spawns.json");
        String filename = "spawns.json";
        BlockSpawnRegistry.extractFile("/assets/pixelmon/spawning/" + filename, spawningDir, filename);
    }

    private static void extractFile(String resourceName, File npcsDir, String filename) {
        try {
            File file = new File(npcsDir, filename);
            if (!file.exists()) {
                int nBytes;
                InputStream link = BlockSpawnRegistry.class.getResourceAsStream(resourceName);
                BufferedInputStream in = new BufferedInputStream(link);
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
                byte[] buffer = new byte[2048];
                while ((nBytes = in.read(buffer)) > 0) {
                    out.write(buffer, 0, nBytes);
                }
                out.flush();
                out.close();
                in.close();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Rarity getRarity(String time, int spawnRarity) {
        int day = 0;
        int night = 0;
        int duskDawn = 0;
        if (time.contains("day")) {
            day = spawnRarity;
        }
        if (time.contains("night")) {
            night = spawnRarity;
        }
        if (time.contains("dusk/dawn")) {
            duskDawn = spawnRarity;
        }
        return new Rarity(day, duskDawn, night);
    }

    public static BlockSpawnArea getRandomAreaForBlockType(EnumBattleStartTypes type) {
        ArrayList<BlockSpawnArea> areas = new ArrayList<BlockSpawnArea>();
        for (BlockSpawnArea spawnArea : areaSpawns.values()) {
            if (spawnArea.type != type) continue;
            areas.add(spawnArea);
        }
        if (areas.isEmpty()) {
            return null;
        }
        return (BlockSpawnArea)areas.get(RandomHelper.getRandomNumberBetween(0, areas.size() - 1));
    }
}

