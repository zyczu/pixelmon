/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawning;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public abstract class SpawningBlock
extends Block {
    public String areaName = "";

    public SpawningBlock(Material materialIn) {
        super(materialIn);
    }
}

