/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.block.spawning;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.events.PixelmonBlockStartingBattleEvent;
import com.pixelmongenerations.api.events.PixelmonBlockTriggeredBattleEvent;
import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.conditions.LocationType;
import com.pixelmongenerations.api.spawning.util.SpatialData;
import com.pixelmongenerations.api.world.MutableLocation;
import com.pixelmongenerations.common.battle.BattleFactory;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.block.spawning.BlockSpawnArea;
import com.pixelmongenerations.common.block.spawning.BlockSpawnData;
import com.pixelmongenerations.common.block.spawning.BlockSpawnRegistry;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.Headbutt;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.RockSmash;
import com.pixelmongenerations.common.entity.pixelmon.stats.Level;
import com.pixelmongenerations.common.spawning.PixelmonSpawning;
import com.pixelmongenerations.common.spawning.PlayerTrackingSpawner;
import com.pixelmongenerations.common.spawning.spawners.SpawnerLand;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.battle.EnumBattleStartTypes;
import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class BlockSpawningHandler {
    private static final BlockSpawningHandler instance = new BlockSpawningHandler();
    private static final Random RNG = new Random();

    public static BlockSpawningHandler getInstance() {
        return instance;
    }

    public void performBattleStartCheck(World world, BlockPos pos, Entity entity, EntityPixelmon entityPixelmon, String areaname, EnumBattleStartTypes startType) {
        EntityPlayerMP playerMP = (EntityPlayerMP)entity;
        if (BattleRegistry.getBattle(playerMP) != null) {
            return;
        }
        if (MinecraftForge.EVENT_BUS.post(new PixelmonBlockTriggeredBattleEvent(this, world, pos, playerMP, entityPixelmon, startType))) {
            return;
        }
        if (startType == EnumBattleStartTypes.PUGRASSSINGLE || startType == EnumBattleStartTypes.PUGRASSDOUBLE || startType == EnumBattleStartTypes.SEAWEED || startType == EnumBattleStartTypes.CUSTOMGRASS) {
            if (startType == EnumBattleStartTypes.PUGRASSSINGLE) {
                this.createBattle(world, pos, playerMP, areaname, EnumBattleStartTypes.PUGRASSSINGLE, null);
            } else if (startType == EnumBattleStartTypes.PUGRASSDOUBLE) {
                this.createDoubleBattle(world, pos, playerMP, areaname);
            } else if (startType == EnumBattleStartTypes.SEAWEED) {
                this.createUnderwaterBattle(world, pos, playerMP, areaname);
            } else {
                this.createBattle(world, pos, playerMP, areaname, EnumBattleStartTypes.CUSTOMGRASS, entityPixelmon);
            }
        } else if (startType == EnumBattleStartTypes.HEADBUTT) {
            if (RNG.nextInt(100) <= 40) {
                return;
            }
            this.createBattle(world, pos, playerMP, areaname, EnumBattleStartTypes.HEADBUTT, entityPixelmon);
        } else if (startType == EnumBattleStartTypes.ROCKSMASH) {
            if (RNG.nextInt(100) <= 20) {
                return;
            }
            this.createBattle(world, pos, playerMP, areaname, EnumBattleStartTypes.ROCKSMASH, entityPixelmon);
        } else if (startType == EnumBattleStartTypes.SWEETSCENT) {
            this.createBattle(world, pos, playerMP, areaname, EnumBattleStartTypes.SWEETSCENT, entityPixelmon);
        }
    }

    private void createBattle(World worldIn, BlockPos pos, EntityPlayerMP player, String areaname, EnumBattleStartTypes startType, EntityPixelmon fightingPokemon) {
        EntityPixelmon pixelmon = null;
        BlockSpawnData spawnData = null;
        Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (optstorage.isPresent()) {
            PlayerStorage storage = optstorage.get();
            if (storage.getFirstAblePokemon(worldIn) == null) {
                return;
            }
            if (startType == EnumBattleStartTypes.PUGRASSSINGLE || startType == EnumBattleStartTypes.CUSTOMGRASS) {
                EntityPixelmon startingPixelmon = storage.getFirstAblePokemon(worldIn);
                if (startType != EnumBattleStartTypes.CUSTOMGRASS && areaname.isEmpty()) {
                    SpawnerLand spawner = new SpawnerLand();
                    String pokemonName = this.getPixelmonNameFromSpawner(worldIn, pos, spawner);
                    if (pokemonName.isEmpty()) {
                        return;
                    }
                    pixelmon = (EntityPixelmon)PixelmonEntityList.createEntityByName(pokemonName, worldIn);
                    pixelmon.setLocationAndAngles(pos.getX(), (float)pos.getY() + spawner.getYOffset(pos.getX(), pos.getY(), pos.getZ(), pixelmon), pos.getZ(), worldIn.rand.nextFloat() * 360.0f, 0.0f);
                } else if (startType == EnumBattleStartTypes.CUSTOMGRASS) {
                    pixelmon = fightingPokemon;
                } else if (PixelmonSpawning.coordinator != null) {
                    SpawnLocation spawnLocation = new SpawnLocation(new MutableLocation(worldIn, pos), LocationType.getPotentialTypes(worldIn.getBlockState(player.getPosition().add(0, -1, 0))), worldIn.getBlockState(pos).getBlock(), null, worldIn.getBiome(pos), worldIn.canSeeSky(pos), 6);
                    ArrayList<SpawnLocation> spawnLocations = new ArrayList<SpawnLocation>();
                    spawnLocations.add(spawnLocation);
                    PlayerTrackingSpawner playerTrackingSpawner = new PlayerTrackingSpawner(player.getUniqueID());
                    for (AbstractSpawner spawner : PixelmonSpawning.coordinator.spawners) {
                        if (!(spawner instanceof PlayerTrackingSpawner) || ((PlayerTrackingSpawner)spawner).playerUUID.compareTo(player.getUniqueID()) != 0) continue;
                        playerTrackingSpawner = (PlayerTrackingSpawner)spawner;
                    }
                    ArrayList<SpawnAction<? extends Entity>> possibleSpawns = playerTrackingSpawner.calculateSpawnActions(spawnLocations);
                    if (possibleSpawns.isEmpty()) {
                        return;
                    }
                    SpawnAction<? extends Entity> spawnAction = possibleSpawns.get(0);
                    Entity entity = spawnAction.getOrCreateEntity();
                    if (entity instanceof EntityPixelmon) {
                        pixelmon = (EntityPixelmon)entity;
                    }
                }
                if (!worldIn.isRemote && pixelmon != null) {
                    worldIn.spawnEntity(pixelmon);
                }
                if (PixelmonConfig.scaleGrassBattles && spawnData == null) {
                    int minLvl;
                    int maxLvl = Math.min(storage.getHighestLevel(), PixelmonServerConfig.maxLevel);
                    if (maxLvl - (minLvl = this.getLowestLevel(storage.getList())) > 5) {
                        minLvl = maxLvl - minLvl;
                    }
                    Level level = pixelmon.getLvl();
                    if (minLvl < maxLvl) {
                        int lvl = RNG.nextInt(maxLvl - minLvl) + minLvl;
                        level.setLevel(lvl);
                    } else {
                        level.setLevel(maxLvl);
                    }
                    pixelmon.loadMoveset();
                } else if (spawnData != null) {
                    int minLvl = spawnData.minLvl;
                    int maxLvl = spawnData.maxLvl;
                    if (minLvl < maxLvl) {
                        int lvl = RNG.nextInt(maxLvl - minLvl) + minLvl;
                        pixelmon.getLvl().setLevel(lvl);
                    } else {
                        pixelmon.getLvl().setLevel(maxLvl);
                    }
                    pixelmon.loadMoveset();
                }
                if (MinecraftForge.EVENT_BUS.post(new PixelmonBlockStartingBattleEvent(worldIn, pos, player, startType, fightingPokemon, pixelmon, null))) {
                    return;
                }
                BattleFactory.createBattle().team1(new PlayerParticipant(player, startingPixelmon)).team2(new WildPixelmonParticipant(true, pixelmon)).startBattle();
            } else if (startType == EnumBattleStartTypes.HEADBUTT) {
                if (Headbutt.HEADBUTT_SPAWNER != null && PixelmonSpawning.coordinator != null) {
                    SpawnLocation spawnLocation = new SpawnLocation(new MutableLocation(worldIn, pos), LocationType.getPotentialTypes(worldIn.getBlockState(player.getPosition().add(0, -1, 0))), worldIn.getBlockState(pos).getBlock(), null, worldIn.getBiome(pos), worldIn.canSeeSky(pos), 6);
                    Entity entity = Headbutt.HEADBUTT_SPAWNER.trigger(spawnLocation);
                    if (entity instanceof EntityPixelmon) {
                        pixelmon = (EntityPixelmon)entity;
                    }
                } else {
                    int heatbuttSelector = RNG.nextInt(EnumSpecies.headbuttEncounters.size());
                    String pokemonName = EnumSpecies.getFromName((String)EnumSpecies.headbuttEncounters.get((int)heatbuttSelector)).orElse((EnumSpecies)EnumSpecies.Magikarp).name;
                    while (pokemonName.equals(EnumSpecies.Magikarp.name)) {
                        heatbuttSelector = RNG.nextInt(EnumSpecies.headbuttEncounters.size());
                        pokemonName = EnumSpecies.getFromName((String)EnumSpecies.headbuttEncounters.get((int)heatbuttSelector)).orElse((EnumSpecies)EnumSpecies.Magikarp).name;
                    }
                    pixelmon = (EntityPixelmon)PixelmonEntityList.createEntityByName(pokemonName, worldIn);
                    pixelmon.setLocationAndAngles(pos.getX(), pos.up().getY(), pos.getZ(), worldIn.rand.nextFloat() * 360.0f, 0.0f);
                    if (!worldIn.isRemote) {
                        worldIn.spawnEntity(pixelmon);
                    }
                }
                if (pixelmon == null || MinecraftForge.EVENT_BUS.post(new PixelmonBlockStartingBattleEvent(worldIn, pos, player, startType, fightingPokemon, pixelmon, null))) {
                    return;
                }
                BattleFactory.createBattle().team1(new PlayerParticipant(player, fightingPokemon)).team2(new WildPixelmonParticipant(true, pixelmon)).startBattle();
            } else if (startType == EnumBattleStartTypes.ROCKSMASH) {
                if (RockSmash.ROCK_SMASH_SPAWNER != null && PixelmonSpawning.coordinator != null) {
                    SpatialData data = RockSmash.ROCK_SMASH_SPAWNER.calculateSpatialData(worldIn, pos.up(), 10, true, block -> true);
                    SpawnLocation spawnLocation = new SpawnLocation(new MutableLocation(worldIn, pos), Lists.newArrayList(LocationType.ROCK_SMASH), data.baseBlock, data.uniqueSurroundingBlocks, worldIn.getBiome(pos), worldIn.canSeeSky(pos), 10);
                    Entity entity = RockSmash.ROCK_SMASH_SPAWNER.trigger(spawnLocation);
                    if (entity instanceof EntityPixelmon) {
                        pixelmon = (EntityPixelmon)entity;
                    }
                } else {
                    int rocksmashSelector = RNG.nextInt(EnumSpecies.rockSmashEncounters.size());
                    String pokemonName = EnumSpecies.getFromName((String)EnumSpecies.rockSmashEncounters.get((int)rocksmashSelector)).orElse((EnumSpecies)EnumSpecies.Magikarp).name;
                    while (pokemonName.equals(EnumSpecies.Magikarp.name)) {
                        rocksmashSelector = RNG.nextInt(EnumSpecies.rockSmashEncounters.size());
                        pokemonName = EnumSpecies.getFromName((String)EnumSpecies.rockSmashEncounters.get((int)rocksmashSelector)).orElse((EnumSpecies)EnumSpecies.Magikarp).name;
                    }
                    pixelmon = (EntityPixelmon)PixelmonEntityList.createEntityByName(pokemonName, worldIn);
                    pixelmon.setLocationAndAngles(pos.getX(), pos.up().getY(), pos.getZ(), worldIn.rand.nextFloat() * 360.0f, 0.0f);
                    if (!worldIn.isRemote) {
                        worldIn.spawnEntity(pixelmon);
                    }
                }
                if (pixelmon == null || MinecraftForge.EVENT_BUS.post(new PixelmonBlockStartingBattleEvent(worldIn, pos, player, startType, fightingPokemon, pixelmon, null))) {
                    return;
                }
                BattleFactory.createBattle().team1(new PlayerParticipant(player, fightingPokemon)).team2(new WildPixelmonParticipant(true, pixelmon)).startBattle();
            } else if (startType == EnumBattleStartTypes.SWEETSCENT && PixelmonSpawning.coordinator != null) {
                SpawnLocation spawnLocation = new SpawnLocation(new MutableLocation(worldIn, pos), LocationType.getPotentialTypes(worldIn.getBlockState(player.getPosition().add(0, -1, 0))), worldIn.getBlockState(pos).getBlock(), null, worldIn.getBiome(pos), worldIn.canSeeSky(pos), 6);
                ArrayList<SpawnLocation> spawnLocations = new ArrayList<SpawnLocation>();
                spawnLocations.add(spawnLocation);
                PlayerTrackingSpawner playerTrackingSpawner = new PlayerTrackingSpawner(player.getUniqueID());
                for (AbstractSpawner spawner : PixelmonSpawning.coordinator.spawners) {
                    if (!(spawner instanceof PlayerTrackingSpawner) || ((PlayerTrackingSpawner)spawner).playerUUID.compareTo(player.getUniqueID()) != 0) continue;
                    playerTrackingSpawner = (PlayerTrackingSpawner)spawner;
                }
                ArrayList<SpawnAction<? extends Entity>> possibleSpawns = playerTrackingSpawner.calculateSpawnActions(spawnLocations);
                if (possibleSpawns.isEmpty()) {
                    return;
                }
                SpawnAction<? extends Entity> spawnAction = possibleSpawns.get(0);
                Entity entity = spawnAction.getOrCreateEntity();
                if (entity instanceof EntityPixelmon) {
                    pixelmon = (EntityPixelmon)entity;
                }
                if (pixelmon == null || MinecraftForge.EVENT_BUS.post(new PixelmonBlockStartingBattleEvent(worldIn, pos, player, startType, fightingPokemon, pixelmon, null))) {
                    return;
                }
                BattleFactory.createBattle().team1(new PlayerParticipant(player, fightingPokemon)).team2(new WildPixelmonParticipant(true, pixelmon)).startBattle();
            }
        }
    }

    private void createDoubleBattle(World worldIn, BlockPos pos, EntityPlayerMP player, String areaname) {
        Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (optstorage.isPresent()) {
            int minLvl;
            EntityPixelmon pixelmon2;
            EntityPixelmon pixelmon1;
            PlayerStorage storage = optstorage.get();
            if (storage.getFirstAblePokemon(worldIn) == null) {
                return;
            }
            BlockSpawnData spawnData1 = null;
            BlockSpawnData spawnData2 = null;
            if (areaname.isEmpty()) {
                SpawnerLand spawner = new SpawnerLand();
                String pokemonName = this.getPixelmonNameFromSpawner(worldIn, pos, spawner);
                if (pokemonName.isEmpty()) {
                    return;
                }
                pixelmon1 = (EntityPixelmon)PixelmonEntityList.createEntityByName(pokemonName, worldIn);
                pixelmon1.setLocationAndAngles(pos.getX(), (float)pos.getY() + spawner.getYOffset(pos.getX(), pos.getY(), pos.getZ(), pixelmon1), pos.getZ(), worldIn.rand.nextFloat() * 360.0f, 0.0f);
                pokemonName = this.getPixelmonNameFromSpawner(worldIn, pos, spawner);
                pixelmon2 = (EntityPixelmon)PixelmonEntityList.createEntityByName(pokemonName, worldIn);
                pixelmon2.setLocationAndAngles(pos.getX(), (float)pos.getY() + spawner.getYOffset(pos.getX(), pos.getY(), pos.getZ(), pixelmon2), pos.getZ(), worldIn.rand.nextFloat() * 360.0f, 0.0f);
            } else {
                BlockSpawnArea spawnArea = BlockSpawnRegistry.areaSpawns.get(areaname);
                String pokemonName = this.getPixelmonNameFromArea(worldIn, spawnArea);
                if (pokemonName == null) {
                    return;
                }
                spawnData1 = spawnArea.blockSpawns.get(pokemonName);
                pixelmon1 = (EntityPixelmon)PixelmonEntityList.createEntityByName(pokemonName, worldIn);
                pixelmon1.setLocationAndAngles(pos.getX(), pos.getY(), pos.getZ(), worldIn.rand.nextFloat() * 360.0f, 0.0f);
                pokemonName = this.getPixelmonNameFromArea(worldIn, spawnArea);
                spawnData2 = spawnArea.blockSpawns.get(pokemonName);
                pixelmon2 = (EntityPixelmon)PixelmonEntityList.createEntityByName(pokemonName, worldIn);
                pixelmon2.setLocationAndAngles(pos.getX(), pos.getY(), pos.getZ(), worldIn.rand.nextFloat() * 360.0f, 0.0f);
            }
            if (!worldIn.isRemote) {
                worldIn.spawnEntity(pixelmon1);
                worldIn.spawnEntity(pixelmon2);
            }
            if (PixelmonConfig.scaleGrassBattles && spawnData1 == null && spawnData2 == null) {
                int maxLvl = storage.getHighestLevel();
                minLvl = this.getLowestLevel(storage.getList());
                if (minLvl < maxLvl) {
                    int lvl = RNG.nextInt(maxLvl - minLvl) + minLvl;
                    pixelmon1.getLvl().setLevel(lvl);
                    lvl = RNG.nextInt(maxLvl - minLvl) + minLvl;
                    pixelmon2.getLvl().setLevel(lvl);
                } else {
                    pixelmon1.getLvl().setLevel(maxLvl);
                    pixelmon2.getLvl().setLevel(maxLvl);
                }
                pixelmon1.loadMoveset();
                pixelmon2.loadMoveset();
            } else if (spawnData1 != null && spawnData2 != null) {
                minLvl = spawnData1.minLvl;
                int maxLvl = spawnData1.maxLvl;
                if (minLvl < maxLvl) {
                    int lvl = RNG.nextInt(maxLvl - minLvl) + minLvl;
                    pixelmon1.getLvl().setLevel(lvl);
                } else {
                    pixelmon1.getLvl().setLevel(maxLvl);
                }
                pixelmon1.loadMoveset();
                minLvl = spawnData2.minLvl;
                maxLvl = spawnData2.maxLvl;
                if (minLvl < maxLvl) {
                    int lvl = RNG.nextInt(maxLvl - minLvl) + minLvl;
                    pixelmon2.getLvl().setLevel(lvl);
                } else {
                    pixelmon2.getLvl().setLevel(maxLvl);
                }
                pixelmon2.loadMoveset();
            }
            EntityPixelmon primaryPixelmon = storage.getFirstAblePokemon(worldIn);
            EntityPixelmon secondaryPixelmon = storage.getSecondAblePokemon(worldIn);
            if (MinecraftForge.EVENT_BUS.post(new PixelmonBlockStartingBattleEvent(worldIn, pos, player, EnumBattleStartTypes.PUGRASSDOUBLE, null, pixelmon1, pixelmon2))) {
                return;
            }
            BattleFactory.createBattle().team1(new PlayerParticipant(player, primaryPixelmon, secondaryPixelmon)).team2(new WildPixelmonParticipant(true, pixelmon1, pixelmon2)).type(EnumBattleType.Double).startBattle();
        }
    }

    private void createUnderwaterBattle(World worldIn, BlockPos pos, EntityPlayerMP player, String areaname) {
        EntityPixelmon pixelmon = null;
        BlockSpawnData spawnData = null;
        Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (optstorage.isPresent()) {
            PlayerStorage storage = optstorage.get();
            if (storage.getFirstAblePokemon(worldIn) == null) {
                return;
            }
            EntityPixelmon startingPixelmon = storage.getFirstAblePokemon(worldIn);
            if (areaname.isEmpty()) {
                SpawnerLand spawner = new SpawnerLand();
                String pokemonName = this.getPixelmonNameFromSpawner(worldIn, pos, spawner);
                if (pokemonName.isEmpty()) {
                    return;
                }
                pixelmon = (EntityPixelmon)PixelmonEntityList.createEntityByName(pokemonName, worldIn);
                pixelmon.setLocationAndAngles(pos.getX(), (float)pos.getY() + spawner.getYOffset(pos.getX(), pos.getY(), pos.getZ(), pixelmon), pos.getZ(), worldIn.rand.nextFloat() * 360.0f, 0.0f);
            } else {
                BlockSpawnArea spawnArea = BlockSpawnRegistry.areaSpawns.get(areaname);
                String pokemonName = this.getPixelmonNameFromArea(worldIn, spawnArea);
                if (pokemonName == null) {
                    return;
                }
                spawnData = spawnArea.blockSpawns.get(pokemonName);
                pixelmon = (EntityPixelmon)PixelmonEntityList.createEntityByName(pokemonName, worldIn);
                pixelmon.setLocationAndAngles(pos.getX(), pos.getY(), pos.getZ(), worldIn.rand.nextFloat() * 360.0f, 0.0f);
            }
            if (!worldIn.isRemote) {
                worldIn.spawnEntity(pixelmon);
            }
            if (PixelmonConfig.scaleGrassBattles && spawnData == null) {
                int minLvl;
                int maxLvl = storage.getHighestLevel();
                if (maxLvl - (minLvl = this.getLowestLevel(storage.getList())) > 5) {
                    minLvl = maxLvl - minLvl;
                }
                if (minLvl < maxLvl) {
                    int lvl = RNG.nextInt(maxLvl - minLvl) + minLvl;
                    pixelmon.getLvl().setLevel(lvl);
                } else {
                    pixelmon.getLvl().setLevel(maxLvl);
                }
                pixelmon.loadMoveset();
            } else if (spawnData != null) {
                int minLvl = spawnData.minLvl;
                int maxLvl = spawnData.maxLvl;
                if (minLvl < maxLvl) {
                    int lvl = RNG.nextInt(maxLvl - minLvl) + minLvl;
                    pixelmon.getLvl().setLevel(lvl);
                } else {
                    pixelmon.getLvl().setLevel(maxLvl);
                }
                pixelmon.loadMoveset();
            }
            if (MinecraftForge.EVENT_BUS.post(new PixelmonBlockStartingBattleEvent(worldIn, pos, player, EnumBattleStartTypes.SEAWEED, null, pixelmon, null))) {
                return;
            }
            BattleFactory.createBattle().team1(new PlayerParticipant(player, startingPixelmon)).team2(new WildPixelmonParticipant(true, pixelmon)).startBattle();
        }
    }

    public int getLowestLevel(NBTTagCompound[] partyPokemon) {
        int lvl = 100;
        for (NBTTagCompound nbt : partyPokemon) {
            if (nbt == null || nbt.getInteger("Level") >= lvl) continue;
            lvl = nbt.getInteger("Level");
        }
        return lvl;
    }

    private String getPixelmonNameFromSpawner(World worldIn, BlockPos pos, SpawnerLand spawner) {
        String biomeName = worldIn.getBiomeForCoordsBody(pos).getRegistryName().toString();
        String pokemonName = spawner.getRandomEntity(worldIn, RNG, biomeName, pos);
        if (!PixelmonConfig.blocksHaveLegendaries) {
            while (EnumSpecies.legendaries.contains(pokemonName)) {
                pokemonName = spawner.getRandomEntity(worldIn, RNG, biomeName, pos);
                while (!EnumSpecies.hasPokemonAnyCase(pokemonName)) {
                    pokemonName = spawner.getRandomEntity(worldIn, RNG, biomeName, pos);
                }
            }
        }
        return pokemonName;
    }

    private String getPixelmonNameFromArea(World world, BlockSpawnArea spawnArea) {
        String pokemonName = spawnArea.getRandomPokemon(world);
        if (!PixelmonConfig.blocksHaveLegendaries) {
            while (EnumSpecies.legendaries.contains(pokemonName)) {
                pokemonName = spawnArea.getRandomPokemon(world);
                while (!EnumSpecies.hasPokemonAnyCase(pokemonName)) {
                    pokemonName = spawnArea.getRandomPokemon(world);
                }
            }
        }
        return pokemonName;
    }
}

