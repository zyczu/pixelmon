/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.base.Predicate
 *  com.google.common.base.Predicates
 */
package com.pixelmongenerations.common.block.spawning;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.pixelmongenerations.common.block.enums.EnumSpawnerAggression;
import com.pixelmongenerations.common.block.machines.PokemonRarity;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.EnumAggression;
import com.pixelmongenerations.common.spawning.spawners.SpawnerAir;
import com.pixelmongenerations.common.spawning.spawners.SpawnerLand;
import com.pixelmongenerations.common.spawning.spawners.SpawnerUnderWater;
import com.pixelmongenerations.common.spawning.spawners.SpawnerUnderground;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EntitySelectors;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TileEntityPixelmonSpawner
extends TileEntity
implements ITickable {
    public ArrayList<PokemonRarity> pokemonList = new ArrayList();
    public int spawnTick = 40;
    public int spawnRadius = 8;
    public int maxSpawns = 5;
    public int levelMin = 5;
    public int levelMax = 10;
    public int activationRange = 16;
    public boolean fireOnTick = true;
    public boolean spawnRandom = false;
    public EnumSpawnerAggression aggression = EnumSpawnerAggression.Default;
    public int bossRatio = 100;
    public ArrayList<EntityPixelmon> spawnedPokemon = new ArrayList();
    public SpawnLocation spawnLocation = SpawnLocation.Land;
    public static SpawnerLand spawnerLand = new SpawnerLand();
    public static SpawnerAir spawnerAir = new SpawnerAir();
    public static SpawnerUnderground spawnerUnderground = new SpawnerUnderground();
    public static SpawnerUnderWater spawnerUnderwater = new SpawnerUnderWater();
    private boolean editing = false;
    private int tick = -1;

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setInteger("spawnTick", this.spawnTick);
        nbt.setShort("spawnRadius", (short)this.spawnRadius);
        nbt.setShort("maxSpawns", (short)this.maxSpawns);
        nbt.setShort("levelMin", (short)this.levelMin);
        nbt.setShort("levelMax", (short)this.levelMax);
        nbt.setBoolean("fireOnTick", this.fireOnTick);
        nbt.setBoolean("spawnRandom", this.spawnRandom);
        nbt.setShort("aggression", (short)this.aggression.ordinal());
        nbt.setShort("bossRatio", (short)this.bossRatio);
        nbt.setShort("numPokemon", (short)this.pokemonList.size());
        nbt.setShort("spawnLocation", (short)this.spawnLocation.ordinal());
        nbt.setShort("activationRange", (short)this.activationRange);
        for (int i = 0; i < this.pokemonList.size(); ++i) {
            nbt.setString("pokemonName" + i, this.pokemonList.get((int)i).pokemon.name);
            nbt.setShort("rarity" + i, (short)this.pokemonList.get((int)i).rarity);
            nbt.setShort("form" + i, (short)this.pokemonList.get((int)i).form);
            nbt.setShort("texture" + i, (short)this.pokemonList.get((int)i).texture);
            short chance = (short)this.pokemonList.get((int)i).shinyChance;
            nbt.setShort("shinyChance" + i, chance == 0 || chance == 1 ? (short)4096 : (short)this.pokemonList.get((int)i).shinyChance);
        }
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.spawnTick = nbt.getInteger("spawnTick");
        this.spawnRadius = nbt.getShort("spawnRadius");
        this.maxSpawns = nbt.getShort("maxSpawns");
        if (nbt.hasKey("levelMin")) {
            this.levelMin = nbt.getShort("levelMin");
        }
        if (nbt.hasKey("levelMax")) {
            this.levelMax = nbt.getShort("levelMax");
        }
        if (nbt.hasKey("fireOnTick")) {
            this.fireOnTick = nbt.getBoolean("fireOnTick");
        }
        if (nbt.hasKey("spawnRandom")) {
            this.spawnRandom = nbt.getBoolean("spawnRandom");
        }
        if (nbt.hasKey("aggression")) {
            this.aggression = EnumSpawnerAggression.getFromOrdinal(nbt.getShort("aggression"));
        }
        if (nbt.hasKey("bossRatio")) {
            this.bossRatio = nbt.getShort("bossRatio");
        }
        if (nbt.hasKey("spawnLocation")) {
            this.spawnLocation = SpawnLocation.getFromIndex(nbt.getShort("spawnLocation"));
        }
        if (nbt.hasKey("activationRange")) {
            this.activationRange = nbt.getShort("activationRange");
        }
        int numPokemon = nbt.getShort("numPokemon");
        this.pokemonList.clear();
        for (int i = 0; i < numPokemon; ++i) {
            if (!nbt.hasKey("pokemonName" + i)) continue;
            int shinyChance = 4096;
            if (nbt.hasKey("shinyChance" + i)) {
                shinyChance = nbt.getShort("shinyChance" + i);
            }
            if (shinyChance < 2) {
                shinyChance = 4096;
            }
            this.pokemonList.add(new PokemonRarity(EnumSpecies.getFromName(nbt.getString("pokemonName" + i)).get(), nbt.getShort("rarity" + i), nbt.getShort("form" + i), nbt.getShort("texture" + i), shinyChance));
        }
    }

    @Override
    public void update() {
        if (!this.world.isRemote && this.fireOnTick && !this.editing) {
            boolean shouldActivate;
            Predicate VALID_PLAYER = Predicates.and(EntitySelectors.IS_ALIVE, EntitySelectors.withinRange(this.pos.getX(), this.pos.getY(), this.pos.getZ(), this.activationRange));
            boolean bl = shouldActivate = this.world.getPlayers(EntityPlayerMP.class, VALID_PLAYER).size() > 0;
            if (shouldActivate) {
                this.doSpawning(false);
            }
        }
    }

    private void spawnPixelmon(PokemonRarity p) {
        this.checkForDead();
        if (this.spawnedPokemon.size() >= this.maxSpawns) {
            return;
        }
        if (this.spawnLocation == null) {
            return;
        }
        int x = this.pos.getX() + this.world.rand.nextInt(this.spawnRadius * 2 + 1) - this.spawnRadius;
        Integer y = this.pos.getY();
        int z = this.pos.getZ() + this.world.rand.nextInt(this.spawnRadius * 2 + 1) - this.spawnRadius;
        boolean valid = false;
        if (this.spawnLocation == SpawnLocation.Land) {
            valid = (y = spawnerLand.getSpawnConditionY(this.world, new BlockPos(x, y, z))) != null && spawnerLand.canPokemonSpawnHere(this.world, new BlockPos(x, y, z));
        } else if (this.spawnLocation == SpawnLocation.Air) {
            valid = (y = Integer.valueOf(this.getTopSolidBlock(x, y, z))) != null && spawnerLand.canPokemonSpawnHere(this.world, new BlockPos(x, y, z));
        } else if (this.spawnLocation == SpawnLocation.AirPersistent) {
            Integer ytmp = this.getFirstAirBlock(x, y, z);
            if (ytmp != null) {
                y = ytmp;
                valid = spawnerAir.canPokemonSpawnHere(this.world, new BlockPos(x, y, z));
            }
        } else if (this.spawnLocation == SpawnLocation.Water) {
            Integer ytmp = this.getFirstWaterBlock(x, y, z);
            if (ytmp != null) {
                y = ytmp;
                valid = spawnerUnderwater.canPokemonSpawnHere(this.world, new BlockPos(x, y + 1, z));
            }
        } else if (this.spawnLocation == SpawnLocation.UnderGround) {
            y = this.getTopSolidBlock(x, y, z);
            valid = spawnerUnderground.canPokemonSpawnHere(this.world, new BlockPos(x, y, z));
        }
        if (valid) {
            EntityPixelmon pokemon = (EntityPixelmon)PixelmonEntityList.createEntityByName(p.pokemon.name, this.world);
            pokemon.setPosition((double)x + 0.5, y.intValue(), (double)z + 0.5);
            pokemon.setForm(p.form);
            pokemon.setSpecialTexture(p.texture);
            pokemon.setShiny(RandomHelper.getRandomChance(1.0f / (float)p.shinyChance));
            if (this.bossRatio > 0 && this.world.rand.nextInt(this.bossRatio) == 0) {
                pokemon.setBoss(EnumBossMode.getRandomMode());
            }
            if (this.aggression != EnumSpawnerAggression.Default) {
                if (this.aggression == EnumSpawnerAggression.Timid) {
                    pokemon.aggression = EnumAggression.timid;
                } else if (this.aggression == EnumSpawnerAggression.Passive) {
                    pokemon.aggression = EnumAggression.passive;
                } else if (this.aggression == EnumSpawnerAggression.Aggressive) {
                    pokemon.aggression = EnumAggression.aggressive;
                }
            }
            int level = this.world.rand.nextInt(this.levelMax + 1 - this.levelMin) + this.levelMin;
            pokemon.setSpawnLocation(this.spawnLocation);
            pokemon.getLvl().setLevel(level);
            pokemon.setSpawnerParent(this);
            this.world.spawnEntity(pokemon);
            this.spawnedPokemon.add(pokemon);
        }
    }

    private int getTopSolidBlock(int x, int y, int z) {
        Material blockMaterial;
        BlockPos pos;
        int i;
        boolean valid = false;
        for (i = 1; i <= this.spawnRadius / 2; ++i) {
            pos = new BlockPos(x, y + i, z);
            blockMaterial = this.world.getBlockState(pos).getMaterial();
            if (blockMaterial != Material.AIR && blockMaterial != Material.SNOW || !this.isSolidSurface(this.world, pos)) continue;
            y += i;
            valid = true;
            break;
        }
        if (!valid) {
            for (i = 1; i <= this.spawnRadius / 2; ++i) {
                pos = new BlockPos(x, y - i, z);
                blockMaterial = this.world.getBlockState(pos).getMaterial();
                if (blockMaterial != Material.AIR && blockMaterial != Material.SNOW || !this.isSolidSurface(this.world, pos)) continue;
                y -= i;
                break;
            }
        }
        return y + 1;
    }

    private boolean isSolidSurface(World worldIn, BlockPos pos) {
        return worldIn.getBlockState(pos.down()).isSideSolid(worldIn, pos, EnumFacing.UP) && !worldIn.getBlockState(pos).getMaterial().isSolid() && !worldIn.getBlockState(pos.up()).getMaterial().isSolid();
    }

    private Integer getFirstAirBlock(int x, int y, int z) {
        int i = 0;
        while (this.world.getBlockState(new BlockPos(x, y + i, z)).getMaterial() != Material.AIR) {
            if (i > this.spawnRadius / 2) {
                return null;
            }
            ++i;
        }
        return y + i;
    }

    private Integer getFirstWaterBlock(int x, int y, int z) {
        int i = 0;
        while (this.world.getBlockState(new BlockPos(x, y + i, z)).getMaterial() != Material.WATER) {
            if (this.world.getBlockState(new BlockPos(x, y + i, z)).getMaterial() == Material.AIR) {
                return null;
            }
            ++i;
        }
        return y + i;
    }

    private void doSpawning(boolean override) {
        if (this.tick == 0 || override) {
            PokemonRarity p = this.selectPokemonForSpawn();
            if (p == null) {
                return;
            }
            this.spawnPixelmon(p);
            this.resetSpawnTick();
            if (override) {
                return;
            }
        }
        if (this.tick == -1) {
            this.resetSpawnTick();
        }
        --this.tick;
    }

    private void checkForDead() {
        for (int i = 0; i < this.spawnedPokemon.size(); ++i) {
            EntityPixelmon p = this.spawnedPokemon.get(i);
            if (p.isLoaded(false) && !p.isDead) continue;
            this.spawnedPokemon.remove(i);
            --i;
        }
    }

    private void resetSpawnTick() {
        this.tick = (int)((double)this.spawnTick * (1.0 + (this.world.rand.nextDouble() - 0.5) * 0.2));
    }

    private PokemonRarity selectPokemonForSpawn() {
        int total = 0;
        for (PokemonRarity aPokemonList1 : this.pokemonList) {
            total += aPokemonList1.rarity;
        }
        if (this.spawnRandom) {
            EnumSpecies pokemon = EnumSpecies.randomPoke();
            return new PokemonRarity(pokemon, 0, 0, 0, 4096);
        }
        if (total <= 0) {
            return null;
        }
        int rand = this.world.rand.nextInt(total);
        total = 0;
        for (PokemonRarity aPokemonList : this.pokemonList) {
            if (rand >= (total += aPokemonList.rarity)) continue;
            return aPokemonList;
        }
        return null;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
    }

    public void updateRedstone() {
        if (!this.fireOnTick && !this.editing) {
            this.doSpawning(true);
        }
    }

    public void onActivate() {
        this.editing = true;
    }

    public void finishEdit() {
        this.editing = false;
        this.resetSpawnTick();
        while (!this.spawnedPokemon.isEmpty()) {
            this.spawnedPokemon.get(0).unloadEntity();
            this.spawnedPokemon.remove(0);
        }
    }

    public void despawnAllPokemon() {
        while (!this.spawnedPokemon.isEmpty()) {
            this.spawnedPokemon.get(0).unloadEntity();
            this.spawnedPokemon.remove(0);
        }
    }
}

