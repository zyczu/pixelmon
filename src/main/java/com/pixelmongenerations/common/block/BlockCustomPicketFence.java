/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.common.block.BlockPicketFence;
import com.pixelmongenerations.common.block.enums.ColorEnum;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPicketFenceNormal;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockCustomPicketFence
extends BlockPicketFence {
    private ColorEnum colorEnum;

    public BlockCustomPicketFence() {
    }

    public BlockCustomPicketFence(ColorEnum colorEnum) {
        this.colorEnum = colorEnum;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityPicketFenceNormal(this.colorEnum == null ? "picketFenceTexture.png" : this.colorEnum.name().toLowerCase() + "picketFenceTexture.png");
    }
}

