/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawnmethod;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityTaoTrioShrine;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockTaoTrioShrine
extends BlockShrine {
    public BlockTaoTrioShrine() {
        this.setTranslationKey("tao_trio_shrine");
        this.setBlockUnbreakable();
    }

    @Override
    public TileEntityShrine createTileEntity() {
        return new TileEntityTaoTrioShrine();
    }

    @Override
    protected boolean checkExtraRequirements(EntityPlayer player, World world, BlockPos pos) {
        return true;
    }

    @Override
    public boolean canAccept(PokemonGroup group) {
        return group.contains(EnumSpecies.Kyurem) || group.contains(EnumSpecies.Zekrom) || group.contains(EnumSpecies.Reshiram);
    }
}

