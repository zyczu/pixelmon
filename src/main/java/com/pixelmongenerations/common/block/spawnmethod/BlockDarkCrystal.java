/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.block.spawnmethod;

import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityDarkCrystal;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import javax.annotation.Nullable;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockDarkCrystal
extends GenericRotatableModelBlock {
    public BlockDarkCrystal() {
        super(Material.ROCK);
        this.setHardness(2.5f);
        this.setTranslationKey("dark_crystal");
        this.setCreativeTab(PixelmonCreativeTabs.shrines);
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!worldIn.isRemote && hand == EnumHand.MAIN_HAND) {
            ItemStack stack = playerIn.getHeldItem(hand);
            if (stack.getItem() == Item.getItemFromBlock(this)) {
                stack.shrink(1);
                worldIn.setBlockState(pos, this.getDefaultState());
            } else {
                DropItemHelper.giveItemStackToPlayer(playerIn, new ItemStack(Item.getItemFromBlock(this)));
                worldIn.setBlockToAir(pos);
            }
            return true;
        }
        return false;
    }

    @Override
    @Nullable
    public TileEntity createTileEntity(World world, IBlockState state) {
        return new TileEntityDarkCrystal();
    }
}

