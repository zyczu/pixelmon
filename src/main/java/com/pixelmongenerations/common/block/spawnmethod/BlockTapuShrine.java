/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.block.spawnmethod;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityTapuShrine;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.FriendShip;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockTapuShrine
extends BlockShrine {
    @Override
    public TileEntityShrine createTileEntity() {
        return new TileEntityTapuShrine();
    }

    @Override
    protected boolean checkExtraRequirements(EntityPlayer player, World world, BlockPos pos) {
        PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
        for (int i = 0; i < 6; ++i) {
            EntityPixelmon pokemon = storage.getPokemon(storage.getIDFromPosition(i), player.world);
            if (pokemon == null) continue;
            if (!Lists.newArrayList((Object[])new EnumSpecies[]{EnumSpecies.Cosmog, EnumSpecies.Cosmoem, EnumSpecies.Solgaleo, EnumSpecies.Lunala}).contains((Object)pokemon.getSpecies()) || pokemon.friendship.getFriendship() < FriendShip.getMaxFriendship()) continue;
            pokemon.friendship.setFriendship(0);
            pokemon.update(EnumUpdateType.Friendship);
            return true;
        }
        return false;
    }

    @Override
    public boolean canAccept(PokemonGroup group) {
        return group.contains(EnumSpecies.TapuBulu) || group.contains(EnumSpecies.TapuFini) || group.contains(EnumSpecies.TapuKoko) || group.contains(EnumSpecies.TapuLele);
    }
}

