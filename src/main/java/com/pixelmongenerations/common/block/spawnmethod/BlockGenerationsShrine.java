/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawnmethod;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityGenerationsShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockGenerationsShrine
extends BlockShrine {
    @Override
    public TileEntityShrine createTileEntity() {
        return new TileEntityGenerationsShrine();
    }

    @Override
    protected boolean checkExtraRequirements(EntityPlayer player, World world, BlockPos pos) {
        return true;
    }

    @Override
    public boolean canAccept(PokemonGroup group) {
        return TileEntityGenerationsShrine.getPokemonGroup().getMembers().stream().anyMatch(group::contains);
    }
}

