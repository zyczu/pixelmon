/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawnmethod;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityAbundantShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.common.entity.pixelmon.stats.extraStats.MeloettaStats;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Objects;
import java.util.stream.IntStream;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockAbundantShrine
extends BlockShrine {
    @Override
    public TileEntityShrine createTileEntity() {
        return new TileEntityAbundantShrine();
    }

    @Override
    protected boolean checkExtraRequirements(EntityPlayer player, World world, BlockPos pos) {
        if (player.world.isThundering()) {
            PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
            return IntStream.range(0, 6).mapToObj(storage::getIDFromPosition).map(id -> storage.getPokemon((int[])id, player.world)).filter(Objects::nonNull).filter(a -> a.getSpecies() == EnumSpecies.Meloetta).map(a -> a.extraStats).filter(MeloettaStats.class::isInstance).map(MeloettaStats.class::cast).anyMatch(a -> a.abundantActivations < MeloettaStats.MAX_ACTIVATIONS);
        }
        return false;
    }

    @Override
    public boolean canAccept(PokemonGroup group) {
        return group.contains(EnumSpecies.Landorus) || group.contains(EnumSpecies.Tornadus) || group.contains(EnumSpecies.Thundurus);
    }
}

