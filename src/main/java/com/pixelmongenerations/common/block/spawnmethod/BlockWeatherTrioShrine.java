/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.spawnmethod;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityWeatherTrioShrine;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockWeatherTrioShrine
extends BlockShrine {
    private PokemonGroup.PokemonData pokemon;

    public BlockWeatherTrioShrine(EnumSpecies pokemon) {
        this.setBlockUnbreakable();
        this.pokemon = new PokemonGroup.PokemonData(pokemon, null);
        this.setTranslationKey(pokemon.name.toLowerCase() + "_shrine");
    }

    @Override
    public TileEntityShrine createTileEntity() {
        return new TileEntityWeatherTrioShrine();
    }

    @Override
    protected boolean checkExtraRequirements(EntityPlayer player, World world, BlockPos pos) {
        PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
        return IntStream.range(0, 6).mapToObj(storage::getIDFromPosition).map(a -> storage.getAlreadyExists((int[])a, player.world)).flatMap(a -> a.map(Stream::of).orElse(Stream.empty())).filter(a -> a.getSpecies() == EnumSpecies.Rayquaza).anyMatch(a -> a.getFormEnum() == EnumForms.Mega);
    }

    @Override
    public boolean canAccept(PokemonGroup group) {
        return group.contains(this.pokemon);
    }

    public PokemonGroup.PokemonData getActive() {
        return this.pokemon;
    }
}

