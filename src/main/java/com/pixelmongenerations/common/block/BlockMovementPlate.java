/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.api.events.MovementPlateEvent;
import com.pixelmongenerations.common.block.BlockStickPlate;
import com.pixelmongenerations.common.block.generic.GenericRotatableBlock;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.network.ChatHandler;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class BlockMovementPlate
extends GenericRotatableBlock {
    private static final int MAX_LOOP_LENGTH = 100;
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.0625, 1.0);

    public BlockMovementPlate() {
        super(Material.IRON);
        this.setHardness(2.5f);
        this.setCreativeTab(PixelmonCreativeTabs.utilityBlocks);
    }

    public Boolean isLoop(World world, BlockPos posOriginal, int maxLength) {
        BlockPos posCurrent = new BlockPos(posOriginal);
        for (int iteration = 1; iteration < maxLength; ++iteration) {
            switch (world.getBlockState(posCurrent).getValue(GenericRotatableBlock.FACING)) {
                case NORTH: {
                    posCurrent = posCurrent.north();
                    break;
                }
                case SOUTH: {
                    posCurrent = posCurrent.south();
                    break;
                }
                case EAST: {
                    posCurrent = posCurrent.east();
                    break;
                }
                case WEST: {
                    posCurrent = posCurrent.west();
                    break;
                }
                default: {
                    return false;
                }
            }
            if (!(world.getBlockState(posCurrent).getBlock() instanceof BlockMovementPlate)) {
                return false;
            }
            if (!posCurrent.equals(posOriginal)) continue;
            return true;
        }
        return false;
    }

    public void onEntityCollision(World worldIn, BlockPos pos, IBlockState state, Entity entity) {
        EnumFacing facing = state.getValue(GenericRotatableBlock.FACING);
        double speedModifier = 1.0;
        if (this.targetSticky(worldIn, pos)) {
            speedModifier = 0.4;
        }
        MovementPlateEvent event = new MovementPlateEvent(entity, pos, facing, speedModifier, false);
        MinecraftForge.EVENT_BUS.post(event);
        if (!event.isCanceled()) {
            facing = event.getFacing();
            speedModifier = event.getSpeedModifier();
            double distanceFromCenterX = entity.posX - (double)pos.getX() - 0.5;
            double distanceFromCenterZ = entity.posZ - (double)pos.getZ() - 0.5;
            double pushToMiddle = 0.2;
            if (facing == EnumFacing.NORTH || facing == EnumFacing.SOUTH) {
                entity.motionZ = (double)facing.getZOffset() * speedModifier;
                if (distanceFromCenterX > 0.1) {
                    if (entity.motionX > -0.2) {
                        entity.motionX = -1.0 * pushToMiddle;
                    }
                } else if (distanceFromCenterX < -0.1) {
                    if (entity.motionX < 0.2) {
                        entity.motionX = pushToMiddle;
                    }
                } else {
                    entity.motionX = 0.0;
                }
            } else if (facing == EnumFacing.EAST || facing == EnumFacing.WEST) {
                entity.motionX = (double)facing.getXOffset() * speedModifier;
                if (distanceFromCenterZ > 0.1) {
                    if (entity.motionZ > -0.2) {
                        entity.motionZ = -1.0 * pushToMiddle;
                    }
                } else if (distanceFromCenterZ < -0.1) {
                    if (entity.motionZ < 0.2) {
                        entity.motionZ = pushToMiddle;
                    }
                } else {
                    entity.motionZ = 0.0;
                }
            }
            entity.motionY = (double)facing.getYOffset() * speedModifier;
        }
    }

    public boolean targetSticky(World world, BlockPos pos) {
        BlockPos targetPos = null;
        switch (world.getBlockState(pos).getValue(GenericRotatableBlock.FACING)) {
            case NORTH: {
                targetPos = pos.north();
                break;
            }
            case SOUTH: {
                targetPos = pos.south();
                break;
            }
            case EAST: {
                targetPos = pos.east();
                break;
            }
            case WEST: {
                targetPos = pos.west();
                break;
            }
        }
        if (targetPos != null) {
            return world.getBlockState(targetPos).getBlock() instanceof BlockStickPlate;
        }
        return false;
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
        worldIn.setBlockState(pos, state.withProperty(FACING, placer.getHorizontalFacing()), 2);
        if (this.isLoop(worldIn, pos, 100).booleanValue()) {
            worldIn.setBlockToAir(pos);
            if (!worldIn.isRemote) {
                EntityPlayer player = (EntityPlayer)placer;
                ChatHandler.sendChat(player, "pixelmon.blocks.movepad.loopbuilderror", new Object[0]);
            }
        }
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        return AABB;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return face == EnumFacing.DOWN ? BlockFaceShape.SOLID : BlockFaceShape.UNDEFINED;
    }
}

