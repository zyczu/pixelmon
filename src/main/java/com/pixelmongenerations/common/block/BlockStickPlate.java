/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.api.events.StickPlateEvent;
import com.pixelmongenerations.common.block.generic.GenericRotatableBlock;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class BlockStickPlate
extends GenericRotatableBlock {
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.0625, 1.0);

    public BlockStickPlate() {
        super(Material.IRON);
        this.setHardness(2.5f);
        this.setCreativeTab(PixelmonCreativeTabs.utilityBlocks);
    }

    public void onEntityCollision(World worldIn, BlockPos pos, IBlockState state, Entity entity) {
        double distanceFromMiddleX = Math.abs((double)pos.getX() - (entity.posX - 0.5));
        double distanceFromMiddleZ = Math.abs((double)pos.getZ() - (entity.posZ - 0.5));
        StickPlateEvent event = new StickPlateEvent(entity, pos, false);
        MinecraftForge.EVENT_BUS.post(event);
        if (!event.isCanceled()) {
            if (distanceFromMiddleX < 0.2) {
                entity.motionX = 0.0;
            }
            if (distanceFromMiddleZ < 0.2) {
                entity.motionZ = 0.0;
            }
        }
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
        worldIn.setBlockState(pos, state.withProperty(FACING, placer.getHorizontalFacing()), 2);
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess access, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        return AABB;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return face == EnumFacing.DOWN ? BlockFaceShape.SOLID : BlockFaceShape.UNDEFINED;
    }
}

