/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.generic;

import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.entity.EntityChairMount;
import java.util.List;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class GenericRotatableSittableModelBlock
extends GenericRotatableModelBlock {
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.1, 0.0, 0.1, 0.9, 1.0, 0.9);
    private AxisAlignedBB boundingBox;

    protected GenericRotatableSittableModelBlock(Material materialIn, AxisAlignedBB boundingBox) {
        super(materialIn);
        this.boundingBox = boundingBox;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess access, BlockPos pos) {
        return this.boundingBox != null ? this.boundingBox : AABB;
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return this.boundingBox != null ? this.boundingBox : AABB;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (hand == EnumHand.OFF_HAND) {
            return false;
        }
        return this.mountBlock(world, pos, player);
    }

    public void onPlayerDestroy(World worldIn, BlockPos pos, IBlockState state) {
        this.unMountBlock(worldIn, pos);
    }

    public void onExplosionDestroy(World worldIn, BlockPos pos, Explosion explosionIn) {
        this.unMountBlock(worldIn, pos);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    private void unMountBlock(World world, BlockPos pos) {
        List<EntityChairMount> list = world.getEntitiesWithinAABB(EntityChairMount.class, new AxisAlignedBB(pos.getX(), pos.getY(), pos.getZ(), pos.getX() + 1, pos.getY() + 1, pos.getZ() + 1));
        for (EntityChairMount entity : list) {
            entity.removePassengers();
        }
    }

    public boolean mountBlock(World world, BlockPos pos, EntityPlayer player) {
        if (world.isRemote) {
            return true;
        }
        List<Entity> list = world.getEntitiesWithinAABB(Entity.class, new AxisAlignedBB(pos.getX(), pos.getY() - 1, pos.getZ(), pos.getX() + 1, pos.getY() + 2, pos.getZ() + 1));
        for (Entity entity : list) {
            if (!(entity instanceof EntityChairMount)) continue;
            return false;
        }
        EntityChairMount mount = new EntityChairMount(world);
        mount.setPosition((float)pos.getX() + 0.5f, (float)pos.getY() - 0.65f + this.getSittingHeight(), (double)pos.getZ() + 0.5);
        world.spawnEntity(mount);
        player.startRiding(mount);
        return true;
    }

    public float getSittingHeight() {
        return 0.5f;
    }
}

