/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.generic;

import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class GenericOre
extends Block {
    private List<ItemStack> drops = new ArrayList<ItemStack>(1);
    private int base = 0;
    private int deviant = 0;

    public GenericOre(Material par2Material) {
        super(par2Material);
        this.setHardness(0.5f);
    }

    public GenericOre addDrop(ItemStack drop) {
        this.drops.add(drop);
        return this;
    }

    public GenericOre setBase(int baseXP) {
        this.base = baseXP;
        return this;
    }

    public GenericOre setDeviant(int deviantXP) {
        this.deviant = deviantXP;
        return this;
    }

    @Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        if (this.drops.isEmpty()) {
            return super.getDrops(world, pos, state, fortune);
        }
        ArrayList<ItemStack> drops1 = new ArrayList<ItemStack>(this.drops.size());
        for (ItemStack stack : this.drops) {
            ItemStack count = stack.copy();
            count.setCount(RandomHelper.getFortuneAmount(fortune));
            drops1.add(count);
        }
        return drops1;
    }

    @Override
    public int getExpDrop(IBlockState state, IBlockAccess world, BlockPos pos, int fortune) {
        return (int)(Math.random() * (double)this.deviant + (double)this.base);
    }

    @SideOnly(value=Side.CLIENT)
    public BlockRenderLayer getRenderLayer() {
        if (this.material == Material.GLASS) {
            return BlockRenderLayer.CUTOUT;
        }
        return super.getRenderLayer();
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        if (this.material == Material.GLASS) {
            return false;
        }
        return super.isFullCube(state);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return this.material != Material.GLASS;
    }

    public GenericOre setSound(SoundType type) {
        this.setSoundType(type);
        return this;
    }
}

