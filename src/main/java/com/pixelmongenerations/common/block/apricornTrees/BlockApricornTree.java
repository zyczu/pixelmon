/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.apricornTrees;

import com.pixelmongenerations.api.events.ApricornEvent;
import com.pixelmongenerations.common.block.enums.EnumBlockPos;
import com.pixelmongenerations.common.block.tileEntities.TileEntityApricornTree;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonItemsApricorns;
import com.pixelmongenerations.core.enums.EnumApricornTrees;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.IGrowable;
import net.minecraft.block.material.EnumPushReaction;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;

public class BlockApricornTree
extends Block
implements IGrowable {
    public static final PropertyEnum<EnumBlockPos> BLOCKPOS = PropertyEnum.create("blockpos", EnumBlockPos.class);
    private static final AxisAlignedBB AABBBase = new AxisAlignedBB(0.06f, 0.0, 0.06f, 0.94f, 1.65f, 0.94f);
    private static final AxisAlignedBB AABBBaseStage0 = new AxisAlignedBB(0.4f, 0.0, 0.4f, 0.6f, 0.2f, 0.6f);
    private static final AxisAlignedBB AABBBaseStage1 = new AxisAlignedBB(0.25, 0.0, 0.25, 0.75, 0.7f, 0.75);
    private static final AxisAlignedBB AABBBaseStage2 = new AxisAlignedBB(0.17f, 0.0, 0.17f, 0.83f, 1.0, 0.83f);
    private static final AxisAlignedBB AABBTop = new AxisAlignedBB(0.06f, -1.0, 0.06f, 0.94f, 0.1f, 0.94f);
    private static final AxisAlignedBB AABBTopStage2 = new AxisAlignedBB(0.06f, -1.0, 0.06f, 0.94f, 0.65f, 0.94f);
    private EnumApricornTrees tree;

    public BlockApricornTree(EnumApricornTrees tree) {
        super(Material.WOOD);
        this.tree = tree;
        this.setTickRandomly(true);
        this.setHardness(2.0f);
        this.setTranslationKey("apricorn tree");
        this.setRegistryName("pixelmon:" + tree.name().toLowerCase() + "_apricorn_tree");
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, BLOCKPOS);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(BLOCKPOS, EnumBlockPos.fromMeta(meta));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(BLOCKPOS).toMeta();
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return Item.getItemFromBlock(Blocks.LOG);
    }

    @Override
    public boolean canSilkHarvest(World world, BlockPos pos, IBlockState state, EntityPlayer player) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        if (face == EnumFacing.UP) {
            return BlockFaceShape.CENTER;
        }
        return BlockFaceShape.SOLID;
    }

    @Override
    public int quantityDropped(Random random) {
        return 1;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess access, BlockPos pos) {
        return this.getBlockBounds(access, pos, blockState);
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return this.getBlockBounds(worldIn, pos, state);
    }

    public AxisAlignedBB getBlockBounds(IBlockAccess world, BlockPos pos, IBlockState state) {
        EnumBlockPos blockpos = state.getValue(BLOCKPOS);
        BlockPos loc = pos;
        if (blockpos == EnumBlockPos.TOP) {
            loc = pos.down();
        }
        if (world.getBlockState(loc).getBlock() != this) {
            if (world instanceof WorldServer) {
                ((WorldServer)world).setBlockToAir(pos);
            }
            return Block.FULL_BLOCK_AABB;
        }
        TileEntityApricornTree tile = BlockHelper.getTileEntity(TileEntityApricornTree.class, world, loc);
        if (tile == null) {
            return Block.FULL_BLOCK_AABB;
        }
        int stage = tile.getStage();
        if (blockpos == EnumBlockPos.TOP && world.getBlockState(new BlockPos(pos.getX(), pos.getY() - 1, pos.getZ())).getBlock() == this) {
            if (stage == 2) {
                return AABBTopStage2;
            }
            return AABBTop;
        }
        if (stage == 0) {
            return AABBBaseStage0;
        }
        if (stage == 1) {
            return AABBBaseStage1;
        }
        if (stage == 2) {
            return AABBBaseStage2;
        }
        return AABBBase;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.INVISIBLE;
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(PixelmonItemsApricorns.getApricorn(this.tree.apricorn), 1);
    }

    @Override
    public boolean hasTileEntity(IBlockState state) {
        return state.getValue(BLOCKPOS) == EnumBlockPos.BOTTOM;
    }

    @Override
    public TileEntity createTileEntity(World world, IBlockState state) {
        return new TileEntityApricornTree(this.tree);
    }

    @Override
    public boolean canHarvestBlock(IBlockAccess world, BlockPos pos, EntityPlayer player) {
        return true;
    }

    @Override
    public boolean removedByPlayer(IBlockState state, World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {
        EnumBlockPos blockpos = state.getValue(BLOCKPOS);
        if (blockpos == EnumBlockPos.TOP && world.getBlockState(pos.down()).getBlock() == this) {
            world.setBlockToAir(pos.down());
        } else if (world.getBlockState(pos.up()).getBlock() == this) {
            world.setBlockToAir(pos.up());
        }
        return super.removedByPlayer(state, world, pos, player, willHarvest);
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        TileEntityApricornTree tile;
        if (world.isRemote || hand == EnumHand.OFF_HAND) {
            return true;
        }
        EnumBlockPos blockpos = state.getValue(BLOCKPOS);
        BlockPos loc = pos;
        if (blockpos == EnumBlockPos.TOP) {
            loc = pos.down();
        }
        if ((tile = BlockHelper.getTileEntity(TileEntityApricornTree.class, world, loc)).getStage() == 5) {
            Item item = PixelmonItemsApricorns.getApricorn(this.tree.apricorn);
            ItemStack pickedStack = new ItemStack(item);
            ApricornEvent.PickApricornEvent pickEvent = new ApricornEvent.PickApricornEvent(this.tree.apricorn, pos, (EntityPlayerMP)player, tile, pickedStack);
            if (MinecraftForge.EVENT_BUS.post(pickEvent)) {
                return false;
            }
            DropItemHelper.giveItemStackToPlayer(player, pickEvent.getPickedStack());
            tile.setStage(3);
            return true;
        }
        return !player.getHeldItemMainhand().isEmpty() && player.getHeldItemMainhand().getItem() instanceof ItemBlock;
    }

    @Override
    public void updateTick(World world, BlockPos pos, IBlockState state, Random rand) {
        try {
            super.updateTick(world, pos, state, rand);
            BlockPos posUp = pos.up();
            if (world.getLightFromNeighbors(posUp) >= 9) {
                EnumBlockPos blockpos = state.getValue(BLOCKPOS);
                if (blockpos == EnumBlockPos.TOP) {
                    return;
                }
                TileEntityApricornTree tile = BlockHelper.getTileEntity(TileEntityApricornTree.class, world, pos);
                int stage = tile.getStage();
                if (this.canGrow(world, pos, state, false)) {
                    float growthChance = 0.25f;
                    ApricornEvent.GrowthChanceEvent growthEvent = new ApricornEvent.GrowthChanceEvent(this.tree.apricorn, pos, tile, growthChance);
                    if (!MinecraftForge.EVENT_BUS.post(growthEvent) && RandomHelper.getRandomChance(growthEvent.getGrowthChance())) {
                        tile.setStage(stage + 1);
                        if (stage == 5) {
                            MinecraftForge.EVENT_BUS.post(new ApricornEvent.ApricornReadyEvent(this.tree.apricorn, pos, tile));
                        }
                        if (stage + 1 >= 3) {
                            world.setBlockState(posUp, state.withProperty(BLOCKPOS, EnumBlockPos.TOP), 2);
                        }
                        ((WorldServer)world).getPlayerChunkMap().markBlockForUpdate(pos);
                    }
                }
            }
        }
        catch (Exception e) {
            Pixelmon.LOGGER.error("Error in Apricorn update tick.");
            e.printStackTrace();
        }
    }

    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos from) {
        this.checkAndDropBlock(worldIn, pos, state);
    }

    @Override
    public boolean canGrow(World worldIn, BlockPos pos, IBlockState state, boolean isClient) {
        TileEntityApricornTree tile;
        EnumBlockPos blockpos = state.getValue(BLOCKPOS);
        BlockPos loc = pos;
        if (blockpos == EnumBlockPos.TOP) {
            loc = pos.down();
        }
        if ((tile = BlockHelper.getTileEntity(TileEntityApricornTree.class, worldIn, loc)) != null) {
            if (tile.getStage() + 1 == 3 && worldIn.getBlockState(loc.up()).getBlock() != Blocks.AIR) {
                return false;
            }
            return tile.getStage() < 5;
        }
        return false;
    }

    @Override
    public boolean canUseBonemeal(World worldIn, Random rand, BlockPos pos, IBlockState state) {
        return this.canGrow(worldIn, pos, state, worldIn.isRemote);
    }

    @Override
    public void grow(World worldIn, Random rand, BlockPos pos, IBlockState state) {
        TileEntityApricornTree tile;
        EnumBlockPos blockpos = state.getValue(BLOCKPOS);
        if (blockpos == EnumBlockPos.TOP) {
            pos = pos.down();
        }
        if ((tile = BlockHelper.getTileEntity(TileEntityApricornTree.class, worldIn, pos)) == null) {
            return;
        }
        int stage = tile.getStage();
        if (stage < 5) {
            tile.setStage(stage + 1);
            if (stage + 1 >= 3) {
                worldIn.setBlockState(pos.up(), state.withProperty(BLOCKPOS, EnumBlockPos.TOP), 2);
            }
            if (!worldIn.isRemote) {
                ((WorldServer)worldIn).getPlayerChunkMap().markBlockForUpdate(pos);
            }
        }
    }

    public boolean canBlockStay(World worldIn, BlockPos pos) {
        BlockPos down = pos.down();
        Block soil = worldIn.getBlockState(down).getBlock();
        if (soil == this) {
            soil = worldIn.getBlockState(down.down()).getBlock();
        }
        return this.canPlaceBlockOn(soil);
    }

    protected void checkAndDropBlock(World worldIn, BlockPos pos, IBlockState state) {
        if (!this.canBlockStay(worldIn, pos)) {
            this.dropBlockAsItem(worldIn, pos, state, 0);
            worldIn.setBlockState(pos, Blocks.AIR.getDefaultState(), 3);
        }
    }

    protected boolean canPlaceBlockOn(Block ground) {
        return BlockApricornTree.isSuitableSoil(ground);
    }

    public static boolean isSuitableSoil(Block ground) {
        if (ground == PixelmonBlocks.pokeGrassBlock || ground == PixelmonBlocks.pokeDirtBlock || ground == PixelmonBlocks.softSoil || ground == Blocks.DIRT || ground == Blocks.GRASS_PATH || ground == Blocks.GRASS || ground == Blocks.FARMLAND) {
            return true;
        }
        String name = ground.getRegistryName().toString();
        return name.equals("biomesoplenty:grass") || name.equals("biomesoplenty:dirt");
    }

    public EnumPushReaction getPushReaction(IBlockState state) {
        return EnumPushReaction.BLOCK;
    }

    public EnumApricornTrees getApricornType() {
        return this.tree;
    }
}

