/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.common.block.PixelmonBlock;
import com.pixelmongenerations.common.item.ItemMulch;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.EnumMulch;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockSoftSoil
extends PixelmonBlock {
    public static PropertyEnum<EnumMulch> MULCH = PropertyEnum.create("mulch", EnumMulch.class);

    public BlockSoftSoil(String name) {
        super(Material.GROUND);
        this.setTranslationKey(name);
        this.setSound(SoundType.GROUND);
        this.setCreativeTab(PixelmonCreativeTabs.buildingBlocks);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(MULCH, EnumMulch.getFromOrdinal(meta));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(MULCH).ordinal();
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, MULCH);
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        ItemStack stack = playerIn.getHeldItem(hand);
        if (stack.getItem() instanceof ItemMulch) {
            worldIn.setBlockState(pos, state.withProperty(MULCH, ((ItemMulch)stack.getItem()).getMulch()));
            if (!playerIn.capabilities.isCreativeMode) {
                stack.shrink(1);
            }
            return true;
        }
        return false;
    }

    public static EnumMulch getMulch(World world, BlockPos pos) {
        IBlockState state = world.getBlockState(pos);
        return state.getBlock() instanceof BlockSoftSoil ? state.getValue(MULCH) : EnumMulch.None;
    }
}

