/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityGracidea;
import com.pixelmongenerations.core.config.PixelmonItems;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;

public class BlockGracidea
extends GenericRotatableModelBlock
implements IPlantable {
    public BlockGracidea() {
        super(Material.PLANTS);
    }

    @Override
    public EnumPlantType getPlantType(IBlockAccess world, BlockPos pos) {
        return EnumPlantType.Plains;
    }

    @Override
    public IBlockState getPlant(IBlockAccess world, BlockPos pos) {
        IBlockState state = world.getBlockState(pos);
        if (state.getBlock() != this) {
            return this.getDefaultState();
        }
        return state;
    }

    @Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        return Collections.singletonList(new ItemStack(PixelmonItems.gracidea, 1));
    }

    @Override
    public TileEntity createNewTileEntity(World world, int var) {
        try {
            return new TileEntityGracidea();
        }
        catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    public boolean canBlockStay(World world, BlockPos pos, IBlockState state) {
        return world.getBlockState(pos.down()).getBlock() == Blocks.GRASS;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        return new AxisAlignedBB(0.3, 0.0, 0.3, 0.7, 0.2, 0.7);
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(PixelmonItems.gracidea, 1);
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return PixelmonItems.gracidea;
    }

    @Override
    public int quantityDropped(Random random) {
        return 1;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }
}

