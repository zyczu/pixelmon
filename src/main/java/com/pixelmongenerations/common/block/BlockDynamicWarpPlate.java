/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.api.events.WarpPlateEvent;
import com.pixelmongenerations.common.block.generic.GenericBlockContainer;
import com.pixelmongenerations.common.block.tileEntities.TileEntityWarpPlate;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.Timer;
import java.util.TimerTask;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockDynamicWarpPlate
extends GenericBlockContainer {
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.0625, 1.0);

    public BlockDynamicWarpPlate() {
        super(Material.BARRIER);
        this.setBlockUnbreakable();
        this.setCreativeTab(PixelmonCreativeTabs.utilityBlocks);
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityWarpPlate();
    }

    public void onEntityCollision(World worldIn, BlockPos pos, IBlockState state, final Entity entity) {
        TileEntityWarpPlate warpPlate;
        if (entity instanceof EntityPlayerMP && (warpPlate = BlockHelper.getTileEntity(TileEntityWarpPlate.class, worldIn, pos)) != null && warpPlate.getWarpPosition() != null) {
            final WarpPlateEvent event = new WarpPlateEvent((EntityPlayerMP)entity, warpPlate.getPos(), warpPlate.getWarpPosition(), true);
            MinecraftForge.EVENT_BUS.post(event);
            if (!event.isCanceled()) {
                Timer t = new Timer();
                t.schedule(new TimerTask(){

                    @Override
                    public void run() {
                        entity.setPositionAndUpdate((double)event.getDestinationPos().getX() + 0.5, event.getDestinationPos().getY(), (double)event.getDestinationPos().getZ() + 0.5);
                    }
                }, 10L);
            }
        }
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        return AABB;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return face == EnumFacing.DOWN ? BlockFaceShape.SOLID : BlockFaceShape.UNDEFINED;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }

    @SideOnly(value=Side.CLIENT)
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.TRANSLUCENT;
    }
}

