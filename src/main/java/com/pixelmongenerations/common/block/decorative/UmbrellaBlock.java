/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.enums.ColorEnum;
import com.pixelmongenerations.common.block.generic.GenericModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityUmbrella;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class UmbrellaBlock
extends GenericModelBlock {
    private static final AxisAlignedBB AABB = new AxisAlignedBB(0.4, 0.0, 0.4, 0.6, 2.4, 0.6);
    private ColorEnum color;

    public UmbrellaBlock(ColorEnum color) {
        super(Material.IRON);
        this.setHardness(1.0f);
        this.setSoundType(SoundType.METAL);
        this.color = color;
        this.setTranslationKey((Object)((Object)color) + "_umbrella");
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return AABB;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityUmbrella();
    }

    public ColorEnum getColor() {
        return this.color;
    }
}

