/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.enums.ColorEnum;
import com.pixelmongenerations.common.block.generic.GenericRotatableSittableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityGreenFoldingChair;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;

public class FoldingChairBlock
extends GenericRotatableSittableModelBlock {
    private ColorEnum color;

    public FoldingChairBlock(ColorEnum color) {
        super(Material.WOOD, (AxisAlignedBB)null);
        this.setHardness(1.0f);
        this.setSoundType(SoundType.WOOD);
        this.setTranslationKey((Object)((Object)color) + "_folding_chair");
        this.color = color;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        if (this.color == ColorEnum.Green) {
            return new TileEntityGreenFoldingChair();
        }
        return null;
    }

    @Override
    public float getSittingHeight() {
        return 0.84f;
    }
}

