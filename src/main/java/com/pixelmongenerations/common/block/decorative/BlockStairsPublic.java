/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import net.minecraft.block.Block;
import net.minecraft.block.BlockStairs;

public class BlockStairsPublic
extends BlockStairs {
    public BlockStairsPublic(Block referenceBlock) {
        super(referenceBlock.getDefaultState());
        this.useNeighborBrightness = true;
    }
}

