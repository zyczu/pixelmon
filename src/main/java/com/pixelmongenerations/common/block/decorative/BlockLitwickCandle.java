/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityLitwickCandle;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockLitwickCandle
extends GenericRotatableModelBlock {
    public BlockLitwickCandle() {
        super(Material.WOOD);
        this.setHardness(0.5f);
        this.setLightLevel(0.7f);
        this.setTranslationKey("litwick_candle");
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityLitwickCandle();
    }

    public Item getDroppedItem() {
        return Item.getItemFromBlock(this);
    }
}

