/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.generic.GenericRotatableSittableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityChair;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;

public class ChairBlock
extends GenericRotatableSittableModelBlock {
    public ChairBlock() {
        super(Material.WOOD, (AxisAlignedBB)null);
        this.setSoundType(SoundType.WOOD);
        this.setTranslationKey("chair");
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityChair();
    }

    @Override
    public float getSittingHeight() {
        return 0.3f;
    }
}

