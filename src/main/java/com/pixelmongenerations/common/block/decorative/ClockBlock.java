/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.enums.ColorEnum;
import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityClock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class ClockBlock
extends GenericRotatableModelBlock {
    private static final AxisAlignedBB AABBEast = new AxisAlignedBB(0.0, 0.25, 0.25, 0.1, 0.75, 0.75);
    private static final AxisAlignedBB AABBWest = new AxisAlignedBB(0.9, 0.25, 0.25, 1.0, 0.75, 0.75);
    private static final AxisAlignedBB AABBSouth = new AxisAlignedBB(0.25, 0.25, 0.0, 0.75, 0.75, 0.1);
    private static final AxisAlignedBB AABBNorth = new AxisAlignedBB(0.25, 0.25, 0.9, 0.75, 0.75, 1.0);
    private ColorEnum color;

    public ClockBlock(ColorEnum color) {
        super(Material.IRON);
        this.color = color;
        this.setHardness(0.5f);
        this.setSoundType(SoundType.METAL);
        this.setTranslationKey((Object)((Object)color) + "Clock");
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
        return this.getBlockBounds(blockState);
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos) {
        return this.getBlockBounds(state);
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    private AxisAlignedBB getBlockBounds(IBlockState blockState) {
        EnumFacing enumfacing = blockState.getValue(FACING);
        switch (enumfacing) {
            case EAST: {
                return AABBEast;
            }
            case WEST: {
                return AABBWest;
            }
            case SOUTH: {
                return AABBSouth;
            }
            case NORTH: {
                return AABBNorth;
            }
        }
        return null;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityClock();
    }

    public ColorEnum getColor() {
        return this.color;
    }
}

