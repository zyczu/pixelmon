/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.enums.EnumAxis;
import com.pixelmongenerations.common.block.tileEntities.TileEntityDecorativeBase;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockContainerPlus
extends BlockContainer {
    public static final PropertyEnum<EnumAxis> AXIS = PropertyEnum.create("axis", EnumAxis.class);
    protected String iconName = "quartzblock_bottom";
    public Class<? extends ModelBase> modelClass;
    protected int renderType;
    protected int amountDropped = 1;
    protected boolean opaqueCube = false;
    protected boolean renderNormalBlock = false;
    protected Class<? extends TileEntity> tileClass = TileEntityDecorativeBase.class;
    public float invScale = 1.0f;
    public float[] invOffsets = new float[]{0.0f, 0.0f, 0.0f};

    public BlockContainerPlus(Material mat) {
        super(mat);
    }

    public BlockContainerPlus setRenderOptions(int renderType, boolean opaqueCube, boolean renderNormal) {
        this.renderType = renderType;
        this.opaqueCube = opaqueCube;
        this.renderNormalBlock = renderNormal;
        return this;
    }

    public BlockContainerPlus setModelClass(Class<? extends ModelBase> modelClass) {
        this.modelClass = modelClass;
        return this;
    }

    public BlockContainerPlus setAmountDropped(int amount) {
        this.amountDropped = amount;
        return this;
    }

    public BlockContainerPlus setIconName(String name) {
        this.iconName = name;
        return this;
    }

    public BlockContainerPlus setTileEntityClass(Class<? extends TileEntity> tileClass) {
        this.tileClass = tileClass;
        return this;
    }

    @Override
    public int quantityDropped(Random random) {
        return this.amountDropped;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return this.opaqueCube;
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(AXIS, EnumAxis.fromMeta(meta));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(AXIS).ordinal();
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, AXIS);
    }

    @Override
    public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
        return this.getDefaultState().withProperty(AXIS, EnumAxis.fromFacingAxis(facing.getAxis()));
    }

    public boolean isAnotherWithSameOrientationOnSide(IBlockAccess world, BlockPos pos, EnumAxis orientation, EnumFacing dir) {
        BlockPos loc = pos.offset(dir);
        IBlockState blockstate = world.getBlockState(loc);
        if (blockstate.getBlock() != this) {
            return false;
        }
        EnumAxis otherorientation = blockstate.getValue(AXIS);
        return orientation == otherorientation;
    }

    public boolean isSameOrientationAndType(Class<? extends Block> blockClass, int thisMeta, int thatMeta) {
        return blockClass == this.getClass() && (thisMeta & 7) / 2 == (thatMeta & 7) / 2;
    }

    @Override
    public TileEntity createNewTileEntity(World world, int var2) {
        try {
            return this.tileClass.newInstance();
        }
        catch (Exception e) {
            return null;
        }
    }
}

