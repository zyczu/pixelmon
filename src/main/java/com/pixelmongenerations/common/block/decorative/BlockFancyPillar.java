/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.decorative.BlockContainerPlus;
import com.pixelmongenerations.common.block.enums.EnumAxis;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockFancyPillar
extends BlockContainerPlus {
    public static final EnumFacing[] UP_VALS = new EnumFacing[]{EnumFacing.EAST, EnumFacing.SOUTH, EnumFacing.UP};
    public static final EnumFacing[] DOWN_VALS = new EnumFacing[]{EnumFacing.WEST, EnumFacing.NORTH, EnumFacing.DOWN};
    public static final PropertyBool DAMAGED = PropertyBool.create("damaged");

    public BlockFancyPillar(Material mat) {
        super(mat);
        this.setTranslationKey("temple_pillar");
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(AXIS, EnumAxis.values()[meta & 7]).withProperty(DAMAGED, (meta & 8) > 0);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        int i = ((EnumAxis)state.getValue(AXIS)).ordinal();
        if (state.getValue(DAMAGED).booleanValue()) {
            i |= 8;
        }
        return i;
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, AXIS, DAMAGED);
    }

    public boolean getIsDamaged(IBlockState state) {
        return state.getValue(DAMAGED);
    }

    @Override
    public void getSubBlocks(CreativeTabs tabs, NonNullList<ItemStack> list) {
        list.add(new ItemStack(this, 1, 0));
        list.add(new ItemStack(this, 1, 1));
    }

    @Override
    public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
        return super.getStateForPlacement(worldIn, pos, facing, hitX, hitY, hitZ, meta, placer).withProperty(DAMAGED, meta == 1);
    }

    public Connections getConnections(IBlockAccess world, BlockPos pos, IBlockState state) {
        EnumAxis axis = (EnumAxis)state.getValue(AXIS);
        Connections result = new Connections();
        int index = axis.ordinal() - 1;
        if (index < 0) {
            index = 0;
        }
        result.top = this.isAnotherWithSameOrientationOnSide(world, pos, axis, DOWN_VALS[index]);
        result.bottom = this.isAnotherWithSameOrientationOnSide(world, pos, axis, UP_VALS[index]);
        return result;
    }

    @Override
    public int damageDropped(IBlockState state) {
        return state.getValue(DAMAGED) != false ? 1 : 0;
    }

    public Boolean isEnd(World world, BlockPos pos, EnumFacing dir) {
        if (world.getBlockState(pos.offset(dir)).getBlock() == this) {
            return null;
        }
        return world.getBlockState(pos.offset(dir)).getMaterial() == Material.AIR;
    }

    public class Connections {
        public boolean top = false;
        public boolean bottom = false;
    }
}

