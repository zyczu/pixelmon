/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.decorative.BlockCouchBase;
import com.pixelmongenerations.common.block.tileEntities.TileEntityCouchCornerRight;

public class BlockCouchCornerRight
extends BlockCouchBase<TileEntityCouchCornerRight> {
    public BlockCouchCornerRight() {
        super(TileEntityCouchCornerRight.class);
        this.setTranslationKey("couch_corner_right");
    }
}

