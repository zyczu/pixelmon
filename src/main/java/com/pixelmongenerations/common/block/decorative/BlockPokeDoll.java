/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.decorative;

import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPokeDoll;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockPokeDoll
extends GenericRotatableModelBlock {
    public String name;
    private boolean shiny;
    private float scale;

    public BlockPokeDoll(String name, boolean shiny) {
        this(name, shiny, 1.0f);
    }

    public BlockPokeDoll(String name, boolean shiny, float scale) {
        super(Material.CLOTH);
        this.setSoundType(SoundType.CLOTH);
        this.setTranslationKey("pokedoll_" + name);
        this.setHardness(1.0f);
        this.setCreativeTab(PixelmonCreativeTabs.pokedolls);
        this.name = name;
        this.shiny = shiny;
        this.scale = scale;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityPokeDoll(this.name, this.shiny, this.scale);
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!world.isRemote && player.isSneaking()) {
            TileEntity tileEntity = world.getTileEntity(pos);
            if (!(tileEntity instanceof TileEntityPokeDoll)) {
                return true;
            }
            TileEntityPokeDoll tile = BlockHelper.getTileEntity(TileEntityPokeDoll.class, world, pos);
            tile.rotate();
        }
        return true;
    }
}

