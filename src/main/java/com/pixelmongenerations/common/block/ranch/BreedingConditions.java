/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.reflect.TypeToken
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 */
package com.pixelmongenerations.common.block.ranch;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumType;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BreedingConditions {
    public static Gson gsonInstance = new GsonBuilder().setPrettyPrinting().create();
    private static Map<EnumType, Map<String, Integer>> typeBlockList = new HashMap<EnumType, Map<String, Integer>>();
    List<String> blockList;

    public BreedingConditions(List<String> blockList) {
        this.blockList = blockList;
    }

    public static Map<EnumType, Map<String, Integer>> getBlockConditions() {
        return typeBlockList;
    }

    public static void registerBreedingConditions() {
        Pixelmon.LOGGER.info("Registering Breeding Conditions.");
        typeBlockList = PixelmonConfig.useExternalJSONFilesBreedingConditions ? BreedingConditions.importSetsFrom("./config/pixelmon/breeding_conditions.json") : BreedingConditions.retrieveBreedingConditionsFromAssets();
    }

    public static Map<EnumType, Map<String, Integer>> importSetsFrom(String dir) {
        File file = new File(dir);
        if (!file.exists()) {
            BreedingConditions.retrieveBreedingConditionsFromAssets();
        }
        try {
            return (Map)gsonInstance.fromJson((Reader)new FileReader(file), new TypeToken<Map<EnumType, Map<String, Integer>>>(){}.getType());
        }
        catch (Exception var6) {
            var6.printStackTrace();
            return new HashMap<EnumType, Map<String, Integer>>();
        }
    }

    public static Map<EnumType, Map<String, Integer>> retrieveBreedingConditionsFromAssets() {
        Map<EnumType, Map<String, Integer>> map = new HashMap<EnumType, Map<String, Integer>>();
        try {
            String primaryPath;
            File file;
            InputStream iStream = BreedingConditions.class.getResourceAsStream("/assets/pixelmon/breeding_conditions.json");
            try {
                map = (Map)gsonInstance.fromJson((Reader)new InputStreamReader(iStream), new TypeToken<Map<EnumType, Map<String, Integer>>>(){}.getType());
            }
            catch (Exception e) {
                Pixelmon.LOGGER.error("Couldn't load breeding conditions  JSON:");
                e.printStackTrace();
            }
            if (PixelmonConfig.useExternalJSONFilesBreedingConditions && !(file = new File(primaryPath = "./config/pixelmon/breeding_conditions.json")).exists()) {
                file.getParentFile().mkdirs();
                PrintWriter pw = new PrintWriter(file);
                pw.write(gsonInstance.toJson(map));
                pw.flush();
                pw.close();
            }
            try {
                iStream.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            return map;
        }
        catch (Exception e) {
            e.printStackTrace();
            return map;
        }
    }

    public float getBreedingStrength(ArrayList<EnumType> types) {
        int sum = 0;
        int nonNullSize = 0;
        if (types == null || types.isEmpty()) {
            return 0.0f;
        }
        for (EnumType type : types) {
            if (type == null) continue;
            ++nonNullSize;
            Map<String, Integer> weightedList = typeBlockList.get((Object)type);
            if (weightedList == null) {
                sum = (int)((float)sum + 80.0f);
                continue;
            }
            for (String b : this.blockList) {
                if (!weightedList.containsKey(b)) continue;
                sum += weightedList.get(b).intValue();
            }
        }
        if ((sum /= nonNullSize) < 35) {
            return 0.0f;
        }
        if (sum < 70) {
            return 0.5f;
        }
        if (sum < 105) {
            return 1.0f;
        }
        if (sum < 140) {
            return 1.5f;
        }
        return 2.0f;
    }
}

