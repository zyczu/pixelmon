/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.ranch;

import com.pixelmongenerations.common.block.IBlockHasOwner;
import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.ranch.RanchBounds;
import com.pixelmongenerations.common.block.tileEntities.TileEntityRanchBlock;
import com.pixelmongenerations.common.item.EnumIsisHourglassType;
import com.pixelmongenerations.common.item.ItemIsisHourglass;
import com.pixelmongenerations.common.item.ItemRanchUpgrade;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.packetHandlers.ranch.EnumRanchClientPacketMode;
import com.pixelmongenerations.core.network.packetHandlers.ranch.RanchBlockClientPacket;
import com.pixelmongenerations.core.storage.PCServer;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockRanchBlock
extends MultiBlock
implements IBlockHasOwner {
    boolean updatedClient = true;

    public BlockRanchBlock() {
        super(Material.ROCK, 1, 2.0, 1);
        this.setHardness(2.5f);
        this.setSoundType(SoundType.STONE);
        this.setTranslationKey("ranchblock");
        this.setHarvestLevel("pickaxe", 1);
    }

    @Override
    public int quantityDropped(Random random) {
        return 1;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (player instanceof EntityPlayerMP && hand == EnumHand.MAIN_HAND) {
            if (!PixelmonConfig.allowBreeding) {
                ChatHandler.sendChat(player, "pixelmon.general.disabledblock", new Object[0]);
                return false;
            }
            ItemStack heldItem = player.getHeldItem(hand);
            BlockPos loc = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), state);
            TileEntityRanchBlock ranchblock = BlockHelper.getTileEntity(TileEntityRanchBlock.class, world, loc);
            if (ranchblock == null) {
                return true;
            }
            Item playerItem = null;
            if (!heldItem.isEmpty()) {
                playerItem = heldItem.getItem();
            }
            if (playerItem instanceof ItemRanchUpgrade && ranchblock.getBounds().canExtend()) {
                EntityPlayerMP playerMP = (EntityPlayerMP)player;
                PCServer.sendContentsToPlayer(playerMP);
                Pixelmon.NETWORK.sendTo(new RanchBlockClientPacket(ranchblock, EnumRanchClientPacketMode.UpgradeBlock), playerMP);
            } else if (playerItem instanceof ItemIsisHourglass && ((ItemIsisHourglass)playerItem).type == EnumIsisHourglassType.Gold) {
                if (ranchblock.applyHourglass()) {
                    player.sendMessage(new TextComponentTranslation("ranch.hourglass.upgradedall", new Object[0]));
                    if (!player.capabilities.isCreativeMode) {
                        player.inventory.clearMatchingItems(playerItem, heldItem.getMetadata(), 1, heldItem.getTagCompound());
                    }
                }
            } else {
                if (player.getUniqueID().equals(ranchblock.getOwnerUUID())) {
                    ((EntityPlayerMP)player).connection.sendPacket(ranchblock.getUpdatePacket());
                    ranchblock.onActivate(player);
                    return true;
                }
                ChatHandler.sendChat(player, "pixelmon.general.needowner", new Object[0]);
                return false;
            }
        }
        return true;
    }

    @Override
    protected Optional<TileEntity> getTileEntity(World world, IBlockState state) {
        return Optional.of(new TileEntityRanchBlock());
    }

    @Override
    public List<ItemStack> getDrops(IBlockAccess worldAccess, BlockPos pos, IBlockState state, int fortune) {
        ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
        if (worldAccess instanceof World) {
            int upgrades;
            TileEntityRanchBlock ranchBlock;
            World world = (World)worldAccess;
            Item item = this.getDroppedItem(world, pos);
            if (item != null) {
                drops.add(new ItemStack(item, 1, this.damageDropped(state)));
            }
            if (!world.isRemote && (ranchBlock = BlockHelper.getTileEntity(TileEntityRanchBlock.class, world, pos)) != null && ranchBlock.getBounds() instanceof RanchBounds && (upgrades = ((RanchBounds)ranchBlock.getBounds()).getUpgradeCount()) > 0 && PixelmonConfig.ranchBlocksDropUpgrades) {
                drops.add(new ItemStack(PixelmonItems.ranchUpgrade, upgrades));
            }
        }
        return drops;
    }

    @Override
    public Item getDroppedItem(World world, BlockPos pos) {
        return Item.getItemFromBlock(this);
    }

    @Override
    public float getPlayerRelativeBlockHardness(IBlockState state, EntityPlayer player, World world, BlockPos pos) {
        BlockPos loc = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), world.getBlockState(pos));
        TileEntityRanchBlock ranchblock = BlockHelper.getTileEntity(TileEntityRanchBlock.class, world, loc);
        if (ranchblock != null && player.getUniqueID().equals(ranchblock.getOwnerUUID()) || player.capabilities.isCreativeMode) {
            return super.getPlayerRelativeBlockHardness(state, player, world, pos);
        }
        return -1.0f;
    }

    @Override
    public boolean removedByPlayer(IBlockState state, World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {
        BlockPos loc = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), world.getBlockState(pos));
        TileEntityRanchBlock ranchblock = BlockHelper.getTileEntity(TileEntityRanchBlock.class, world, loc);
        ranchblock.onDestroy();
        return super.removedByPlayer(state, world, pos, player, willHarvest);
    }

    @Override
    public void setOwner(BlockPos pos, EntityPlayer playerIn) {
        if (!playerIn.world.isRemote) {
            TileEntityRanchBlock ranchblock = BlockHelper.getTileEntity(TileEntityRanchBlock.class, playerIn.world, pos);
            ranchblock.setOwner((EntityPlayerMP)playerIn);
            ranchblock.setInitBounds();
            this.updatedClient = false;
        }
    }

    @Override
    public int tickRate(World world) {
        return 2;
    }

    @Override
    public int getWeakPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side) {
        TileEntityRanchBlock ranchblock = BlockHelper.getTileEntity(TileEntityRanchBlock.class, blockAccess, pos);
        if (ranchblock != null && ranchblock.hasEgg()) {
            return 15;
        }
        return 0;
    }

    @Override
    public int getStrongPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side) {
        return 0;
    }

    @Override
    public boolean canProvidePower(IBlockState state) {
        return true;
    }
}

