/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.decorative.UmbrellaBlock;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntityUmbrella
extends TileEntity
implements ISpecialTexture {
    @Override
    @SideOnly(value=Side.CLIENT)
    public AxisAlignedBB getRenderBoundingBox() {
        return super.getRenderBoundingBox().expand(0.0, 3.0, 0.0);
    }

    @Override
    public ResourceLocation getTexture() {
        IBlockState state = this.getWorld().getBlockState(this.getPos());
        if (state.getBlock() instanceof UmbrellaBlock) {
            UmbrellaBlock block = (UmbrellaBlock)state.getBlock();
            return new ResourceLocation("pixelmon:textures/blocks/umbrella-" + block.getColor().name() + ".png");
        }
        return new ResourceLocation("pixelmon:textures/blocks/umbrella-red.png");
    }
}

