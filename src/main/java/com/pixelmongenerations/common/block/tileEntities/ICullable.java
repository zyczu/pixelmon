/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

public interface ICullable {
    public void setCulled(boolean var1);

    public boolean isCulled();
}

