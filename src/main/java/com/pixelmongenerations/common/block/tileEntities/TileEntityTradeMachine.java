/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.api.enums.ReceiveType;
import com.pixelmongenerations.api.events.PixelmonReceivedEvent;
import com.pixelmongenerations.api.events.PixelmonTradeEvent;
import com.pixelmongenerations.api.events.PokedexEvent;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.pokedex.EnumPokedexRegisterStatus;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.PixelmonStatsData;
import com.pixelmongenerations.core.network.packetHandlers.trading.RegisterTrader;
import com.pixelmongenerations.core.network.packetHandlers.trading.SetSelectedStats;
import com.pixelmongenerations.core.network.packetHandlers.trading.SetTradeTarget;
import com.pixelmongenerations.core.network.packetHandlers.trading.TradeReady;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;

public class TileEntityTradeMachine
extends TileEntity
implements ITickable,
ISpecialTexture {
    private static final HashMap<String, ResourceLocation> textures = new HashMap();
    private String colour = "";
    private UUID owner = null;
    public int playerCount = 0;
    public EntityPlayerMP player1;
    public EntityPlayerMP player2;
    public boolean ready1;
    public boolean ready2;
    public int pos1 = -1;
    public int pos2 = -1;
    public String user1 = "";
    public String user2 = "";
    public NBTTagCompound poke1;
    public NBTTagCompound poke2;
    private boolean tradePushed = false;
    private boolean rave = false;
    private short raveTick = 0;

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setString("colour", this.colour);
        compound.setBoolean("rave", this.rave);
        if (this.owner != null) {
            compound.setString("owner", this.owner.toString());
        }
        return compound;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if (compound.hasKey("colour")) {
            this.colour = compound.getString("colour");
            this.rave = compound.getBoolean("rave");
        } else {
            this.setColour("red");
        }
        if (compound.hasKey("owner")) {
            this.owner = UUID.fromString(compound.getString("owner"));
        }
    }

    private void refreshTexture() {
        if (this.hasWorld() && !this.world.isRemote) {
            ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
            this.markDirty();
        }
    }

    public String getColour() {
        return this.colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
        this.refreshTexture();
    }

    @Override
    public ResourceLocation getTexture() {
        if (this.colour.isEmpty() || !textures.containsKey(this.colour)) {
            return textures.get("red");
        }
        return textures.get(this.colour);
    }

    public UUID getOwnerUUID() {
        return this.owner;
    }

    public void setOwner(UUID owner) {
        this.owner = owner;
        this.markDirty();
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        nbt.setString("user1", this.player1 == null ? "" : this.player1.getName());
        nbt.setString("user2", this.player2 == null ? "" : this.player2.getName());
        nbt.setBoolean("ready1", this.ready1);
        nbt.setBoolean("ready2", this.ready2);
        if (this.poke1 != null) {
            nbt.setTag("poke1", this.poke1);
        }
        if (this.poke2 != null) {
            nbt.setTag("poke2", this.poke2);
        }
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager networkManager, SPacketUpdateTileEntity packet) {
        NBTTagCompound data = packet.getNbtCompound();
        this.colour = data.getString("colour");
        this.user1 = data.getString("user1");
        this.user2 = data.getString("user2");
        this.ready1 = data.getBoolean("ready1");
        this.ready2 = data.getBoolean("ready2");
        NBTTagCompound nBTTagCompound = data.hasKey("poke1") ? (data.getCompoundTag("poke1").isEmpty() ? null : data.getCompoundTag("poke1")) : (this.poke1 = null);
        this.poke2 = data.hasKey("poke2") ? (data.getCompoundTag("poke2").isEmpty() ? null : data.getCompoundTag("poke2")) : null;
        this.refreshTexture();
    }

    public void registerPlayer(EntityPlayerMP player) {
        if (this.playerCount == 1 && this.player1 == player) {
            return;
        }
        ++this.playerCount;
        if (this.playerCount == 1) {
            this.player1 = player;
        } else if (this.playerCount == 2) {
            this.player2 = player;
        } else {
            return;
        }
        player.openGui(Pixelmon.INSTANCE, EnumGui.Trading.getIndex(), player.world, this.pos.getX(), this.pos.getY(), this.pos.getZ());
        if (player == this.player2) {
            Optional<PlayerStorage> optstorage;
            Pixelmon.NETWORK.sendTo(new RegisterTrader(this.player1.getUniqueID()), this.player2);
            Pixelmon.NETWORK.sendTo(new RegisterTrader(this.player2.getUniqueID()), this.player1);
            if (this.pos1 != -1 && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(this.player1)).isPresent()) {
                PlayerStorage s = optstorage.get();
                NBTTagCompound n = s.getNBT(s.getIDFromPosition(this.pos1));
                Pixelmon.NETWORK.sendTo(new SetTradeTarget(new PixelmonData(n), new PixelmonStatsData(n)), this.player2);
                Pixelmon.NETWORK.sendTo(new SetSelectedStats(new PixelmonStatsData(n)), this.player1);
            }
        }
        this.sendChanges();
    }

    public boolean ready(EntityPlayer player, boolean ready) {
        if (this.player1 != null && this.player2 != null) {
            if (player.getUniqueID().equals(this.player1.getUniqueID())) {
                this.ready1 = ready;
                Pixelmon.NETWORK.sendTo(new TradeReady(ready), this.player2);
            }
            if (player.getUniqueID().equals(this.player2.getUniqueID())) {
                this.ready2 = ready;
                Pixelmon.NETWORK.sendTo(new TradeReady(ready), this.player1);
            }
        }
        this.tradePushed = false;
        this.sendChanges();
        return false;
    }

    public void setPos1(int pos) {
        this.ready1 = false;
        this.ready2 = false;
        Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(this.player1);
        if (optstorage.isPresent()) {
            Optional<EntityPixelmon> pixelmonOptional;
            PlayerStorage s = optstorage.get();
            this.pos1 = pos;
            NBTTagCompound n = s.getNBT(s.getIDFromPosition(pos));
            if (n == null) {
                return;
            }
            if (s.isInWorld(PixelmonMethods.getID(n)) && (pixelmonOptional = s.getAlreadyExists(PixelmonMethods.getID(n), this.player1.world)).isPresent()) {
                pixelmonOptional.get().catchInPokeball();
            }
            this.poke1 = new NBTTagCompound();
            this.poke1.setString("Name", n.getString("Name"));
            this.poke1.setInteger("Level", n.getInteger("Level"));
            this.poke1.setBoolean("isEgg", n.getBoolean("isEgg"));
            this.poke1.setInteger("eggCycles", n.getInteger("eggCycles"));
            this.poke1.setInteger("Variant", n.getInteger("Variant"));
            this.poke1.setBoolean("IsShiny", n.getBoolean("IsShiny"));
            this.poke1.setInteger("CaughtBall", n.getInteger("CaughtBall"));
            if (n.hasKey("HeldItemStack")) {
                this.poke1.setTag("HeldItemStack", n.getTag("HeldItemStack"));
            }
            this.sendChanges();
            Pixelmon.NETWORK.sendTo(new SetSelectedStats(new PixelmonStatsData(n)), this.player1);
            if (this.player2 == null) {
                return;
            }
            Pixelmon.NETWORK.sendTo(new SetTradeTarget(new PixelmonData(n), new PixelmonStatsData(n)), this.player2);
        }
    }

    public void setPos2(int pos) {
        this.ready1 = false;
        this.ready2 = false;
        Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(this.player2);
        if (optstorage.isPresent()) {
            Optional<EntityPixelmon> pixelmonOptional;
            PlayerStorage s = optstorage.get();
            this.pos2 = pos;
            NBTTagCompound n = s.getNBT(s.getIDFromPosition(pos));
            if (n == null) {
                return;
            }
            if (s.isInWorld(PixelmonMethods.getID(n)) && (pixelmonOptional = s.getAlreadyExists(PixelmonMethods.getID(n), this.player2.world)).isPresent()) {
                pixelmonOptional.get().catchInPokeball();
            }
            this.poke2 = new NBTTagCompound();
            this.poke2.setString("Name", n.getString("Name"));
            this.poke2.setInteger("Level", n.getInteger("Level"));
            this.poke2.setBoolean("isEgg", n.getBoolean("isEgg"));
            this.poke2.setInteger("eggCycles", n.getInteger("eggCycles"));
            this.poke2.setInteger("Variant", n.getInteger("Variant"));
            this.poke2.setBoolean("IsShiny", n.getBoolean("IsShiny"));
            this.poke2.setInteger("CaughtBall", n.getInteger("CaughtBall"));
            if (n.hasKey("HeldItemStack")) {
                this.poke2.setTag("HeldItemStack", n.getTag("HeldItemStack"));
            }
            this.sendChanges();
            Pixelmon.NETWORK.sendTo(new SetSelectedStats(new PixelmonStatsData(n)), this.player2);
            if (this.player1 == null) {
                return;
            }
            Pixelmon.NETWORK.sendTo(new SetTradeTarget(new PixelmonData(n), new PixelmonStatsData(n)), this.player1);
        }
    }

    public void removePlayer(EntityPlayer player) {
        this.ready1 = false;
        this.ready2 = false;
        if (this.player1 == player) {
            this.player1 = this.player2;
            this.player2 = null;
            this.pos1 = this.pos2;
            this.pos2 = -1;
            this.poke1 = this.poke2;
            this.poke2 = null;
            --this.playerCount;
        } else if (this.player2 == player) {
            this.player2 = null;
            this.pos2 = -1;
            this.poke2 = null;
            --this.playerCount;
        }
        if (this.playerCount < 0) {
            this.playerCount = 0;
        }
        if (this.playerCount == 1) {
            Pixelmon.NETWORK.sendTo(new RegisterTrader(null), this.player1);
            Pixelmon.NETWORK.sendTo(new SetTradeTarget(true), this.player1);
        }
        this.sendChanges();
    }

    public void trade() {
        if (this.tradePushed || this.playerCount < 2 || !this.ready1 || !this.ready2) {
            return;
        }
        Optional<PlayerStorage> optstorage1 = PixelmonStorage.pokeBallManager.getPlayerStorage(this.player1);
        Optional<PlayerStorage> optstorage2 = PixelmonStorage.pokeBallManager.getPlayerStorage(this.player2);
        if (optstorage1.isPresent() && optstorage2.isPresent()) {
            NBTTagCompound pokemon2;
            PlayerStorage storage1 = optstorage1.get();
            PlayerStorage storage2 = optstorage2.get();
            this.tradePushed = true;
            NBTTagCompound pokemon1 = storage1.getNBT(storage1.getIDFromPosition(this.pos1));
            if (MinecraftForge.EVENT_BUS.post(new PixelmonTradeEvent.PlayerTradeEvent(this.player1, this.player2, pokemon1, pokemon2 = storage2.getNBT(storage2.getIDFromPosition(this.pos2))))) {
                this.player1.closeScreen();
                this.player2.closeScreen();
                this.playerCount = 0;
                this.sendChanges();
                return;
            }
            EnumSpecies species = EnumSpecies.getFromNameAnyCase(pokemon1.getString("Name"));
            storage1.changePokemonAndAssignID(this.pos1, pokemon2);
            if (!MinecraftForge.EVENT_BUS.post(new PokedexEvent(this.player1, storage1.pokedex, species, EnumPokedexRegisterStatus.caught))) {
                storage1.pokedex.set(species, EnumPokedexRegisterStatus.caught);
                storage1.pokedex.sendToPlayer(this.player1);
            }
            species = EnumSpecies.getFromNameAnyCase(pokemon1.getString("Name"));
            storage2.changePokemonAndAssignID(this.pos2, pokemon1);
            if (!MinecraftForge.EVENT_BUS.post(new PokedexEvent(this.player2, storage2.pokedex, species, EnumPokedexRegisterStatus.caught))) {
                storage2.pokedex.set(species, EnumPokedexRegisterStatus.caught);
                storage2.pokedex.sendToPlayer(this.player2);
            }
            this.player1.closeScreen();
            this.player2.closeScreen();
            this.playerCount = 0;
            EntityPixelmon pixelmon1 = storage1.sendOut(PixelmonMethods.getID(pokemon2), this.world);
            EntityPixelmon pixelmon2 = storage2.sendOut(PixelmonMethods.getID(pokemon1), this.world);
            EnumSpecies species1 = pixelmon1.getSpecies();
            EnumSpecies species2 = pixelmon2.getSpecies();
            pixelmon1.friendship.setFriendship(pixelmon1.baseStats.baseFriendship);
            pixelmon1.update(EnumUpdateType.Friendship);
            pixelmon1.testTradeEvolution(species2);
            MinecraftForge.EVENT_BUS.post(new PixelmonReceivedEvent(this.player1, ReceiveType.Trade, pixelmon1));
            pixelmon2.friendship.setFriendship(pixelmon2.baseStats.baseFriendship);
            pixelmon2.update(EnumUpdateType.Friendship);
            pixelmon2.testTradeEvolution(species1);
            MinecraftForge.EVENT_BUS.post(new PixelmonReceivedEvent(this.player2, ReceiveType.Trade, pixelmon2));
            ChatHandler.sendChat(this.player1, "pixelmon.blocks.tradesuccess", pokemon1.getString("Name"), this.player2.getName(), pokemon2.getString("Name"));
            ChatHandler.sendChat(this.player2, "pixelmon.blocks.tradesuccess", pokemon2.getString("Name"), this.player1.getName(), pokemon1.getString("Name"));
        }
        this.sendChanges();
    }

    @Override
    public void update() {
        if (this.player1 == null && this.playerCount > 0) {
            this.player1 = this.player2;
            this.player2 = null;
            --this.playerCount;
            this.pos2 = -1;
            this.poke2 = null;
        } else if (this.player2 == null && this.playerCount > 1) {
            --this.playerCount;
            this.pos2 = -1;
            this.poke2 = null;
        }
        if (this.playerCount == 1) {
            Pixelmon.NETWORK.sendTo(new RegisterTrader(null), this.player1);
        }
        if (this.getRave() && this.raveTick % 20 == 0) {
            this.setColour(EnumDyeColor.values()[RandomHelper.rand.nextInt(EnumDyeColor.values().length)].getName());
        } else {
            this.raveTick = (short)(this.raveTick + 1);
        }
    }

    public void sendChanges() {
        if (this.hasWorld()) {
            ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
        }
    }

    public boolean getRave() {
        return this.rave;
    }

    public void setRave(boolean rave) {
        this.rave = rave;
    }

    static {
        for (EnumDyeColor color : EnumDyeColor.values()) {
            textures.put(color.getName().toLowerCase(), new ResourceLocation("pixelmon:textures/blocks/trademachine/texture_" + color.getName() + ".png"));
        }
    }
}

