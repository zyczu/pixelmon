/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.tileEntities.TileEntityPrisonBottleStem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;

public class TileEntityPrisonBottle
extends TileEntityPrisonBottleStem {
    @Override
    public int getRingCount() {
        return 6;
    }

    @Override
    public void activate(EntityPlayer player, ItemStack item, int x, int y, int z) {
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return null;
    }
}

