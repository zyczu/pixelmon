/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.enums.ColorEnum;
import com.pixelmongenerations.common.block.machines.BlockVendingMachine;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import java.util.EnumMap;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class TileEntityVendingMachine
extends TileEntity
implements ISpecialTexture {
    private static EnumMap<ColorEnum, ResourceLocation> textures = new EnumMap(ColorEnum.class);

    @Override
    public ResourceLocation getTexture() {
        ColorEnum color = ColorEnum.Red;
        IBlockState state = this.getWorld().getBlockState(this.getPos());
        if (state.getBlock() instanceof BlockVendingMachine) {
            color = ((BlockVendingMachine)state.getBlock()).getColor();
        }
        return textures.get((Object)color);
    }

    static {
        for (ColorEnum color : ColorEnum.values()) {
            textures.put(color, new ResourceLocation("pixelmon:textures/blocks/vending-machine/vending-machine-" + color.name().toLowerCase() + ".png"));
        }
    }
}

