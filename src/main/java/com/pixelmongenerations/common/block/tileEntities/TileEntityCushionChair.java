/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.decorative.CushionChair;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class TileEntityCushionChair
extends TileEntity
implements ISpecialTexture {
    @Override
    public ResourceLocation getTexture() {
        IBlockState state = this.getWorld().getBlockState(this.getPos());
        if (state.getBlock() instanceof CushionChair) {
            CushionChair block = (CushionChair)state.getBlock();
            return new ResourceLocation("pixelmon:textures/blocks/" + block.getColor().name() + "CushionChairModel.png");
        }
        return new ResourceLocation("pixelmon:textures/blocks/RedCushionChairModel.png");
    }
}

