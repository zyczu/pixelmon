/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.client.models.blocks.GenericSmdModel;
import com.pixelmongenerations.client.render.BlockModelHolder;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class TileEntityDoubleStreetLamp
extends TileEntity
implements ISpecialTexture {
    private ResourceLocation texture;
    private BlockModelHolder<GenericSmdModel> model;

    public TileEntityDoubleStreetLamp() {
        this.getModelAndTexture();
    }

    public void getModelAndTexture() {
        this.texture = new ResourceLocation("pixelmon:textures/blocks/double_street_lamp.png");
        this.model = new BlockModelHolder("blocks/double_street_lamp.pqc");
    }

    @Override
    public ResourceLocation getTexture() {
        if (this.texture == null) {
            this.getModelAndTexture();
        }
        return this.texture;
    }

    public BlockModelHolder<GenericSmdModel> getModel() {
        if (this.model == null) {
            this.getModelAndTexture();
        }
        return this.model;
    }
}

