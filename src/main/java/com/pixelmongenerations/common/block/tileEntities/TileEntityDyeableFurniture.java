/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.tileEntities.ICullable;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.WorldServer;

public class TileEntityDyeableFurniture
extends TileEntity
implements ICullable {
    private boolean culled;
    private String customTexture;

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        if (this.customTexture != null) {
            compound.setString("CustomTexture", this.customTexture);
        }
        return compound;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        if (compound.hasKey("CustomTexture")) {
            this.customTexture = compound.getString("CustomTexture");
        }
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager networkManager, SPacketUpdateTileEntity packet) {
        if (packet.getNbtCompound().hasKey("CustomTexture")) {
            this.customTexture = packet.getNbtCompound().getString("CustomTexture");
        }
    }

    public void setCustomTexture(String customTexture) {
        this.customTexture = customTexture;
        this.refreshTexture();
    }

    public String getCustomTexture() {
        return this.customTexture;
    }

    private void refreshTexture() {
        if (this.hasWorld() && !this.world.isRemote) {
            ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
            this.markDirty();
        }
    }

    @Override
    public void setCulled(boolean isCulled) {
        this.culled = isCulled;
    }

    @Override
    public boolean isCulled() {
        return this.culled;
    }
}

