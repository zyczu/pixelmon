/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import net.minecraft.tileentity.TileEntity;

public class TileEntityLitwickCandle
extends TileEntity {
    public int renderPass;

    @Override
    public boolean shouldRenderInPass(int pass) {
        this.renderPass = pass;
        return true;
    }
}

