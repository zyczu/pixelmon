/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.HashBasedTable
 *  com.google.common.collect.Table
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.pixelmongenerations.common.block.BlockBerryTree;
import com.pixelmongenerations.common.block.BlockSoftSoil;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.EnumMulch;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.lang.invoke.LambdaMetafactory;
import java.util.function.Function;
import java.util.stream.Stream;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

public class TileEntityBerryTree
extends TileEntity
implements ITickable,
ISpecialTexture {
    public static final Table<EnumBerry, EnumBerry, EnumBerry> berryMutations;
    private static final boolean DEBUG = true;
    private EnumBerry berry;
    private EnumBerry mutation;
    private byte stage = 1;
    private double projectedYield;
    private boolean initalized;
    private short timeUntilWeeds = (short)2;
    private short timeUntilDry = (short)-1;
    private short timeUntilInfested = (short)2;
    private short timeUntilDeath = (short)-1;
    private short timeUntilNextStage = (short)-1;
    private short regrowAmount = (short)-1;
    private boolean isDry = false;
    private boolean isInfested = false;
    private boolean isWeeded = false;
    private boolean hasMutation = false;
    private boolean isGenerated = false;
    private long lastWorldTime = -1L;
    private static final ResourceLocation berryTreeSeedTexture;
    private static final ResourceLocation berryTreeSproutTexture;
    private ResourceLocation textureLocation = null;

    public byte getStage() {
        return this.stage;
    }

    public void setStage(byte i) {
        this.stage = i;
        this.calculateNextGrowthStage();
    }

    public short getProjectedYield() {
        if (this.isGenerated && this.projectedYield > 2.0) {
            this.projectedYield = this.berry.minYield + (this.world.rand.nextInt(2) + 1);
        }
        return (short)((short)this.projectedYield + this.getMulch().getBaseYieldIncrease());
    }

    public TileEntityBerryTree() {
    }

    public TileEntityBerryTree(EnumBerry berry) {
        this.berry = berry;
        this.projectedYield = berry.minYield;
    }

    private void calculateNextGrowthStage() {
        this.timeUntilNextStage = (short)(this.getMulch().modifyGrowthTime(this.getBerry().growthTime) / 6);
    }

    public void calculateDriedOut() {
        this.timeUntilDry = (short)this.getMulch().modifyDryoutTime(24);
    }

    public EnumMulch getMulch() {
        return BlockSoftSoil.getMulch(this.world, this.pos.down());
    }

    @Override
    public void update() {
        if (!this.world.isRemote) {
            long currentWorldTime = this.world.getTotalWorldTime();
            if (this.lastWorldTime == -1L || this.lastWorldTime > currentWorldTime) {
                this.lastWorldTime = currentWorldTime;
            }
            if (!this.initalized) {
                this.initalized = true;
                this.calculateDriedOut();
                this.calculateNextGrowthStage();
                this.regrowAmount = (short)this.getMulch().modifyGrowthAmount(9);
            }
            int ticksPerHour = (int)(1000.0f * PixelmonConfig.berryTreeGrowthMultiplier);
            if (!this.isGenerated && this.getBlock() != null) {
                while (currentWorldTime - this.lastWorldTime > (long)ticksPerHour) {
                    this.onHour();
                    this.lastWorldTime += (long)ticksPerHour;
                }
            }
        } else {
            this.world.getTotalWorldTime();
        }
    }

    public void onHour() {
        boolean dry = this.checkisDry();
        boolean infested = this.checkInfested();
        boolean weeded = this.checkWeeded();
        boolean mutuated = this.checkMutation();
        boolean grown = this.checkGrowth();
        if (grown || this.stage != 6 && (dry || infested || weeded || mutuated)) {
            this.updateBlock();
        }
    }

    private void updateBlock() {
        ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
    }

    private boolean checkMutation() {
        if (!this.hasMutation && RandomHelper.getRandomChance(this.getMulch().isIncreasedMutationChance() ? 60 : 30)) {
            this.hasMutation = true;
            EnumBerry b = Stream.of(EnumFacing.HORIZONTALS)
                    .map(facing -> this.pos.offset(facing))
                    .map(blockPos -> this.world.getTileEntity(blockPos))
                    .filter(TileEntityBerryTree.class::isInstance).map(TileEntityBerryTree.class::cast)
                    .map(TileEntityBerryTree::getBerry).findFirst().orElse(null);
            if (b != null) {
                EnumBerry mutationBerry = (EnumBerry)berryMutations.get(this.berry, b);
                if (mutationBerry == null) {
                    mutationBerry = (EnumBerry)berryMutations.get(b, this.berry);
                }
                this.mutation = mutationBerry;
                return true;
            }
            this.mutation = null;
        }
        return false;
    }

    private boolean checkWeeded() {
        if (!this.isWeeded && (this.timeUntilWeeds = (short)(this.timeUntilWeeds - 1)) == 0) {
            this.timeUntilWeeds = (short)-1;
            this.isWeeded = true;
            return true;
        }
        return false;
    }

    private boolean checkInfested() {
        if (!this.isInfested && (this.timeUntilInfested = (short)(this.timeUntilInfested - 1)) == 0) {
            this.timeUntilInfested = (short)-1;
            this.isInfested = true;
            return true;
        }
        return false;
    }

    private boolean checkisDry() {
        if (!this.isDry && (this.timeUntilDry = (short)(this.timeUntilDry - 1)) == 0) {
            this.timeUntilDry = (short)-1;
            this.isDry = true;
            return !this.world.isRainingAt(this.getPos()) || this.water();
        }
        return false;
    }

    private boolean checkGrowth() {
        boolean markForUpdate = false;
        if (this.stage != 6 && (this.timeUntilNextStage = (short)(this.timeUntilNextStage - 1)) >= 0) {
            if (this.getBlock().canGrowStage(this.world, this.getPos(), this.world.getBlockState(this.getPos()), this.getStage())) {
                this.getBlock().growStage(this.world, this.world.rand, this.getPos(), this.world.getBlockState(this.getPos()));
                markForUpdate = true;
                if (this.stage == 6) {
                    this.timeUntilDeath = (short)this.getMulch().modifyUntilDeath(this.getBerry().growthTime);
                    this.timeUntilNextStage = (short)-1;
                }
            }
        } else if (this.stage == 6 && (this.timeUntilDeath = (short)(this.timeUntilDeath - 1)) == 0) {
            markForUpdate = true;
            this.regrowAmount = (short)(this.regrowAmount - 1);
            if (this.regrowAmount != 0) {
                this.getBlock().replant(this.world, this.getPos(), this.world.getBlockState(this.getPos()));
                this.replant();
            } else {
                this.world.setBlockState(this.pos, Blocks.AIR.getDefaultState(), 3);
                if (this.berry.height > 1) {
                    this.world.setBlockState(this.pos.up(), Blocks.AIR.getDefaultState(), 3);
                }
            }
        }
        return markForUpdate;
    }

    private BlockBerryTree getBlock() {
        Block block = this.world.getBlockState(this.getPos()).getBlock();
        if (block instanceof BlockBerryTree) {
            return (BlockBerryTree)block;
        }
        return null;
    }

    public void replant() {
        this.projectedYield = this.getBerry().minYield;
        this.timeUntilWeeds = (short)2;
        this.timeUntilInfested = (short)2;
        this.timeUntilDeath = (short)-1;
        this.calculateDriedOut();
        this.calculateNextGrowthStage();
        this.isDry = false;
        this.isInfested = false;
        this.isWeeded = false;
        this.hasMutation = false;
        this.mutation = null;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setByte("typeOrdinal", (byte)this.berry.ordinal());
        nbt.setByte("stage", this.stage);
        nbt.setShort("timeUntilWeeds", this.timeUntilWeeds);
        nbt.setShort("timeUntilDry", this.timeUntilDry);
        nbt.setShort("timeUntilInfested", this.timeUntilInfested);
        nbt.setShort("timeUntilDeath", this.timeUntilDeath);
        nbt.setShort("timeUntilNextStage", this.timeUntilNextStage);
        nbt.setBoolean("isDry", this.isDry);
        nbt.setBoolean("isInfested", this.isInfested);
        nbt.setBoolean("isWeeded", this.isWeeded);
        nbt.setDouble("projectedYield", this.projectedYield);
        nbt.setLong("lastWorldTime", this.lastWorldTime);
        nbt.setBoolean("isGenerated", this.isGenerated);
        nbt.setBoolean("hasMutation", this.hasMutation);
        if (this.mutation != null) {
            nbt.setByte("mutation", (byte)this.mutation.ordinal());
        }
        nbt.setBoolean("initialized", this.initalized);
        nbt.setShort("regrowAmount", this.regrowAmount);
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.berry = EnumBerry.fromId(nbt.getByte("typeOrdinal"));
        this.stage = nbt.getByte("stage");
        this.projectedYield = nbt.getDouble("projectedYield");
        this.lastWorldTime = nbt.getLong("lastWorldTime");
        this.isGenerated = nbt.getBoolean("isGenerated");
        this.initalized = nbt.getBoolean("initialized");
        if (this.isGenerated) {
            this.stage = (byte)6;
            this.initalized = true;
        }
        this.timeUntilWeeds = (short)(nbt.hasKey("timeUntilWeeds") ? (int)nbt.getShort("timeUntilWeeds") : 2);
        this.timeUntilDry = (short)(nbt.hasKey("timeUntilDry") ? (int)nbt.getShort("timeUntilDry") : -1);
        this.timeUntilInfested = (short)(nbt.hasKey("timeUntilInfested") ? (int)nbt.getShort("timeUntilInfestation") : 2);
        this.timeUntilDeath = (short)(nbt.hasKey("timeUntilDeath") ? (int)nbt.getShort("timeUntilDeath") : -1);
        this.timeUntilNextStage = (short)(nbt.hasKey("timeUntilNextStage") ? (int)nbt.getShort("timeUntilNextStage") : -1);
        this.isDry = nbt.getBoolean("isDry");
        this.isInfested = nbt.getBoolean("isInfested");
        this.isWeeded = nbt.getBoolean("isWeeded");
        this.regrowAmount = nbt.getShort("regrowAmount");
        this.hasMutation = nbt.getBoolean("hasMutation");
        if (nbt.hasKey("mutation")) {
            this.mutation = EnumBerry.fromId(nbt.getByte("mutation"));
        }
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBTClient(nbt);
        return new SPacketUpdateTileEntity(this.pos, 1, nbt);
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBTClient(pkt.getNbtCompound());
    }

    private void readFromNBTClient(NBTTagCompound nbt) {
        this.berry = EnumBerry.fromId(nbt.getByte("typeOrdinal"));
        this.stage = nbt.getByte("stage");
        this.isDry = nbt.getBoolean("isDry");
        this.isInfested = nbt.getBoolean("isInfested");
        this.isWeeded = nbt.getBoolean("isWeeded");
        this.hasMutation = nbt.getBoolean("hasMutation");
    }

    private void writeToNBTClient(NBTTagCompound nbt) {
        nbt.setByte("typeOrdinal", (byte)this.berry.ordinal());
        nbt.setByte("stage", this.stage);
        nbt.setBoolean("isDry", this.isDry);
        nbt.setBoolean("isInfested", this.isInfested);
        nbt.setBoolean("isWeeded", this.isWeeded);
        nbt.setBoolean("hasMutation", this.hasMutation);
    }

    @Override
    public ResourceLocation getTexture() {
        switch (this.stage) {
            case 1: {
                return berryTreeSeedTexture;
            }
            case 2: {
                return berryTreeSproutTexture;
            }
        }
        if (this.textureLocation == null) {
            this.textureLocation = new ResourceLocation("pixelmon:textures/berries/" + this.getBerry().name().toLowerCase() + ".png");
        }
        return this.textureLocation;
    }

    public void setGenerated() {
        this.isGenerated = true;
        this.initalized = true;
        this.stage = (byte)6;
    }

    public EnumBerry getBerry() {
        return this.berry;
    }

    public boolean water() {
        if (this.isDry) {
            this.isDry = false;
            this.calculateDriedOut();
            this.addToYield(this.getBerry().getWaterAmount());
            this.updateBlock();
            return true;
        }
        return false;
    }

    public boolean removeWeeds() {
        if (this.isWeeded()) {
            this.isWeeded = false;
            this.timeUntilWeeds = (short)2;
            this.addToYield(this.getBerry().getWeedingAmount());
            this.updateBlock();
            return true;
        }
        return false;
    }

    public boolean removePests() {
        if (this.isInfested()) {
            this.isInfested = false;
            this.timeUntilInfested = (short)2;
            this.addToYield(this.getBerry().getPestRemovalAmount());
            this.updateBlock();
            return true;
        }
        return false;
    }

    private void addToYield(double amount) {
        this.projectedYield = Math.min(this.projectedYield + amount, (double)this.berry.maxYield);
        System.out.println(this.projectedYield);
    }

    public short getTimeUntilWeeds() {
        return this.timeUntilWeeds;
    }

    public short getTimeUntilDry() {
        return this.timeUntilDry;
    }

    public short getTimeUntilInfestation() {
        return this.timeUntilInfested;
    }

    public boolean isDry() {
        return this.isDry;
    }

    public boolean isInfested() {
        return this.isInfested;
    }

    public boolean isWeeded() {
        return this.isWeeded;
    }

    public boolean isGenerated() {
        return this.isGenerated;
    }

    public long getLastWorldTime() {
        return this.lastWorldTime;
    }

    public short getTimeUntilDeath() {
        return this.timeUntilDeath;
    }

    public short getTimeUntilNextStage() {
        return this.timeUntilNextStage;
    }

    public EnumBerry getBerryMutation() {
        return this.mutation;
    }

    static {
        berryTreeSeedTexture = new ResourceLocation("pixelmon:textures/berries/seeded.png");
        berryTreeSproutTexture = new ResourceLocation("pixelmon:textures/berries/sprouted.png");
        berryMutations = HashBasedTable.create();
        berryMutations.put(EnumBerry.Iapapa, EnumBerry.Mago, EnumBerry.Pomeg);
        berryMutations.put(EnumBerry.Chesto, EnumBerry.Persim, EnumBerry.Kelpsy);
        berryMutations.put(EnumBerry.Oran, EnumBerry.Pecha, EnumBerry.Qualot);
        berryMutations.put(EnumBerry.Aspear, EnumBerry.Figy, EnumBerry.Grepa);
        berryMutations.put(EnumBerry.Lum, EnumBerry.Sitrus, EnumBerry.Tamato);
        berryMutations.put(EnumBerry.Hondew, EnumBerry.Yache, EnumBerry.Liechi);
        berryMutations.put(EnumBerry.Qualot, EnumBerry.Tanga, EnumBerry.Ganlon);
        berryMutations.put(EnumBerry.Grepa, EnumBerry.Roseli, EnumBerry.Salac);
        berryMutations.put(EnumBerry.Pomeg, EnumBerry.Kasib, EnumBerry.Petaya);
        berryMutations.put(EnumBerry.Kelpsy, EnumBerry.Wacan, EnumBerry.Apicot);
        berryMutations.put(EnumBerry.Ganlon, EnumBerry.Liechi, EnumBerry.Kee);
        berryMutations.put(EnumBerry.Salac, EnumBerry.Petaya, EnumBerry.Maranga);
    }
}

