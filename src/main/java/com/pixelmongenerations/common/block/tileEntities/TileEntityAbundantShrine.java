/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.extraStats.MeloettaStats;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.IntStream;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class TileEntityAbundantShrine
extends TileEntityShrine {
    @Override
    public void activate(EntityPlayer player, World world, BlockShrine block, IBlockState state, ItemStack item) {
        if (this.isSpawning()) {
            return;
        }
        this.startSpawning(player, state, world, this.pos);
        PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
        Optional<EntityPixelmon> pixelmonOptional = IntStream.range(0, 6).mapToObj(storage::getIDFromPosition).map(id -> storage.getPokemon((int[])id, player.world)).filter(Objects::nonNull).filter(a -> a.getSpecies() == EnumSpecies.Meloetta).findFirst();
        if (pixelmonOptional.isPresent()) {
            EntityPixelmon pixelmon = pixelmonOptional.get();
            MeloettaStats stats = (MeloettaStats)pixelmon.extraStats;
            ++stats.abundantActivations;
            pixelmon.update(EnumUpdateType.AbundantActivations);
        }
        item.shrink(1);
    }

    @Override
    protected PokemonGroup.PokemonData selectActiveGroup() {
        if (this.summoningPlayer != null) {
            PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID(this.summoningPlayer.getUniqueID()).get();
            boolean hasLandorus = false;
            boolean hasThundurus = false;
            boolean hasTornadus = false;
            for (int i = 0; i < 6; ++i) {
                EntityPixelmon pokemon = storage.getPokemon(storage.getIDFromPosition(i), this.world);
                if (pokemon == null) continue;
                if (pokemon.getSpecies() == EnumSpecies.Landorus) {
                    hasLandorus = true;
                    continue;
                }
                if (pokemon.getSpecies() == EnumSpecies.Thundurus) {
                    hasThundurus = true;
                    continue;
                }
                if (pokemon.getSpecies() != EnumSpecies.Tornadus) continue;
                hasTornadus = true;
            }
            if (hasLandorus && hasThundurus && hasTornadus) {
                return PokemonGroup.PokemonData.of(EnumSpecies.Enamorus);
            }
            int a = (int)(Math.random() * 3.0);
            if (a == 0) {
                return PokemonGroup.PokemonData.of(EnumSpecies.Landorus);
            }
            if (a == 1) {
                return PokemonGroup.PokemonData.of(EnumSpecies.Tornadus);
            }
            if (a == 2) {
                return PokemonGroup.PokemonData.of(EnumSpecies.Thundurus);
            }
            return PokemonGroup.PokemonData.of(EnumSpecies.Landorus);
        }
        int a = (int)(Math.random() * 3.0);
        if (a == 0) {
            return PokemonGroup.PokemonData.of(EnumSpecies.Landorus);
        }
        if (a == 1) {
            return PokemonGroup.PokemonData.of(EnumSpecies.Tornadus);
        }
        if (a == 2) {
            return PokemonGroup.PokemonData.of(EnumSpecies.Thundurus);
        }
        return PokemonGroup.PokemonData.of(EnumSpecies.Landorus);
    }

    @Override
    public int maxTick() {
        return 100;
    }

    @Override
    public String getRightClickMessage(ItemStack heldItem, IBlockState state) {
        return "pixelmon.abundant_shrine.right_click";
    }
}

