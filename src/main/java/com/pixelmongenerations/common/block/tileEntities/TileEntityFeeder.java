/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.api.events.FeederSpawnEvent;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.battle.nobles.NobleBattle;
import com.pixelmongenerations.common.battle.nobles.NobleBattlePlayer;
import com.pixelmongenerations.common.battle.nobles.NobleBattlePokemon;
import com.pixelmongenerations.common.block.machines.FeederSpawns;
import com.pixelmongenerations.common.block.tileEntities.AugmentedItemStackHandler;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.UpdateFeederPokemon;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Random;
import java.util.UUID;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;

public class TileEntityFeeder
extends TileEntity
implements ITickable {
    private UUID ownerUUID = null;
    private String playerName = "";
    private boolean isNoble = false;
    private Random random = new Random();
    private AugmentedItemStackHandler inventory = new AugmentedItemStackHandler(6){

        @Override
        protected void onContentsChanged(int slot) {
            super.onContentsChanged(slot);
            TileEntityFeeder.this.markDirty();
        }
    };
    private String customName;
    public EntityPixelmon pokemon = null;
    public int coolDown = PixelmonConfig.pokeBlockTickTime;
    public int pokemonLeaves = 7200;

    @Override
    public void update() {
        if (!this.world.isRemote) {
            boolean hasPokeBlock;
            boolean hasPokemon = this.pokemon != null;
            boolean bl = hasPokeBlock = !this.inventory.isEmpty() || this.inventory.fillIfEmpty(0);
            if (hasPokeBlock) {
                if (!hasPokemon) {
                    this.attractPokemon();
                }
                this.coolDown -= hasPokemon ? 2 : 1;
                if (this.coolDown <= 0) {
                    this.inventory.extractItem(0, 1, false);
                    this.coolDown = PixelmonConfig.pokeBlockTickTime;
                }
            } else if (hasPokemon) {
                if (this.pokemonLeaves > 0) {
                    --this.pokemonLeaves;
                } else {
                    this.pokemon = null;
                    this.pokemonLeaves = 7200;
                    this.coolDown = PixelmonConfig.pokeBlockTickTime;
                }
                this.updateClientPokemon();
            }
        }
    }

    public void updateClientPokemon() {
        Pixelmon.NETWORK.sendToAllAround(new UpdateFeederPokemon(this.pokemon == null ? null : new PixelmonData(this.pokemon)), new NetworkRegistry.TargetPoint(this.world.provider.getDimension(), this.pos.getX(), this.pos.getY(), this.pos.getZ(), 10.0));
        this.getWorld().notifyNeighborsOfStateChange(this.pos, this.getBlockType(), true);
        ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
    }

    private void attractPokemon() {
        double rand = this.random.nextDouble();
        if (PixelmonConfig.feederAttractChance >= rand) {
            if (PixelmonConfig.feederNobleAttractChance >= rand) {
                if (NobleBattle.nobleBattleMap.containsKey(this.ownerUUID)) {
                    this.pokemon = RandomHelper.getRandomElementFromList(FeederSpawns.getSpawns().getCommonSpawns()).create(this.world);
                } else {
                    this.pokemon = FeederSpawns.getSpawns().getRandomNoble();
                    this.isNoble = true;
                }
            } else {
                this.pokemon = RandomHelper.getRandomElementFromList(FeederSpawns.getSpawns().getCommonSpawns()).create(this.world);
            }
            this.pokemonLeaves = 7200;
            this.updateClientPokemon();
        }
    }

    private void sendUpdates() {
        this.world.markBlockRangeForRenderUpdate(this.pos, this.pos);
        this.world.notifyBlockUpdate(this.pos, this.getState(), this.getState(), 3);
        this.world.scheduleBlockUpdate(this.pos, this.getBlockType(), 0, 0);
        this.markDirty();
    }

    private IBlockState getState() {
        return this.world.getBlockState(this.pos);
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public AxisAlignedBB getRenderBoundingBox() {
        return INFINITE_EXTENT_AABB;
    }

    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;
    }

    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
        if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            return (T)this.inventory;
        }
        return super.getCapability(capability, facing);
    }

    public boolean hasCustomName() {
        return this.customName != null && !this.customName.isEmpty();
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    @Override
    public ITextComponent getDisplayName() {
        return this.hasCustomName() ? new TextComponentString(this.customName) : new TextComponentTranslation("container.feeder", new Object[0]);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        this.pokemon = compound.hasKey("pokemon") ? new PokemonSpec(compound.getString("pokemon")).create(this.world) : null;
        this.coolDown = compound.getInteger("coolDown");
        this.pokemonLeaves = compound.getInteger("pokemonLeaves");
        this.inventory.deserializeNBT(compound.getCompoundTag("inventory"));
        if (compound.hasKey("customName", 8)) {
            this.setCustomName(compound.getString("customName"));
        }
        if (compound.hasKey("playerName")) {
            this.playerName = compound.getString("playerName");
        }
        this.ownerUUID = compound.hasKey("UUIDMost", 4) && compound.hasKey("UUIDLeast", 4) ? new UUID(compound.getLong("UUIDMost"), compound.getLong("UUIDLeast")) : null;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        if (this.pokemon != null) {
            compound.setString("pokemon", this.pokemon.toString());
        }
        compound.setInteger("coolDown", this.coolDown);
        compound.setInteger("pokemonLeaves", this.pokemonLeaves);
        compound.setTag("inventory", this.inventory.serializeNBT());
        if (this.ownerUUID != null) {
            compound.setLong("UUIDMost", this.ownerUUID.getMostSignificantBits());
            compound.setLong("UUIDLeast", this.ownerUUID.getLeastSignificantBits());
        }
        if (!this.playerName.equals("")) {
            compound.setString("playerName", this.playerName);
        }
        if (this.hasCustomName()) {
            compound.setString("customName", this.customName);
        }
        return compound;
    }

    public boolean isUsableByPlayer(EntityPlayer playerIn) {
        if (playerIn.getUniqueID().toString().equalsIgnoreCase(this.ownerUUID.toString())) {
            return true;
        }
        TextComponentString msg = new TextComponentString("\u00a7eOnly the block owner can use this block!");
        playerIn.sendMessage(msg);
        return false;
    }

    public boolean hasPokemon() {
        return this.pokemon != null;
    }

    public void spawnPokemon(EntityPlayerMP player) {
        if (this.pokemon == null) {
            return;
        }
        EntityPixelmon spawnedPokemon = this.pokemon;
        FeederSpawnEvent event = new FeederSpawnEvent(player, spawnedPokemon, this.pos);
        MinecraftForge.EVENT_BUS.post(event);
        if (!event.isCanceled()) {
            int randX = RandomHelper.getRandomNumberBetween(-3, 3) + event.getPos().getX();
            int randZ = RandomHelper.getRandomNumberBetween(-3, 3) + event.getPos().getZ();
            if (event.canBeNoble() && !NobleBattle.nobleBattleMap.containsKey(player.getUniqueID()) && this.isNoble) {
                NobleBattlePokemon noble = new NobleBattlePokemon(spawnedPokemon);
                NobleBattlePlayer noblePlayer = new NobleBattlePlayer(player);
                NobleBattle battle = new NobleBattle(noblePlayer, noble);
                battle.startBattle();
                spawnedPokemon.setNoble(true, player);
            }
            spawnedPokemon.setLocationAndAngles(randX, event.getPos().getY(), randZ, 0.0f, 0.0f);
            this.world.getMinecraftServer().addScheduledTask(() -> this.world.spawnEntity(spawnedPokemon));
        }
        this.pokemon = null;
        this.updateClientPokemon();
    }

    public void setOwner(EntityPlayerMP entity) {
        this.ownerUUID = entity.getUniqueID();
        this.playerName = entity.getDisplayNameString();
    }
}

