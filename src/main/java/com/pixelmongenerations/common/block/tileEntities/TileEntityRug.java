/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.multiBlocks.BlockRug;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class TileEntityRug
extends TileEntity
implements ISpecialTexture {
    @Override
    public ResourceLocation getTexture() {
        IBlockState state = this.getWorld().getBlockState(this.getPos());
        if (state.getBlock() instanceof BlockRug) {
            BlockRug block = (BlockRug)state.getBlock();
            return new ResourceLocation("pixelmon:textures/blocks/rug-" + block.getColor().name() + ".png");
        }
        return new ResourceLocation("pixelmon:textures/blocks/rug-red.png");
    }
}

