/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemFlute;
import com.pixelmongenerations.core.enums.items.EnumFlutes;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.CustomTeleporter;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.IntStream;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntityCelestialAltar
extends TileEntity
implements ITickable {
    public static int timeBeforeWormhole = 200;
    private boolean songPlaying = false;
    private int playTime = 0;

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        NBTTagCompound tagCompound = new NBTTagCompound();
        this.writeToNBT(tagCompound);
        return new SPacketUpdateTileEntity(this.pos, 0, tagCompound);
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
    }

    public void activate(EntityPlayer player, ItemStack item) {
        if (!player.isSneaking() && item.getItem() instanceof ItemFlute && !this.songPlaying) {
            EnumFlutes type = ((ItemFlute)item.getItem()).getType();
            if (type.isCorrectTime(this.world)) {
                PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
                Optional<EntityPixelmon> pixelmonOptional = IntStream.range(0, 6).mapToObj(storage::getIDFromPosition).map(id -> storage.getPokemon((int[])id, this.world)).filter(Objects::nonNull).filter(type::isPokemonUsable).findFirst();
                if (pixelmonOptional.isPresent()) {
                    if (!this.world.isRemote) {
                        int[] location = new int[]{(int)player.posX, (int)player.posY + 1, (int)player.posZ};
                        NBTTagCompound tagCompound = new NBTTagCompound();
                        tagCompound.setIntArray("Location", location);
                        item.setTagCompound(tagCompound);
                    }
                    CustomTeleporter.teleportToDimension(player, 24, 0.0, 100.0, 0.0);
                } else {
                    ChatHandler.sendChat(player, "You lack any pokemon with the sufficent amount of ultra energy needed to form ultra wormholes.", new Object[0]);
                }
            } else {
                ChatHandler.sendChat(player, "A strange feeling tells you it isn't the right time of day.", new Object[0]);
            }
        } else if (player.isSneaking()) {
            ChatHandler.sendChat(player, "This altar requires a Sun or Moon Flute and will transport you to another world.", new Object[0]);
        }
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public AxisAlignedBB getRenderBoundingBox() {
        return INFINITE_EXTENT_AABB;
    }

    @Override
    public void update() {
        if (this.songPlaying) {
            if (this.playTime > timeBeforeWormhole) {
                this.songPlaying = false;
                this.playTime = 0;
            } else {
                ++this.playTime;
            }
        }
    }
}

