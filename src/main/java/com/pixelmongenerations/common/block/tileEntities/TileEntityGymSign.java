/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import java.util.UUID;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.WorldServer;

public class TileEntityGymSign
extends TileEntity
implements ISpecialTexture {
    private ItemStack itemInSign = null;
    private UUID owner = null;
    private String colour = "red";
    private ResourceLocation texture = new ResourceLocation("pixelmon:textures/blocks/gymSign/Texture_red.png");
    private boolean itemDrops = true;

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        ItemStack itemStack = this.itemInSign = nbt.hasKey("ItemIn") ? new ItemStack(nbt.getCompoundTag("ItemIn")) : null;
        if (nbt.hasKey("ItemDrops")) {
            this.itemDrops = nbt.getBoolean("ItemDrops");
        }
        if (nbt.hasKey("owner")) {
            this.owner = UUID.fromString(nbt.getString("owner"));
            this.colour = nbt.getString("colour");
            this.refreshTexture();
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.itemInSign == null) {
            nbt.removeTag("ItemIn");
        } else {
            NBTTagCompound itemTag = new NBTTagCompound();
            this.itemInSign.writeToNBT(itemTag);
            nbt.setTag("ItemIn", itemTag);
        }
        nbt.setBoolean("ItemDrops", this.itemDrops);
        if (this.owner != null) {
            nbt.setString("colour", this.colour);
            nbt.setString("owner", this.owner.toString());
        }
        return nbt;
    }

    private void refreshTexture() {
        this.texture = new ResourceLocation("pixelmon:textures/blocks/gymSign/Texture_" + this.colour + ".png");
    }

    public String getColour() {
        return this.colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
        this.sendChanges();
        this.markDirty();
    }

    @Override
    public ResourceLocation getTexture() {
        return this.texture;
    }

    public void sendChanges() {
        if (this.hasWorld() && this.getWorld() instanceof WorldServer) {
            ((WorldServer)this.getWorld()).getPlayerChunkMap().markBlockForUpdate(this.pos);
        }
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        nbt.setString("colour", this.colour);
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
        NBTTagCompound data = pkt.getNbtCompound();
        this.colour = data.getString("colour");
    }

    public UUID getOwnerUUID() {
        return this.owner;
    }

    public void setOwner(UUID owner) {
        this.owner = owner;
        this.markDirty();
    }

    public ItemStack getItemInSign() {
        return this.itemInSign;
    }

    public void setItemInSign(ItemStack item) {
        this.itemInSign = item;
        this.markDirty();
    }

    public void setDroppable(boolean b) {
        this.itemDrops = false;
    }

    public boolean isDroppable() {
        return this.itemDrops;
    }
}

