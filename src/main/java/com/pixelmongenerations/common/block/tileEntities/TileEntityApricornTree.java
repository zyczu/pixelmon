/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.tileEntities.ICullable;
import com.pixelmongenerations.common.block.tileEntities.TileEntityDecorativeBase;
import com.pixelmongenerations.core.enums.EnumApricornTrees;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.world.WorldServer;

public class TileEntityApricornTree
extends TileEntityDecorativeBase
implements ICullable {
    private short stage = 0;
    public EnumApricornTrees tree;
    public long timeLastWatered;
    private boolean culled;

    public TileEntityApricornTree() {
    }

    public TileEntityApricornTree(EnumApricornTrees tree) {
        this.tree = tree;
        this.timeLastWatered = 0L;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setShort("stage", this.stage);
        nbt.setLong("TimeLastWatered", this.timeLastWatered);
        nbt.setShort("ApricornTreeID", (short)this.tree.id);
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.stage = nbt.getShort("stage");
        this.tree = EnumApricornTrees.getFromID(nbt.getShort("ApricornTreeID"));
        this.timeLastWatered = nbt.getLong("TimeLastWatered");
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
    }

    public boolean wasWateredToday() {
        if (this.timeLastWatered == 0L) {
            return false;
        }
        long tickTimeOfDay = this.world.getWorldTime() % 24000L;
        long tickTimeSinceLastWatering = this.world.getTotalWorldTime() - this.timeLastWatered;
        return tickTimeSinceLastWatering <= tickTimeOfDay;
    }

    public void updateWatering() {
        this.timeLastWatered = this.world.getTotalWorldTime();
    }

    public void setStage(int s) {
        if (this.stage != s && s < 6) {
            this.stage = (short)s;
            if (this.hasWorld() && !this.world.isRemote) {
                ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
                this.markDirty();
            }
        }
    }

    public int getStage() {
        return this.stage;
    }

    @Override
    public void setCulled(boolean isCulled) {
        this.culled = isCulled;
    }

    @Override
    public boolean isCulled() {
        return this.culled;
    }
}

