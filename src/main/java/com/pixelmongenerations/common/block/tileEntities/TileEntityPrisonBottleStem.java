/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.SpawningEntity;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.util.SpawnColors;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TileEntityPrisonBottleStem
extends TileEntity {
    public int renderPass;
    private int ringCount = 0;

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        NBTTagCompound compound = super.writeToNBT(nbt);
        compound.setInteger("hoopaBottleStatus", this.ringCount);
        return compound;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        this.ringCount = compound.getInteger("hoopaBottleStatus");
    }

    @Override
    public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate) {
        return super.shouldRefresh(world, pos, oldState, newSate);
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        NBTTagCompound tagCompound = new NBTTagCompound();
        this.writeToNBT(tagCompound);
        return new SPacketUpdateTileEntity(this.pos, 0, tagCompound);
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
    }

    public void activate(EntityPlayer player, ItemStack item, int x, int y, int z) {
        if (item.getCount() >= 1 && item.getItem() == PixelmonItems.hoopaRing && !player.world.isRemote) {
            if (this.ringCount >= 6) {
                return;
            }
            ++this.ringCount;
            item.shrink(1);
            this.sendUpdates();
            if (this.ringCount == 6) {
                PokemonSpec spec = new PokemonSpec("Hoopa");
                spec.level = 70;
                spec.ability = "Magician";
                spec.form = 0;
                this.world.spawnEntity(SpawningEntity.builder().pixelmon(spec).location(x, y + 2, z).maxTick(60).colors(SpawnColors.GOLD, SpawnColors.PINK, SpawnColors.GRAY).build(this.world, (EntityPlayerMP)player));
                ChatHandler.sendChat(player, "Hoopa appeared!", new Object[0]);
            }
        }
    }

    private void sendUpdates() {
        this.world.markBlockRangeForRenderUpdate(this.pos, this.pos);
        this.world.notifyBlockUpdate(this.pos, this.getState(), this.getState(), 3);
        this.world.scheduleBlockUpdate(this.pos, this.getBlockType(), 0, 0);
        this.markDirty();
    }

    private IBlockState getState() {
        return this.world.getBlockState(this.pos);
    }

    @Override
    public boolean shouldRenderInPass(int pass) {
        this.renderPass = pass;
        return true;
    }

    public int getRingCount() {
        return this.ringCount;
    }

    public void setRingCount(int ringCount) {
        this.ringCount = ringCount;
    }
}

