/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.api.events.BreedEvent;
import com.pixelmongenerations.common.block.tileEntities.TileEntityRanchBase;
import com.pixelmongenerations.common.entity.pixelmon.Entity10CanBreed;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.network.packetHandlers.ranch.EnumRanchClientPacketMode;
import com.pixelmongenerations.core.network.packetHandlers.ranch.RanchBlockClientPacket;
import com.pixelmongenerations.core.storage.PCServer;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.storage.deepstorage.DeepStorageManager;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;

public class TileEntityRanchBlock
extends TileEntityRanchBase {
    private UUID ownerUUID = null;
    private String playerName = "";
    private NBTTagCompound egg = null;

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.egg != null) {
            nbt.setTag("egg", this.egg);
        }
        if (this.ownerUUID != null) {
            nbt.setLong("UUIDMost", this.ownerUUID.getMostSignificantBits());
            nbt.setLong("UUIDLeast", this.ownerUUID.getLeastSignificantBits());
        }
        if (!this.playerName.equals("")) {
            nbt.setString("playerName", this.playerName);
        }
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        NBTTagCompound nBTTagCompound = this.egg = nbt.hasKey("egg") ? (NBTTagCompound)nbt.getTag("egg") : null;
        if (nbt.hasKey("playerName")) {
            this.playerName = nbt.getString("playerName");
        }
        this.ownerUUID = nbt.hasKey("UUIDMost", 4) && nbt.hasKey("UUIDLeast", 4) ? new UUID(nbt.getLong("UUIDMost"), nbt.getLong("UUIDLeast")) : null;
    }

    public boolean hasEgg() {
        return this.egg != null;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onActivate(EntityPlayer player) {
        if (!enabled) {
            ChatHandler.sendChat(player, "pixelmon.general.disabledblock", new Object[0]);
            return;
        }
        this.updateStatus();
        PCServer.sendContentsToPlayer((EntityPlayerMP)player);
        RanchBlockClientPacket vrb = new RanchBlockClientPacket(this, EnumRanchClientPacketMode.ViewBlock);
        Pixelmon.NETWORK.sendTo(vrb, (EntityPlayerMP)player);
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
        this.updateStatus();
    }

    @Override
    public void updateStatus() {
        super.updateStatus();
        for (EntityPixelmon p : this.entities) {
            if (this.hasBreedingPartner(p)) {
                EntityPixelmon q = this.getFirstBreedingPartner(p);
                if (q == null || this.egg != null || !p.readyToMakeEgg() || !q.readyToMakeEgg()) continue;
                EntityPixelmon pokemon = new EntityPixelmon(p.world);
                pokemon.makeEntityIntoEgg(p, q);
                if (!EnumSpecies.hasPokemon(pokemon.getName())) {
                    return;
                }
                BreedEvent.MakeEggEvent eggEvent = new BreedEvent.MakeEggEvent(p.getOwnerId(), this.ranchBounds.ranch, pokemon, p, q);
                if (!MinecraftForge.EVENT_BUS.post(eggEvent)) {
                    pokemon = eggEvent.getEgg();
                    this.egg = new NBTTagCompound();
                    if (!p.originalTrainerUUID.equals(q.originalTrainerUUID) && PixelmonConfig.shinyRate > 0.0f && pokemon.getRNG().nextFloat() < 2.0f / PixelmonConfig.shinyRate) {
                        pokemon.setShiny(true);
                    }
                    pokemon.writeToNBT(this.egg);
                }
                try {
                    p.resetBreedingLevel();
                }
                catch (ConcurrentModificationException er) {
                    Pixelmon.LOGGER.error("Encountered an error resetting breeding levels!");
                    Pixelmon.LOGGER.error("Ranch at " + this.pos + " / Pokemon: " + p);
                }
                try {
                    q.resetBreedingLevel();
                }
                catch (ConcurrentModificationException er) {
                    Pixelmon.LOGGER.error("Encountered an error resetting breeding levels!");
                    Pixelmon.LOGGER.error("Ranch at " + this.pos + " / Pokemon: " + q);
                }
                ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
                this.getWorld().notifyNeighborsOfStateChange(this.pos, this.getBlockType(), true);
                continue;
            }
            p.resetBreedingLevel();
        }
        this.getWorld().notifyNeighborsOfStateChange(this.pos, this.getBlockType(), true);
    }

    @Override
    public boolean hasBreedingPartner(EntityPixelmon pixelmon) {
        for (EntityPixelmon p : this.entities) {
            if (p == pixelmon || !Entity10CanBreed.canBreed(pixelmon, p)) continue;
            return true;
        }
        return false;
    }

    @Override
    public EntityPixelmon getFirstBreedingPartner(EntityPixelmon pixelmon) {
        ArrayList possiblePartners = (ArrayList)this.entities.stream().filter(p -> p != pixelmon).filter(p -> Entity10CanBreed.canBreed(pixelmon, p)).collect(Collectors.toList());
        if (possiblePartners.size() == 1) {
            return (EntityPixelmon)possiblePartners.get(0);
        }
        if (possiblePartners.size() > 1) {
            EntityPixelmon highestPartner = (EntityPixelmon)possiblePartners.get(0);
            for (int i = 1; i < possiblePartners.size(); ++i) {
                if (((EntityPixelmon)possiblePartners.get(i)).getNumBreedingLevels() <= highestPartner.getNumBreedingLevels()) continue;
                highestPartner = (EntityPixelmon)possiblePartners.get(i);
            }
            return highestPartner;
        }
        return null;
    }

    @Override
    public boolean isRanchOwnerInGame() {
        return this.ownerUUID != null && this.world.getPlayerEntityByUUID(this.ownerUUID) != null;
    }

    public void setOwner(EntityPlayerMP entity) {
        this.ownerUUID = entity.getUniqueID();
        this.playerName = entity.getDisplayNameString();
    }

    @Override
    public void claimEgg(EntityPlayerMP player) {
        Optional<PlayerStorage> optstorage;
        if (this.egg != null && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID(player.getServer(), this.ownerUUID)).isPresent()) {
            EntityPixelmon pokemon = (EntityPixelmon)PixelmonEntityList.createEntityFromNBT(this.egg, this.world);
            BreedEvent.CollectEggEvent collectEggEvent = new BreedEvent.CollectEggEvent(player.getUniqueID(), this.ranchBounds.ranch, pokemon);
            MinecraftForge.EVENT_BUS.post(collectEggEvent);
            optstorage.get().addToParty(collectEggEvent.getEgg());
            this.egg = null;
            this.updateStatus();
            ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
        }
    }

    public PixelmonData getPokemonEggData() {
        if (this.egg != null) {
            if (!EnumSpecies.hasPokemonAnyCase(this.egg.getString("Name"))) {
                DeepStorageManager.getOrCreateDeepStorage(this.ownerUUID).put(this.egg);
                DeepStorageManager.save(this.ownerUUID);
                this.egg = null;
                return null;
            }
            return new PixelmonData(this.egg);
        }
        return null;
    }

    public UUID getOwnerUUID() {
        return this.ownerUUID;
    }

    public String getPlayerName() {
        return this.playerName;
    }

    @Override
    protected void checkAboveGround() {
        if (this.aboveGround && this.percentAbove < 100) {
            this.percentAbove += 5;
        } else if (!this.aboveGround && this.percentAbove > 0) {
            this.percentAbove -= 5;
        }
        if (this.tick % 20 == 0) {
            int x = this.pos.getX();
            int y = this.pos.getY();
            int z = this.pos.getZ();
            List<EntityPlayer> closePlayers = this.world.getEntitiesWithinAABB(EntityPlayer.class, new AxisAlignedBB(x - 5, y - 5, z - 5, x + 5, y + 5, z + 5));
            this.aboveGround = false;
            for (EntityPlayer closePlayer : closePlayers) {
                if (!closePlayer.getUniqueID().equals(this.ownerUUID)) continue;
                this.aboveGround = true;
                break;
            }
        }
    }

    @Override
    public void onDestroy() {
        if (!this.world.isRemote && this.hasEgg()) {
            try {
                PlayerComputerStorage storage = PixelmonStorage.computerManager.getPlayerStorageFromUUID(this.world, this.ownerUUID);
                if (storage != null) {
                    storage.addToComputer((EntityPixelmon)PixelmonEntityList.createEntityFromNBT(this.egg, this.world));
                    if (storage.isOffline()) {
                        PixelmonStorage.computerManager.savePlayer(storage);
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onDestroy();
    }
}

