/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableMap
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.common.item.ItemShrineOrbWithInfo;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.forms.EnumKyurem;
import com.pixelmongenerations.core.util.SpawnColors;
import java.awt.Color;
import java.util.List;
import java.util.Map;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class TileEntityTaoTrioShrine
extends TileEntityShrine {

    Map<EnumSpecies, List<Color>> colors = ImmutableMap.of(
            EnumSpecies.Reshiram, Lists.newArrayList(SpawnColors.WHITE),
            EnumSpecies.Zekrom, Lists.newArrayList(SpawnColors.BLACK),
            EnumSpecies.Kyurem, Lists.newArrayList(SpawnColors.GRAY, SpawnColors.LIGHT_BLUE));
    private int orbIn = 0;

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        this.orbIn = compound.getInteger("OrbIn");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        NBTTagCompound tag = super.writeToNBT(compound);
        tag.setInteger("OrbIn", this.orbIn);
        return tag;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        NBTTagCompound tagCompound = new NBTTagCompound();
        this.writeToNBT(tagCompound);
        return new SPacketUpdateTileEntity(this.pos, 0, tagCompound);
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
    }

    @Override
    protected PokemonGroup.PokemonData selectActiveGroup() {
        switch (this.orbIn) {
            case 1: {
                return new PokemonGroup.PokemonData(EnumSpecies.Zekrom, EnumForms.NoForm);
            }
            case 2: {
                return new PokemonGroup.PokemonData(EnumSpecies.Reshiram, EnumKyurem.Normal);
            }
            case 3: {
                return new PokemonGroup.PokemonData(EnumSpecies.Kyurem, EnumForms.NoForm);
            }
        }
        return null;
    }

    @Override
    public void activate(EntityPlayer player, World world, BlockShrine block, IBlockState state, ItemStack item) {
        if (this.isSpawning()) {
            return;
        }
        ItemShrineOrbWithInfo orb = (ItemShrineOrbWithInfo)item.getItem();
        this.orbIn = orb.getGroup().contains(EnumSpecies.Zekrom) ? 1 : (orb.getGroup().contains(EnumSpecies.Reshiram) ? 2 : (orb.getGroup().contains(EnumSpecies.Kyurem) ? 3 : 0));
        world.notifyBlockUpdate(this.pos, state, state, 3);
        player.getHeldItem(EnumHand.MAIN_HAND).shrink(1);
        this.startSpawning(player, state, world, this.pos);
    }

    @Override
    protected void finish() {
        this.orbIn = 0;
    }

    @Override
    public int maxTick() {
        return 200;
    }

    @Override
    public String getRightClickMessage(ItemStack heldStack, IBlockState state) {
        return "pixelmon.tao_trio_shrine.right_click";
    }
}

