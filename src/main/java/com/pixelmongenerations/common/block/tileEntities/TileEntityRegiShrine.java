/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableMap
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.decorative.BlockUnown;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.spawnmethod.BlockRegiShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.event.ForgeListener;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.SpawnColors;
import java.awt.Color;
import java.util.List;
import java.util.Map;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

public class TileEntityRegiShrine
extends TileEntityShrine {
    Map<EnumSpecies, List<Color>> colors = ImmutableMap.of(
            EnumSpecies.Regice, Lists.newArrayList(SpawnColors.LIGHT_BLUE, SpawnColors.WHITE),
            EnumSpecies.Registeel, Lists.newArrayList(SpawnColors.GRAY, SpawnColors.BLACK),
            EnumSpecies.Regirock, Lists.newArrayList(SpawnColors.BROWN, SpawnColors.GRAY));

    @Override
    public void activate(EntityPlayer player, World world, BlockShrine block, IBlockState state, ItemStack item) {
        if (this.isSpawning()) {
            return;
        }
        List<BlockPos> blockPos = ForgeListener.searchForBlock(world, this.pos, 15, 1, (w, bpos) -> (w.getBlockState((BlockPos)bpos).getBlock() == PixelmonBlocks.fancyPillar || w.getBlockState((BlockPos)bpos).getBlock() == PixelmonBlocks.castlePillar) && w.getBlockState(bpos.up()).getBlock() instanceof BlockUnown);
        if (!blockPos.isEmpty() && block instanceof BlockRegiShrine) {
            List<BlockPos> list = ((BlockRegiShrine)block).checkForUnownSequence(world, blockPos.get(0));
            if (PixelmonConfig.limitRegigigas) {
                PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
                for (int i = 0; i < 6; ++i) {
                    String name;
                    if (storage.getNBT(storage.getIDFromPosition(i)) == null || !(name = storage.getNBT(storage.getIDFromPosition(i)).getString("Name")).equals("Regigigas")) continue;
                    NBTTagCompound compound = storage.getNBT(storage.getIDFromPosition(i));
                    String key = this.getKeyFromActiveSpecies(this.selectActiveGroup().getSpecies());
                    if (key == null) {
                        return;
                    }
                    if (compound.getBoolean(key)) {
                        player.sendMessage(new TextComponentString((Object)((Object)TextFormatting.RED) + "You have already summoned a " + this.selectActiveGroup().getSpecies().name + " using this Regigigas!"));
                        return;
                    }
                    compound.setBoolean(key, true);
                }
            }
            if (!list.isEmpty()) {
                list.forEach(a -> world.setBlockToAir(a.up()));
                this.startSpawning(player, state, world, this.pos);
                item.shrink(1);
            }
        }
    }

    public String getKeyFromActiveSpecies(EnumSpecies species) {
        switch (species) {
            case Regirock: {
                return "Regirock";
            }
            case Registeel: {
                return "Registeel";
            }
            case Regice: {
                return "Regice";
            }
            case Regieleki: {
                return "Regieleki";
            }
            case Regidrago: {
                return "Regidrago";
            }
        }
        return null;
    }

    @Override
    protected PokemonGroup.PokemonData selectActiveGroup() {
        Block block = this.world.getBlockState(this.pos).getBlock();
        if (block instanceof BlockRegiShrine) {
            return PokemonGroup.PokemonData.of(((BlockRegiShrine)block).getSpecies());
        }
        return null;
    }

    @Override
    public int maxTick() {
        return 100;
    }

    @Override
    public String getRightClickMessage(ItemStack heldStack, IBlockState state) {
        if (state.getBlock() == PixelmonBlocks.regiceShrine) {
            return "pixelmon.regice_shrine.right_click";
        }
        if (state.getBlock() == PixelmonBlocks.registeelShrine) {
            return "pixelmon.registeel_shrine.right_click";
        }
        if (state.getBlock() == PixelmonBlocks.regirockShrine) {
            return "pixelmon.regirock_shrine.right_click";
        }
        if (state.getBlock() == PixelmonBlocks.regidragoShrine) {
            return "pixelmon.regidrago_shrine.right_click";
        }
        if (state.getBlock() == PixelmonBlocks.regidragoShrine) {
            return "pixelmon.regieleki_shrine.right_click";
        }
        return "";
    }
}

