/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.client.models.animations.AnimateTask;
import com.pixelmongenerations.common.block.decorative.EndTableBlock;
import com.pixelmongenerations.common.block.tileEntities.IFrameCounter;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import com.pixelmongenerations.common.block.tileEntities.TileEntityInventory;
import java.util.EnumMap;
import java.util.TimerTask;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.util.ResourceLocation;

public class TileEntityEndTable
extends TileEntityInventory
implements IFrameCounter,
ISpecialTexture {
    private static EnumMap<BlockPlanks.EnumType, ResourceLocation> textures = new EnumMap(BlockPlanks.EnumType.class);
    public int frame = 0;
    AnimateTask animateTask;

    public void closeDrawer() {
        if (this.frame != 15) {
            return;
        }
        if (this.animateTask != null) {
            this.animateTask.cancel();
        }
        this.animateTask = new AnimateTask(this, 15, 30);
        AnimateTask.timer.scheduleAtFixedRate((TimerTask)this.animateTask, 100L, 30L);
        if (!this.world.isRemote) {
            this.sendAnimation("close");
            this.markDirty();
        }
    }

    public void openDrawer() {
        if (this.frame != 0 && this.frame != 30) {
            return;
        }
        if (this.animateTask != null) {
            this.animateTask.cancel();
        }
        this.animateTask = new AnimateTask(this, 0, 15);
        AnimateTask.timer.scheduleAtFixedRate((TimerTask)this.animateTask, 100L, 30L);
        if (!this.world.isRemote) {
            this.sendAnimation("open");
            this.markDirty();
        }
    }

    public boolean isOpen() {
        return this.frame == 15;
    }

    @Override
    public int getFrame() {
        return this.frame;
    }

    @Override
    public void setFrame(int frame) {
        this.frame = frame;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.setFrame(nbt.getInteger("frame"));
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.frame > 29 || this.frame < 1) {
            nbt.setInteger("frame", 0);
        } else {
            nbt.setInteger("frame", 15);
        }
        return nbt;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    public void sendAnimation(String str) {
        NBTTagCompound nbt = new NBTTagCompound();
        if (this.frame > 29 || this.frame < 1) {
            nbt.setInteger("frame", 0);
        } else {
            nbt.setInteger("frame", 15);
        }
        nbt.setString("Animation", str);
        SPacketUpdateTileEntity packet = new SPacketUpdateTileEntity(this.pos, 0, nbt);
        this.world.getMinecraftServer().getPlayerList().sendToAllNearExcept(null, this.pos.getX(), this.pos.getY(), this.pos.getZ(), 20.0, this.world.provider.getDimension(), packet);
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        NBTTagCompound nbt = pkt.getNbtCompound();
        this.setFrame(nbt.getInteger("frame"));
        if (nbt.hasKey("Animation", 8)) {
            if (nbt.getString("Animation").equals("open")) {
                this.openDrawer();
            } else {
                this.closeDrawer();
            }
        }
    }

    @Override
    public int getSizeInventory() {
        return 18;
    }

    @Override
    String getInventoryName() {
        return "EndTable.name";
    }

    @Override
    public boolean isUsableByPlayer(EntityPlayer player) {
        return this.world.getTileEntity(this.pos) == this && player.getDistanceSq((double)this.pos.getX() + 0.5, (double)this.pos.getY() + 0.5, (double)this.pos.getZ() + 0.5) <= 64.0;
    }

    @Override
    public ResourceLocation getTexture() {
        BlockPlanks.EnumType color = BlockPlanks.EnumType.OAK;
        IBlockState state = this.getWorld().getBlockState(this.getPos());
        if (state.getBlock() instanceof EndTableBlock) {
            color = ((EndTableBlock)state.getBlock()).getWood();
        }
        return textures.get(color);
    }

    static {
        for (BlockPlanks.EnumType plank : BlockPlanks.EnumType.values()) {
            textures.put(plank, new ResourceLocation("pixelmon:textures/blocks/end_table/end_table_" + plank.getName() + ".png"));
        }
    }
}

