/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import java.lang.invoke.LambdaMetafactory;
import java.util.function.Function;
import java.util.stream.Collectors;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;

public class AugmentedItemStackHandler
extends ItemStackHandler {
    public AugmentedItemStackHandler(int size) {
        super(size);
    }

    public boolean isEmpty() {
        return ((ItemStack)this.stacks.get(0)).isEmpty();
    }

    public boolean fillIfEmpty(int target) {
        if (this.getStackInSlot(target).isEmpty()) {
            for (int slot = 0; slot < this.getSlots(); ++slot) {
                ItemStack itemStack;
                if (slot == target || (itemStack = this.getStackInSlot(slot)).isEmpty()) continue;
                this.setStackInSlot(target, itemStack);
                this.setStackInSlot(slot, ItemStack.EMPTY);
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return this.stacks.stream().map(ItemStack::getCount).map(String::valueOf).collect(Collectors.joining(",", "(", ")"));
    }
}

