/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.dialogue.Choice;
import com.pixelmongenerations.api.dialogue.Dialogue;
import com.pixelmongenerations.api.events.player.PlayerShinyChanceEvent;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemZygardeCube;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class TileEntityZygardeMachine
extends TileEntity {
    public int renderPass;

    public void activate(EntityPlayer player, EnumHand hand, ItemStack item) {
        Optional<PlayerStorage> oStorage;
        if (player instanceof EntityPlayerMP && (oStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player)).isPresent()) {
            PlayerStorage storage = oStorage.get();
            if (item != null && item.getItem() instanceof ItemZygardeCube && !this.world.isRemote) {
                Dialogue.DialogueBuilder dialogue = Dialogue.builder();
                dialogue.column(2);
                dialogue.setName(I18n.translateToLocal("pixelmon.gui.zygarde_machine"));
                dialogue.setText(I18n.translateToLocal("pixelmon.gui.zygarde_machine.description"));
                dialogue.addChoice(new Choice("Create 10%", event -> {
                    EntityPlayerMP choicePlayer = event.getPlayer();
                    FMLCommonHandler.instance().getMinecraftServerInstance().addScheduledTask(() -> {
                        Optional<PlayerStorage> choiceOStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
                        if (choiceOStorage.isPresent()) {
                            TileEntityZygardeMachine.createZygardeTen(choicePlayer, choicePlayer.getHeldItem(hand), choiceOStorage.get(), choicePlayer.getPosition().getX(), choicePlayer.getPosition().getY(), choicePlayer.getPosition().getZ());
                        }
                    });
                }));
                dialogue.addChoice(new Choice("Create 50%", event -> {
                    EntityPlayerMP choicePlayer = event.getPlayer();
                    FMLCommonHandler.instance().getMinecraftServerInstance().addScheduledTask(() -> {
                        Optional<PlayerStorage> choiceOStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
                        if (choiceOStorage.isPresent()) {
                            TileEntityZygardeMachine.createZygardeFifty(choicePlayer, choicePlayer.getHeldItem(hand), choiceOStorage.get(), choicePlayer.getPosition().getX(), choicePlayer.getPosition().getY(), choicePlayer.getPosition().getZ());
                        }
                    });
                }));
                dialogue.addChoice(new Choice(I18n.translateToLocal("pixelmon.gui.zygarde_100"), event -> {
                    EntityPlayerMP choicePlayer = event.getPlayer();
                    FMLCommonHandler.instance().getMinecraftServerInstance().addScheduledTask(() -> {
                        Optional<PlayerStorage> choiceOStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
                        if (choiceOStorage.isPresent()) {
                            TileEntityZygardeMachine.createZygardeOneHundred(choicePlayer, choicePlayer.getHeldItem(hand), choiceOStorage.get(), choicePlayer.getPosition().getX(), choicePlayer.getPosition().getY(), choicePlayer.getPosition().getZ());
                        }
                    });
                }));
                dialogue.addChoice(new Choice(I18n.translateToLocal("pixelmon.gui.zygarde_combine10with40"), event -> {
                    EntityPlayerMP choicePlayer = event.getPlayer();
                    FMLCommonHandler.instance().getMinecraftServerInstance().addScheduledTask(() -> {
                        Optional<PlayerStorage> choiceOStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
                        if (choiceOStorage.isPresent()) {
                            TileEntityZygardeMachine.combineTen(choicePlayer, choicePlayer.getHeldItem(hand), choiceOStorage.get(), choicePlayer.getPosition().getX(), choicePlayer.getPosition().getY(), choicePlayer.getPosition().getZ());
                        }
                    });
                }));
                dialogue.addChoice(new Choice(I18n.translateToLocal("pixelmon.gui.zygarde_combine90"), event -> {
                    EntityPlayerMP choicePlayer = event.getPlayer();
                    FMLCommonHandler.instance().getMinecraftServerInstance().addScheduledTask(() -> {
                        Optional<PlayerStorage> choiceOStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
                        if (choiceOStorage.isPresent()) {
                            TileEntityZygardeMachine.combineTenWithNinety(choicePlayer, choicePlayer.getHeldItem(hand), choiceOStorage.get(), choicePlayer.getPosition().getX(), choicePlayer.getPosition().getY(), choicePlayer.getPosition().getZ());
                        }
                    });
                }));
                dialogue.addChoice(new Choice(I18n.translateToLocal("pixelmon.gui.zygarde_combine50"), event -> {
                    EntityPlayerMP choicePlayer = event.getPlayer();
                    FMLCommonHandler.instance().getMinecraftServerInstance().addScheduledTask(() -> {
                        Optional<PlayerStorage> choiceOStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
                        if (choiceOStorage.isPresent()) {
                            TileEntityZygardeMachine.combineFiftyWith50(choicePlayer, choicePlayer.getHeldItem(hand), choiceOStorage.get(), choicePlayer.getPosition().getX(), choicePlayer.getPosition().getY(), choicePlayer.getPosition().getZ());
                        }
                    });
                }));
                Dialogue.setPlayerDialogueData(storage.getPlayer(), Lists.newArrayList(dialogue.build()), true);
            }
        }
    }

    public static void createZygardeTen(EntityPlayer player, ItemStack item, PlayerStorage storage, int x, int y, int z) {
        if (item.getItemDamage() >= 10 && !player.world.isRemote) {
            EntityPixelmon pixelmonEntity = null;
            player.sendMessage(new TextComponentString(I18n.translateToLocal("pixelmon.zygarde_machine.interact.merge10")));
            pixelmonEntity = (EntityPixelmon)PixelmonEntityList.createEntityByName(EnumSpecies.Zygarde.name, player.world);
            pixelmonEntity.setForm(1, true);
            pixelmonEntity.setAbilitySlot(0);
            pixelmonEntity.setAbility(pixelmonEntity.baseStats.abilities[0]);
            pixelmonEntity.update(EnumUpdateType.Ability);
            pixelmonEntity.level.setLevel(70);
            float chance = PixelmonConfig.shinyRate;
            if (PixelmonConfig.enableCatchCombos) {
                if (PixelmonConfig.enableCatchComboShinyLock && PixelmonMethods.isCatchComboSpecies((EntityPlayerMP)player, EnumSpecies.getFromNameAnyCase(pixelmonEntity.getName()))) {
                    chance = PixelmonMethods.getCatchComboChance((EntityPlayerMP)player);
                } else if (!PixelmonConfig.enableCatchComboShinyLock) {
                    chance = PixelmonMethods.getCatchComboChance((EntityPlayerMP)player);
                }
            } else if (PixelmonMethods.isWearingShinyCharm((EntityPlayerMP)player)) {
                chance = PixelmonConfig.shinyCharmRate;
            }
            PlayerShinyChanceEvent event = new PlayerShinyChanceEvent((EntityPlayerMP)player, chance);
            MinecraftForge.EVENT_BUS.post(event);
            chance = event.isCanceled() ? 0.0f : event.getChance();
            pixelmonEntity.setShiny(chance > 0.0f && RandomHelper.getRandomChance(1.0f / chance));
            item.setItemDamage(item.getItemDamage() - 10);
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
            if (optstorage.isPresent()) {
                storage.addToParty(pixelmonEntity);
            }
        }
    }

    public static void createZygardeFifty(EntityPlayer player, ItemStack item, PlayerStorage storage, int x, int y, int z) {
        if (item.getItemDamage() >= 50 && !player.world.isRemote) {
            EntityPixelmon pixelmonEntity = null;
            player.sendMessage(new TextComponentString(I18n.translateToLocal("pixelmon.zygarde_machine.interact.merge50")));
            pixelmonEntity = (EntityPixelmon)PixelmonEntityList.createEntityByName(EnumSpecies.Zygarde.name, player.world);
            pixelmonEntity.setForm(0, true);
            pixelmonEntity.setAbilitySlot(0);
            pixelmonEntity.setAbility(pixelmonEntity.baseStats.abilities[0]);
            pixelmonEntity.update(EnumUpdateType.Ability);
            pixelmonEntity.level.setLevel(70);
            float chance = PixelmonConfig.shinyRate;
            if (PixelmonConfig.enableCatchCombos) {
                if (PixelmonConfig.enableCatchComboShinyLock && PixelmonMethods.isCatchComboSpecies((EntityPlayerMP)player, EnumSpecies.getFromNameAnyCase(pixelmonEntity.getName()))) {
                    chance = PixelmonMethods.getCatchComboChance((EntityPlayerMP)player);
                } else if (!PixelmonConfig.enableCatchComboShinyLock) {
                    chance = PixelmonMethods.getCatchComboChance((EntityPlayerMP)player);
                }
            } else if (PixelmonMethods.isWearingShinyCharm((EntityPlayerMP)player)) {
                chance = PixelmonConfig.shinyCharmRate;
            }
            PlayerShinyChanceEvent event = new PlayerShinyChanceEvent((EntityPlayerMP)player, chance);
            MinecraftForge.EVENT_BUS.post(event);
            chance = event.isCanceled() ? 0.0f : event.getChance();
            pixelmonEntity.setShiny(chance > 0.0f && RandomHelper.getRandomChance(1.0f / chance));
            item.setItemDamage(item.getItemDamage() - 50);
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
            if (optstorage.isPresent()) {
                storage.addToParty(pixelmonEntity);
            }
        }
    }

    public static void createZygardeOneHundred(EntityPlayer player, ItemStack item, PlayerStorage storage, int x, int y, int z) {
        if (item.getItemDamage() == 100 && !player.world.isRemote) {
            EntityPixelmon pixelmonEntity = null;
            player.sendMessage(new TextComponentString(I18n.translateToLocal("pixelmon.zygarde_machine.interact.merge100")));
            pixelmonEntity = (EntityPixelmon)PixelmonEntityList.createEntityByName(EnumSpecies.Zygarde.name, player.world);
            pixelmonEntity.setForm(0, true);
            pixelmonEntity.setAbilitySlot(1);
            pixelmonEntity.setAbility(pixelmonEntity.baseStats.abilities[1]);
            pixelmonEntity.update(EnumUpdateType.Ability);
            pixelmonEntity.level.setLevel(70);
            float chance = PixelmonConfig.shinyRate;
            if (PixelmonConfig.enableCatchCombos) {
                if (PixelmonConfig.enableCatchComboShinyLock && PixelmonMethods.isCatchComboSpecies((EntityPlayerMP)player, EnumSpecies.getFromNameAnyCase(pixelmonEntity.getName()))) {
                    chance = PixelmonMethods.getCatchComboChance((EntityPlayerMP)player);
                } else if (!PixelmonConfig.enableCatchComboShinyLock) {
                    chance = PixelmonMethods.getCatchComboChance((EntityPlayerMP)player);
                }
            } else if (PixelmonMethods.isWearingShinyCharm((EntityPlayerMP)player)) {
                chance = PixelmonConfig.shinyCharmRate;
            }
            PlayerShinyChanceEvent event = new PlayerShinyChanceEvent((EntityPlayerMP)player, chance);
            MinecraftForge.EVENT_BUS.post(event);
            chance = event.isCanceled() ? 0.0f : event.getChance();
            pixelmonEntity.setShiny(chance > 0.0f && RandomHelper.getRandomChance(1.0f / chance));
            item.setItemDamage(item.getItemDamage() - 100);
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
            if (optstorage.isPresent()) {
                storage.addToParty(pixelmonEntity);
            }
        }
    }

    public static void combineTen(EntityPlayer player, ItemStack item, PlayerStorage storage, int x, int y, int z) {
        Optional<PlayerStorage> oStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
        if (!oStorage.isPresent()) {
            return;
        }
        NBTTagCompound zygarde = null;
        for (NBTTagCompound nbt : storage.getList()) {
            if (nbt == null || !nbt.getString("Name").equals("Zygarde")) continue;
            int form = nbt.getInteger("Variant");
            if (form == 0) {
                zygarde = nbt;
                continue;
            }
            if (form != 1) continue;
            zygarde = nbt;
            break;
        }
        if (zygarde != null && item != null && item.getItem() instanceof ItemZygardeCube && item.getItemDamage() >= 40 && !player.world.isRemote) {
            for (int i = 0; i < storage.getList().length; ++i) {
                NBTTagCompound nbt = storage.getList()[i];
                if (!nbt.getString("Name").equals("Zygarde") || nbt.getInteger("Variant") != 1) continue;
                nbt.setInteger("Variant", 0);
                EntityPixelmon pixelmonEntity = storage.sendOutFromPosition(i, ((EntityPlayerMP)player).world);
                pixelmonEntity.setForm(0);
                storage.updateNBT(pixelmonEntity);
                item.setItemDamage(item.getItemDamage() - 40);
                player.sendMessage(new TextComponentString(I18n.translateToLocal("pixelmon.zygarde_machine.interact.merge10with40")));
                break;
            }
        }
    }

    public static void combineTenWithNinety(EntityPlayer player, ItemStack item, PlayerStorage storage, int x, int y, int z) {
        Optional<PlayerStorage> oStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
        if (!oStorage.isPresent()) {
            return;
        }
        NBTTagCompound zygarde = null;
        for (NBTTagCompound nbt : storage.getList()) {
            if (nbt == null || !nbt.getString("Name").equals("Zygarde")) continue;
            int form = nbt.getInteger("Variant");
            if (form == 0) {
                zygarde = nbt;
                continue;
            }
            if (form != 1) continue;
            zygarde = nbt;
            break;
        }
        if (zygarde != null && item != null && item.getItem() instanceof ItemZygardeCube && item.getItemDamage() >= 90 && !player.world.isRemote) {
            for (int i = 0; i < storage.getList().length; ++i) {
                NBTTagCompound nbt = storage.getList()[i];
                if (!nbt.getString("Name").equals("Zygarde") || nbt.getInteger("Variant") != 1) continue;
                nbt.setInteger("isTenPercent", 1);
                EntityPixelmon pixelmonEntity = storage.sendOutFromPosition(i, ((EntityPlayerMP)player).world);
                pixelmonEntity.setAbilitySlot(1);
                pixelmonEntity.setAbility(pixelmonEntity.baseStats.abilities[1]);
                pixelmonEntity.update(EnumUpdateType.Ability);
                storage.updateNBT(pixelmonEntity);
                item.setItemDamage(item.getItemDamage() - 90);
                player.sendMessage(new TextComponentString(I18n.translateToLocal("pixelmon.zygarde_machine.interact.merge10with90")));
                break;
            }
        }
    }

    public static void combineFiftyWith50(EntityPlayer player, ItemStack item, PlayerStorage storage, int x, int y, int z) {
        Optional<PlayerStorage> oStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
        if (!oStorage.isPresent()) {
            return;
        }
        NBTTagCompound zygarde = null;
        for (NBTTagCompound nbt : storage.getList()) {
            if (nbt == null || !nbt.getString("Name").equals("Zygarde")) continue;
            int form = nbt.getInteger("Variant");
            if (form == 0) {
                zygarde = nbt;
                continue;
            }
            if (form != 1) continue;
            zygarde = nbt;
            break;
        }
        if (zygarde != null && item != null && item.getItem() instanceof ItemZygardeCube && item.getItemDamage() >= 50 && !player.world.isRemote) {
            for (int i = 0; i < storage.getList().length; ++i) {
                NBTTagCompound nbt = storage.getList()[i];
                if (!nbt.getString("Name").equals("Zygarde") || nbt.getInteger("Variant") != 0) continue;
                EntityPixelmon pixelmonEntity = storage.sendOutFromPosition(i, ((EntityPlayerMP)player).world);
                pixelmonEntity.setAbilitySlot(1);
                pixelmonEntity.setAbility(pixelmonEntity.baseStats.abilities[1]);
                pixelmonEntity.update(EnumUpdateType.Ability);
                storage.updateNBT(pixelmonEntity);
                item.setItemDamage(item.getItemDamage() - 50);
                player.sendMessage(new TextComponentString(I18n.translateToLocal("pixelmon.zygarde_machine.interact.merge10with40")));
                break;
            }
        }
    }

    @Override
    public boolean shouldRenderInPass(int pass) {
        this.renderPass = pass;
        return true;
    }
}

