/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  javax.vecmath.Vector3d
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.conditions.LocationType;
import com.pixelmongenerations.api.world.MutableLocation;
import com.pixelmongenerations.common.block.enums.EnumPokeChestType;
import com.pixelmongenerations.common.block.enums.EnumPokechestVisibility;
import com.pixelmongenerations.common.block.spawning.TileEntityPixelmonSpawner;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.spawning.PixelmonSpawning;
import com.pixelmongenerations.common.spawning.PlayerTrackingSpawner;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.util.LootClaim;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import javax.vecmath.Vector3d;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

public class TileEntityPokeChest
extends TileEntity
implements ITickable {
    private EnumPokechestVisibility visibility = EnumPokechestVisibility.Visible;
    private UUID ownerID = null;
    private boolean chestOneTime = true;
    private boolean dropOneTime = true;
    private boolean doCustomDrop = false;
    private boolean timeEnabled = false;
    private ArrayList<ItemStack> customDrops = null;
    private int frontFace = 4;
    private boolean grotto = false;
    private boolean isEvent = false;
    private ArrayList<LootClaim> claimed = new ArrayList();
    public int lureTimer;
    public int maxSpawns;
    public int currentSpawns;
    public boolean lureActivated;
    public int frame;

    public void setOwner(UUID id) {
        this.ownerID = id;
    }

    public UUID getOwner() {
        return this.ownerID;
    }

    public EnumPokeChestType getType() {
        if (this.isEvent) {
            return EnumPokeChestType.SPECIAL;
        }
        if (this.blockType == PixelmonBlocks.pokeChest) {
            return EnumPokeChestType.POKEBALL;
        }
        if (this.blockType == PixelmonBlocks.ultraChest) {
            return EnumPokeChestType.ULTRABALL;
        }
        if (this.blockType == PixelmonBlocks.masterChest) {
            return EnumPokeChestType.MASTERBALL;
        }
        if (this.blockType == PixelmonBlocks.beastChest) {
            return EnumPokeChestType.BEASTBALL;
        }
        if (this.blockType == PixelmonBlocks.pokeStop) {
            return EnumPokeChestType.POKESTOP;
        }
        return EnumPokeChestType.POKEBALL;
    }

    public void setGrotto() {
        this.grotto = true;
    }

    public boolean isGrotto() {
        return this.grotto;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound tagger) {
        super.writeToNBT(tagger);
        tagger.setLong("ownerIDMost", this.ownerID == null ? -1L : this.ownerID.getMostSignificantBits());
        tagger.setLong("ownerIDLeast", this.ownerID == null ? -1L : this.ownerID.getLeastSignificantBits());
        tagger.setBoolean("chestOneTime", this.chestOneTime);
        tagger.setBoolean("dropOneTime", this.dropOneTime);
        tagger.setInteger("maxSpawns", this.maxSpawns);
        tagger.setInteger("lureTimer", this.lureTimer);
        if (!this.claimed.isEmpty()) {
            NBTTagCompound claimedTag = new NBTTagCompound();
            for (int i = 0; i < this.claimed.size(); ++i) {
                NBTTagCompound playerInfoTag = new NBTTagCompound();
                LootClaim playerClaim = this.claimed.get(i);
                playerInfoTag.setLong("most", playerClaim.getPlayerID().getMostSignificantBits());
                playerInfoTag.setLong("least", playerClaim.getPlayerID().getLeastSignificantBits());
                playerInfoTag.setLong("timeClaimed", playerClaim.getTimeClaimed());
                claimedTag.setTag("player" + i, playerInfoTag);
            }
            tagger.setTag("claimedPlayers", claimedTag);
        }
        if (this.customDrops != null) {
            NBTTagList list = new NBTTagList();
            for (ItemStack customDrop : this.customDrops) {
                NBTTagCompound stackCompound = new NBTTagCompound();
                if (customDrop == null) continue;
                list.appendTag(customDrop.writeToNBT(stackCompound));
            }
            tagger.setTag("customDrops", list);
        }
        tagger.setBoolean("customDrop", this.doCustomDrop);
        tagger.setBoolean("timeEnabled", this.timeEnabled);
        tagger.setBoolean("grotto", this.grotto);
        if (this.isEvent) {
            tagger.setBoolean("specialEvent", this.isEvent);
        }
        tagger.setShort("visibility", (short)this.visibility.ordinal());
        tagger.setBoolean("lureActivated", this.lureActivated);
        return tagger;
    }

    public void writeToNBTClient(NBTTagCompound tagger) {
        if (this.isEvent) {
            tagger.setBoolean("specialEvent", this.isEvent);
        }
        tagger.setShort("visibility", (short)this.visibility.ordinal());
        tagger.setBoolean("lureActivated", this.lureActivated);
        super.writeToNBT(tagger);
    }

    public void readFromNBTClient(NBTTagCompound tagger) {
        this.isEvent = tagger.hasKey("specialEvent");
        this.visibility = EnumPokechestVisibility.values()[tagger.getShort("visibility")];
        this.lureActivated = tagger.getBoolean("lureActivated");
        super.readFromNBT(tagger);
    }

    @Override
    public void readFromNBT(NBTTagCompound tagger) {
        int i;
        if (tagger.getLong("ownerIDMost") != -1L) {
            this.ownerID = new UUID(tagger.getLong("ownerIDMost"), tagger.getLong("ownerIDLeast"));
        }
        this.chestOneTime = tagger.getBoolean("chestOneTime") && !tagger.hasKey("specialEvent");
        this.dropOneTime = tagger.getBoolean("dropOneTime") || tagger.hasKey("specialEvent");
        boolean bl = this.dropOneTime;
        if (tagger.hasKey("claimedPlayers")) {
            NBTTagCompound claimedTag = (NBTTagCompound)tagger.getTag("claimedPlayers");
            i = 0;
            while (claimedTag.hasKey("player" + i)) {
                NBTTagCompound playerTag = (NBTTagCompound)claimedTag.getTag("player" + i);
                this.claimed.add(new LootClaim(new UUID(playerTag.getLong("most"), playerTag.getLong("least")), playerTag.getLong("timeClaimed")));
                ++i;
            }
        }
        this.doCustomDrop = tagger.getBoolean("customDrop");
        this.customDrops = new ArrayList<>();
        if (tagger.hasKey("customDropID")) {
            this.customDrops = Lists.newArrayList(new ItemStack(Item.getItemById(tagger.getInteger("customDropID"))));
        } else {
            NBTTagList dropList = tagger.getTagList("customDrops", 10);
            this.customDrops = new ArrayList<>();
            for (i = 0; i < dropList.tagCount(); ++i) {
                this.customDrops.add(new ItemStack(dropList.getCompoundTagAt(i)));
            }
        }
        this.timeEnabled = tagger.getBoolean("timeEnabled") && !tagger.hasKey("specialEvent");
        this.grotto = tagger.getBoolean("grotto");
        this.isEvent = tagger.hasKey("specialEvent");
        this.visibility = EnumPokechestVisibility.values()[tagger.getShort("visibility")];
        this.maxSpawns = tagger.getInteger("maxSpawns");
        this.lureTimer = tagger.getInteger("lureTimer");
        this.lureActivated = tagger.getBoolean("lureActivated");
        super.readFromNBT(tagger);
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = super.getUpdateTag();
        this.writeToNBTClient(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBTClient(pkt.getNbtCompound());
    }

    public boolean canClaim(UUID playerID) {
        if (!this.dropOneTime) {
            return true;
        }
        LootClaim claim = this.getLootClaim(playerID);
        if (claim != null) {
            if (this.getType() != EnumPokeChestType.POKESTOP && this.timeEnabled && (System.currentTimeMillis() - claim.getTimeClaimed()) / 1000L > (long)(PixelmonConfig.lootTime * 3600)) {
                return true;
            }
            return this.getType() == EnumPokeChestType.POKESTOP && this.timeEnabled && (System.currentTimeMillis() - claim.getTimeClaimed()) / 1000L > (long)(PixelmonConfig.pokeStopLootTime * 3600);
        }
        return true;
    }

    public LootClaim getLootClaim(UUID playerID) {
        for (LootClaim claim : this.claimed) {
            if (!claim.getPlayerID().toString().equals(playerID.toString())) continue;
            return claim;
        }
        return null;
    }

    public void addClaimer(UUID playerID) {
        if (this.dropOneTime || this.timeEnabled) {
            this.claimed.add(new LootClaim(playerID, System.currentTimeMillis()));
        }
    }

    public void removeClaimer(UUID playerID) {
        this.claimed.remove(this.getLootClaim(playerID));
    }

    public boolean shouldBreakBlock() {
        return this.chestOneTime && !this.timeEnabled;
    }

    public void setChestOneTime(boolean val) {
        this.chestOneTime = val;
    }

    public boolean getChestMode() {
        return this.chestOneTime;
    }

    public void setDropOneTime(boolean val) {
        this.dropOneTime = val;
    }

    public boolean getDropMode() {
        return this.dropOneTime;
    }

    public boolean isCustomDrop() {
        return this.doCustomDrop && this.customDrops != null;
    }

    public boolean isTimeEnabled() {
        return this.timeEnabled;
    }

    public void setTimeEnabled(boolean val) {
        this.timeEnabled = val;
    }

    public List<ItemStack> getCustomDrops() {
        if (this.isEvent) {
            for (ItemStack s : this.customDrops) {
                if (s.isStackable()) continue;
                if (!s.hasTagCompound()) {
                    NBTTagCompound nbt = new NBTTagCompound();
                    nbt.setBoolean("specialEvent", true);
                    s.setTagCompound(nbt);
                    continue;
                }
                s.getTagCompound().setBoolean("specialEvent", true);
            }
        }
        return this.customDrops;
    }

    public void setCustomDrops(ItemStack ... customDrops) {
        this.doCustomDrop = true;
        this.customDrops = Lists.newArrayList(customDrops);
    }

    public void addCustomDrop(ItemStack drop) {
        this.doCustomDrop = true;
        if (this.customDrops == null) {
            this.customDrops = new ArrayList();
        }
        this.customDrops.add(drop);
    }

    public void setCustomDropEnabled(boolean enabled) {
        this.doCustomDrop = enabled;
    }

    public boolean isUsableByPlayer(EntityPlayer player) {
        return this.world.getTileEntity(this.pos) == this && player.getDistanceSq((double)this.pos.getX() + 0.5, (double)this.pos.getY() + 0.5, (double)this.pos.getZ() + 0.5) < 64.0;
    }

    public void setFrontFace(int face) {
        this.frontFace = face;
    }

    public int getFrontFace() {
        return this.frontFace;
    }

    public void setSpecialEventDrop(ItemStack itemStack) {
        this.isEvent = true;
        this.chestOneTime = false;
        this.dropOneTime = true;
        this.setCustomDrops(itemStack);
    }

    public EnumPokechestVisibility getVisibility() {
        return this.visibility;
    }

    public void setVisibility(EnumPokechestVisibility visible) {
        this.visibility = visible;
        ((WorldServer)this.world).getPlayerChunkMap().markBlockForUpdate(this.pos);
    }

    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        if (this.getType() == EnumPokeChestType.POKESTOP) {
            return super.getRenderBoundingBox().expand(0.0, 9.0, 0.0);
        }
        return super.getRenderBoundingBox();
    }

    @Override
    public void update() {
        if (this.getBlockType() != PixelmonBlocks.pokeStop) {
            return;
        }
        if (!this.lureActivated()) {
            return;
        }
        if (this.lureTimer > 0 && this.currentSpawns < this.maxSpawns) {
            --this.lureTimer;
            if (this.lureTimer == 0) {
                int z;
                int y;
                ++this.currentSpawns;
                int spawnRadius = PixelmonConfig.lureRadius - 1;
                int x = this.getPos().getX() + this.world.rand.nextInt(spawnRadius * 2 + 1) - spawnRadius;
                if (!TileEntityPokeChest.attemptPokemonSpawn(this.world, x, y = this.getPos().getY(), z = this.getPos().getZ() + this.world.rand.nextInt(spawnRadius * 2 + 1) - spawnRadius)) {
                    y = this.world.getHeight(x, z);
                    Block block = this.world.getBlockState(new BlockPos(x, y - 1, z)).getBlock();
                    if (block.getRegistryName().toString().contains("leaves")) {
                        y -= 8;
                    }
                    if ((block = this.world.getBlockState(new BlockPos(x, y, z)).getBlock()) == Blocks.AIR) {
                        y -= 5;
                    }
                    TileEntityPokeChest.attemptPokemonSpawn(this.world, x, y, z);
                }
                if (this.currentSpawns < this.maxSpawns) {
                    this.lureTimer = PixelmonConfig.lureTimer;
                } else {
                    this.currentSpawns = 0;
                    this.maxSpawns = 0;
                    this.lureTimer = 0;
                    this.setActivated(false);
                }
            }
        }
    }

    public static boolean attemptPokemonSpawn(World world, int x, int y, int z) {
        Integer yLevel = TileEntityPixelmonSpawner.spawnerLand.getSpawnConditionY(world, new BlockPos(x, y, z));
        boolean valid = yLevel != null && TileEntityPixelmonSpawner.spawnerLand.canPokemonSpawnHere(world, new BlockPos(x, yLevel, z));
        if (valid) {
            SpawnAction<? extends Entity> spawnAction;
            Entity entity;
            EntityPlayer player = world.getClosestPlayer((double)x, (double)y, (double)z, 30.0, false);
            BlockPos spawnPos = new BlockPos(x, yLevel, z);
            if (player == null) {
                return true;
            }
            SpawnLocation spawnLocation = new SpawnLocation(new MutableLocation(world, spawnPos), LocationType.getPotentialTypes(world.getBlockState(player.getPosition().add(0, -1, 0))), world.getBlockState(spawnPos).getBlock(), null, world.getBiome(spawnPos), world.canSeeSky(spawnPos), 6);
            ArrayList<SpawnLocation> spawnLocations = new ArrayList<SpawnLocation>();
            spawnLocations.add(spawnLocation);
            PlayerTrackingSpawner playerTrackingSpawner = new PlayerTrackingSpawner(player.getUniqueID());
            for (AbstractSpawner spawner : PixelmonSpawning.coordinator.spawners) {
                if (!(spawner instanceof PlayerTrackingSpawner) || ((PlayerTrackingSpawner)spawner).playerUUID.compareTo(player.getUniqueID()) != 0) continue;
                playerTrackingSpawner = (PlayerTrackingSpawner)spawner;
            }
            ArrayList<SpawnAction<? extends Entity>> possibleSpawns = playerTrackingSpawner.calculateSpawnActions(spawnLocations);
            if (!possibleSpawns.isEmpty() && (entity = (spawnAction = possibleSpawns.get(0)).getOrCreateEntity()) instanceof EntityPixelmon) {
                EntityPixelmon pixelmon = (EntityPixelmon)entity;
                pixelmon.setPosition(spawnPos.getX(), spawnPos.getY(), spawnPos.getZ());
                Random random = new Random();
                if (random.nextInt(PixelmonConfig.lureShinyChance) == 1) {
                    pixelmon.setShiny(true);
                }
                world.spawnEntity(pixelmon);
                for (int i = 0; i < 20; ++i) {
                    Pixelmon.PROXY.spawnParticle(EnumParticleTypes.SPELL_WITCH.getParticleID(), pixelmon.posX, pixelmon.posY, pixelmon.posZ, 0.0, 1.0, 0.0, 1, 1, 1);
                }
            }
            return true;
        }
        return false;
    }

    public boolean lureActivated() {
        return this.currentSpawns < this.maxSpawns;
    }

    public void setMaxSpawns(int maxSpawns) {
        this.maxSpawns = maxSpawns;
    }

    public void setLureTimer(int lureTimer) {
        this.lureTimer = lureTimer;
    }

    public ArrayList<Vector3d> getCirclePositions(Vector3d center, double radius, int amount) {
        double increment = Math.PI * 2 / (double)amount;
        ArrayList<Vector3d> positions = new ArrayList<Vector3d>();
        for (int i = 0; i < amount; ++i) {
            double angle = (double)i * increment;
            double x = center.getX() + radius * Math.cos(angle);
            double z = center.getZ() + radius * Math.sin(angle);
            positions.add(new Vector3d(x, center.getY(), z));
        }
        return positions;
    }

    public void sendUpdates() {
        this.world.markBlockRangeForRenderUpdate(this.pos, this.pos);
        this.world.notifyBlockUpdate(this.pos, this.getState(), this.getState(), 3);
        this.world.scheduleBlockUpdate(this.pos, this.getBlockType(), 0, 0);
        this.markDirty();
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        this.lureActivated = tag.getBoolean("lureActivated");
    }

    private IBlockState getState() {
        return this.world.getBlockState(this.pos);
    }

    public boolean isActivated() {
        return this.lureActivated;
    }

    public void setActivated(boolean lureActivated) {
        this.lureActivated = lureActivated;
        this.sendUpdates();
    }
}

