/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.block.tileEntities.ICullable;
import net.minecraft.tileentity.TileEntity;

public class TileEntityScarecrow
extends TileEntity
implements ICullable {
    private boolean culled;

    @Override
    public void setCulled(boolean isCulled) {
        this.culled = isCulled;
    }

    @Override
    public boolean isCulled() {
        return this.culled;
    }
}

