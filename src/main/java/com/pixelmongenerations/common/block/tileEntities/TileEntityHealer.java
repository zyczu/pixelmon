/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.common.entity.npcs.NPCNurseJoy;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelSounds;
import com.pixelmongenerations.core.util.PixelmonMethods;
import java.util.Arrays;
import java.util.Optional;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;

public class TileEntityHealer
extends TileEntity
implements ITickable {
    private static final int ticksToPlace = 5;
    private static final int ticksToHeal = 100;
    public boolean beingUsed = false;
    public EnumPokeball[] pokeballType = new EnumPokeball[6];
    private PlayerStorage storage;
    public EntityPlayer player;
    private int pokemonLastPlaced = -1;
    private float healingRate = 1.0f;
    private int tickCount = 0;
    public float rotation = 0.0f;
    public int flashTimer = 0;
    public boolean allPlaced = false;
    public boolean playSound = false;
    public boolean stayDark = false;

    public void use(NPCNurseJoy npc, EntityPlayer player) {
        this.startHealing(player, 1.5f);
    }

    public void use(EntityPlayer player) {
        this.startHealing(player, 1.0f);
    }

    private void startHealing(EntityPlayer player, float healingRate) {
        this.healingRate = healingRate;
        Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
        if (optstorage.isPresent()) {
            this.storage = optstorage.get();
            this.storage.teleportPos.store(player.posX, player.posY, player.posZ, player.rotationYaw, player.rotationPitch);
            this.player = player;
            this.beingUsed = true;
            if (healingRate == 1.5f) {
                BlockPos pos = this.getPos();
                player.openGui(Pixelmon.INSTANCE, EnumGui.HealerNurseJoy.getIndex(), player.world, pos.getX(), pos.getY(), pos.getZ());
            } else {
                player.openGui(Pixelmon.INSTANCE, EnumGui.Healer.getIndex(), player.world, 0, 0, 0);
            }
            this.tickCount = 0;
            this.allPlaced = false;
            this.pokemonLastPlaced = -1;
            Arrays.fill(this.pokeballType, null);
            this.stayDark = false;
        }
        if (!this.world.isRemote && this.beingUsed) {
            this.sendRedstoneSignal();
        }
    }

    private int getTicksToHeal() {
        return (int)((float)PixelmonConfig.healerSpeed / this.healingRate);
    }

    @Override
    public void update() {
        if (this.world.isRemote) {
            this.rotation += 19.0f;
            ++this.flashTimer;
            return;
        }
        if (this.beingUsed) {
            int ticksToHeal;
            if (this.allPlaced && !this.playSound) {
                this.world.playSound(null, this.pos, PixelSounds.healerActive, SoundCategory.BLOCKS, 0.7f, 1.0f);
                this.playSound = true;
            }
            ++this.tickCount;
            if (!this.allPlaced && this.tickCount == 5) {
                boolean hasNextPokemon = false;
                for (int i = this.pokemonLastPlaced + 1; i < this.storage.getList().length; ++i) {
                    Optional<EntityPixelmon> pixelmonOptional;
                    if (this.storage.getList()[i] == null) continue;
                    this.pokemonLastPlaced = i;
                    hasNextPokemon = true;
                    this.pokeballType[i] = EnumPokeball.getFromIndex(this.storage.getList()[i].getInteger("CaughtBall"));
                    Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)this.player);
                    if (!optstorage.isPresent() || !(pixelmonOptional = optstorage.get().getAlreadyExists(PixelmonMethods.getID(this.storage.getList()[i]), this.player.world)).isPresent()) break;
                    EntityPixelmon pixelmon = pixelmonOptional.get();
                    pixelmon.catchInPokeball();
                    break;
                }
                if (!hasNextPokemon) {
                    this.allPlaced = true;
                }
                this.tickCount = 0;
                this.world.getMinecraftServer().getPlayerList().sendToAllNearExcept(null, this.pos.getX(), this.pos.getY(), this.pos.getZ(), 10.0, 0, this.getUpdatePacket());
            }
            if (this.tickCount == (ticksToHeal = this.getTicksToHeal()) - 30) {
                this.stayDark = true;
                this.world.getMinecraftServer().getPlayerList().sendToAllNearExcept(null, this.pos.getX(), this.pos.getY(), this.pos.getZ(), 10.0, 0, this.getUpdatePacket());
            }
            if (this.tickCount == ticksToHeal) {
                this.storage.healAllPokemon(this.world);
                this.beingUsed = false;
                this.playSound = false;
                this.world.getMinecraftServer().getPlayerList().sendToAllNearExcept(null, this.pos.getX(), this.pos.getY(), this.pos.getZ(), 10.0, 0, this.getUpdatePacket());
                this.player.closeScreen();
                if (!this.world.isRemote) {
                    this.sendRedstoneSignal();
                }
            }
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        for (int i = 0; i < this.pokeballType.length; ++i) {
            if (this.pokeballType[i] == null) continue;
            nbt.setShort("PokeballType" + i, (short)this.pokeballType[i].ordinal());
        }
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        for (int i = 0; i < this.pokeballType.length; ++i) {
            this.pokeballType[i] = null;
            if (!nbt.hasKey("PokeballType" + i)) continue;
            this.pokeballType[i] = EnumPokeball.getFromIndex(nbt.getShort("PokeballType" + i));
        }
        if (nbt.hasKey("BeingUsed")) {
            this.beingUsed = nbt.getBoolean("BeingUsed");
            this.allPlaced = nbt.getBoolean("AllPlaced");
            this.stayDark = nbt.getBoolean("StayDark");
        }
        if (nbt.hasKey("HealRate")) {
            this.healingRate = nbt.getFloat("HealRate");
        }
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        nbt.setBoolean("BeingUsed", this.beingUsed);
        nbt.setBoolean("AllPlaced", this.allPlaced);
        nbt.setBoolean("StayDark", this.stayDark);
        nbt.setFloat("HealRate", this.healingRate);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
    }

    private void sendRedstoneSignal() {
        Block block = this.getBlockType();
        if (block != null) {
            this.world.notifyNeighborsOfStateChange(this.pos, block, true);
        }
    }
}

