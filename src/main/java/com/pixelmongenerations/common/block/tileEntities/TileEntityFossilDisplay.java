/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.pixelmongenerations.client.models.animations.AnimateTask;
import com.pixelmongenerations.common.block.multiBlocks.BlockFossilDisplay;
import com.pixelmongenerations.common.block.tileEntities.IFrameCounter;
import com.pixelmongenerations.common.block.tileEntities.ISpecialTexture;
import com.pixelmongenerations.common.item.ItemFossil;
import java.util.TimerTask;
import java.util.UUID;
import net.minecraft.item.Item;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.WorldServer;

public class TileEntityFossilDisplay
extends TileEntity
implements ISpecialTexture,
IFrameCounter {
    private ItemFossil itemInDisplay = null;
    private UUID owner = null;
    public int renderPass = 0;
    public int frame = 0;
    AnimateTask animateTask;

    @Override
    public ResourceLocation getTexture() {
        if (this.getBlockType() instanceof BlockFossilDisplay) {
            BlockFossilDisplay block = (BlockFossilDisplay)this.getBlockType();
            if (block.getColor() == null) {
                return new ResourceLocation("pixelmon:textures/blocks/FossilDisplayModel.png");
            }
            return new ResourceLocation("pixelmon:textures/blocks/" + block.getColor().name() + "FossilDisplayModel.png");
        }
        return new ResourceLocation("pixelmon:textures/blocks/FossilDisplayModel.png");
    }

    public void openGlass() {
        if (this.getLastFrame() == 30) {
            if (this.animateTask != null) {
                this.animateTask.cancel();
            }
            this.animateTask = new AnimateTask(this, 30, 60);
            AnimateTask.timer.scheduleAtFixedRate((TimerTask)this.animateTask, 100L, 20L);
            if (!this.world.isRemote) {
                this.sendAnimation("open");
                this.markDirty();
            }
        }
    }

    public void closeGlass() {
        if (this.getLastFrame() == 0 || this.getLastFrame() == 60) {
            if (this.animateTask != null) {
                this.animateTask.cancel();
            }
            this.animateTask = new AnimateTask(this, 0, 30);
            AnimateTask.timer.scheduleAtFixedRate((TimerTask)this.animateTask, 100L, 20L);
            if (!this.world.isRemote) {
                this.sendAnimation("close");
                this.markDirty();
            }
        }
    }

    public boolean isOpen() {
        return this.getLastFrame() == 0 || this.getLastFrame() == 60;
    }

    @Override
    public int getFrame() {
        return this.frame;
    }

    @Override
    public void setFrame(int frame) {
        this.frame = frame;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        Item item = Item.getItemById(nbt.getInteger("ItemIn"));
        this.setItemInDisplay(item instanceof ItemFossil ? item : null);
        this.setLastFrame(nbt.getInteger("frame"));
        if (nbt.hasKey("owner")) {
            this.owner = UUID.fromString(nbt.getString("owner"));
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setInteger("ItemIn", Item.getIdFromItem(this.itemInDisplay));
        if (this.frame <= 30 && this.frame >= 1) {
            nbt.setInteger("frame", 30);
        } else {
            nbt.setInteger("frame", 0);
        }
        if (this.owner != null) {
            nbt.setString("owner", this.owner.toString());
        }
        return nbt;
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound nbt = new NBTTagCompound();
        this.writeToNBT(nbt);
        return nbt;
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 0, this.getUpdateTag());
    }

    @Override
    public void onLoad() {
        if (this.hasWorld() && this.getWorld() instanceof WorldServer) {
            ((WorldServer)this.getWorld()).getPlayerChunkMap().markBlockForUpdate(this.pos);
        }
    }

    public void sendAnimation(String str) {
        NBTTagCompound nbt = new NBTTagCompound();
        if (this.frame <= 30 && this.frame >= 1) {
            nbt.setInteger("frame", 30);
        } else {
            nbt.setInteger("frame", 0);
        }
        nbt.setString("Animation", str);
        SPacketUpdateTileEntity packet = new SPacketUpdateTileEntity(this.pos, 0, nbt);
        this.world.getMinecraftServer().getPlayerList().sendToAllNearExcept(null, this.pos.getX(), this.pos.getY(), this.pos.getZ(), 20.0, this.world.provider.getDimension(), packet);
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        NBTTagCompound nbt = pkt.getNbtCompound();
        if (nbt.hasKey("ItemIn", 3)) {
            Item item = Item.getItemById(nbt.getInteger("ItemIn"));
            this.setItemInDisplay(item instanceof ItemFossil ? item : null);
        }
        this.setFrame(nbt.getInteger("frame"));
        if (nbt.hasKey("Animation", 8)) {
            if (nbt.getString("Animation").equals("open")) {
                this.openGlass();
            } else {
                this.closeGlass();
            }
        }
    }

    public UUID getOwnerUUID() {
        return this.owner;
    }

    public void setOwner(UUID owner) {
        this.owner = owner;
        this.markDirty();
    }

    public ItemFossil getItemInDisplay() {
        return this.itemInDisplay;
    }

    public void setItemInDisplay(Item item) {
        if (item == null || item instanceof ItemFossil) {
            this.itemInDisplay = (ItemFossil)item;
            this.markDirty();
        }
    }

    public int getLastFrame() {
        return this.frame;
    }

    public void setLastFrame(int frame) {
        this.frame = frame;
    }

    @Override
    public boolean shouldRenderInPass(int pass) {
        this.renderPass = pass;
        return true;
    }
}

