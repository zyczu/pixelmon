/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableMap
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.block.tileEntities;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.pixelmongenerations.api.pokemon.PokemonGroup;
import com.pixelmongenerations.common.block.machines.BlockShrine;
import com.pixelmongenerations.common.block.spawnmethod.BlockWeatherTrioShrine;
import com.pixelmongenerations.common.block.tileEntities.TileEntityShrine;
import com.pixelmongenerations.common.item.IShrineItem;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.SpawnColors;
import java.awt.Color;
import java.util.List;
import java.util.Map;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

public class TileEntityWeatherTrioShrine extends TileEntityShrine {

    Map<EnumSpecies, List<Color>> colors = ImmutableMap.of(
            EnumSpecies.Groudon, Lists.newArrayList(SpawnColors.RED, SpawnColors.BLACK, SpawnColors.WHITE),
            EnumSpecies.Kyogre, Lists.newArrayList(SpawnColors.BLUE, SpawnColors.RED, SpawnColors.WHITE));

    @Override
    public void activate(EntityPlayer player, World world, BlockShrine block, IBlockState state, ItemStack item) {
        if (this.isSpawning()) {
            return;
        }
        if (PixelmonConfig.limitRayquaza) {
            PlayerStorage storage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player).get();
            for (int i = 0; i < 6; ++i) {
                String name;
                if (storage.getNBT(storage.getIDFromPosition(i)) == null || !(name = storage.getNBT(storage.getIDFromPosition(i)).getString("Name")).equals("Rayquaza")) continue;
                NBTTagCompound compound = storage.getNBT(storage.getIDFromPosition(i));
                String key = this.getKeyFromActiveSpecies(this.getActiveGroup().getSpecies());
                if (key == null) {
                    return;
                }
                if (compound.getBoolean(key)) {
                    player.sendMessage(new TextComponentString((Object)((Object)TextFormatting.RED) + "You have already summoned a " + this.selectActiveGroup().getSpecies().name + " using this Rayquaza!"));
                    return;
                }
                compound.setBoolean(key, true);
            }
        }
        player.addItemStackToInventory(((IShrineItem)((Object)item.getItem())).getUsedForm());
        player.getHeldItem(EnumHand.MAIN_HAND).shrink(1);
        this.startSpawning(player, state, world, this.pos);
    }

    public String getKeyFromActiveSpecies(EnumSpecies species) {
        switch (species) {
            case Groudon: {
                return "Groudon";
            }
            case Kyogre: {
                return "Kyogre";
            }
        }
        return null;
    }

    @Override
    protected PokemonGroup.PokemonData selectActiveGroup() {
        Block block = this.world.getBlockState(this.pos).getBlock();
        if (block instanceof BlockWeatherTrioShrine) {
            return ((BlockWeatherTrioShrine)block).getActive();
        }
        return null;
    }

    @Override
    public int maxTick() {
        return 100;
    }

    @Override
    protected void checkTick(int timeSpent) {
        super.checkTick(timeSpent);
    }

    @Override
    public String getRightClickMessage(ItemStack heldStack, IBlockState state) {
        if (state.getBlock() == PixelmonBlocks.groudonShrine) {
            return "pixelmon.groudon_shrine.right_click";
        }
        if (state.getBlock() == PixelmonBlocks.kyogreShrine) {
            return "pixelmon.kyogre_shrine.right_click";
        }
        return "";
    }

    public double adjust(double theta) {
        double t = theta * 2.0;
        return t > 1.0 ? 2.0 - t : t;
    }

    public boolean getActive() {
        return this.getTick() > 0;
    }
}

