/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.client.render.ParticleBlocks;
import com.pixelmongenerations.common.block.BoundingBoxSet;
import com.pixelmongenerations.common.block.enums.EnumMultiPos;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.BlockHorizontal;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.particle.ParticleManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class MultiBlock
extends BlockContainer {
    public static final PropertyDirection FACING = BlockHorizontal.FACING;
    public static final PropertyEnum<EnumMultiPos> MULTIPOS = PropertyEnum.create("multipos", EnumMultiPos.class);
    public int width;
    public int length;
    public double height;
    private static HashMap<Block, BoundingBoxSet> boundingBoxes = new HashMap();

    protected MultiBlock(Material material, int width, double height, int length) {
        super(material);
        this.width = width;
        this.height = height;
        this.length = length;
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, FACING, MULTIPOS);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(FACING, EnumFacing.byHorizontalIndex((int)(meta & 3))).withProperty(MULTIPOS, EnumMultiPos.fromMeta((meta & 0xF) >> 2));
    }

    @Override
    public IBlockState withRotation(IBlockState state, Rotation rot) {
        return state.withProperty(FACING, rot.rotate(state.getValue(FACING)));
    }

    @Override
    public IBlockState withMirror(IBlockState state, Mirror mirrorIn) {
        return state.withRotation(mirrorIn.toRotation(state.getValue(FACING)));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        int b0 = 0;
        int i = b0 | state.getValue(FACING).getHorizontalIndex();
        return i | state.getValue(MULTIPOS).toMeta() << 2;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        return this.getMultiBlockBoundingBox(source, pos, state.getValue(MULTIPOS), state.getValue(FACING));
    }

    protected AxisAlignedBB getMultiBlockBoundingBox(IBlockAccess worldIn, BlockPos pos, EnumMultiPos multiPos, EnumFacing facing) {
        if (!boundingBoxes.containsKey(this)) {
            BoundingBoxSet set = new BoundingBoxSet(this.width, this.height, this.length);
            boundingBoxes.put(this, set);
        }
        if (worldIn.getBlockState(pos).getBlock() != this) {
            return new AxisAlignedBB(0.0, 0.0, 0.0, this.width, this.height, this.length);
        }
        if (multiPos == EnumMultiPos.BASE) {
            if (facing == EnumFacing.SOUTH) {
                return MultiBlock.boundingBoxes.get((Object)this).AABBBaseSouth;
            }
            if (facing == EnumFacing.NORTH) {
                return MultiBlock.boundingBoxes.get((Object)this).AABBBaseNorth;
            }
            if (facing == EnumFacing.EAST) {
                return MultiBlock.boundingBoxes.get((Object)this).AABBBaseEast;
            }
            return MultiBlock.boundingBoxes.get((Object)this).AABBBaseWest;
        }
        BlockPos base = this.findBaseBlock(worldIn, new BlockPos.MutableBlockPos(pos), worldIn.getBlockState(pos));
        try {
            return this.getMultiBlockBoundingBox(worldIn, base, EnumMultiPos.BASE, worldIn.getBlockState(base).getValue(FACING)).offset(base.getX() - pos.getX(), base.getY() - pos.getY(), base.getZ() - pos.getZ());
        }
        catch (IllegalArgumentException e) {
            return new AxisAlignedBB(0.0, 0.0, 0.0, this.width, this.height, this.length);
        }
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public boolean addDestroyEffects(World world, BlockPos pos, ParticleManager effectRenderer) {
        int nb = 4;
        BlockPos base = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), world.getBlockState(pos));
        for (int i = 0; i < nb; ++i) {
            for (int j = 0; j < nb; ++j) {
                for (int k = 0; k < nb; ++k) {
                    double fxX = (double)pos.getX() + ((double)i + 0.5) / (double)nb;
                    double fxY = (double)pos.getY() + ((double)j + 0.5) / (double)nb;
                    double fxZ = (double)pos.getZ() + ((double)k + 0.5) / (double)nb;
                    try {
                        ParticleBlocks fx = new ParticleBlocks(world, fxX, fxY, fxZ, 0.0, 0.0, 0.0, world.getBlockState(base));
                        effectRenderer.addEffect(fx);
                        continue;
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return true;
    }

    @Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        Item item;
        ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
        if (world instanceof World && (item = this.getDroppedItem((World)world, pos)) != null) {
            ret.add(new ItemStack(item, 1, this.damageDropped(state)));
        }
        return ret;
    }

    public abstract Item getDroppedItem(World var1, BlockPos var2);

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return null;
    }

    @Override
    public TileEntity createTileEntity(World world, IBlockState state) {
        if (state.getValue(MULTIPOS) == EnumMultiPos.BASE) {
            return this.getTileEntity(world, state).orElse(null);
        }
        return null;
    }

    protected abstract Optional<TileEntity> getTileEntity(World var1, IBlockState var2);

    @Override
    public boolean removedByPlayer(IBlockState state, World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {
        EnumMultiPos multiPos = state.getValue(MULTIPOS);
        BlockPos location = this.findBaseBlock(world, new BlockPos.MutableBlockPos(pos), state);
        if (location.toLong() != pos.toLong()) {
            state = world.getBlockState(location);
            try {
                multiPos = state.getValue(MULTIPOS);
            }
            catch (Exception e) {
                return super.removedByPlayer(state, world, pos, player, willHarvest);
            }
        }
        if (multiPos == EnumMultiPos.BASE && !player.capabilities.isCreativeMode) {
            this.dropBlockAsItem(world, pos, state, 0);
        }
        this.setMultiBlocksWidth(location, world, state);
        return super.removedByPlayer(state, world, pos, player, willHarvest);
    }

    public BlockPos findBaseBlock(IBlockAccess world, BlockPos.MutableBlockPos pos, IBlockState state) {
        try {
            if (state.getBlock() != this) {
                return pos;
            }
            EnumFacing facing = state.getValue(FACING);
            EnumMultiPos multipos = state.getValue(MULTIPOS);
            if (multipos == EnumMultiPos.TOP) {
                pos.move(EnumFacing.DOWN);
            } else if (multipos == EnumMultiPos.BOTTOM) {
                pos.move(facing, -1);
            } else {
                return new BlockPos(pos);
            }
            return this.findBaseBlock(world, pos, world.getBlockState(pos));
        }
        catch (Exception e) {
            return pos;
        }
    }

    private void setMultiBlocksWidth(BlockPos pos, World world, IBlockState state) {
        EnumFacing facing = state.getValue(FACING);
        if (facing == EnumFacing.EAST) {
            for (int l = 0; l < this.width; ++l) {
                this.setMultiBlocksLength(pos, 0, l, state, world);
            }
        } else if (facing == EnumFacing.NORTH) {
            for (int l = 0; l < this.width; ++l) {
                this.setMultiBlocksLength(pos, l, 0, state, world);
            }
        } else if (facing == EnumFacing.WEST) {
            for (int l = 0; l < this.width; ++l) {
                this.setMultiBlocksLength(pos, 0, -1 * l, state, world);
            }
        } else {
            for (int l = 0; l < this.width; ++l) {
                this.setMultiBlocksLength(pos, -1 * l, 0, state, world);
            }
        }
    }

    private void setMultiBlocksLength(BlockPos pos, int xd, int zd, IBlockState state, World world) {
        EnumFacing facing = state.getValue(FACING);
        if (facing == EnumFacing.EAST) {
            for (int w = 0; w < this.length; ++w) {
                this.setMultiBlocksHeight(pos, xd + w, zd, state, world);
            }
        } else if (facing == EnumFacing.NORTH) {
            for (int w = 0; w < this.length; ++w) {
                this.setMultiBlocksHeight(pos, xd, zd - w, state, world);
            }
        } else if (facing == EnumFacing.WEST) {
            for (int w = 0; w < this.length; ++w) {
                this.setMultiBlocksHeight(pos, xd - w, zd, state, world);
            }
        } else {
            for (int w = 0; w < this.length; ++w) {
                this.setMultiBlocksHeight(pos, xd, zd + w, state, world);
            }
        }
    }

    private void setMultiBlocksHeight(BlockPos pos, int xd, int zd, IBlockState state, World world) {
        int h = 0;
        while ((double)h < this.height) {
            BlockPos p = new BlockPos(pos.getX() + xd, pos.getY() + h, pos.getZ() + zd);
            world.setBlockToAir(p);
            ++h;
        }
    }
}

