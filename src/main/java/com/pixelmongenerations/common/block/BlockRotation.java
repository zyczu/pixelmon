/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

public enum BlockRotation {
    Normal(0),
    CW(1),
    CCW(3),
    Rotate180(2);

    public int metadata;

    private BlockRotation(int metadata) {
        this.metadata = metadata;
    }

    public static BlockRotation getRotationFromMetadata(int metadata) {
        for (BlockRotation b : BlockRotation.values()) {
            if (b.metadata != metadata) continue;
            return b;
        }
        return BlockRotation.tryForCloningMetadata(metadata);
    }

    @Deprecated
    private static BlockRotation tryForCloningMetadata(int metadata) {
        switch (metadata) {
            case 8: {
                return Rotate180;
            }
            case 9: {
                return CW;
            }
            case 10: {
                return Normal;
            }
            case 11: {
                return CCW;
            }
        }
        return null;
    }
}

