/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.block;

import com.pixelmongenerations.api.events.BerryEvent;
import com.pixelmongenerations.common.block.apricornTrees.BlockApricornTree;
import com.pixelmongenerations.common.block.enums.EnumBlockPos;
import com.pixelmongenerations.common.block.generic.GenericRotatableModelBlock;
import com.pixelmongenerations.common.block.tileEntities.TileEntityBerryTree;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.EnumBerryQuality;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.MinecraftForge;

public class BlockBerryTree
extends GenericRotatableModelBlock
implements IPlantable {
    public static final PropertyEnum<EnumBlockPos> BLOCKPOS = PropertyEnum.create("blockpos", EnumBlockPos.class);
    private final EnumBerry berry;

    public BlockBerryTree(EnumBerry type) {
        super(Material.WOOD);
        this.berry = type;
    }

    @Override
    public Material getMaterial(IBlockState state) {
        return Material.PLANTS;
    }

    @Override
    public float getBlockHardness(IBlockState state, World world, BlockPos pos) {
        TileEntityBerryTree tile;
        if (state.getValue(BLOCKPOS) == EnumBlockPos.TOP) {
            pos = pos.down();
        }
        if ((tile = BlockHelper.getTileEntity(TileEntityBerryTree.class, world, pos)) != null && tile.getStage() < 2) {
            return 1.0f;
        }
        return 2.0f;
    }

    @Override
    public IBlockState withRotation(IBlockState state, Rotation rot) {
        return state.withProperty(FACING, rot.rotate(state.getValue(FACING)));
    }

    @Override
    public IBlockState withMirror(IBlockState state, Mirror mirrorIn) {
        return state.withRotation(mirrorIn.toRotation(state.getValue(FACING)));
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(this.berry.getBerry());
    }

    @Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        ArrayList drops = new ArrayList();
        TileEntityBerryTree tile = BlockHelper.getTileEntity(TileEntityBerryTree.class, world, pos);
        if (tile == null) {
            return drops;
        }
        byte stage = tile.getStage();
        if (stage > 0 && stage < 6) {
            return Collections.singletonList(new ItemStack(this.berry.getBerry()));
        }
        if (world instanceof World) {
            drops = IntStream.range(0, tile.getProjectedYield()).mapToObj(i -> this.getBerry(this.berry)).collect(Collectors.toCollection(ArrayList::new));
            if (tile.getBerryMutation() != null) {
                drops.add(new ItemStack(tile.getBerryMutation().getBerry()));
            }
        }
        return drops;
    }

    private ItemStack getBerry(EnumBerry berry) {
        ItemStack stack = new ItemStack(berry.getBerry());
        ItemBerry.setQuality(stack, EnumBerryQuality.getFromIndex(RandomHelper.getRandomNumberBetween(0, 4)));
        return stack;
    }

    @Override
    public TileEntity createTileEntity(World world, IBlockState state) {
        if (state.getValue(BLOCKPOS) == EnumBlockPos.BOTTOM) {
            return new TileEntityBerryTree(this.berry);
        }
        return null;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return null;
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        TileEntityBerryTree tile;
        if (world.isRemote) {
            return true;
        }
        if (state.getValue(BLOCKPOS) == EnumBlockPos.TOP) {
            pos = pos.down();
            state = world.getBlockState(pos);
        }
        if ((tile = BlockHelper.getTileEntity(TileEntityBerryTree.class, world, pos)) == null) {
            return false;
        }
        byte stage = tile.getStage();
        if (stage == 6) {
            List<ItemStack> drops = this.getDrops(world, pos, state, 0);
            BerryEvent.PickBerryEvent pickEvent = new BerryEvent.PickBerryEvent(this.berry, pos, (EntityPlayerMP)player, tile, drops);
            if (MinecraftForge.EVENT_BUS.post(pickEvent)) {
                return false;
            }
            pickEvent.getPickedStacks().forEach(drop -> DropItemHelper.giveItemStackToPlayer((EntityPlayerMP)player, drop));
            this.removedByPlayerNoDrops(state, world, pos, player, false);
            return true;
        }
        return false;
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer((Block)this, FACING, BLOCKPOS);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        int b0 = 0;
        int i = b0 | state.getValue(FACING).getHorizontalIndex();
        return i | state.getValue(BLOCKPOS).toMeta() << 2;
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(FACING, EnumFacing.byHorizontalIndex((int)(meta & 3))).withProperty(BLOCKPOS, EnumBlockPos.fromMeta((meta & 0xF) >> 2));
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
        worldIn.setBlockState(pos, state.withProperty(FACING, placer.getHorizontalFacing().getOpposite()), 2);
        if (placer instanceof EntityPlayer) {
            ItemBlock.setTileEntityNBT(worldIn, (EntityPlayer)placer, pos, stack);
        }
    }

    public void growStage(World world, Random rand, BlockPos pos, IBlockState state) {
        TileEntityBerryTree tile;
        EnumBlockPos blockpos = state.getValue(BLOCKPOS);
        if (blockpos == EnumBlockPos.TOP) {
            pos = pos.down();
        }
        if ((tile = BlockHelper.getTileEntity(TileEntityBerryTree.class, world, pos)) == null) {
            return;
        }
        int stage = tile.getStage();
        if (stage < 6) {
            tile.setStage((byte)(++stage));
            if (stage == 6) {
                MinecraftForge.EVENT_BUS.post(new BerryEvent.BerryReadyEvent(this.berry, pos, tile));
            }
            if (stage >= 3 && world.getBlockState(pos.up()).getBlock() != this && this.berry.height > 1) {
                world.setBlockState(pos.up(), state.withProperty(BLOCKPOS, EnumBlockPos.TOP), 2);
            }
            ((WorldServer)world).getPlayerChunkMap().markBlockForUpdate(pos);
        }
    }

    public void replant(World worldIn, BlockPos pos, IBlockState state) {
        TileEntityBerryTree tile;
        EnumBlockPos blockpos = state.getValue(BLOCKPOS);
        if (blockpos == EnumBlockPos.TOP) {
            pos = pos.down();
        }
        if ((tile = BlockHelper.getTileEntity(TileEntityBerryTree.class, worldIn, pos)) == null) {
            return;
        }
        byte stage = tile.getStage();
        tile.setStage((byte)1);
        if (stage >= 3 && this.berry.height > 1) {
            worldIn.setBlockState(pos.up(), Blocks.AIR.getDefaultState());
        }
        ((WorldServer)worldIn).getPlayerChunkMap().markBlockForUpdate(pos);
    }

    public boolean canBlockStay(World worldIn, BlockPos pos, IBlockState state) {
        BlockPos down = pos.down();
        Block soil = worldIn.getBlockState(down).getBlock();
        if (soil == this) {
            soil = worldIn.getBlockState(down.down()).getBlock();
        }
        return this.canPlaceBlockOn(soil);
    }

    protected void checkAndDropBlock(World worldIn, BlockPos pos, IBlockState state) {
        if (!this.canBlockStay(worldIn, pos, state)) {
            this.dropBlockAsItem(worldIn, pos, state, 0);
            worldIn.setBlockState(pos, Blocks.AIR.getDefaultState(), 3);
        }
    }

    protected boolean canPlaceBlockOn(Block ground) {
        return BlockApricornTree.isSuitableSoil(ground);
    }

    @Override
    public boolean removedByPlayer(IBlockState state, World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {
        BlockPos upPos;
        EnumBlockPos multiPos = state.getValue(BLOCKPOS);
        if (multiPos == EnumBlockPos.TOP) {
            pos = pos.down();
        }
        this.dropBlockAsItem(world, pos, state, 0);
        if (world.getBlockState(pos).getBlock() != this) {
            world.setBlockState(pos.up(), Blocks.AIR.getDefaultState(), 3);
            return super.removedByPlayer(state, world, pos, player, willHarvest);
        }
        if (this.berry.height > 1 && world.getBlockState(upPos = pos.up()).getBlock() == this) {
            world.setBlockState(upPos, Blocks.AIR.getDefaultState(), 3);
        }
        return super.removedByPlayer(state, world, pos, player, willHarvest);
    }

    public boolean removedByPlayerNoDrops(IBlockState state, World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {
        BlockPos upPos;
        EnumBlockPos multiPos = state.getValue(BLOCKPOS);
        if (multiPos == EnumBlockPos.TOP) {
            pos = pos.down();
        }
        if (world.getBlockState(pos).getBlock() != this) {
            world.setBlockState(pos.up(), Blocks.AIR.getDefaultState(), 3);
            return super.removedByPlayer(state, world, pos, player, willHarvest);
        }
        if (this.berry.height > 1 && world.getBlockState(upPos = pos.up()).getBlock() == this) {
            world.setBlockState(upPos, Blocks.AIR.getDefaultState(), 3);
        }
        return super.removedByPlayer(state, world, pos, player, willHarvest);
    }

    @Override
    public EnumPlantType getPlantType(IBlockAccess world, BlockPos pos) {
        return EnumPlantType.Plains;
    }

    @Override
    public IBlockState getPlant(IBlockAccess world, BlockPos pos) {
        IBlockState state = world.getBlockState(pos);
        if (state.getBlock() != this) {
            return this.getDefaultState();
        }
        return state;
    }

    public boolean canGrowStage(World worldObj, BlockPos pos, IBlockState state, int stage) {
        if (stage != 2 || this.berry.height == 1) {
            return true;
        }
        EnumBlockPos blockpos = state.getValue(BLOCKPOS);
        BlockPos loc = pos;
        if (blockpos == EnumBlockPos.BOTTOM) {
            loc = pos.up();
        }
        return worldObj.getBlockState(loc).getBlock() == Blocks.AIR && worldObj.canSeeSky(pos.up());
    }

    @Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
        this.checkAndDropBlock(worldIn, pos, state);
    }

    @Override
    public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand) {
        this.checkAndDropBlock(worldIn, pos, state);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    public EnumBerry getBerryType() {
        return this.berry;
    }
}

