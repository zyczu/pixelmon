/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.block.enums.EnumPokechestVisibility;
import com.pixelmongenerations.common.block.tileEntities.TileEntityPokeChest;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemItemFinder
extends PixelmonItem {
    public ItemItemFinder() {
        super("item_finder");
        this.setCreativeTab(PixelmonCreativeTabs.tabPokeLoot);
        this.setMaxStackSize(1);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        ItemStack itemStack = player.getHeldItem(hand);
        player.getCooldownTracker().setCooldown(this, 20);
        if (!world.isRemote) {
            TileEntityPokeChest chest = BlockHelper.findClosestTileEntity(TileEntityPokeChest.class, player, 35.0, p -> p.getVisibility() == EnumPokechestVisibility.Hidden);
            if (chest != null) {
                EnumFacing direction = this.getDirection(player, chest.getPos());
                if (direction == EnumFacing.NORTH) {
                    itemStack.setItemDamage(1);
                } else if (direction == EnumFacing.SOUTH) {
                    itemStack.setItemDamage(2);
                } else if (direction == EnumFacing.EAST) {
                    itemStack.setItemDamage(3);
                } else {
                    itemStack.setItemDamage(4);
                }
                world.playSound(null, player.posX, player.posY + 0.5, player.posZ, SoundEvents.BLOCK_LEVER_CLICK, SoundCategory.PLAYERS, 0.5f, 1.0f);
            } else {
                itemStack.setItemDamage(0);
            }
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemStack);
    }

    private EnumFacing getDirection(EntityPlayer player, BlockPos pos) {
        int x = (int)(player.posX - (double)pos.getX());
        int z = (int)(player.posZ - (double)pos.getZ());
        EnumFacing direction = Math.abs(x) > Math.abs(z) ? (x > 0 ? EnumFacing.WEST : EnumFacing.EAST) : (z > 0 ? EnumFacing.NORTH : EnumFacing.SOUTH);
        EnumFacing facing = player.getHorizontalFacing();
        if (facing == direction) {
            return EnumFacing.NORTH;
        }
        if (facing.getOpposite() == direction) {
            return EnumFacing.SOUTH;
        }
        if (facing.rotateY() == direction) {
            return EnumFacing.EAST;
        }
        return EnumFacing.WEST;
    }
}

