/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.ICurryRarity;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.EnumCurryType;
import java.util.HashMap;
import java.util.Map;

public class CurryIngredient
extends PixelmonItem
implements ICurryRarity {
    private static final Map<EnumCurryType, CurryIngredient> ingredientToTypeMap = new HashMap<EnumCurryType, CurryIngredient>();
    private final EnumCurryType type;

    public CurryIngredient(String name, EnumCurryType type) {
        super(name);
        this.type = type;
        this.setCreativeTab(PixelmonCreativeTabs.curryIngredients);
        ingredientToTypeMap.put(type, this);
    }

    public EnumCurryType getType() {
        return this.type;
    }

    @Override
    public int getRarity() {
        return this.type.getRarity();
    }

    public static CurryIngredient from(EnumCurryType type) {
        return ingredientToTypeMap.get(type);
    }
}

