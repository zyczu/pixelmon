/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.custom.EntityPixelmonPainting;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemPixelmonPainting
extends PixelmonItem {
    public ItemPixelmonPainting(String itemName) {
        super(itemName);
        this.setCreativeTab(PixelmonCreativeTabs.decoration);
        this.setTranslationKey("pixelmon_painting");
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (!worldIn.isRemote) {
            ItemStack stack;
            if (side == EnumFacing.DOWN) {
                return EnumActionResult.FAIL;
            }
            if (side == EnumFacing.UP) {
                return EnumActionResult.FAIL;
            }
            BlockPos blockpos1 = pos.offset(side);
            if (!player.canPlayerEdit(blockpos1, side, stack = player.getHeldItem(hand))) {
                return EnumActionResult.FAIL;
            }
            EntityPixelmonPainting entityhanging = this.createEntity(worldIn, blockpos1, side);
            if (entityhanging.onValidSurface()) {
                worldIn.spawnEntity(entityhanging);
                stack.splitStack(1);
            }
            return EnumActionResult.SUCCESS;
        }
        return EnumActionResult.PASS;
    }

    private EntityPixelmonPainting createEntity(World worldIn, BlockPos pos, EnumFacing clickedSide) {
        return new EntityPixelmonPainting(worldIn, pos, clickedSide);
    }
}

