/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import net.minecraft.entity.player.EntityPlayer;

public class ItemAbilityPatch
extends PixelmonItem {
    public ItemAbilityPatch() {
        super("ability_patch");
    }

    public boolean useOnEntity(EntityPixelmon pixelmon, EntityPlayer player) {
        if (!pixelmon.isPokemon(EnumSpecies.Zygarde)) {
            int slot = pixelmon.getAbilitySlot();
            if (pixelmon.isPokemon(EnumSpecies.Greninja)) {
                if (slot != 2) {
                    pixelmon.setAbilitySlot(2);
                    pixelmon.setAbility(pixelmon.baseStats.abilities[2]);
                    pixelmon.update(EnumUpdateType.Ability);
                    ChatHandler.sendChat(player, "pixelmon.interaction.abilitycapsule", pixelmon.getNickname(), pixelmon.getAbility().getLocalizedName());
                    return true;
                }
                ChatHandler.sendChat(player, "pixelmon.interaction.noeffect", new Object[0]);
                return false;
            }
            if (slot == 2 || pixelmon.baseStats.abilities[1] == null && pixelmon.baseStats.abilities[2] == null) {
                ChatHandler.sendChat(player, "pixelmon.interaction.noeffect", new Object[0]);
                return false;
            }
            if (pixelmon.baseStats.abilities[2] == null) {
                pixelmon.setAbilitySlot(1);
                pixelmon.setAbility(pixelmon.baseStats.abilities[1]);
            } else {
                pixelmon.setAbilitySlot(2);
                pixelmon.setAbility(pixelmon.baseStats.abilities[2]);
            }
            pixelmon.update(EnumUpdateType.Ability);
            ChatHandler.sendChat(player, "pixelmon.interaction.abilitycapsule", pixelmon.getNickname(), pixelmon.getAbility().getLocalizedName());
            return true;
        }
        ChatHandler.sendChat(player, "pixelmon.interaction.noeffect", new Object[0]);
        return false;
    }
}

