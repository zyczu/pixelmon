/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import net.minecraft.item.ItemBlock;

public class ItemHiddenCube
extends ItemBlock {
    public ItemHiddenCube() {
        super(PixelmonBlocks.hiddenCube);
        this.setTranslationKey("hidden_cube");
        this.setMaxStackSize(16);
        this.setCreativeTab(PixelmonCreativeTabs.utilityBlocks);
    }
}

