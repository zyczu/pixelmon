/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.enums.EnumEvolutionStone;
import net.minecraft.creativetab.CreativeTabs;

public class ItemEvolutionStone
extends PixelmonItem {
    private final EnumEvolutionStone stoneType;

    public ItemEvolutionStone(EnumEvolutionStone stoneType, String itemName) {
        super(itemName);
        this.stoneType = stoneType;
        this.setCreativeTab(CreativeTabs.MISC);
        this.canRepair = false;
    }

    public EnumEvolutionStone getType() {
        return this.stoneType;
    }
}

