/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import net.minecraft.item.Item;

public interface IEnumItem {
    public Item getItem(int var1);

    public int numTypes();
}

