/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.capabilities.curry.CurryData;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumCurryType;
import com.pixelmongenerations.core.enums.EnumFlavor;
import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.jetbrains.annotations.NotNull;

public class ItemCurry
extends PixelmonItem {
    public ItemCurry(String name) {
        super(name, false);
        this.setMaxStackSize(64);
        this.setCreativeTab(PixelmonCreativeTabs.restoration);
    }

    @Override
    @NotNull
    public String getItemStackDisplayName(@NotNull ItemStack stack) {
        CurryData data = ItemCurry.getData(stack);
        String name = this.getLocalizedName();
        if (data.getCurryType() != EnumCurryType.None) {
            name = data.getCurryType().getLocalizedName() + " " + name;
        }
        if (data.getFlavor() != EnumFlavor.None) {
            name = data.getFlavor().getLocalizedName() + " " + name;
        }
        return name;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        String info = I18n.format("gui.shopkeeper." + this.getTranslationKey(), new Object[0]);
        if (!this.hasHideFlag(stack) && !info.startsWith("gui.shopkeeper.")) {
            if (GuiScreen.isShiftKeyDown()) {
                tooltip.add(info);
                tooltip.add("Rating: " + ItemCurry.getData(stack).getRating().getName());
            } else {
                tooltip.add("Hold shift for more info.");
            }
        }
    }

    @Override
    public boolean hasHideFlag(ItemStack stack) {
        return stack.hasTagCompound() && stack.getTagCompound().getBoolean("HideTooltip");
    }

    public static ItemStack createStack(CurryData data) {
        ItemStack stack = new ItemStack(PixelmonItems.curry);
        ItemCurry.setData(stack, data);
        return stack;
    }

    public static void setData(ItemStack stack, CurryData data) {
        stack.setTagInfo("data", data.toNbt());
    }

    public static CurryData getData(ItemStack stack) {
        return CurryData.fromNbt(stack.getOrCreateSubCompound("data"));
    }

    @Override
    public boolean getShareTag() {
        return true;
    }
}

