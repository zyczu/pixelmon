/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.item.tools;

import com.pixelmongenerations.common.item.ItemHammer;
import com.pixelmongenerations.common.item.tools.ToolEffects;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonItemsTools;
import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class GenericHammer
extends ItemHammer {
    public GenericHammer(Item.ToolMaterial material, String itemName) {
        super(material, itemName);
    }

    public String getTranslationKey() {
        return super.getTranslationKey().substring(5);
    }

    @Override
    public float getDestroySpeed(ItemStack stack, IBlockState state) {
        if (state.getBlock() == PixelmonBlocks.anvil) {
            if (this.toolMaterial == PixelmonItemsTools.RUBY || this.toolMaterial == PixelmonItemsTools.SAPPHIRE || this.toolMaterial == PixelmonItemsTools.AMETHYST || this.toolMaterial == PixelmonItemsTools.CRYSTAL || this.toolMaterial == PixelmonItemsTools.LEAFSTONE) {
                return 4.0f;
            }
            if (this.toolMaterial == PixelmonItemsTools.THUNDERSTONE || this.toolMaterial == PixelmonItemsTools.SUNSTONE || this.toolMaterial == PixelmonItemsTools.MOONSTONE || this.toolMaterial == PixelmonItemsTools.DAWNSTONE || this.toolMaterial == PixelmonItemsTools.FIRESTONE || this.toolMaterial == PixelmonItemsTools.WATERSTONE || this.toolMaterial == PixelmonItemsTools.DUSKSTONE) {
                return 6.0f;
            }
        }
        return 1.0f;
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        return ToolEffects.processEffect(playerIn.getHeldItem(hand), playerIn, worldIn, pos.up()) ? EnumActionResult.SUCCESS : EnumActionResult.FAIL;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        String info = I18n.translateToLocal("gui.shopkeeper.item." + this.getTranslationKey());
        if (!this.hasHideFlag(stack) && !info.startsWith("gui.shopkeeper.")) {
            tooltip.add(GuiScreen.isShiftKeyDown() ? info : I18n.translateToLocal("pixelmon.item.tooltip"));
        }
    }

    public boolean hasHideFlag(ItemStack stack) {
        return stack.hasTagCompound() && stack.getTagCompound().getBoolean("HideTooltip");
    }
}

