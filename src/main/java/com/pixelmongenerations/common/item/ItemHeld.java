/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AsOne;
import com.pixelmongenerations.common.entity.pixelmon.abilities.CheekPouch;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Klutz;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Unnerve;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.common.item.heldItems.ChoiceItem;
import com.pixelmongenerations.common.item.heldItems.EVAdjusting;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.common.item.heldItems.ItemBlackSludge;
import com.pixelmongenerations.common.item.heldItems.ItemFlameOrb;
import com.pixelmongenerations.common.item.heldItems.ItemIronBall;
import com.pixelmongenerations.common.item.heldItems.ItemLaggingTail;
import com.pixelmongenerations.common.item.heldItems.ItemRingTarget;
import com.pixelmongenerations.common.item.heldItems.ItemStickyBarb;
import com.pixelmongenerations.common.item.heldItems.ItemToxicOrb;
import com.pixelmongenerations.common.item.heldItems.NoItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public abstract class ItemHeld
extends PixelmonItem {
    private EnumHeldItems heldItemType;

    public ItemHeld(EnumHeldItems heldItemType, String itemName) {
        super(itemName);
        this.heldItemType = heldItemType;
        this.setCreativeTab(PixelmonCreativeTabs.held);
        this.canRepair = false;
    }

    public EnumHeldItems getHeldItemType() {
        return this.heldItemType;
    }

    public static boolean isItemOfType(ItemStack itemStack, EnumHeldItems type) {
        Item item;
        return itemStack != null && (item = itemStack.getItem()) instanceof ItemHeld && ((ItemHeld)item).heldItemType == type;
    }

    public boolean interact(EntityPixelmon pokemon, ItemStack itemstack, EntityPlayer player) {
        return false;
    }

    public void dealtDamage(PixelmonWrapper attacker, PixelmonWrapper defender, Attack attack, DamageTypeEnum damageType) {
    }

    public void tookDamage(PixelmonWrapper attacker, PixelmonWrapper defender, float damage, DamageTypeEnum damageType) {
    }

    public void onAttackUsed(PixelmonWrapper user, Attack attack) {
    }

    public double preProcessAttackUser(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, double damage) {
        return damage;
    }

    public double preProcessAttackTarget(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, double damage) {
        return damage;
    }

    public void postProcessAttackUser(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, float damage) {
    }

    public void postProcessAttackTarget(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, float damage) {
    }

    public double modifyDamageIncludeFixed(double damage, PixelmonWrapper attacker, PixelmonWrapper target, Attack attack) {
        return damage;
    }

    public void onMiss(PixelmonWrapper attacker, PixelmonWrapper target, Object cause) {
    }

    public boolean allowsIncomingAttack(PixelmonWrapper pokemon, PixelmonWrapper user, Attack a) {
        return true;
    }

    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
    }

    public void applySwitchOutEffect(PixelmonWrapper pw) {
    }

    public void onStatusAdded(PixelmonWrapper user, PixelmonWrapper opponent, StatusBase status) {
    }

    public void onStatModified(PixelmonWrapper affected) {
    }

    public int adjustCritStage(PixelmonWrapper user) {
        return 0;
    }

    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
    }

    public void applyRepeatedEffectAfterStatus(PixelmonWrapper pokemon) {
    }

    public void applyEffectOnContact(PixelmonWrapper pokemon, PixelmonWrapper opponent) {
    }

    public void applyAfterBattleEffect() {
    }

    public List<EnumType> getEffectiveTypes(PixelmonWrapper user, PixelmonWrapper target) {
        return target.type;
    }

    public void eatBerry(PixelmonWrapper user) {
        if (user.getBattleAbility() instanceof CheekPouch && !user.hasFullHealth()) {
            user.healByPercent(33.0f);
            user.bc.sendToAll("pixelmon.abilities.cheekpouch", user.getNickname());
        }
        user.eatenBerry = true;
    }

    public static boolean canEatBerry(PixelmonWrapper user) {
        if (user.bc == null) {
            return true;
        }
        if (!ItemHeld.canUseItem(user)) {
            return false;
        }
        ArrayList<PixelmonWrapper> opponents = user.bc.getOpponentPokemon(user.getParticipant());
        for (PixelmonWrapper wrapper : opponents) {
            if (!(wrapper.getBattleAbility() instanceof Unnerve) && !(wrapper.getBattleAbility() instanceof AsOne)) continue;
            return false;
        }
        return true;
    }

    public int[] modifyPowerAndAccuracyUser(int[] modifiedMoveStats, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return modifiedMoveStats;
    }

    public int[] modifyPowerAndAccuracyTarget(int[] modifiedMoveStats, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        return modifiedMoveStats;
    }

    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        return stats;
    }

    public void onStatIncrease() {
    }

    public void onStatDecrease(PixelmonWrapper user, PixelmonWrapper target, int amount, StatsType stat) {
    }

    public float modifyPriority(PixelmonWrapper pokemon, float priority) {
        return priority;
    }

    public boolean affectMultiturnMove(PixelmonWrapper user) {
        return false;
    }

    public float modifyWeight(float initWeight) {
        return initWeight;
    }

    public boolean hasNegativeEffect() {
        return this instanceof ChoiceItem || this instanceof EVAdjusting || this instanceof ItemBlackSludge || this instanceof ItemFlameOrb || this instanceof ItemIronBall || this instanceof ItemLaggingTail || this instanceof ItemRingTarget || this instanceof ItemStickyBarb || this instanceof ItemToxicOrb;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public static boolean canUseItem(PixelmonWrapper user) {
        if (user.getHeldItem() == null) return false;
        if (user.getBattleAbility() instanceof Klutz) return false;
        if (user.hasStatus(StatusType.Embargo)) return false;
        if (user.bc.globalStatusController.hasStatus(StatusType.MagicRoom)) return false;
        return true;
    }

    public boolean isBerry() {
        return this instanceof ItemBerry;
    }

    public static void writeHeldItemToNBT(NBTTagCompound nbt, ItemStack heldItem) {
        Item item = null;
        if (heldItem != null) {
            item = heldItem.getItem();
        }
        if (item instanceof ItemHeld) {
            nbt.setTag("HeldItemStack", heldItem.writeToNBT(new NBTTagCompound()));
        } else {
            nbt.removeTag("HeldItemStack");
        }
    }

    public static ItemStack readHeldItemFromNBT(NBTTagCompound nbt) {
        ItemStack heldItem = null;
        if (nbt.hasKey("HeldItemName") && !nbt.getString("HeldItemName").isEmpty()) {
            heldItem = new ItemStack(Item.getByNameOrId(nbt.getString("HeldItemName")));
        }
        if (nbt.hasKey("HeldItemStack")) {
            heldItem = new ItemStack((NBTTagCompound)nbt.getTag("HeldItemStack"));
        }
        if (heldItem != null && !(heldItem.getItem() instanceof ItemHeld)) {
            heldItem = null;
        }
        return heldItem;
    }

    public static ItemHeld getItemHeld(ItemStack itemStack) {
        if (itemStack != null && itemStack != ItemStack.EMPTY) {
            Item item = itemStack.getItem();
            return item instanceof ItemHeld ? (ItemHeld)item : NoItem.noItem;
        }
        return NoItem.noItem;
    }
}

