/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Intimidated;
import com.pixelmongenerations.common.battle.status.Mist;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.battle.EnumBattleItems;

public class ItemBattleItem
extends PixelmonItem {
    public EnumBattleItems type;

    public ItemBattleItem(EnumBattleItems type, String itemName) {
        super(itemName);
        this.type = type;
        this.setMaxStackSize(16);
        this.setCreativeTab(PixelmonCreativeTabs.restoration);
        this.canRepair = false;
    }

    public void increaseStats(PixelmonWrapper pxm, PixelmonWrapper target) {
        if (this.type == EnumBattleItems.maxMushrooms) {
            StatsType.getMainTypes().forEach(type -> pxm.getBattleStats().modifyStat(1, (StatsType)((Object)type)));
        } else if (this.type == EnumBattleItems.guardSpec) {
            Mist.doMistEffect(pxm, false);
        } else if (this.type == EnumBattleItems.adrenalineOrb) {
            Intimidated.becomeIntimidated(target);
        } else if (this.type.effectType != StatsType.None) {
            pxm.getBattleStats().modifyStat(1, this.type.effectType);
        } else if (pxm.getBattleStats().getCritStage() == 0) {
            pxm.getBattleStats().increaseCritStage(1);
        }
    }

    @Override
    public boolean useFromBag(PixelmonWrapper userWrapper, PixelmonWrapper targetWrapper) {
        this.increaseStats(userWrapper, targetWrapper);
        return super.useFromBag(userWrapper, targetWrapper);
    }
}

