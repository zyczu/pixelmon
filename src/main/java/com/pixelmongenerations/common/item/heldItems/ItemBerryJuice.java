/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.common.item.HealFixed;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.MedicinePotion;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class ItemBerryJuice
extends ItemHeld {
    private MedicinePotion healMethod = new MedicinePotion(new HealFixed(20));

    public ItemBerryJuice(String itemName) {
        super(EnumHeldItems.berryJuice, itemName);
    }

    @Override
    public boolean interact(EntityPixelmon pokemon, ItemStack itemstack, EntityPlayer player) {
        return this.healMethod.useMedicine(new EntityLink(pokemon));
    }

    @Override
    public void tookDamage(PixelmonWrapper attacker, PixelmonWrapper pokemon, float damage, DamageTypeEnum damageType) {
        if (pokemon.isAlive() && pokemon.getHealthPercent() <= 50.0f) {
            this.consumeItem(pokemon);
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.getHealthPercent() <= 50.0f) {
            this.consumeItem(newPokemon);
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        this.applySwitchInEffect(pw);
    }

    private void consumeItem(PixelmonWrapper pokemon) {
        if (this.healPokemon(pokemon)) {
            pokemon.consumeItem();
        }
    }

    @Override
    public boolean useFromBag(PixelmonWrapper userWrapper, PixelmonWrapper targetWrapper) {
        if (!this.healPokemon(userWrapper)) {
            userWrapper.bc.sendToAll("pixelmon.general.noeffect", new Object[0]);
        }
        return super.useFromBag(userWrapper, targetWrapper);
    }

    public boolean healPokemon(PixelmonWrapper pokemon) {
        if (this.healMethod.useMedicine(new WrapperLink(pokemon))) {
            String nickname = pokemon.getNickname();
            pokemon.bc.sendToAll("pixelmon.helditems.consumerestorehp", nickname, this.getLocalizedName());
            return true;
        }
        return false;
    }
}

