/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemDragonToothScale
extends ItemHeld {
    public ItemDragonToothScale(EnumHeldItems heldItemType, String itemName) {
        super(heldItemType, itemName);
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        int n = this.getHeldItemType() == EnumHeldItems.deepSeaScale ? StatsType.SpecialDefence.getStatIndex() : StatsType.SpecialAttack.getStatIndex();
        stats[n] = stats[n] * 2;
        return stats;
    }
}

