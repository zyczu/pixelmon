/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.heldItems.ItemRaiseStat;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemCellBattery
extends ItemRaiseStat {
    public ItemCellBattery() {
        super(EnumHeldItems.cellbattery, "cell_battery", StatsType.Attack, EnumType.Electric);
    }
}

