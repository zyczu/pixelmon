/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemLightClay
extends ItemHeld {
    public ItemLightClay() {
        super(EnumHeldItems.lightClay, "light_clay");
    }
}

