/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SheerForce;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemLifeOrb
extends ItemHeld {
    public ItemLifeOrb() {
        super(EnumHeldItems.lifeorb, "life_orb");
    }

    @Override
    public double preProcessAttackUser(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, double damage) {
        if (damage > 0.0) {
            return damage * 1.3;
        }
        return damage;
    }

    @Override
    public void dealtDamage(PixelmonWrapper attacker, PixelmonWrapper defender, Attack attack, DamageTypeEnum damageType) {
        AbilityBase attackerAbility = attacker.getBattleAbility();
        if (attack != null && damageType == DamageTypeEnum.ATTACK && !attacker.isFainted()) {
            boolean doesRecoil;
            boolean bl = doesRecoil = !(attackerAbility instanceof MagicGuard || !defender.isFainted() && attacker.inParentalBond || attackerAbility instanceof SheerForce && !attack.getAttackBase().hasSecondaryEffect());
            if (doesRecoil) {
                int recoil = attacker.getPercentMaxHealth(10.0f);
                attacker.bc.sendToAll("pixelmon.helditem.lifeowner", attacker.getNickname());
                attacker.doBattleDamage(attacker, recoil, DamageTypeEnum.ITEM);
            }
        }
    }
}

