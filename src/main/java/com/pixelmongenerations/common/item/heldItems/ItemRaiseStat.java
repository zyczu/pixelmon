/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemRaiseStat
extends ItemHeld {
    private StatsType stat;
    private EnumType moveType;

    public ItemRaiseStat(EnumHeldItems heldItemType, String name, StatsType stat, EnumType moveType) {
        super(heldItemType, name);
        this.stat = stat;
        this.moveType = moveType;
    }

    @Override
    public void postProcessAttackTarget(PixelmonWrapper attacker, PixelmonWrapper holder, Attack attack, float damage) {
        if (attack.getAttackBase().attackType == this.moveType && damage > 0.0f && holder.isAlive()) {
            holder.bc.sendToAll("pixelmon.abilities.activated", holder.getNickname(), this.getLocalizedName());
            holder.getBattleStats().modifyStat(1, this.stat);
            holder.consumeItem();
        }
    }
}

