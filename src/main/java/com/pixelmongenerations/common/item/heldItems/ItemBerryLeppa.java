/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.Moveset;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.common.item.ItemEther;
import com.pixelmongenerations.common.item.ItemPPRestore;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.network.packetHandlers.battles.OpenBattleMode;
import java.util.Iterator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;

public class ItemBerryLeppa
extends ItemBerry {
    private Attack move;

    public ItemBerryLeppa() {
        super(EnumHeldItems.leppa, EnumBerry.Leppa, "leppa_berry");
    }

    @Override
    public boolean interact(EntityPixelmon pokemon, ItemStack itemstack, EntityPlayer player) {
        if (!pokemon.getMoveset().hasFullPP()) {
            Pixelmon.NETWORK.sendTo(new OpenBattleMode(BattleMode.ChooseEther, pokemon.getPartyPosition()), (EntityPlayerMP)player);
            return true;
        }
        return false;
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        this.activate(pokemon);
    }

    private void activate(PixelmonWrapper pokemon) {
        Moveset moveset = pokemon.getMoveset();
        for (Attack attack : moveset) {
            this.move = attack;
            if (this.move == null || this.move.pp > 0) continue;
            this.eatBerry(pokemon);
            break;
        }
        this.move = null;
    }

    @Override
    public void eatBerry(PixelmonWrapper helper) {
        if (ItemBerryLeppa.canEatBerry(helper)) {
            if (this.move == null) {
                if (helper.attack != null) {
                    this.move = helper.attack;
                }
                if (this.move == null) {
                    return;
                }
            }
            ItemPPRestore.restorePP((PokemonLink)new WrapperLink(helper), this.move, false);
            helper.consumeItem();
            helper.bc.sendToAll("pixelmon.helditems.consumeleppa", helper.getNickname(), this.move.getAttackBase().getLocalizedName());
            super.eatBerry(helper);
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        this.activate(newPokemon);
    }

    @Override
    public boolean useFromBag(PixelmonWrapper userWrapper, PixelmonWrapper targetWrapper, int selectedMove) {
        return ((ItemEther)PixelmonItems.ether).useFromBag(userWrapper, targetWrapper, selectedMove);
    }
}

