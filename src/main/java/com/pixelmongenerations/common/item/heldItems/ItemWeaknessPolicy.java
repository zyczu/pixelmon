/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemWeaknessPolicy
extends ItemHeld {
    public ItemWeaknessPolicy() {
        super(EnumHeldItems.weaknessPolicy, "weakness_policy");
    }

    @Override
    public void postProcessAttackTarget(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, float damage) {
        if (damage > 0.0f && attack.getTypeEffectiveness(attacker, target) >= 2.0) {
            target.bc.sendToAll("pixelmon.abilities.activated", target.getNickname(), target.getHeldItem().getLocalizedName());
            target.getBattleStats().modifyStat(2, StatsType.Attack, StatsType.SpecialAttack);
            target.consumeItem();
        }
    }
}

