/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.heldItems.HeldItem;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemBlunderPolicy
extends HeldItem {
    public ItemBlunderPolicy() {
        super(EnumHeldItems.blunderPolicy, "blunder_policy");
    }

    @Override
    public void onMiss(PixelmonWrapper attacker, PixelmonWrapper target, Object cause) {
        if (cause == null) {
            if (!attacker.attack.isAttack("Fissure", "Guillotine", "Horn Drill", "Sheer Cold") && !attacker.inMultipleHit) {
                attacker.getBattleStats().increaseStat(2, StatsType.Speed, attacker, false, true);
            }
        }
    }
}

