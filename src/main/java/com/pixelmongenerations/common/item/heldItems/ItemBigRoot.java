/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemBigRoot
extends ItemHeld {
    public ItemBigRoot() {
        super(EnumHeldItems.bigRoot, "big_root");
    }
}

