/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemBerryEnigma
extends ItemBerry {
    public ItemBerryEnigma() {
        super(EnumHeldItems.berryEnigma, EnumBerry.Enigma, "enigma_berry");
    }

    @Override
    public void tookDamage(PixelmonWrapper attacker, PixelmonWrapper defender, float damage, DamageTypeEnum damageType) {
        if (damage > 0.0f && damageType == DamageTypeEnum.ATTACK && attacker.attack.getTypeEffectiveness(attacker, defender) >= 2.0 && ItemBerryEnigma.canEatBerry(defender)) {
            this.eatBerry(defender);
        }
    }

    @Override
    public void eatBerry(PixelmonWrapper pokemon) {
        if (ItemBerryEnigma.canEatBerry(pokemon) && !pokemon.hasFullHealth()) {
            pokemon.bc.sendToAll("pixelmon.helditems.consumerestorehp", pokemon.getNickname(), this.getLocalizedName());
            pokemon.healByPercent(25.0f);
            super.eatBerry(pokemon);
            pokemon.consumeItem();
        }
    }
}

