/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemLeftovers
extends ItemHeld {
    public ItemLeftovers() {
        super(EnumHeldItems.leftovers, "leftovers");
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        if (!pw.hasFullHealth()) {
            if (!pw.hasStatus(StatusType.HealBlock)) {
                int par1 = (int)((float)pw.getMaxHealth() * 0.0625f);
                pw.healEntityBy(par1);
                if (pw.bc != null) {
                    pw.bc.sendToAll("pixelmon.helditems.leftovers", pw.getNickname());
                }
            }
        }
    }
}

