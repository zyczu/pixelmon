/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemWhiteHerb
extends ItemHeld {
    public ItemWhiteHerb() {
        super(EnumHeldItems.whiteHerb, "white_herb");
    }

    @Override
    public void onStatModified(PixelmonWrapper affected) {
        if (ItemWhiteHerb.healStats(affected)) {
            affected.consumeItem();
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        this.onStatModified(newPokemon);
    }

    @Override
    public boolean useFromBag(PixelmonWrapper userWrapper, PixelmonWrapper targetWrapper) {
        if (!ItemWhiteHerb.healStats(userWrapper)) {
            userWrapper.bc.sendToAll("pixelmon.general.noeffect", new Object[0]);
        }
        return super.useFromBag(userWrapper, targetWrapper);
    }

    public static boolean healStats(PixelmonWrapper affected) {
        boolean activated = false;
        int[] stages = affected.getBattleStats().getStages();
        for (int i = 0; i < 7; ++i) {
            if (stages[i] >= 0) continue;
            affected.getBattleStats().resetStat(i);
            activated = true;
        }
        if (activated) {
            affected.bc.sendToAll("pixelmon.helditems.whiteherb", affected.getNickname());
        }
        return activated;
    }
}

