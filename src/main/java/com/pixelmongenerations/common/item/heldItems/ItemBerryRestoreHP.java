/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.Confusion;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Gluttony;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.WrapperLink;
import com.pixelmongenerations.common.item.HealFixed;
import com.pixelmongenerations.common.item.HealFraction;
import com.pixelmongenerations.common.item.IHealHP;
import com.pixelmongenerations.common.item.MedicinePotion;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.heldItems.EnumBerryRestoreHP;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class ItemBerryRestoreHP
extends ItemBerry {
    public EnumBerryRestoreHP berryType;
    public StatsType confusedStat;
    private MedicinePotion healMethod;

    public ItemBerryRestoreHP(EnumBerryRestoreHP berryType, EnumBerry berry, String itemName, StatsType confusedStat) {
        super(EnumHeldItems.berryRestoreHP, berry, itemName);
        this.berryType = berryType;
        this.confusedStat = confusedStat;
        this.healMethod = new MedicinePotion(this.getHealAmount());
    }

    @Override
    public boolean interact(EntityPixelmon pokemon, ItemStack itemstack, EntityPlayer player) {
        return this.healMethod.useMedicine(new EntityLink(pokemon));
    }

    @Override
    public void tookDamage(PixelmonWrapper attacker, PixelmonWrapper pokemon, float damage, DamageTypeEnum damageType) {
        int lowHealth;
        int n = lowHealth = this.confusedStat == null ? 50 : 25;
        if (lowHealth == 25 && pokemon.getBattleAbility() instanceof Gluttony) {
            lowHealth = 50;
        }
        if (pokemon.isAlive() && pokemon.getHealthPercent() <= (float)lowHealth) {
            this.eatBerry(pokemon);
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        int lowHealth;
        int n = lowHealth = this.confusedStat == null ? 50 : 25;
        if (lowHealth == 25 && newPokemon.getBattleAbility() instanceof Gluttony) {
            lowHealth = 50;
        }
        if (newPokemon.getHealthPercent() <= (float)lowHealth) {
            this.eatBerry(newPokemon);
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        this.applySwitchInEffect(pw);
    }

    @Override
    public void eatBerry(PixelmonWrapper pokemon) {
        if (ItemBerryRestoreHP.canEatBerry(pokemon) && this.healPokemon(pokemon)) {
            super.eatBerry(pokemon);
            pokemon.consumeItem();
        }
    }

    @Override
    public boolean useFromBag(PixelmonWrapper userWrapper, PixelmonWrapper targetWrapper) {
        if (!this.healPokemon(userWrapper)) {
            userWrapper.bc.sendToAll("pixelmon.general.noeffect", new Object[0]);
        }
        return super.useFromBag(userWrapper, targetWrapper);
    }

    public boolean healPokemon(PixelmonWrapper pokemon) {
        if (this.healMethod.useMedicine(new WrapperLink(pokemon))) {
            String nickname = pokemon.getNickname();
            pokemon.bc.sendToAll("pixelmon.helditems.consumerestorehp", nickname, this.getLocalizedName());
            if (pokemon.getNature().decreasedStat == this.confusedStat && pokemon.addStatus(new Confusion(), pokemon)) {
                pokemon.bc.sendToAll("pixelmon.effect.becameconfused", nickname);
            }
            return true;
        }
        return false;
    }

    public IHealHP getHealAmount() {
        switch (this.berryType) {
            case oranBerry: {
                return new HealFixed(10);
            }
            case sitrusBerry: {
                return new HealFraction(0.25f);
            }
            case aguavBerry: 
            case figyBerry: 
            case iapapaBerry: 
            case magoBerry: 
            case wikiBerry: {
                return new HealFraction(0.33f);
            }
        }
        return new HealFraction(0.5f);
    }
}

