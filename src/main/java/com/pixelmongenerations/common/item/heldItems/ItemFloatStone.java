/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemFloatStone
extends ItemHeld {
    public ItemFloatStone() {
        super(EnumHeldItems.floatStone, "float_stone");
    }

    @Override
    public float modifyWeight(float initWeight) {
        return initWeight / 2.0f;
    }
}

