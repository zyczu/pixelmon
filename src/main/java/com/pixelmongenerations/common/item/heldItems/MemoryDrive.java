/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class MemoryDrive
extends ItemHeld {
    private EnumType type;

    public MemoryDrive(String itemName, EnumType type) {
        super(EnumHeldItems.memoryDrives, itemName);
        this.type = type;
    }

    public EnumType getType() {
        return this.type;
    }
}

