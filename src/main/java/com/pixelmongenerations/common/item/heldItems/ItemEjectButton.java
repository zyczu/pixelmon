/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SheerForce;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.network.packetHandlers.battles.EnforcedSwitch;

public class ItemEjectButton
extends ItemHeld {
    public ItemEjectButton() {
        super(EnumHeldItems.ejectButton, "eject_button");
    }

    @Override
    public void postProcessAttackTarget(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, float damage) {
        if (!attacker.bc.simulateMode && damage > 0.0f && target.isAlive()) {
            if (attacker.getBattleAbility() instanceof SheerForce && !attack.getAttackBase().hasSecondaryEffect()) {
                return;
            }
            BattleParticipant targetParticipant = target.getParticipant();
            ParticipantType targetType = targetParticipant.getType();
            if (targetType == ParticipantType.WildPokemon || !targetParticipant.hasMorePokemonReserve()) {
                return;
            }
            this.setUpSwitch(target);
            if (targetType == ParticipantType.Player) {
                Pixelmon.NETWORK.sendTo(new EnforcedSwitch(target.bc.getPositionOfPokemon(target, targetParticipant)), target.getPlayerOwner());
            } else {
                target.bc.switchPokemon(target.getPokemonID(), target.getBattleAI().getNextSwitch(target), true);
            }
        }
    }

    private void setUpSwitch(PixelmonWrapper target) {
        target.bc.sendToAll("pixelmon.helditems.ejectbutton", target.getNickname());
        target.consumeItem();
        target.setUpSwitchMove();
    }
}

