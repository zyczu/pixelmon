/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.LongReach;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.ItemProtectivePads;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemRockyHelmet
extends ItemHeld {
    public ItemRockyHelmet() {
        super(EnumHeldItems.rockyHelmet, "rocky_helmet");
    }

    @Override
    public void applyEffectOnContact(PixelmonWrapper user, PixelmonWrapper target) {
        if (!(user.getBattleAbility() instanceof MagicGuard) && user.isAlive()) {
            if (user.hasHeldItem() && user.getHeldItem() instanceof ItemProtectivePads) {
                user.bc.sendToAll("pixelmon.effect.protectivepads", user.getNickname());
                return;
            }
            if (user.getBattleAbility() instanceof LongReach) {
                return;
            }
            user.bc.sendToAll("pixelmon.helditems.rockyhelmet", user.getNickname(), target.getNickname());
            user.doBattleDamage(target, user.getPercentMaxHealth(16.666666f), DamageTypeEnum.ITEM);
        }
    }
}

