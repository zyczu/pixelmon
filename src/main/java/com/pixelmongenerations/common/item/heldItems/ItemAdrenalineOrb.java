/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.item.heldItems.HeldItem;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemAdrenalineOrb
extends HeldItem {
    public ItemAdrenalineOrb() {
        super(EnumHeldItems.adrenalineOrb, "adrenaline_orb");
    }
}

