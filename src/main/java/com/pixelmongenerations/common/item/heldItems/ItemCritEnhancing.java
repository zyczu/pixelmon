/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemCritEnhancing
extends ItemHeld {
    private EnumSpecies pokemon;

    public ItemCritEnhancing(EnumHeldItems type, String name) {
        super(type, name);
    }

    @Override
    public int adjustCritStage(PixelmonWrapper user) {
        return user.getBaseStats().pokemon == this.pokemon ? 2 : 0;
    }
}

