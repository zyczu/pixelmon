/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.MagicGuard;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemBlackSludge
extends ItemHeld {
    public ItemBlackSludge() {
        super(EnumHeldItems.blackSludge, "black_sludge");
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        int damage = pw.getPercentMaxHealth(6.25f);
        if (pw.hasType(EnumType.Poison)) {
            if (pw.hasFullHealth()) {
                return;
            }
            pw.bc.sendToAll("pixelmon.helditems.blacksludge", pw.getNickname());
            pw.healEntityBy(damage);
        } else if (!(pw.getBattleAbility() instanceof MagicGuard)) {
            pw.bc.sendToAll("pixelmon.helditems.blacksludgepsn", pw.getNickname());
            pw.doBattleDamage(pw, damage * 2, DamageTypeEnum.ITEM);
        }
    }
}

