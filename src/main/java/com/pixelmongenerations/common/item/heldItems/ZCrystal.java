/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.attacks.AttackBase;
import com.pixelmongenerations.common.battle.attacks.RegularAttackBase;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.enums.heldItems.EnumZCrystals;
import java.util.Optional;

public class ZCrystal
extends ItemHeld {
    public EnumZCrystals crystalType;

    public ZCrystal(EnumZCrystals zCrystal, String itemName) {
        super(EnumHeldItems.zCrystals, itemName);
        this.crystalType = zCrystal;
    }

    public boolean canEffectMove(Attack move) {
        if (this.crystalType.getPokemon() == null || this.crystalType.getPokemon().isEmpty()) {
            AttackBase base = move.getAttackBase();
            return base.attackType == this.crystalType.getType();
        }
        return false;
    }

    public boolean canEffectMove(PokemonSpec userPokemon, Attack move) {
        if (this.crystalType.getPokemon() != null && !this.crystalType.getPokemon().isEmpty()) {
            for (PokemonSpec spec : this.crystalType.getPokemon()) {
                if (!userPokemon.name.equalsIgnoreCase(spec.name) || spec.form != null && !userPokemon.form.equals(spec.form) || !move.getAttackBase().isAttack(this.crystalType.getRequiredAttack())) continue;
                return true;
            }
        } else {
            return this.canEffectMove(move);
        }
        return false;
    }

    public boolean canBeAppliedToPokemon(PokemonSpec pokemon) {
        if (this.crystalType.getUltraBurstSpecs() != null && !this.crystalType.getUltraBurstSpecs().isEmpty()) {
            for (PokemonSpec spec : this.crystalType.getUltraBurstSpecs()) {
                if (!pokemon.name.equalsIgnoreCase(spec.name) || spec.form != null && !pokemon.form.equals(spec.form)) continue;
                return true;
            }
        }
        if (this.crystalType.getPokemon() != null && !this.crystalType.getPokemon().isEmpty()) {
            for (PokemonSpec spec : this.crystalType.getPokemon()) {
                if (!pokemon.name.equalsIgnoreCase(spec.name) || spec.form != null && !pokemon.form.equals(spec.form)) continue;
                return true;
            }
        } else {
            return true;
        }
        return false;
    }

    public Attack getUsableAttack(Attack usedMove) {
        return Optional.of(usedMove).map(Attack::getAttackBase).filter(RegularAttackBase.class::isInstance).map(RegularAttackBase.class::cast).flatMap(move -> move.getZAttackBase(this.crystalType.getKey())).map(Attack::new).orElse(usedMove);
    }
}

