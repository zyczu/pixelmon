/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemFocusSash
extends ItemHeld {
    public ItemFocusSash() {
        super(EnumHeldItems.focussash, "focus_sash");
    }

    @Override
    public double modifyDamageIncludeFixed(double damage, PixelmonWrapper attacker, PixelmonWrapper target, Attack attack) {
        if (target != null && attacker != null && damage >= (double)target.getHealth() && target.hasFullHealth()) {
            if (attacker.bc != null) {
                attacker.bc.sendToAll("pixelmon.helditems.focussash", target.getNickname());
            }
            target.consumeItem();
            return target.getHealth() - 1;
        }
        return damage;
    }
}

