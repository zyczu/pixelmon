/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemMentalHerb
extends ItemHeld {
    public ItemMentalHerb() {
        super(EnumHeldItems.mentalHerb, "mental_herb");
    }

    @Override
    public void onStatusAdded(PixelmonWrapper user, PixelmonWrapper opponent, StatusBase status) {
        if (this.healStatus(user)) {
            user.consumeItem();
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        this.onStatusAdded(newPokemon, newPokemon, null);
    }

    @Override
    public boolean useFromBag(PixelmonWrapper userWrapper, PixelmonWrapper targetWrapper) {
        if (!this.healStatus(userWrapper)) {
            userWrapper.bc.sendToAll("pixelmon.general.noeffect", new Object[0]);
        }
        return super.useFromBag(userWrapper, targetWrapper);
    }

    public boolean healStatus(PixelmonWrapper pokemon) {
        boolean used = pokemon.removeStatuses(false, StatusType.Infatuated, StatusType.Disable, StatusType.Encore, StatusType.Taunt);
        if (used) {
            pokemon.bc.sendToAll("pixelmon.helditems.mentalherb", pokemon.getNickname());
        }
        return used;
    }
}

