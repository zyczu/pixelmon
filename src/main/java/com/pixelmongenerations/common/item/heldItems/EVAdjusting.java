/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumEvAdjustingItems;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class EVAdjusting
extends ItemHeld {
    public EnumEvAdjustingItems type;

    public EVAdjusting(EnumEvAdjustingItems type, String itemName) {
        super(EnumHeldItems.evAdjusting, itemName);
        this.type = type;
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        int n = StatsType.Speed.getStatIndex();
        stats[n] = (int)((double)stats[n] * 0.5);
        return stats;
    }
}

