/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemBerryTypeReducing
extends ItemBerry {
    public EnumType typeReduced;

    public ItemBerryTypeReducing(String itemName, EnumBerry berry, EnumType typeReduced) {
        super(EnumHeldItems.berryTypeReducing, berry, itemName);
        this.typeReduced = typeReduced;
    }

    @Override
    public double preProcessAttackTarget(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, double damage) {
        if (ItemBerryTypeReducing.canEatBerry(target) && attack.getAttackBase().attackType == this.typeReduced && attack.getTypeEffectiveness(attacker, target) >= 2.0 || this.typeReduced == EnumType.Normal) {
            damage /= 2.0;
            target.bc.sendToAll("pixelmon.helditems.berrytypereducing", target.getNickname(), target.getHeldItem().getLocalizedName());
            super.eatBerry(target);
            target.consumeItem();
        }
        return damage;
    }

    @Override
    public void eatBerry(PixelmonWrapper user) {
    }
}

