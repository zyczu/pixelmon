/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemWideLens
extends ItemHeld {
    public ItemWideLens() {
        super(EnumHeldItems.wideLens, "wide_lens");
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        int n = StatsType.Accuracy.getStatIndex();
        stats[n] = (int)((double)stats[n] * 1.1);
        return stats;
    }
}

