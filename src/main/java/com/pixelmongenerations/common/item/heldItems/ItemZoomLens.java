/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemZoomLens
extends ItemHeld {
    public ItemZoomLens() {
        super(EnumHeldItems.zoomLens, "zoom_lens");
    }

    @Override
    public int[] modifyPowerAndAccuracyUser(int[] modifiedMoveStats, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (target.bc.battleLog.getActionForPokemon(target.bc.battleTurn, target) != null || target.attack == null) {
            modifiedMoveStats[1] = (int)((double)modifiedMoveStats[1] * 1.2);
        }
        return modifiedMoveStats;
    }
}

