/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class NoItem
extends ItemHeld {
    public static final NoItem noItem = new NoItem();

    private NoItem() {
        super(EnumHeldItems.other, "");
    }
}

