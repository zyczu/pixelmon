/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemAmuletCoin
extends ItemHeld {
    public ItemAmuletCoin(EnumHeldItems itemEnum, String itemName) {
        super(itemEnum, itemName);
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.getParticipant().getType() == ParticipantType.Player) {
            PlayerParticipant player = (PlayerParticipant)newPokemon.getParticipant();
            player.hasAmuletCoin = true;
        }
    }
}

