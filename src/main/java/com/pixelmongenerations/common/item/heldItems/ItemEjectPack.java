/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.TrainerParticipant;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.heldItems.HeldItem;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;
import com.pixelmongenerations.core.network.packetHandlers.battles.EnforcedSwitch;

public class ItemEjectPack
extends HeldItem {
    boolean statDecreaseThisTurn = false;

    public ItemEjectPack() {
        super(EnumHeldItems.ejectPack, "eject_pack");
    }

    @Override
    public void onStatDecrease(PixelmonWrapper attacker, PixelmonWrapper target, int amount, StatsType stat) {
        BattleParticipant targetParticipant;
        if (this.statDecreaseThisTurn) {
            return;
        }
        if (target.lastAttack != null) {
            target.lastAttack.isAttack("Parting Shot");
        }
        if (!(targetParticipant = target.getParticipant()).hasMorePokemonReserve()) {
            target.nextSwitchIsMove = false;
            return;
        }
        this.statDecreaseThisTurn = true;
        target.nextSwitchIsMove = true;
        if (targetParticipant instanceof TrainerParticipant) {
            this.setUpSwitch(target);
            target.bc.switchPokemon(target.getPokemonID(), target.getBattleAI().getNextSwitch(target), true);
        } else if (targetParticipant instanceof PlayerParticipant) {
            if (!target.bc.simulateMode) {
                this.setUpSwitch(target);
                Pixelmon.NETWORK.sendTo(new EnforcedSwitch(target.bc.getPositionOfPokemon(target, target.getParticipant())), target.getPlayerOwner());
            }
        } else {
            target.nextSwitchIsMove = false;
        }
    }

    private void setUpSwitch(PixelmonWrapper target) {
        if (!target.getParticipant().hasMorePokemonReserve()) {
            return;
        }
        target.bc.sendToAll("pixelmon.helditems.ejectpack", target.getNickname());
        target.setUpSwitchMove();
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        this.statDecreaseThisTurn = false;
    }
}

