/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.GrassyTerrain;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.ItemBerryStatIncrease;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemGrassySeed
extends ItemHeld {
    public ItemGrassySeed() {
        super(EnumHeldItems.grassyseed, "grassy_seed");
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.bc.globalStatusController.getTerrain() instanceof GrassyTerrain) {
            this.eatBerry(newPokemon);
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.bc.globalStatusController.getTerrain() instanceof GrassyTerrain) {
            this.eatBerry(newPokemon);
        }
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        if (user.bc.globalStatusController.getTerrain() instanceof GrassyTerrain) {
            this.eatBerry(user);
        }
        return stats;
    }

    @Override
    public void eatBerry(PixelmonWrapper pokemon) {
        if (ItemBerryStatIncrease.canEatBerry(pokemon)) {
            pokemon.bc.sendToAll("pixelmon.helditems.grassy_seed", pokemon.getNickname(), pokemon.getHeldItem().getLocalizedName());
            if (pokemon.bc.globalStatusController.getTerrain() instanceof GrassyTerrain) {
                pokemon.getBattleStats().modifyStat(1, StatsType.Defence);
                pokemon.consumeItem();
            }
        }
    }
}

