/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.DamageTypeEnum;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Gluttony;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.heldItems.EnumBerryStatIncrease;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemBerryStatIncrease
extends ItemBerry {
    public EnumBerryStatIncrease berryType;
    private StatsType stat;

    public ItemBerryStatIncrease(EnumBerryStatIncrease berryType, EnumBerry berry, String itemName, StatsType stat) {
        super(EnumHeldItems.berryStatIncrease, berry, itemName);
        this.berryType = berryType;
        this.stat = stat;
    }

    @Override
    public void tookDamage(PixelmonWrapper attacker, PixelmonWrapper defender, float damage, DamageTypeEnum damageType) {
        int lowHealth;
        int n = lowHealth = defender.getBattleAbility() instanceof Gluttony ? 50 : 25;
        if (defender.isAlive() && defender.getHealthPercent() <= (float)lowHealth) {
            this.eatBerry(defender);
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.getHealthPercent() <= 25.0f) {
            this.eatBerry(newPokemon);
        }
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper pw) {
        this.applySwitchInEffect(pw);
    }

    @Override
    public void eatBerry(PixelmonWrapper pokemon) {
        if (ItemBerryStatIncrease.canEatBerry(pokemon)) {
            pokemon.bc.sendToAll("pixelmon.helditems.pinchberry", pokemon.getNickname(), pokemon.getHeldItem().getLocalizedName());
            if (this.stat != null) {
                pokemon.getBattleStats().modifyStat(1, this.stat);
            } else if (this.berryType == EnumBerryStatIncrease.starfBerry) {
                pokemon.getBattleStats().raiseRandomStat(2);
            } else if (this.berryType == EnumBerryStatIncrease.lansatBerry) {
                pokemon.getBattleStats().increaseCritStage(2);
            }
            super.eatBerry(pokemon);
            pokemon.consumeItem();
        }
    }
}

