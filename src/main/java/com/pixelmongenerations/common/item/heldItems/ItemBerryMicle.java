/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.item.heldItems.ItemBerry;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemBerryMicle
extends ItemBerry {
    public ItemBerryMicle() {
        super(EnumHeldItems.berryMicle, EnumBerry.Micle, "micle_berry");
    }

    @Override
    public int[] modifyPowerAndAccuracyUser(int[] modifiedMoveStats, PixelmonWrapper user, PixelmonWrapper target, Attack a) {
        if (modifiedMoveStats[1] > 0 && user.getHealthPercent() <= 25.0f && ItemBerryMicle.canEatBerry(user)) {
            user.bc.sendToAll("pixelmon.helditems.micleberry", user.getNickname());
            modifiedMoveStats[1] = (int)((double)modifiedMoveStats[1] * 1.2);
            this.eatBerry(user);
            user.consumeItem();
        }
        return modifiedMoveStats;
    }
}

