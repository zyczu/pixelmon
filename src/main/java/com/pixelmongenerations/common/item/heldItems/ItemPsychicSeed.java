/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.PsychicTerrain;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.common.item.heldItems.ItemBerryStatIncrease;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemPsychicSeed
extends ItemHeld {
    public ItemPsychicSeed() {
        super(EnumHeldItems.psychicseed, "psychic_seed");
    }

    @Override
    public void applyRepeatedEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.bc.globalStatusController.getTerrain() instanceof PsychicTerrain) {
            this.eatBerry(newPokemon);
        }
    }

    @Override
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.bc.globalStatusController.getTerrain() instanceof PsychicTerrain) {
            this.eatBerry(newPokemon);
        }
    }

    @Override
    public int[] modifyStats(PixelmonWrapper user, int[] stats) {
        if (user.bc.globalStatusController.getTerrain() instanceof PsychicTerrain) {
            this.eatBerry(user);
        }
        return stats;
    }

    @Override
    public void eatBerry(PixelmonWrapper pokemon) {
        if (ItemBerryStatIncrease.canEatBerry(pokemon)) {
            pokemon.bc.sendToAll("pixelmon.helditems.psychic_seed", pokemon.getNickname(), pokemon.getHeldItem().getLocalizedName());
            if (pokemon.bc.globalStatusController.getTerrain() instanceof PsychicTerrain) {
                pokemon.getBattleStats().modifyStat(1, StatsType.SpecialDefence);
                pokemon.consumeItem();
            }
        }
    }
}

