/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.heldItems;

import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.ParticipantType;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.abilities.Disguise;
import com.pixelmongenerations.common.entity.pixelmon.abilities.IceFace;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SheerForce;
import com.pixelmongenerations.common.entity.pixelmon.abilities.SuctionCups;
import com.pixelmongenerations.common.item.ItemHeld;
import com.pixelmongenerations.core.enums.heldItems.EnumHeldItems;

public class ItemRedCard
extends ItemHeld {
    public ItemRedCard() {
        super(EnumHeldItems.redCard, "red_card");
    }

    @Override
    public void postProcessAttackTarget(PixelmonWrapper attacker, PixelmonWrapper target, Attack attack, float damage) {
        PixelmonWrapper randomPokemon;
        if (attacker.bc.simulateMode || attacker.getParticipant().getType() == ParticipantType.WildPokemon || attacker.getBattleAbility() instanceof SuctionCups) {
            return;
        }
        if (attacker.getBattleAbility() instanceof SheerForce && !attack.getAttackBase().hasSecondaryEffect()) {
            return;
        }
        if (attacker.isDynamaxed()) {
            return;
        }
        if (damage > 0.0f) {
            if (!attacker.hasStatus(StatusType.Ingrain) && (randomPokemon = attacker.getParticipant().getRandomPartyPokemon()) != null) {
                target.consumeItem();
                attacker.bc.sendToAll("pixelmon.helditems.redcard", target.getNickname());
                attacker.bc.sendToAll("pixelmon.effect.flee", attacker.getNickname());
                attacker.forceRandomSwitch(randomPokemon.getPokemonID());
                return;
            }
        }
        if ((target.getBattleAbility() instanceof Disguise || target.getBattleAbility() instanceof IceFace) && (randomPokemon = attacker.getParticipant().getRandomPartyPokemon()) != null && attacker.attack.movePower > 0) {
            target.consumeItem();
            attacker.bc.sendToAll("pixelmon.helditems.redcard", target.getNickname());
            attacker.bc.sendToAll("pixelmon.effect.flee", attacker.getNickname());
            attacker.forceRandomSwitch(randomPokemon.getPokemonID());
        }
    }
}

