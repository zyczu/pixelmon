/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.armor.armoreffects;

import com.pixelmongenerations.common.item.armor.GenericArmor;
import com.pixelmongenerations.common.item.armor.armoreffects.IArmorEffect;
import com.pixelmongenerations.core.config.PixelmonItemsTools;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Enchantments;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class LeafstoneEffect
implements IArmorEffect {
    @Override
    public void onArmorTick(World world, EntityPlayer player, ItemStack stack, GenericArmor armor) {
        if (world.isRemote) {
            return;
        }
        player.getItemStackFromSlot(EntityEquipmentSlot.FEET);
        ItemStack boots = player.getItemStackFromSlot(EntityEquipmentSlot.FEET);
        if (boots.getItem() == armor && armor.material == PixelmonItemsTools.LEAFSTONEARMORMAT) {
            if (IArmorEffect.isWearingFullSet(player, armor.material)) {
                armor.equippedSet = true;
                if (!stack.isItemEnchanted()) {
                    stack.addEnchantment(Enchantments.FEATHER_FALLING, 3);
                    stack.addEnchantment(Enchantments.THORNS, 3);
                }
            } else {
                armor.equippedSet = false;
            }
        }
    }
}

