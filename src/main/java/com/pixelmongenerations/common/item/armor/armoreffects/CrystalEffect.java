/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.armor.armoreffects;

import com.pixelmongenerations.common.item.armor.GenericArmor;
import com.pixelmongenerations.common.item.armor.armoreffects.IArmorEffect;
import com.pixelmongenerations.core.config.PixelmonItemsTools;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class CrystalEffect
implements IArmorEffect {
    @Override
    public void onArmorTick(World world, EntityPlayer player, ItemStack stack, GenericArmor armor) {
        if (world.isRemote) {
            return;
        }
        ItemStack boots = player.getItemStackFromSlot(EntityEquipmentSlot.FEET);
        if (boots.getItem() == armor && armor.material == PixelmonItemsTools.CRYSTALARMORMAT) {
            if (IArmorEffect.isWearingFullSet(player, armor.material)) {
                armor.equippedSet = true;
                player.addPotionEffect(new PotionEffect(MobEffects.SPEED, 20));
                player.addPotionEffect(new PotionEffect(MobEffects.JUMP_BOOST, 20));
            } else {
                armor.equippedSet = false;
            }
        }
    }
}

