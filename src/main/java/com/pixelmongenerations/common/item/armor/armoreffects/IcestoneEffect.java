/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.armor.armoreffects;

import com.pixelmongenerations.common.item.armor.GenericArmor;
import com.pixelmongenerations.common.item.armor.armoreffects.IArmorEffect;
import com.pixelmongenerations.core.config.PixelmonItemsTools;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Enchantments;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class IcestoneEffect
implements IArmorEffect {
    @Override
    public void onArmorTick(World world, EntityPlayer player, ItemStack stack, GenericArmor armor) {
        if (world.isRemote) {
            return;
        }
        ItemStack boots = player.getItemStackFromSlot(EntityEquipmentSlot.FEET);
        if (boots.getItem() == armor && armor.getArmorMaterial() == PixelmonItemsTools.ICESTONEAMORMAT) {
            if (IArmorEffect.isWearingFullSet(player, armor.getArmorMaterial())) {
                armor.equippedSet = true;
                if (!stack.isItemEnchanted()) {
                    stack.addEnchantment(Enchantments.FROST_WALKER, 2);
                }
            } else {
                armor.equippedSet = false;
            }
        }
    }
}

