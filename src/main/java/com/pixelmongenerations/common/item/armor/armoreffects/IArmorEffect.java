/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.armor.armoreffects;

import com.pixelmongenerations.api.events.player.PlayerArmorEffectEvent;
import com.pixelmongenerations.common.item.armor.GenericArmor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public interface IArmorEffect {
    public void onArmorTick(World var1, EntityPlayer var2, ItemStack var3, GenericArmor var4);

    public static boolean isWearingFullSet(EntityPlayer player, ItemArmor.ArmorMaterial material) {
        ItemStack legs = player.getItemStackFromSlot(EntityEquipmentSlot.LEGS);
        ItemStack chest = player.getItemStackFromSlot(EntityEquipmentSlot.CHEST);
        ItemStack head = player.getItemStackFromSlot(EntityEquipmentSlot.HEAD);
        if (legs.getItem() instanceof GenericArmor && ((GenericArmor)legs.getItem()).material == material && chest.getItem() instanceof GenericArmor && ((GenericArmor)chest.getItem()).material == material && head.getItem() instanceof GenericArmor && ((GenericArmor)head.getItem()).material == material) {
            PlayerArmorEffectEvent event = new PlayerArmorEffectEvent(player, material);
            MinecraftForge.EVENT_BUS.post(event);
            return !event.isCanceled();
        }
        return false;
    }
}

