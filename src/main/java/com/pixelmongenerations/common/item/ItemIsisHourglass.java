/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.EnumIsisHourglassType;
import com.pixelmongenerations.common.item.PixelmonItem;
import net.minecraft.creativetab.CreativeTabs;

public class ItemIsisHourglass
extends PixelmonItem {
    public EnumIsisHourglassType type;

    public ItemIsisHourglass(EnumIsisHourglassType type) {
        super("hourglass_" + type.toString().toLowerCase());
        this.type = type;
        this.setCreativeTab(CreativeTabs.TOOLS);
    }
}

