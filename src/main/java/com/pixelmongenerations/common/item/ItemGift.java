/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.api.enums.ReceiveType;
import com.pixelmongenerations.api.events.PixelmonReceivedEvent;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.time.LocalDate;
import java.time.Month;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class ItemGift
extends PixelmonItem {
    public ItemGift(String name) {
        super(name);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        Optional<PlayerStorage> optstorage;
        ItemStack itemstack = player.getHeldItem(hand);
        if (!world.isRemote && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player)).isPresent()) {
            PlayerStorage storage = optstorage.get();
            if (!storage.playerData.giftOpened && this.isChristmas()) {
                if (player.getRNG().nextFloat() <= 0.8f) {
                    String randomName = EnumSpecies.randomPoke((boolean)PixelmonConfig.allowRandomSpawnedEggsToBeLegendary).name;
                    EntityPixelmon pokemon = (EntityPixelmon)PixelmonEntityList.createEntityByName(randomName, player.world);
                    pokemon.makeEntityIntoEgg();
                    MinecraftForge.EVENT_BUS.post(new PixelmonReceivedEvent((EntityPlayerMP)player, ReceiveType.Christmas, pokemon));
                    storage.addToParty(pokemon);
                } else {
                    ItemStack stack = new ItemStack(Items.COAL);
                    boolean boolAddedToInventory = player.inventory.addItemStackToInventory(stack);
                    if (!boolAddedToInventory && stack.getItemDamage() == 0) {
                        player.dropItem(stack.getItem(), 1);
                        String itemName = stack.getTranslationKey();
                        ChatHandler.sendFormattedChat(player, TextFormatting.RED, "pixelmon.drops.fullinventory", I18n.translateToLocal(itemName + ".name"));
                    }
                }
                ChatHandler.sendChat(player, "christmas.ischristmas", new Object[0]);
                player.inventory.clearMatchingItems(itemstack.getItem(), itemstack.getMetadata(), 1, itemstack.getTagCompound());
                storage.playerData.giftOpened = true;
            } else if (storage.playerData.giftOpened) {
                ChatHandler.sendChat(player, "christmas.alreadygifted", new Object[0]);
            } else {
                ChatHandler.sendChat(player, "christmas.notchristmas", new Object[0]);
            }
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemstack);
    }

    private boolean isChristmas() {
        LocalDate localDate = LocalDate.now();
        return localDate.getMonth() == Month.DECEMBER && localDate.getDayOfMonth() >= 25;
    }
}

