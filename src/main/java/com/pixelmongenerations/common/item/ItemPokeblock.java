/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.enums.items.EnumPokeblock;

public class ItemPokeblock
extends PixelmonItem {
    public ItemPokeblock(EnumPokeblock pokeblock) {
        super(pokeblock.name().toLowerCase());
        this.setCreativeTab(PixelmonCreativeTabs.natural);
    }
}

