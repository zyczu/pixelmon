/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.item.IHealHP;

public class HealFixed
implements IHealHP {
    private int healAmount;

    public HealFixed(int healAmount) {
        this.healAmount = healAmount;
    }

    @Override
    public int getHealAmount(PokemonLink pokemon) {
        return this.healAmount;
    }
}

