/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;

public class PorygonPiece
extends PixelmonItem {
    public PorygonPiece(String name) {
        super(name);
        this.setCreativeTab(CreativeTabs.MISC);
        this.setHasSubtypes(true);
        this.setMaxDamage(0);
    }

    @Override
    public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
        if (stack.getItemDamage() == 0) {
            stack.setCount(0);
            if (!worldIn.isRemote) {
                EntityPixelmon pixelmonEntity = (EntityPixelmon)PixelmonEntityList.createEntityByName(EnumSpecies.Porygon.name, worldIn);
                pixelmonEntity.setPosition(entityIn.posX, entityIn.posY + 2.0, entityIn.posZ);
                worldIn.spawnEntity(pixelmonEntity);
            }
        }
    }

    @Override
    public int getItemStackLimit(ItemStack stack) {
        return stack.getItemDamage() == 0 ? 1 : 8;
    }

    @Override
    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
        if (this.isInCreativeTab(tab)) {
            for (int i = 0; i < 5; ++i) {
                items.add(new ItemStack(this, 1, i));
            }
        }
    }

    public String getTranslationKey(ItemStack stack) {
        if (stack.getItemDamage() > 0) {
            if (stack.getItemDamage() == 1) {
                return super.getTranslationKey(stack) + "_head";
            }
            if (stack.getItemDamage() == 2) {
                return super.getTranslationKey(stack) + "_body";
            }
            if (stack.getItemDamage() == 3) {
                return super.getTranslationKey(stack) + "_leg";
            }
            if (stack.getItemDamage() == 4) {
                return super.getTranslationKey(stack) + "_tail";
            }
        }
        return super.getTranslationKey(stack);
    }
}

