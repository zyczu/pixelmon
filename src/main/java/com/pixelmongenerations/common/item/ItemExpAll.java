/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.network.ChatHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemExpAll
extends PixelmonItem {
    public ItemExpAll(String name) {
        super(name);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        ItemStack itemStackIn = player.getHeldItem(hand);
        if (!world.isRemote) {
            NBTTagCompound nbt;
            boolean activated = false;
            if (itemStackIn.hasTagCompound()) {
                nbt = itemStackIn.getTagCompound();
                activated = nbt.getBoolean("Activated");
            } else {
                nbt = new NBTTagCompound();
                itemStackIn.setTagCompound(nbt);
            }
            nbt.setBoolean("Activated", !activated);
            if (activated) {
                ChatHandler.sendChat(player, "pixelmon.items.expalloff", new Object[0]);
            } else {
                ChatHandler.sendChat(player, "pixelmon.items.expallon", new Object[0]);
            }
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemStackIn);
    }
}

