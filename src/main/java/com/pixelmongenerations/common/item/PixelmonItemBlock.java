/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.block.IBlockHasOwner;
import com.pixelmongenerations.common.block.MultiBlock;
import com.pixelmongenerations.common.block.enums.EnumMultiPos;
import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSnow;
import net.minecraft.block.BlockStairs;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.BlockSnapshot;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PixelmonItemBlock
extends ItemBlock {
    private CreativeTabs tabToDisplayOn;

    public PixelmonItemBlock(Block block) {
        super(block);
    }

    public PixelmonItemBlock(Block block, String name) {
        super(block);
        this.setTranslationKey(name);
        this.setRegistryName(name);
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        ItemStack stack = player.getHeldItem(hand);
        IBlockState iBlockState = worldIn.getBlockState(pos);
        Block placedOn = iBlockState.getBlock();
        if (placedOn == Blocks.SNOW_LAYER && iBlockState.getValue(BlockSnow.LAYERS) < 1) {
            facing = EnumFacing.UP;
        } else if (!placedOn.isReplaceable(worldIn, pos)) {
            pos = pos.offset(facing);
        }
        if (stack.getCount() == 0) {
            return EnumActionResult.FAIL;
        }
        if (!player.canPlayerEdit(pos, facing, stack)) {
            return EnumActionResult.FAIL;
        }
        if (pos.getY() == 255 && iBlockState.getMaterial().isSolid()) {
            return EnumActionResult.FAIL;
        }
        int i = MathHelper.floor((double)(player.rotationYaw * 4.0f / 360.0f) + 0.5) & 3;
        EnumFacing rot = EnumFacing.byHorizontalIndex((int)i);
        if (this.block instanceof MultiBlock) {
            MultiBlock mb = (MultiBlock)this.block;
            if (!PixelmonItemBlock.canPlace(pos, rot, worldIn, mb, player, stack, placedOn)) {
                return EnumActionResult.FAIL;
            }
            IBlockState iblockstate1 = this.block.getStateForPlacement(worldIn, pos, facing, hitX, hitY, hitZ, 0, player, hand);
            this.placeBlock(stack, iblockstate1, player, worldIn, pos);
            SoundType soundtype = iblockstate1.getBlock().getSoundType(iblockstate1, worldIn, pos, player);
            worldIn.playSound(player, pos, soundtype.getPlaceSound(), SoundCategory.BLOCKS, (soundtype.getVolume() + 1.0f) / 2.0f, soundtype.getPitch() * 0.8f);
            PixelmonItemBlock.setMultiBlocksWidth(pos, rot, worldIn, mb, this.block, player);
        } else {
            int meta = this.getMetadata(stack.getMetadata());
            if (this.block instanceof BlockStairs) {
                rot = facing;
            }
            IBlockState iblockstate1 = this.block.getStateForPlacement(worldIn, pos, rot, hitX, hitY, hitZ, meta, player, hand);
            BlockEvent.PlaceEvent placeEvent = new BlockEvent.PlaceEvent(new BlockSnapshot(worldIn, pos, iblockstate1), iBlockState, player, hand);
            MinecraftForge.EVENT_BUS.post(placeEvent);
            if (placeEvent.isCanceled()) {
                return EnumActionResult.FAIL;
            }
            if (this.placeBlock(stack, iblockstate1, player, worldIn, pos)) {
                SoundType soundtype = iblockstate1.getBlock().getSoundType(iblockstate1, worldIn, pos, player);
                worldIn.playSound(player, pos, soundtype.getPlaceSound(), SoundCategory.BLOCKS, (soundtype.getVolume() + 1.0f) / 2.0f, soundtype.getPitch() * 0.8f);
            }
        }
        if (this.block instanceof IBlockHasOwner) {
            ((IBlockHasOwner)((Object)this.block)).setOwner(pos, player);
        }
        if (player.capabilities.isCreativeMode) {
            return EnumActionResult.SUCCESS;
        }
        stack.shrink(1);
        return EnumActionResult.SUCCESS;
    }

    private boolean placeBlock(ItemStack stack, IBlockState newState, EntityPlayer player, World world, BlockPos pos) {
        if (!world.setBlockState(pos, newState, 3)) {
            return false;
        }
        IBlockState state = world.getBlockState(pos);
        if (state.getBlock() == this.block) {
            PixelmonItemBlock.setTileEntityNBT(world, player, pos, stack);
            this.block.onBlockPlacedBy(world, pos, state, player, stack);
        }
        return true;
    }

    @Override
    public boolean canPlaceBlockOnSide(World worldIn, BlockPos pos, EnumFacing side, EntityPlayer player, ItemStack stack) {
        if (this.block instanceof MultiBlock) {
            MultiBlock mb = (MultiBlock)this.block;
            BlockPos pos2 = pos.offset(side);
            return PixelmonItemBlock.canPlace(pos2, player.getHorizontalFacing(), worldIn, mb, player, stack, worldIn.getBlockState(pos2).getBlock());
        }
        return super.canPlaceBlockOnSide(worldIn, pos, side, player, stack);
    }

    public static boolean canPlace(BlockPos pos, EnumFacing rot, World world, MultiBlock mb, EntityPlayer player, ItemStack stack, Block block) {
        if (rot == EnumFacing.EAST) {
            for (int l = 0; l < mb.width; ++l) {
                if (PixelmonItemBlock.canPlaceLength(pos, 0, l, rot, world, mb, player, stack, block)) continue;
                return false;
            }
        } else if (rot == EnumFacing.NORTH) {
            for (int l = 0; l < mb.width; ++l) {
                if (PixelmonItemBlock.canPlaceLength(pos, l, 0, rot, world, mb, player, stack, block)) continue;
                return false;
            }
        } else if (rot == EnumFacing.WEST) {
            for (int l = 0; l < mb.width; ++l) {
                if (PixelmonItemBlock.canPlaceLength(pos, 0, -1 * l, rot, world, mb, player, stack, block)) continue;
                return false;
            }
        } else {
            for (int l = 0; l < mb.width; ++l) {
                if (PixelmonItemBlock.canPlaceLength(pos, -1 * l, 0, rot, world, mb, player, stack, block)) continue;
                return false;
            }
        }
        return true;
    }

    private static boolean canPlaceLength(BlockPos pos, int xd, int zd, EnumFacing rot, World world, MultiBlock mb, EntityPlayer player, ItemStack stack, Block block) {
        if (rot == EnumFacing.EAST) {
            for (int w = 0; w < mb.length; ++w) {
                if (PixelmonItemBlock.canPlaceHeight(pos, xd + w, zd, world, mb, player, stack, block)) continue;
                return false;
            }
        } else if (rot == EnumFacing.NORTH) {
            for (int w = 0; w < mb.length; ++w) {
                if (PixelmonItemBlock.canPlaceHeight(pos, xd, zd - w, world, mb, player, stack, block)) continue;
                return false;
            }
        } else if (rot == EnumFacing.WEST) {
            for (int w = 0; w < mb.length; ++w) {
                if (PixelmonItemBlock.canPlaceHeight(pos, xd - w, zd, world, mb, player, stack, block)) continue;
                return false;
            }
        } else {
            for (int w = 0; w < mb.length; ++w) {
                if (PixelmonItemBlock.canPlaceHeight(pos, xd, zd + w, world, mb, player, stack, block)) continue;
                return false;
            }
        }
        return true;
    }

    private static boolean canPlaceHeight(BlockPos pos, int xd, int zd, World world, MultiBlock mb, EntityPlayer player, ItemStack stack, Block block) {
        int h = 0;
        while ((double)h < mb.height) {
            BlockPos p = new BlockPos(pos.getX() + xd, pos.getY() + h, pos.getZ() + zd);
            IBlockState iblockstate1 = block.getBlockState().getBaseState();
            BlockEvent.PlaceEvent placeEvent = new BlockEvent.PlaceEvent(new BlockSnapshot(world, p, iblockstate1), world.getBlockState(p.down()), player, EnumHand.MAIN_HAND);
            MinecraftForge.EVENT_BUS.post(placeEvent);
            if (placeEvent.isCanceled()) {
                return false;
            }
            IBlockState iblockstate = world.getBlockState(p);
            if (iblockstate.getMaterial() != Material.AIR) {
                return false;
            }
            if (iblockstate.getMaterial() != Material.SNOW && iblockstate.getMaterial() != Material.GRASS && iblockstate.getMaterial() != Material.AIR) {
                return false;
            }
            ++h;
        }
        return true;
    }

    public static void setMultiBlocksWidth(BlockPos pos, EnumFacing rot, World world, MultiBlock mb, Block block, EntityPlayer player) {
        if (rot == EnumFacing.EAST) {
            for (int l = 0; l < mb.width; ++l) {
                PixelmonItemBlock.setMultiBlocksLength(pos, 0, l, rot, world, mb, block, player);
            }
        } else if (rot == EnumFacing.NORTH) {
            for (int l = 0; l < mb.width; ++l) {
                PixelmonItemBlock.setMultiBlocksLength(pos, l, 0, rot, world, mb, block, player);
            }
        } else if (rot == EnumFacing.WEST) {
            for (int l = 0; l < mb.width; ++l) {
                PixelmonItemBlock.setMultiBlocksLength(pos, 0, -1 * l, rot, world, mb, block, player);
            }
        } else {
            for (int l = 0; l < mb.width; ++l) {
                PixelmonItemBlock.setMultiBlocksLength(pos, -1 * l, 0, rot, world, mb, block, player);
            }
        }
    }

    private static void setMultiBlocksLength(BlockPos pos, int xd, int zd, EnumFacing rot, World world, MultiBlock mb, Block block, EntityPlayer player) {
        if (rot == EnumFacing.EAST) {
            for (int w = 0; w < mb.length; ++w) {
                PixelmonItemBlock.setMultiBlocksHeight(pos, xd + w, zd, rot, world, mb, block, player);
            }
        } else if (rot == EnumFacing.NORTH) {
            for (int w = 0; w < mb.length; ++w) {
                PixelmonItemBlock.setMultiBlocksHeight(pos, xd, zd - w, rot, world, mb, block, player);
            }
        } else if (rot == EnumFacing.WEST) {
            for (int w = 0; w < mb.length; ++w) {
                PixelmonItemBlock.setMultiBlocksHeight(pos, xd - w, zd, rot, world, mb, block, player);
            }
        } else {
            for (int w = 0; w < mb.length; ++w) {
                PixelmonItemBlock.setMultiBlocksHeight(pos, xd, zd + w, rot, world, mb, block, player);
            }
        }
    }

    private static void setMultiBlocksHeight(BlockPos pos, int xd, int zd, EnumFacing rot, World world, MultiBlock mb, Block block, EntityPlayer player) {
        int h = 0;
        while ((double)h < mb.height) {
            BlockPos p = new BlockPos(pos.getX() + xd, pos.getY() + h, pos.getZ() + zd);
            EnumFacing facing = xd > 0 ? EnumFacing.EAST : (xd < 0 ? EnumFacing.WEST : (zd > 0 ? EnumFacing.SOUTH : (zd < 0 ? EnumFacing.NORTH : rot)));
            EnumMultiPos multiPos = xd == 0 && zd == 0 && h == 0 ? EnumMultiPos.BASE : (h == 0 ? EnumMultiPos.BOTTOM : EnumMultiPos.TOP);
            IBlockState iblockstate1 = block.getStateForPlacement(world, p, rot, 0.0f, 0.0f, 0.0f, 0, player, EnumHand.MAIN_HAND).withProperty(MultiBlock.MULTIPOS, multiPos).withProperty(MultiBlock.FACING, facing);
            world.setBlockState(p, iblockstate1, 2);
            ++h;
        }
    }

    @Override
    public Item setCreativeTab(CreativeTabs tab) {
        this.tabToDisplayOn = tab;
        return super.setCreativeTab(tab);
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public CreativeTabs getCreativeTab() {
        return this.block.getCreativeTab();
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        String info = I18n.translateToLocal("gui.shopkeeper." + this.getTranslationKey());
        if (!this.hasHideFlag(stack) && !info.startsWith("gui.shopkeeper.")) {
            tooltip.add(GuiScreen.isShiftKeyDown() ? info : "Hold shift for more info.");
        }
    }

    public boolean hasHideFlag(ItemStack stack) {
        return stack.hasTagCompound() && stack.getTagCompound().getBoolean("HideTooltip");
    }
}

