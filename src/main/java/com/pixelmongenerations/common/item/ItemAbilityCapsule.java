/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import net.minecraft.entity.player.EntityPlayer;

public class ItemAbilityCapsule
extends PixelmonItem {
    public ItemAbilityCapsule() {
        super("ability_capsule");
    }

    public boolean useOnEntity(EntityPixelmon pixelmon, EntityPlayer player) {
        block2: {
            int slot;
            block4: {
                block3: {
                    if (pixelmon.isPokemon(EnumSpecies.Zygarde)) break block2;
                    slot = pixelmon.getAbilitySlot();
                    if (slot == 2 || pixelmon.baseStats.abilities[1] == null) break block3;
                    if (!pixelmon.isPokemon(EnumSpecies.Greninja)) break block4;
                }
                ChatHandler.sendChat(player, "pixelmon.interaction.noeffect", new Object[0]);
                return false;
            }
            int newSlot = slot == 1 ? 0 : 1;
            pixelmon.setAbilitySlot(newSlot);
            pixelmon.setAbility(pixelmon.baseStats.abilities[newSlot]);
            pixelmon.update(EnumUpdateType.Ability);
            ChatHandler.sendChat(player, "pixelmon.interaction.abilitycapsule", pixelmon.getNickname(), pixelmon.getAbility().getLocalizedName());
            return true;
        }
        ChatHandler.sendChat(player, "pixelmon.interaction.noeffect", new Object[0]);
        return false;
    }
}

