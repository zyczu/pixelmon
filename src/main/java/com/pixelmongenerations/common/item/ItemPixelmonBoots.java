/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.HashMultimap
 *  com.google.common.collect.Multimap
 */
package com.pixelmongenerations.common.item;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.pixelmongenerations.core.config.PixelmonItemsTools;
import java.util.UUID;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class ItemPixelmonBoots
extends ItemArmor {
    private static UUID runningShoesUUID = UUID.fromString("B7060ADF-8FAF-4C0F-B816-87CB5721979F");
    private static AttributeModifier oldRunningShoesModifier = new AttributeModifier(runningShoesUUID, SharedMonsterAttributes.MOVEMENT_SPEED.getName(), 0.5, 1);
    private static AttributeModifier newRunningShoesModifier = new AttributeModifier(runningShoesUUID, SharedMonsterAttributes.MOVEMENT_SPEED.getName(), 0.75, 1);

    public ItemPixelmonBoots(ItemArmor.ArmorMaterial enumArmorMaterial, int k, EntityEquipmentSlot l, String itemName) {
        super(enumArmorMaterial, k, l);
        this.setMaxDamage(1000);
        this.setCreativeTab(CreativeTabs.COMBAT);
        this.setTranslationKey(itemName);
        this.setRegistryName(itemName);
    }

    @Override
    public Multimap<String, AttributeModifier> getAttributeModifiers(EntityEquipmentSlot slot, ItemStack stack) {
        HashMultimap o = HashMultimap.create();
        if (slot == EntityEquipmentSlot.FEET) {
            AttributeModifier modifier = this == PixelmonItemsTools.oldRunningShoes ? oldRunningShoesModifier : newRunningShoesModifier;
            o.put((Object)SharedMonsterAttributes.MOVEMENT_SPEED.getName(), (Object)modifier);
        }
        return o;
    }

    @Override
    public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack) {
        if (!world.isRemote && player.getItemStackFromSlot(EntityEquipmentSlot.FEET) == itemStack) {
            Item item = itemStack.getItem();
            if (item == PixelmonItemsTools.newRunningShoes) {
                if (player.isCreative() || player.isSpectator() || player.isRiding() || player.isElytraFlying()) {
                    return;
                }
                int maxDamage = PixelmonItemsTools.newRunningShoes.getMaxDamage(itemStack);
                if (itemStack.getItemDamage() >= maxDamage) {
                    player.setItemStackToSlot(EntityEquipmentSlot.FEET, new ItemStack(PixelmonItemsTools.oldRunningShoes));
                } else {
                    double currentX = player.posX;
                    double currentZ = player.posZ;
                    NBTTagCompound compound = itemStack.getOrCreateSubCompound("pos");
                    if (!compound.hasKey("x")) {
                        compound.setDouble("x", currentX);
                        compound.setDouble("z", currentZ);
                        return;
                    }
                    double bootLastX = compound.getDouble("x");
                    double bootLastZ = compound.getDouble("z");
                    double changeX = Math.abs(bootLastX - currentX);
                    double changeZ = Math.abs(bootLastZ - currentZ);
                    if (changeX >= 2.0 || changeZ >= 2.0) {
                        itemStack.setItemDamage(itemStack.getItemDamage() + 1);
                        compound.setDouble("x", currentX);
                        compound.setDouble("z", currentZ);
                    }
                }
            } else if (item == PixelmonItemsTools.oldRunningShoes && itemStack.isItemDamaged()) {
                itemStack.setItemDamage(0);
            }
        }
    }

    @Override
    public String getArmorTexture(ItemStack stack, Entity entity, EntityEquipmentSlot slot, String type) {
        if (stack.getItem() == PixelmonItemsTools.newRunningShoes) {
            return "pixelmon:textures/models/armor/running_1.png";
        }
        return "pixelmon:textures/models/armor/oldrunning_1.png";
    }
}

