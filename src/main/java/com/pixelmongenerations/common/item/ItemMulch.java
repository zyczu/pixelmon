/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.enums.EnumMulch;

public class ItemMulch
extends PixelmonItem {
    private final EnumMulch mulch;

    public ItemMulch(EnumMulch mulch) {
        super(mulch.getName() + "_mulch");
        this.mulch = mulch;
    }

    public EnumMulch getMulch() {
        return this.mulch;
    }
}

