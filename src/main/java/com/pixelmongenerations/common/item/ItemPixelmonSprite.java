/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.pixelmon.Entity1Base;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.util.helper.SpriteHelper;
import java.util.Optional;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.translation.I18n;

public class ItemPixelmonSprite
extends PixelmonItem {
    public ItemPixelmonSprite() {
        super("pixelmon_sprite");
        this.setCreativeTab(null);
    }

    public static ItemStack getPhoto(EntityPixelmon pixelmon) {
        return ItemPixelmonSprite.getPhoto(new EntityLink(pixelmon));
    }

    public static ItemStack getPhoto(PokemonLink pixelmon) {
        ItemStack itemStack = new ItemStack(PixelmonItems.itemPixelmonSprite);
        Optional<BaseStats> stats = Entity3HasStats.getBaseStats(pixelmon.getBaseStats().pixelmonName);
        if (stats.isPresent()) {
            NBTTagCompound tagCompound = new NBTTagCompound();
            String displayName = Entity1Base.getLocalizedName(pixelmon.getBaseStats().pixelmonName);
            String filePath = "pixelmon:sprites/";
            int specialTexture = pixelmon.getSpecialTexture();
            IEnumForm form = pixelmon.getSpecies().getFormEnum(pixelmon.getForm());
            IEnumSpecialTexture texture = pixelmon.getSpecies().getSpecialTexture(form, pixelmon.getSpecialTexture());
            displayName = form.format(displayName);
            displayName = texture.format(displayName);
            if (pixelmon.isShiny() && texture.hasShinyVariant()) {
                displayName = "Shiny " + displayName;
                filePath = filePath + "shiny";
            }
            displayName = displayName + " " + I18n.translateToLocal(PixelmonItems.itemPixelmonSprite.getTranslationKey() + ".name");
            filePath = filePath + "pokemon/" + String.format("%03d", stats.get().nationalPokedexNumber) + SpriteHelper.getSpriteExtra(pixelmon.getBaseStats().pixelmonName, pixelmon.getForm(), pixelmon.getGender(), specialTexture, null);
            tagCompound.setString("SpriteName", filePath);
            itemStack.setTagCompound(tagCompound);
            itemStack.setStackDisplayName(displayName);
        }
        return itemStack;
    }
}

