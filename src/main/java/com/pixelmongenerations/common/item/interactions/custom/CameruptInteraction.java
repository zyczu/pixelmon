/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions.custom;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.drops.DropItemHelper;
import com.pixelmongenerations.common.item.interactions.custom.PixelmonInteraction;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class CameruptInteraction
extends PixelmonInteraction {
    public CameruptInteraction(EntityPixelmon pixelmon) {
        super(pixelmon, 3);
    }

    @Override
    public boolean processInteract(EntityPixelmon entityPixelmon, EntityPlayer player, EnumHand hand, ItemStack stack) {
        if (this.getNumInteractions() == 0) {
            return false;
        }
        if (stack != null && stack.getItem() == Items.BUCKET) {
            stack.shrink(1);
            if (stack.getCount() <= 0) {
                player.inventory.setInventorySlotContents(player.inventory.currentItem, new ItemStack(Items.LAVA_BUCKET));
            } else {
                DropItemHelper.giveItemStackToPlayer(player, new ItemStack(Items.LAVA_BUCKET));
            }
            this.setNumInteractions(this.getNumInteractions() - 1);
            this.count = 0;
            return true;
        }
        return false;
    }
}

