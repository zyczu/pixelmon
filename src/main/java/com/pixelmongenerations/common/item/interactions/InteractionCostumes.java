/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumPikachu;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.packetHandlers.OpenReplaceMoveScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

public class InteractionCostumes
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        Item item = itemStack.getItem();
        if (!this.isCostume(item)) {
            return false;
        }
        if (pixelmon.getOwner() != player) {
            return false;
        }
        if (pixelmon.getSpecies() != EnumSpecies.Pikachu) {
            return false;
        }
        if (pixelmon.getGender() != Gender.Female) {
            String msg = "&eOnly a female Pikachu can use this item!";
            msg = msg.replace("&", "\u00a7");
            TextComponentString text = new TextComponentString(msg);
            player.sendMessage(text);
            return false;
        }
        EnumPikachu form = pixelmon.getForm() == 6 ? EnumPikachu.Libre : (pixelmon.getForm() != -1 ? EnumPikachu.values()[pixelmon.getForm()] : null);
        EnumPikachu newForm = EnumPikachu.getCosplayForm(item);
        if (form != null && newForm == form) {
            return false;
        }
        if (form == null || form.canCosplay()) {
            if (player.getHeldItemMainhand().getCount() == 1) {
                player.setHeldItem(EnumHand.MAIN_HAND, new ItemStack(newForm.getCosplayItem()));
            } else {
                ItemStack newItem = player.getHeldItemMainhand();
                newItem.setCount(newItem.getCount() - 1);
                player.setHeldItem(EnumHand.MAIN_HAND, newItem);
                player.addItemStackToInventory(new ItemStack(newForm.getCosplayItem()));
            }
            if (this.hasSpecialMove(pixelmon)) {
                for (int i = 0; i < pixelmon.getMoveset().size(); ++i) {
                    Attack attack = pixelmon.getMoveset().get(i);
                    if (attack == null || !this.isSpecial(attack)) continue;
                    TextComponentTranslation text = new TextComponentTranslation("replacemove.replace", pixelmon.getPokemonName(), attack.getAttackBase().getLocalizedName(), newForm.getCosplayAttack().getAttackBase().getLocalizedName());
                    text.getStyle().setColor(TextFormatting.GRAY);
                    player.sendMessage(text);
                    pixelmon.getMoveset().set(i, newForm.getCosplayAttack());
                    pixelmon.update(EnumUpdateType.Moveset);
                }
            } else if (pixelmon.getMoveset().size() == 4) {
                Pixelmon.NETWORK.sendTo(new OpenReplaceMoveScreen(player.getUniqueID(), pixelmon.getPokemonId(), newForm.getCosplayAttack().getAttackBase().attackIndex, 0, pixelmon.getLvl().getLevel(), false), (EntityPlayerMP)player);
            } else {
                pixelmon.getMoveset().add(newForm.getCosplayAttack());
                TextComponentTranslation text = new TextComponentTranslation("pixelmon.stats.learnedmove", newForm.getCosplayAttack().getAttackBase().getLocalizedName());
                text.getStyle().setColor(TextFormatting.GRAY);
                player.sendMessage(text);
            }
            pixelmon.setForm(newForm.getForm());
            player.playSound(SoundEvents.ENTITY_ITEM_PICKUP, 1.0f, 1.0f);
            return true;
        }
        return false;
    }

    private boolean isSpecial(Attack attack) {
        return attack.isAttack("Meteor Mash", "Icicle Crash", "Draining Kiss", "Electric Terrain", "Flying Press", "Thunder Shock");
    }

    private boolean hasSpecialMove(EntityPixelmon pixelmon) {
        return pixelmon.getMoveset().hasAttack("Meteor Mash", "Icicle Crash", "Draining Kiss", "Electric Terrain", "Flying Press", "Thunder Shock");
    }

    private boolean isCostume(Item item) {
        return item == PixelmonItems.belleCostume || item == PixelmonItems.libreCostume || item == PixelmonItems.phdCostume || item == PixelmonItems.popStarCostume || item == PixelmonItems.rockStarCostume;
    }
}

