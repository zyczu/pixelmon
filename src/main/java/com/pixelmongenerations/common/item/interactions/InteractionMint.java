/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemPokeMint;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import javax.annotation.Nullable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class InteractionMint
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        Item item = itemStack.getItem();
        if (pixelmon.getOwner() == player && item instanceof ItemPokeMint) {
            itemStack.shrink(1);
            Optional<PlayerStorage> storageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
            if (storageOpt.isPresent()) {
                PlayerStorage storage = storageOpt.get();
                pixelmon.getEntityData().setBoolean("Minted", true);
                pixelmon.setPseudoNature(((ItemPokeMint)item).getMintType());
                storage.update(pixelmon, EnumUpdateType.PseudoNature);
                ChatHandler.sendChat(player, ChatHandler.getMessage("pixelmon.stats.mint", pixelmon.getName()));
            }
        }
        return false;
    }

    @Nullable
    public PlayerStorage getPlayerStorage(EntityPlayerMP player) {
        return PixelmonStorage.pokeBallManager.getPlayerStorage(player).orElse(null);
    }

    public static void updatePartyPokemon(PlayerStorage storage, EntityPixelmon pixelmon) {
        storage.updateNBT(pixelmon);
    }
}

