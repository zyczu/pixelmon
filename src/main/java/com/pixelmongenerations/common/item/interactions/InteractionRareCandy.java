/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.events.RareCandyEvent;
import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.enums.EnumExpSource;
import com.pixelmongenerations.core.network.EnumUpdateType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.common.MinecraftForge;

public class InteractionRareCandy
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon entityPixelmon, EntityPlayer player, EnumHand hand, ItemStack itemstack) {
        if (player instanceof EntityPlayerMP && entityPixelmon.getOwner() == player && !entityPixelmon.isInRanchBlock && itemstack.getItem() == PixelmonItems.rareCandy) {
            if (!entityPixelmon.doesLevel) {
                player.sendMessage(new TextComponentTranslation("pixelmon.interaction.rarecandy", entityPixelmon.getNickname()));
                return false;
            }
            if (entityPixelmon.getLvl().getLevel() >= PixelmonServerConfig.maxLevel || MinecraftForge.EVENT_BUS.post(new RareCandyEvent((EntityPlayerMP)player, entityPixelmon))) {
                return false;
            }
            if (player.isSneaking() && PixelmonServerConfig.sneakingRareCandyLevelsDown) {
                int decreasedLevel = Math.max(1, entityPixelmon.getLvl().getLevel() - 1);
                entityPixelmon.getLvl().setLevel(decreasedLevel);
            } else {
                entityPixelmon.getLvl().awardEXP(entityPixelmon.getLvl().expToNextLevel - entityPixelmon.getLvl().getExp(), PixelmonConfig.rareCandyTriggersLevelEvent, EnumExpSource.RareCandy);
            }
            entityPixelmon.update(EnumUpdateType.Stats);
            if (!player.capabilities.isCreativeMode) {
                player.getHeldItem(hand).shrink(1);
            }
            return true;
        }
        return false;
    }
}

