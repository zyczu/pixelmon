/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Maps
 */
package com.pixelmongenerations.common.item.interactions.custom;

import com.google.common.collect.Maps;
import com.pixelmongenerations.common.entity.pixelmon.Entity8HoldsItems;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.interactions.custom.CameruptInteraction;
import com.pixelmongenerations.common.item.interactions.custom.MiltankInteraction;
import java.util.HashMap;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;

public abstract class PixelmonInteraction {
    public EntityPixelmon pixelmon;
    public int maxInteractions;
    public int maxCount;
    int count = 0;
    private static HashMap<String, Class<? extends PixelmonInteraction>> pixelmonInteractions = Maps.newHashMap();

    public PixelmonInteraction(EntityPixelmon pixelmon, int maxInteractions) {
        this.pixelmon = pixelmon;
        this.maxInteractions = maxInteractions;
        this.setNumInteractions(maxInteractions);
        this.maxCount = pixelmon.getRNG().nextInt(1200) + 800;
    }

    public abstract boolean processInteract(EntityPixelmon var1, EntityPlayer var2, EnumHand var3, ItemStack var4);

    public int getNumInteractions() {
        return this.pixelmon.getDataManager().get(EntityPixelmon.dwNumInteractions);
    }

    public void setNumInteractions(int newValue) {
        this.pixelmon.getDataManager().set(EntityPixelmon.dwNumInteractions, newValue);
    }

    public void tick() {
        if (!this.pixelmon.world.isRemote) {
            ++this.count;
            if (this.count > this.maxCount) {
                if (this.getNumInteractions() < this.maxInteractions) {
                    this.setNumInteractions(this.getNumInteractions() + 1);
                }
                this.count = 0;
                this.maxCount = this.pixelmon.getRNG().nextInt(600) + 400;
            }
        }
    }

    public void writeEntityToNBT(NBTTagCompound nbt) {
        nbt.setShort("NumInteractions", (short)this.getNumInteractions());
        nbt.setShort("InteractionCount", (short)this.count);
    }

    public void readEntityFromNBT(NBTTagCompound nbt) {
        if (nbt.hasKey("NumInteractions")) {
            this.setNumInteractions(nbt.getShort("NumInteractions"));
        }
        if (nbt.hasKey("InteractionCount")) {
            this.count = nbt.getShort("InteractionCount");
        }
    }

    public static PixelmonInteraction getInteraction(Entity8HoldsItems pixelmon) {
        if (!pixelmonInteractions.containsKey(pixelmon.getPokemonName())) {
            return null;
        }
        Class<? extends PixelmonInteraction> c = pixelmonInteractions.get(pixelmon.getPokemonName());
        try {
            return c.getConstructor(EntityPixelmon.class).newInstance(pixelmon);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static {
        pixelmonInteractions.put("Miltank", MiltankInteraction.class);
        pixelmonInteractions.put("Camerupt", CameruptInteraction.class);
    }
}

