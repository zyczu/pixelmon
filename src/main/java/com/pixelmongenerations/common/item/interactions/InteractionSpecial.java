/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class InteractionSpecial
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemstack) {
        if (itemstack.getItem() == Item.getItemFromBlock(Blocks.LIT_PUMPKIN)) {
            World world = pixelmon.world;
            if (pixelmon.baseStats.pokemon != EnumSpecies.Gyarados && EnumSpecies.zombieTextured.contains((Object)pixelmon.baseStats.pokemon) && world.canSeeSky(pixelmon.getPosition())) {
                long time = world.getWorldTime() % 192000L;
                if (time >= 22500L || time < 13500L) {
                    return false;
                }
                if (world.getLightFromNeighbors(pixelmon.getPosition()) > 11 && !world.isRemote) {
                    pixelmon.setSpecialTexture(1);
                    return true;
                }
            }
        }
        return false;
    }
}

