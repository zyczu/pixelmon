/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AuraBreak;
import com.pixelmongenerations.common.entity.pixelmon.abilities.PowerConstruct;
import com.pixelmongenerations.common.item.ItemZygardeCube;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class InteractionZygardeCube
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        Optional<PlayerStorage> oStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
        PlayerStorage storage = oStorage.get();
        Item item = itemStack.getItem();
        if (item instanceof ItemZygardeCube) {
            if (pixelmon.isPokemon(EnumSpecies.Zygarde) && !player.world.isRemote) {
                if (pixelmon.getOwner() != player) {
                    return false;
                }
                if (pixelmon.getForm() == 0 && itemStack.getItemDamage() <= 50 && pixelmon.getAbility() instanceof AuraBreak) {
                    this.deconstructZygarde(storage, pixelmon, itemStack, player, 50);
                    return true;
                }
                if (pixelmon.getForm() == 0 && itemStack.getItemDamage() <= 100 && pixelmon.getAbility() instanceof PowerConstruct) {
                    this.deconstructZygarde(storage, pixelmon, itemStack, player, 100);
                    return true;
                }
                if (pixelmon.getForm() == 1 && itemStack.getItemDamage() <= 90) {
                    this.deconstructZygarde(storage, pixelmon, itemStack, player, 10);
                    return true;
                }
                ChatHandler.sendChat(player, "You do not have enough room in your Zygarde Cube to deconstruct this Zygarde!", new Object[0]);
                return false;
            }
        }
        return false;
    }

    private void deconstructZygarde(PlayerStorage storage, EntityPixelmon pixelmon, ItemStack itemStack, EntityPlayer player, int amount) {
        itemStack.setItemDamage(itemStack.getItemDamage() + amount);
        storage.recallAllPokemon();
        storage.removeFromPartyPlayer(storage.getPosition(pixelmon.getPokemonId()));
        storage.updateNBT(pixelmon);
        ChatHandler.sendChat(player, String.format("You deconstructed your Zygarde and received %s Cells!", amount), new Object[0]);
        pixelmon.setDead();
    }
}

