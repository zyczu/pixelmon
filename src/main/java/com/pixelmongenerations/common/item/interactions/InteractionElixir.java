/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.EntityLink;
import com.pixelmongenerations.common.item.ItemElixir;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class InteractionElixir
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemstack) {
        Item item;
        if (player instanceof EntityPlayerMP && pixelmon.getOwner() == player && !pixelmon.isInRanchBlock && (item = itemstack.getItem()) instanceof ItemElixir && ((ItemElixir)item).useElixir(new EntityLink(pixelmon)) && !player.capabilities.isCreativeMode) {
            player.getHeldItem(hand).shrink(1);
        }
        return false;
    }
}

