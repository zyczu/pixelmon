/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.common.item.interactions.InteractionNBlankizer;
import com.pixelmongenerations.core.config.PixelmonItems;

public class InteractionNSolarizer
extends InteractionNBlankizer {
    public InteractionNSolarizer() {
        super(PixelmonItems.nSolarizer, "Solgaleo", "Sunsteel Strike", 2);
    }
}

