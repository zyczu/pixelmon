/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.enums.ReceiveType;
import com.pixelmongenerations.api.events.PixelmonReceivedEvent;
import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumMissingNo;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelSounds;
import javax.annotation.Nullable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.common.MinecraftForge;

public class InteractionCorruptedGem
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        Item item = itemStack.getItem();
        if (pixelmon.getOwner() == null && item == PixelmonItems.corruptedGem) {
            if (pixelmon.isPokemon(EnumSpecies.MissingNo) && (pixelmon.getFormEnum() == EnumMissingNo.Aerodactyl || pixelmon.getFormEnum() == EnumMissingNo.Ghost || pixelmon.getFormEnum() == EnumMissingNo.Kabutops || pixelmon.getFormEnum() == EnumMissingNo.RedBlue || pixelmon.getFormEnum() == EnumMissingNo.Yellow)) {
                pixelmon.world.playSound(null, player.posX, player.posY, player.posZ, PixelSounds.pokelootObtained, SoundCategory.AMBIENT, 1.0f, 1.0f);
                pixelmon.setDead();
                player.getHeldItem(hand).shrink(1);
                PokemonSpec spec = PokemonSpec.from("MissingNo");
                spec.setForm(pixelmon.getForm());
                spec.boss = null;
                EntityPixelmon pokemon = spec.create(player.world);
                pokemon.caughtBall = pokemon.caughtBall == null ? EnumPokeball.PokeBall : pokemon.caughtBall;
                pokemon.friendship.initFromCapture();
                PlayerStorage storage = this.getPlayerStorage((EntityPlayerMP)player);
                if (storage != null) {
                    MinecraftForge.EVENT_BUS.post(new PixelmonReceivedEvent((EntityPlayerMP)player, ReceiveType.Command, pokemon));
                    if (BattleRegistry.getBattle(player) == null) {
                        storage.addToParty(pokemon);
                    } else {
                        storage.addToPC(pokemon);
                    }
                }
            }
        }
        return false;
    }

    @Nullable
    public PlayerStorage getPlayerStorage(EntityPlayerMP player) {
        return PixelmonStorage.pokeBallManager.getPlayerStorage(player).orElse(null);
    }
}

