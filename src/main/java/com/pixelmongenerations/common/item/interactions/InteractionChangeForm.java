/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.item.interactions;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.dialogue.Choice;
import com.pixelmongenerations.api.dialogue.Dialogue;
import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.database.DatabaseMoves;
import com.pixelmongenerations.core.enums.EnumRotomForm;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumDeoxys;
import com.pixelmongenerations.core.enums.forms.EnumOricorio;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.packetHandlers.OpenReplaceMoveScreen;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.ArrayList;
import javax.annotation.Nullable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.translation.I18n;

public class InteractionChangeForm
implements IInteraction {
    /*
     * Enabled aggressive block sorting
     */
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        if (pixelmon.getOwner() != player) return false;
        Item item = itemStack.getItem();
        if (item == PixelmonItems.meteorite) {
            if (pixelmon.isPokemon(EnumSpecies.Deoxys)) {
                int currentForm = pixelmon.getForm();
                pixelmon.setForm(EnumDeoxys.getNextIndex(currentForm));
                ChatHandler.sendChat(player, "pixelmon.abilities.changeform", pixelmon.getNickname());
                return true;
            }
        }
        if (item == PixelmonItems.gracidea) {
            if (pixelmon.isPokemon(EnumSpecies.Shaymin)) {
                if (!player.getEntityWorld().isDaytime()) {
                    return false;
                }
                if (pixelmon.getForm() == 1) return false;
                pixelmon.setForm(1, true);
                ChatHandler.sendChat(player, "pixelmon.abilities.changeform", pixelmon.getNickname());
                return true;
            }
        }
        if (item == Item.getItemFromBlock(PixelmonBlocks.prisonBottle)) {
            if (pixelmon.isPokemon(EnumSpecies.Hoopa)) {
                if (pixelmon.getForm() == 0) {
                    pixelmon.setForm(1);
                    pixelmon.getMoveset().replaceMove("Hyperspace Hole", new Attack("Hyperspace Fury"));
                    pixelmon.update(EnumUpdateType.Moveset);
                    pixelmon.getEntityData().setLong("unboundTime", System.currentTimeMillis());
                    ChatHandler.sendChat(player, "pixelmon.abilities.changeform", pixelmon.getNickname());
                    return true;
                }
                if (pixelmon.getForm() != 1) return false;
                pixelmon.setForm(0);
                pixelmon.getMoveset().replaceMove("Hyperspace Fury", new Attack("Hyperspace Hole"));
                pixelmon.update(EnumUpdateType.Moveset);
                pixelmon.getEntityData().removeTag("unboundTime");
                ChatHandler.sendChat(player, "pixelmon.abilities.changeform", pixelmon.getNickname());
                return true;
            }
        }
        if (item == Items.FISH) {
            if (pixelmon.isPokemon(EnumSpecies.Wishiwashi) && pixelmon.getForm() == 0 && pixelmon.getLvl().getLevel() >= 20) {
                pixelmon.setForm(1);
                ChatHandler.sendChat(player, "pixelmon.abilities.changeform", pixelmon.getNickname());
                return true;
            }
        }
        if (item instanceof ItemFood) {
            if (pixelmon.isPokemon(EnumSpecies.Morpeko)) {
                if ((item == Items.ROTTEN_FLESH || item == Items.POISONOUS_POTATO) && pixelmon.getForm() == 0) {
                    pixelmon.setForm(1);
                    player.getHeldItemMainhand().shrink(1);
                    ChatHandler.sendChat(player, "pixelmon.abilities.changeform", pixelmon.getNickname());
                    return true;
                }
                if (item == Items.ROTTEN_FLESH) return true;
                if (item == Items.POISONOUS_POTATO) return true;
                if (pixelmon.getForm() != 1) return true;
                pixelmon.setForm(0);
                player.getHeldItemMainhand().shrink(1);
                ChatHandler.sendChat(player, "pixelmon.abilities.changeform", pixelmon.getNickname());
                return true;
            }
        }
        if (item == PixelmonItems.reveal_glass) {
            if (pixelmon.isPokemon(EnumSpecies.Thundurus, EnumSpecies.Landorus, EnumSpecies.Tornadus, EnumSpecies.Enamorus)) {
                pixelmon.setForm(pixelmon.getForm() != 1 ? 1 : -1);
                ChatHandler.sendChat(player, "pixelmon.abilities.changeform", pixelmon.getNickname());
                return false;
            }
        }
        if (!pixelmon.isPokemon(EnumSpecies.Oricorio)) {
            if (item != PixelmonItems.rotomCatalog) return false;
            if (!pixelmon.isPokemon(EnumSpecies.Rotom)) return false;
            Dialogue.DialogueBuilder dialogue = Dialogue.builder();
            dialogue.setName(I18n.translateToLocal("item.rotom_catalog.name"));
            dialogue.setText(I18n.translateToLocal("pixelmon.items.rotomcatalogmenutext"));
            ArrayList<EnumRotomForm> rotomForms = Lists.newArrayList(EnumRotomForm.Default, EnumRotomForm.Heat, EnumRotomForm.Wash, EnumRotomForm.Frost, EnumRotomForm.Fan, EnumRotomForm.Mow);
            rotomForms.forEach(rotomForm -> dialogue.addChoice(new Choice(rotomForm.name(), event -> {
                ArrayList rotomMoves = Lists.newArrayList((Object[])new Attack[]{DatabaseMoves.getAttack("Thunder Shock"), DatabaseMoves.getAttack("Overheat"), DatabaseMoves.getAttack("Hydro Pump"), DatabaseMoves.getAttack("Blizzard"), DatabaseMoves.getAttack("Air Slash"), DatabaseMoves.getAttack("Leaf Storm")});
                Attack a = null;
                switch (rotomForm.getForm()) {
                    case 0: {
                        a = (Attack)rotomMoves.get(0);
                        break;
                    }
                    case 1: {
                        a = (Attack)rotomMoves.get(1);
                        break;
                    }
                    case 2: {
                        a = (Attack)rotomMoves.get(2);
                        break;
                    }
                    case 3: {
                        a = (Attack)rotomMoves.get(3);
                        break;
                    }
                    case 4: {
                        a = (Attack)rotomMoves.get(4);
                        break;
                    }
                    case 5: {
                        a = (Attack)rotomMoves.get(5);
                    }
                }
                pixelmon.getMoveset().removeIf(rotomMoves::contains);
                if (pixelmon.getMoveset().size() == 4) {
                    Pixelmon.NETWORK.sendTo(new OpenReplaceMoveScreen(player.getUniqueID(), pixelmon.getPokemonId(), a.getAttackBase().attackIndex, 0, pixelmon.getLvl().getLevel()), (EntityPlayerMP)player);
                } else {
                    pixelmon.getMoveset().add(a);
                }
                pixelmon.update(EnumUpdateType.Moveset);
                pixelmon.setForm(rotomForm.getForm());
            })));
            Dialogue.setPlayerDialogueData((EntityPlayerMP)player, Lists.newArrayList(dialogue.build()), true);
            return false;
        }
        if (item == PixelmonItems.pinkNectar && pixelmon.getForm() != EnumOricorio.Pau.getForm()) {
            pixelmon.setForm(EnumOricorio.Pau.getForm(), true);
            ChatHandler.sendChat(player, "pixelmon.abilities.changeform", pixelmon.getNickname());
            return true;
        }
        if (item == PixelmonItems.yellowNectar && pixelmon.getForm() != EnumOricorio.PomPom.getForm()) {
            pixelmon.setForm(EnumOricorio.PomPom.getForm(), true);
            ChatHandler.sendChat(player, "pixelmon.abilities.changeform", pixelmon.getNickname());
            return true;
        }
        if (item == PixelmonItems.purpleNectar && pixelmon.getForm() != EnumOricorio.Sensu.getForm()) {
            pixelmon.setForm(EnumOricorio.Sensu.getForm(), true);
            ChatHandler.sendChat(player, "pixelmon.abilities.changeform", pixelmon.getNickname());
            return true;
        }
        if (item != PixelmonItems.redNectar) return false;
        if (pixelmon.getForm() == EnumOricorio.Baile.getForm()) return false;
        pixelmon.setForm(EnumOricorio.Baile.getForm(), true);
        ChatHandler.sendChat(player, "pixelmon.abilities.changeform", pixelmon.getNickname());
        return true;
    }

    @Nullable
    public PlayerStorage getPlayerStorage(EntityPlayerMP player) {
        return PixelmonStorage.pokeBallManager.getPlayerStorage(player).orElse(null);
    }
}

