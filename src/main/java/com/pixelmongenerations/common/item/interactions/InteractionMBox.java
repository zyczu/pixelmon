/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.battle.BattleFactory;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemMeltanBox;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelSounds;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;

public class InteractionMBox
implements IInteraction {
    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        Item item;
        if (pixelmon.battleController != null) {
            return false;
        }
        Optional<PlayerStorage> oStorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
        PlayerStorage storage = oStorage.get();
        if (pixelmon.playerOwned) {
            if (!pixelmon.getOwnerId().toString().equalsIgnoreCase(player.getUniqueID().toString())) {
                return false;
            }
            PixelmonData data = new PixelmonData(pixelmon);
            if (data.isInRanch) {
                return false;
            }
        }
        if ((item = itemStack.getItem()) instanceof ItemMeltanBox) {
            if (pixelmon.isPokemon(EnumSpecies.Meltan) && itemStack.getItemDamage() < 49) {
                if (itemStack.getItemDamage() >= ItemMeltanBox.full || player.world.isRemote) return false;
                itemStack.setItemDamage(itemStack.getItemDamage() + 1);
                ChatHandler.sendChat(player, "You collected a Meltan!", new Object[0]);
                pixelmon.world.playSound(null, player.posX, player.posY, player.posZ, PixelSounds.pokelootObtained, SoundCategory.AMBIENT, 1.0f, 1.0f);
                pixelmon.setDead();
                if (!pixelmon.playerOwned) return false;
                for (int i = 0; i < 6; ++i) {
                    EntityPixelmon p = storage.getPokemon(storage.getIDFromPosition(i), player.world);
                    if (p == null || !p.getUniqueID().toString().equalsIgnoreCase(pixelmon.getUniqueID().toString())) continue;
                    storage.removeFromPartyPlayer(i);
                    return false;
                }
                return false;
            }
        }
        if (!(item instanceof ItemMeltanBox)) return false;
        if (!pixelmon.isPokemon(EnumSpecies.Meltan)) return false;
        if (itemStack.getItemDamage() == 49) {
            if (player.world.isRemote) return false;
            pixelmon.setDead();
            ChatHandler.sendChat(player, "A Melmetal appeared!", new Object[0]);
            EntityPixelmon pixelmonEntity = (EntityPixelmon)PixelmonEntityList.createEntityByName(EnumSpecies.Melmetal.name, player.world);
            itemStack.setItemDamage(itemStack.getItemDamage() - 49);
            Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player);
            if (!optstorage.isPresent()) return false;
            PlayerStorage wow = optstorage.get();
            if (storage.getFirstAblePokemon(player.world) == null) {
                pixelmonEntity.setPosition(player.posX, player.posY, player.posZ);
                player.world.spawnEntity(pixelmonEntity);
            }
            Attack a = new Attack("Thunder Punch");
            pixelmonEntity.getMoveset().add(a);
            pixelmonEntity.update(EnumUpdateType.Moveset);
            EntityPixelmon startingPixelmon = wow.getFirstAblePokemon(player.world);
            pixelmonEntity.setPosition(player.posX + 5.0, player.posY, player.posZ);
            player.world.spawnEntity(pixelmonEntity);
            BattleFactory.createBattle().team1(new PlayerParticipant((EntityPlayerMP)player, startingPixelmon)).team2(new WildPixelmonParticipant(true, pixelmonEntity)).startBattle();
            return false;
        } else {
            ChatHandler.sendChat(player, "This Meltan Box is full!", new Object[0]);
        }
        return false;
    }
}

