/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Lists
 */
package com.pixelmongenerations.common.item.interactions;

import com.google.common.collect.Lists;
import com.pixelmongenerations.api.dialogue.Choice;
import com.pixelmongenerations.api.dialogue.Dialogue;
import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.translation.I18n;

public class InteractionBottleCap
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        if (!(player instanceof EntityPlayerMP) || pixelmon.getOwner() == null || pixelmon.getOwner().getUniqueID() != player.getUniqueID()) {
            return false;
        }
        if (!pixelmon.stats.isBottleCapMaxed()) {
            if (itemStack.getItem() == PixelmonItems.bottleCap) {
                Dialogue.DialogueBuilder dialogue = Dialogue.builder();
                dialogue.setName(I18n.translateToLocal("item.bottle_cap.name"));
                dialogue.column(2);
                dialogue.setText(I18n.translateToLocal("pixelmon.items.bottlecapmenutext"));
                StatsType.getMainTypes().stream().filter(a -> !pixelmon.stats.isBottleCapIV((StatsType)((Object)a))).forEach(stat -> dialogue.addChoice(new Choice(stat.getLocalizedName(), event -> {
                    player.getHeldItem(hand).shrink(1);
                    pixelmon.stats.addBottleCapIV((StatsType)((Object)stat));
                    pixelmon.updateStats();
                    pixelmon.update(EnumUpdateType.Stats);
                    ChatHandler.sendChat(player, ChatHandler.getMessage("pixelmon.items.bottlecap", stat.getLocalizedName(), pixelmon.getName()));
                })));
                Dialogue.setPlayerDialogueData((EntityPlayerMP)player, Lists.newArrayList(dialogue.build()), true);
            } else if (itemStack.getItem() == PixelmonItems.goldBottleCap) {
                player.getHeldItem(hand).shrink(1);
                StatsType.getMainTypes().forEach(stat -> pixelmon.stats.addBottleCapIV((StatsType)((Object)stat)));
                pixelmon.updateStats();
                pixelmon.update(EnumUpdateType.Stats);
                ChatHandler.sendChat(player, ChatHandler.getMessage("pixelmon.items.goldbottlecap", pixelmon.getName()));
            }
        } else {
            ChatHandler.sendChat(player, ChatHandler.getMessage("pixelmon.stats.ivmaxed", pixelmon.getName()));
        }
        return false;
    }
}

