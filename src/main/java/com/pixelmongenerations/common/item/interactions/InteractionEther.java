/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemEther;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.battle.BattleMode;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.packetHandlers.battles.OpenBattleMode;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class InteractionEther
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon entityPixelmon, EntityPlayer player, EnumHand hand, ItemStack itemstack) {
        if (player instanceof EntityPlayerMP && entityPixelmon.getOwner() == player && !entityPixelmon.isInRanchBlock && itemstack.getItem() instanceof ItemEther) {
            if (!entityPixelmon.getMoveset().hasFullPP()) {
                Pixelmon.NETWORK.sendTo(new OpenBattleMode(BattleMode.ChooseEther, entityPixelmon.getPartyPosition()), (EntityPlayerMP)player);
                return true;
            }
            ChatHandler.sendChat(player, "pixelmon.interaction.ppfail", entityPixelmon.getNickname());
        }
        return false;
    }
}

