/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.ItemAbilityPatch;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class InteractionAbilityPatch
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        Item item;
        if (!pixelmon.isInRanchBlock && player instanceof EntityPlayerMP && pixelmon.getOwner() == player && (item = itemStack.getItem()) instanceof ItemAbilityPatch && ((ItemAbilityPatch)item).useOnEntity(pixelmon, player) && !player.capabilities.isCreativeMode) {
            player.getHeldItem(hand).shrink(1);
        }
        return false;
    }
}

