/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.api.interactions.IInteraction;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.network.EnumUpdateType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;

public class InteractionMaxSoup
implements IInteraction {
    @Override
    public boolean processInteract(EntityPixelmon pixelmon, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        Item item = itemStack.getItem();
        if (!pixelmon.isInRanchBlock && pixelmon.getOwner() == player && item == PixelmonItems.maxSoup && player.isServerWorld()) {
            EnumSpecies species = pixelmon.getSpecies();
            if (!species.hasGmaxForm()) {
                ChatHandler.sendChat(player, ChatHandler.getMessage("pixelmon.interaction.maxsoup.badspecies", pixelmon.getName()));
                return true;
            }
            if (species == EnumSpecies.Melmetal) {
                ChatHandler.sendChat(player, ChatHandler.getMessage("pixelmon.interaction.maxsoup.melmetal", pixelmon.getName()));
                return true;
            }
            if (!(species != EnumSpecies.Urshifu || itemStack.hasTagCompound() && itemStack.getTagCompound().getBoolean("MaxSoupHoney"))) {
                ChatHandler.sendChat(player, ChatHandler.getMessage("pixelmon.interaction.maxsoup.urshifu", pixelmon.getName()));
            } else {
                pixelmon.setGmaxFactor(!pixelmon.hasGmaxFactor());
                pixelmon.update(EnumUpdateType.values());
                ChatHandler.sendChat(player, ChatHandler.getMessage("pixelmon.interaction.maxsoup." + (pixelmon.hasGmaxFactor() ? "added" : "removed"), pixelmon.getName()));
                if (!player.capabilities.isCreativeMode) {
                    player.getHeldItem(hand).shrink(1);
                }
            }
            return true;
        }
        return false;
    }
}

