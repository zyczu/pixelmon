/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item.interactions;

import com.pixelmongenerations.common.item.interactions.InteractionNBlankizer;
import com.pixelmongenerations.core.config.PixelmonItems;

public class InteractionNLunarizer
extends InteractionNBlankizer {
    public InteractionNLunarizer() {
        super(PixelmonItems.nLunarizer, "Lunala", "Moongeist Beam", 1);
    }
}

