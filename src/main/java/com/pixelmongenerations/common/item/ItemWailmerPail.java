/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.api.events.ApricornEvent;
import com.pixelmongenerations.api.events.BerryEvent;
import com.pixelmongenerations.common.block.BlockBerryTree;
import com.pixelmongenerations.common.block.apricornTrees.BlockApricornTree;
import com.pixelmongenerations.common.block.enums.EnumBlockPos;
import com.pixelmongenerations.common.block.tileEntities.TileEntityApricornTree;
import com.pixelmongenerations.common.block.tileEntities.TileEntityBerryTree;
import com.pixelmongenerations.core.network.ChatHandler;
import net.minecraft.block.IGrowable;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class ItemWailmerPail
extends Item {
    public ItemWailmerPail(String itemName) {
        this.setCreativeTab(CreativeTabs.TOOLS);
        this.setMaxStackSize(1);
        this.setMaxDamage(32);
        this.canRepair = false;
        this.setTranslationKey(itemName);
        this.setRegistryName(itemName);
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (!player.canPlayerEdit(pos, side, player.getHeldItem(hand)) || world.isRemote) {
            return EnumActionResult.FAIL;
        }
        IBlockState state = world.getBlockState(pos);
        if (state.getBlock() instanceof BlockApricornTree) {
            IGrowable tree = (IGrowable)((Object)state.getBlock());
            if (tree.canGrow(world, pos, state, false)) {
                TileEntityApricornTree apricornTree;
                BlockPos loc = pos;
                if (state.getValue(BlockApricornTree.BLOCKPOS) == EnumBlockPos.TOP) {
                    loc = pos.down();
                }
                if ((apricornTree = (TileEntityApricornTree)world.getTileEntity(loc)) != null) {
                    if (!apricornTree.wasWateredToday()) {
                        BlockApricornTree treeBlock = (BlockApricornTree)state.getBlock();
                        ApricornEvent.ApricornWateredEvent event = new ApricornEvent.ApricornWateredEvent(treeBlock.getApricornType().apricorn, loc, (EntityPlayerMP)player, apricornTree);
                        if (!MinecraftForge.EVENT_BUS.post(event)) {
                            apricornTree.updateWatering();
                            tree.grow(world, world.rand, pos, state);
                        }
                    } else {
                        ChatHandler.sendChat(player, "pixelmon.blocks.bonemealwatered", new Object[0]);
                    }
                }
            }
            return EnumActionResult.SUCCESS;
        }
        if (state.getBlock() instanceof BlockBerryTree) {
            TileEntityBerryTree tileEntity;
            BlockPos loc = pos;
            if (state.getValue(BlockApricornTree.BLOCKPOS) == EnumBlockPos.TOP) {
                loc = pos.down();
            }
            if ((tileEntity = (TileEntityBerryTree)world.getTileEntity(loc)) != null) {
                BerryEvent.BerryWateredEvent event = new BerryEvent.BerryWateredEvent(tileEntity.getBerry(), loc, (EntityPlayerMP)player, tileEntity);
                MinecraftForge.EVENT_BUS.post(event);
                if (!event.isCanceled() && !tileEntity.water()) {
                    ChatHandler.sendChat(player, "It already looks well watered.", new Object[0]);
                }
            }
            return EnumActionResult.SUCCESS;
        }
        return EnumActionResult.PASS;
    }
}

