/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PixelmonItem
extends Item {
    private boolean hasEffect;

    public PixelmonItem(String name) {
        this.setCreativeTab(CreativeTabs.MISC);
        this.setTranslationKey(name);
        this.setRegistryName(name);
        this.hasEffect = false;
    }

    public PixelmonItem(String name, boolean hasEffect) {
        this.setCreativeTab(CreativeTabs.MISC);
        this.setTranslationKey(name);
        this.setRegistryName(name);
        this.hasEffect = hasEffect;
    }

    public boolean useFromBag(PixelmonWrapper pixelmonWrapper, PixelmonWrapper target) {
        EntityPlayerMP player = pixelmonWrapper.getPlayerOwner();
        if (player != null) {
            return !player.capabilities.isCreativeMode;
        }
        return false;
    }

    public boolean useFromBag(PixelmonWrapper pixelmonWrapper, PixelmonWrapper target, int additionalInfo) {
        return this.useFromBag(pixelmonWrapper, target);
    }

    public String getLocalizedName() {
        return I18n.translateToLocal(this.getTranslationKey() + ".name");
    }

    public void consumeItem(EntityPlayer player, ItemStack itemstack) {
        if (!player.capabilities.isCreativeMode) {
            player.inventory.clearMatchingItems(itemstack.getItem(), itemstack.getMetadata(), 1, itemstack.getTagCompound());
        }
    }

    @Override
    public boolean hasEffect(ItemStack itemstack) {
        if (itemstack.hasTagCompound()) {
            assert (itemstack.getTagCompound() != null);
            if (itemstack.getTagCompound().getBoolean("GlowEffect")) {
                return true;
            }
        }
        return this.hasEffect;
    }

    @Override
    @SideOnly(value=Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        String info = I18n.translateToLocal("gui.shopkeeper." + this.getTranslationKey());
        if (!this.hasHideFlag(stack) && !info.startsWith("gui.shopkeeper.")) {
            tooltip.add(GuiScreen.isShiftKeyDown() ? info : "Hold shift for more info.");
        }
    }

    public boolean hasHideFlag(ItemStack stack) {
        return stack.hasTagCompound() && stack.getTagCompound().getBoolean("HideTooltip");
    }
}

