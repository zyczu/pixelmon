/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.world.gen.structure.HiddenGrotto;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import com.pixelmongenerations.core.network.ChatHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemSpawnGrotto
extends Item {
    private static HiddenGrotto grotto = new HiddenGrotto();
    public static boolean isOPOnly = false;

    public ItemSpawnGrotto() {
        this.setCreativeTab(PixelmonCreativeTabs.tabPokeLoot);
        this.setTranslationKey("grotto_spawner");
        this.setRegistryName("grotto_spawner");
        this.setMaxStackSize(1);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        ItemStack itemstack = player.getHeldItem(hand);
        if (isOPOnly && !player.canUseCommand(4, "pixelmon.grottospawner.use")) {
            ChatHandler.sendChat(player, "pixelmon.general.needop", new Object[0]);
            return new ActionResult<ItemStack>(EnumActionResult.FAIL, itemstack);
        }
        if (!world.isRemote && player.capabilities.isCreativeMode) {
            int x = (int)player.posX - 4;
            int y = (int)player.posY;
            int z = (int)player.posZ - 4;
            grotto.generate(world, new BlockPos(x, y, z));
            world.playSound(null, player.getPosition(), SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.PLAYERS, 0.5f, 1.0f);
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemstack);
    }

    @Override
    public EnumRarity getRarity(ItemStack par1ItemStack) {
        return EnumRarity.EPIC;
    }
}

