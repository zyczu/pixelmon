/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.bikes.EntityBike;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.enums.items.EnumBike;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemBike
extends PixelmonItem {
    private final EnumBike enumBike;

    public ItemBike(EnumBike bike) {
        super(bike.name().toLowerCase() + "_bike");
        this.enumBike = bike;
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (hand == EnumHand.MAIN_HAND && !worldIn.isRemote) {
            player.getHeldItemMainhand().shrink(1);
            EntityBike entityBike = new EntityBike(worldIn, this.enumBike);
            entityBike.setPosition(pos.getX(), pos.getY() + 1, pos.getZ());
            worldIn.spawnEntity(entityBike);
            entityBike.setOwner(player.getUniqueID());
            entityBike.setEnumBike(this.enumBike);
            player.playSound(SoundEvents.ENTITY_CHICKEN_EGG, 1.0f, 1.0f);
        }
        return super.onItemUse(player, worldIn, pos, hand, facing, hitX, hitY, hitZ);
    }
}

