/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.entity.projectiles.EntityVolcanoBalm;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemVolcanoBalm
extends PixelmonItem {
    public ItemVolcanoBalm(String name) {
        super(name);
        this.setCreativeTab(PixelmonCreativeTabs.natural);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer entityPlayer, EnumHand handIn) {
        ItemStack itemStack = entityPlayer.getHeldItem(handIn);
        if (!entityPlayer.capabilities.isCreativeMode) {
            itemStack.shrink(1);
        }
        entityPlayer.getCooldownTracker().setCooldown(this, 10);
        if (!world.isRemote) {
            EntityVolcanoBalm balm = new EntityVolcanoBalm(world, entityPlayer);
            balm.shoot(entityPlayer, entityPlayer.rotationPitch, entityPlayer.rotationYaw, 0.0f, 1.5f, 1.0f);
            world.spawnEntity(balm);
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemStack);
    }
}

