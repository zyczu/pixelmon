/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.item.server;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.common.item.server.ServerToolType;
import javax.annotation.Nullable;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class ServerItem
extends PixelmonItem {
    public ServerItem() {
        super("server_item");
        this.setCreativeTab(null);
    }

    @Override
    public EnumAction getItemUseAction(ItemStack stack) {
        short holdAction;
        NBTTagCompound compound = stack.getTagCompound();
        if (compound != null && compound.hasKey("ServerItemHoldAction") && (holdAction = compound.getShort("ServerItemHoldAction")) < EnumAction.values().length) {
            return EnumAction.values()[holdAction];
        }
        return super.getItemUseAction(stack);
    }

    @Override
    public int getHarvestLevel(ItemStack stack, String toolClass, @Nullable EntityPlayer player, @Nullable IBlockState blockState) {
        NBTTagCompound compound = stack.getTagCompound();
        if (compound != null && compound.hasKey("ServerToolType")) {
            ServerToolType toolType = ServerToolType.getToolType(compound.getShort("ServerToolType"));
            if (toolType == ServerToolType.None) {
                return -1;
            }
            int harvestLevel = compound.getShort("ServerItemHarvestLevel");
            if (toolType == ServerToolType.MultiTool) {
                return harvestLevel;
            }
            return toolClass.equals(toolType.getName()) ? harvestLevel : -1;
        }
        return super.getHarvestLevel(stack, toolClass, player, blockState);
    }

    @Override
    public float getDestroySpeed(ItemStack stack, IBlockState state) {
        NBTTagCompound compound = stack.getTagCompound();
        if (compound != null && compound.hasKey("ServerItemDestroySpeed")) {
            return compound.getFloat("ServerItemDestroySpeed");
        }
        return super.getDestroySpeed(stack, state);
    }

    @Override
    public String getItemStackDisplayName(ItemStack stack) {
        return "Server Item";
    }
}

