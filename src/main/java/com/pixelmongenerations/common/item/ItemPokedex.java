/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.enums.EnumGui;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.storage.PokeballManager;
import java.util.Optional;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemPokedex
extends PixelmonItem {
    public ItemPokedex() {
        super("pokedex");
        this.setCreativeTab(CreativeTabs.TOOLS);
        this.canRepair = false;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand hand) {
        ItemStack stack = playerIn.getHeldItem(hand);
        if (!worldIn.isRemote) {
            this.openPokedexGui(1, playerIn, worldIn);
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
    }

    @Override
    public EnumRarity getRarity(ItemStack i) {
        return EnumRarity.RARE;
    }

    public void openPokedexGui(int i, EntityPlayer player, World world) {
        PokeballManager pm = PixelmonStorage.pokeBallManager;
        EntityPlayerMP e = (EntityPlayerMP)player;
        Optional<PlayerStorage> optstorage = pm.getPlayerStorage(e);
        if (optstorage.isPresent()) {
            PlayerStorage storage = optstorage.get();
            storage.pokedex.sendToPlayer(e);
            player.openGui(Pixelmon.INSTANCE, EnumGui.PokedexInfo.getIndex(), world, i, 0, 0);
        }
    }
}

