/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItemBlock;
import com.pixelmongenerations.core.config.PixelmonCreativeTabs;
import net.minecraft.block.Block;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;

public class ItemPokeBrick
extends PixelmonItemBlock {
    public ItemPokeBrick(Block block) {
        super(block);
        this.setCreativeTab(PixelmonCreativeTabs.buildingBlocks);
        this.setHasSubtypes(true);
    }

    @Override
    public int getMetadata(int damage) {
        return damage;
    }

    public String getTranslationKey(ItemStack stack) {
        return super.getTranslationKey() + "_" + EnumDyeColor.byMetadata(this.getDamage(stack));
    }
}

