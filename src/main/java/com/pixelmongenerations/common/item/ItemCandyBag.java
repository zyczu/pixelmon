/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.api.enums.ReceiveType;
import com.pixelmongenerations.api.events.PixelmonReceivedEvent;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.config.PixelmonItems;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.time.LocalDate;
import java.time.Month;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class ItemCandyBag
extends PixelmonItem {
    public ItemCandyBag(String name) {
        super(name);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        Optional<PlayerStorage> optstorage;
        ItemStack itemstack = player.getHeldItem(hand);
        if (!world.isRemote && (optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP)player)).isPresent()) {
            PlayerStorage storage = optstorage.get();
            if (!storage.playerData.candyOpened && this.isHalloween()) {
                if (player.getRNG().nextFloat() <= 0.6f) {
                    String randomName = EnumSpecies.randomPoke((boolean)PixelmonConfig.allowRandomSpawnedEggsToBeLegendary).name;
                    EntityPixelmon pokemon = (EntityPixelmon)PixelmonEntityList.createEntityByName(randomName, player.world);
                    pokemon.makeEntityIntoEgg();
                    MinecraftForge.EVENT_BUS.post(new PixelmonReceivedEvent((EntityPlayerMP)player, ReceiveType.Halloween, pokemon));
                    storage.addToParty(pokemon);
                } else if (player.getRNG().nextFloat() <= 0.4f) {
                    ItemStack stack = new ItemStack(PixelmonItems.rareCandy, 2);
                    boolean boolAddedToInventory = player.inventory.addItemStackToInventory(stack);
                    if (!boolAddedToInventory && stack.getItemDamage() == 0) {
                        player.dropItem(stack.getItem(), 2);
                        String itemName = stack.getTranslationKey();
                        ChatHandler.sendFormattedChat(player, TextFormatting.RED, "pixelmon.drops.fullinventory", I18n.translateToLocal(itemName + ".name"));
                    }
                } else if (player.getRNG().nextFloat() <= 0.3f) {
                    ItemStack stack = new ItemStack(PixelmonItems.corruptedGem, 1);
                    boolean boolAddedToInventory = player.inventory.addItemStackToInventory(stack);
                    if (!boolAddedToInventory && stack.getItemDamage() == 0) {
                        player.dropItem(stack.getItem(), 1);
                        String itemName = stack.getTranslationKey();
                        ChatHandler.sendFormattedChat(player, TextFormatting.RED, "pixelmon.drops.fullinventory", I18n.translateToLocal(itemName + ".name"));
                    }
                } else {
                    ItemStack stack = new ItemStack(Items.POISONOUS_POTATO, 1);
                    boolean boolAddedToInventory = player.inventory.addItemStackToInventory(stack);
                    if (!boolAddedToInventory && stack.getItemDamage() == 0) {
                        player.dropItem(stack.getItem(), 1);
                        String itemName = stack.getTranslationKey();
                        ChatHandler.sendFormattedChat(player, TextFormatting.RED, "pixelmon.drops.fullinventory", I18n.translateToLocal(itemName + ".name"));
                    }
                }
                ChatHandler.sendChat(player, "halloween.ishalloween", new Object[0]);
                player.inventory.clearMatchingItems(itemstack.getItem(), itemstack.getMetadata(), 1, itemstack.getTagCompound());
                storage.playerData.giftOpened = true;
            } else if (storage.playerData.giftOpened) {
                ChatHandler.sendChat(player, "halloween.alreadyrecievedcandy", new Object[0]);
            } else {
                ChatHandler.sendChat(player, "halloween.nothalloween", new Object[0]);
            }
        }
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemstack);
    }

    private boolean isHalloween() {
        return LocalDate.now().getMonth() == Month.OCTOBER;
    }
}

