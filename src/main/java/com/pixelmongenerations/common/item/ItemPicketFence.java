/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.item;

import com.pixelmongenerations.common.item.PixelmonItem;
import com.pixelmongenerations.core.config.PixelmonBlocks;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class ItemPicketFence
extends PixelmonItem {
    public ItemPicketFence() {
        super("picket_fence");
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand hand) {
        ItemStack stack = playerIn.getHeldItem(hand);
        if (stack.getCount() <= 0) {
            return null;
        }
        RayTraceResult movingobjectposition = this.rayTrace(worldIn, playerIn, true);
        if (movingobjectposition.typeOfHit != RayTraceResult.Type.BLOCK) {
            return new ActionResult<ItemStack>(EnumActionResult.PASS, stack);
        }
        BlockPos pos = movingobjectposition.getBlockPos();
        if (!worldIn.isBlockModifiable(playerIn, pos) || !playerIn.canPlayerEdit(pos.offset(movingobjectposition.sideHit), movingobjectposition.sideHit, stack)) {
            return new ActionResult<ItemStack>(EnumActionResult.PASS, stack);
        }
        if (!worldIn.isAirBlock(pos = pos.up())) {
            return new ActionResult<ItemStack>(EnumActionResult.PASS, stack);
        }
        worldIn.setBlockState(pos, PixelmonBlocks.picketFenceNormalBlock.getDefaultState(), 2);
        if (!playerIn.capabilities.isCreativeMode) {
            stack.shrink(1);
        }
        return new ActionResult<ItemStack>(EnumActionResult.PASS, stack);
    }
}

