/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning.spawners;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.FlyingOptions;
import com.pixelmongenerations.common.spawning.SpawnData;
import com.pixelmongenerations.common.spawning.SpawnRegistry;
import com.pixelmongenerations.common.spawning.spawners.SpawnerBase;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldType;

public class SpawnerAir
extends SpawnerBase {
    static Set<Material> validFloorMaterials = new HashSet<Material>();
    static Set<Material> validAirMaterials;

    public SpawnerAir() {
        super(SpawnLocation.AirPersistent);
    }

    @Override
    public Integer getSpawnConditionY(World world, BlockPos pos) {
        if (world.getWorldInfo().getTerrainType() == WorldType.FLAT && this.isBlockValidForPixelmonSpawning(world, new BlockPos(pos.getX(), world.getHeight(pos).getY(), pos.getZ()))) {
            return world.getHeight(pos).getY();
        }
        Integer topEarth = this.getTopEarthBlock(world, pos, false);
        Integer cpY = null;
        if (topEarth != null && this.isBlockValidForPixelmonSpawning(world, new BlockPos(pos.getX(), topEarth, pos.getZ()))) {
            cpY = topEarth;
        }
        return cpY;
    }

    @Override
    public List<SpawnData> getEntityList(String biomeID) {
        return SpawnRegistry.getAirSpawnsForBiome(biomeID);
    }

    @Override
    public boolean canPokemonSpawnHereImpl(World world, BlockPos pos) {
        return true;
    }

    @Override
    public float getYOffset(float x, float y, float z, EntityLiving pokemonName) {
        FlyingOptions parameters = ((EntityPixelmon)pokemonName).getFlyingParameters();
        if (parameters == null) {
            return 0.0f;
        }
        int heightMin = parameters.flyHeightMin;
        int heightMax = parameters.flyHeightMax;
        return RandomHelper.getRandomNumberBetween(heightMin, heightMax);
    }

    @Override
    public Set<Material> getSpawnCheckMaterials() {
        return validFloorMaterials;
    }

    @Override
    public Set<Material> getValidSpawnAirMaterials() {
        return validAirMaterials;
    }

    @Override
    public int getMaxNum() {
        return PixelmonConfig.maxNumAirPokemon;
    }

    static {
        validFloorMaterials.add(Material.CRAFTED_SNOW);
        validFloorMaterials.add(Material.GRASS);
        validFloorMaterials.add(Material.GROUND);
        validFloorMaterials.add(Material.ICE);
        validFloorMaterials.add(Material.LEAVES);
        validFloorMaterials.add(Material.ROCK);
        validFloorMaterials.add(Material.SAND);
        validAirMaterials = new HashSet<Material>();
        validAirMaterials.add(Material.AIR);
        validAirMaterials.add(Material.PLANTS);
        validAirMaterials.add(Material.SNOW);
        validAirMaterials.add(Material.VINE);
    }
}

