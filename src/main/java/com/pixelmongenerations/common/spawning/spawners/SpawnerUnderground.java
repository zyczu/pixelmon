/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning.spawners;

import com.pixelmongenerations.common.spawning.SpawnData;
import com.pixelmongenerations.common.spawning.SpawnRegistry;
import com.pixelmongenerations.common.spawning.spawners.SpawnerBase;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import java.util.List;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldType;

public class SpawnerUnderground
extends SpawnerBase {
    protected static final int ENCLOSURE_RADIUS = 5;

    public SpawnerUnderground() {
        super(SpawnLocation.UnderGround);
    }

    @Override
    public Integer getSpawnConditionY(World world, BlockPos pos) {
        if (world.getWorldInfo().getTerrainType() == WorldType.FLAT) {
            return null;
        }
        int intMaxAttempts = 5;
        boolean isValidY = false;
        Integer cpY = null;
        for (int intCounter = 0; intCounter < intMaxAttempts && !isValidY; ++intCounter) {
            cpY = world.rand.nextInt(55) + 5;
            isValidY = this.canPokemonSpawnHereImpl(world, new BlockPos(pos.getX(), cpY, pos.getZ()));
        }
        if (isValidY) {
            return cpY;
        }
        return null;
    }

    @Override
    public List<SpawnData> getEntityList(String biomeID) {
        return SpawnRegistry.getUndergroundSpawns();
    }

    @Override
    public boolean canPokemonSpawnHereImpl(World par1World, BlockPos pos) {
        if (!this.isMostlyEnclosedSpace(par1World, pos, 5)) {
            return false;
        }
        return this.isBlockValidForPixelmonSpawning(par1World, pos);
    }

    protected boolean isMostlyEnclosedSpace(World world, BlockPos pos, int radius) {
        for (EnumFacing dir : EnumFacing.values()) {
            boolean ok = false;
            for (int i = 0; i < radius; ++i) {
                if (!world.isBlockNormalCube(new BlockPos(pos.getX() + dir.getXOffset() * i, pos.getY() + dir.getYOffset() * i, pos.getZ() + dir.getZOffset() * i), true)) continue;
                ok = true;
                break;
            }
            if (ok) continue;
            return false;
        }
        return true;
    }

    @Override
    public int getMaxNum() {
        return PixelmonConfig.maxNumUndergroundPokemon;
    }
}

