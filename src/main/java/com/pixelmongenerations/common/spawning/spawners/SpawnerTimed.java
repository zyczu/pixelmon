/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning.spawners;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.FlyingOptions;
import com.pixelmongenerations.common.entity.pixelmon.stats.SwimOptions;
import com.pixelmongenerations.common.spawning.EnumBiomeType;
import com.pixelmongenerations.common.spawning.PixelmonBiomeDictionary;
import com.pixelmongenerations.common.spawning.SpawnData;
import com.pixelmongenerations.common.spawning.WorldVariable;
import com.pixelmongenerations.common.spawning.spawners.SpawnerBase;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldType;

public abstract class SpawnerTimed
extends SpawnerBase {
    private WorldVariable<Long> initTicks = new WorldVariable<Long>(0L);
    private WorldVariable<Integer> numTicksTillSpawn = new WorldVariable<Integer>(0);
    private WorldVariable<Boolean> init = new WorldVariable<Boolean>(true);
    static Set<Material> validFloorMaterials = new HashSet<Material>();
    static Set<Material> validAirMaterials;

    public SpawnerTimed(SpawnLocation location) {
        super(location);
    }

    @Override
    public Integer getSpawnConditionY(World world, BlockPos pos) {
        if (this.isDisabled()) {
            return null;
        }
        long worldTime = world.getTotalWorldTime();
        int baseTicks = this.getSpawnTickBase();
        double tickVariance = this.getSpawnTickVariance();
        int maxTicksTillSpawn = (int)((double)baseTicks * (1.0 + tickVariance));
        int curNumTicksTillSpawn = Math.min(this.numTicksTillSpawn.get(world), maxTicksTillSpawn);
        this.numTicksTillSpawn.set(world, curNumTicksTillSpawn);
        long curInitTicks = Math.min(this.initTicks.get(world), worldTime);
        this.initTicks.set(world, curInitTicks);
        if (worldTime - curInitTicks >= (long)curNumTicksTillSpawn) {
            this.numTicksTillSpawn.set(world, RandomHelper.getRandomNumberBetween((int)((double)baseTicks * (1.0 - tickVariance)), maxTicksTillSpawn));
            this.initTicks.set(world, worldTime);
            if (!this.init.get(world).booleanValue() && this.rollSpawnChance()) {
                int yPos;
                int xPos = pos.getX();
                int zPos = pos.getZ();
                if (world.getWorldInfo().getTerrainType() == WorldType.FLAT && this.isBlockValidForPixelmonSpawning(world, new BlockPos(xPos, yPos = world.getHeight(pos).getY(), zPos))) {
                    return yPos;
                }
                Integer topEarth = this.getTopEarthBlock(world, pos, true);
                if (topEarth == null) {
                    return null;
                }
                Integer cpY = null;
                if (this.isBlockValidForPixelmonSpawning(world, new BlockPos(xPos, topEarth, zPos))) {
                    cpY = topEarth;
                } else {
                    String biomeID = world.getBiome(pos).getRegistryName().getPath();
                    if (PixelmonBiomeDictionary.isBiomeOfType(biomeID, EnumBiomeType.JUNGLE)) {
                        for (int i = 0; i < 5; ++i) {
                            if (!this.isBlockValidForPixelmonSpawning(world, new BlockPos(xPos, topEarth + i, zPos))) continue;
                            cpY = topEarth + i;
                            break;
                        }
                    }
                }
                return cpY;
            }
            this.init.set(world, false);
        }
        return null;
    }

    protected abstract boolean isDisabled();

    protected abstract int getSpawnTickBase();

    protected abstract double getSpawnTickVariance();

    protected boolean rollSpawnChance() {
        return true;
    }

    @Override
    public abstract int getMaxNum();

    @Override
    public abstract List<SpawnData> getEntityList(String var1);

    @Override
    protected boolean canPokemonSpawnHereImpl(World par1World, BlockPos pos) {
        return this.isBlockValidForPixelmonSpawning(par1World, pos);
    }

    @Override
    public boolean shouldSpawnInThisChunk(int chunkCount) {
        return RandomHelper.getRandomChance(1.0f / (float)chunkCount);
    }

    @Override
    public Set<Material> getSpawnCheckMaterials() {
        return validFloorMaterials;
    }

    @Override
    public Set<Material> getValidSpawnAirMaterials() {
        return validAirMaterials;
    }

    @Override
    public float getYOffset(float x, float y, float z, EntityLiving entity) {
        if (entity instanceof EntityPixelmon) {
            SwimOptions swimmingParameters;
            EntityPixelmon pokemon = (EntityPixelmon)entity;
            SpawnLocation location = this.getActualSpawnLocation(pokemon);
            if (location == SpawnLocation.AirPersistent) {
                FlyingOptions flyingParameters = pokemon.getFlyingParameters();
                int heightMin = flyingParameters.flyHeightMin;
                int heightMax = flyingParameters.flyHeightMax;
                return RandomHelper.getRandomNumberBetween(heightMin, heightMax);
            }
            if (location == SpawnLocation.Water && (swimmingParameters = pokemon.getSwimmingParameters()) != null && swimmingParameters.depthRangeStart == -1) {
                Integer topEarth = this.getTopEarthBlock(pokemon.world, new BlockPos(x, y, z), false);
                return (float)topEarth.intValue() - y;
            }
        }
        return 0.0f;
    }

    static {
        validFloorMaterials.add(Material.CRAFTED_SNOW);
        validFloorMaterials.add(Material.GRASS);
        validFloorMaterials.add(Material.GROUND);
        validFloorMaterials.add(Material.ICE);
        validFloorMaterials.add(Material.LEAVES);
        validFloorMaterials.add(Material.ROCK);
        validFloorMaterials.add(Material.SAND);
        validFloorMaterials.add(Material.SNOW);
        validFloorMaterials.add(Material.PACKED_ICE);
        validFloorMaterials.add(Material.PLANTS);
        validAirMaterials = new HashSet<Material>();
        validAirMaterials.add(Material.AIR);
        validAirMaterials.add(Material.WATER);
    }
}

