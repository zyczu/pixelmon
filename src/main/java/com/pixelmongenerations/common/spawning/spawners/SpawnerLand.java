/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning.spawners;

import com.pixelmongenerations.common.spawning.EnumBiomeType;
import com.pixelmongenerations.common.spawning.PixelmonBiomeDictionary;
import com.pixelmongenerations.common.spawning.SpawnData;
import com.pixelmongenerations.common.spawning.SpawnRegistry;
import com.pixelmongenerations.common.spawning.spawners.SpawnerBase;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.minecraft.block.material.Material;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class SpawnerLand
extends SpawnerBase {
    static Set<Material> validAirMaterials = new HashSet<Material>();

    public SpawnerLand() {
        super(SpawnLocation.Land);
    }

    @Override
    public Integer getSpawnConditionY(World world, BlockPos pos) {
        Integer topEarth = this.getNextTopEarthBlock(world, pos, false);
        if (topEarth == null) {
            return null;
        }
        Integer cpY = null;
        if (this.isBlockValidForPixelmonSpawning(world, new BlockPos(pos.getX(), topEarth, pos.getZ()))) {
            cpY = topEarth;
        } else {
            String biomeID = world.getBiome(pos).getRegistryName().getPath();
            if (PixelmonBiomeDictionary.isBiomeOfType(biomeID, EnumBiomeType.JUNGLE)) {
                for (int i = 0; i < 5; ++i) {
                    if (!this.isBlockValidForPixelmonSpawning(world, new BlockPos(pos.getX(), topEarth + i, pos.getZ()))) continue;
                    cpY = topEarth + i;
                    break;
                }
            }
        }
        return cpY;
    }

    @Override
    public List<SpawnData> getEntityList(String biomeID) {
        return SpawnRegistry.getSpawnsForBiome(biomeID);
    }

    @Override
    public boolean canPokemonSpawnHereImpl(World world, BlockPos pos) {
        return this.isBlockValidForPixelmonSpawning(world, pos);
    }

    @Override
    public Set<Material> getValidSpawnAirMaterials() {
        return validAirMaterials;
    }

    @Override
    public int getMaxNum() {
        return PixelmonConfig.maxNumLandPokemon;
    }

    static {
        validAirMaterials.add(Material.AIR);
        validAirMaterials.add(Material.PLANTS);
        validAirMaterials.add(Material.SNOW);
        validAirMaterials.add(Material.VINE);
    }
}

