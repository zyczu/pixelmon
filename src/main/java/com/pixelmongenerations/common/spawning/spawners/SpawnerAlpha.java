/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning.spawners;

import com.pixelmongenerations.common.entity.npcs.registry.DropItemRegistry;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.drops.AlphaInfo;
import com.pixelmongenerations.common.spawning.SpawnData;
import com.pixelmongenerations.common.spawning.spawners.EntityData;
import com.pixelmongenerations.common.spawning.spawners.SpawnerTimed;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class SpawnerAlpha
extends SpawnerTimed {
    public SpawnerAlpha() {
        super(SpawnLocation.Alpha);
    }

    @Override
    protected boolean isDisabled() {
        return PixelmonConfig.maxNumAlphas <= 0;
    }

    @Override
    protected int getSpawnTickBase() {
        return PixelmonConfig.alphaSpawnTicks;
    }

    @Override
    protected double getSpawnTickVariance() {
        return 0.6;
    }

    @Override
    public int getMaxNum() {
        return PixelmonConfig.maxNumAlphas;
    }

    @Override
    public EntityData getRandomEntity(World world, Random rand, String biomeID, BlockPos pos, int level) {
        ArrayList<AlphaInfo> alphaSpawns = DropItemRegistry.getAlphaPokemon();
        if (alphaSpawns.isEmpty()) {
            return null;
        }
        ArrayList<AlphaInfo> possibleSpawns = new ArrayList<AlphaInfo>();
        if (world.getBlockState(new BlockPos(pos.getX(), this.getTopEarthBlock(world, pos, false) - 1, pos.getZ())).getMaterial() == Material.WATER) {
            for (AlphaInfo a : alphaSpawns) {
                if (a.spawnLocation != SpawnLocation.Water || a.dimensions != null && !a.dimensions.isEmpty() && !a.dimensions.contains(world.provider.getDimension())) continue;
                possibleSpawns.add(a);
            }
        } else {
            for (AlphaInfo a : alphaSpawns) {
                if (a.spawnLocation == SpawnLocation.Water || a.dimensions != null && !a.dimensions.isEmpty() && !a.dimensions.contains(world.provider.getDimension())) continue;
                possibleSpawns.add(a);
            }
        }
        if (possibleSpawns.isEmpty()) {
            return null;
        }
        AlphaInfo alphaInfo = (AlphaInfo)RandomHelper.getRandomElementFromList(possibleSpawns);
        return new EntityData(alphaInfo.pokemon.name, level, alphaInfo.form);
    }

    @Override
    public void modifyPokemon(EntityLiving entity, int x, int z) {
        if (entity instanceof EntityPixelmon) {
            EntityPixelmon pokemon = (EntityPixelmon)entity;
            pokemon.setAlpha(true);
        }
    }

    @Override
    public SpawnLocation getActualSpawnLocation(EntityPixelmon pokemon) {
        ArrayList<AlphaInfo> alphaSpawns = DropItemRegistry.getAlphaPokemon();
        for (AlphaInfo a : alphaSpawns) {
            if (a.pokemon != pokemon.baseStats.pokemon) continue;
            return a.spawnLocation;
        }
        return null;
    }

    @Override
    public List<SpawnData> getEntityList(String var1) {
        return null;
    }
}

