/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.common.spawning;

import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.archetypes.spawners.TickingSpawner;
import com.pixelmongenerations.api.world.BlockCollection;
import com.pixelmongenerations.common.block.tileEntities.TileEntityScarecrow;
import com.pixelmongenerations.common.spawning.PixelmonSpawning;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.event.RepelHandler;
import com.pixelmongenerations.core.util.helper.BlockHelper;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import javax.annotation.Nullable;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class PlayerTrackingSpawner
extends TickingSpawner {
    public final UUID playerUUID;
    protected BlockCollection collection;
    protected ArrayList<SpawnLocation> spawnLocations;
    protected ArrayList<SpawnAction<? extends Entity>> possibleSpawns = new ArrayList();
    public int minDistFromCentre = PixelmonConfig.minimumDistanceFromCentre;
    public int maxDistFromCentre = PixelmonConfig.maximumDistanceFromCentre;
    public float horizontalTrackFactor = PixelmonConfig.horizontalTrackFactor;
    public float verticalTrackFactor = PixelmonConfig.verticalTrackFactor;
    public int horizontalSliceRadius = PixelmonConfig.horizontalSliceRadius;
    public int verticalSliceRadius = PixelmonConfig.verticalSliceRadius;
    public int scarecrowTicks = 0;

    public PlayerTrackingSpawner(UUID playerUUID) {
        this.playerUUID = playerUUID;
    }

    @Override
    public int getNumPasses() {
        return 2;
    }

    @Nullable
    public EntityPlayerMP getTrackedPlayer() {
        return FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUUID(this.playerUUID);
    }

    @Override
    public boolean shouldDoSpawning() {
        if (this.scarecrowTicks > 0) {
            --this.scarecrowTicks;
            return false;
        }
        if (this.getTrackedPlayer() != null && BlockHelper.findClosestTileEntity(TileEntityScarecrow.class, this.getTrackedPlayer(), PixelmonConfig.scarecrowRadius, s -> true) != null) {
            this.scarecrowTicks = PixelmonConfig.scarecrowEffectPeriod * 20;
            return false;
        }
        return super.shouldDoSpawning();
    }

    public boolean isTrackedPlayerOnline() {
        return this.getTrackedPlayer() != null;
    }

    @Override
    public ArrayList<SpawnAction<? extends Entity>> getSpawns(int pass) {
        if (pass == 0) {
            EntityPlayerMP player = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUUID(this.playerUUID);
            BlockPos centre = player.getPosition().add((double)this.horizontalTrackFactor * player.motionX, (double)this.verticalTrackFactor * player.motionY, (double)this.horizontalTrackFactor * player.motionZ);
            float theta = RandomHelper.getRandomNumberBetween(0.0f, (float)Math.PI * 2);
            int r = RandomHelper.getRandomNumberBetween(this.minDistFromCentre, this.maxDistFromCentre);
            int x = centre.getX() + (int)((double)r * Math.cos(theta));
            int z = centre.getZ() + (int)((double)r * Math.sin(theta));
            this.collection = new BlockCollection(player.getServerWorld(), x - this.horizontalSliceRadius, x + this.horizontalSliceRadius, centre.getY() - this.verticalSliceRadius, centre.getY() + this.verticalSliceRadius, z - this.horizontalSliceRadius, z + this.horizontalSliceRadius);
            this.isBusy = true;
            PixelmonSpawning.coordinator.processor.addProcess(() -> {
                this.spawnLocations = this.spawnLocationCalculator.calculateSpawnableLocations(this.collection);
                Collections.shuffle(this.spawnLocations);
                this.possibleSpawns = this.calculateSpawnActions(this.spawnLocations);
                this.isBusy = false;
            });
            return null;
        }
        if (this.possibleSpawns == null || this.possibleSpawns.isEmpty()) {
            return null;
        }
        this.possibleSpawns.removeIf(action -> {
            if (!action.spawnLocation.location.world.getEntities(EntityPlayerMP.class, p -> RepelHandler.hasRepel(p.getUniqueID()) && p.getPosition().distanceSq(action.spawnLocation.location.pos) < 5500.0).isEmpty()) {
                return true;
            }
            return !action.spawnLocation.location.world.getEntities(EntityLiving.class, entityLiving -> entityLiving.getPosition().getDistance(action.spawnLocation.location.pos.getX(), action.spawnLocation.location.pos.getY(), action.spawnLocation.location.pos.getZ()) < (double)this.minDistBetweenSpawns).isEmpty();
        });
        return this.possibleSpawns;
    }

    public ArrayList<SpawnAction<? extends Entity>> calculateSpawnActions(ArrayList<SpawnLocation> spawnLocations) {
        ArrayList<SpawnAction<? extends Entity>> toSpawn = new ArrayList<SpawnAction<? extends Entity>>();
        HashMap<SpawnInfo, SpawnLocation> potentialSpawns = new HashMap<SpawnInfo, SpawnLocation>();
        HashMap<SpawnInfo, Float> adjustedRarities = new HashMap<SpawnInfo, Float>();
        for (SpawnLocation spawnLocation2 : spawnLocations) {
            SpawnInfo weightedSpawnInfo = this.getWeightedSpawnInfo(spawnLocation2);
            if (weightedSpawnInfo == null) continue;
            float rarity = weightedSpawnInfo.getAdjustedRarity(spawnLocation2);
            potentialSpawns.put(weightedSpawnInfo, spawnLocation2);
            adjustedRarities.put(weightedSpawnInfo, Float.valueOf(rarity));
        }
        block1: while (!potentialSpawns.isEmpty() && this.hasCapacity(toSpawn.size() + 1) && toSpawn.size() <= this.spawnsPerPass) {
            float raritySum = 0.0f;
            for (Map.Entry entry : adjustedRarities.entrySet()) {
                raritySum += ((Float)entry.getValue()).floatValue();
            }
            float chosenSum = RandomHelper.getRandomNumberBetween(0.0f, raritySum);
            raritySum = 0.0f;
            Iterator<Map.Entry<SpawnInfo, Float>> it = adjustedRarities.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<SpawnInfo, Float> entry = it.next();
                if (!potentialSpawns.containsKey(entry.getKey())) {
                    it.remove();
                    continue;
                }
                raritySum += ((Float)entry.getValue()).floatValue();
                if (raritySum < chosenSum) continue;
                SpawnInfo spawn = (SpawnInfo)entry.getKey();
                SpawnLocation chosenLocation = (SpawnLocation)potentialSpawns.get(spawn);
                BlockPos.MutableBlockPos pos = chosenLocation.location.pos;
                toSpawn.add(spawn.construct(this, chosenLocation));
                it.remove();
                potentialSpawns.remove(spawn);
                potentialSpawns.values().removeIf(spawnLocation -> {
                    if (spawnLocation.location.world != chosenLocation.location.world) {
                        return false;
                    }
                    return !(spawnLocation.location.pos.getDistance(pos.getX(), pos.getY(), pos.getZ()) >= (double)this.minDistBetweenSpawns);
                });
                continue block1;
            }
        }
        return toSpawn;
    }

    public static class PlayerTrackingSpawnerBuilder<T extends PlayerTrackingSpawner>
    extends TickingSpawner.TickingSpawnerBuilder<T> {
        protected int minDistFromCentre = PixelmonConfig.minimumDistanceFromCentre;
        protected int maxDistFromCentre = PixelmonConfig.maximumDistanceFromCentre;
        protected float horizontalTrackFactor = PixelmonConfig.horizontalTrackFactor;
        protected float verticalTrackFactor = PixelmonConfig.verticalTrackFactor;
        protected int horizontalSliceRadius = PixelmonConfig.horizontalSliceRadius;
        protected int verticalSliceRadius = PixelmonConfig.verticalSliceRadius;

        public <E extends PlayerTrackingSpawnerBuilder<T>> E setDistanceFromCentre(int minimum, int maximum) {
            if (minimum < 0) {
                minimum = 0;
            }
            this.minDistFromCentre = minimum;
            if (maximum < 0) {
                maximum = 0;
            }
            this.maxDistFromCentre = maximum;
            return (E)this;
        }

        public <E extends PlayerTrackingSpawnerBuilder<T>> E setTrackFactors(float horizontal, float vertical) {
            this.horizontalTrackFactor = horizontal;
            this.verticalTrackFactor = vertical;
            return (E)this;
        }

        public <E extends PlayerTrackingSpawnerBuilder<T>> E setSliceRadii(int horizontal, int vertical) {
            if (horizontal < 1) {
                horizontal = 1;
            }
            this.horizontalSliceRadius = horizontal;
            if (vertical < 1) {
                vertical = 1;
            }
            this.verticalSliceRadius = vertical;
            return (E)this;
        }

        @Override
        public T apply(T spawner) {
            super.apply(spawner);
            ((PlayerTrackingSpawner)spawner).minDistFromCentre = this.minDistFromCentre;
            ((PlayerTrackingSpawner)spawner).maxDistFromCentre = this.maxDistFromCentre;
            ((PlayerTrackingSpawner)spawner).verticalTrackFactor = this.verticalTrackFactor;
            ((PlayerTrackingSpawner)spawner).horizontalTrackFactor = this.horizontalTrackFactor;
            ((PlayerTrackingSpawner)spawner).verticalSliceRadius = this.verticalSliceRadius;
            ((PlayerTrackingSpawner)spawner).horizontalSliceRadius = this.horizontalSliceRadius;
            return spawner;
        }
    }
}

