/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning;

import com.pixelmongenerations.api.spawning.SpawnSet;
import com.pixelmongenerations.api.spawning.archetypes.entities.pokemon.SpawnInfoPokemon;
import com.pixelmongenerations.api.spawning.conditions.LocationType;
import com.pixelmongenerations.api.spawning.util.SetLoader;
import com.pixelmongenerations.common.entity.npcs.registry.ServerNPCRegistry;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.Rarity;
import com.pixelmongenerations.common.spawning.PixelmonBiomeDictionary;
import com.pixelmongenerations.common.spawning.SpawnData;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.EnumEntityListClassType;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumNPCType;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SpawnRegistry {
    private static HashMap<String, List<SpawnData>> landSpawns = new HashMap();
    private static HashMap<String, List<SpawnData>> undergroundSpawns = new HashMap();
    private static HashMap<String, List<SpawnData>> biomeWaterSpawns = new HashMap();
    private static HashMap<String, List<SpawnData>> airSpawns = new HashMap();
    private static HashMap<String, List<SpawnData>> legendarySpawns = new HashMap();
    private static HashMap[] npcSpawns = new HashMap[EnumNPCType.values().length];

    public static void addPixelmonSpawn(BaseStats stats) {
        block15: {
            Integer[] biomeIDs;
            block14: {
                biomeIDs = stats.biomeIDs;
                if (biomeIDs == null) {
                    return;
                }
                if (stats.rarity.day >= 0 && stats.rarity.dawndusk >= 0 && stats.rarity.night >= 0) break block14;
                if (stats.rarity.day < 0) {
                    stats.rarity.day = 1;
                }
                if (stats.rarity.dawndusk < 0) {
                    stats.rarity.dawndusk = 1;
                }
                if (stats.rarity.night < 0) {
                    stats.rarity.night = 1;
                }
                for (SpawnLocation s : stats.spawnLocations) {
                    for (Integer biomeID : biomeIDs) {
                        SpawnRegistry.storeSpawnInfo(legendarySpawns, stats.pixelmonName, stats.rarity, EnumEntityListClassType.Pixelmon, PixelmonBiomeDictionary.getBiomeNameFromID(biomeID), s);
                    }
                }
                break block15;
            }
            if (stats.rarity.day <= 0 && stats.rarity.dawndusk <= 0 && stats.rarity.night <= 0) break block15;
            if (stats.spawnLocations == null) {
                for (Integer biomeID : biomeIDs) {
                    SpawnRegistry.storeSpawnInfo(landSpawns, stats.pixelmonName, stats.rarity, EnumEntityListClassType.Pixelmon, PixelmonBiomeDictionary.getBiomeNameFromID(biomeID), null);
                }
            } else {
                for (SpawnLocation s : stats.spawnLocations) {
                    for (Integer biomeID : biomeIDs) {
                        if (s == SpawnLocation.Land || s == SpawnLocation.Air) {
                            SpawnRegistry.storeSpawnInfo(landSpawns, stats.pixelmonName, stats.rarity, EnumEntityListClassType.Pixelmon, PixelmonBiomeDictionary.getBiomeNameFromID(biomeID), s);
                            continue;
                        }
                        if (s == SpawnLocation.AirPersistent) {
                            SpawnRegistry.storeSpawnInfo(airSpawns, stats.pixelmonName, stats.rarity, EnumEntityListClassType.Pixelmon, PixelmonBiomeDictionary.getBiomeNameFromID(biomeID), s);
                            continue;
                        }
                        if (s == SpawnLocation.UnderGround) {
                            SpawnRegistry.storeSpawnInfo(undergroundSpawns, stats.pixelmonName, stats.rarity, EnumEntityListClassType.Pixelmon, PixelmonBiomeDictionary.getBiomeNameFromID(biomeID), s);
                            continue;
                        }
                        if (s != SpawnLocation.Water) continue;
                        SpawnRegistry.storeSpawnInfo(biomeWaterSpawns, stats.pixelmonName, stats.rarity, EnumEntityListClassType.Pixelmon, PixelmonBiomeDictionary.getBiomeNameFromID(biomeID), s);
                    }
                }
            }
        }
    }

    public static boolean addPixelmonSpawnToBiome(String pixelmonName, String biomeName) {
        if (!EnumSpecies.hasPokemon(pixelmonName)) {
            Pixelmon.LOGGER.info("Unable to add custom spawn for " + pixelmonName + " to " + biomeName + ". Pok\u00c3\u00a9mon does not exist!");
            return false;
        }
        String correctName = EnumSpecies.getFromName((String)pixelmonName).get().name;
        BaseStats stats = Entity3HasStats.getBaseStats(correctName).get();
        Integer biomeID = PixelmonBiomeDictionary.getBiomeIDFromName(biomeName);
        if (biomeID == null) {
            Pixelmon.LOGGER.info("Unable to add custom spawn for " + pixelmonName + " to " + biomeName + ". Biome not found!");
            return false;
        }
        Integer[] tempIDs = new Integer[stats.biomeIDs.length + 1];
        System.arraycopy(stats.biomeIDs, 0, tempIDs, 0, stats.biomeIDs.length);
        tempIDs[stats.biomeIDs.length] = biomeID;
        stats.biomeIDs = tempIDs;
        SpawnRegistry.addPixelmonSpawn(stats);
        return true;
    }

    public static void addNPCSpawn(String name, int rarity, EnumNPCType npcType, String[] biomeIDs) {
        if (biomeIDs == null) {
            return;
        }
        if (npcSpawns[npcType.ordinal()] == null) {
            SpawnRegistry.npcSpawns[npcType.ordinal()] = new HashMap();
        }
        for (String biomeID : biomeIDs) {
            SpawnRegistry.storeSpawnInfo(npcSpawns[npcType.ordinal()], name, new Rarity(rarity, rarity, rarity), EnumEntityListClassType.NPC, biomeID, null);
        }
    }

    private static void storeSpawnInfo(HashMap<String, List<SpawnData>> hashmap, String name, Rarity rarity, EnumEntityListClassType type, String biomeID, SpawnLocation s) {
        List<SpawnData> spawnList = hashmap.containsKey(biomeID) ? hashmap.get(biomeID) : new ArrayList<>();
        spawnList.add(new SpawnData(name, rarity, type, s));
        hashmap.put(biomeID, spawnList);
    }

    public static void getGenerationInfo(HashMap<String, String> hashmap, String name) {
        Optional<BaseStats> statOptional = Entity3HasStats.getBaseStats(name);
        if (!statOptional.isPresent()) {
            Pixelmon.LOGGER.info(name + " returned NULL stats.");
            return;
        }
        BaseStats stats = statOptional.get();
        int ID = stats.nationalPokedexNumber;
        if (!hashmap.containsKey(name) && stats.rarity != null) {
            if (ID <= 151) {
                if (PixelmonConfig.Gen1) {
                    SpawnRegistry.addPixelmonSpawn(stats);
                    hashmap.put(name, "Gen1");
                }
            } else if (ID <= 251) {
                if (PixelmonConfig.Gen2) {
                    SpawnRegistry.addPixelmonSpawn(stats);
                    hashmap.put(name, "Gen2");
                }
            } else if (ID <= 386) {
                if (PixelmonConfig.Gen3) {
                    SpawnRegistry.addPixelmonSpawn(stats);
                    hashmap.put(name, "Gen3");
                }
            } else if (ID <= 493) {
                if (PixelmonConfig.Gen4) {
                    SpawnRegistry.addPixelmonSpawn(stats);
                    hashmap.put(name, "Gen4");
                }
            } else if (ID <= 649) {
                if (PixelmonConfig.Gen5) {
                    SpawnRegistry.addPixelmonSpawn(stats);
                    hashmap.put(name, "Gen5");
                }
            } else if (ID <= 721) {
                if (PixelmonConfig.Gen6) {
                    SpawnRegistry.addPixelmonSpawn(stats);
                    hashmap.put(name, "Gen6");
                }
            } else if (ID <= 807) {
                if (PixelmonConfig.Gen7) {
                    SpawnRegistry.addPixelmonSpawn(stats);
                    hashmap.put(name, "Gen7");
                }
            } else if (ID <= 9999) {
                if (PixelmonConfig.Gen8) {
                    SpawnRegistry.addPixelmonSpawn(stats);
                    hashmap.put(name, "Gen8");
                }
            } else {
                Pixelmon.LOGGER.info(name + " does not have a valid ID number.");
            }
        }
    }

    public static List<SpawnData> getLegendarySpawnsForBiome(String biomeName) {
        return legendarySpawns.get(biomeName);
    }

    public static List<SpawnData> getSpawnsForBiome(String biomeName) {
        return SetLoader.<SpawnSet>getAllSets().stream().flatMap(spawnSet -> spawnSet.spawnInfos.stream()).filter(SpawnInfoPokemon.class::isInstance).map(SpawnInfoPokemon.class::cast).filter(info -> info.condition.biomes.stream().anyMatch(biome1 -> biome1.getRegistryName().toString().equalsIgnoreCase(biomeName))).filter(info -> !EnumSpecies.legendaries.contains(info.getPokemonSpec().name) && !EnumSpecies.ultrabeasts.contains(info.getPokemonSpec().name)).filter(info -> info.locationTypes.contains(LocationType.LAND)).map(info -> new SpawnData(info.getPokemonSpec().name, Entity3HasStats.getBaseStats((String)info.getPokemonSpec().name).get().rarity, EnumEntityListClassType.Pixelmon, SpawnLocation.Land)).collect(Collectors.toList());
    }

    public static List<SpawnData> getUndergroundSpawns() {
        ArrayList<SpawnData> undergroundSpawnData = new ArrayList<SpawnData>();
        HashSet alreadyListed = new HashSet();
        for (String biomeName : undergroundSpawns.keySet()) {
            undergroundSpawns.get(biomeName).stream().filter(sd -> !alreadyListed.contains(sd.name)).forEach(sd -> {
                undergroundSpawnData.add((SpawnData)sd);
                alreadyListed.add(sd.name);
            });
        }
        return undergroundSpawnData;
    }

    public static List<SpawnData> getWaterSpawnsForBiome(String biomeName) {
        return biomeWaterSpawns.get(biomeName);
    }

    public static List<SpawnData> getAirSpawnsForBiome(String biomeName) {
        return airSpawns.get(biomeName);
    }

    public static List<SpawnData> getNPCSpawnsForBiome(String biomeID) {
        ArrayList<List> eligibleTypes = new ArrayList<List>();
        ArrayList<Integer> eligibleTypeRarities = new ArrayList<Integer>();
        for (EnumNPCType t : EnumNPCType.values()) {
            List spawnsForBiome;
            HashMap typeSpawns = npcSpawns[t.ordinal()];
            if (typeSpawns == null || (spawnsForBiome = (List)typeSpawns.get(biomeID)) == null) continue;
            eligibleTypes.add(spawnsForBiome);
            eligibleTypeRarities.add(ServerNPCRegistry.rarities.getRarityForType(t));
        }
        int npcTypeIndex = RandomHelper.getRandomIndexFromWeights(eligibleTypeRarities);
        if (npcTypeIndex != -1) {
            return (List)eligibleTypes.get(npcTypeIndex);
        }
        return null;
    }
}

