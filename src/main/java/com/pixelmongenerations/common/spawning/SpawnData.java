/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning;

import com.pixelmongenerations.common.entity.pixelmon.stats.Rarity;
import com.pixelmongenerations.common.spawning.spawners.EnumWorldState;
import com.pixelmongenerations.core.config.EnumEntityListClassType;
import com.pixelmongenerations.core.database.SpawnLocation;

public final class SpawnData {
    public final String name;
    public Rarity rarity;
    public final EnumEntityListClassType type;
    public final SpawnLocation spawnLocation;

    public SpawnData(String name, Rarity rarity, EnumEntityListClassType classType, SpawnLocation location) {
        this.name = name;
        this.rarity = rarity;
        this.type = classType;
        this.spawnLocation = location;
    }

    public int getRarity(EnumWorldState state) {
        return SpawnData.getRarity(state, this.rarity);
    }

    public static int getRarity(EnumWorldState state, Rarity entityRarity) {
        return state == EnumWorldState.day ? entityRarity.day : (state == EnumWorldState.dawn || state == EnumWorldState.dusk ? entityRarity.dawndusk : entityRarity.night);
    }
}

