/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning;

import java.util.HashMap;
import java.util.Map;
import net.minecraft.world.World;

public class WorldVariable<T> {
    private Map<Integer, T> worldMap = new HashMap<Integer, T>();
    private T defaultValue;

    public WorldVariable(T defaultValue) {
        this.defaultValue = defaultValue;
    }

    public T get(World world) {
        return this.worldMap.getOrDefault(world.provider.getDimension(), this.defaultValue);
    }

    public void set(World world, T value) {
        this.worldMap.put(world.provider.getDimension(), value);
    }
}

