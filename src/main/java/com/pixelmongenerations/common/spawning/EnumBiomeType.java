/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning;

public enum EnumBiomeType {
    AMPLIFIED,
    BEACH,
    DESERT,
    EDGE,
    END,
    EXTREMEHILLS,
    FLOWER,
    FOREST,
    FROZEN,
    HILLS,
    JUNGLE,
    MAGICAL,
    MESA,
    MUSHROOM,
    MUTATION,
    NETHER,
    OCEAN,
    PLAINS,
    PLATEAU,
    RIVER,
    SWAMP,
    TAIGA,
    WASTELAND;

}

