/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.spawning;

import com.pixelmongenerations.common.spawning.EnumBiomeType;
import com.pixelmongenerations.core.util.RegexPatterns;
import java.util.ArrayList;
import java.util.Collections;
import net.minecraft.world.biome.Biome;

public class PixelmonBiomeDictionary {
    private static PixelmonBiomeInfo[] biomeList = new PixelmonBiomeInfo[256];
    private static ArrayList<Integer>[] biomesWithType = new ArrayList[EnumBiomeType.values().length];

    public static Integer getBiomeIDFromName(String biomeName) {
        Integer i = 0;
        while (i < biomeList.length) {
            if (biomeList[i] == null || PixelmonBiomeDictionary.biomeList[i.intValue()].biomeName == null || !biomeList[i].isSameNameAs(biomeName)) continue;
            return i;
        }
        return null;
    }

    public static PixelmonBiomeInfo[] getBiomeInfoList() {
        return biomeList;
    }

    public static boolean registerBiomeType(Integer biomeID, String biomeEnglishName, EnumBiomeType ... types) {
        if (types == null) {
            types = new EnumBiomeType[]{EnumBiomeType.WASTELAND};
        }
        if (Biome.getBiomeForId(biomeID) != null) {
            for (EnumBiomeType type : types) {
                if (biomesWithType[type.ordinal()] == null) {
                    PixelmonBiomeDictionary.biomesWithType[type.ordinal()] = new ArrayList();
                }
                biomesWithType[type.ordinal()].add(biomeID);
            }
            if (biomeList[biomeID] == null) {
                PixelmonBiomeDictionary.biomeList[biomeID.intValue()] = new PixelmonBiomeInfo(Biome.getBiomeForId(biomeID).getRegistryName().getPath(), biomeEnglishName, types);
            } else {
                Collections.addAll(PixelmonBiomeDictionary.biomeList[biomeID.intValue()].typeList, types);
            }
            return true;
        }
        return false;
    }

    public static EnumBiomeType[] getTypesForBiome(Biome biome) {
        return PixelmonBiomeDictionary.getTypesForBiomeID(biome.getRegistryName().getPath());
    }

    public static EnumBiomeType[] getTypesForBiomeID(String biomeID) {
        PixelmonBiomeInfo info = PixelmonBiomeDictionary.getBiomeInfo(biomeID);
        if (info != null) {
            return info.typeList.toArray(new EnumBiomeType[info.typeList.size()]);
        }
        return null;
    }

    public static String getBiomeNameFromID(int id) {
        if (id < 0 || id >= biomeList.length) {
            return null;
        }
        return PixelmonBiomeDictionary.biomeList[id].biomeName;
    }

    public static PixelmonBiomeInfo getBiomeInfo(Biome biome) {
        if (biome != null) {
            return PixelmonBiomeDictionary.getBiomeInfo(biome.getRegistryName().getPath());
        }
        return null;
    }

    public static PixelmonBiomeInfo getBiomeInfo(String biomeID) {
        for (PixelmonBiomeInfo info : biomeList) {
            if (info == null || !info.biomeName.equals(biomeID)) continue;
            return info;
        }
        return null;
    }

    public static boolean isBiomeOfType(Biome biome, EnumBiomeType tag) {
        return PixelmonBiomeDictionary.isBiomeOfType(biome.getRegistryName().getPath(), tag);
    }

    public static boolean isBiomeOfType(String biomeID, EnumBiomeType tag) {
        PixelmonBiomeInfo info = PixelmonBiomeDictionary.getBiomeInfo(biomeID);
        if (info != null) {
            return info.typeList.contains((Object)tag);
        }
        return false;
    }

    public static ArrayList<Integer> getBiomeIDsMatchingAnyTag(ArrayList<EnumBiomeType> tags) {
        ArrayList<Integer> intBiomeIDs = new ArrayList<Integer>();
        for (int i = 0; i < biomeList.length; ++i) {
            if (tags == null || tags.isEmpty() || biomeList[i] == null || PixelmonBiomeDictionary.biomeList[i].typeList == null) continue;
            for (EnumBiomeType tag : tags) {
                if (!PixelmonBiomeDictionary.biomeList[i].typeList.contains((Object)tag)) continue;
                intBiomeIDs.add(i);
            }
        }
        return intBiomeIDs;
    }

    public static ArrayList<Integer> getBiomeIDsMatchingAllTags(ArrayList<EnumBiomeType> tags) {
        ArrayList<Integer> intBiomeIDs = new ArrayList<Integer>();
        for (int i = 0; i < biomeList.length; ++i) {
            int intCountMatches = 0;
            if (tags == null || tags.isEmpty() || biomeList[i] == null || PixelmonBiomeDictionary.biomeList[i].typeList == null) continue;
            for (EnumBiomeType tag : tags) {
                if (!PixelmonBiomeDictionary.biomeList[i].typeList.contains((Object)tag)) continue;
                ++intCountMatches;
            }
            if (intCountMatches != tags.size()) continue;
            intBiomeIDs.add(i);
        }
        return intBiomeIDs;
    }

    public static ArrayList<Integer> getBiomeIDsMatchingAllTagsExactly(ArrayList<EnumBiomeType> tags) {
        ArrayList<Integer> intBiomeIDs = new ArrayList<Integer>();
        for (int i = 0; i < biomeList.length; ++i) {
            int intCountMatches = 0;
            if (tags == null || tags.isEmpty() || biomeList[i] == null || PixelmonBiomeDictionary.biomeList[i].typeList == null) continue;
            for (EnumBiomeType tag : tags) {
                if (!PixelmonBiomeDictionary.biomeList[i].typeList.contains((Object)tag)) continue;
                ++intCountMatches;
            }
            if (intCountMatches != tags.size() || intCountMatches != PixelmonBiomeDictionary.biomeList[i].typeList.size()) continue;
            intBiomeIDs.add(i);
        }
        return intBiomeIDs;
    }

    public static void setupCustomBiomeTags() {
        PixelmonBiomeDictionary.registerBiomeType(0, "Ocean", EnumBiomeType.OCEAN);
        PixelmonBiomeDictionary.registerBiomeType(1, "Plains", EnumBiomeType.PLAINS);
        PixelmonBiomeDictionary.registerBiomeType(2, "Desert", EnumBiomeType.DESERT);
        PixelmonBiomeDictionary.registerBiomeType(3, "Extreme Hills", EnumBiomeType.EXTREMEHILLS);
        PixelmonBiomeDictionary.registerBiomeType(4, "Forest", EnumBiomeType.FOREST);
        PixelmonBiomeDictionary.registerBiomeType(5, "Taiga", EnumBiomeType.TAIGA);
        PixelmonBiomeDictionary.registerBiomeType(6, "Swampland", EnumBiomeType.SWAMP);
        PixelmonBiomeDictionary.registerBiomeType(7, "River", EnumBiomeType.RIVER);
        PixelmonBiomeDictionary.registerBiomeType(8, "Hell", EnumBiomeType.NETHER);
        PixelmonBiomeDictionary.registerBiomeType(9, "The End", EnumBiomeType.END);
        PixelmonBiomeDictionary.registerBiomeType(10, "FrozenOcean", EnumBiomeType.FROZEN, EnumBiomeType.OCEAN);
        PixelmonBiomeDictionary.registerBiomeType(11, "FrozenRiver", EnumBiomeType.FROZEN, EnumBiomeType.RIVER);
        PixelmonBiomeDictionary.registerBiomeType(12, "Ice Plains", EnumBiomeType.FROZEN, EnumBiomeType.PLAINS);
        PixelmonBiomeDictionary.registerBiomeType(13, "Ice Mountains", EnumBiomeType.FROZEN, EnumBiomeType.HILLS);
        PixelmonBiomeDictionary.registerBiomeType(14, "MushroomIsland", EnumBiomeType.MUSHROOM);
        PixelmonBiomeDictionary.registerBiomeType(15, "MushroomIslandShore", EnumBiomeType.BEACH, EnumBiomeType.MUSHROOM);
        PixelmonBiomeDictionary.registerBiomeType(16, "Beach", EnumBiomeType.BEACH);
        PixelmonBiomeDictionary.registerBiomeType(17, "DesertHills", EnumBiomeType.DESERT, EnumBiomeType.HILLS);
        PixelmonBiomeDictionary.registerBiomeType(18, "ForestHills", EnumBiomeType.FOREST, EnumBiomeType.HILLS);
        PixelmonBiomeDictionary.registerBiomeType(19, "TaigaHills", EnumBiomeType.HILLS, EnumBiomeType.TAIGA);
        PixelmonBiomeDictionary.registerBiomeType(20, "Extreme Hills Edge", EnumBiomeType.EDGE, EnumBiomeType.EXTREMEHILLS);
        PixelmonBiomeDictionary.registerBiomeType(21, "Jungle", EnumBiomeType.JUNGLE);
        PixelmonBiomeDictionary.registerBiomeType(22, "JungleHills", EnumBiomeType.JUNGLE, EnumBiomeType.HILLS);
        PixelmonBiomeDictionary.registerBiomeType(23, "JungleEdge", EnumBiomeType.EDGE, EnumBiomeType.JUNGLE);
        PixelmonBiomeDictionary.registerBiomeType(24, "Deep Ocean", EnumBiomeType.AMPLIFIED, EnumBiomeType.OCEAN);
        PixelmonBiomeDictionary.registerBiomeType(25, "Stone Beach", EnumBiomeType.AMPLIFIED, EnumBiomeType.BEACH);
        PixelmonBiomeDictionary.registerBiomeType(26, "Cold Beach", EnumBiomeType.BEACH, EnumBiomeType.FROZEN);
        PixelmonBiomeDictionary.registerBiomeType(27, "Birch Forest", EnumBiomeType.FOREST, EnumBiomeType.MUTATION);
        PixelmonBiomeDictionary.registerBiomeType(28, "Birch Forest Hills", EnumBiomeType.FOREST, EnumBiomeType.HILLS, EnumBiomeType.MUTATION);
        PixelmonBiomeDictionary.registerBiomeType(29, "Roofed Forest", EnumBiomeType.FOREST, EnumBiomeType.MUSHROOM, EnumBiomeType.MUTATION);
        PixelmonBiomeDictionary.registerBiomeType(30, "Cold Taiga", EnumBiomeType.FROZEN, EnumBiomeType.TAIGA);
        PixelmonBiomeDictionary.registerBiomeType(31, "Cold Taiga Hills", EnumBiomeType.FROZEN, EnumBiomeType.HILLS, EnumBiomeType.TAIGA);
        PixelmonBiomeDictionary.registerBiomeType(32, "Mega Taiga", EnumBiomeType.TAIGA, EnumBiomeType.MUTATION);
        PixelmonBiomeDictionary.registerBiomeType(33, "Mega Taiga Hills", EnumBiomeType.AMPLIFIED, EnumBiomeType.HILLS, EnumBiomeType.TAIGA);
        PixelmonBiomeDictionary.registerBiomeType(34, "Extreme Hills+", EnumBiomeType.EXTREMEHILLS, EnumBiomeType.MUTATION);
        PixelmonBiomeDictionary.registerBiomeType(35, "Savanna", EnumBiomeType.PLAINS, EnumBiomeType.MUTATION);
        PixelmonBiomeDictionary.registerBiomeType(36, "Savanna Plateau", EnumBiomeType.PLAINS, EnumBiomeType.MUTATION, EnumBiomeType.PLATEAU);
        PixelmonBiomeDictionary.registerBiomeType(37, "Mesa", EnumBiomeType.MESA);
        PixelmonBiomeDictionary.registerBiomeType(38, "Mesa Plateau F", EnumBiomeType.FOREST, EnumBiomeType.MESA, EnumBiomeType.PLATEAU);
        PixelmonBiomeDictionary.registerBiomeType(39, "Mesa Plateau", EnumBiomeType.MESA, EnumBiomeType.PLATEAU);
        PixelmonBiomeDictionary.registerBiomeType(129, "Sunflower Plains", EnumBiomeType.FLOWER, EnumBiomeType.MUTATION, EnumBiomeType.PLAINS);
        PixelmonBiomeDictionary.registerBiomeType(130, "Desert M", EnumBiomeType.DESERT, EnumBiomeType.MUTATION);
        PixelmonBiomeDictionary.registerBiomeType(131, "Extreme Hills M", EnumBiomeType.AMPLIFIED, EnumBiomeType.EXTREMEHILLS);
        PixelmonBiomeDictionary.registerBiomeType(132, "Flower Forest", EnumBiomeType.FLOWER, EnumBiomeType.FOREST, EnumBiomeType.MUTATION);
        PixelmonBiomeDictionary.registerBiomeType(133, "Taiga M", EnumBiomeType.AMPLIFIED, EnumBiomeType.TAIGA);
        PixelmonBiomeDictionary.registerBiomeType(134, "Swampland M", EnumBiomeType.AMPLIFIED, EnumBiomeType.SWAMP);
        PixelmonBiomeDictionary.registerBiomeType(140, "Ice Plains Spikes", EnumBiomeType.FROZEN, EnumBiomeType.MUTATION, EnumBiomeType.PLAINS);
        PixelmonBiomeDictionary.registerBiomeType(149, "Jungle M", EnumBiomeType.AMPLIFIED, EnumBiomeType.JUNGLE);
        PixelmonBiomeDictionary.registerBiomeType(151, "Jungle Edge M", EnumBiomeType.AMPLIFIED, EnumBiomeType.EDGE, EnumBiomeType.JUNGLE);
        PixelmonBiomeDictionary.registerBiomeType(155, "Birch Forest M", EnumBiomeType.AMPLIFIED, EnumBiomeType.FOREST, EnumBiomeType.MUTATION);
        PixelmonBiomeDictionary.registerBiomeType(156, "Birch Forest Hills M", EnumBiomeType.AMPLIFIED, EnumBiomeType.FOREST, EnumBiomeType.HILLS, EnumBiomeType.MUTATION);
        PixelmonBiomeDictionary.registerBiomeType(157, "Roofed Forest M", EnumBiomeType.AMPLIFIED, EnumBiomeType.FOREST, EnumBiomeType.MUSHROOM, EnumBiomeType.MUTATION);
        PixelmonBiomeDictionary.registerBiomeType(158, "Cold Taiga M", EnumBiomeType.AMPLIFIED, EnumBiomeType.FROZEN, EnumBiomeType.TAIGA);
        PixelmonBiomeDictionary.registerBiomeType(160, "Mega Spruce Taiga", EnumBiomeType.AMPLIFIED, EnumBiomeType.TAIGA, EnumBiomeType.MUTATION);
        PixelmonBiomeDictionary.registerBiomeType(161, "Redwood Taiga Hills M", EnumBiomeType.AMPLIFIED, EnumBiomeType.HILLS, EnumBiomeType.MUTATION, EnumBiomeType.TAIGA);
        PixelmonBiomeDictionary.registerBiomeType(162, "Extreme Hills+ M", EnumBiomeType.AMPLIFIED, EnumBiomeType.EXTREMEHILLS, EnumBiomeType.MUTATION);
        PixelmonBiomeDictionary.registerBiomeType(163, "Savanna M", EnumBiomeType.AMPLIFIED, EnumBiomeType.PLAINS, EnumBiomeType.MUTATION);
        PixelmonBiomeDictionary.registerBiomeType(164, "Savanna Plateau M", EnumBiomeType.AMPLIFIED, EnumBiomeType.MUTATION, EnumBiomeType.PLAINS, EnumBiomeType.PLATEAU);
        PixelmonBiomeDictionary.registerBiomeType(165, "Mesa (Bryce)", EnumBiomeType.MESA, EnumBiomeType.MUTATION);
        PixelmonBiomeDictionary.registerBiomeType(166, "Mesa Plateau F M", EnumBiomeType.AMPLIFIED, EnumBiomeType.FOREST, EnumBiomeType.MESA, EnumBiomeType.PLATEAU);
        PixelmonBiomeDictionary.registerBiomeType(167, "Mesa Plateau M", EnumBiomeType.AMPLIFIED, EnumBiomeType.MESA, EnumBiomeType.PLATEAU);
    }

    public static class PixelmonBiomeInfo {
        public String biomeName;
        public String biomeEnglishName;
        public ArrayList<EnumBiomeType> typeList = new ArrayList();

        public PixelmonBiomeInfo(String biomeName, String biomeEnglishName, EnumBiomeType[] types) {
            this.biomeName = biomeName;
            this.biomeEnglishName = biomeEnglishName;
            Collections.addAll(this.typeList, types);
        }

        public boolean isSameNameAs(String strTestBiomeName) {
            String tempBiomeName = RegexPatterns.SPACE_SYMBOL.matcher(this.biomeName).replaceAll("").trim();
            String tempBiomeEnglishName = RegexPatterns.SPACE_SYMBOL.matcher(this.biomeEnglishName).replaceAll("").trim();
            String tempTestName = RegexPatterns.SPACE_SYMBOL.matcher(strTestBiomeName).replaceAll("").trim();
            return tempBiomeName.equalsIgnoreCase(tempTestName) || tempBiomeEnglishName.equalsIgnoreCase(tempTestName);
        }
    }
}

