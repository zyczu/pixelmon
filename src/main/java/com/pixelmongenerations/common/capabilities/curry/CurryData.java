/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.common.capabilities.curry;

import com.pixelmongenerations.core.enums.EnumCurryTasteRating;
import com.pixelmongenerations.core.enums.EnumCurryType;
import com.pixelmongenerations.core.enums.EnumFlavor;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

public class CurryData {
    private EnumFlavor flavor = EnumFlavor.None;
    private EnumCurryType curryType = EnumCurryType.None;
    private int experience = 0;
    private double healthPercentage = 0.0;
    private boolean canHealStatus = false;
    private boolean canRestorePP = false;
    private int friendship = 0;
    private EnumCurryTasteRating rating = EnumCurryTasteRating.Koffing;

    public EnumFlavor getFlavor() {
        return this.flavor;
    }

    public EnumCurryType getCurryType() {
        return this.curryType;
    }

    public EnumCurryTasteRating getRating() {
        return this.rating;
    }

    public int getExperience() {
        return this.experience;
    }

    public double getHealthPercentage() {
        return this.healthPercentage;
    }

    public boolean canRestorePP() {
        return this.canRestorePP;
    }

    public boolean canHealStatus() {
        return this.canHealStatus;
    }

    public int getFriendship() {
        return this.friendship;
    }

    public CurryData setFlavor(EnumFlavor flavor) {
        this.flavor = flavor;
        return this;
    }

    public CurryData setCurryType(EnumCurryType curryType) {
        this.curryType = curryType;
        return this;
    }

    public CurryData setRating(EnumCurryTasteRating rating) {
        this.rating = rating;
        return this;
    }

    public CurryData setExperience(int experience) {
        this.experience = experience;
        return this;
    }

    public CurryData setHealthPercentage(double healthPercentage) {
        this.healthPercentage = healthPercentage;
        return this;
    }

    public CurryData setCanHealStatus(boolean canHealStatus) {
        this.canHealStatus = canHealStatus;
        return this;
    }

    public CurryData setCanRestorePP(boolean canRestorePP) {
        this.canRestorePP = canRestorePP;
        return this;
    }

    public CurryData setFriendship(int friendship) {
        this.friendship = friendship;
        return this;
    }

    public ResourceLocation getResourceLocation() {
        return this.getCurryType().getResourceLocation();
    }

    public NBTBase toNbt() {
        NBTTagCompound nbt = new NBTTagCompound();
        nbt.setInteger("flavor", this.flavor.getIndex());
        nbt.setInteger("type", this.curryType.getIndex());
        nbt.setInteger("rating", this.rating.ordinal());
        nbt.setInteger("experience", this.experience);
        nbt.setDouble("healthPercentage", this.healthPercentage);
        nbt.setBoolean("canHealStatus", this.canHealStatus);
        nbt.setBoolean("canRestorePP", this.canRestorePP);
        nbt.setInteger("friendship", this.friendship);
        return nbt;
    }

    public static CurryData fromNbt(NBTTagCompound nbt) {
        CurryData instance = new CurryData();
        if (nbt.isEmpty()) {
            return instance;
        }
        instance.setFlavor(EnumFlavor.geFlavorFromIndex(nbt.getInteger("flavor")));
        instance.setCurryType(EnumCurryType.getCurryTypeFromIndex(nbt.getInteger("type")));
        instance.setRating(EnumCurryTasteRating.fromId(nbt.getInteger("rating")));
        instance.setExperience(nbt.getInteger("experience"));
        instance.setHealthPercentage(nbt.getDouble("healthPercentage"));
        instance.setCanHealStatus(nbt.getBoolean("canHealStatus"));
        instance.setCanRestorePP(nbt.getBoolean("canRestorePP"));
        instance.setFriendship(nbt.getInteger("friendship"));
        return instance;
    }
}

