/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.enums;

public enum ReceiveType {
    Christmas,
    Halloween,
    Command,
    Evolution,
    Fossil,
    PokeBall,
    Starter,
    SelectPokemon,
    Trade;

}

