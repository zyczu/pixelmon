/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.economy;

import com.pixelmongenerations.api.economy.Transaction;
import java.util.UUID;

public interface AccountHolder {
    public int getBalance();

    public Transaction withdraw(int var1);

    public Transaction deposit(int var1);

    public Transaction set(int var1);

    public void setupAccount(UUID var1);
}

