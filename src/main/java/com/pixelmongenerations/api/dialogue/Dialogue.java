/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.api.dialogue;

import com.pixelmongenerations.api.dialogue.Choice;
import com.pixelmongenerations.api.events.dialogue.DialogueChoiceEvent;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.dialogue.SetDialogueData;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class Dialogue {
    public final String name;
    public final String text;
    public final List<Choice> choices;
    public final int column;

    public Dialogue(ByteBuf buffer) {
        this.name = ByteBufUtils.readUTF8String(buffer);
        this.text = ByteBufUtils.readUTF8String(buffer);
        ArrayList<Choice> choices = new ArrayList<Choice>();
        for (byte choiceCount = buffer.readByte(); choiceCount > 0; choiceCount = (byte)(choiceCount - 1)) {
            choices.add(new Choice(buffer));
        }
        this.choices = choices;
        this.column = buffer.readInt();
    }

    public Dialogue(String name, String text, List<Choice> choices) {
        this(name, text, choices, 1);
    }

    public Dialogue(String name, String text, List<Choice> choices, int column) {
        this.name = name;
        this.text = text;
        this.choices = choices == null ? new ArrayList() : choices;
        this.column = column;
    }

    public void writeToBytes(ByteBuf buffer) {
        ByteBufUtils.writeUTF8String(buffer, this.name);
        ByteBufUtils.writeUTF8String(buffer, this.text);
        buffer.writeByte(this.choices.size());
        for (Choice choice : this.choices) {
            choice.toBytes(buffer);
        }
        buffer.writeInt(this.column);
    }

    public static void setPlayerDialogueData(EntityPlayerMP player, ArrayList<Dialogue> dialogues, boolean openGui) {
        HashMap<Integer, Consumer<DialogueChoiceEvent>> choiceMap = new HashMap<Integer, Consumer<DialogueChoiceEvent>>();
        for (Dialogue dialogue : dialogues) {
            for (Choice choice : dialogue.choices) {
                choiceMap.put(choice.choiceID, choice.handle);
            }
        }
        Choice.handleMap.put(player.getUniqueID(), choiceMap);
        Pixelmon.NETWORK.sendTo(new SetDialogueData(dialogues, openGui), player);
    }

    public static DialogueBuilder builder() {
        return new DialogueBuilder();
    }

    public static enum EnumColumnStyle {
        SINGLE,
        DOUBLE,
        TRIPLE,
        QUADRUPLE,
        QUINTUPLE;


        public int getMultiplier() {
            return this.ordinal() + 1;
        }

        public static EnumColumnStyle getValue(int id) {
            return EnumColumnStyle.values()[id];
        }
    }

    public static class DialogueBuilder {
        private String name = "";
        private String text = "";
        private List<Choice> choices = new ArrayList<Choice>();
        private int column = 1;

        public Dialogue build() {
            return new Dialogue(this.name, this.text, this.choices, this.column);
        }

        public DialogueBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public DialogueBuilder setText(String text) {
            this.text = text;
            return this;
        }

        public DialogueBuilder addChoice(Choice choice) {
            this.choices.add(choice);
            return this;
        }

        public DialogueBuilder column(int column) {
            this.column = Math.max(column, 1);
            return this;
        }

        public DialogueBuilder setChoices(List<Choice> choices) {
            this.choices = choices;
            return this;
        }

        public DialogueBuilder setChoices(Stream<Choice> choices) {
            this.choices = choices.collect(Collectors.toList());
            return this;
        }
    }
}

