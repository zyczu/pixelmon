/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.Streams
 */
package com.pixelmongenerations.api.def;

import com.google.common.collect.Streams;
import com.pixelmongenerations.core.enums.EnumTutorType;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

public class MoveContainer {
    public List<String> alphaMoves;
    public HashMap<Integer, List<String>> levelMoves;
    public List<String> tmTRMoves;
    public HashMap<EnumTutorType, List<String>> tutorMoves;
    public List<String> evolutionMoves;
    public List<String> eggMoves;

    public boolean equals(Object object) {
        if (!(object instanceof MoveContainer)) {
            return false;
        }
        MoveContainer mc = (MoveContainer)object;
        if (!this.alphaMoves.equals(mc.alphaMoves)) {
            return false;
        }
        if (!this.levelMoves.equals(mc.levelMoves)) {
            return false;
        }
        if (!this.tmTRMoves.equals(mc.tmTRMoves)) {
            return false;
        }
        if (!this.eggMoves.equals(mc.eggMoves)) {
            return false;
        }
        if (!this.evolutionMoves.equals(mc.evolutionMoves)) {
            return false;
        }
        if (!this.tutorMoves.get((Object)EnumTutorType.Transfer).equals(mc.tutorMoves.get((Object)EnumTutorType.Transfer))) {
            return false;
        }
        if (!this.tutorMoves.get((Object)EnumTutorType.Regular).equals(mc.tutorMoves.get((Object)EnumTutorType.Regular))) {
            return false;
        }
        return this.tutorMoves.get((Object)EnumTutorType.Event).equals(mc.tutorMoves.get((Object)EnumTutorType.Event));
    }

    public boolean hasNoMoves() {
        if (this.alphaMoves != null && !this.alphaMoves.isEmpty()) {
            return false;
        }
        if (this.levelMoves != null && !this.levelMoves.isEmpty()) {
            return false;
        }
        if (this.tmTRMoves != null && this.tmTRMoves.size() != 0) {
            return false;
        }
        if (this.eggMoves != null && this.eggMoves.size() != 0) {
            return false;
        }
        if (this.evolutionMoves != null && this.evolutionMoves.size() != 0) {
            return false;
        }
        for (List<String> list : this.tutorMoves.values()) {
            if (list == null || list.size() == 0) continue;
            return false;
        }
        return true;
    }

    public boolean canLearnMove(String name) {
        return Streams.concat((Stream[])new Stream[]{this.levelMoves.values().stream(), this.alphaMoves.stream(), this.tmTRMoves.stream(), this.eggMoves.stream(), this.evolutionMoves.stream(), this.tutorMoves.values().stream().flatMap(Collection::stream)}).anyMatch(name::equals);
    }
}

