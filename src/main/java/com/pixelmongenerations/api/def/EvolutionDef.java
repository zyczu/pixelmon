/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.def;

import com.pixelmongenerations.api.def.NatureDef;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.ArrayList;
import java.util.Objects;

public class EvolutionDef {
    public Integer targetId;
    public Integer formTo = 0;
    public Integer evolveLevel;
    public String evolveCondition;
    public EnumSpecies targetSpecies;
    public ArrayList<NatureDef> natureEvolve;
    public boolean alcremieEvo;

    public boolean equals(Object object) {
        if (!(object instanceof EvolutionDef)) {
            return false;
        }
        EvolutionDef def = (EvolutionDef)object;
        if (!Objects.equals(def.targetId, this.targetId)) {
            return false;
        }
        if (!Objects.equals(def.formTo, this.formTo)) {
            return false;
        }
        if (!Objects.equals(def.evolveLevel, this.evolveLevel)) {
            return false;
        }
        if (this.evolveCondition != null && !def.evolveCondition.equals(this.evolveCondition)) {
            return false;
        }
        return this.natureEvolve == null || def.natureEvolve.equals(this.natureEvolve);
    }
}

