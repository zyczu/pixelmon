/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ArrayListMultimap
 *  com.google.common.collect.Multimap
 */
package com.pixelmongenerations.api.pokemon;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.pixelmongenerations.api.pokemon.ExtraSpecType;
import com.pixelmongenerations.api.pokemon.ExtraSpecValue;
import com.pixelmongenerations.api.pokemon.specs.UntradeableSpec;
import com.pixelmongenerations.client.models.PixelmonModelRegistry;
import com.pixelmongenerations.common.entity.pixelmon.Entity3HasStats;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.abilities.AbilityBase;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.EVsStore;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.stats.IVStore;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.common.entity.pixelmon.textures.EnumTextures;
import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;
import com.pixelmongenerations.core.config.PixelmonServerConfig;
import com.pixelmongenerations.core.enums.EnumBossMode;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.forms.EnumForms;
import com.pixelmongenerations.core.enums.forms.IEnumForm;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.PixelmonData;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.lang.invoke.LambdaMetafactory;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import net.minecraft.client.model.ModelBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class PokemonSpec {
    private static Multimap<ExtraSpecType, String> extraSpecTypes = ArrayListMultimap.create();
    public String[] args = new String[0];
    public int specialTexture = -1;
    public String name = null;
    public Integer level = null;
    public Integer gender = null;
    public Integer growth = null;
    public Integer nature = null;
    public Integer pseudonature = null;
    public String ability = null;
    public Integer boss = null;
    public Boolean shiny = null;
    public Integer form = null;
    public Integer ball = null;
    public Boolean totem = null;
    public Boolean alpha = null;
    public String customTexture = null;
    public String colorTint = null;
    public List<ExtraSpecValue> extraSpecs = null;
    public String particle = null;
    public Boolean hasGmaxFactor = null;
    public Boolean catchable = null;
    public Boolean breedable = null;
    public int[] ivs;
    public int[] evs;

    public String toString() {
        ArrayList<String> list = new ArrayList<String>();
        list.add(this.name);
        if (this.level != null) {
            list.add("level:" + this.level);
        }
        if (this.gender != null) {
            list.add("gender:" + this.gender);
        }
        if (this.growth != null) {
            list.add("growth:" + this.growth);
        }
        if (this.nature != null) {
            list.add("nature:" + this.nature);
        }
        if (this.pseudonature != null) {
            list.add("pseudonature:" + this.pseudonature);
        }
        if (this.ability != null) {
            list.add("ability:" + this.ability);
        }
        if (this.boss != null) {
            list.add("boss:" + this.boss);
        }
        if (this.shiny != null) {
            list.add(this.shiny != false ? "shiny" : "!shiny");
        }
        if (this.form != null) {
            list.add("form:" + this.form);
        }
        if (this.ball != null) {
            list.add("ball:" + this.ball);
        }
        if (this.extraSpecs != null) {
            this.extraSpecs.stream().map(ExtraSpecValue::serialize).forEach(list::add);
        }
        if (this.particle != null) {
            list.add("particle:" + this.particle);
        }
        if (this.totem != null) {
            list.add("totem: " + this.totem);
        }
        if (this.alpha != null) {
            list.add("alpha: " + this.alpha);
        }
        if (this.customTexture != null) {
            list.add("customtexture: " + this.customTexture);
        }
        if (this.colorTint != null) {
            list.add("colorTint: " + this.colorTint);
        }
        if (this.hasGmaxFactor != null) {
            list.add("hasGmaxFactor: " + this.hasGmaxFactor);
        }
        if (this.catchable != null) {
            list.add("catchable: " + this.catchable);
        }
        if (this.breedable != null) {
            list.add("breedable: " + this.breedable);
        }
        return String.join((CharSequence)" ", list);
    }

    public String toStringFull() {
        ArrayList<String> list = new ArrayList<String>();
        list.add(this.name);
        list.add("level:" + this.level);
        list.add("gender:" + this.gender);
        list.add("growth:" + this.growth);
        list.add("nature:" + this.nature);
        list.add("pseudonature:" + this.pseudonature);
        list.add("ability:" + this.ability);
        list.add("boss:" + this.boss);
        list.add(this.shiny != false ? "shiny" : "!shiny");
        list.add("form:" + this.form);
        list.add("ball:" + this.ball);
        this.extraSpecs.stream().map(ExtraSpecValue::serialize).forEach(list::add);
        list.add("particle:" + this.particle);
        list.add("totem: " + this.totem);
        list.add("alpha: " + this.alpha);
        list.add("customtexture: " + this.customTexture);
        list.add("colorTint: " + this.colorTint);
        list.add("hasGmaxFactor: " + this.hasGmaxFactor);
        return String.join((CharSequence)" ", list);
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    public PokemonSpec(String ... args) {
        if (args == null || args.length <= 0) return;

        for (String arg : args) {
            if (!arg.isEmpty()) {
                if (EnumSpecies.hasPokemonAnyCase(arg)) {
                    this.name = EnumSpecies.getFromNameAnyCase(arg).name;
                } else if (arg.toLowerCase().equals("random")) {
                    this.name = EnumSpecies.randomPoke().name;
                } else {
                    String value = null;
                    String key;
                    if (arg.contains(":")) {
                        key = arg.substring(0, arg.indexOf(58)).toLowerCase();
                        if (arg.length() > key.length() + 1) {
                            value = arg.substring(key.length() + 1);
                        }
                    } else {
                        key = arg.toLowerCase();
                    }

                    if (key.isEmpty()) {
                        return;
                    }

                    try {
                        switch (key) {
                            case "s":
                            case "shiny":
                                this.shiny = true;
                                break;
                            case "!s":
                            case "!shiny":
                                this.shiny = false;
                                break;
                            case "t":
                            case "totem":
                                this.totem = true;
                                break;
                            case "a":
                            case "alpha":
                                this.alpha = true;
                                break;
                            case "!a":
                            case "!alpha":
                                this.alpha = false;
                                break;
                            case "!t":
                            case "!totem":
                                this.totem = false;
                                break;
                            case "gmax":
                                this.hasGmaxFactor = true;
                                break;
                            case "!gmax":
                                this.hasGmaxFactor = false;
                                break;
                            case "level":
                            case "lvl":
                                if (value == null) {
                                    break;
                                }

                                this.level = Integer.parseInt(value);
                                if (this.level < 1 || this.level > PixelmonServerConfig.maxLevel) {
                                    this.level = null;
                                }
                                break;
                            case "levelrange":
                                if (value == null) {
                                    break;
                                }

                                int min = Integer.parseInt(value.split("-")[0]);
                                int max = Integer.parseInt(value.split("-")[1]);
                                this.level = RandomHelper.getRandomNumberBetween(min, max);
                                if (this.level < 1 || this.level > PixelmonServerConfig.maxLevel) {
                                    this.level = null;
                                }

                                break;
                            case "ivperc":
                            case "ivpercent":
                                if (value == null) break;
                                double percent = Double.parseDouble(value);
                                this.ivs = IVStore.createPercentIVs(percent).getArray();
                                break;
                            case "g":
                            case "gender":
                                if (value != null) {
                                    if (Gender.getGender(value) != null) {
                                        this.gender = Gender.getGender(value).ordinal();
                                    } else {
                                        this.gender = Gender.getGender(Short.parseShort(value)).ordinal();
                                    }
                                }
                                break;
                            case "n":
                            case "nature":
                                if (value != null) {
                                    if (EnumNature.hasNature(value)) {
                                        this.nature = EnumNature.natureFromString(value).index;
                                    } else {
                                        this.nature = EnumNature.getNatureFromIndex(Integer.parseInt(value)).index;
                                    }
                                }
                                break;
                            case "pseudonature":
                                if (value != null) {
                                    if (EnumNature.hasNature(value)) {
                                        this.pseudonature = EnumNature.natureFromString(value).index;
                                    } else {
                                        this.pseudonature = EnumNature.getNatureFromIndex(Integer.parseInt(value)).index;
                                    }
                                }
                                break;
                            case "gr":
                            case "growth":
                                if (value != null) {
                                    if (EnumGrowth.hasGrowth(value)) {
                                        this.growth = EnumGrowth.growthFromString(value).index;
                                    } else {
                                        this.growth = EnumGrowth.getGrowthFromIndex(Integer.parseInt(value)).index;
                                    }
                                }
                                break;
                            case "ab":
                            case "ability":
                                if (value != null) {
                                    this.ability = AbilityBase.getAbility(value).map(a -> a.getName()).orElse(null);
                                }
                                break;
                            case "boss":
                            case "b":
                                if (value != null) {
                                    if (EnumBossMode.hasBossMode(value)) {
                                        this.boss = EnumBossMode.getBossMode(value).index;
                                    } else {
                                        this.boss = EnumBossMode.getMode(Integer.parseInt(value)).index;
                                    }

                                    if (this.boss == EnumBossMode.Equal.index) {
                                        this.boss = null;
                                    }
                                }
                                break;
                            case "form":
                            case "f":
                                if (value != null) {
                                    int tempSpecial = 0;

                                    try {
                                        tempSpecial = Integer.parseInt(value);
                                    } catch (Exception var15) {
                                        tempSpecial = EnumSpecies.getFormFromName(EnumSpecies.getFromNameAnyCase(this.name), value);
                                    }

                                    this.form = tempSpecial;
                                }
                                break;
                            case "ball":
                            case "ba":
                            case "pokeball":
                                if (value == null) {
                                    break;
                                }

                                for (EnumPokeball pokeball : EnumPokeball.values()) {
                                    if (pokeball.name().toLowerCase().contains(value.toLowerCase())) {
                                        this.ball = pokeball.getIndex();
                                    }
                                }

                                this.ball = EnumPokeball.getFromIndex(Integer.parseInt(value)).getIndex();
                                break;
                            case "special":
                            case "sp":
                                if (value == null) {
                                    break;
                                }

                                int tempSpecial = 0;

                                try {
                                    tempSpecial = Integer.parseInt(value);
                                } catch (Exception var18) {
                                    Optional<EnumSpecies> species = EnumSpecies.getFromName(this.name);
                                    if (species.isPresent()) {
                                        IEnumForm iEnumForm = ((EnumSpecies)species.get()).getFormEnum(this.form != null ? this.form : 0);
                                        String valueLowerCase = value.toLowerCase();
                                        tempSpecial = ((EnumSpecies)species.get()).getSpecialTextures(iEnumForm).stream().filter((a) -> {
                                            return a.name().toLowerCase().equals(valueLowerCase);
                                        }).findAny().orElse(EnumTextures.None).getId();
                                    }
                                }

                                this.specialTexture = tempSpecial;
                                break;
                            case "p":
                            case "particle":
                                if (value != null) {
                                    this.particle = value;
                                }
                                break;
                            case "ct":
                            case "customtexture":
                                this.customTexture = value;
                                break;
                            case "ivs":
                                if (value == null) {
                                    break;
                                }

                                try {
                                    int[] tempIVs = new int[6];
                                    String[] stringIVs = value.split("/");
                                    if (stringIVs.length != 6) {
                                        break;
                                    }

                                    for(int i = 0; i < 6; ++i) {
                                        int iv = Integer.parseInt(stringIVs[i]);
                                        tempIVs[i] = iv;
                                    }

                                    this.ivs = tempIVs;
                                } catch (Exception var17) {
                                    var17.printStackTrace();
                                }
                                break;
                            case "evs":
                                if (value == null) {
                                    break;
                                }

                                try {
                                    int[] tempEVs = new int[6];
                                    String[] stringEVs = value.split("/");
                                    if (stringEVs.length != 6) {
                                        break;
                                    }

                                    for(int i = 0; i < 6; ++i) {
                                        int ev = Integer.parseInt(stringEVs[i]);
                                        tempEVs[i] = ev;
                                    }

                                    this.evs = tempEVs;
                                } catch (Exception var16) {
                                    var16.printStackTrace();
                                }
                                break;
                            case "generation":
                                tempSpecial = Integer.parseInt(value);
                                this.name = EnumSpecies.randomPokeFromGeneration(tempSpecial).name;
                                break;
                            case "legendary":
                                boolean legendary = Boolean.parseBoolean(value);
                                if (legendary) {
                                    this.name = EnumSpecies.randomLegendary().name;
                                } else {
                                    this.name = EnumSpecies.randomPoke(false).name;
                                }
                                break;
                            case "tint":
                                this.colorTint = value;
                                break;
                            case "catchable":
                            case "catch":
                                this.catchable = true;
                                break;
                            case "!catchable":
                            case "!catch":
                                this.catchable = false;
                                break;
                            case "breedable":
                            case "breed":
                                this.breedable = true;
                                break;
                            case "!breedable":
                            case "!breed":
                                this.breedable = false;
                                break;
                            default:
                                ExtraSpecType extraSpec = findExtraSpec(key);
                                if (extraSpec != null) {
                                    if (this.extraSpecs == null) {
                                        this.extraSpecs = new ArrayList();
                                    }

                                    this.extraSpecs.add(extraSpec.create(extraSpec.parse(EnumSpecies.getFromNameAnyCase(this.name), value)));
                                }
                        }
                    } catch (Exception var19) {
                    }
                }
            }
        }
    }

    public PokemonSpec(NBTTagCompound pokemon) {
        this.unapply(pokemon);
    }

    public static PokemonSpec from(String ... args) {
        return new PokemonSpec(args);
    }

    public static PokemonSpec from(EnumSpecies species) {
        return new PokemonSpec(species.name);
    }

    public static PokemonSpec fromString(String line) {
        return new PokemonSpec(line.split(" "));
    }

    public static void registerExtraSpec(ExtraSpecType extraSpec) {
        extraSpecTypes.put(extraSpec, extraSpec.getKey());
        extraSpecTypes.putAll(extraSpec, extraSpec.getAlternativeKeys());
    }

    private static ExtraSpecType findExtraSpec(String key) {
        return extraSpecTypes.entries().stream().filter(e -> ((String)e.getValue()).equals(key)).map(Map.Entry::getKey).findAny().orElse(null);
    }

    public PokemonSpec copy() {
        PokemonSpec copy = new PokemonSpec(new String[0]);
        try {
            for (Field field : this.getClass().getFields()) {
                if (Modifier.isFinal(field.getModifiers()) || Modifier.isStatic(field.getModifiers()) || field.getName().equals("extraSpecs")) continue;
                field.set(copy, field.get(this));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if (this.extraSpecs != null) {
            copy.extraSpecs = new ArrayList<ExtraSpecValue>();
            for (ExtraSpecValue extraSpec : this.extraSpecs) {
                copy.extraSpecs.add(extraSpec.getType().create(extraSpec.getValue()));
            }
        }
        return copy;
    }

    public boolean matches(NBTTagCompound nbt) {
        if (this.name != null && !nbt.getString("Name").equals(this.name)) {
            return false;
        }
        if (this.level != null && nbt.getInteger("Level") != this.level.intValue()) {
            return false;
        }
        if (this.gender != null && nbt.getShort("Gender") != this.gender.intValue()) {
            return false;
        }
        if (this.growth != null && nbt.getShort("Growth") != this.growth.intValue()) {
            return false;
        }
        if (this.nature != null && nbt.getShort("Nature") != this.nature.intValue()) {
            return false;
        }
        if (this.pseudonature != null && nbt.getShort("PseudoNature") != this.pseudonature.intValue()) {
            return false;
        }
        if (this.ability != null && !nbt.getString("Ability").equals(this.ability)) {
            return false;
        }
        if (this.boss != null && nbt.getShort("BossMode") != this.boss.intValue()) {
            return false;
        }
        if (this.shiny != null && nbt.getBoolean("IsShiny") != this.shiny.booleanValue()) {
            return false;
        }
        if (this.form != null && nbt.getShort("Variant") != this.form.intValue()) {
            return false;
        }
        if (this.ball != null && nbt.getInteger("CaughtBall") != this.ball.intValue()) {
            return false;
        }
        if (this.extraSpecs != null) {
            for (ExtraSpecValue extraSpec : this.extraSpecs) {
                if (extraSpec.matches(nbt)) continue;
                return false;
            }
        }
        if (this.particle != null && !nbt.getString("ParticleId").equals(this.particle)) {
            return false;
        }
        return this.customTexture == null || nbt.getString("CustomTexture").equals(this.customTexture);
    }

    public boolean matches(EntityPixelmon pokemon) {
        if (this.name != null && !pokemon.getPokemonName().equals(this.name)) {
            return false;
        }
        if (this.level != null && pokemon.getLvl().getLevel() != this.level.intValue()) {
            return false;
        }
        if (this.gender != null && pokemon.getGender().ordinal() != this.gender.intValue()) {
            return false;
        }
        if (this.growth != null && pokemon.getGrowth().index != this.growth) {
            return false;
        }
        if (this.nature != null && pokemon.getNature().index != this.nature) {
            return false;
        }
        if (this.pseudonature != null && pokemon.getPseudoNature().index != this.pseudonature) {
            return false;
        }
        if (this.ability != null && !pokemon.getAbility().getName().equals(this.ability)) {
            return false;
        }
        if (this.boss != null && pokemon.getBossMode().index != this.boss) {
            return false;
        }
        if (this.shiny != null && pokemon.isShiny() != this.shiny.booleanValue()) {
            return false;
        }
        if (this.hasGmaxFactor != null && pokemon.hasGmaxFactor() != this.hasGmaxFactor.booleanValue()) {
            return false;
        }
        if (this.form != null && pokemon.getForm() != this.form.intValue()) {
            return false;
        }
        if (this.ball != null && pokemon.caughtBall.getIndex() != this.ball.intValue()) {
            return false;
        }
        if (this.extraSpecs != null) {
            for (ExtraSpecValue extraSpec : this.extraSpecs) {
                if (extraSpec.matches(pokemon)) continue;
                return false;
            }
        }
        if (this.particle != null && !pokemon.getParticleId().equals(this.particle)) {
            return false;
        }
        return !(this.customTexture != null & !pokemon.getCustomSpecialTexture().equals(this.customTexture));
    }

    public EntityPixelmon create(World world) {
        if (this.name == null) {
            return null;
        }
        EnumSpecies species = EnumSpecies.getFromNameAnyCase(this.name);
        if (species == null) {
            System.out.println("ERROR CREATING POKEMON : " + this.name);
            return null;
        }
        EntityPixelmon pokemon = new EntityPixelmon(world);
        if (this.form != null) {
            pokemon.setForm(this.form, false);
        } else if (species.getNumForms(false) > 1) {
            ModelBase noFormModel;
            boolean setForm = false;
            for (IEnumForm form : EnumSpecies.formList.get(species)) {
                if (!form.isDefaultForm()) continue;
                pokemon.setForm(form.getForm(), false);
                setForm = true;
            }
            if (!setForm && (noFormModel = PixelmonModelRegistry.getModel(pokemon.getSpecies(), EnumForms.NoForm)) != null) {
                pokemon.setForm(EnumForms.NoForm.getForm(), false);
                setForm = true;
            }
            if (!setForm && pokemon.getForm() == -1) {
                pokemon.setForm(EntityPixelmon.getRandomForm(species), false);
            }
        }
        if (this.gender != null) {
            pokemon.setGender(Gender.getGender(this.gender.shortValue()));
        }
        if (this.hasGmaxFactor != null) {
            pokemon.setGmaxFactor(this.hasGmaxFactor);
        }
        if (this.customTexture != null) {
            pokemon.setCustomSpecialTexture(this.customTexture);
        }
        if (this.colorTint != null) {
            pokemon.setColorTint(this.colorTint);
        }
        if (this.alpha != null) {
            pokemon.setAlphaFactor(this.alpha);
        }
        pokemon.init(this.name);
        pokemon.isInitialised = true;
        this.apply(pokemon);
        return pokemon;
    }

    public void apply(NBTTagCompound nbt) {
        if (this.name != null) {
            nbt.setString("SpecName", this.name);
        }
        if (this.level != null) {
            nbt.setInteger("Level", this.level);
        }
        if (this.gender != null) {
            nbt.setShort("Gender", this.gender.shortValue());
        }
        if (this.growth != null) {
            nbt.setShort("Growth", this.growth.shortValue());
        }
        if (this.nature != null) {
            nbt.setShort("Nature", this.nature.shortValue());
        }
        if (this.pseudonature != null) {
            nbt.setShort("PseudoNature", this.pseudonature.shortValue());
        }
        if (this.ability != null) {
            nbt.setString("Ability", this.ability);
        }
        if (this.boss != null) {
            nbt.setShort("BossMode", this.boss.shortValue());
        }
        if (this.shiny != null) {
            nbt.setBoolean("IsShiny", this.shiny);
        }
        if (this.form != null) {
            nbt.setShort("Variant", this.form.shortValue());
        }
        if (this.ball != null) {
            nbt.setInteger("CaughtBall", this.ball);
        }
        if (this.specialTexture != -1) {
            nbt.setShort("specialTexture", (short)this.specialTexture);
        }
        if (this.extraSpecs != null) {
            for (ExtraSpecValue extraSpec : this.extraSpecs) {
                extraSpec.apply(nbt);
            }
        }
        if (this.particle != null) {
            nbt.setString("ParticleId", this.particle);
        }
        if (this.ivs != null) {
            nbt.setInteger("IVHP", this.ivs[0]);
            nbt.setInteger("IVAttack", this.ivs[1]);
            nbt.setInteger("IVDefence", this.ivs[2]);
            nbt.setInteger("IVSpAtt", this.ivs[3]);
            nbt.setInteger("IVSpDef", this.ivs[4]);
            nbt.setInteger("IVSpeed", this.ivs[5]);
        }
        if (this.evs != null) {
            nbt.setInteger("EVHP", this.evs[0]);
            nbt.setInteger("EVAttack", this.evs[1]);
            nbt.setInteger("EVDefence", this.evs[2]);
            nbt.setInteger("EVSpecialAttack", this.evs[3]);
            nbt.setInteger("EVSpecialDefence", this.evs[4]);
            nbt.setInteger("EVSpeed", this.evs[5]);
        }
        if (this.customTexture != null) {
            nbt.setString("CustomTexture", this.customTexture);
        }
        if (this.breedable != null) {
            nbt.setBoolean("Breedable", this.breedable);
        }
        if (this.catchable != null) {
            nbt.setBoolean("Catchable", this.catchable);
        }
    }

    public void unapply(NBTTagCompound nbt) {
        if (nbt.hasKey("SpecName")) {
            this.name = nbt.getString("SpecName");
        }
        if (nbt.hasKey("Level")) {
            this.level = nbt.getInteger("Level");
        }
        if (nbt.hasKey("Gender")) {
            this.gender = (int) nbt.getShort("Gender");
        }
        if (nbt.hasKey("Growth")) {
            this.growth = (int) nbt.getShort("Growth");
        }
        if (nbt.hasKey("Nature")) {
            this.nature = (int) nbt.getShort("Nature");
        }
        if (nbt.hasKey("PseudoNature")) {
            this.pseudonature = (int) nbt.getShort("PseudoNature");
        }
        if (nbt.hasKey("Ability")) {
            this.ability = nbt.getString("Ability");
        }
        if (nbt.hasKey("BossMode")) {
            this.boss = (int) nbt.getShort("BossMode");
        }
        if (nbt.hasKey("IsShiny")) {
            this.shiny = nbt.getBoolean("IsShiny");
        }
        if (nbt.hasKey("Variant")) {
            this.form = (int) nbt.getShort("Variant");
        }
        if (nbt.hasKey("CaughtBall")) {
            this.ball = nbt.getInteger("CaughtBall");
        }
        if (nbt.hasKey("specialTexture")) {
            this.specialTexture = nbt.getShort("specialTexture");
        }
        if (nbt.hasKey("CustomTexture")) {
            this.customTexture = nbt.getString("CustomTexture");
        }
    }

    public void apply(PokemonLink link) {
        if (this.level != null) {
            link.setLevel(this.level);
        }
        if (this.gender != null) {
            link.setGender(Gender.getGender(this.gender.shortValue()));
            BaseStats bs = link.getBaseStats();
            if (bs.malePercent < 0) {
                link.setGender(Gender.None);
            } else if (bs.malePercent == 0) {
                link.setGender(Gender.Female);
            } else if (bs.malePercent == 100) {
                link.setGender(Gender.Male);
            } else if (bs.malePercent > 0 && link.getGender() == Gender.None) {
                link.setGender(Entity3HasStats.getRandomGender(bs));
            }
        }
        if (this.growth != null) {
            link.setGrowth(EnumGrowth.getGrowthFromIndex(this.growth));
        }
        if (this.nature != null) {
            link.setNature(EnumNature.getNatureFromIndex(this.nature));
        }
        if (this.pseudonature != null) {
            link.setPseudoNature(EnumNature.getNatureFromIndex(this.pseudonature));
        }
        if (this.form != null) {
            link.setForm(this.form);
        }
        if (this.ability != null) {
            link.setAbility(this.ability);
        }
        if (this.boss != null) {
            link.setBossMode(EnumBossMode.getMode(this.boss));
        }
        if (this.shiny != null) {
            link.setShiny(this.shiny);
        }
        if (this.ball != null) {
            link.setCaughtBall(EnumPokeball.getFromIndex(this.ball));
        }
        if (this.specialTexture >= 0) {
            link.setSpecialTexture(this.specialTexture);
        }
        if (this.extraSpecs != null) {
            for (ExtraSpecValue extraSpec : this.extraSpecs) {
                extraSpec.apply(link.getNBT());
            }
        }
        if (this.particle != null && !this.particle.isEmpty()) {
            link.setParticleId(this.particle);
        }
        if (this.ivs != null) {
            link.getStats().IVs = new IVStore(this.ivs);
        }
        if (this.evs != null) {
            link.getStats().EVs = new EVsStore(this.evs);
        }
        if (this.totem != null) {
            link.setTotem(this.totem);
        }
        if (this.alpha != null) {
            link.setAlpha(this.alpha);
        }
        if (this.breedable != null) {
            link.setBreedable(this.breedable);
        }
        if (this.catchable != null) {
            link.setCatchable(this.catchable);
        }
        if (this.customTexture != null) {
            link.setCustomTexture(this.customTexture);
        }
    }

    public void apply(EntityPixelmon pokemon) {
        if (this.level != null) {
            pokemon.getLvl().setLevel(this.level);
        }
        if (this.gender != null) {
            pokemon.setGender(Gender.getGender(this.gender.shortValue()));
            if (pokemon.baseStats.malePercent < 0) {
                pokemon.setGender(Gender.None);
            } else if (pokemon.baseStats.malePercent == 0) {
                pokemon.setGender(Gender.Female);
            } else if (pokemon.baseStats.malePercent == 100) {
                pokemon.setGender(Gender.Male);
            } else if (pokemon.baseStats.malePercent > 0 && pokemon.getGender() == Gender.None) {
                pokemon.chooseRandomGender();
            }
        }
        if (this.growth != null) {
            pokemon.setGrowth(EnumGrowth.getGrowthFromIndex(this.growth));
        }
        if (this.nature != null) {
            pokemon.setNature(EnumNature.getNatureFromIndex(this.nature));
        }
        if (this.pseudonature != null) {
            pokemon.setPseudoNature(EnumNature.getNatureFromIndex(this.pseudonature));
        }
        if (this.form != null) {
            pokemon.setForm(this.form);
        }
        if (this.ability != null) {
            pokemon.setAbility(this.ability);
            pokemon.giveAbilitySlot();
        }
        if (this.boss != null) {
            pokemon.setBoss(EnumBossMode.getMode(this.boss));
        }
        if (this.shiny != null) {
            pokemon.setShiny(this.shiny);
        }
        if (this.ball != null) {
            pokemon.caughtBall = EnumPokeball.getFromIndex(this.ball);
        }
        if (this.specialTexture >= 0) {
            pokemon.setSpecialTexture(this.specialTexture);
        }
        if (this.extraSpecs != null) {
            for (ExtraSpecValue extraSpec : this.extraSpecs) {
                extraSpec.apply(pokemon);
            }
        }
        if (this.particle != null && !this.particle.isEmpty()) {
            pokemon.setParticleId(this.particle);
        }
        if (this.ivs != null) {
            pokemon.stats.IVs = new IVStore(this.ivs);
        }
        if (this.evs != null) {
            pokemon.stats.EVs = new EVsStore(this.evs);
        }
        if (this.totem != null) {
            pokemon.setTotem(this.totem);
        }
        if (this.alpha != null) {
            pokemon.setAlpha(this.alpha);
        }
        if (this.customTexture != null) {
            pokemon.setCustomSpecialTexture(this.customTexture);
        }
        if (this.breedable != null) {
            pokemon.setBreedable(this.breedable);
        }
        if (this.catchable != null) {
            pokemon.setCatchable(this.catchable);
        }
    }

    public PokemonSpec fromData(PixelmonData pixelmonData) {
        this.name = pixelmonData.name;
        this.level = pixelmonData.lvl;
        this.gender = (int) pixelmonData.gender.getForm();
        this.growth = pixelmonData.growth.scaleOrdinal;
        this.nature = pixelmonData.nature.index;
        this.pseudonature = pixelmonData.pseudoNature.index;
        this.ability = pixelmonData.ability;
        this.boss = pixelmonData.bossMode.index;
        this.shiny = pixelmonData.isShiny;
        this.form = (int) pixelmonData.form;
        this.ball = pixelmonData.pokeball.getIndex();
        this.customTexture = pixelmonData.customTexture;
        this.breedable = pixelmonData.breedable;
        return this;
    }

    public static void registerDefaultExtraSpecs() {
        PokemonSpec.registerExtraSpec(new UntradeableSpec());
    }

    public EnumSpecies getSpecies() {
        return EnumSpecies.getFromNameAnyCase(this.name);
    }

    public PokemonSpec setParticleId(String particleId) {
        this.particle = particleId;
        return this;
    }

    public PokemonSpec setForm(int givenForm) {
        this.form = givenForm;
        return this;
    }

    public PokemonSpec setLevel(int level) {
        this.level = level;
        return this;
    }

    public PokemonSpec setGender(Gender gender) {
        this.gender = gender.ordinal();
        return this;
    }

    public PokemonSpec setCustomTexture(String customTexture) {
        this.customTexture = customTexture;
        return this;
    }

}

