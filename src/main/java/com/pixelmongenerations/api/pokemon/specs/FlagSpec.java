/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.api.pokemon.specs;

import com.pixelmongenerations.api.pokemon.ExtraSpecType;
import com.pixelmongenerations.api.pokemon.ExtraSpecValue;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import javax.annotation.Nullable;
import net.minecraft.nbt.NBTTagCompound;

public abstract class FlagSpec
implements ExtraSpecType<Void>,
ExtraSpecValue<ExtraSpecType<Void>, Void> {
    protected abstract FlagSpec create();

    @Override
    public Void parse(EnumSpecies species, @Nullable String value) {
        return null;
    }

    public FlagSpec create(@Nullable Void value) {
        return this.create();
    }

    @Override
    public Void getValue() {
        return null;
    }

    @Override
    public FlagSpec getType() {
        return this;
    }

    @Override
    public boolean matches(EntityPixelmon pokemon) {
        return pokemon.getEntityData().hasKey(this.getKey());
    }

    @Override
    public boolean matches(NBTTagCompound nbt) {
        return nbt.getCompoundTag("ForgeData").hasKey(this.getKey());
    }

    @Override
    public void apply(NBTTagCompound nbt) {
        nbt.getCompoundTag("ForgeData").setBoolean(this.getKey(), true);
    }

    @Override
    public void apply(EntityPixelmon pokemon) {
        pokemon.getEntityData().setBoolean(this.getKey(), true);
    }

    @Override
    public String serialize() {
        return this.getKey();
    }
}

