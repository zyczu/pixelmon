/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.pc;

import com.pixelmongenerations.api.pc.PCBackground;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PCServer;
import java.util.function.Predicate;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;

public class RecipeUnlockedBackground
extends PCBackground {
    private Predicate<ItemStack> itemChecker;

    public RecipeUnlockedBackground(String id, String name, Predicate<ItemStack> itemChecker) {
        super(id, name);
        this.itemChecker = itemChecker;
    }

    public void onItemCrafted(EntityPlayerMP player, ItemStack itemStack) {
        boolean unlocked = super.hasUnlocked(player);
        if (unlocked) {
            return;
        }
        if (this.itemChecker.test(itemStack)) {
            PCServer.giveBackground(player, this.getId());
            ChatHandler.sendChat(player, "pixelmon.backgrounds.unlocked", this.getName());
        }
    }
}

