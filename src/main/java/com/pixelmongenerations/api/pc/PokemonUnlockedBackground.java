/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.pc;

import com.pixelmongenerations.api.pc.PCBackground;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PCServer;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;

public class PokemonUnlockedBackground
extends PCBackground {
    private EnumSpecies[] pokemon;

    public PokemonUnlockedBackground(String id, String name, EnumSpecies ... pokemon) {
        super(id, name);
        this.pokemon = pokemon;
    }

    @Override
    public boolean hasUnlocked(EntityPlayerMP player) {
        boolean unlocked = super.hasUnlocked(player);
        if (unlocked) {
            return true;
        }
        Optional<PlayerStorage> playerStorageOpt = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
        if (!playerStorageOpt.isPresent()) {
            return false;
        }
        PlayerStorage playerStorage = playerStorageOpt.get();
        for (EnumSpecies species : this.pokemon) {
            if (!playerStorage.pokedex.hasCaught(species)) continue;
            PCServer.giveBackground(player, this.getId());
            return true;
        }
        return false;
    }
}

