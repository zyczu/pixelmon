/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.pc;

import com.pixelmongenerations.api.pc.PCBackground;
import com.pixelmongenerations.core.network.ChatHandler;
import com.pixelmongenerations.core.storage.PCServer;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.function.Predicate;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.biome.Biome;

public class BiomeUnlockedBackground
extends PCBackground {
    private Predicate<String> biomeChecker;

    public BiomeUnlockedBackground(String id, String name, Predicate<String> biomeChecker) {
        super(id, name);
        this.biomeChecker = biomeChecker;
    }

    public void rollBiomeUnlock(EntityPlayerMP player) {
        boolean unlocked = super.hasUnlocked(player);
        if (unlocked) {
            return;
        }
        Biome biome = player.world.getBiome(player.getPosition());
        String biomeId = biome.getRegistryName().toString();
        if (this.biomeChecker.test(biomeId) && RandomHelper.getRandomNumberBetween(1, 5) == 1) {
            PCServer.giveBackground(player, this.getId());
            ChatHandler.sendChat(player, "pixelmon.backgrounds.unlocked", this.getName());
        }
    }
}

