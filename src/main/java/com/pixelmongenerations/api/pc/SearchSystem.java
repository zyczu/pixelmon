/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.pc;

import com.pixelmongenerations.api.Tuple;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.Gender;
import com.pixelmongenerations.common.entity.pixelmon.textures.IEnumSpecialTexture;
import com.pixelmongenerations.core.enums.EnumGrowth;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.enums.EnumType;
import com.pixelmongenerations.core.enums.items.EnumPokeball;
import com.pixelmongenerations.core.network.PixelmonData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.BiPredicate;

public class SearchSystem {
    private static HashMap<String, BiPredicate<PixelmonData, Object>> filters = new HashMap();
    private static final String SHINY = "shiny";
    private static final String NATURE = "nature";
    private static final String GENDER = "gender";
    private static final String IVS = "ivs";
    private static final String GROWTH = "growth";
    private static final String GMAX = "gmax";
    private static final String LEVEL = "level";
    private static final String BALL = "ball";
    private static final String SPECIAL = "special";
    private static final String TYPE = "type";
    private static final String ABILITY = "ability";
    private static final String LEGENDARY = "legendary";
    private ArrayList<Tuple<String, Object>> searchOptions;
    private String remainingString;

    public void updateSearch(String[] args) {
        this.remainingString = "";
        this.searchOptions = new ArrayList();
        StringBuilder remainingStringBuilder = new StringBuilder();
        if (args == null || args.length == 0) {
            return;
        }
        block33: for (String arg : args) {
            String key;
            if (arg.isEmpty()) continue;
            String value = null;
            if (arg.contains(":")) {
                key = arg.substring(0, arg.indexOf(58)).toLowerCase();
                if (arg.length() > key.length() + 1) {
                    value = arg.substring(key.length() + 1);
                }
            } else {
                key = arg.toLowerCase();
            }
            if (key.isEmpty()) continue;
            switch (key) {
                case "shiny": {
                    this.searchOptions.add(Tuple.of(SHINY, value != null ? this.formatValue(value) : ""));
                    continue block33;
                }
                case "gmax": {
                    this.searchOptions.add(Tuple.of(GMAX, value != null ? this.formatValue(value) : ""));
                    continue block33;
                }
                case "na": 
                case "nature": {
                    this.searchOptions.add(Tuple.of(NATURE, value != null ? this.formatValue(value) : ""));
                    continue block33;
                }
                case "gender": {
                    this.searchOptions.add(Tuple.of(GENDER, value != null ? this.formatValue(value) : ""));
                    continue block33;
                }
                case "type": {
                    this.searchOptions.add(Tuple.of(TYPE, value != null ? this.formatValue(value) : ""));
                    continue block33;
                }
                case "gr": 
                case "growth": {
                    this.searchOptions.add(Tuple.of(GROWTH, value != null ? this.formatValue(value) : ""));
                    continue block33;
                }
                case "lvl": 
                case "level": {
                    this.searchOptions.add(Tuple.of(LEVEL, value != null ? this.formatValue(value) : ""));
                    continue block33;
                }
                case "ab": 
                case "ability": {
                    this.searchOptions.add(Tuple.of(ABILITY, value != null ? this.formatValue(value) : ""));
                    continue block33;
                }
                case "legend": 
                case "legendary": {
                    this.searchOptions.add(Tuple.of(LEGENDARY, value != null ? this.formatValue(value) : ""));
                    continue block33;
                }
                case "ivs": {
                    this.searchOptions.add(Tuple.of(IVS, value != null ? this.formatValue(value) : ""));
                    continue block33;
                }
                case "st": 
                case "specialtexture": 
                case "special": {
                    this.searchOptions.add(Tuple.of(SPECIAL, value != null ? this.formatValue(value) : ""));
                    continue block33;
                }
                default: {
                    String extra = "";
                    if (value != null) {
                        extra = ":" + value;
                    }
                    remainingStringBuilder.append(key + extra);
                }
            }
        }
        this.remainingString = remainingStringBuilder.toString();
    }

    public boolean match(PixelmonData pokemon) {
        for (Tuple<String, Object> param : this.searchOptions) {
            try {
                if (filters.get(param.getFirst()).test(pokemon, param.getSecond())) continue;
                return false;
            }
            catch (Exception ignored) {
                return false;
            }
        }
        return true;
    }

    public String getRemainingString() {
        return this.remainingString;
    }

    private Object formatValue(String value) {
        if (value.equals("true")) {
            return true;
        }
        if (value.equals("false")) {
            return false;
        }
        try {
            Integer number = Integer.parseInt(value);
            if (number != null) {
                return number;
            }
        }
        catch (Exception exception) {
            // empty catch block
        }
        return value.toLowerCase();
    }

    static {
        filters.put(SHINY, (pokemon, value) -> {
            if (value instanceof Boolean) {
                return (Boolean)value == pokemon.isShiny;
            }
            if (value instanceof String) {
                if ("true".startsWith((String)value)) {
                    return pokemon.isShiny;
                }
                if ("false".startsWith((String)value)) {
                    return !pokemon.isShiny;
                }
            }
            return true;
        });
        filters.put(NATURE, (pokemon, value) -> {
            EnumNature nature = value instanceof String ? EnumNature.getNatureFromStartingString((String)value) : EnumNature.getNatureFromIndex((Integer)value);
            return nature != null && nature == pokemon.nature;
        });
        filters.put(GENDER, (pokemon, value) -> {
            Gender gender = value instanceof String ? Gender.getGenderFromStartingString((String)value) : Gender.getGender((short)((Integer)value).intValue());
            return gender != null && gender == pokemon.gender;
        });
        filters.put(IVS, (pokemon, value) -> {
            if (value instanceof String) {
                boolean equals;
                String valueStr = (String)value;
                if (valueStr.contains("bottle") || valueStr.contains("cap")) {
                    return pokemon.hasBottleCap();
                }
                if (valueStr.startsWith("perf") || valueStr.contains("max")) {
                    return pokemon.hasPerfectIVs();
                }
                if (valueStr.length() < 2) {
                    return false;
                }
                char c = valueStr.charAt(0);
                int mode = c == '>' ? 1 : (c == '<' ? 2 : 0);
                c = valueStr.charAt(valueStr.length() - 1);
                if (c != '%') {
                    return false;
                }
                boolean bl = equals = valueStr.charAt(1) == '=';
                if (mode > 0) {
                    if ((valueStr = valueStr.replace(">", "").replace("<", "").replace("=", "").replace("%", "")).length() == 0) {
                        return false;
                    }
                    try {
                        Integer valueNum = Integer.parseInt(valueStr);
                        if (!equals) {
                            if (mode == 1 && pokemon.getIVPercent() > (double)valueNum.intValue()) {
                                return true;
                            }
                            if (mode == 2 && pokemon.getIVPercent() < (double)valueNum.intValue()) {
                                return true;
                            }
                        } else {
                            if (mode == 1 && pokemon.getIVPercent() >= (double)valueNum.intValue()) {
                                return true;
                            }
                            if (mode == 2 && pokemon.getIVPercent() <= (double)valueNum.intValue()) {
                                return true;
                            }
                        }
                    }
                    catch (Exception exception) {
                        // empty catch block
                    }
                }
            }
            return false;
        });
        filters.put(GMAX, (pokemon, value) -> {
            if (value instanceof Boolean) {
                return (Boolean)value == pokemon.hasGmaxFactor;
            }
            if (value instanceof String) {
                if ("true".startsWith((String)value)) {
                    return pokemon.isShiny;
                }
                if ("false".startsWith((String)value)) {
                    return !pokemon.isShiny;
                }
            }
            return true;
        });
        filters.put(GROWTH, (pokemon, value) -> {
            EnumGrowth growth = value instanceof String ? EnumGrowth.getGrowthFromStartingString((String)value) : EnumGrowth.getGrowthFromIndex((Integer)value);
            return growth != null && growth == pokemon.growth;
        });
        filters.put(LEVEL, (pokemon, value) -> {
            if (value instanceof String) {
                boolean equals;
                String valueStr = (String)value;
                if (valueStr.length() == 0) {
                    return false;
                }
                char c = valueStr.charAt(0);
                int mode = c == '>' ? 1 : (c == '<' ? 2 : 0);
                boolean bl = equals = valueStr.charAt(1) == '=';
                if (mode > 0) {
                    if ((valueStr = valueStr.replace(">", "").replace("<", "").replace("=", "")).length() == 0) {
                        return false;
                    }
                    try {
                        Integer valueNum = Integer.parseInt(valueStr);
                        if (!equals) {
                            if (mode == 1 && pokemon.lvl > valueNum) {
                                return true;
                            }
                            if (mode == 2 && pokemon.lvl < valueNum) {
                                return true;
                            }
                        } else {
                            if (mode == 1 && pokemon.lvl >= valueNum) {
                                return true;
                            }
                            if (mode == 2 && pokemon.lvl <= valueNum) {
                                return true;
                            }
                        }
                    }
                    catch (Exception exception) {}
                }
            } else if (value instanceof Integer) {
                return (Integer)value == pokemon.lvl;
            }
            return false;
        });
        filters.put(BALL, (pokemon, value) -> {
            EnumPokeball pokeball = value instanceof String ? EnumPokeball.getPokeballFromStartingString((String)value) : EnumPokeball.getFromIndex((Integer)value);
            return pokeball != null && pokeball == pokemon.pokeball;
        });
        filters.put(SPECIAL, (pokemon, value) -> {
            if (value instanceof String) {
                String valueStr = (String)value;
                EnumSpecies species = pokemon.getSpecies();
                IEnumSpecialTexture special = species.getSpecialTexture(species.getFormEnum(pokemon.form), pokemon.specialTexture);
                return pokemon.customTexture.startsWith(valueStr) || special.getProperName().startsWith(valueStr);
            }
            if (value instanceof Integer) {
                return pokemon.specialTexture == (Integer)value;
            }
            if (value instanceof Boolean) {
                boolean hasSpecial = pokemon.specialTexture > 0 || !pokemon.customTexture.isEmpty();
                return (Boolean)value == hasSpecial;
            }
            return false;
        });
        filters.put(TYPE, (pokemon, value) -> {
            EnumType type = value instanceof String ? EnumType.getTypeFromStartingString((String)value) : EnumType.parseType((short)((Integer)value).intValue());
            return type != null && type == pokemon.type1 || pokemon.type2 != null && type == pokemon.type2;
        });
        filters.put(ABILITY, (pokemon, value) -> {
            if (value instanceof String) {
                String hiddenAbility;
                String valueStr = (String)value;
                String abilityName = pokemon.ability.toLowerCase();
                if ((valueStr.equals("ha") || valueStr.startsWith("hidden")) && (hiddenAbility = BaseStats.hiddenAbilities.get((Object)pokemon.getSpecies())) != null) {
                    return hiddenAbility.equals(abilityName);
                }
                return abilityName.startsWith(valueStr);
            }
            return false;
        });
        filters.put(LEGENDARY, (pokemon, value) -> {
            if (value instanceof Boolean) {
                return ((Boolean)value).booleanValue() == pokemon.getSpecies().isLegendary();
            }
            if (value instanceof String) {
                if ("true".startsWith((String)value)) {
                    return pokemon.isShiny;
                }
                if ("false".startsWith((String)value)) {
                    return !pokemon.isShiny;
                }
            }
            return true;
        });
    }
}

