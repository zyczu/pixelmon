/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableList
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.api.command;

import com.google.common.collect.ImmutableList;
import com.pixelmongenerations.api.events.commands.DeregisterSubcommandEvent;
import com.pixelmongenerations.api.events.commands.ExecutePixelmonCommandEvent;
import com.pixelmongenerations.api.events.commands.RegisterSubcommandEvent;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerComputerStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonPlayerUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;

public abstract class PixelmonCommand
extends CommandBase {
    private ArrayList<PixelmonCommand> subcommands = new ArrayList();

    protected abstract void execute(ICommandSender var1, String[] var2) throws CommandException;

    public PixelmonCommand(PixelmonCommand ... subcommands) {
        this.subcommands.addAll(Arrays.asList(subcommands));
    }

    @Nullable
    public static PixelmonCommand getParentCommand(PixelmonCommand subcommand) {
        for (ICommand command : FMLCommonHandler.instance().getMinecraftServerInstance().commandManager.getCommands().values()) {
            if (!(command instanceof PixelmonCommand) || !((PixelmonCommand)command).subcommands.contains(subcommand)) continue;
            return (PixelmonCommand)command;
        }
        return null;
    }

    @Override
    public final void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        ExecutePixelmonCommandEvent event;
        PixelmonCommand targetCommand = this;
        if (args.length > 0) {
            String arg0 = args[0];
            block0: for (PixelmonCommand subcommand : this.subcommands) {
                ArrayList<String> aliases = new ArrayList<String>(subcommand.getAliases());
                aliases.add(subcommand.getName());
                for (String alias : aliases) {
                    if (!alias.equalsIgnoreCase(arg0)) continue;
                    targetCommand = subcommand;
                    String[] newArgs = new String[args.length - 1];
                    System.arraycopy(args, 1, newArgs, 0, args.length - 1);
                    args = newArgs;
                    break block0;
                }
            }
        }
        if (!MinecraftForge.EVENT_BUS.post(event = new ExecutePixelmonCommandEvent(sender, targetCommand, args))) {
            event.getCommand().execute(event.getSender(), event.getArgs());
        }
    }

    public ImmutableList<PixelmonCommand> getSubcommands() {
        return ImmutableList.copyOf(this.subcommands);
    }

    public void registerSubcommand(PixelmonCommand subcommand) {
        if (!MinecraftForge.EVENT_BUS.post(new RegisterSubcommandEvent(this, subcommand))) {
            this.subcommands.add(subcommand);
        }
    }

    public void deregisterSubcommand(String name) {
        for (PixelmonCommand subcommand : this.subcommands) {
            if (!subcommand.getName().equalsIgnoreCase(name)) continue;
            this.deregisterSubcommand(subcommand);
        }
    }

    public void deregisterSubcommand(PixelmonCommand subcommand) {
        if (!MinecraftForge.EVENT_BUS.post(new DeregisterSubcommandEvent(this, subcommand))) {
            this.subcommands.remove(subcommand);
        }
    }

    @Nullable
    public EntityPlayerMP getPlayer(String name) {
        return PixelmonPlayerUtils.getUniquePlayerStartingWith(name);
    }

    @Nullable
    public EntityPlayerMP getPlayer(ICommandSender sender, String name) {
        return PixelmonPlayerUtils.getUniquePlayerStartingWith(name, sender);
    }

    @Nullable
    public PlayerStorage getPlayerStorage(EntityPlayerMP player) {
        return PixelmonStorage.pokeBallManager.getPlayerStorage(player).orElse(null);
    }

    @Nullable
    public PlayerComputerStorage getComputerStorage(EntityPlayerMP player) {
        return PixelmonStorage.computerManager.getPlayerStorage(player);
    }

    public MinecraftServer getServer() {
        return FMLCommonHandler.instance().getMinecraftServerInstance();
    }

    public List<String> tabCompleteUsernames(String[] args) {
        return PixelmonCommand.getListOfStringsMatchingLastWord(args, this.getServer().getOnlinePlayerNames());
    }

    public List<String> tabCompletePokemon(String[] args) {
        return PixelmonCommand.getListOfStringsMatchingLastWord(args, EnumSpecies.getNameList());
    }

    public void sendMessage(ICommandSender sender, TextFormatting color, String string, Object ... data) {
        TextComponentTranslation text = new TextComponentTranslation(string, data);
        text.getStyle().setColor(color);
        sender.sendMessage(text);
    }

    public void sendMessage(ICommandSender sender, String string, Object ... data) {
        this.sendMessage(sender, TextFormatting.GRAY, string, data);
    }
}

