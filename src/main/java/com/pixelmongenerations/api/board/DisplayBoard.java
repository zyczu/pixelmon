/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.board;

public class DisplayBoard {
    private String boardId;
    private String textureId;
    private String hoverTextureId;
    private double[] position;
    private float[] rotation;
    private float width;
    private float height;
    private boolean facePlayer;

    public DisplayBoard(String boardId, String textureId, double[] position, float[] rotation, float width, float height) {
        this(boardId, textureId, null, position, rotation, width, height, false);
    }

    public DisplayBoard(String boardId, String textureId, String hoverTextureId, double[] position, float[] rotation, float width, float height) {
        this(boardId, textureId, hoverTextureId, position, rotation, width, height, false);
    }

    public DisplayBoard(String boardId, String textureId, String hoverTextureId, double[] position, float[] rotation, float width, float height, boolean facePlayer) {
        this.boardId = boardId;
        this.textureId = textureId;
        this.hoverTextureId = hoverTextureId;
        this.position = position;
        this.rotation = rotation;
        this.width = width;
        this.height = height;
        this.facePlayer = facePlayer;
    }

    public String boardId() {
        return this.boardId;
    }

    public String textureId() {
        return this.textureId;
    }

    public String hoverTextureId() {
        return this.hoverTextureId;
    }

    public double[] position() {
        return this.position;
    }

    public float[] rotation() {
        return this.rotation;
    }

    public float width() {
        return this.width;
    }

    public float height() {
        return this.height;
    }

    public boolean facePlayer() {
        return this.facePlayer;
    }
}

