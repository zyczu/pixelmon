/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class StickPlateEvent
extends Event {
    private final Entity entity;
    private final BlockPos pos;
    private final boolean isDynamic;

    public StickPlateEvent(Entity entity, BlockPos pos, boolean isDynamic) {
        this.entity = entity;
        this.pos = pos;
        this.isDynamic = isDynamic;
    }

    public Entity getEntity() {
        return this.entity;
    }

    public BlockPos getPos() {
        return this.pos;
    }

    public boolean isDynamic() {
        return this.isDynamic;
    }
}

