/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.api.enums.DeleteType;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.eventhandler.Event;

public class PixelmonDeleteEvent
extends Event {
    private final EntityPlayerMP player;
    private final NBTTagCompound pokemon;
    private final DeleteType deleteType;

    public PixelmonDeleteEvent(EntityPlayerMP player, NBTTagCompound pokemonAtPos, DeleteType deleteType) {
        this.player = player;
        this.pokemon = pokemonAtPos;
        this.deleteType = deleteType;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public NBTTagCompound getTagCompound() {
        return this.pokemon;
    }

    public DeleteType getDeleteType() {
        return this.deleteType;
    }
}

