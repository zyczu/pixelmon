/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.api.events.UrsalunaItemEvent;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.ArrayList;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class SummonMountEvent
extends Event {
    private EntityPlayer player;
    private PokemonSpec pokemon;

    public SummonMountEvent(EntityPlayer player, PokemonSpec pokemon) {
        this.player = player;
        this.pokemon = pokemon;
    }

    public void summon() {
        PokemonSpec copy = this.pokemon.copy();
        copy.shiny = false;
        copy.setLevel(100);
        EntityPixelmon mount = copy.create(this.player.world);
        mount.setCatchable(false);
        mount.setPositionAndUpdate(this.player.posX, this.player.posY, this.player.posZ);
        this.player.world.spawnEntity(mount);
        mount.addTag("MountPokemon");
        this.player.startRiding(mount, true);
        if (mount.getSpecies() == EnumSpecies.Ursaluna) {
            ArrayList<ItemStack> defaultItems = new ArrayList<ItemStack>();
            defaultItems.add(new ItemStack(Item.getByNameOrId("pixelmon:moon_stone")));
            defaultItems.add(new ItemStack(Item.getByNameOrId("pixelmon:sun_stone")));
            defaultItems.add(new ItemStack(Item.getByNameOrId("pixelmon:peat_block")));
            defaultItems.add(new ItemStack(Item.getByNameOrId("minecraft:iron_ore")));
            defaultItems.add(new ItemStack(Item.getByNameOrId("pixelmon:black_augurite")));
            defaultItems.add(new ItemStack(Item.getByNameOrId("pixelmon:star_piece")));
            UrsalunaItemEvent event = new UrsalunaItemEvent((EntityPlayerMP)this.player, 0.25, 10000L, defaultItems);
            MinecraftForge.EVENT_BUS.post(event);
            event.start();
        }
    }

    public EntityPlayer getPlayer() {
        return this.player;
    }

    public PokemonSpec getPokemon() {
        return this.pokemon;
    }
}

