/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

public class ReceivedModListEvent
extends Event {
    private final EntityPlayerMP player;
    private final String[] modIds;

    public ReceivedModListEvent(EntityPlayerMP player, String[] modIds) {
        this.player = player;
        this.modIds = modIds;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public String[] getModIds() {
        return this.modIds;
    }
}

