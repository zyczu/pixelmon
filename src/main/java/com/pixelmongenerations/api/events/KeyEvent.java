/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.core.network.packetHandlers.EnumPixelmonKeyType;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class KeyEvent
extends Event {
    private final EntityPlayerMP player;
    private final EnumPixelmonKeyType key;

    public KeyEvent(EntityPlayerMP player, EnumPixelmonKeyType key) {
        this.player = player;
        this.key = key;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public EnumPixelmonKeyType getKeyType() {
        return this.key;
    }
}

