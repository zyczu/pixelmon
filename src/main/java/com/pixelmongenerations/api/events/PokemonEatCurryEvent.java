/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.capabilities.curry.CurryData;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class PokemonEatCurryEvent
extends Event {
    private EntityPixelmon pokemon;
    private EntityPlayer player;
    private CurryData curry;

    public PokemonEatCurryEvent(EntityPixelmon pokemon, EntityPlayer player, CurryData curry) {
        this.pokemon = pokemon;
        this.player = player;
        this.curry = curry;
    }

    public EntityPixelmon getPokemon() {
        return this.pokemon;
    }

    public EntityPlayer getPlayer() {
        return this.player;
    }

    public CurryData getCurry() {
        return this.curry;
    }
}

