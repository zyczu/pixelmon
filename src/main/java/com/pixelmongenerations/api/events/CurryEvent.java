/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.capabilities.curry.CurryData;
import com.pixelmongenerations.common.currydex.CurryDexEntry;
import com.pixelmongenerations.core.enums.EnumBerry;
import com.pixelmongenerations.core.enums.EnumBerryQuality;
import com.pixelmongenerations.core.enums.EnumCurryType;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Tuple;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class CurryEvent
extends Event {

    public static class AddEntry
    extends CurryEvent {
        private EntityPlayer player;
        private CurryData curry;
        private CurryDexEntry entry;

        public AddEntry(EntityPlayer player, CurryData curry, CurryDexEntry entry) {
            this.player = player;
            this.curry = curry;
            this.entry = entry;
        }

        public CurryDexEntry getEntry() {
            return this.entry;
        }

        public void setEntry(CurryDexEntry entry) {
            this.entry = entry;
        }

        public CurryData getCurry() {
            return this.curry;
        }

        public EntityPlayer getPlayer() {
            return this.player;
        }
    }

    public static class Cook
    extends CurryEvent {
        private EnumCurryType mainIngredient;
        private List<Tuple<EnumBerry, EnumBerryQuality>> berries;
        private CurryData output;

        public Cook(EnumCurryType mainIngredient, List<Tuple<EnumBerry, EnumBerryQuality>> berries, CurryData output) {
            this.mainIngredient = mainIngredient;
            this.berries = berries;
            this.output = output;
        }

        public EnumCurryType getMainIngredient() {
            return this.mainIngredient;
        }

        public List<Tuple<EnumBerry, EnumBerryQuality>> getBerries() {
            return this.berries;
        }

        public CurryData getOutput() {
            return this.output;
        }

        public void setOutput(CurryData output) {
            this.output = output;
        }
    }
}

