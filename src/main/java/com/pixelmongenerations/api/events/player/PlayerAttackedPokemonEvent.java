/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.player;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class PlayerAttackedPokemonEvent
extends Event {
    private final EntityPlayerMP player;
    private final EntityPixelmon pokemon;
    private float damage;

    public PlayerAttackedPokemonEvent(EntityPlayerMP player, EntityPixelmon pokemon, float damage) {
        this.player = player;
        this.pokemon = pokemon;
        this.damage = damage;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public EntityPixelmon getPokemon() {
        return this.pokemon;
    }

    public float getDamage() {
        return this.damage;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }
}

