/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.player;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class PlayerShinyChanceEvent
extends Event {
    private EntityPlayerMP player;
    private float chance;

    public PlayerShinyChanceEvent(EntityPlayerMP player, float chance) {
        this.player = player;
        this.chance = chance;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public float getChance() {
        return this.chance;
    }

    public void setChance(float chance) {
        this.chance = chance;
    }
}

