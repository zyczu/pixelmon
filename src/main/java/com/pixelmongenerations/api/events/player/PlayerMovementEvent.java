/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.player;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.eventhandler.Event;

public class PlayerMovementEvent
extends Event {
    private final EntityPlayerMP player;
    private final int stepsTaken;
    private final BlockPos lastPos;
    private final BlockPos currentPos;

    public PlayerMovementEvent(EntityPlayerMP player, int stepsTaken, BlockPos lastPos, BlockPos currentPos) {
        this.player = player;
        this.stepsTaken = stepsTaken;
        this.lastPos = lastPos;
        this.currentPos = currentPos;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public int getSteps() {
        return this.stepsTaken;
    }

    public BlockPos getLastPos() {
        return this.lastPos;
    }

    public BlockPos getCurrentPos() {
        return this.currentPos;
    }
}

