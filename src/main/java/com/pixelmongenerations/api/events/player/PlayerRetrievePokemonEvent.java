/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.player;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

public class PlayerRetrievePokemonEvent
extends Event {
    private EntityPlayerMP player;
    private EntityPixelmon pokemon;

    public PlayerRetrievePokemonEvent(EntityPlayerMP player, EntityPixelmon pokemon) {
        this.player = player;
        this.pokemon = pokemon;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public EntityPixelmon getPokemon() {
        return this.pokemon;
    }

    public void setPokemon(EntityPixelmon pokemon) {
        this.pokemon = pokemon;
    }
}

