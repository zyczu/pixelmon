/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.player;

import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.core.enums.battle.BattleResults;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

@Deprecated
public class PlayerBattleEndedEvent
extends Event {
    private final EntityPlayerMP player;
    private final BattleControllerBase battleController;
    private final BattleResults result;

    public PlayerBattleEndedEvent(EntityPlayerMP player, BattleControllerBase battleControllerBase, BattleResults result) {
        this.player = player;
        this.battleController = battleControllerBase;
        this.result = result;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public BattleControllerBase getBattleController() {
        return this.battleController;
    }

    public BattleResults getBattleResults() {
        return this.result;
    }
}

