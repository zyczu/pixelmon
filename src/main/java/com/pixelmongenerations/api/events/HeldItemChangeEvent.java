/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import java.util.Optional;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class HeldItemChangeEvent
extends Event {
    private final EntityLivingBase owner;
    private final Optional<EntityPixelmon> pokemon;
    private final Optional<NBTTagCompound> nbt;
    private ItemStack newHeldItem;

    public HeldItemChangeEvent(EntityLivingBase owner, Optional<EntityPixelmon> pokemon, Optional<NBTTagCompound> nbt, ItemStack newItem) {
        this.owner = owner;
        this.pokemon = pokemon;
        this.nbt = nbt;
        this.newHeldItem = newItem;
    }

    public EntityLivingBase getOwner() {
        return this.owner;
    }

    public Optional<EntityPixelmon> getPokemon() {
        return this.pokemon;
    }

    public Optional<NBTTagCompound> getTagCompound() {
        return this.nbt;
    }

    public ItemStack getItem() {
        return this.newHeldItem;
    }

    public void setItem(ItemStack heldItem) {
        this.newHeldItem = heldItem;
    }

    public EnumSpecies getSpecies() {
        if (this.pokemon.isPresent()) {
            return EnumSpecies.getFromName(this.pokemon.get().getPokemonName()).get();
        }
        return EnumSpecies.getFromName(this.nbt.get().getString("Name")).get();
    }
}

