/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class RareCandyEvent
extends Event {
    private EntityPixelmon pixelmon;
    private EntityPlayerMP player;

    public RareCandyEvent(EntityPlayerMP player, EntityPixelmon pixelmon) {
        this.player = player;
        this.pixelmon = pixelmon;
    }

    public EntityPixelmon getPokemon() {
        return this.pixelmon;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }
}

