/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.item.ItemRepel;
import java.util.UUID;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class RepelEvent
extends Event {
    private final UUID uuid;
    private final ItemRepel.EnumRepel repel;

    public RepelEvent(UUID uuid, ItemRepel.EnumRepel repel) {
        this.uuid = uuid;
        this.repel = repel;
    }

    public UUID getUUID() {
        return this.uuid;
    }

    public ItemRepel.EnumRepel getRepel() {
        return this.repel;
    }

    public int getUnits() {
        return this.repel.units;
    }

    public void setUnit(int units) {
        this.repel.units = units;
    }
}

