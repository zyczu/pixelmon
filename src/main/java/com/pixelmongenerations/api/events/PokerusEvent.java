/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.NBTLink;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public abstract class PokerusEvent
extends Event {
    private EntityPlayerMP player;
    private NBTLink link;

    public PokerusEvent(EntityPlayerMP player, NBTLink link) {
        this.player = player;
        this.link = link;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public NBTLink getPokemonLink() {
        return this.link;
    }

    public void setPokemonLink(NBTLink link) {
        this.link = link;
    }

    @Cancelable
    public static class CuredEvent
    extends PokerusEvent {
        public CuredEvent(EntityPlayerMP player, NBTLink link) {
            super(player, link);
        }
    }

    @Cancelable
    public static class SpreadEvent
    extends PokerusEvent {
        public SpreadEvent(EntityPlayerMP player, NBTLink link) {
            super(player, link);
        }
    }

    @Cancelable
    public static class InfectEvent
    extends PokerusEvent {
        public InfectEvent(EntityPlayerMP player, NBTLink link) {
            super(player, link);
        }
    }
}

