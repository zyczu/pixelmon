/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import net.minecraft.advancements.Advancement;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

public class PixelmonAdvancementEvent
extends Event {
    private final Advancement advancement;
    private final EntityPlayerMP player;

    public PixelmonAdvancementEvent(EntityPlayerMP player, Advancement advancement) {
        this.player = player;
        this.advancement = advancement;
    }

    public Advancement getAdvancement() {
        return this.advancement;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }
}

