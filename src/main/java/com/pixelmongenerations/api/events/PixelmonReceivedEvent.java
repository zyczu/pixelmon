/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.api.enums.ReceiveType;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

public class PixelmonReceivedEvent
extends Event {
    private final EntityPlayerMP player;
    private final ReceiveType receiveType;
    private final EntityPixelmon pokemon;

    public PixelmonReceivedEvent(EntityPlayerMP player, ReceiveType recievedType, EntityPixelmon pokemon) {
        this.player = player;
        this.receiveType = recievedType;
        this.pokemon = pokemon;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public ReceiveType getReceiveType() {
        return this.receiveType;
    }

    public EntityPixelmon getPokemon() {
        return this.pokemon;
    }
}

