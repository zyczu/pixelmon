/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import com.pixelmongenerations.core.enums.EnumExpSource;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class LevelUpEvent
extends Event {
    private final EntityPlayerMP player;
    private final PokemonLink pokemon;
    private final int newLevel;
    private final EnumExpSource source;

    public LevelUpEvent(EntityPlayerMP player, PokemonLink pokemon, int newLevel) {
        this(player, pokemon, newLevel, EnumExpSource.Default);
    }

    public LevelUpEvent(EntityPlayerMP player, PokemonLink pokemon, int newLevel, EnumExpSource source) {
        this.player = player;
        this.pokemon = pokemon;
        this.newLevel = newLevel;
        this.source = source;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public PokemonLink getPokemon() {
        return this.pokemon;
    }

    public int getLevel() {
        return this.newLevel;
    }

    public EnumExpSource getXPSource() {
        return this.source;
    }
}

