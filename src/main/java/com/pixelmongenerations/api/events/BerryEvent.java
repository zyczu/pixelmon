/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.block.tileEntities.TileEntityBerryTree;
import com.pixelmongenerations.core.enums.EnumBerry;
import java.util.List;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public abstract class BerryEvent
extends Event {
    private final EnumBerry berry;
    private final BlockPos pos;

    protected BerryEvent(EnumBerry berry, BlockPos pos) {
        this.berry = berry;
        this.pos = pos;
    }

    public EnumBerry getBerryType() {
        return this.berry;
    }

    public BlockPos getBlockPos() {
        return this.pos;
    }

    @Cancelable
    public static class BerryWateredEvent
    extends BerryEvent {
        private final EntityPlayerMP player;
        private final TileEntityBerryTree tree;

        public BerryWateredEvent(EnumBerry berry, BlockPos pos, EntityPlayerMP player, TileEntityBerryTree tree) {
            super(berry, pos);
            this.player = player;
            this.tree = tree;
        }

        public EntityPlayerMP getPlayer() {
            return this.player;
        }

        public TileEntityBerryTree getTree() {
            return this.tree;
        }
    }

    @Cancelable
    public static class BerryPlantedEvent
    extends BerryEvent {
        private final EntityPlayerMP player;

        public BerryPlantedEvent(EnumBerry berry, BlockPos pos, EntityPlayerMP player) {
            super(berry, pos);
            this.player = player;
        }

        public EntityPlayerMP getPlayer() {
            return this.player;
        }
    }

    public static class BerryReadyEvent
    extends BerryEvent {
        private final TileEntityBerryTree tree;

        public BerryReadyEvent(EnumBerry berry, BlockPos pos, TileEntityBerryTree tree) {
            super(berry, pos);
            this.tree = tree;
        }

        public TileEntityBerryTree getBerryTileEntity() {
            return this.tree;
        }
    }

    @Cancelable
    public static class PickBerryEvent
    extends BerryEvent {
        private final EntityPlayerMP player;
        private final TileEntityBerryTree tree;
        private List<ItemStack> pickedStacks;

        public PickBerryEvent(EnumBerry berry, BlockPos pos, EntityPlayerMP player, TileEntityBerryTree tree, List<ItemStack> pickedStack) {
            super(berry, pos);
            this.player = player;
            this.tree = tree;
            this.pickedStacks = pickedStack;
        }

        public EntityPlayerMP getPlayer() {
            return this.player;
        }

        public TileEntityBerryTree getBerryTileEntity() {
            return this.tree;
        }

        public List<ItemStack> getPickedStacks() {
            return this.pickedStacks;
        }

        public void setPickedStack(NonNullList<ItemStack> pickedStacks) throws IllegalArgumentException {
            if (pickedStacks == null) {
                throw new IllegalArgumentException("ItemStack can not be null in setPickedStack");
            }
            this.pickedStacks = pickedStacks;
        }
    }
}

