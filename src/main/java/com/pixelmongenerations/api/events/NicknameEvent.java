/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class NicknameEvent
extends Event {
    private EntityPlayerMP player;
    private Optional<EntityPixelmon> pokemon;
    private String nickname;

    public NicknameEvent(EntityPlayerMP player, Optional<EntityPixelmon> pokemon, String nickname) {
        this.player = player;
        this.pokemon = pokemon;
        this.nickname = nickname;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public Optional<EntityPixelmon> getPokemon() {
        return this.pokemon;
    }

    public String getNickname() {
        return this.nickname;
    }

    public void setNickname(String name) {
        this.nickname = name;
    }
}

