/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class BattleStartEvent
extends Event {
    private final BattleControllerBase bc;
    private final BattleParticipant[] participant1;
    private final BattleParticipant[] participant2;

    public BattleStartEvent(BattleControllerBase bc, BattleParticipant[] team1, BattleParticipant[] team2) {
        this.bc = bc;
        this.participant1 = team1;
        this.participant2 = team2;
    }

    public BattleControllerBase getBattleController() {
        return this.bc;
    }

    public BattleParticipant[] getParticipantOne() {
        return this.participant1;
    }

    public BattleParticipant[] getParticipantTwo() {
        return this.participant2;
    }
}

