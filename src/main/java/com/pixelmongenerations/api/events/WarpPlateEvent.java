/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class WarpPlateEvent
extends Event {
    private final EntityPlayerMP player;
    private final BlockPos platePos;
    private BlockPos destinationPos;
    private final boolean isDynamic;

    public WarpPlateEvent(EntityPlayerMP player, BlockPos platePos, BlockPos destinationPos, boolean isDynamic) {
        this.player = player;
        this.platePos = platePos;
        this.destinationPos = destinationPos;
        this.isDynamic = isDynamic;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public BlockPos getPlatePos() {
        return this.platePos;
    }

    public BlockPos getDestinationPos() {
        return this.destinationPos;
    }

    public boolean isDynamic() {
        return this.isDynamic;
    }

    public void setDestinationPos(BlockPos pos) {
        this.destinationPos = pos;
    }
}

