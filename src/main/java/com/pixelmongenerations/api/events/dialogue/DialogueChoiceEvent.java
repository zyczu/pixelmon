/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.dialogue;

import com.pixelmongenerations.api.dialogue.Choice;
import com.pixelmongenerations.api.dialogue.Dialogue;
import com.pixelmongenerations.core.network.packetHandlers.dialogue.DialogueNextAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Consumer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

public class DialogueChoiceEvent
extends Event {
    private final EntityPlayerMP player;
    private final Choice choice;
    private DialogueNextAction.DialogueGuiAction action;
    private ArrayList<Dialogue> newDialogues = new ArrayList();

    public DialogueChoiceEvent(EntityPlayerMP player, Choice choice) {
        this.player = player;
        this.choice = choice;
        this.action = DialogueNextAction.DialogueGuiAction.CONTINUE;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public Choice getChoice() {
        return this.choice;
    }

    public DialogueNextAction.DialogueGuiAction getAction() {
        return this.action;
    }

    public void setAction(DialogueNextAction.DialogueGuiAction action) {
        if (action != null) {
            this.action = action;
        }
    }

    public DialogueChoiceEvent addDialogue(Dialogue newDialogue) {
        if (newDialogue != null) {
            this.newDialogues.add(newDialogue);
        }
        return this;
    }

    public void setNewDialogues(ArrayList<Dialogue> newDialogues) {
        if (newDialogues != null) {
            this.newDialogues = newDialogues;
        }
    }

    public ArrayList<Dialogue> getNewDialogues() {
        return this.newDialogues;
    }

    public void reply(Dialogue ... dialogues) {
        this.reply(false, dialogues);
    }

    public void reply(boolean insert, Dialogue ... dialogues) {
        this.action = insert ? DialogueNextAction.DialogueGuiAction.INSERT_DIALOGUES : DialogueNextAction.DialogueGuiAction.NEW_DIALOGUES;
        HashMap<Integer, Consumer<DialogueChoiceEvent>> choiceMap = new HashMap<Integer, Consumer<DialogueChoiceEvent>>();
        for (Dialogue dialogue : dialogues) {
            for (Choice choice : dialogue.choices) {
                choiceMap.put(choice.choiceID, choice.handle);
            }
            this.addDialogue(dialogue);
        }
        Choice.handleMap.put(this.player.getUniqueID(), choiceMap);
    }
}

