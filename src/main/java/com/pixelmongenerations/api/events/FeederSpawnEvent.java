/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.core.enums.EnumSpecies;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class FeederSpawnEvent
extends Event {
    private final EntityPlayerMP player;
    private EntityPixelmon pokemon;
    private BlockPos pos;

    public FeederSpawnEvent(EntityPlayerMP player, EntityPixelmon pokemon, BlockPos pos) {
        this.player = player;
        this.pokemon = pokemon;
        this.pos = pos;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public EntityPixelmon getPokemon() {
        return this.pokemon;
    }

    public BlockPos getPos() {
        return this.pos;
    }

    public void setPos(BlockPos blockPos) {
        this.pos = blockPos;
    }

    public void setPokemon(EntityPixelmon entityPixelmon) {
        this.pokemon = entityPixelmon;
    }

    public boolean canBeNoble() {
        return this.pokemon.getSpecies() == EnumSpecies.Kleavor || this.pokemon.getSpecies() == EnumSpecies.Lilligant && this.pokemon.getForm() == 11 || this.pokemon.getSpecies() == EnumSpecies.Arcanine && this.pokemon.getForm() == 11 || this.pokemon.getSpecies() == EnumSpecies.Electrode && this.pokemon.getForm() == 11 || this.pokemon.getSpecies() == EnumSpecies.Avalugg && this.pokemon.getForm() == 11;
    }
}

