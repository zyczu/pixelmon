/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import net.minecraft.entity.Entity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class MovementPlateEvent
extends Event {
    private final Entity entity;
    private final BlockPos pos;
    private EnumFacing facing;
    private double speedModifier;
    private final boolean isDynamicPlate;

    public MovementPlateEvent(Entity entity, BlockPos pos, EnumFacing facing, double speedModifier, boolean isDynamicPlate) {
        this.entity = entity;
        this.pos = pos;
        this.facing = facing;
        this.speedModifier = speedModifier;
        this.isDynamicPlate = isDynamicPlate;
    }

    public Entity getEntity() {
        return this.entity;
    }

    public BlockPos getPos() {
        return this.pos;
    }

    public EnumFacing getFacing() {
        return this.facing;
    }

    public void setFacing(EnumFacing facing) {
        this.facing = facing;
    }

    public double getSpeedModifier() {
        return this.speedModifier;
    }

    public void setSpeedModifier(double speedMod) {
        this.speedModifier = speedMod;
    }

    public boolean isDynamicPlate() {
        return this.isDynamicPlate;
    }
}

