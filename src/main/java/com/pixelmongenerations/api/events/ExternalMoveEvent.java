/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.externalMoves.ExternalMoveBase;
import com.pixelmongenerations.core.storage.playerData.TeleportPosition;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public abstract class ExternalMoveEvent
extends Event {
    private final EntityPlayerMP player;
    private final EntityPixelmon pokemon;
    private final ExternalMoveBase externalMove;
    private RayTraceResult target;

    public ExternalMoveEvent(EntityPlayerMP player, EntityPixelmon pokemon, ExternalMoveBase externalMove, RayTraceResult target) {
        this.player = player;
        this.pokemon = pokemon;
        this.externalMove = externalMove;
        this.target = target;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public EntityPixelmon getPokemon() {
        return this.pokemon;
    }

    public ExternalMoveBase getExternalMove() {
        return this.externalMove;
    }

    public boolean isDestructive() {
        return this.externalMove.isDestructive();
    }

    public int getOriginalCooldown() {
        return this.externalMove.getCooldown(this.pokemon);
    }

    public RayTraceResult getTarget() {
        return this.target;
    }

    @Cancelable
    public static final class Hyperspace
    extends ExternalMoveEvent {
        public Hyperspace(EntityPlayerMP player, EntityPixelmon pokemon, ExternalMoveBase externalMove, RayTraceResult target) {
            super(player, pokemon, externalMove, target);
        }
    }

    @Cancelable
    public static final class PreparingMoveEvent
    extends ExternalMoveEvent {
        private int cooldown;
        private RayTraceResult target;

        public PreparingMoveEvent(EntityPlayerMP player, EntityPixelmon pokemon, ExternalMoveBase externalMove, RayTraceResult target) {
            super(player, pokemon, externalMove, target);
            this.target = target;
            this.cooldown = this.getOriginalCooldown();
        }

        public int getCooldown() {
            return this.cooldown;
        }

        public void setCooldown(int cooldown) {
            if (cooldown < 0) {
                cooldown = 0;
            }
            this.cooldown = cooldown;
        }

        @Override
        public RayTraceResult getTarget() {
            return this.target;
        }

        public void setTarget(RayTraceResult target) {
            if (target != null) {
                this.target = target;
            }
        }
    }

    @Cancelable
    public static final class ExplodeMoveEvent
    extends ExternalMoveEvent {
        private float explosionPower;

        public ExplodeMoveEvent(EntityPlayerMP player, EntityPixelmon pokemon, ExternalMoveBase externalMove, RayTraceResult target, float explosionPower) {
            super(player, pokemon, externalMove, target);
            this.explosionPower = explosionPower;
        }

        public float getExplosionPower() {
            return this.explosionPower;
        }

        public void setExplosionStrength(float explosionPower) {
            if (explosionPower < 0.0f) {
                explosionPower = 0.0f;
            }
            this.explosionPower = explosionPower;
        }
    }

    @Cancelable
    public static final class ForageMoveEvent
    extends ExternalMoveEvent {
        private Optional<ItemStack> foragedItem;

        public ForageMoveEvent(EntityPlayerMP player, EntityPixelmon pokemon, ExternalMoveBase externalMove, RayTraceResult target, Optional<ItemStack> foragedItem) {
            super(player, pokemon, externalMove, target);
            this.foragedItem = foragedItem;
        }

        public Optional<ItemStack> getForagedItem() {
            return this.foragedItem;
        }

        public void setForagedItem(ItemStack item) {
            this.foragedItem = item == null ? Optional.empty() : Optional.of(item);
        }
    }

    @Cancelable
    public static final class HealOtherMoveEvent
    extends ExternalMoveEvent {
        private float healAmount;
        private float damageAmount;

        public HealOtherMoveEvent(EntityPlayerMP player, EntityPixelmon pokemon, ExternalMoveBase externalMove, RayTraceResult target, float healAmount) {
            super(player, pokemon, externalMove, target);
            this.healAmount = this.damageAmount = healAmount;
        }

        public float getHealAmount() {
            return this.healAmount;
        }

        public void setHealAmount(float healAmount) {
            if (healAmount < 0.0f) {
                healAmount = 0.0f;
            }
            this.healAmount = healAmount;
        }

        public float getDamageAmount() {
            return this.damageAmount;
        }

        public void setDamageAmount(float damageAmount) {
            if (damageAmount < 0.0f) {
                damageAmount = 0.0f;
            }
            this.damageAmount = damageAmount;
        }
    }

    @Cancelable
    public static final class LightFireMoveEvent
    extends ExternalMoveEvent {
        private int length;
        private int width;
        private boolean cross;

        public LightFireMoveEvent(EntityPlayerMP player, EntityPixelmon pokemon, ExternalMoveBase externalMove, RayTraceResult target, int length, int width, boolean cross) {
            super(player, pokemon, externalMove, target);
            this.length = length;
            this.width = width;
            this.cross = cross;
        }

        public int getLength() {
            return this.length;
        }

        public void setLength(int length) {
            if (length < 1) {
                length = 1;
            }
            this.length = length;
        }

        public int getWidth() {
            return this.width;
        }

        public void setWidth(int width) {
            if (width < 1) {
                width = 1;
            }
            this.width = width;
        }

        public boolean isCross() {
            return this.cross;
        }

        public void setCross(boolean cross) {
            this.cross = cross;
        }
    }

    @Cancelable
    public static final class TeleportFlyMoveEvent
    extends ExternalMoveEvent {
        private TeleportPosition destination;

        public TeleportFlyMoveEvent(EntityPlayerMP player, EntityPixelmon pokemon, ExternalMoveBase externalMove, RayTraceResult target, TeleportPosition destination) {
            super(player, pokemon, externalMove, target);
            this.destination = destination;
        }

        public TeleportPosition getDestination() {
            return this.destination;
        }

        public void setDestination(double x, double y, double z, float yaw, float pitch) {
            this.destination = new TeleportPosition();
            this.destination.store(x, y, z, yaw, pitch);
        }
    }
}

