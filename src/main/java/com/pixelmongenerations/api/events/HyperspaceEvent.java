/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.EntityMysteriousRing;
import java.util.UUID;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public abstract class HyperspaceEvent
extends Event {
    private final EntityMysteriousRing ring;

    public HyperspaceEvent(EntityMysteriousRing ring) {
        this.ring = ring;
    }

    public EntityMysteriousRing getRing() {
        return this.ring;
    }

    @Cancelable
    public static class Collide
    extends HyperspaceEvent {
        private final Entity entity;
        private boolean kill = false;

        public Collide(EntityMysteriousRing ring, Entity entity) {
            super(ring);
            this.entity = entity;
        }

        public Entity getEntity() {
            return this.entity;
        }

        public boolean shouldKill() {
            return this.kill;
        }

        public void setKills(boolean kills) {
            this.kill = kills;
        }
    }

    @Cancelable
    public static class Click
    extends HyperspaceEvent {
        private final UUID uuid;
        private boolean kill = false;

        public Click(EntityMysteriousRing ring, UUID uuid) {
            super(ring);
            this.uuid = uuid;
        }

        public UUID getPlayerUUID() {
            return this.uuid;
        }

        public boolean shouldKill() {
            return this.kill;
        }

        public void setKills(boolean kill) {
            this.kill = kill;
        }
    }

    @Cancelable
    public static class Spawn
    extends HyperspaceEvent {
        private EntityPlayerMP player = null;

        public Spawn(EntityMysteriousRing ring) {
            super(ring);
        }

        public Spawn(EntityMysteriousRing ring, EntityPlayerMP player) {
            super(ring);
            this.player = player;
        }

        public EntityPlayerMP getPlayer() {
            return this.player;
        }
    }
}

