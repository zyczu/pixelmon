/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.npcs.NPCTrader;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public abstract class PixelmonTradeEvent
extends Event {
    private final NBTTagCompound pokemon1;
    private final NBTTagCompound pokemon2;

    public PixelmonTradeEvent(NBTTagCompound pokemon1, NBTTagCompound pokemon2) {
        this.pokemon1 = pokemon1;
        this.pokemon2 = pokemon2;
    }

    public NBTTagCompound getPokemonOne() {
        return this.pokemon1;
    }

    public NBTTagCompound getPokemonTwo() {
        return this.pokemon2;
    }

    @Cancelable
    public static class NPCTradeEvent
    extends PixelmonTradeEvent {
        private final EntityPlayer player;
        private final NPCTrader npc;

        public NPCTradeEvent(EntityPlayer player, NBTTagCompound pokemon1, NPCTrader npc, NBTTagCompound pokemon2) {
            super(pokemon1, pokemon2);
            this.player = player;
            this.npc = npc;
        }

        public EntityPlayer getPlayer() {
            return this.player;
        }

        public NPCTrader getNPC() {
            return this.npc;
        }
    }

    @Cancelable
    public static class PlayerTradeEvent
    extends PixelmonTradeEvent {
        private final EntityPlayer player1;
        private final EntityPlayer player2;

        public PlayerTradeEvent(EntityPlayer player1, EntityPlayer player2, NBTTagCompound pokemon1, NBTTagCompound pokemon2) {
            super(pokemon1, pokemon2);
            this.player1 = player1;
            this.player2 = player2;
        }

        public EntityPlayer getPlayerOne() {
            return this.player1;
        }

        public EntityPlayer getPlayerTwo() {
            return this.player2;
        }
    }
}

