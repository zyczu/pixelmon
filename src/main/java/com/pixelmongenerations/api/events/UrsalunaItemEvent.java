/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class UrsalunaItemEvent
extends Event {
    private final EntityPlayerMP player;
    private double chance;
    private long interval;
    private List<ItemStack> pool;
    public static Map<UUID, Timer> timerMap = new HashMap<UUID, Timer>();

    public UrsalunaItemEvent(EntityPlayerMP player, double chance, long interval, List<ItemStack> pool) {
        this.player = player;
        this.chance = chance;
        this.interval = interval;
        this.pool = pool;
    }

    public void start() {
        Timer t = new Timer();
        t.schedule(new TimerTask(){

            @Override
            public void run() {
                if (RandomHelper.getRandomChance(UrsalunaItemEvent.this.getChance())) {
                    ItemStack pickedUpItem = UrsalunaItemEvent.this.getPool().get(RandomHelper.getRandomNumberBetween(0, UrsalunaItemEvent.this.getPool().size()));
                    String id = pickedUpItem.getItem().getRegistryName().toString();
                    String[] split = id.split(":");
                    StringBuilder name = new StringBuilder();
                    if (split[1].contains("_")) {
                        String[] fullName;
                        for (String s : fullName = split[1].split("_")) {
                            String word = s.substring(0, 1).toUpperCase() + s.substring(1);
                            name.append(word).append(" ");
                        }
                        name = new StringBuilder(name.substring(0, name.length() - 1));
                    } else {
                        name = new StringBuilder(split[1].substring(0, 1).toUpperCase() + split[1].substring(1));
                    }
                    if (UrsalunaItemEvent.this.getPlayer().inventory.addItemStackToInventory(pickedUpItem)) {
                        UrsalunaItemEvent.this.getPlayer().sendMessage(new TextComponentString("Ursaluna picked a " + name + "!"));
                    }
                }
            }
        }, this.getInterval(), this.getInterval());
        timerMap.put(this.getPlayer().getUniqueID(), t);
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public double getChance() {
        return this.chance;
    }

    public long getInterval() {
        return this.interval;
    }

    public List<ItemStack> getPool() {
        return this.pool;
    }

    public void setChance(double chance) {
        this.chance = chance;
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }

    public void setPool(List<ItemStack> pool) {
        this.pool = pool;
    }
}

