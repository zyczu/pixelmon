/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class CameraEvent
extends Event {
    private PokemonLink link;
    private EntityPlayer player;

    public CameraEvent(PokemonLink link, EntityPlayer player) {
        this.link = link;
        this.player = player;
    }

    public PokemonLink getPokemonLink() {
        return this.link;
    }

    public void setPokemonLink(PokemonLink link) {
        this.link = link;
    }

    public EntityPlayer getPlayer() {
        return this.player;
    }
}

