/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;
import net.minecraftforge.fml.common.eventhandler.Event;

public class ExperienceGainEvent
extends Event {
    private final PokemonLink pokemon;
    private int experience;

    public ExperienceGainEvent(PokemonLink pokemon, int experience) {
        this.pokemon = pokemon;
        this.experience = experience;
    }

    public PokemonLink getPokemon() {
        return this.pokemon;
    }

    public int getExperience() {
        return this.experience;
    }

    public void setExperience(int experience) {
        this.experience = experience < 0 ? 0 : experience;
    }
}

