/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.block.tileEntities.TileEntityAnvil;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public abstract class AnvilEvent
extends Event {
    private final EntityPlayerMP player;
    private final TileEntityAnvil anvil;

    private AnvilEvent(EntityPlayerMP player, TileEntityAnvil anvil) {
        this.player = player;
        this.anvil = anvil;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public TileEntityAnvil getAnvilTileEntity() {
        return this.anvil;
    }

    @Cancelable
    public static class BeatAnvilEvent
    extends AnvilEvent {
        private final Item item;
        private int neededHits;
        private int force;

        public BeatAnvilEvent(EntityPlayerMP player, TileEntityAnvil anvil, Item item, int neededHits, int force) {
            super(player, anvil);
            this.item = item;
            this.neededHits = neededHits;
            this.force = force;
        }

        public Item getAnvilItem() {
            return this.item;
        }

        public int getNeededHits() {
            return this.neededHits;
        }

        public void setNeededHits(int neededHits) {
            if (neededHits < 0) {
                neededHits = 0;
            }
            this.neededHits = neededHits;
        }

        public int getForce() {
            return this.force;
        }

        public void setForce(int force) {
            if (force < 0) {
                force = 0;
            }
            this.force = force;
        }
    }

    @Cancelable
    public static class HammerDamageEvent
    extends AnvilEvent {
        private final ItemStack hammer;
        private int damage;

        public HammerDamageEvent(EntityPlayerMP player, TileEntityAnvil anvil, ItemStack hammer, int damage) {
            super(player, anvil);
            this.hammer = hammer;
            this.damage = damage;
        }

        public ItemStack getHammerItem() {
            return this.hammer;
        }

        public int getDamage() {
            return this.damage;
        }

        public void setDamage(int damage) {
            this.damage = damage;
        }
    }

    @Cancelable
    public static class MaterialChangedEvent
    extends AnvilEvent {
        private ItemStack material;
        private final boolean isCollecting;

        public MaterialChangedEvent(EntityPlayerMP player, TileEntityAnvil anvil, ItemStack material, boolean isCollecting) {
            super(player, anvil);
            this.material = material;
            this.isCollecting = isCollecting;
        }

        public ItemStack getMaterial() {
            return this.material;
        }

        public void setMaterial(ItemStack material) {
            this.material = material;
        }

        public boolean isBeingCollected() {
            return this.isCollecting;
        }
    }

    public static class FinishedSmithEvent
    extends AnvilEvent {
        private Item item;

        public FinishedSmithEvent(EntityPlayerMP player, TileEntityAnvil anvil, Item item) {
            super(player, anvil);
            this.item = item;
        }

        public Item getSmithedItem() {
            return this.item;
        }

        public void setSmithedItem(Item item) {
            this.item = item;
        }
    }
}

