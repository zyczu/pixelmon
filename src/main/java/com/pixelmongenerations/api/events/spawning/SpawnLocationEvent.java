/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.spawning;

import com.pixelmongenerations.api.spawning.SpawnLocation;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class SpawnLocationEvent
extends Event {
    private SpawnLocation spawnLocation;

    public SpawnLocationEvent(SpawnLocation spawnLocation) {
        this.spawnLocation = spawnLocation;
    }

    public void setSpawnLocation(SpawnLocation spawnLocation) {
        if (spawnLocation != null) {
            this.spawnLocation = spawnLocation;
        }
    }

    public SpawnLocation getSpawnLocation() {
        return this.spawnLocation;
    }
}

