/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.spawning;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class DespawnEvent
extends Event {
    private EntityPixelmon pokemon;

    public DespawnEvent(EntityPixelmon pokemon) {
        this.pokemon = pokemon;
    }

    public EntityPixelmon getPokemon() {
        return this.pokemon;
    }
}

