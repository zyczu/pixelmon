/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.spawning;

import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnAction;
import net.minecraft.entity.Entity;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class SpawnEvent
extends Event {
    private final AbstractSpawner spawner;
    private final SpawnAction<? extends Entity> action;

    public SpawnEvent(AbstractSpawner spawner, SpawnAction<? extends Entity> action) {
        this.spawner = spawner;
        this.action = action;
    }

    public AbstractSpawner getSpawner() {
        return this.spawner;
    }

    public SpawnAction<? extends Entity> getSpawnAction() {
        return this.action;
    }
}

