/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.entity.EntitySpaceTimeDistortion;
import java.util.Map;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

public class SpaceTimeDistortionEvent
extends Event {
    private final EntitySpaceTimeDistortion distortion;

    public SpaceTimeDistortionEvent(EntitySpaceTimeDistortion distortion) {
        this.distortion = distortion;
    }

    public EntitySpaceTimeDistortion getDistortion() {
        return this.distortion;
    }

    public static class EndEvent
    extends SpaceTimeDistortionEvent {
        public EndEvent(EntitySpaceTimeDistortion distortion) {
            super(distortion);
        }
    }

    @Cancelable
    public static class ShrinkingEvent
    extends SpaceTimeDistortionEvent {
        private final int currentTick;

        public ShrinkingEvent(EntitySpaceTimeDistortion distortion, int currentTick) {
            super(distortion);
            this.currentTick = currentTick;
        }

        public int getCurrentTick() {
            return this.currentTick;
        }
    }

    @Cancelable
    public static class StartEvent
    extends SpaceTimeDistortionEvent {
        private Map<String, Double> items;
        private Map<PokemonSpec, Double> pokemon;

        public StartEvent(EntitySpaceTimeDistortion distortion, Map<String, Double> items, Map<PokemonSpec, Double> pokemon) {
            super(distortion);
            this.items = items;
            this.pokemon = pokemon;
        }

        public void setItems(Map<String, Double> items) {
            this.items = items;
        }

        public void setPokemon(Map<PokemonSpec, Double> pokemon) {
            this.pokemon = pokemon;
        }

        public Map<String, Double> getItems() {
            return this.items;
        }

        public Map<PokemonSpec, Double> getPokemon() {
            return this.pokemon;
        }
    }

    public static class ExpandingEvent
    extends SpaceTimeDistortionEvent {
        private final int currentTick;

        public ExpandingEvent(EntitySpaceTimeDistortion distortion, int currentTick) {
            super(distortion);
            this.currentTick = currentTick;
        }

        public int getCurrentTick() {
            return this.currentTick;
        }
    }

    @Cancelable
    public static class SpawnEvent
    extends SpaceTimeDistortionEvent {
        public SpawnEvent(EntitySpaceTimeDistortion distortion) {
            super(distortion);
        }
    }
}

