/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

public class BeatWildPixelmonEvent
extends Event {
    private final EntityPlayerMP player;
    private final WildPixelmonParticipant wpp;

    public BeatWildPixelmonEvent(EntityPlayerMP player, WildPixelmonParticipant wpp) {
        this.player = player;
        this.wpp = wpp;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public WildPixelmonParticipant getWildPokemon() {
        return this.wpp;
    }
}

