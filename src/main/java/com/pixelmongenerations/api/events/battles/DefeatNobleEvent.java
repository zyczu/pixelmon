/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.battles;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

public class DefeatNobleEvent
extends Event {
    private EntityPlayerMP player;
    private EntityPixelmon pokemon;

    public DefeatNobleEvent(EntityPlayerMP player, EntityPixelmon pokemon) {
        this.player = player;
        this.pokemon = pokemon;
    }

    public EntityPlayerMP getPlayer() {
        return this.player;
    }

    public EntityPixelmon getPokemon() {
        return this.pokemon;
    }
}

