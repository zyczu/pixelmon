/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.battles;

import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import net.minecraftforge.fml.common.eventhandler.Event;

public class TurnEndEvent
extends Event {
    private final BattleControllerBase bc;

    public TurnEndEvent(BattleControllerBase bc) {
        this.bc = bc;
    }

    public BattleControllerBase getBattleController() {
        return this.bc;
    }
}

