/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.battles;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class UseBattleItemEvent
extends Event {
    private PixelmonWrapper pixelmonWrapper;
    private ItemStack useStack;
    private boolean shouldConsume;

    public UseBattleItemEvent(PixelmonWrapper wrapper, ItemStack useStack) {
        this.pixelmonWrapper = wrapper;
        this.useStack = useStack;
        this.shouldConsume = true;
    }

    public EntityPlayerMP getPlayer() {
        return ((PlayerParticipant)this.pixelmonWrapper.getParticipant()).player;
    }

    public PixelmonWrapper getPixelmonWrapper() {
        return this.pixelmonWrapper;
    }

    public ItemStack getUsedStack() {
        return this.useStack;
    }

    public void setUsedStack(ItemStack itemStack) {
        this.useStack = itemStack;
        this.shouldConsume = false;
    }

    public void setItemConsume(boolean consumes) {
        this.shouldConsume = consumes;
    }

    public boolean shouldConsume() {
        return this.shouldConsume;
    }
}

