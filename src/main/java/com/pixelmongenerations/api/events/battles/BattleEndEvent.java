/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableMap
 */
package com.pixelmongenerations.api.events.battles;

import com.google.common.collect.ImmutableMap;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.core.enums.battle.BattleResults;
import com.pixelmongenerations.core.enums.battle.EnumBattleEndCause;
import java.util.ArrayList;
import java.util.HashMap;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

public class BattleEndEvent
extends Event {
    private final BattleControllerBase bc;
    private final EnumBattleEndCause cause;
    private final boolean abnormal;
    private final ImmutableMap<BattleParticipant, BattleResults> results;

    public BattleEndEvent(BattleControllerBase bc, EnumBattleEndCause cause, boolean abnormal, HashMap<BattleParticipant, BattleResults> results) {
        this.bc = bc;
        this.cause = cause;
        this.abnormal = abnormal;
        this.results = ImmutableMap.copyOf(results);
    }

    public BattleControllerBase getBattleController() {
        return this.bc;
    }

    public EnumBattleEndCause getEndCause() {
        return this.cause;
    }

    public boolean hasAbnormalEnd() {
        return this.abnormal;
    }

    public ImmutableMap<BattleParticipant, BattleResults> getBattleResults() {
        return this.results;
    }

    public ArrayList<EntityPlayerMP> getPlayers() {
        ArrayList<EntityPlayerMP> players = new ArrayList<EntityPlayerMP>();
        this.bc.participants.forEach(p -> {
            if (p instanceof PlayerParticipant) {
                players.add(((PlayerParticipant)p).player);
            }
        });
        return players;
    }
}

