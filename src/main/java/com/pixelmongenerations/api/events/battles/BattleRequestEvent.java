/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.battles;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class BattleRequestEvent
extends Event {
    private final EntityPlayerMP attacker;
    private final EntityPixelmon attackerPokemon;
    private final EntityPlayerMP target;
    private final EntityPixelmon targetPokemon;

    public BattleRequestEvent(EntityPlayerMP attacker, EntityPixelmon attackerPokemon, EntityPlayerMP target, EntityPixelmon targetPokemon) {
        this.attacker = attacker;
        this.attackerPokemon = attackerPokemon;
        this.target = target;
        this.targetPokemon = targetPokemon;
    }

    public EntityPlayerMP getAttacker() {
        return this.attacker;
    }

    public EntityPixelmon getAttackerPoke() {
        return this.attackerPokemon;
    }

    public EntityPlayerMP getTarget() {
        return this.target;
    }

    public EntityPixelmon getTargetPoke() {
        return this.targetPokemon;
    }
}

