/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.battles;

import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.common.battle.BattleRegistry;
import com.pixelmongenerations.common.battle.controller.BattleControllerBase;
import com.pixelmongenerations.common.battle.controller.participants.BattleParticipant;
import com.pixelmongenerations.common.battle.controller.participants.PlayerParticipant;
import com.pixelmongenerations.common.battle.controller.participants.WildPixelmonParticipant;
import com.pixelmongenerations.common.battle.rules.BattleRules;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.StatsType;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.battle.EnumBattleEndCause;
import com.pixelmongenerations.core.enums.battle.EnumBattleType;
import com.pixelmongenerations.core.network.EnumUpdateType;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class SOSSummonEvent
extends Event {
    private BattleControllerBase bc;
    private EntityPixelmon callingPokemon;
    private EntityPlayerMP player;
    private EntityPixelmon summonPokemon;
    private EntityPixelmon playersPokemon;

    public SOSSummonEvent(BattleControllerBase bc, EntityPixelmon callingPokemon, EntityPlayerMP player, EntityPixelmon playersPokemon) {
        this.bc = bc;
        this.callingPokemon = callingPokemon;
        this.player = player;
        this.playersPokemon = playersPokemon;
    }

    public EntityPlayer getPlayer() {
        return this.player;
    }

    public EntityPixelmon getCallingPokemon() {
        return this.callingPokemon;
    }

    public void setSummonPokemon(EntityPixelmon pokemon) {
        this.summonPokemon = pokemon;
    }

    public void summon() {
        int perfIVs = 0;
        int haChance = 0;
        int shinyRolls = 0;
        int chain = this.bc.sosChain;
        if (chain >= 5 && chain < 10) {
            perfIVs = 1;
            haChance = 0;
            shinyRolls = 1;
        } else if (chain == 10) {
            perfIVs = 2;
            haChance = 5;
            shinyRolls = 1;
        } else if (chain >= 11 && chain < 20) {
            perfIVs = 3;
            haChance = 5;
            shinyRolls = 5;
        } else if (chain == 20) {
            perfIVs = 3;
            haChance = 10;
            shinyRolls = 5;
        } else if (chain >= 21 && chain < 30) {
            perfIVs = 3;
            haChance = 10;
            shinyRolls = 9;
        } else if (chain >= 31) {
            perfIVs = 4;
            haChance = 15;
            shinyRolls = 13;
        }
        this.summonPokemon = PokemonSpec.from(this.callingPokemon.getPokemonName()).create(this.callingPokemon.world);
        if (perfIVs > 0) {
            StatsType[] stats = new StatsType[]{StatsType.HP, StatsType.Attack, StatsType.Defence, StatsType.SpecialAttack, StatsType.SpecialDefence, StatsType.Speed};
            ArrayList<StatsType> notPerfIVs = new ArrayList<StatsType>();
            for (int i = 0; i < 6; ++i) {
                if (this.summonPokemon.stats.IVs.isMaxed(stats[i])) continue;
                notPerfIVs.add(stats[i]);
            }
            if (notPerfIVs.size() > 0) {
                StatsType stat = (StatsType)((Object)notPerfIVs.get(new Random().nextInt(notPerfIVs.size())));
                this.summonPokemon.stats.IVs.maxIV(stat);
                this.summonPokemon.updateStats();
            }
        }
        if (this.summonPokemon.baseStats.abilities.length > 2 && haChance > 0 && RandomHelper.getRandomNumberBetween(haChance, PixelmonConfig.hiddenAbilityRate) == haChance) {
            this.summonPokemon.setAbilitySlot(2);
            this.summonPokemon.update(EnumUpdateType.Ability);
        }
        if (shinyRolls > 0 && !this.summonPokemon.isShiny()) {
            for (int i = 0; i < shinyRolls; ++i) {
                if (RandomHelper.getRandomNumberBetween(1.0f, PixelmonConfig.shinyRate) != 1.0f) continue;
                this.summonPokemon.setShiny(true);
                break;
            }
        }
        ++chain;
        PlayerParticipant playerPart = new PlayerParticipant(this.player, this.playersPokemon);
        WildPixelmonParticipant wildPokemon = this.bc.participants.get(0) instanceof WildPixelmonParticipant ? (WildPixelmonParticipant)this.bc.participants.get(0) : (WildPixelmonParticipant)this.bc.participants.get(1);
        WildPixelmonParticipant addedPokemon = new WildPixelmonParticipant(this.summonPokemon);
        if (addedPokemon.getPokemon().pokemon != null) {
            EntityPixelmon original = wildPokemon.getTeamPokemon().get((int)0).pokemon;
            int x = original.getPosition().getX();
            int y = original.getPosition().getY();
            int z = original.getPosition().getZ();
            addedPokemon.getPokemon().pokemon.setLocationAndAngles(x, y, z, 0.0f, 0.0f);
            this.player.world.spawnEntity(addedPokemon.getPokemon().pokemon);
        }
        BattleParticipant[] wilds = new BattleParticipant[]{wildPokemon, addedPokemon};
        BattleParticipant[] players = new BattleParticipant[]{playerPart};
        int currentHealth = wildPokemon.getTeamPokemon().get(0).getHealth();
        int currentPlayerPokemonHealth = this.playersPokemon.getPixelmonWrapper().getHealth();
        this.bc.endBattle(EnumBattleEndCause.FORCE);
        BattleRegistry.getBattle(this.player).endBattle(EnumBattleEndCause.NORMAL);
        Timer t = new Timer();
        int finalChain = chain;
        final BattleControllerBase newBattle = new BattleControllerBase(wilds, players, new BattleRules(EnumBattleType.SOS));
        newBattle.setSosBattle(true);
        newBattle.setHordeBattle(false);
        newBattle.update();
        newBattle.updateClients();
        newBattle.getParticipantForEntity((EntityLivingBase)wildPokemon.getTeamPokemon().get((int)0).pokemon).controlledPokemon.get(0).setHealth(currentHealth);
        newBattle.getParticipantForEntity((EntityLivingBase)wildPokemon.getTeamPokemon().get((int)0).pokemon).controlledPokemon.get(0).updateHPIncrease();
        newBattle.sosChain = finalChain;
        t.schedule(new TimerTask(){

            @Override
            public void run() {
                BattleRegistry.registerBattle(newBattle);
            }
        }, 10L);
    }
}

