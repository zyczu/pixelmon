/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.battles;

import com.pixelmongenerations.core.enums.items.EnumPokeball;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.fml.common.eventhandler.Event;

public class PokeballSpeedEvent
extends Event {
    private EntityLivingBase thrower;
    private EnumPokeball pokeball;
    private float speed;

    public PokeballSpeedEvent(EntityLivingBase thrower, EnumPokeball pokeball, float speed) {
        this.thrower = thrower;
        this.pokeball = pokeball;
        this.speed = speed;
    }

    public EntityLivingBase getThrower() {
        return this.thrower;
    }

    public EnumPokeball getBallType() {
        return this.pokeball;
    }

    public float getSpeed() {
        return this.speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }
}

