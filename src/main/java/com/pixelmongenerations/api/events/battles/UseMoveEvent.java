/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.battles;

import com.pixelmongenerations.common.battle.attacks.AttackBase;
import com.pixelmongenerations.common.battle.attacks.ZAttackBase;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class UseMoveEvent
extends Event {
    private PixelmonWrapper pixelmonWrapper;
    private AttackBase base;
    private String failMessage;

    public UseMoveEvent(PixelmonWrapper wrapper, AttackBase base, String failMessage) {
        this.pixelmonWrapper = wrapper;
        this.base = base;
        this.failMessage = failMessage;
    }

    public PixelmonWrapper getPixelmonWrapper() {
        return this.pixelmonWrapper;
    }

    public AttackBase getMoveBase() {
        return this.base;
    }

    public void setFailMessage(String failMessage) {
        this.failMessage = failMessage;
    }

    public String getFailMessage() {
        return this.failMessage;
    }

    @Cancelable
    public static class UseMaxMoveEvent
    extends UseMoveEvent {
        public UseMaxMoveEvent(PixelmonWrapper wrapper, AttackBase base, String failMessage) {
            super(wrapper, base, failMessage);
        }
    }

    @Cancelable
    public static class UseZMoveEvent
    extends UseMoveEvent {
        private ZAttackBase zBase;

        public UseZMoveEvent(PixelmonWrapper wrapper, ZAttackBase base, String failMessage) {
            super(wrapper, base, failMessage);
            this.zBase = base;
        }

        public ZAttackBase getZMoveBase() {
            return this.zBase;
        }
    }
}

