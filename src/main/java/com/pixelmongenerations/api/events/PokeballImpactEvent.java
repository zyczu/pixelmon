/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  javax.annotation.Nullable
 */
package com.pixelmongenerations.api.events;

import com.pixelmongenerations.common.entity.pokeballs.EntityEmptyPokeball;
import javax.annotation.Nullable;
import net.minecraft.entity.Entity;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class PokeballImpactEvent
extends Event {
    private final EntityThrowable pokeball;
    private final RayTraceResult ballPosition;
    private final boolean emptyBall;

    public PokeballImpactEvent(EntityThrowable pokeball, RayTraceResult movingobjectposition) {
        this.pokeball = pokeball;
        this.ballPosition = movingobjectposition;
        this.emptyBall = pokeball instanceof EntityEmptyPokeball;
    }

    public EntityThrowable getPokeBall() {
        return this.pokeball;
    }

    public RayTraceResult getBallPosition() {
        return this.ballPosition;
    }

    public boolean isEmptyBall() {
        return this.emptyBall;
    }

    @Nullable
    public Entity getEntityHit() {
        return this.ballPosition.entityHit;
    }
}

