/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.commands;

import com.pixelmongenerations.api.command.PixelmonCommand;
import net.minecraft.command.ICommandSender;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class ExecutePixelmonCommandEvent
extends Event {
    private final ICommandSender sender;
    private PixelmonCommand command;
    private String[] args;

    public ExecutePixelmonCommandEvent(ICommandSender sender, PixelmonCommand command, String[] args) {
        this.sender = sender;
        this.command = command;
        this.args = args;
    }

    public ICommandSender getSender() {
        return this.sender;
    }

    public PixelmonCommand getCommand() {
        return this.command;
    }

    public void setCommand(PixelmonCommand command) {
        if (command != null) {
            this.command = command;
        }
    }

    public String[] getArgs() {
        return this.args;
    }

    public void setArgs(String[] args) {
        if (args != null) {
            for (String arg : args) {
                if (arg != null) continue;
                return;
            }
            this.args = args;
        }
    }
}

