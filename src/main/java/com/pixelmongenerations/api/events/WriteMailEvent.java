/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class WriteMailEvent
extends Event {
    private final EntityPlayer player;
    private String contents;

    public WriteMailEvent(EntityPlayer player, String contents) {
        this.player = player;
        this.contents = contents;
    }

    public EntityPlayer getPlayer() {
        return this.player;
    }

    public String getContents() {
        return this.contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }
}

