/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.pokemon;

import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.eventhandler.Event;

public class PokemonMovementEvent
extends Event {
    private final EntityPixelmon pokemon;
    private final BlockPos lastPos;
    private final BlockPos currentPos;

    public PokemonMovementEvent(EntityPixelmon pokemon, BlockPos lastPos, BlockPos currentPos) {
        this.pokemon = pokemon;
        this.lastPos = lastPos;
        this.currentPos = currentPos;
    }

    public EntityPixelmon getPokemon() {
        return this.pokemon;
    }

    public BlockPos getLastPos() {
        return this.lastPos;
    }

    public BlockPos getCurrentPos() {
        return this.currentPos;
    }
}

