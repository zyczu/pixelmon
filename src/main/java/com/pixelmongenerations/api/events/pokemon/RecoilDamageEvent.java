/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.events.pokemon;

import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;

@Cancelable
public class RecoilDamageEvent
extends Event {
    private final PixelmonWrapper pokemon;
    private final float damage;

    public RecoilDamageEvent(PixelmonWrapper pokemon, float damage) {
        this.pokemon = pokemon;
        this.damage = damage;
    }

    public PixelmonWrapper getPokemon() {
        return this.pokemon;
    }

    public float getDamage() {
        return this.damage;
    }
}

