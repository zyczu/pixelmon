/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.api.ui;

import com.pixelmongenerations.core.util.ISerializable;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class Popup
implements ISerializable {
    private String texture;
    private int width;
    private int height;
    private String text;
    private int textColor;
    private boolean textOutline;
    private int textOffsetX;
    private int textOffsetY;
    private int showTicks;

    public Popup(String texture, int width, int height) {
        this(texture, width, height, "", 0, false, 0, 0, 50);
    }

    public Popup(String texture, int width, int height, String text, int textOffsetX, int textOffsetY) {
        this(texture, width, height, text, 0xFFFFFF, true, textOffsetX, textOffsetY, 50);
    }

    public Popup(String texture, int width, int height, String text, int textColor, boolean textOutline, int textOffsetX, int textOffsetY) {
        this(texture, width, height, text, textColor, textOutline, textOffsetX, textOffsetY, 50);
    }

    public Popup(String texture, int width, int height, String text, int textColor, boolean textOutline, int textOffsetX, int textOffsetY, int showTicks) {
        this.texture = texture;
        this.width = width;
        this.height = height;
        this.text = text;
        this.textColor = textColor;
        this.textOutline = textOutline;
        this.textOffsetX = textOffsetX;
        this.textOffsetY = textOffsetY;
        this.showTicks = showTicks;
    }

    public String texture() {
        return this.texture;
    }

    public int width() {
        return this.width;
    }

    public int height() {
        return this.height;
    }

    public String text() {
        return this.text;
    }

    public int textColor() {
        return this.textColor;
    }

    public boolean textOutline() {
        return this.textOutline;
    }

    public int textOffsetX() {
        return this.textOffsetX;
    }

    public int textOffsetY() {
        return this.textOffsetY;
    }

    public int showTicks() {
        return this.showTicks;
    }

    @Override
    public void serialize(ByteBuf buffer) {
        ByteBufUtils.writeUTF8String(buffer, this.texture);
        buffer.writeInt(this.width);
        buffer.writeInt(this.height);
        ByteBufUtils.writeUTF8String(buffer, this.text);
        buffer.writeInt(this.textColor);
        buffer.writeBoolean(this.textOutline);
        buffer.writeInt(this.textOffsetX);
        buffer.writeInt(this.textOffsetY);
        buffer.writeInt(this.showTicks);
    }

    public static Popup unserialize(ByteBuf buffer) {
        return new Popup(ByteBufUtils.readUTF8String(buffer), buffer.readInt(), buffer.readInt(), ByteBufUtils.readUTF8String(buffer), buffer.readInt(), buffer.readBoolean(), buffer.readInt(), buffer.readInt(), buffer.readInt());
    }
}

