/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.world;

import java.awt.Color;
import net.minecraft.util.math.Vec3d;

public class BoxZone {
    private String name;
    private Color color;
    private Vec3d min;
    private Vec3d max;

    private BoxZone(String name, Color color, Vec3d min, Vec3d max) {
        this.name = name;
        this.color = color;
        this.min = min;
        this.max = max;
    }

    public String getZoneName() {
        return this.name;
    }

    public Color getColor() {
        return this.color;
    }

    public Vec3d getMin() {
        return this.min;
    }

    public Vec3d getMax() {
        return this.max;
    }

    public static BoxZone of(String name, Color color, Vec3d min, Vec3d max) {
        return new BoxZone(name, color, min, max);
    }
}

