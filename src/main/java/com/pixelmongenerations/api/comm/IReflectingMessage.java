/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package com.pixelmongenerations.api.comm;

import io.netty.buffer.ByteBuf;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public interface IReflectingMessage
extends IMessage {
    @Override
    default public void fromBytes(ByteBuf buff) {
        for (Field field : this.getClass().getFields()) {
            if (!this.isValidField(field)) continue;
            try {
                Object value;
                if (field.getType() == Boolean.class) {
                    value = buff.readBoolean();
                } else if (field.getType() == Integer.class) {
                    value = buff.readInt();
                } else if (field.getType() == String.class) {
                    value = ByteBufUtils.readUTF8String(buff);
                } else if (field.getType() == Short.class) {
                    value = buff.readShort();
                } else if (field.getType() == Byte.class) {
                    value = buff.readByte();
                } else {
                    throw new IllegalArgumentException("Type is not a Boolean, Integer, String, Short, and or Byte");
                }
                field.set(this, value);
            }
            catch (IllegalAccessException | IllegalArgumentException | IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    default public void toBytes(ByteBuf buff) {
        for (Field field : this.getClass().getFields()) {
            if (!this.isValidField(field)) continue;
            try {
                if (field.getType() == Boolean.class) {
                    buff.writeBoolean(field.getBoolean(this));
                    continue;
                }
                if (field.getType() == Integer.class) {
                    buff.writeInt(field.getInt(this));
                    continue;
                }
                if (field.getType() == String.class) {
                    ByteBufUtils.writeUTF8String(buff, (String)field.get(this));
                    continue;
                }
                if (field.getType() == Short.class) {
                    buff.writeShort((int)field.getShort(this));
                    continue;
                }
                if (field.getType() == Byte.class) {
                    buff.writeByte((int)field.getByte(this));
                    continue;
                }
                throw new IllegalArgumentException("What is this variable: " + field.getName() + "/" + field.getType().getName());
            }
            catch (IllegalAccessException | IllegalArgumentException | IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    }

    default public boolean isValidField(Field field) {
        boolean isPublic = Modifier.isPublic(field.getModifiers());
        boolean isStatic = Modifier.isStatic(field.getModifiers());
        return isPublic && !isStatic;
    }
}

