/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning.archetypes.entities.items;

import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.archetypes.entities.items.SpawnActionItem;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.util.NBTTools;
import java.util.ArrayList;
import java.util.Map;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

public class SpawnInfoItem
extends SpawnInfo {
    public static final String TYPE_ID_ITEM = "item";
    public String itemID = "minecraft:string";
    public int minQuantity = 1;
    public int maxQuantity = 1;
    public int meta = -1;
    public Map<String, Object> nbt = null;
    public transient ItemStack itemStack = null;

    public SpawnInfoItem() {
        super(TYPE_ID_ITEM);
    }

    @Override
    public void onImport() {
        super.onImport();
        Item item = null;
        if (this.itemID.contains(":")) {
            ResourceLocation resourceLocation = new ResourceLocation(this.itemID.split(":")[0], this.itemID.split(":")[1]);
            item = Item.REGISTRY.getObject(resourceLocation);
            if (item == null) {
                Pixelmon.LOGGER.error("Invalid item ID found in SpawnInfo: " + this.itemID + " is not a recognized item.");
                return;
            }
        } else {
            ArrayList matchingItems = new ArrayList();
            Item.REGISTRY.forEach(testItem -> {
                if (testItem.getRegistryName().getPath().equalsIgnoreCase(this.itemID)) {
                    matchingItems.add(testItem);
                }
            });
            if (matchingItems.isEmpty()) {
                Pixelmon.LOGGER.error("Invalid item ID found in SpawnInfo: " + this.itemID + " is not a recognized item.");
                return;
            }
            if (matchingItems.size() > 1) {
                Pixelmon.LOGGER.error("Duplicate items found for item ID in SpawnInfo: " + this.itemID + ". Prefix this id with the resource domain, such as: 'minecraft:'");
                return;
            }
            item = (Item)matchingItems.get(0);
        }
        this.itemStack = this.meta == -1 ? new ItemStack(item) : new ItemStack(item, this.meta);
        NBTTagCompound deserializedNBT = null;
        if (this.nbt != null) {
            deserializedNBT = NBTTools.nbtFromMap(this.nbt);
        }
        if (deserializedNBT != null) {
            this.itemStack.setTagCompound(deserializedNBT);
            if (deserializedNBT.hasKey("Damage")) {
                this.itemStack.setItemDamage(deserializedNBT.getInteger("Damage"));
            }
        }
    }

    public SpawnAction<? extends EntityItem> construct(AbstractSpawner spawner, SpawnLocation spawnLocation) {
        return new SpawnActionItem(this, spawnLocation);
    }

    @Override
    public String toString() {
        return this.itemStack.getDisplayName();
    }
}

