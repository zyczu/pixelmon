/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning.archetypes.entities.npcs;

import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.archetypes.entities.npcs.SpawnInfoNPC;
import com.pixelmongenerations.common.entity.npcs.EntityNPC;
import com.pixelmongenerations.core.config.PixelmonEntityList;
import com.pixelmongenerations.core.enums.EnumNPCType;

public class SpawnActionNPC
extends SpawnAction<EntityNPC> {
    public EnumNPCType npcType;

    public SpawnActionNPC(SpawnInfoNPC spawnInfo, SpawnLocation spawnLocation) {
        super(spawnInfo, spawnLocation);
        this.npcType = spawnInfo.npcType;
    }

    @Override
    protected EntityNPC createEntity() {
        return (EntityNPC)PixelmonEntityList.createEntityByName(this.npcType.getDefaultName(), this.spawnLocation.location.world);
    }
}

