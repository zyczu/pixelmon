/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning.archetypes.entities.pokemon;

import com.pixelmongenerations.api.events.player.PlayerPokemonSpawnEvent;
import com.pixelmongenerations.api.events.player.PlayerShinyChanceEvent;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.api.spawning.AbstractSpawner;
import com.pixelmongenerations.api.spawning.SpawnAction;
import com.pixelmongenerations.api.spawning.SpawnInfo;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.archetypes.entities.pokemon.SpawnActionPokemon;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.spawning.PlayerTrackingSpawner;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.enums.EnumNature;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.PixelmonMethods;
import com.pixelmongenerations.core.util.helper.RandomHelper;
import java.util.Optional;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.common.MinecraftForge;

public class SpawnInfoPokemon
extends SpawnInfo {
    public static final String TYPE_ID_POKEMON = "pokemon";
    private PokemonSpec spec = PokemonSpec.from("Psyduck");
    public int minLevel = 1;
    public int maxLevel = 100;
    public Float spawnSpecificShinyRate = null;

    public SpawnInfoPokemon() {
        super(TYPE_ID_POKEMON);
    }

    @Override
    public void onImport() {
        this.calculateRequiredSpace();
        super.onImport();
        if (this.minLevel > this.maxLevel) {
            Pixelmon.LOGGER.warn("A SpawnInfo for " + this.spec.name + " has minLevel=" + this.minLevel + " and maxLevel=" + this.maxLevel + " which is weird. Fixing.");
            int temp = this.minLevel;
            this.minLevel = this.maxLevel;
            this.maxLevel = temp;
        }
    }

    public void setPokemon(PokemonSpec spec) {
        this.spec = spec;
        this.calculateRequiredSpace();
    }

    public void setNature(EnumNature nature) {
        this.setNature(nature.index);
    }

    public void setNature(int natureIndex) {
        this.spec.nature = natureIndex;
    }

    public void setPseudoNature(EnumNature pseudoNature) {
        this.setPseudoNature(pseudoNature);
    }

    public void setPseudoNature(int natureIndex) {
        this.spec.pseudonature = natureIndex;
    }

    public void calculateRequiredSpace() {
        Optional<BaseStats> optBaseStats;
        if (this.spec.name != null && this.requiredSpace == -1 && (optBaseStats = EntityPixelmon.getBaseStats(this.spec.name)).isPresent()) {
            BaseStats baseStats = optBaseStats.get();
            this.requiredSpace = (int)Math.ceil(baseStats.length > baseStats.height ? (double)baseStats.length : (double)baseStats.height);
        }
    }

    public SpawnAction<EntityPixelmon> construct(AbstractSpawner spawner, SpawnLocation spawnLocation) {
        PokemonSpec spec = this.spec.copy();
        if (spec.name == null) {
            spec.name = EnumSpecies.Psyduck.name;
        }
        if (spec.level == null) {
            if (PixelmonConfig.spawnLevelsByDistance) {
                BlockPos spawnPos = spawnLocation.location.world.getSpawnPoint();
                double distanceFromSpawn = spawnPos.getDistance(spawnLocation.location.pos.getX(), spawnLocation.location.pos.getY(), spawnLocation.location.pos.getZ());
                int levelBase = (int)(distanceFromSpawn / (double)PixelmonConfig.distancePerLevel) + 1;
                if (levelBase > PixelmonConfig.maxLevelByDistance) {
                    levelBase = PixelmonConfig.maxLevelByDistance;
                }
                int variation = RandomHelper.getRandomNumberBetween(-5, 5);
                spec.level = MathHelper.clamp(levelBase + variation, 1, PixelmonConfig.maxLevelByDistance);
                if (spec.level > PixelmonConfig.maxLevel) {
                    spec.level = PixelmonConfig.maxLevel;
                }
            } else {
                spec.level = RandomHelper.getRandomNumberBetween(this.minLevel, this.maxLevel);
            }
        }
        spec.hasGmaxFactor = spec.getSpecies().hasGmaxForm() && spec.hasGmaxFactor == null ? Boolean.valueOf(RandomHelper.getRandomChance(1.0f / PixelmonConfig.gmaxFactorSpawnRate)) : Boolean.valueOf(false);
        if (spec.shiny == null) {
            if (this.spawnSpecificShinyRate != null) {
                if (this.spawnSpecificShinyRate.floatValue() == 0.0f) {
                    spec.shiny = false;
                } else if (this.spawnSpecificShinyRate.floatValue() > 0.0f) {
                    spec.shiny = RandomHelper.getRandomChance(1.0f / this.spawnSpecificShinyRate.floatValue());
                }
            } else if (this.set.setSpecificShinyRate != null) {
                if (this.set.setSpecificShinyRate.floatValue() == 0.0f) {
                    spec.shiny = false;
                } else if (this.set.setSpecificShinyRate.floatValue() > 0.0f) {
                    spec.shiny = RandomHelper.getRandomChance(1.0f / this.set.setSpecificShinyRate.floatValue());
                }
            } else if (spawner instanceof PlayerTrackingSpawner) {
                PlayerTrackingSpawner playerSpawner = (PlayerTrackingSpawner)spawner;
                float chance = PixelmonConfig.shinyRate;
                EntityPlayerMP player = playerSpawner.getTrackedPlayer();
                if (player != null) {
                    if (PixelmonConfig.enableCatchCombos) {
                        if (PixelmonConfig.enableCatchComboShinyLock && PixelmonMethods.isCatchComboSpecies(player, EnumSpecies.getFromNameAnyCase(spec.name))) {
                            chance = PixelmonMethods.getCatchComboChance(player);
                        } else if (!PixelmonConfig.enableCatchComboShinyLock) {
                            chance = PixelmonMethods.getCatchComboChance(player);
                        }
                    } else if (PixelmonMethods.isWearingShinyCharm(player)) {
                        chance = PixelmonConfig.shinyCharmRate;
                    }
                    PlayerShinyChanceEvent event = new PlayerShinyChanceEvent(player, chance);
                    MinecraftForge.EVENT_BUS.post(event);
                    chance = event.isCanceled() ? 0.0f : event.getChance();
                }
                spec.shiny = chance > 0.0f && RandomHelper.getRandomChance(1.0f / chance);
                if (spec.shiny.booleanValue() && RandomHelper.getRandomNumberBetween(1, PixelmonConfig.ultraShinyRate) == 1) {
                    spec.setParticleId("ultrashiny");
                }
                PlayerPokemonSpawnEvent spawnEvent = new PlayerPokemonSpawnEvent(player, spawnLocation, spec);
                MinecraftForge.EVENT_BUS.post(spawnEvent);
                spec = spawnEvent.getPokemonSpec();
            } else {
                spec.shiny = PixelmonConfig.shinyRate > 0.0f && RandomHelper.getRandomChance(1.0f / PixelmonConfig.shinyRate);
            }
        }
        return new SpawnActionPokemon(this, spawnLocation, spec);
    }

    public PokemonSpec getPokemonSpec() {
        return this.spec;
    }

    @Override
    public String toString() {
        if (this.spec.name == null || this.spec.name.equals("random")) {
            return "Random";
        }
        return I18n.translateToLocal("pixelmon." + this.spec.name.toLowerCase() + ".name");
    }
}

