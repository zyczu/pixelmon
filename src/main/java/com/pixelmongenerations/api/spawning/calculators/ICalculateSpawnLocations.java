/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning.calculators;

import com.pixelmongenerations.api.events.spawning.SpawnLocationEvent;
import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.conditions.LocationType;
import com.pixelmongenerations.api.world.BlockCollection;
import com.pixelmongenerations.api.world.MutableLocation;
import com.pixelmongenerations.core.config.BetterSpawnerConfig;
import java.util.ArrayList;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public interface ICalculateSpawnLocations {
    public static final int MIN_RADIUS = 1;

    public static ICalculateSpawnLocations getDefault() {
        return new DummyImpl();
    }

    default public int getMaxSpawnLocationRadius() {
        return 10;
    }

    default public boolean maximiseLocationTypesForSpawnLocation() {
        return true;
    }

    default public ArrayList<SpawnLocation> calculateSpawnableLocations(BlockCollection collection) {
        ArrayList<SpawnLocation> spawnableLocations = new ArrayList<SpawnLocation>();
        World world = collection.world;
        int minX = collection.minX + 1;
        int minY = collection.minY + 1;
        int minZ = collection.minZ + 1;
        int maxX = collection.maxX - 1;
        int maxY = collection.maxY - 1;
        int maxZ = collection.maxZ - 1;
        for (int baseX = minX; baseX <= maxX; ++baseX) {
            for (int baseZ = minZ; baseZ <= maxZ; ++baseZ) {
                IBlockState state;
                boolean canSeeSky = true;
                for (int skyY = 254; skyY >= maxY + 1; --skyY) {
                    if (!canSeeSky || BetterSpawnerConfig.doesBlockSeeSky(collection.getBlockState(baseX, skyY + 1, baseZ))) continue;
                    canSeeSky = false;
                    break;
                }
                for (int baseY = maxY - 1; baseY >= minY && (state = collection.getBlockState(baseX, baseY + 1, baseZ)) != null; --baseY) {
                    MutableLocation loc;
                    SpawnLocation spawnLocation;
                    SpawnLocationEvent event;
                    int z;
                    ArrayList<LocationType> types;
                    if (canSeeSky && !BetterSpawnerConfig.doesBlockSeeSky(state)) {
                        canSeeSky = false;
                    }
                    if ((types = LocationType.getPotentialTypes(collection.getBlockState(baseX, baseY, baseZ))).isEmpty()) continue;
                    ArrayList<LocationType> extended = new ArrayList<LocationType>(types);
                    BlockPos base = new BlockPos(baseX, baseY + 1, baseZ);
                    int radius = 0;
                    int r = 0;
                    block4: while (true) {
                        int y = base.getY() + r;
                        for (int signX = -1; signX < 2; signX += 2) {
                            for (int signZ = -1; signZ < 2; signZ += 2) {
                                int x = base.getX() + r * signX;
                                z = base.getZ() + r * signZ;
                                if (x > maxX || x < minX || y > maxY || y < minY || z > maxZ || z < minZ) {
                                    if (r <= 1) continue;
                                    break block4;
                                }
                                IBlockState rstate = collection.getBlockState(x, y, z);
                                if (rstate == null) break block4;
                                if (r <= 1) {
                                    types.removeIf(type -> !type.surroundingBlockCondition.test(rstate));
                                    if (types.isEmpty()) continue;
                                    extended.removeIf(type -> !types.contains(type));
                                    continue;
                                }
                                extended.removeIf(type -> !type.surroundingBlockCondition.test(rstate));
                                if (extended.size() < types.size() && this.maximiseLocationTypesForSpawnLocation() || extended.isEmpty()) break block4;
                                types.removeIf(type -> !type.surroundingBlockCondition.test(rstate));
                            }
                        }
                        radius = r++;
                    }
                    int searchRad = this.getMaxSpawnLocationRadius();
                    ArrayList<Block> uniqueBlocks = new ArrayList<Block>();
                    for (int x = baseX - searchRad; x < baseX + searchRad; ++x) {
                        if (x > collection.maxX || x < collection.minX) continue;
                        for (int y = baseY - searchRad; y <= baseY + searchRad; ++y) {
                            if (y > collection.maxY || y < collection.minY) continue;
                            for (z = baseZ - searchRad; z <= baseZ + searchRad; ++z) {
                                if (z > collection.maxZ || z < collection.minZ || (state = collection.getBlockState(x, y, z)) == null || uniqueBlocks.contains(state.getBlock())) continue;
                                uniqueBlocks.add(state.getBlock());
                            }
                        }
                    }
                    boolean fCanSeeSky = canSeeSky;
                    types.removeIf(type -> type.seesSky != null && fCanSeeSky != type.seesSky);
                    types.removeIf(type -> type.neededNearbyBlockCondition != null && !type.neededNearbyBlockCondition.test(uniqueBlocks));
                    if (types.isEmpty() || MinecraftForge.EVENT_BUS.post(event = new SpawnLocationEvent(spawnLocation = new SpawnLocation(loc = new MutableLocation(world, baseX, baseY + 1, baseZ), types, collection.getBlockState(baseX, baseY, baseZ).getBlock(), uniqueBlocks, collection.getBiome(baseX, baseZ), canSeeSky, radius)))) continue;
                    spawnableLocations.add(event.getSpawnLocation());
                }
            }
        }
        return spawnableLocations;
    }

    public static class DummyImpl
    implements ICalculateSpawnLocations {
    }
}

