/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning.conditions;

import com.pixelmongenerations.api.spawning.SpawnLocation;
import com.pixelmongenerations.api.spawning.conditions.TriggerLocation;
import com.pixelmongenerations.core.config.BetterSpawnerConfig;
import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Predicate;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;

public class LocationType {
    public static final ArrayList<LocationType> locationTypes = new ArrayList();
    public String name;
    public Predicate<IBlockState> baseBlockCondition = state -> true;
    public Predicate<IBlockState> surroundingBlockCondition = state -> state.getBlock() == Blocks.AIR;
    public Predicate<ArrayList<Block>> neededNearbyBlockCondition = block -> true;
    public Consumer<SpawnLocation> mutator = spawnLocation -> {};
    public Boolean seesSky = null;
    public static LocationType LAND = new LocationType("Land").setSeesSky(true).setBaseBlockCondition(state -> BetterSpawnerConfig.getLandBlocks().contains(state.getBlock().getRegistryName().toString())).setSurroundingBlockCondition(state -> BetterSpawnerConfig.getAirBlocks().contains(state.getBlock().getRegistryName().toString()));
    public static LocationType UNDERGROUND = new LocationType("Underground").setSeesSky(false).setBaseBlockCondition(LocationType.LAND.baseBlockCondition).setSurroundingBlockCondition(LocationType.LAND.surroundingBlockCondition);
    public static LocationType WATER = new LocationType("Water").setSeesSky(true).setBaseBlockCondition(state -> BetterSpawnerConfig.getWaterBlocks().contains(state.getBlock().getRegistryName().toString())).setSurroundingBlockCondition(state -> BetterSpawnerConfig.getWaterBlocks().contains(state.getBlock().getRegistryName().toString()));
    public static LocationType SURFACE_WATER = new LocationType("Surface Water").setSeesSky(true).setBaseBlockCondition(LocationType.WATER.baseBlockCondition).setSurroundingBlockCondition(LocationType.LAND.surroundingBlockCondition);
    public static LocationType SEAFLOOR = new LocationType("Seafloor").setSeesSky(true).setBaseBlockCondition(LocationType.LAND.baseBlockCondition).setSurroundingBlockCondition(LocationType.WATER.surroundingBlockCondition);
    public static LocationType UNDERGROUND_WATER = new LocationType("Underground Water").setSeesSky(false).setBaseBlockCondition(LocationType.WATER.baseBlockCondition).setSurroundingBlockCondition(LocationType.WATER.surroundingBlockCondition);
    public static LocationType AIR = new LocationType("Air").setSeesSky(true).setBaseBlockCondition(LocationType.LAND.baseBlockCondition).setSurroundingBlockCondition(LocationType.LAND.surroundingBlockCondition).setLocationMutator(spawnLocation -> {
        if (spawnLocation.location.pos.getY() < 230 && spawnLocation.location.pos.getY() > 60) {
            spawnLocation.location.pos.setY(spawnLocation.location.pos.getY() + 20);
        }
    });
    public static LocationType LAVA = new LocationType("Lava").setBaseBlockCondition(state -> BetterSpawnerConfig.getLavaBlocks().contains(state.getBlock().getRegistryName().toString())).setSurroundingBlockCondition(state -> BetterSpawnerConfig.getLavaBlocks().contains(state.getBlock().getRegistryName().toString()));
    public static LocationType ROCK_SMASH = new TriggerLocation("Rock Smash");
    public static LocationType HEADBUTT = new TriggerLocation("Headbutt");
    public static TriggerLocation OLD_ROD = new TriggerLocation("Old Rod");
    public static TriggerLocation GOOD_ROD = new TriggerLocation("Good Rod");
    public static TriggerLocation SUPER_ROD = new TriggerLocation("Super Rod");
    public static TriggerLocation OLD_ROD_LAVA = new TriggerLocation("Old Rod Lava");
    public static TriggerLocation GOOD_ROD_LAVA = new TriggerLocation("Good Rod Lava");
    public static TriggerLocation SUPER_ROD_LAVA = new TriggerLocation("Super Rod Lava");

    public LocationType(String name) {
        this.name = name;
    }

    public LocationType setBaseBlockCondition(Predicate<IBlockState> baseBlockCondition) {
        this.baseBlockCondition = baseBlockCondition;
        return this;
    }

    public LocationType setSurroundingBlockCondition(Predicate<IBlockState> surroundingBlockCondition) {
        this.surroundingBlockCondition = surroundingBlockCondition;
        return this;
    }

    public LocationType setNeededNearbyBlockCondition(Predicate<ArrayList<Block>> neededNearbyBlockCondition) {
        this.neededNearbyBlockCondition = neededNearbyBlockCondition;
        return this;
    }

    public LocationType setSeesSky(Boolean seesSky) {
        this.seesSky = seesSky;
        return this;
    }

    public LocationType setLocationMutator(Consumer<SpawnLocation> mutator) {
        this.mutator = mutator;
        return this;
    }

    public String toString() {
        return this.name;
    }

    public static LocationType of(String name) {
        for (LocationType type : locationTypes) {
            if (!type.name.equalsIgnoreCase(name)) continue;
            return type;
        }
        return null;
    }

    public static ArrayList<LocationType> getPotentialTypes(IBlockState state) {
        ArrayList<LocationType> types = new ArrayList<LocationType>();
        for (LocationType type : locationTypes) {
            if (state == null || !type.baseBlockCondition.test(state)) continue;
            types.add(type);
        }
        return types;
    }

    static {
        locationTypes.add(LAND);
        locationTypes.add(UNDERGROUND);
        locationTypes.add(WATER);
        locationTypes.add(SURFACE_WATER);
        locationTypes.add(SEAFLOOR);
        locationTypes.add(UNDERGROUND_WATER);
        locationTypes.add(AIR);
        locationTypes.add(LAVA);
        locationTypes.add(ROCK_SMASH);
        locationTypes.add(HEADBUTT);
        locationTypes.add(OLD_ROD);
        locationTypes.add(GOOD_ROD);
        locationTypes.add(SUPER_ROD);
        locationTypes.add(OLD_ROD_LAVA);
        locationTypes.add(GOOD_ROD_LAVA);
        locationTypes.add(SUPER_ROD_LAVA);
    }
}

