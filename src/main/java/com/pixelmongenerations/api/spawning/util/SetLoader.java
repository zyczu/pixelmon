/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.common.collect.ImmutableList
 *  com.google.common.collect.Lists
 *  org.apache.logging.log4j.Level
 */
package com.pixelmongenerations.api.spawning.util;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.pixelmongenerations.api.pokemon.PokemonSpec;
import com.pixelmongenerations.api.spawning.SpawnSet;
import com.pixelmongenerations.api.spawning.archetypes.entities.pokemon.SpawnInfoPokemon;
import com.pixelmongenerations.api.spawning.conditions.LocationType;
import com.pixelmongenerations.api.spawning.conditions.WorldTime;
import com.pixelmongenerations.api.world.WeatherType;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;
import com.pixelmongenerations.common.entity.pixelmon.stats.BaseStats;
import com.pixelmongenerations.common.entity.pixelmon.stats.Rarity;
import com.pixelmongenerations.common.spawning.PixelmonBiomeDictionary;
import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.config.PixelmonConfig;
import com.pixelmongenerations.core.database.SpawnLocation;
import com.pixelmongenerations.core.enums.EnumSpecies;
import com.pixelmongenerations.core.util.helper.RCFileHelper;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.apache.logging.log4j.Level;

public class SetLoader {
    private static HashMap<String, SpawnSet> loadedSets = new HashMap();
    public static Class<? extends SpawnSet> targetedSpawnSetClass = SpawnSet.class;
    public static final String SPAWN_SET_ROOT = "pixelmon/spawning/";
    public static final String DEFAULT_SPAWN_SET_FOLDER = "default";

    public static <T> ImmutableList<T> getAllSets() {
        ArrayList<T> loadedSetsT = new ArrayList<>();
        for (SpawnSet spawnSet : loadedSets.values()) {
            loadedSetsT.add((T)spawnSet);
        }
        return ImmutableList.copyOf(loadedSetsT);
    }

    public static <T> T getSet(String name) {
        return (T)loadedSets.get(name);
    }

    public static void export(String dir, String name) {
        SpawnSet spawnSet = loadedSets.get(name);
        if (spawnSet != null) {
            spawnSet.export(dir);
        }
    }

    public static void exportAll(String dir) {
        for (SpawnSet spawnSet : loadedSets.values()) {
            spawnSet.export(dir);
        }
    }

    public static ArrayList<SpawnSet> importSetsFrom(String dir) {
        ArrayList<SpawnSet> setList = new ArrayList<SpawnSet>();
        File file = new File(dir);
        if (file.exists()) {
            ArrayList<File> jsons = new ArrayList<File>();
            if (file.isDirectory()) {
                SetLoader.recursiveSetSearch(dir, jsons);
            } else if (dir.endsWith(".set.json")) {
                jsons.add(file);
            }
            while (!jsons.isEmpty()) {
                File json = jsons.remove(0);
                try {
                    SpawnSet spawnSet = SpawnSet.deserialize(new FileReader(json));
                    setList.add(spawnSet);
                    loadedSets.put(spawnSet.id, spawnSet);
                }
                catch (Exception var6) {
                    var6.printStackTrace();
                }
            }
        }
        return setList;
    }

    public static void recursiveSetSearch(String dir, ArrayList<File> jsons) {
        File file = new File(dir);
        String[] var3 = file.list();
        int var4 = var3.length;
        for (String name : var3) {
            File subFile = new File(dir + "/" + name);
            if (subFile.isFile() && name.endsWith(".set.json")) {
                jsons.add(subFile);
                continue;
            }
            if (!subFile.isDirectory()) continue;
            SetLoader.recursiveSetSearch(dir + "/" + name, jsons);
        }
    }

    public static void addSet(SpawnSet set) {
        loadedSets.put(set.id, set);
    }

    public static String getSpawnSetPath() {
        return SPAWN_SET_ROOT + PixelmonConfig.spawnSetFolder;
    }

    public static void checkForMissingSpawnSets() {
        File file = new File("pixelmon/spawning/default/");
        file.mkdirs();
        SetLoader.retrieveSpawnSetsFromAssets();
    }

    public static ArrayList<SpawnSet> retrieveSpawnSetsFromAssets() {
        ArrayList<SpawnSet> spawnSets = new ArrayList<SpawnSet>();
        try {
            Path path = RCFileHelper.pathFromResourceLocation(new ResourceLocation("pixelmon", "spawning/"));
            List<Path> setPaths = RCFileHelper.listFilesRecursively(path, entry -> entry.getFileName().toString().endsWith(".json"), true);
            Iterator<Path> var3 = setPaths.iterator();
            while (var3.hasNext()) {
                Path setPath;
                Path lowerPath = setPath = var3.next();
                String root = "/assets/pixelmon/spawning/";
                String assetPath = "";
                while (!lowerPath.getParent().endsWith("spawning")) {
                    assetPath = lowerPath.getParent().getFileName().toString() + "/" + assetPath;
                    lowerPath = lowerPath.getParent();
                }
                InputStream iStream = SetLoader.class.getResourceAsStream(root + assetPath + setPath.getFileName());
                if (iStream == null) {
                    Pixelmon.LOGGER.log(Level.WARN, "Couldn't find internal spawning JSON at " + root + assetPath + setPath.getFileName());
                    continue;
                }
                String json = "";
                SpawnSet set = null;
                try {
                    Scanner s = new Scanner(iStream);
                    s.useDelimiter("\\A");
                    json = s.hasNext() ? s.next() : "";
                    s.close();
                    set = SpawnSet.deserialize(json);
                }
                catch (Exception e) {
                    Pixelmon.LOGGER.error("Couldn't load spawn JSON: " + root + assetPath + setPath.getFileName());
                    e.printStackTrace();
                    continue;
                }
                spawnSets.add(set);
                if (PixelmonConfig.useExternalJSONFilesSpawning) {
                    String primaryPath = "./pixelmon/spawning/default";
                    String relevantPath = "";
                    while (!setPath.getParent().endsWith("spawning")) {
                        relevantPath = setPath.getParent().getFileName().toString() + "/" + relevantPath;
                        setPath = setPath.getParent();
                    }
                    File file = new File(primaryPath + "/" + relevantPath + set.id + ".set.json");
                    if (!file.exists()) {
                        file.getParentFile().mkdirs();
                        PrintWriter pw = new PrintWriter(file);
                        pw.write(json);
                        pw.flush();
                        pw.close();
                    }
                } else {
                    loadedSets.put(set.id, set);
                }
                try {
                    iStream.close();
                }
                catch (IOException var15) {
                    var15.printStackTrace();
                }
            }
            return spawnSets;
        }
        catch (Exception var17) {
            var17.printStackTrace();
            return spawnSets;
        }
    }

    public static SpawnSet getDefaultSpawnSetFor(EnumSpecies pokemon) {
        SpawnSet set = new SpawnSet();
        set.id = pokemon.name;
        Optional<BaseStats> optBaseStats = EntityPixelmon.getBaseStats(pokemon.name);
        if (optBaseStats.isPresent()) {
            BaseStats bs = optBaseStats.get();
            PokemonSpec spec = PokemonSpec.from(pokemon.name);
            ArrayList<LocationType> locations = new ArrayList<LocationType>();
            for (SpawnLocation location : bs.spawnLocations) {
                if (location == SpawnLocation.AirPersistent) {
                    locations.add(LocationType.AIR);
                    continue;
                }
                if (location != SpawnLocation.Land && location != SpawnLocation.Air) {
                    if (location == SpawnLocation.UnderGround) {
                        locations.add(LocationType.UNDERGROUND);
                        continue;
                    }
                    if (location == SpawnLocation.Water) {
                        locations.add(LocationType.WATER);
                        continue;
                    }
                    System.out.println("Somehow no caught spawn location for " + bs.pokemon.name);
                    continue;
                }
                locations.add(LocationType.LAND);
            }
            if (EnumSpecies.rockSmashEncounters.contains(pokemon.name)) {
                locations.add(LocationType.ROCK_SMASH);
            }
            if (EnumSpecies.headbuttEncounters.contains(pokemon.name)) {
                locations.add(LocationType.HEADBUTT);
            }
            ArrayList<Biome> biomes = new ArrayList<Biome>();
            PixelmonBiomeDictionary.PixelmonBiomeInfo[] biomeInfos = PixelmonBiomeDictionary.getBiomeInfoList();
            for (Integer biomeID : bs.biomeIDs) {
                if (biomeID == null) continue;
                for (Biome biome : GameRegistry.findRegistry(Biome.class).getValues()) {
                    if (!biome.getRegistryName().getPath().toLowerCase().replaceAll(" ", "").replaceAll("_", "").equals(biomeInfos[biomeID.intValue()].biomeName.toLowerCase().replaceAll(" ", "").replaceAll("_", ""))) continue;
                    biomes.add(biome);
                }
            }
            float rarityMultiplier = 1.0f;
            if (locations.contains(LocationType.WATER)) {
                rarityMultiplier *= 3.0f;
            }
            if (locations.contains(LocationType.AIR)) {
                rarityMultiplier = (float)((double)rarityMultiplier / 1.5);
            }
            if (EnumSpecies.legendaries.contains(spec.name) || EnumSpecies.ultrabeasts.contains(spec.name)) {
                rarityMultiplier /= 1000.0f;
            }
            Rarity nativeRarity = bs.rarity;
            ArrayList<WorldTime> times = new ArrayList<WorldTime>();
            int rarity = 0;
            if (nativeRarity.dawndusk != 0) {
                times.add(WorldTime.DAWN);
                times.add(WorldTime.DUSK);
                rarity = nativeRarity.dawndusk;
            }
            if (nativeRarity.day != 0) {
                times.add(WorldTime.DAY);
                rarity = nativeRarity.day;
            }
            if (nativeRarity.night != 0) {
                times.add(WorldTime.NIGHT);
                rarity = nativeRarity.night;
            }
            SpawnInfoPokemon spawnInfo = new SpawnInfoPokemon();
            spawnInfo.setPokemon(spec);
            spawnInfo.condition.times = times;
            spawnInfo.condition.biomes = biomes;
            spawnInfo.locationTypes = locations;
            spawnInfo.condition.weathers = Lists.newArrayList(WeatherType.CLEAR, WeatherType.RAIN, WeatherType.STORM);
            spawnInfo.minLevel = bs.spawnLevel;
            spawnInfo.maxLevel = bs.spawnLevel + bs.spawnLevelRange;
            spawnInfo.rarity = rarityMultiplier * (float)rarity;
            set.spawnInfos.add(spawnInfo);
        }
        if (Pixelmon.devEnvironment && set.spawnInfos.isEmpty()) {
            System.out.println("Empty set: " + pokemon.name);
        }
        return set;
    }
}

