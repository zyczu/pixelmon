/*
 * Decompiled with CFR 0.150.
 */
package com.pixelmongenerations.api.spawning.util;

import java.util.ArrayList;
import net.minecraft.block.Block;

public class SpatialData {
    public int radius = 0;
    public Block baseBlock;
    public ArrayList<Block> uniqueSurroundingBlocks = new ArrayList();

    public SpatialData(int radius, Block baseBlock, ArrayList<Block> uniqueSurroundingBlocks) {
        this.radius = radius;
        this.baseBlock = baseBlock;
        this.uniqueSurroundingBlocks = uniqueSurroundingBlocks;
    }
}

