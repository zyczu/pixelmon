/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  dev.lightdream.lambda.LambdaExecutor$NoReturnNoArgLambdaExecutor
 */
package dev.lightdream.gui.manager;

import dev.lightdream.lambda.LambdaExecutor;
import java.util.HashMap;
import java.util.UUID;

public class PacketManager {
    private static final HashMap<UUID, HashMap<String, LambdaExecutor.NoReturnNoArgLambdaExecutor>> executorIDs = new HashMap();

    public static void registerPlayerExecutor(UUID uuid) {
        executorIDs.put(uuid, new HashMap());
    }

    public static void registerExecutor(UUID uuid, String id, LambdaExecutor.NoReturnNoArgLambdaExecutor executor) {
        if (executorIDs.get(uuid) == null) {
            PacketManager.registerPlayerExecutor(uuid);
        }
        executorIDs.get(uuid).put(id, executor);
    }

    public static void execute(UUID uuid, String id) {
        if (executorIDs.get(uuid) == null) {
            return;
        }
        LambdaExecutor.NoReturnNoArgLambdaExecutor executor = executorIDs.get(uuid).get(id);
        if (executor == null) {
            return;
        }
        executor.execute();
    }
}

