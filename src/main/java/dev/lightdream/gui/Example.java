/*
 * Decompiled with CFR 0.150.
 */
package dev.lightdream.gui;

import dev.lightdream.gui.dto.GUIElement;
import dev.lightdream.gui.dto.RenderPoint;
import dev.lightdream.gui.dto.network.NetworkButton;
import dev.lightdream.gui.dto.network.NetworkImage;
import dev.lightdream.gui.dto.network.NetworkText;
import dev.lightdream.gui.dto.network.NetworkTooltip;
import dev.lightdream.gui.dto.packet.ImagePacket;
import dev.lightdream.gui.manager.PacketManager;
import dev.lightdream.gui.network.DisplayOverlayPacket;
import dev.lightdream.gui.network.OpenGUIPacket;
import dev.lightdream.gui.utils.NetworkUtils;
import java.util.Arrays;
import java.util.List;
import net.minecraft.entity.player.EntityPlayerMP;

public class Example {
    public void sendImage(EntityPlayerMP player) {
        ImagePacket imagePacket = new ImagePacket("id", "https://url.to.some.image/image.png");
        imagePacket.send(player);
    }

    public void openCustomGUI(EntityPlayerMP player) {
        List<NetworkImage> images = Arrays.asList(new NetworkImage("id", 0, new GUIElement<NetworkImage>(0.0, 0.0, 250.0, 100.0, RenderPoint.CENTER)));
        PacketManager.registerExecutor(player.getUniqueID(), "execution-id", () -> {});
        List<NetworkButton> buttons = Arrays.asList(new NetworkButton(new NetworkImage("id", 0, new GUIElement<NetworkImage>(0.0, 0.0, 250.0, 100.0, RenderPoint.CENTER)), "execution-id"));
        List<NetworkTooltip> tooltips = Arrays.asList(new NetworkTooltip(0.0, 0.0, 100.0, 200.0, Arrays.asList("text"), 20, 10));
        List<NetworkText> texts = Arrays.asList(new NetworkText("text", 0xFFFFFF, 1.0f, new GUIElement<NetworkText>(0.0, 0.0, 100.0, 100.0, RenderPoint.CENTER)));
        OpenGUIPacket packet = new OpenGUIPacket(images, buttons, tooltips, texts);
        NetworkUtils.sendPacket(packet, (Object)player);
    }

    public void displayOverlay(EntityPlayerMP player) {
        List<NetworkImage> images = Arrays.asList(new NetworkImage("id", 0, new GUIElement<NetworkImage>(0.0, 0.0, 250.0, 100.0, RenderPoint.CENTER)));
        PacketManager.registerExecutor(player.getUniqueID(), "execution-id", () -> {});
        List<NetworkText> texts = Arrays.asList(new NetworkText("text", 0xFFFFFF, 1.0f, new GUIElement<NetworkText>(0.0, 0.0, 100.0, 100.0, RenderPoint.CENTER)));
        DisplayOverlayPacket packet = new DisplayOverlayPacket("overlay-id", images, texts);
        NetworkUtils.sendPacket(packet, (Object)player);
    }
}

