/*
 * Decompiled with CFR 0.150.
 */
package dev.lightdream.gui.utils;

import dev.lightdream.gui.dto.GUIElement;

public class GUICalculator {
    public static Double getScaleFactorX(int screenWidth) {
        return (double)screenWidth / 1920.0;
    }

    public static Double getScaleFactorY(int screenHeight) {
        return (double)screenHeight / 1080.0;
    }

    public static <T extends GUIElement<?>> T scale(GUIElement<T> element, int screenWidth, int screenHeight) {
        GUIElement output = (GUIElement)element.clone();
        if (output.scalePositionX) {
            output.x *= GUICalculator.getScaleFactorX(screenWidth).doubleValue();
        }
        if (output.scalePositionY) {
            output.y *= GUICalculator.getScaleFactorY(screenHeight).doubleValue();
        }
        if (output.scaleSizeX) {
            output.width *= GUICalculator.getScaleFactorX(screenWidth).doubleValue();
        }
        if (output.scaleSizeY) {
            output.height *= GUICalculator.getScaleFactorY(screenHeight).doubleValue();
        }
        switch (element.renderPoint) {
            case CENTER: {
                double centerX = (double)screenWidth / 2.0 - output.width / 2.0;
                double centerY = (double)screenHeight / 2.0 - output.height / 2.0;
                output.x += centerX;
                output.y = centerY - output.y;
                break;
            }
            case BOTTOM_LEFT: {
                double centerX = -output.width / 2.0;
                double centerY = (double)screenHeight - output.height / 2.0;
                output.x += centerX;
                output.y = centerY - output.y;
            }
        }
        return (T)output;
    }
}

