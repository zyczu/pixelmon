/*
 * Decompiled with CFR 0.150.
 */
package dev.lightdream.gui.utils;

import com.pixelmongenerations.core.Pixelmon;
import com.pixelmongenerations.core.network.packetHandlers.SyncManifest;
import dev.lightdream.gui.network.DisplayOverlayPacket;
import dev.lightdream.gui.network.OpenGUIPacket;
import net.minecraft.entity.player.EntityPlayerMP;

public class NetworkUtils {
    public static void sendPacket(DisplayOverlayPacket packet, Object player) {
        Pixelmon.NETWORK.sendTo(packet, (EntityPlayerMP)player);
    }

    public static void sendPacket(OpenGUIPacket packet, Object player) {
        Pixelmon.NETWORK.sendTo(packet, (EntityPlayerMP)player);
    }

    public static void sendPacket(SyncManifest manifest, Object object) {
        Pixelmon.NETWORK.sendTo(manifest, (EntityPlayerMP)object);
    }
}

