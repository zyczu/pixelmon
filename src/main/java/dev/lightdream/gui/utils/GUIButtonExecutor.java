/*
 * Decompiled with CFR 0.150.
 */
package dev.lightdream.gui.utils;

import com.pixelmongenerations.core.Pixelmon;
import dev.lightdream.gui.dto.packet.ExecutePacket;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class GUIButtonExecutor {
    private static List<String> rateLimit = new ArrayList<String>();

    public static void sendPacket(final ExecutePacket packet) {
        if (rateLimit.contains(packet.executeID)) {
            return;
        }
        rateLimit.add(packet.executeID);
        Timer timer = new Timer();
        timer.schedule(new TimerTask(){

            @Override
            public void run() {
                rateLimit.remove(packet.executeID);
            }
        }, 1000L);
        Pixelmon.NETWORK.sendToServer(packet);
    }
}

