/*
 * Decompiled with CFR 0.150.
 */
package dev.lightdream.gui.utils;

import com.pixelmongenerations.client.gui.overlay.GuiPixelmonOverlay;
import dev.lightdream.gui.gui.CustomGUI;
import dev.lightdream.gui.gui.CustomOverlay;
import dev.lightdream.gui.network.DisplayOverlayPacket;
import dev.lightdream.gui.network.OpenGUIPacket;
import net.minecraft.client.Minecraft;

public class GUIOpener {
    public static void openGUI(OpenGUIPacket packet) {
        Minecraft.getMinecraft().displayGuiScreen(new CustomGUI(packet.packet));
    }

    public static void displayOverlay(DisplayOverlayPacket packet) {
        CustomOverlay overlay = new CustomOverlay(packet.packet);
        GuiPixelmonOverlay.registerOverlay(packet.id, overlay);
    }
}

