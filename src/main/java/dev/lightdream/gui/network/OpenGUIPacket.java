/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package dev.lightdream.gui.network;

import dev.lightdream.gui.dto.network.NetworkButton;
import dev.lightdream.gui.dto.network.NetworkImage;
import dev.lightdream.gui.dto.network.NetworkText;
import dev.lightdream.gui.dto.network.NetworkTooltip;
import dev.lightdream.gui.dto.packet.GUIPacket;
import dev.lightdream.gui.utils.GUIOpener;
import io.netty.buffer.ByteBuf;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class OpenGUIPacket
implements IMessage {
    public GUIPacket packet;

    public OpenGUIPacket(List<NetworkImage> images, List<NetworkButton> buttons, List<NetworkTooltip> tooltips, List<NetworkText> texts) {
        this.packet = new GUIPacket(images, buttons, tooltips, texts);
    }

    public OpenGUIPacket() {
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        this.packet.serialize(buffer);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.packet = GUIPacket.deserialize(buffer);
    }

    public static class Handler
    implements IMessageHandler<OpenGUIPacket, IMessage> {
        @Override
        public IMessage onMessage(OpenGUIPacket packet, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> GUIOpener.openGUI(packet));
            return null;
        }
    }
}

