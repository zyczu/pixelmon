/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package dev.lightdream.gui.network;

import dev.lightdream.gui.dto.network.NetworkImage;
import dev.lightdream.gui.dto.network.NetworkText;
import dev.lightdream.gui.dto.packet.OverlayPacket;
import dev.lightdream.gui.utils.GUIOpener;
import io.netty.buffer.ByteBuf;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class DisplayOverlayPacket
implements IMessage {
    public String id;
    public OverlayPacket packet;

    public DisplayOverlayPacket(String id, List<NetworkImage> images, List<NetworkText> texts) {
        this.id = id;
        this.packet = new OverlayPacket(images, texts);
    }

    public DisplayOverlayPacket() {
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        ByteBufUtils.writeUTF8String(buffer, this.id);
        this.packet.serialize(buffer);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.id = ByteBufUtils.readUTF8String(buffer);
        this.packet = OverlayPacket.deserialize(buffer);
    }

    public static class Handler
    implements IMessageHandler<DisplayOverlayPacket, IMessage> {
        @Override
        public IMessage onMessage(DisplayOverlayPacket packet, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> GUIOpener.displayOverlay(packet));
            return null;
        }
    }
}

