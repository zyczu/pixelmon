/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 *  org.lwjgl.opengl.GL11
 */
package dev.lightdream.gui.dto.network;

import com.pixelmongenerations.core.util.ISerializable;
import dev.lightdream.gui.dto.GUIElement;
import dev.lightdream.gui.utils.GUICalculator;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.gui.FontRenderer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import org.lwjgl.opengl.GL11;

public class NetworkText
extends GUIElement<NetworkText>
implements ISerializable<NetworkText> {
    public String text;
    public int color;
    public float size;

    public NetworkText(String text, int color, float size, GUIElement<NetworkText> element) {
        super(element);
        this.text = text;
        this.color = color;
        this.size = size;
    }

    public static NetworkText deserialize(ByteBuf buffer) {
        GUIElement<NetworkText> element = GUIElement.deserialize(buffer, NetworkText.class);
        return new NetworkText(ByteBufUtils.readUTF8String(buffer), buffer.readInt(), buffer.readFloat(), element);
    }

    public void render(int screenWidth, int screenHeight, FontRenderer fontRenderer) {
        this.width = (double)((float)fontRenderer.getStringWidth(this.text) * this.size) * 1.0;
        this.height = (double)((float)fontRenderer.FONT_HEIGHT * this.size) * 1.0;
        NetworkText element = GUICalculator.scale(this, screenWidth, screenHeight);
        GL11.glPushMatrix();
        GL11.glScalef((float)element.size, (float)element.size, (float)element.size);
        fontRenderer.drawString(element.text, (int)(element.x / (double)element.size), (int)(element.y / (double)element.size), element.color);
        GL11.glPopMatrix();
    }

    @Override
    public void serialize(ByteBuf buffer) {
        super.serialize(buffer);
        ByteBufUtils.writeUTF8String(buffer, this.text);
        buffer.writeInt(this.color);
        buffer.writeFloat(this.size);
    }

    @Override
    public NetworkText clone() {
        return new NetworkText(this.text, this.color, this.size, this);
    }
}

