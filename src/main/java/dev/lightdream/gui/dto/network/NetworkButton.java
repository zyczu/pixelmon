/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package dev.lightdream.gui.dto.network;

import com.pixelmongenerations.core.util.ISerializable;
import dev.lightdream.gui.dto.network.NetworkImage;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class NetworkButton
implements ISerializable<NetworkButton> {
    public NetworkImage image;
    public String executor;

    public NetworkButton(NetworkImage image, String executor) {
        this.image = image;
        this.executor = executor;
    }

    public NetworkButton() {
    }

    public static NetworkButton deserialize(ByteBuf buffer) {
        NetworkImage image = NetworkImage.deserialize(buffer);
        String executor = ByteBufUtils.readUTF8String(buffer);
        return new NetworkButton(image, executor);
    }

    @Override
    public void serialize(ByteBuf buffer) {
        this.image.serialize(buffer);
        ByteBufUtils.writeUTF8String(buffer, this.executor);
    }
}

