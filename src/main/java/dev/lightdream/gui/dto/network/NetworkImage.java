/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 *  org.lwjgl.opengl.GL11
 */
package dev.lightdream.gui.dto.network;

import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.core.proxy.ClientProxy;
import com.pixelmongenerations.core.util.ISerializable;
import dev.lightdream.gui.dto.GUIElement;
import dev.lightdream.gui.utils.GUICalculator;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import org.lwjgl.opengl.GL11;

public class NetworkImage
extends GUIElement<NetworkImage>
implements ISerializable<NetworkImage> {
    public String texture;
    public int zLevel;

    public NetworkImage(String texture, int zLevel, GUIElement<NetworkImage> element) {
        super(element);
        this.texture = texture;
        this.zLevel = zLevel;
    }

    public static NetworkImage deserialize(ByteBuf buffer) {
        GUIElement<NetworkImage> element = GUIElement.deserialize(buffer, NetworkImage.class);
        String texture = ByteBufUtils.readUTF8String(buffer);
        int zLevel = buffer.readInt();
        return new NetworkImage(texture, zLevel, element);
    }

    @Override
    public void serialize(ByteBuf buffer) {
        super.serialize(buffer);
        ByteBufUtils.writeUTF8String(buffer, this.texture);
        buffer.writeInt(this.zLevel);
    }

    public void render(int screenWidth, int screenHeight) {
        NetworkImage image = GUICalculator.scale(this, screenWidth, screenHeight);
        GL11.glColor3f((float)1.0f, (float)1.0f, (float)1.0f);
        TextureResource texture = ClientProxy.TEXTURE_STORE.getObject(image.texture);
        if (texture != null) {
            texture.bindTexture();
            GuiHelper.drawImageQuad(image.x, image.y, image.width, (float)image.height, 0.0, 0.0, 1.0, 1.0, image.zLevel);
        } else {
            System.out.println("Texture " + image.texture + " was not found");
        }
    }

    @Override
    public NetworkImage clone() {
        return new NetworkImage(this.texture, this.zLevel, this);
    }
}

