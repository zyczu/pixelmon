/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package dev.lightdream.gui.dto.network;

import com.pixelmongenerations.core.util.ISerializable;
import dev.lightdream.gui.dto.network.NetworkImage;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.List;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class NetworkTooltip
implements ISerializable<NetworkImage> {
    public double x1;
    public double y1;
    public double x2;
    public double y2;
    public List<String> text;
    public int textRelativeX;
    public int textRelativeY;

    private NetworkTooltip(double x1, double y1, double x2, double y2, List<String> text, int textRelativeX, int textRelativeY, boolean privateMethod) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.text = text;
        this.textRelativeX = textRelativeX;
        this.textRelativeY = textRelativeY;
    }

    public NetworkTooltip(double x, double y, double width, double height, List<String> text, int textRelativeX, int textRelativeY) {
        this.x1 = x;
        this.x2 = x + width;
        this.y1 = y;
        this.y2 = y + height;
        this.text = text;
        this.textRelativeX = textRelativeX;
        this.textRelativeY = textRelativeY;
    }

    public static NetworkTooltip deserialize(ByteBuf buffer) {
        double x1 = buffer.readDouble();
        double y1 = buffer.readDouble();
        double x2 = buffer.readDouble();
        double y2 = buffer.readDouble();
        ArrayList<String> text = new ArrayList<String>();
        int size = buffer.readInt();
        for (int i = 0; i < size; ++i) {
            String t = ByteBufUtils.readUTF8String(buffer);
            text.add(t);
        }
        int textRelativeX = buffer.readInt();
        int textRelativeY = buffer.readInt();
        return new NetworkTooltip(x1, y1, x2, y2, text, textRelativeX, textRelativeY, false);
    }

    public double getWidth() {
        return Math.max(this.x1, this.x2) - Math.min(this.x1, this.x2);
    }

    public double getHeight() {
        return Math.max(this.y1, this.y2) - Math.min(this.y1, this.y2);
    }

    public double getPos1X() {
        return Math.min(this.x1, this.x2);
    }

    public double getPos1Y() {
        return Math.max(this.x1, this.x2);
    }

    public double getPos2X() {
        return Math.min(this.y1, this.y2);
    }

    public double getPos2Y() {
        return Math.max(this.y1, this.y2);
    }

    @Override
    public void serialize(ByteBuf buffer) {
        buffer.writeDouble(this.x1);
        buffer.writeDouble(this.y1);
        buffer.writeDouble(this.x2);
        buffer.writeDouble(this.y2);
        buffer.writeInt(this.text.size());
        this.text.forEach(text -> ByteBufUtils.writeUTF8String(buffer, text));
        buffer.writeInt(this.textRelativeX);
        buffer.writeInt(this.textRelativeY);
    }
}

