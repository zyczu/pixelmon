/*
 * Decompiled with CFR 0.150.
 */
package dev.lightdream.gui.dto.packet;

import com.pixelmongenerations.core.network.packetHandlers.SyncManifest;
import dev.lightdream.gui.utils.NetworkUtils;

public class ImagePacket {
    public String id;
    public String url;
    public String type;

    public ImagePacket(String id, String url) {
        this.id = id;
        this.url = url;
        this.type = "POPUP";
    }

    public ImagePacket(String id, String url, String type) {
        this.id = id;
        this.url = url;
        this.type = type;
    }

    public ImagePacket() {
    }

    public void send(Object player) {
        SyncManifest manifest = new SyncManifest();
        manifest.manifestJson = "{ \"resources\": [ { \"textureId\": \"" + this.id + "\", \"textureName\": \"" + this.id + "\", \"textureType\": \"" + this.type + "\", \"textureURL\": \"" + this.url + "\" }] }";
        manifest.type = SyncManifest.AssetManifestType.TEXTURE;
        NetworkUtils.sendPacket(manifest, player);
    }

    public ImagePacket clone() {
        return new ImagePacket(this.id, this.url, this.type);
    }
}

