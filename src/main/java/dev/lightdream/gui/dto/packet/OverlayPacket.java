/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package dev.lightdream.gui.dto.packet;

import com.pixelmongenerations.core.util.ISerializable;
import dev.lightdream.gui.dto.network.NetworkImage;
import dev.lightdream.gui.dto.network.NetworkText;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.List;

public class OverlayPacket
implements ISerializable<OverlayPacket> {
    public List<NetworkImage> images;
    public List<NetworkText> texts;

    public OverlayPacket(List<NetworkImage> images, List<NetworkText> texts) {
        this.images = images;
        this.texts = texts;
    }

    public OverlayPacket() {
    }

    public static OverlayPacket deserialize(ByteBuf buffer) {
        ArrayList<NetworkImage> images = new ArrayList<NetworkImage>();
        ArrayList<NetworkText> texts = new ArrayList<NetworkText>();
        int size1 = buffer.readInt();
        for (int i = 0; i < size1; ++i) {
            images.add(NetworkImage.deserialize(buffer));
        }
        int size3 = buffer.readInt();
        for (int i = 0; i < size3; ++i) {
            texts.add(NetworkText.deserialize(buffer));
        }
        return new OverlayPacket(images, texts);
    }

    @Override
    public void serialize(ByteBuf buffer) {
        buffer.writeInt(this.images.size());
        this.images.forEach(image -> image.serialize(buffer));
        buffer.writeInt(this.texts.size());
        this.texts.forEach(button -> button.serialize(buffer));
    }
}

