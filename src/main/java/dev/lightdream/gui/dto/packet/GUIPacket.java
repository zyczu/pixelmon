/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package dev.lightdream.gui.dto.packet;

import com.pixelmongenerations.core.util.ISerializable;
import dev.lightdream.gui.dto.network.NetworkButton;
import dev.lightdream.gui.dto.network.NetworkImage;
import dev.lightdream.gui.dto.network.NetworkText;
import dev.lightdream.gui.dto.network.NetworkTooltip;
import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.List;

public class GUIPacket
implements ISerializable<GUIPacket> {
    public List<NetworkImage> images;
    public List<NetworkButton> buttons;
    public List<NetworkTooltip> tooltips;
    public List<NetworkText> texts;

    public GUIPacket(List<NetworkImage> images, List<NetworkButton> buttons, List<NetworkTooltip> tooltips, List<NetworkText> texts) {
        this.images = images;
        this.buttons = buttons;
        this.tooltips = tooltips;
        this.texts = texts;
    }

    public GUIPacket() {
    }

    public static GUIPacket deserialize(ByteBuf buffer) {
        ArrayList<NetworkImage> images = new ArrayList<NetworkImage>();
        ArrayList<NetworkButton> buttons = new ArrayList<NetworkButton>();
        ArrayList<NetworkTooltip> tooltips = new ArrayList<NetworkTooltip>();
        ArrayList<NetworkText> texts = new ArrayList<NetworkText>();
        int size1 = buffer.readInt();
        for (int i = 0; i < size1; ++i) {
            images.add(NetworkImage.deserialize(buffer));
        }
        int size2 = buffer.readInt();
        for (int i = 0; i < size2; ++i) {
            buttons.add(NetworkButton.deserialize(buffer));
        }
        int size3 = buffer.readInt();
        for (int i = 0; i < size3; ++i) {
            tooltips.add(NetworkTooltip.deserialize(buffer));
        }
        int size4 = buffer.readInt();
        for (int i = 0; i < size4; ++i) {
            texts.add(NetworkText.deserialize(buffer));
        }
        return new GUIPacket(images, buttons, tooltips, texts);
    }

    @Override
    public void serialize(ByteBuf buffer) {
        buffer.writeInt(this.images.size());
        this.images.forEach(image -> image.serialize(buffer));
        buffer.writeInt(this.buttons.size());
        this.buttons.forEach(button -> button.serialize(buffer));
        buffer.writeInt(this.tooltips.size());
        this.tooltips.forEach(button -> button.serialize(buffer));
        buffer.writeInt(this.texts.size());
        this.texts.forEach(text -> text.serialize(buffer));
    }
}

