/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package dev.lightdream.gui.dto.packet;

import dev.lightdream.gui.manager.PacketManager;
import io.netty.buffer.ByteBuf;
import java.util.UUID;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ExecutePacket
implements IMessage {
    public String uuid;
    public String executeID;

    public ExecutePacket(String uuid, String executeID) {
        this.uuid = uuid;
        this.executeID = executeID;
    }

    public ExecutePacket() {
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        ByteBufUtils.writeUTF8String(buffer, this.uuid);
        ByteBufUtils.writeUTF8String(buffer, this.executeID);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        this.uuid = ByteBufUtils.readUTF8String(buffer);
        this.executeID = ByteBufUtils.readUTF8String(buffer);
    }

    public static class Handler
    implements IMessageHandler<ExecutePacket, IMessage> {
        @Override
        public IMessage onMessage(ExecutePacket message, MessageContext ctx) {
            PacketManager.execute(UUID.fromString(message.uuid), message.executeID);
            return null;
        }
    }
}

