/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package dev.lightdream.gui.dto;

import com.pixelmongenerations.core.util.ISerializable;
import dev.lightdream.gui.dto.RenderPoint;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class GUIElement<T>
implements ISerializable<T> {
    public double x;
    public double y;
    public double width;
    public double height;
    public boolean scalePositionX;
    public boolean scalePositionY;
    public boolean scaleSizeX;
    public boolean scaleSizeY;
    public RenderPoint renderPoint;

    public GUIElement(double x, double y, double width, double height, RenderPoint renderPoint, boolean scalePositionX, boolean scalePositionY, boolean scaleSizeX, boolean scaleSizeY) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.renderPoint = renderPoint;
        this.scalePositionX = scalePositionX;
        this.scalePositionY = scalePositionY;
        this.scaleSizeX = scaleSizeX;
        this.scaleSizeY = scaleSizeY;
    }

    public GUIElement(GUIElement<T> element) {
        this.x = element.x;
        this.y = element.y;
        this.width = element.width;
        this.height = element.height;
        this.renderPoint = element.renderPoint;
        this.scalePositionX = element.scalePositionX;
        this.scalePositionY = element.scalePositionY;
        this.scaleSizeX = element.scaleSizeX;
        this.scaleSizeY = element.scaleSizeY;
    }

    public GUIElement(double x, double y, double width, double height, RenderPoint renderPoint) {
        this(x, y, width, height, renderPoint, false, false, false, false);
    }

    public static <T> GUIElement<T> deserialize(ByteBuf buffer, Class<T> clazz) {
        double x = buffer.readDouble();
        double y = buffer.readDouble();
        double width = buffer.readDouble();
        double height = buffer.readDouble();
        boolean scalePositionX = buffer.readBoolean();
        boolean scalePositionY = buffer.readBoolean();
        boolean scaleSizeX = buffer.readBoolean();
        boolean scaleSizeY = buffer.readBoolean();
        String renderPoint = ByteBufUtils.readUTF8String(buffer);
        return new GUIElement<T>(x, y, width, height, RenderPoint.valueOf(renderPoint), scalePositionX, scalePositionY, scaleSizeX, scaleSizeY);
    }

    @Override
    public void serialize(ByteBuf buffer) {
        buffer.writeDouble(this.x);
        buffer.writeDouble(this.y);
        buffer.writeDouble(this.width);
        buffer.writeDouble(this.height);
        buffer.writeBoolean(this.scalePositionX);
        buffer.writeBoolean(this.scalePositionY);
        buffer.writeBoolean(this.scaleSizeX);
        buffer.writeBoolean(this.scaleSizeY);
        ByteBufUtils.writeUTF8String(buffer, this.renderPoint.toString());
    }

    public T clone() {
        throw new RuntimeException("#clone called but Not implemented");
    }
}

