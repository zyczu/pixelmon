/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package dev.lightdream.gui.gui;

import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.core.proxy.ClientProxy;
import dev.lightdream.gui.dto.packet.ExecutePacket;
import dev.lightdream.gui.utils.GUIButtonExecutor;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.opengl.GL11;

public class GUIButton
extends GuiButton {
    private final String image;
    private final int screenWidth;
    private final int screenHeight;
    private final String executor;
    private final int zLevel;
    private final double x;
    private final double y;
    private final double width;
    private final double height;

    public GUIButton(int buttonId, String image, double x, double y, double widthIn, double heightIn, int screenWidth, int screenHeight, int zLevel, String executor) {
        super(buttonId, (int)(x + (double)(screenWidth / 2) - widthIn / 2.0), (int)((double)((int)y + screenHeight / 2) - heightIn / 2.0), (int)widthIn, (int)heightIn, "");
        this.image = image;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        this.executor = executor;
        this.zLevel = zLevel;
        this.x = x;
        this.y = y;
        this.width = widthIn;
        this.height = heightIn;
    }

    @Override
    public boolean mousePressed(@NotNull Minecraft minecraft, int mouseX, int mouseY) {
        if (super.mousePressed(minecraft, mouseX, mouseY)) {
            ExecutePacket packet = new ExecutePacket(minecraft.player.getUniqueID().toString(), this.executor);
            GUIButtonExecutor.sendPacket(packet);
            return true;
        }
        return false;
    }

    @Override
    public void drawButton(@NotNull Minecraft minecraft, int mouseX, int mouseY, float partialTicks) {
        this.placeImage(this.image, this.x, this.y, this.width, this.height, this.zLevel);
    }

    private void placeImage(String image, double x, double y, double width, double height, int zLevel) {
        double centerX = (double)this.screenWidth / 2.0 - width / 2.0;
        double centerY = (double)this.screenHeight / 2.0 - height / 2.0;
        GL11.glColor3f((float)1.0f, (float)1.0f, (float)1.0f);
        TextureResource texture = ClientProxy.TEXTURE_STORE.getObject(image);
        if (texture != null) {
            texture.bindTexture();
            GuiHelper.drawImageQuad(x + centerX, y + centerY, width, (float)height, 0.0, 0.0, 1.0, 1.0, zLevel);
        } else {
            System.out.println("Image " + image + " was not found");
        }
    }
}

