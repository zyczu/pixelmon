/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package dev.lightdream.gui.gui;

import com.pixelmongenerations.client.gui.overlay.BaseOverlay;
import dev.lightdream.gui.dto.packet.OverlayPacket;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import org.lwjgl.opengl.GL11;

public class CustomOverlay
extends BaseOverlay {
    private final OverlayPacket packet;

    public CustomOverlay(OverlayPacket packet) {
        this.packet = packet;
    }

    private void placeText(String text, int x, int y, int screenWidth, int screenHeight, float size, FontRenderer fontRenderer, int color) {
        int width = (int)((float)fontRenderer.getStringWidth(text) * size);
        int height = (int)((float)fontRenderer.FONT_HEIGHT * size);
        int centerX = (int)((double)screenWidth / 2.0 - (double)(width / 2));
        int centerY = (int)((double)screenHeight / 2.0 - (double)(height / 2));
        GL11.glPushMatrix();
        GL11.glScalef((float)size, (float)size, (float)size);
        fontRenderer.drawString(text, (int)((float)(x + centerX) / size), (int)((float)(y + centerY) / size), color);
        GL11.glPopMatrix();
    }

    @Override
    public void render(int screenWidth, int screenHeight, Minecraft mc, FontRenderer fontRenderer) {
        fontRenderer.setUnicodeFlag(!fontRenderer.getUnicodeFlag());
        this.packet.images.forEach(image -> image.render(screenWidth, screenHeight));
        this.packet.texts.forEach(text -> text.render(screenWidth, screenHeight, fontRenderer));
        fontRenderer.setUnicodeFlag(!fontRenderer.getUnicodeFlag());
    }
}

