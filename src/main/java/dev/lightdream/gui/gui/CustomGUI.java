/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.lwjgl.opengl.GL11
 */
package dev.lightdream.gui.gui;

import com.pixelmongenerations.client.assets.resource.TextureResource;
import com.pixelmongenerations.client.gui.GuiHelper;
import com.pixelmongenerations.core.proxy.ClientProxy;
import dev.lightdream.gui.dto.packet.GUIPacket;
import dev.lightdream.gui.gui.GUIButton;
import java.util.concurrent.atomic.AtomicInteger;
import net.minecraft.client.gui.GuiScreen;
import org.lwjgl.opengl.GL11;

public class CustomGUI
extends GuiScreen {
    private final GUIPacket packet;

    public CustomGUI(GUIPacket packet) {
        this.packet = packet;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        this.packet.images.forEach(image -> this.placeImage(image.texture, image.x, image.y, image.width, image.height, image.zLevel));
        AtomicInteger id = new AtomicInteger();
        this.packet.buttons.forEach(button -> this.addButton(new GUIButton(id.getAndIncrement(), button.image.texture, button.image.x, button.image.y, button.image.width, button.image.height, this.width, this.height, button.image.zLevel, button.executor)));
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.packet.tooltips.forEach(tooltip -> {
            double centerX = (double)this.width / 2.0 - tooltip.getWidth() / 2.0;
            double centerY = (double)this.height / 2.0 - tooltip.getHeight() / 2.0;
            double x1 = tooltip.getPos1X() + centerX;
            double x2 = tooltip.getPos1Y() + centerX;
            double y1 = tooltip.getPos2X() + centerY;
            double y2 = tooltip.getPos2Y() + centerY;
            if (x1 < (double)mouseX && (double)mouseX < x2 && y1 < (double)mouseY && (double)mouseY < y2) {
                this.drawHoveringText(tooltip.text, mouseX + tooltip.textRelativeX, mouseY + tooltip.textRelativeY);
            }
        });
    }

    @Override
    public void initGui() {
        super.initGui();
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }

    private void placeImage(String image, double x, double y, double width, double height, int zLevel) {
        double centerX = (double)this.width / 2.0 - width / 2.0;
        double centerY = (double)this.height / 2.0 - height / 2.0;
        GL11.glColor3f((float)1.0f, (float)1.0f, (float)1.0f);
        TextureResource texture = ClientProxy.TEXTURE_STORE.getObject(image);
        if (texture != null) {
            texture.bindTexture();
            GuiHelper.drawImageQuad(x + centerX, y + centerY, width, (float)height, 0.0, 0.0, 1.0, 1.0, zLevel);
        } else {
            System.out.println("Image " + image + " was not found");
        }
    }

    private void placeImageExact(String image, double x, double y, double width, double height, int zLevel) {
        GL11.glColor3f((float)1.0f, (float)1.0f, (float)1.0f);
        TextureResource texture = ClientProxy.TEXTURE_STORE.getObject(image);
        if (texture != null) {
            texture.bindTexture();
            GuiHelper.drawImageQuad(x, y, width, (float)height, 0.0, 0.0, 1.0, 1.0, zLevel);
        } else {
            System.out.println("Image " + image + " was not found");
        }
    }
}

