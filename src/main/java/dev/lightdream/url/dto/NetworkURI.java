/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package dev.lightdream.url.dto;

import com.pixelmongenerations.core.util.ISerializable;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;

public class NetworkURI
implements ISerializable<NetworkURI> {
    public String uri;

    public NetworkURI(String uri) {
        this.uri = uri;
    }

    public static NetworkURI deserialize(ByteBuf buffer) {
        return new NetworkURI(ByteBufUtils.readUTF8String(buffer));
    }

    @Override
    public void serialize(ByteBuf buffer) {
        ByteBufUtils.writeUTF8String(buffer, this.uri);
    }
}

