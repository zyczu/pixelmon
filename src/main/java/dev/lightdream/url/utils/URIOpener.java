/*
 * Decompiled with CFR 0.150.
 */
package dev.lightdream.url.utils;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class URIOpener {
    public static void openURI(String uri_s) {
        try {
            URI uri = new URI(uri_s);
            Desktop.getDesktop().browse(uri);
        }
        catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }
}

