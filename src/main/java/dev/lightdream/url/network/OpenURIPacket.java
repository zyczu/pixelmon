/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  io.netty.buffer.ByteBuf
 */
package dev.lightdream.url.network;

import dev.lightdream.url.dto.NetworkURI;
import dev.lightdream.url.utils.URIOpener;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class OpenURIPacket
implements IMessage {
    public NetworkURI packet;

    public OpenURIPacket() {
    }

    public OpenURIPacket(NetworkURI packet) {
        this.packet = packet;
    }

    public OpenURIPacket(String uri) {
        this.packet = new NetworkURI(uri);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.packet = NetworkURI.deserialize(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        this.packet.serialize(buf);
    }

    public static class Handler
    implements IMessageHandler<OpenURIPacket, IMessage> {
        @Override
        public IMessage onMessage(OpenURIPacket message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> URIOpener.openURI(message.packet.uri));
            return null;
        }
    }
}

